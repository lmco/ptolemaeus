plugins {
    id 'java-library'
    id 'signing'
    id 'io.github.gradle-nexus.publish-plugin' version '1.3.0'
    id 'com.palantir.git-version'              version "3.0.0"
    id 'maven-publish'
    id 'checkstyle'
    id 'com.github.spotbugs' version '5.0.14'           apply false // adds the SpotBugsMain/Test tasks
    id "com.github.hierynomus.license" version "0.16.1" apply false // adds the license tasks
    id 'jacoco'
}

// Project information
group = 'com.lmco'
configurations.implementation.canBeResolved = true

nexusPublishing {
    repositories {
        sonatype()
    }
}

/**
 * Utility function to get the project description
 * @param project The project object
 */
static String getProjectDescription(final String name) {
    return name == "commons" ?
             ("Common utilities such as MultiIndexMap (an efficient implementation Map<Iterable<K>, V>), "
                + "ConcurrentMemoizer (simplifies and wraps the lazy construction and memoization of expensive operations), "
                + "StreamUtils, MapUtils, SetUtils, etc. which - among other common static convenience methods - use "
                + "LinkedHashMap/Set, rather than the default HashMap/Set that the base Java statics in Stream, Collectors, Map and Set, "
                + "and much other commonly sought-after functionality such as Validation (which holds common many parameter checks), "
                + "AtomicDouble, IStreamable, and IPartitionable")
            :
            ("A rigorous and well-sourced Java mathematics library that extends the Hipparchus mathematics library that includes "
                + "linear algebra over R^n and C^n (e.g. Matrix, ComplexMatrix, Eigen, complex Schur using the M-shifted large-bulge QR alg., "
                + "and complex QR and Hessenberg decompositions using Householder transformations), "
                + "frameworks for and implementations of hyper-dimensional (N-dimensional) geometry "
                + "(e.g., NOrthotope, NBall, RealNDomain, AbstractNDomain), weighted non-linear least-squares optimization "
                + "(Levenmerg-Marquardt with geodesic acceleration), non-linear dynamics (e.g., DynamicalSystem, StateTransitionMatrix),"
                + "and a rich framework of multi-variate, vector- and matrix-valued function definitions "
                + "(e.g., MultivariateVectorValuedFunction, VectorField, MultivariateMatrixValuedFunction).  "
                + "Whenever a reference is used in the process of development, that source is included in the JavaDoc and, "
                + "if possible, a hyperlink is provided.");
}

subprojects {
    
    repositories {
        mavenCentral()
        mavenLocal()
    }
    
    apply plugin: 'java-library'
    apply plugin: 'signing'
    apply plugin: 'maven-publish'
    apply plugin: 'checkstyle'
    apply plugin: 'com.github.spotbugs'
    apply plugin: "com.github.hierynomus.license"
    apply plugin: 'jacoco'
    
    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(17))
        }
        
        withJavadocJar()
        withSourcesJar()
        
        modularity.inferModulePath = true
    }
    
    tasks.named('test') {
        useJUnitPlatform{ }
    }
    
    license {
        header = project.file("../LICENSE")
        exclude '**/*.txt'
    }
    
    checkstyle {
        maxErrors = 0
        maxWarnings = 0
        toolVersion = '8.45'
    }
    
    spotbugs {
        excludeFilter = file("../config/spotbugs/exclude.xml")
    }
    
    test {
       finalizedBy jacocoTestReport
    }
    
    jacocoTestReport {
      description = "Generate code coverage reports after running tests."
      reports {
        xml.enabled = true
        html.enabled = true
        csv.enabled = true
      }
    }
    
    javadoc {
        if(JavaVersion.current().isJava9Compatible()) {
            options.addBooleanOption('html5', true)
        }
        
        options.tags = [ "apiNote:a:API Note:",
                         "implSpec:a:Implementation Specification:",
                         "implNote:a:Implementation Note:" ]

        classpath += sourceSets.test.compileClasspath
        source += sourceSets.test.allJava
    }

    /**
     * Generate javadoc for test classes
     */
    task testJar(type: Jar, dependsOn: testClasses) {
        duplicatesStrategy = 'exclude'
        dependsOn("javadoc")
        from sourceSets.test.output
        from sourceSets.test.allSource
        archiveClassifier.set('test')
    }

    publishing {
        publications {
            mavenJava(MavenPublication) {
                String packageName = "${project.name}"
                String projectDescription = getProjectDescription(packageName)
                groupId 'com.lmco.ptolemaeus'
                version gitVersion()
                artifactId packageName
                from components.java
                artifact testJar

                pom {
                    name = packageName
                    description = projectDescription
                    url = 'https://gitlab.com/lmco/ptolemaeus'
                    licenses {
                        license {
                            name = 'MIT'
                            url = 'https://opensource.org/license/mit/'
                        }
                    }
                    developers {
                        developer {
                            id   = '@RyanMoser'
                            name = 'Ryan Moser'
                        }
                        developer {
                            id   = '@PDavisLmco'
                            name = 'Peter Davis'
                        }
                        developer {
                            id   = '@jrw348'
                            name = 'Justin Weaver'
                        }
                        developer {
                            id   = '@bruceromney'
                            name = 'Bruce Romney'
                        }
                        developer {
                            id   = '@joe102192'
                            name = 'Joseph Hernandez'
                        }
                        developer {
                            id   = '@cadude'
                            name = 'Catherine Doud'
                        }
                    }
                    scm {
                        connection.set('scm:git:git://gitlab.com/lmco/ptolemaeus.git')
                        developerConnection.set('scm:git:ssh://gitlab.com/lmco/ptolemaeus.git')
                        url.set('https://gitlab.com/lmco/ptolemaeus')
                    }
                }
            }
        }
    }
    
    signing { // the signing block must come after the publishing block!
        def signingKey = findProperty("signingKey") ?: System.env.signingKey
        def signingPassword = findProperty("signingKeyPassword") ?: System.env.signingKeyPassword
        useInMemoryPgpKeys(signingKey, signingPassword)
        
        sign publishing.publications.mavenJava
    }
    
    def hipparchusVersion = '3.1'
    def mockitoVersion = '5.2.0'
    dependencies {
        constraints {
            implementation 'org.apache.logging.log4j:log4j-core:2.18.0'
            
            implementation 'org.apache.commons:commons-collections4:4.4'
            implementation 'org.apache.commons:commons-lang3:3.9'
            
            implementation "org.hipparchus:hipparchus:${hipparchusVersion}"
            implementation "org.hipparchus:hipparchus-core:${hipparchusVersion}"
            implementation "org.hipparchus:hipparchus-optim:${hipparchusVersion}"
            implementation "org.hipparchus:hipparchus-geometry:${hipparchusVersion}"
            
            compileOnly "com.github.spotbugs:spotbugs-annotations:${spotbugs.toolVersion.get()}"
            testCompileOnly "com.github.spotbugs:spotbugs-annotations:${spotbugs.toolVersion.get()}"
            
            testImplementation "org.mockito:mockito-core:${mockitoVersion}"
            testImplementation "org.mockito:mockito-junit-jupiter:${mockitoVersion}"
        }
    }
}