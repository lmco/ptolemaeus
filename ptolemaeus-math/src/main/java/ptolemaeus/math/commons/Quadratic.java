/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.commons;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;

import ptolemaeus.math.NumericalMethodsException;

/** A class that can compute the {@link QuadraticRoots roots} of a quadratic polynomial
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public final class Quadratic {
    
    /** do nothing */
    private Quadratic() { }
    
    /** Compute and return the {@link QuadraticRoots} of the quadratic polynomial with the given coefficients
     * 
     * @param a the x^2 coefficient
     * @param b the x^1 coefficient
     * @param c the x^0 coefficient
     * @return the {@link QuadraticRoots} of the quadratic polynomial with the given coefficients
     * 
     * @see QuadraticRoots
     * 
     * @apiNote if {@code a == b == c == 0.0}, we return a {@link QuadraticRoots} holding two {@link Complex#ZERO
     *          complex zeroes} but, of course, such a function is zero everywhere. If {@code a == b == 0.0, c != 0.0}
     *          an exception is thrown. If {@code a == 0.0, b != 0.0, c != 0.0} we return a {@link QuadraticRoots}
     *          holding two of the same roots of that linear function. */
    public static QuadraticRoots computeQuadraticRoots(final double a, final double b, final double c) {
        return new QuadraticRoots(a, b, c);
    }
    
    /** Instance of this class solve for and hold the roots of a quadratic polynomial
     * 
     * @author Ryan Moser, Ryan.T.Moser@lmco.com
     * 
     * @apiNote if {@code a == b == c == 0.0}, we return a {@link QuadraticRoots} holding two {@link Complex#ZERO
     *          complex zeroes} but, of course, such a function is zero everywhere. If {@code a == b == 0.0, c != 0.0}
     *          an exception is thrown. If {@code a == 0.0, b != 0.0, c != 0.0} we return a {@link QuadraticRoots}
     *          holding two of the same roots of that linear function. */
    public static final class QuadraticRoots { // TODO Ryan Moser, 2023-07-12: write a constructor for Complex values
        
        /** the more smaller root */
        private final Complex smaller;
        /** the more larger root */
        private final Complex larger;
        
        /** Constructor to compute and save the roots of a quadratic polynomial
         * 
         * @param a the x^2 coefficient
         * @param b the x^1 coefficient
         * @param c the x^0 coefficient */
        private QuadraticRoots(final double a, final double b, final double c) {
            if (a == 0.0) {
                if (b == 0.0) {
                    if (c == 0.0) { // constant-valued function identically equal to zero, c = 0
                        this.smaller = Complex.ZERO;
                        this.larger  = Complex.ZERO;
                    }
                    else {
                        throw new NumericalMethodsException("A non-zero constant-valued function does not have zeroes");
                    }
                }
                else { // degenerate quadratic -> linear
                    if (c == 0.0) { // bx = 0
                        this.smaller = Complex.ZERO;
                        this.larger  = Complex.ZERO;
                    }
                    else { // bx + c = 0
                        final double minusBOnC = -c / b;
                        this.smaller = new Complex(minusBOnC);
                        this.larger  = new Complex(minusBOnC);
                    }
                }
            }
            else { // quadratic, ax^2 + bx + c = 0
                final double desc = b * b - 4.0 * a * c;
                final double twoA = 2.0 * a;
                final double minusBOnTwoA = -b / twoA;
                
                final Complex root1; 
                final Complex root2; 
                if (desc == 0) { // one real root of multiplicity 2
                    root1 = new Complex(minusBOnTwoA, 0.0);
                    root2 = new Complex(minusBOnTwoA, 0.0);
                }
                else {
                    final double posDesc = FastMath.signum(desc) * desc;
                    final double sqrtDesc = FastMath.sqrt(posDesc);
                    if (desc < 0) { // two complex roots
                        root1 = new Complex(minusBOnTwoA,  sqrtDesc / twoA);
                        root2 = new Complex(minusBOnTwoA, -sqrtDesc / twoA);
                    }
                    else { // two real roots
                        root1 = new Complex((-b + sqrtDesc) / twoA, 0.0);
                        root2 = new Complex((-b - sqrtDesc) / twoA, 0.0);
                    }
                }
                
                final double root1Norm = root1.norm();
                final double root2Norm = root2.norm();
                if (root1Norm <= root2Norm) {
                    this.smaller = root1;
                    this.larger  = root2;
                }
                else {
                    this.smaller = root2;
                    this.larger  = root1;
                }
            }
        }
        
        /** @return the root with a smaller {@link Complex#norm() norm} */
        public Complex smallerRoot() {
            return this.smaller;
        }
        
        /** @return the root with a larger {@link Complex#norm() norm} */
        public Complex largerRoot() {
            return this.larger;
        }
        
        /** @return {@code true} if both roots are real, {@code false} otherwise */
        public boolean areReal() {
            return this.smaller.isReal();
        }
        
        /** @return {@code true} if both roots are equal, {@code false} otherwise */
        public boolean areDegenerate() {
            return this.smaller.equals(this.larger); // exact equality check
        }
    }
}
