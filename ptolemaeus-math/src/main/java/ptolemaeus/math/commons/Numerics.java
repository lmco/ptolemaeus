/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.commons;

import static java.util.Objects.requireNonNull;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;

import org.apache.commons.collections4.IterableUtils;

import org.hipparchus.Field;
import org.hipparchus.FieldElement;
import org.hipparchus.complex.Complex;
import org.hipparchus.exception.LocalizedCoreFormats;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.MathArrays;
import org.hipparchus.util.Precision;

import ptolemaeus.commons.BeCareful;
import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;

/**
 * <p>
 * A utility class for running numerical methods.
 * </p>
 * <p>
 * A note on the machine epsilon constants:
 * </p>
 * <p>
 * In terms of error control/round-off-error minimization, these - sqrt(ME) and cbrt(ME) - are the optimal step-sizes
 * {@code h} to use when approximating derivatives using Newton's difference quotient method -
 * {@code (f(x + h) - f(x)) / h} - and the symmetric difference quotient (SDQ) method -
 * {@code (f(x + h) - f(x - h)) / 2h} - respectively.
 * </p>
 * <p>
 * Given that we will be using the SDQ for approximating derivatives, I'm only going to explain more about the cbrt(eps)
 * value. The complete proof of the result for SDQ uses the standard "solve for x in f'(x) == 0" approach to find the h
 * (dx) that minimizes the error-function of h which is the sum of the round-off + approximation errors in the
 * derivative.
 * </p>
 * <p>
 * The actual result of the proof is a bit different than just {@code cbrt(eps)}:
 * {@code cbrt(24 * eps * f(x) / f'''(x))}, but nobody has the 3rd and 0th derivatives and needs to approximate the
 * first. So, the numerical analysts say "use a characteristic scalar" and "x works pretty well!"
 * </p>
 * Numerical differentiation notes sources:
 * <ul>
 * <li><a href="https://aip.scitation.org/doi/pdf/10.1063/1.4822971">Numerical Calculation of Derivatives</a></li>
 * <li><a href="https://en.wikipedia.org/wiki/Numerical_differentiation">Numerical Differentiation</a></li>
 * <li><a href="https://en.wikipedia.org/wiki/Machine_epsilon">Machine Epsilon</a></li>
 * <li>A. Gezerlis, Numerical Methods in Physics with Python, p. [87 - 105]</li>
 * <li>W. H. Press et al., Numerical Recipes 3rd Edition, p. [229, 231]</li>
 * </ul>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public final class Numerics {
    
    /** The {@link BeCareful#reason() reason to be careful} when given the option to clone data arrays */
    public static final String OPTIONAL_CLONE_WARNING =
            "using clone == false results in the data array *reference* being "
          + "copied, not the data itself, thus external modifications to that "
          + "reference will be reflected in the instance!";
    
    /** This is the machine epsilon if using the definition that it is the difference between 1.0 and the next smallest
     * floating-point number that can be represented exactly; i.e., <code>&epsilon;</code> s.t.
     * <code>&epsilon; + 1.0 &gt; 1.0</code> <br>
     * <br>
     * 
     * This is the "mainstream" or "interval" definition of {@code machEps}.<br>
     * <br>
     * 
     * On a 64 bit system following IEEE 754-2008, binary64 machine epsilon =
     * {@code 2 ^ (-52) = 2.220446049250313e-16}<br>
     * <br>
     * 
     * For completeness, the "formal" or "rounding" machine epsilon is the largest representable number s.t.
     * <code>&epsilon; + 1.0 == 1.0</code> and that, of course, is {@link #MACHINE_EPSILON machEps}{@code  / 2.0} */
    public static final double MACHINE_EPSILON = Math.ulp(1.0);
    
    /** The square-root of {@link #MACHINE_EPSILON}. See the {@link Numerics class-level JavaDoc} for an
     * explanation.<br>
     * <br>
     * 
     * On a 64 bit system following IEEE 754-2008,
     * {@code sqrt(machEps) = (2 ^ (-52)) ^ (1 / 2) = 2 ^ (-26) = 1.4901161193847656E-8} */
    public static final double SQUARE_ROOT_MACHINE_EPSILON = FastMath.sqrt(MACHINE_EPSILON);
    
    /** The cube-root of {@link #MACHINE_EPSILON}. See the {@link Numerics class-level JavaDoc} for an
     * explanation.<br>
     * <br>
     * 
     * On a 64 bit system following IEEE 754-2008, {@code cbrt(machEps) = 6.0554544523933395E-6} */
    public static final double CUBE_ROOT_MACHINE_EPSILON = FastMath.cbrt(MACHINE_EPSILON);
    
    /** The smallest <em>normal</em> number that is non-zero.<br>
     * <br>
     * 
     * The FORTRAN spec. doesn't distinguish between normal vs. subnormal numbers - it only discusses "model numbers";
     * i.e., numbers specific to a given model - but restricting this to the normal numbers is much less
     * problematic.<br>
     * <br>
     * 
     * See FORTRAN's intrinsic <a href="https://gcc.gnu.org/onlinedocs/gfortran/TINY.html">tiny</a> function */
    public static final double TINY = Double.MIN_NORMAL;
    
    /** The largest number that is not infinity.<br>
     * <br>
     * 
     * See FORTRAN's intrinsic <a href="https://gcc.gnu.org/onlinedocs/gfortran/HUGE.html">huge</a> function */
    public static final double HUGE = Double.MAX_VALUE; // FastMath.nextDown(Double.POSITIVE_INFINITY)
    
    /** {@link #SAFE_MIN} is the smallest positive representable number s.t.
     * <code>1 / {@link #SAFE_MIN} {@literal <} {@link Double#POSITIVE_INFINITY Infinity}</code> <br>
     * <br>
     * 
     * Note that<br>
     * {@code Double.longBitsToDouble(0x4000000000001L) == SAFE_MIN}<br>
     * and<br>
     * {@code 1.0 / Double.longBitsToDouble(0x4000000000000L) == Infinity}<br>
     * <br>
     * 
     * Lastly, this is computed differently than in LAPACK's
     * <a href="https://www.netlib.org/lapack/explore-html/d4/d86/group__lamch.html">lamch</a> routines.<br>
     * <br>
     * 
     * In particular, following the {@code dlamch} routine would have us compute
     * 
     * <pre>
     * <code>
     * static {
     *     final double small = 1.0 / HUGE;
     *     SAFE_MIN = small * (1.0 + MACHINE_EPSILON);
     * }
     * </code>
     * </pre>
     * 
     * but in doing so, we find that {@code 1.0 / (small * (1.0 + MACHINE_EPSILON)) == Infinity}<br>
     * We cannot use {@link Double#MIN_NORMAL SAFE_MIN == TINY == MIN_NORMAL} because there are values smaller than
     * {@code MIN_NORMAL} which still satisfy the reciprocal criteria, and we can't use {@code Double#MIN_VALUE} because
     * it's far too small.<br>
     * <br>
     * 
     * Instead, this implementation gives us precisely the value that {@code sfmin} is intended to provide; i.e., the
     * smallest positive representable number s.t. {@code 1 / SAFE_MIN < Infinity}. */
    public static final double SAFE_MIN = FastMath.nextUp(1.0 / HUGE);
    
    /** The minimum value for which we will compute a non-constant optimal DX in
     * {@link Numerics#computeOptimalCentralDifferencesDX(double)} */
    public static final double MIN_AUTOSCALING_X = TINY / Numerics.CUBE_ROOT_MACHINE_EPSILON;
    
    /** Two {@link FastMath#PI} */
    public static final double TWO_PI = 2.0 * FastMath.PI;
    
    /** Two {@link FastMath#PI} */
    public static final double TAU = TWO_PI;
    
    /** a {@link Complex} with real and imaginary part equal to {@link #MACHINE_EPSILON}. This is not public because
     * this {@link Complex} actually has magnitude {@code sqrt(2) * MACHINE_EPSILON}, which could cause confusion */
    private static final Complex COMPLEX_MACH_EPS = new Complex(MACHINE_EPSILON, MACHINE_EPSILON);
    
    /** noop */
    private Numerics() { }
    
    /** Computes the {@code 1-norm} of a {@link Complex} number; i.e., computes {@code |Re(x)| + |Im(z)|}
     * 
     * @param z the {@link Complex} number
     * @return {@code |Re(x)| + |Im(z)|} */
    public static double complex1Norm(final Complex z) {
        return complex1Norm(z.getReal(), z.getImaginary());
    }
    
    /** Computes the {@code 1-norm} of a {@link Complex} given as its real and imaginary parts; i.e., computes
     * {@code |re| + |im|}
     * 
     * @param re the real part
     * @param im the imaginary part
     * 
     * @return {@code |re| + |im|}
     * @see #complex1Norm(Complex) */
    public static double complex1Norm(final double re, final double im) {
        return FastMath.abs(re) + FastMath.abs(im);
    }
    
    /** Compute the 2-norm of the given {@link Complex} number
     * 
     * @param z the {@link Complex} number
     * @return the norm of the complex number */
    public static double complexNorm(final Complex z) {
        return complexNorm(z.getReal(), z.getImaginary());
    }
    
    /** Given the real and imaginary components of a complex number, compute the 2-norm of the complex number that they
     * represent.
     * 
     * @param re the real part
     * @param im the imaginary part
     * @return the norm of the complex number */
    public static double complexNorm(final double re, final double im) {
        return FastMath.hypot(re, im);
    }
    
    /** Compute the square of the 2-norm of the given {@link Complex} number
     * 
     * @param z the {@link Complex} number
     * @return the square of the norm of the complex number */
    public static double complexNormSq(final Complex z) {
        return complexNormSq(z.getReal(), z.getImaginary());
    }
    
    /** Given the real and imaginary components of a complex number, compute the square of the 2-norm of the complex
     * number that they represent.
     * 
     * @param re the real part
     * @param im the imaginary part
     * @return the square of the norm of the complex number */
    public static double complexNormSq(final double re, final double im) {
        return MathArrays.linearCombination(re, re, im, im);
    }
    
    /** Given the scalar components of two {@link Complex} values {@code z1 = a + b * i} and {@code z2 = c + d * i},
     * compute and return the real portion of their product:
     * 
     * <pre>
     * (a + b i)(c + d i)
     * 
     * = a c + a d i + b c i - b d
     * 
     * = (a c - b d) + (a d + b c) i
     * 
     * So,
     * 
     * Re((a + b i)(c + d i)) = (a c - b d)
     * </pre>
     * 
     * TODO Ryan Moser, 2024-08-19: investigate whether these special linear combinations improve convergence of the QR
     * algorithm in {@link ComplexSchurDecomposition}, as the performance impact is not insignificant (e.g., 1/2 shorter
     * runtime for random {@code 512x512} {@link ComplexMatrix})<br>
     * <br>
     * 
     * @param a {@code Re(z1)}
     * @param d {@code Im(z1)}
     * @param b {@code Re(z2)}
     * @param c {@code Im(z2)}
     * @return {@code Re(z1 * z2) = a c - b d}
     * 
     * @implNote we do this in the same way that {@link Complex#multiply(Complex)} does, but without constructing a new
     *           {@link Complex} instance. */
    public static double complexXYRe(final double a, final double b,
                                     final double c, final double d) {
        // return a * c - b * d;
        return MathArrays.linearCombination(a, c, -b, d);
    }
    
    /** Given the scalar components of two {@link Complex} values {@code z1 = a + b * i} and {@code z2 = c + d * i},
     * compute and return the imaginary portion of their multiplication:
     * 
     * <pre>
     * (a + b i)(c + d i)
     * 
     * = a c + a d i + b c i - b d
     * 
     * = (a c - b d) + (a d + b c) i
     * 
     * So,
     * 
     * Im((a + b i)(c + d i)) = (a d + b c)
     * </pre>
     * 
     * TODO Ryan Moser, 2024-08-19: investigate whether these special linear combinations improve convergence of the QR
     * algorithm in {@link ComplexSchurDecomposition}, as the performance impact is not insignificant (e.g., 1/2 shorter
     * runtime for random {@code 512x512} {@link ComplexMatrix})<br>
     * <br>
     * 
     * @param a {@code Re(z1)}
     * @param b {@code Im(z1)}
     * @param c {@code Re(z2)}
     * @param d {@code Im(z2)}
     * @return {@code Im(z1 * z2) = a d + b c}
     * 
     * @implNote we do this in the same way that {@link Complex#multiply(Complex)} does, but without constructing a new
     *           {@link Complex} instance. */
    public static double complexXYIm(final double a, final double b,
                                     final double c, final double d) {
        // return a * d + b * c;
        return MathArrays.linearCombination(a, d, b, c);
    }

    /** Given the scalar components of two {@link Complex} values {@code z1 = a + b * i} and {@code z2 = c + d * i},
     * compute and return the real and imaginary parts of their multiplication:
     * 
     * <pre>
     * (a + b i)(c + d i)
     * 
     * = a c + a d i + b c i - b d
     * 
     * = (a c - b d) + (a d + b c) i
     * 
     * So,
     * 
     * Re((a + b i)(c + d i)) = (a c - b d)
     * Im((a + b i)(c + d i)) = (a d + b c)
     * </pre>
     * 
     * TODO Ryan Moser, 2024-08-19: investigate whether these special linear combinations improve convergence of the QR
     * algorithm in {@link ComplexSchurDecomposition}, as the performance impact is not insignificant (e.g., 1/2 shorter
     * runtime for random {@code 512x512} {@link ComplexMatrix})<br>
     * <br>
     * 
     * @param a {@code Re(z1)}
     * @param b {@code Im(z1)}
     * @param c {@code Re(z2)}
     * @param d {@code Im(z2)}
     * @return saveTo {@code { Re(z1 * z2), Im(z1 * z2) } = { (a c - b d), (a d + b c) }} 
     * @implNote we do this in the same way that {@link Complex#multiply(Complex)} does, but without constructing a new
     *           {@link Complex} instance. */
    public static double[] complexMultiply(final double a, final double b,
                                           final double c, final double d) {
        // return {a * c - b * d, a * d + b * c};
        return new double[] { Numerics.complexXYRe(a, b, c, d),
                              Numerics.complexXYIm(a, b, c, d) };
    }
    
    /** For {@code x = a + bi}, {@code y = c + di}, compute {@code x / y}
     * 
     * <pre>
     *  <code>
     *     x       a + bi         ac + bd + (bc - ad)i
     *    --- = ------------ = --------------------------
     *     y       c + di                c<sup>2</sup> + d<sup>2</sup>
     *  </code>
     * </pre>
     * 
     * and save the real and complex parts to the given array.<br><br>
     * 
     * This implementation is adapted from {@link Complex#divide(Complex)}. The algebra is the same, but we skip
     * instantiating any new instances of {@link Complex} don't perform any input checks.<br>
     * <br>
     * 
     * As in {@link Complex#divide(Complex)}, this method uses the algorithm described in the following paper to limit
     * the effects of overflows and underflows in the computation:<br>
     * 
     * <a href="http://doi.acm.org/10.1145/1039813.1039814"> prescaling of operands</a>
     * 
     * @param a the real part of {@code x}
     * @param b the imaginary part of {@code x}
     * @param c the real part of {@code y}
     * @param d the imaginary part of {@code y}
     * @return the result of {@code x / y}, as a {@code double[]}, where the first value in the array is the real part
     *         and the second is the imaginary part */
    public static double[] complexDivide(final double a, final double b,
                                         final double c, final double d) {
        final double[] saveTo = new double[2];
        complexDivide(a, b, c, d, saveTo);
        return saveTo;
    }
    
    /** For {@code x = a + bi}, {@code y = c + di}, compute {@code x / y}
     * 
     * <pre>
     *  <code>
     *     x       a + bi         ac + bd + (bc - ad)i
     *    --- = ------------ = --------------------------
     *     y       c + di                c<sup>2</sup> + d<sup>2</sup>
     *  </code>
     * </pre>
     * 
     * and save the real and complex parts to the given array.<br><br>
     * 
     * This implementation is adapted from {@link Complex#divide(Complex)}. The algebra is the same, but we skip
     * instantiating any new instances of {@link Complex} don't perform any input checks.<br>
     * <br>
     * 
     * As in {@link Complex#divide(Complex)}, this method uses the algorithm described in the following paper to limit
     * the effects of overflows and underflows in the computation:<br>
     * 
     * <a href="http://doi.acm.org/10.1145/1039813.1039814"> prescaling of operands</a>
     * 
     * @param a the real part of {@code x}
     * @param b the imaginary part of {@code x}
     * @param c the real part of {@code y}
     * @param d the imaginary part of {@code y}
     * @param saveTo save the result of the division to this array, which is assumed to be of length exactly 2 */
    public static void complexDivide(final double a, final double b,
                                     final double c, final double d,
                                     final double[] saveTo) {
        if (FastMath.abs(c) < FastMath.abs(d)) {
            double q           = c / d;
            double denominator = c * q + d;
            saveTo[0] = (a * q + b) / denominator;
            saveTo[1] = (b * q - a) / denominator;
        }
        else {
            double q           = d / c;
            double denominator = d * q + c;
            saveTo[0] = (a + b * q) / denominator;
            saveTo[1] = (b - a * q) / denominator;
        }
    }

    /** Compute the square of the complex number<br>
     * <code>a + bi = a<sup>2</sup> + 2abi - b<sup>2</sup> = (a<sup>2</sup> - b<sup>2</sup>) + (2ab)i</code> <br>
     * <br>
     * 
     * Note: this uses {@link #complexXYRe(double, double, double, double)} and
     * {@link #complexXYIm(double, double, double, double)}
     * 
     * @param a the real part of the complex number
     * @param b the real part of the complex number
     * @return the result saved to a new {@code double[2]} with the real part in {@code [0]} and the imaginary part in
     *         {@code [1]} */
    public static double[] complexSquare(final double a, final double b) {
        final double[] saveTo = new double[2];
        complexSquare(a, b, saveTo);
        return saveTo;
    }
    
    /** Compute the square of the complex number<br>
     * <code>a + bi = a<sup>2</sup> + 2abi - b<sup>2</sup> = (a<sup>2</sup> - b<sup>2</sup>) + (2ab)i</code> <br>
     * and save the result to {@code saveTo} <br>
     * <br>
     * 
     * Note: this uses {@link #complexXYRe(double, double, double, double)} and
     * {@link #complexXYIm(double, double, double, double)}
     * 
     * @param a the real part of the complex number
     * @param b the real part of the complex number
     * @param saveTo the array to which we'll save the result, with the real part in {@code [0]} and the imaginary part
     *        in {@code [1]} */
    public static void complexSquare(final double a, final double b, final double[] saveTo) {
        saveTo[0] = Numerics.complexXYRe(a, b, a, b);
        saveTo[1] = Numerics.complexXYIm(a, b, a, b);
    }

    /** Take the square root of a complex number represented as two doubles
     * 
     * @param a the real part of the number to take a sqrt of 
     * @param b the imaginary part of the number to take a sqrt of 
     * @return the square root of x
     */
    public static double[] complexSqrt(final double a, final double b) {
        final double[] saveTo = new double[2];
        complexSqrt(a, b, saveTo);
        return saveTo;
    }

    /** For {@code z = a + bi}, compute {@code \u221A(a + bi)}
     * 
     * <pre>
     *  <code>
     *    Let r = norm(z) = \u221A(a^2 + b^2), then
     *    
     *    \u221A(a + bi) = (\u221A(2)/2) \u221A(r + a)
     *              + (\u221A(2)/2) \u221A(r - a)i
     *  </code>
     * </pre>
     * 
     * and save the real and complex parts to the given array.<br><br>
     * 
     * This implementation is adapted from {@link Complex#sqrt()}. The algebra is the same, but we skip
     * instantiating any new instances of {@link Complex} and don't perform any input checks.<br>
     * <br>
     * 
     * @param a the real part of the number to take a square root of 
     * @param b the imaginary part of the number to take a square root of 
     * @param saveTo the {@code double[]} to save the output to */
    public static void complexSqrt(final double a, final double b, final double[] saveTo) {
        if (a == 0.0 && b == 0.0) {
            saveTo[0] = 0;
            saveTo[1] = 0;
        }

        final double t = FastMath.sqrt((FastMath.abs(a) + FastMath.hypot(a, b)) * 0.5);
        if (FastMath.copySign(1, a) >= 0.0) {
            saveTo[0] = t;
            saveTo[1] = (b / (2.0 * t));
        }
        else {
            saveTo[0] = FastMath.abs(b) / (2.0 * t);
            saveTo[1] = FastMath.copySign(t, b);
        }
    }

    // CHECKSTYLE.OFF: ParameterName - it is customary to name matrices using upper-case characters
    
    // CHECKSTYLE.OFF: LineLength - long URL
    
    /** Compute a value by which we should be able to safely perturb matrix entries. Safely meaning we likely won't
     * disturb the qualities of the matrix too much. This value is also used to perturb Eigenvalues when searching for
     * Eigenvectors of nearly identical Eigenvalues.<br>
     * <br>
     * 
     * This epsilon uses the {@link RealMatrix#getNormInfty() L-infinity norm} of the matrix, and is based on LAPACK.
     * Often, this value is assigned to a variable {@code eps3} (the reason for this name is unknown to me), e.g., see
     * lines 404-406 of <a href=
     * "https://www.netlib.org/lapack/explore-html/d1/dc8/group__hsein_ga84b81b5af925aabb479806c8d2d781d3.html#ga84b81b5af925aabb479806c8d2d781d3">ZHSEIN</a>
     * <br>
     * <br>
     * 
     * If the matrix has {@link RealMatrix#getNormInfty() L-infinity norm == 0}, then we return
     * {@link #getMatrixSmlNum(AnyMatrix)}.
     * 
     * @param A the {@link ComplexMatrix} for which we need a matrix-epsilon
     * @return the matrix epsilon as described */
    public static double getInftyNormMatrixEpsilon(final RealMatrix A) {
        final double infNorm = A.getNormInfty();
        if (infNorm == 0.0) {
            return getMatrixSmlNum(A);
        }
        
        return infNorm * Numerics.MACHINE_EPSILON;
    }
    
    /** Compute a value by which we should be able to safely perturb matrix entries. Safely meaning we likely won't
     * disturb the qualities of the matrix too much. This value is also used to perturb Eigenvalues when searching for
     * Eigenvectors of nearly identical Eigenvalues.<br>
     * <br>
     * 
     * This epsilon uses the {@link ComplexMatrix#getNormInfty() L-infinity norm} of the matrix, and is based on LAPACK.
     * Often, this value is assigned to a variable {@code eps3} (the reason for this name is unknown to me), e.g., see
     * lines 404-406 of <a href=
     * "https://www.netlib.org/lapack/explore-html/d1/dc8/group__hsein_ga84b81b5af925aabb479806c8d2d781d3.html#ga84b81b5af925aabb479806c8d2d781d3">ZHSEIN</a>
     * <br>
     * <br>
     * 
     * If the matrix has {@link ComplexMatrix#getNormInfty() L-infinity norm == 0}, then we return
     * {@link #getMatrixSmlNum(AnyMatrix)}.
     * 
     * @param A the {@link ComplexMatrix} for which we need a matrix-epsilon
     * @return the matrix epsilon as described */
    public static double getInftyNormMatrixEpsilon(final ComplexMatrix A) {
        final double infNorm = A.getNormInfty();
        if (infNorm == 0.0) {
            return getMatrixSmlNum(A);
        }
        
        return infNorm * Numerics.MACHINE_EPSILON;
    }
    
    /** Compute a value analogous to {@link #SAFE_MIN}, but for matrices. I (RM) have not found theoretical motivation
     * for this value, but it is used consistently throughout LAPACK an <em>extremely</em> small non-zero matrix entry;
     * e.g., see line 345 of
     * <a href="https://www.netlib.org/lapack/explore-html/d1/dc8/group__hsein_ga84b81b5af925aabb479806c8d2d781d3.html#ga84b81b5af925aabb479806c8d2d781d3">ZHSEIN</a>
     * 
     * @param A the matrix for which we want a {@code smlnum}
     * @return the {@code smlnum} for the matrix */
    public static double getMatrixSmlNum(final AnyMatrix A) {
        final double k = (A.getRowDimension() + A.getColumnDimension()) / 2.0;
        return SAFE_MIN * (k / MACHINE_EPSILON);
    }
    
    // CHECKSTYLE.ON: LineLength
    
    /** Compute and return the value of A<sup>-1</sup>B without computing the inverse of A explicitly. We do this by
     * solving the equation AX = B for X that minimizes the 2-norm of the error.
     * 
     * @param A a {@link RealMatrix}.  May not be {@code null}.
     * @param B a {@link RealMatrix}.  May not be {@code null}.
     * @param matrix2Decomp a function that can return a {@link DecompositionSolver} for A so that we can use
     *        {@link DecompositionSolver#solve(RealMatrix)}.  May not be {@code null}.
     * @return A<sup>-1</sup>B */
    public static RealMatrix computeAInvB(final RealMatrix A, final RealMatrix B,
                                          final Function<RealMatrix, DecompositionSolver> matrix2Decomp) {
        return matrix2Decomp.apply(A).solve(B);
    }
    
    /** Compute and return the value of A<sup>-1</sup>B without computing the inverse of A explicitly. We do this by
     * solving the equation AX = B for X that minimizes the 2-norm of the error using
     * {@link LinearSolver#solve(RealMatrix, RealMatrix)}.
     * 
     * @param A a {@link RealMatrix}. May not be {@code null}.
     * @param B a {@link RealMatrix}. May not be {@code null}.
     * @return A<sup>-1</sup>B
     * 
     * @see #computeAInvB(RealMatrix, RealMatrix, Function) */
    public static RealMatrix computeAInvB(final RealMatrix A, final RealMatrix B) {
        return LinearSolver.defaultSolver().solve(A, B);
    }
    
    /** Compute and return the value of AB<sup>-1</sup> without computing the inverse of B explicitly. We do this by
     * solving the equation B<sup>T</sup>X<sup>T</sup> = A<sup>T</sup> for X<sup>T</sup> that minimizes the 2-norm of
     * the error, and then transposing X<sup>T</sup> to resolve X itself.<br>
     * <br>
     * 
     * We do this because we want an equation of the form M<sub>1</sub>Y = M<sub>2</sub>.<br>
     * <br>
     * 
     * Starting with X = AB<sup>-1</sup>:<br>
     * <br>
     * 
     * X = AB<sup>-1</sup> <br>
     * XB = A <br>
     * (XB)<sup>T</sup> = A<sup>T</sup> <br>
     * B<sup>T</sup>X<sup>T</sup> = A<sup>T</sup><br>
     * <br>
     * 
     * Thus we can use {@link #computeAInvB(RealMatrix, RealMatrix, Function)} and take the transpose of that result as
     * the solution.
     * 
     * @param A a {@link RealMatrix}.  May not be {@code null}.
     * @param B a {@link RealMatrix}.  May not be {@code null}.
     * @param matrix2Decomp a function that can return a {@link DecompositionSolver} for B<sup>T</sup> so that we can
     *        use {@link DecompositionSolver#solve(RealMatrix)}. May not be {@code null}.
     * @return AB<sup>-1</sup> */
    public static RealMatrix computeABInv(final RealMatrix A, final RealMatrix B,
                                          final Function<RealMatrix, DecompositionSolver> matrix2Decomp) {
        return computeAInvB(B.transpose(), A.transpose(), matrix2Decomp).transpose();
    }
    
    /** Compute and return the value of AB<sup>-1</sup> without computing the inverse of B explicitly. We do this by
     * solving the equation B<sup>T</sup>X<sup>T</sup> = A<sup>T</sup> for X<sup>T</sup> that minimizes the 2-norm of
     * the error using {@link LinearSolver#solve(RealMatrix, RealMatrix)}, and then transposing X<sup>T</sup> to resolve
     * X itself.<br>
     * <br>
     * 
     * We do this because we want an equation of the form M<sub>1</sub>Y = M<sub>2</sub>.<br>
     * <br>
     * 
     * Starting with X = AB<sup>-1</sup>:<br>
     * <br>
     * 
     * X = AB<sup>-1</sup> <br>
     * XB = A <br>
     * (XB)<sup>T</sup> = A<sup>T</sup> <br>
     * B<sup>T</sup>X<sup>T</sup> = A<sup>T</sup><br>
     * <br>
     * 
     * Thus we can use {@link #computeAInvB(RealMatrix, RealMatrix, Function)} and take the transpose of that result as
     * the solution.
     * 
     * @param A a {@link RealMatrix}. May not be {@code null}.
     * @param B a {@link RealMatrix}. May not be {@code null}.
     * @return AB<sup>-1</sup> */
    public static RealMatrix computeABInv(final RealMatrix A, final RealMatrix B) {
        return computeAInvB(B.transpose(), A.transpose()).transpose();
    }
    
    /** Compute and return the value of A<sup>-1</sup>b without computing the inverse of A explicitly. We do this by
     * solving the equation Ax = b for x that minimizes the 2-norm of the error.
     * 
     * @param A a {@link RealMatrix}.  May not be {@code null}.
     * @param b a {@link RealVector}.  May not be {@code null}.
     * @param matrix2Decomp a function that can return a {@link DecompositionSolver} for A so that we can use
     *        {@link DecompositionSolver#solve(RealVector)}.  May not be {@code null}.
     * @return A<sup>-1</sup>b */
    public static RealVector computeAInvB(final RealMatrix A, final RealVector b,
                                          final Function<RealMatrix, DecompositionSolver> matrix2Decomp) {
        return matrix2Decomp.apply(A).solve(b);
    }
    
    /** Compute and return the value of A<sup>-1</sup>b without computing the inverse of A explicitly. We do this by
     * solving the equation Ax = b for x that minimizes the 2-norm of the error using
     * {@link LinearSolver#solve(RealMatrix, RealVector)}.
     * 
     * @param A a {@link RealMatrix}. May not be {@code null}.
     * @param b a {@link RealVector}. May not be {@code null}.
     * @return A<sup>-1</sup>b
     * 
     * @see #computeAInvB(RealMatrix, RealVector, Function) */
    public static RealVector computeAInvB(final RealMatrix A, final RealVector b) {
        return LinearSolver.defaultSolver().solve(A, b);
    }
    
    /** Determine whether the given matrix is square. If not, throw an exception <br>
     * <br>
     * 
     * This implementation is copied straight out of {@link MatrixUtils MatrixUtils::isSymmetricInternal}, although I'm
     * using {@link AnyMatrix#isSquare()} rather than {@code A.getColumnDimension() != A.getRowDimension()}
     * 
     * @param A the {@link ComplexMatrix} to check */
    public static void requireSquare(final AnyMatrix A) {
        if (!A.isSquare()) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.NON_SQUARE_MATRIX,
                                                   A.getRowDimension(), A.getColumnDimension());
        }
    }
    
    /** Check for and throw a {@link NumericalMethodsException} if the given {@link ComplexMatrix} has any
     * {@link Double#NaN} entries (real or imaginary)
     * 
     * @param A the matrix to check
     * @return {@code A}, as is */
    public static ComplexMatrix requireNonNaN(final ComplexMatrix A) {
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                final double ijRe = A.getEntryRe(i, j);
                final double ijIm = A.getEntryIm(i, j);
                if (Double.isNaN(ijRe + ijIm)) {
                    throw NumericalMethodsException.formatted("matrix element at (%d, %d) was NaN", i, j).get();
                }
            }
        }
        
        return A;
    }
    
    /** Check for and throw a {@link NumericalMethodsException} if the given {@link RealMatrix} has any
     * {@link Double#NaN} entries
     * 
     * @param <M> the {@link RealMatrix} type
     * @param A the matrix to check 
     * @return {@code A}, as is */
    public static <M extends RealMatrix> M requireNonNaN(final M A) {
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                final double ij = A.getEntry(i, j);
                if (Double.isNaN(ij)) {
                    throw NumericalMethodsException.formatted("matrix element at (%d, %d) was NaN", i, j).get();
                }
            }
        }
        
        return A;
    }
    
    // CHECKSTYLE.ON: ParameterName
    
    /** @return create and return a new high-precision {@link NumberFormat}
     * @implNote We don't use a constant because {@link NumberFormat} instances are not thread-safe */
    public static NumberFormat highPrecisionNumberFormat() {
        final NumberFormat format = new DecimalFormat("#0.00000000000000000000");
        format.setMinimumFractionDigits(20);
        format.setMinimumFractionDigits(0);
        return format;
    }
    
    /** Calculate the angle from {@code v1} to {@code V2}, signed so that it is positive if {@code v1 x v2} is in the
     * same direction as {@code normal}.
     * 
     * @param v1 the {@link Vector3D} from which we want to measure an angle
     * @param v2 the {@link Vector3D} to which we want to measure an angle
     * @param normal reference vector identifying which way is positive
     * @return angle in degrees (from -pi to pi)  */
    public static double signedAngleTo3D(final Vector3D v1, final Vector3D v2, final Vector3D normal) {
        final double initialAnswer = Vector3D.angle(v1, v2);
        final double multiplier = FastMath.signum(v1.crossProduct(v2).dotProduct(normal));
        return multiplier * initialAnswer;
    }
    
    /** Determine whether {@code x == y} within a relative tolerance equal to one {@link #MACHINE_EPSILON machine
     * epsilon} <br>
     * <br>
     * 
     * See {@link #equalsRelEps(double, double, double)} for details regarding relative vs. absolute tolerance.
     * 
     * @param x the value we're checking
     * @param y the value to check against
     * @return {@code true} or {@code x == y} within a relative tolerance equal to one {@link #MACHINE_EPSILON machine
     *         epsilon}, {@code false} otherwise. This method considers {@code NaN == NaN}.
     *         
     * @see #equalsRelEps(double, double, double) */
    public static boolean equalsRelMachEps(final double x, final double y) {
        return equalsRelEps(x, y, MACHINE_EPSILON);
    }
    
    /** Determine whether {@code x == y} with a relative tolerance of {@code epsilon}<br>
     * <br>
     * 
     * Relative tolerance:<br>
     * <br>
     * 
     * A relative tolerance is one which takes into account the magnitude of the value against which we want to compare
     * other values.<br>
     * <br>
     * 
     * More formally, if {@code x (1 - epsilon) <= y <= x (1 + epsilon)}, we say that {@code y} is equal to {@code x}
     * within relative tolerance {@code epsilon}<br>
     * <br>
     * 
     * E.g.
     * 
     * <pre>
     * Let x == 100, epsilon = 1e-2 (see note1*).
     * 
     * Then our bounds are
     *    [100 - 100 * 1e-2, 100 + 100 * 1e-2]
     * == [100 - 1, 100 + 1]
     * == [99, 101]
     * 
     * So - e.g. - 99, 99.5, 100, 100.5, and 101 are all considered equal to x == 100 within the relative tolerance.
     * </pre>
     * 
     * *note1: in practice, values of epsilon should be very small (see {@link #equalsRelMachEps(double, double)} which
     * uses {@link #MACHINE_EPSILON} by default
     * 
     * Absolute tolerance:<br>
     * <br>
     * 
     * An absolute tolerance is one which is without respect to the magnitude of the values being compared.<br>
     * <br>
     * 
     * E.g.:
     * 
     * <pre>
     * Let x == 100, epsilon = 1e-2 (in practice, values of epsilon should be very small)
     * 
     * Then our bounds are
     *    [100 - 1e-2, 100 + 1e-2]
     * == [99.99, 100.01]
     * 
     * So - e.g. - 99.999, 100, 100.001, and 100.01 are all considered equal to x == 100 within the absolute tolerance.
     * </pre>
     * 
     * @param x the value we're checking
     * @param y the value to check against
     * @param epsilon the relative tolerance
     * @return {@code true} if {@code x == y} with a relative tolerance of {@code epsilon}, {@code false} otherwise.
     *         This method considers {@code NaN == NaN}.
     *         
     * @see #computeRelativeDifference(double, double) */
    public static boolean equalsRelEps(final double x, final double y, final double epsilon) {
        final int maxUlps = epsilon == 0 ? 0 : 1; // max num floating points between two values considered equal
        if ((x == Double.POSITIVE_INFINITY && y == Double.POSITIVE_INFINITY)        // both positive infinity?
                || (x == Double.NEGATIVE_INFINITY && y == Double.NEGATIVE_INFINITY) // both negative infinity?
                || (Precision.equalsIncludingNaN(x, y, maxUlps))) {    // adjacent doubles or both NaN?
            return true;
        }
        
        final double aBit = FastMath.abs(x * epsilon);
        final double aBitMore = x + aBit; // if x == Double.MAX_VALUE, aBitMore will be Infinity, which works
        final double aBitLess = x - aBit; // if x == -Double.MAX_VALUE, aBitLess will be -Infinity, which works
        return aBitLess <= y && y <= aBitMore;
    }
    
    /** Determine whether {@code x == y} within a relative tolerance equal to one {@link #MACHINE_EPSILON machine
     * epsilon}<br>
     * <br>
     * 
     * See {@link #equalsRelEps(double, double, double)} for details regarding relative vs. absolute tolerance.
     * 
     * @param x the value we're checking
     * @param y the value to check against
     * @return {@code true} if {@code x == y} within a relative tolerance equal to one {@link #MACHINE_EPSILON machine
     *         epsilon} on both the real and complex axes, {@code false} otherwise. This method considers
     *         {@code NaN == NaN}.
     * 
     * @see #equalsRelEps(double, double, double) */
    public static boolean equalsRelMachEps(final Complex x, final Complex y) {
        return equalsRelEps(x, y, COMPLEX_MACH_EPS);
    }
    
    /** Determine whether {@code x == y} with a relative tolerance of {@code epsilon}<br>
     * <br>
     * 
     * See {@link #equalsRelEps(double, double, double)} for details regarding relative vs. absolute tolerance.
     * 
     * @param x the {@link Complex} value we're checking
     * @param y the {@link Complex} to check against
     * @param relEpsilon a {@link Complex} that specifies the relative tolerance in the real an imaginary directions
     * @return {@code true} if {@code x == y} within a relative tolerance equal to {@code epsilon}, {@code false}
     *         otherwise. This method considers {@code NaN == NaN}.
     * 
     * @see #equalsRelEps(double, double, double) */
    public static boolean equalsRelEps(final Complex x,
                                       final Complex y,
                                       final Complex relEpsilon) {
        return     equalsRelEps(x.getReal(),      y.getReal(),      relEpsilon.getReal())
                && equalsRelEps(x.getImaginary(), y.getImaginary(), relEpsilon.getImaginary());
    }
    
    /** Compute and return the relative difference between {@code x} and {@code y}. Given {@code x, y}, the relative
     * difference is defined as {@code |x - y| / max(|x|, |y|)}.
     * 
     * <br>
     * <br>
     * 
     * Examples:
     * <ul>
     * <li>x := 1.0, y := 1.0, relDiff(x, y) = 0.0</li>
     * <li>x := 1.0, y := 1.1, relDiff(x, y) = 0.1 / 1.1 = 0.090909</li>
     * <li>x := 12, y := 32, relDiff(x, y) = 20 / 32 = 0.625</li>
     * </ul>
     * 
     * @param x the first value
     * @param y the second value
     * @return the relative difference between {@code x} and {@code y} */
    public static double computeRelativeDifference(final double x, final double y) {
        if (Precision.equalsIncludingNaN(x, y, 0)) {
            return 0.0;
        }
        
        final double absoluteMax = FastMath.max(FastMath.abs(x), FastMath.abs(y));
        return FastMath.abs((x - y) / absoluteMax);
    }
    
    /** Compare {@link RealVector} element-by-element equality using the specified tolerance
     * 
     * @see RealVector#equals(Object)
     * @param v1 the first {@link RealVector}
     * @param v2 the second {@link RealVector}
     * @param absoluteTolerance the entry tolerance. If two entries are less than this far apart, we consider them
     *        equal.
     * @return {@code true} if the {@link RealVector}s are equal, {@code false} otherwise */
    public static boolean vectorElementwiseEquals(final RealVector v1,
                                                  final RealVector v2,
                                                  final double absoluteTolerance) {
        Objects.requireNonNull(v1, "v1 may not be null");
        Objects.requireNonNull(v2, "v2 may not be null");
        Validation.requireEqualsTo(v1.getDimension(),
                                             v2.getDimension(),
                                             "v1 and v2 must have the same dimension");
        
        if (v2.isNaN()) {
            return v1.isNaN();
        }
        
        return v2.getLInfDistance(v1) <= absoluteTolerance;
    }
    
    /**
     * Given a function f : {@code K}-space -&gt; R<sup>N</sup> and a collection of {@code K}'s, evaluate all provided
     * {@code K}'s and append the results in a single array of length {@code K}'s.size * the length of the array output
     * of the provided {@link Function}
     * 
     * @param <K> the function input type
     * @param f the function
     * @param inputs the collection of inputs
     * @return the concatenated array
     */
    public static <K> double[] generateVector(final Function<K, double[]> f, final Iterable<K> inputs) {
        final int inputSize = IterableUtils.size(inputs);
        double[] answer = null;
        int i = 0;
        for (final K k : inputs) {
            final double[] component = f.apply(k);
            if (answer == null) {
                answer = new double[inputSize * component.length];
            }
            
            for (final double d : component) {
                answer[i++] = d;
            }
        }
        
        return answer;
    }
    
    /**
     * Find the residuals of f at xn
     * 
     * @param f the function
     * @param xn the input to 'f'
     * @param targetValues the values we wish to reach
     * @return the residuals of f at xn
     */
    public static double[] getResiduals(final UnaryOperator<double[]> f,
                                        final double[] xn,
                                        final double[] targetValues) {
        Objects.requireNonNull(f, "the function f may not be null");
        Objects.requireNonNull(xn, "the point xn may not be null");
        Objects.requireNonNull(targetValues, "the target values may not be null");
        double[] yn = f.apply(xn);
        Validation.requireEqualsTo(
                targetValues.length,
                yn.length,
                "targetValues must have the same dimension as the output of f");
        
        double[] residuals = new double[targetValues.length];
        for (int i = 0; i < targetValues.length; i++) {
            residuals[i] = targetValues[i] - yn[i];
        }
        return residuals;
    }
    
    /**
     * Sum the squares of a series of doubles
     * 
     * @param doubles the doubles to square and sum
     * @return the sum of the squares of a series of doubles
     */
    public static double sumSq(final double... doubles) {
        Objects.requireNonNull(doubles, "the doubles to square and sum may not be null");
        double answer = 0.0;
        for (double d : doubles) {
            answer += d * d;
        }
        return answer;
    }
    
    /**
     * Add an array and a scaled array
     * 
     * @param toAddTo the array to which we are adding (will not modify this, we return a new array)
     * @param toScaleAndAdd the array to scale before adding
     * @param scale the amount by which we wish to scale values before adding them
     * @return a new array which is equal to toAddTo + scale * toScaleAndAdd
     */
    public static double[] addScaledArray(final double[] toAddTo, final double[] toScaleAndAdd, final double scale) {
        Objects.requireNonNull(toAddTo, "toAddTo may not be null");
        Objects.requireNonNull(toScaleAndAdd, "toScaleAndAdd may not be null");
        Validation.requireEqualsTo(
                toAddTo.length,
                toScaleAndAdd.length,
                "toAddTo and toScaleAndAdd must have the same length");
        final double[] answer = new double[toAddTo.length];
        for (int i = 0; i < toAddTo.length; i++) {
            answer[i] = toAddTo[i] + toScaleAndAdd[i] * scale;
        }
        return answer;
    }
    
    /**
     * create and return a double array of length 'length' whose entries are all 'val' 
     * 
     * @param length the length of the array to be created
     * @param val the value to insert for each element
     * @return a double array of length 'length' whose entries are all 'val'
     */
    public static double[] getFilledArray(final int length, final double val) {
        double[] array = new double[length];
        Arrays.fill(array, val);
        return array;
    }
    
    /** Compute the optimal differential at a point, assuming that we're using central differences in our derivative
     * calculations. <br>
     * In this context, "differential" means the finite step size to use when calculating a derivative using finite
     * differences. The function is evaluated at the reference point plus or minus this differential. "Optimal" means
     * the smallest possible step size that exceeds the noise floor due to finite precision.<br>
     * 
     * @param x the point at which we want a differential
     * @return the optimal differential at a point, assuming that we're using central differences in our derivative
     *         calculations */
    public static double computeOptimalCentralDifferencesDX(final double x) {
        return CUBE_ROOT_MACHINE_EPSILON * FastMath.max(1.0, FastMath.abs(x));
    }
    
    /** Create and return a {@link MultivariateMatrixValuedFunction} which numerically approximates the Jacobian for the
     * given {@link MultivariateVectorValuedFunction}
     * 
     * @param f the {@link MultivariateVectorValuedFunction}
     * @param optDXs the {@link Optional} approximate differentials. If not provided, we will compute and use the
     *        error-optimal approximate differentials given xs (see {@link #computeOptimalCentralDifferencesDX(double)})
     * @param inputDim the input dimension
     * @param outputDim the output dimension
     * @param parallelism a {@link Parallelism} value to dictate whether we should compute the individual gradients in
     *        parallel, or sequentially. For {@link MultivariateVectorValuedFunction}s which are computationally
     *        expensive, choose {@link Parallelism#PARALLEL}. Otherwise, the parallelism overhead may not be worth it.
     *        If {@code null}, we use {@link Parallelism#SEQUENTIAL}.
     *        
     * @return a {@link MultivariateMatrixValuedFunction} which numerically approximates the Jacobian for the given
     *         {@link MultivariateVectorValuedFunction}
     * 
     * @see #computeOptimalCentralDifferencesDX(double)
     * @see #evaluateJacobian(MultivariateVectorValuedFunction, double[], double[], int, int, Parallelism) */
    public static MultivariateMatrixValuedFunction getJacobianFunction(final MultivariateVectorValuedFunction f,
                                                                       final Optional<double[]> optDXs,
                                                                       final int inputDim,
                                                                       final int outputDim,
                                                                       final Parallelism parallelism) {
        if (optDXs.isPresent()) {
            final double[] dxsClone = optDXs.map(double[]::clone).orElse(null); // only need to clone once
            return xs -> Numerics.evaluateJacobian(f, xs, dxsClone, inputDim, outputDim, parallelism);
        }
        
        return xs -> {
            final double[] localDXs = new double[inputDim];
            for (int i = 0; i < inputDim; i++) { // compute the error-optimal approximate differentials given xs
                localDXs[i] = Numerics.computeOptimalCentralDifferencesDX(xs[i]);
            }
            
            return Numerics.evaluateJacobian(f, xs, localDXs, inputDim, outputDim, parallelism);
        };
    }
    
    /** Compute the Jacobian {@link Matrix} of the provided {@link MultivariateVectorValuedFunction} at the given point
     * using the given differential array
     * 
     * @param f the {@link MultivariateVectorValuedFunction}
     * @param xs the point at which we want to evaluate the Jacobian
     * @param dxs the differential array
     * @param inputDimension the input dimension of the function
     * @param outputDimension the output dimension of the function
     * 
     * @return the Jacobian {@link Matrix} of the provided {@link MultivariateVectorValuedFunction} at the given point
     *         using the given differential array
     *         
     * @implNote passes {@code null} into {@link #evaluateGradient(ToDoubleFunction, double[], double[], Parallelism)}
     *           so that the Jacobian's constituent gradients are computed {@link Parallelism#SEQUENTIAL
     *           sequentially}. */
    public static Matrix evaluateJacobianSequentially(final MultivariateVectorValuedFunction f,
                                                      final double[] xs,
                                                      final double[] dxs,
                                                      final int inputDimension,
                                                      final int outputDimension) {
        return evaluateJacobian(f, xs, dxs, inputDimension, outputDimension, null);
    }
    
    /** Compute the Jacobian {@link Matrix} of the provided {@link MultivariateVectorValuedFunction} at the given point
     * using the given differential array
     * 
     * @param f the {@link MultivariateVectorValuedFunction}
     * @param xs the point at which we want to evaluate the Jacobian
     * @param dxs the differential array
     * @param inputDimension the input dimension of the function
     * @param outputDimension the output dimension of the function
     * @param parallelism a {@link Parallelism} value to dictate whether we should compute the individual gradients in
     *        parallel, or sequentially. For {@link MultivariateVectorValuedFunction}s which are computationally
     *        expensive, choose {@link Parallelism#PARALLEL}. Otherwise, the parallelism overhead may not be worth it.
     *        If {@code null}, we use {@link Parallelism#SEQUENTIAL}.
     *        
     * @return the Jacobian {@link Matrix} of the provided {@link MultivariateVectorValuedFunction} at the given point
     *         using the given differential array */
    public static Matrix evaluateJacobian(final MultivariateVectorValuedFunction f,
                                          final double[] xs,
                                          final double[] dxs,
                                          final int inputDimension,
                                          final int outputDimension,
                                          final Parallelism parallelism) {
        final double[][] answer = new double[outputDimension][inputDimension];
        if (parallelism == Parallelism.PARALLEL) {
            IntStream.range(0, inputDimension).parallel().forEach(i -> updateJacobianIth(answer, i, f, xs, dxs));
        }
        else {
            for (int i = 0; i < inputDimension; i++) {
                updateJacobianIth(answer, i, f, xs, dxs);
            }
        }
        
        return new Matrix(answer);
    }
    
    /** Update the i<sup>th</sup> column of the given {@code double[][]} with the appropriate value of the Jacobian.
     * 
     * @param answer the array to update. To be clear <b>this is updated in place</b>.
     * @param i the column index
     * @param f the function for which we're computing the Jacobian
     * @param xs the value at which we're evaluating the Jacobian
     * @param dxs the finite differentials */
    private static void updateJacobianIth(final double[][] answer,
                                          final int i,
                                          final UnaryOperator<double[]> f,
                                          final double[] xs,
                                          final double[] dxs) {
        final double[] plusDx = xs.clone();
        final double[] mnusDx = xs.clone();
        
        plusDx[i] = xs[i] + dxs[i];
        mnusDx[i] = xs[i] - dxs[i];
        
        final double[] fOfXsPlusDxs = f.apply(plusDx);
        final double[] fOfXsMnusDxs = f.apply(mnusDx);
        for (int j = 0; j < answer.length; j++) {
            answer[j][i] = (fOfXsPlusDxs[j] - fOfXsMnusDxs[j]) / (2.0 * dxs[i]);
        }
    }
    
    /** Compute and return the gradient of the provided function at the given point using the given differential array
     * 
     * @param f the {@link ToDoubleFunction}
     * @param xs the point at which we want to compute the gradient
     * @param dxs the the differential array
     * 
     * @return the Jacobian {@link Matrix} of the provided {@link MultivariateVectorValuedFunction} at the given point
     *         using the given differential array
     *         
     * @implNote passes {@code null} into {@link #evaluateGradient(ToDoubleFunction, double[], double[], Parallelism)}
     *           so that the Jacobian's constituent gradients are computed {@link Parallelism#SEQUENTIAL
     *           sequentially}. */
    public static double[] evaluateGradientSequentially(final ToDoubleFunction<double[]> f,
                                                        final double[] xs,
                                                        final double[] dxs) {
        return evaluateGradient(f, xs, dxs, null);
    }
    
    /** Compute and return the gradient of the provided function at the given point using the given differential array
     * 
     * @param f the {@link ToDoubleFunction}
     * @param xs the point at which we want to compute the gradient
     * @param dxs the the differential array
     * @param parallelism a {@link Parallelism} value to dictate whether we should compute the individual components of
     *        the gradient in parallel, or sequentially. For {@link MultivariateVectorValuedFunction}s which are
     *        computationally expensive, choose {@link Parallelism#PARALLEL}. Otherwise, the parallelism overhead may
     *        not be worth it. If {@code null}, we use {@link Parallelism#SEQUENTIAL}.
     *        
     * @return the gradient of the provided function at the given point using the given differential array */
    public static double[] evaluateGradient(final ToDoubleFunction<double[]> f,
                                            final double[] xs,
                                            final double[] dxs,
                                            final Parallelism parallelism) {
        final int inputDimension = xs.length;
        final double[] answer = new double[inputDimension];
        /* see evaluateJacobian for a discussion of the parallel Stream reasoning */
        if (parallelism == Parallelism.PARALLEL) {
            IntStream.range(0, inputDimension).parallel().forEach(i -> updateGradientIth(answer, i, f, xs, dxs));
        }
        else {
            for (int i = 0; i < inputDimension; i++) {
                updateGradientIth(answer, i, f, xs, dxs);
            }
        }
        
        return answer;
    }
    
    /** Update the i<sup>th</sup> element of the given {@code double[][]} with the appropriate value of the gradient.
     * 
     * @param answer the array to update. To be clear <b>this is updated in place</b>.
     * @param i the column index
     * @param f the function for which we're computing the gradient
     * @param xs the value at which we're evaluating the gradient
     * @param dxs the finite differentials */
    private static void updateGradientIth(final double[] answer,
                                          final int i,
                                          final ToDoubleFunction<double[]> f,
                                          final double[] xs,
                                          final double[] dxs) {
        final double[] plusDx = xs.clone();
        final double[] mnusDx = xs.clone();
        
        plusDx[i] = xs[i] + dxs[i];
        mnusDx[i] = xs[i] - dxs[i];
        
        final double fOfXsPlusDxs = f.applyAsDouble(plusDx);
        final double fOfXsMnusDxs = f.applyAsDouble(mnusDx);
        answer[i] = (fOfXsPlusDxs - fOfXsMnusDxs) / (2.0 * dxs[i]);
    }
    
    /** Compute and return the derivative of the provided function at the given point using the given differential
     * 
     * @param f the function
     * @param x the point
     * @param dx the differential
     * @return the derivative of the provided function at the given point using the given differential */
    public static double evaluateDerivative(final DoubleUnaryOperator f, final double x, final double dx) {
        return (f.applyAsDouble(x + dx) - f.applyAsDouble(x - dx)) / (2.0 * dx);
    }
    
    /** Check whether {@code v} is non-{@code null} and has the correct dimension. If either of those are false, we
     * throw an exception.
     * 
     * @param v the {@link RealVector} to check
     * @param dim the required dimension */
    public static void requireNonNullAndDimension(final RealVector v, final int dim) {
        Objects.requireNonNull(v, "vector may not be null");
        Validation.requireGreaterThanEqualTo(dim, 1, "dim");
        Validation.requireEqualsTo(v.getDimension(), dim, "v dimension");
    }
    
    /**
     * <p>
     * The Cartesian product of two sets A and B is the set of all ordered pairs (a, b) where a is an element of A and b
     * is an element of B.
     * </p>
     * 
     * <p>
     * This method creates the <i>n-ary</i> Cartesian product of the provided lists. The Cartesian product of the sets
     * S<sub>i</sub> belonging to <b>S</b> is the set of all ordered n-tuples (x<sub>0</sub>, x<sub>1</sub>, ...,
     * x<sub>n</sub>) where x<sub>i</sub> belongs to S<sub>i</sub> belongs to <b>S</b>.
     * </p>
     * <p>
     * In set-builder notation: {(x<sub>0</sub>, x<sub>1</sub>, ..., x<sub>n</sub>) | x<sub>i</sub> &#8712;
     * S<sub>i</sub> &#8704; i &#8712; {0, ..., n}}
     * </p>
     * <p>
     * See <a href="https://en.wikipedia.org/wiki/Cartesian_product"> Cartesian Product</a>
     * </p>
     * 
     * @param <T> The element type of the list of lists for which we are creating a Cartesian product
     * @param listlist the list of lists for which we are creating a Cartesian product
     * @return the Cartesian product of the lists in {@code listlist}
     */
    public static <T> List<List<T>> cartesianProduct(final List<List<T>> listlist) {
        Objects.requireNonNull(listlist, "the listlist must be specified");
        
        if (listlist.stream().anyMatch(List::isEmpty)) { // reason: A x {} = {}
            return List.of();
        }
        
        final List<T> current = new ArrayList<>(listlist.size());
        for (int i = 0; i < listlist.size(); i++) { // add nulls explicitly so that the list isn't empty
            current.add(null);
        }
        
        final List<List<T>> answer = new ArrayList<>(); // modified to include the Cartesian product
        cartesianProduct(listlist, current, 0, answer);
        return answer;
    }
    
    /**
     * Algorithm inspired by: https://stackoverflow.com/a/10262388/12297765
     * 
     * @param <T> The element type of the list of lists for which we are creating a Cartesian product
     * @param listlist the list of lists for which we are creating a Cartesian product
     * @param current the list to which we are currently adding elements
     * @param k the current index to which we will add an element
     * @param answer the list of lists which holds the results of the Cartesian product. This list is modified to
     *        include the Cartesian product results.
     */
    private static <T> void cartesianProduct(final List<List<T>> listlist,
                                             final List<T> current,
                                             final int k,
                                             final List<List<T>> answer) {
        if (k == listlist.size()) {
            /* k being our indexing int, if k == listlist.size, we have reached the last sublist and need to add a
             * result. This result is one complete entry for the Cartesian product answer. We must make a copy because
             * `current` is modified as we go. */
            answer.add(new ArrayList<>(current));
        }
        else {
            final List<T> list = listlist.get(k); /* we add elements of this list to the partial Cartesian product
                                                   * result before either recursing or adding a complete result
                                                   * depending on the value of k and the length of listlist */
            for (int i = 0; i < list.size(); i++) {
                current.set(k, list.get(i)); /* set the next value. This is why we must set nulls explicitly in the
                                              * public CatProb call */
                cartesianProduct(listlist, current, k + 1, answer); // increment k and recurse!
            }
        }
    }
    
    /**
     * Create a string representation of an array of doubles with consistent fractional part length and whose total 
     * length is less than twice the fractional length.  If the numbers are too large to fit into the total size 
     * restriction, they will not be truncated, but will also not be column aligned.
     * 
     * @param array the array for which we will create a string
     * @param fractionalLength the desired length of the fractional parts of the doubles
     * @return a string representation of an array of doubles
     */
    public static String doubleArrayToString(final double[] array, final int fractionalLength) {
        Objects.requireNonNull(array, "array may not be null");
        StringBuilder builder = new StringBuilder();
        String formattingString = "% " + 2 * fractionalLength + "." + fractionalLength + "f";
        builder.append('[');
        for (int i = 0; i < array.length; i++) {
            builder.append(String.format(formattingString, array[i]));
            if (i != array.length - 1) {
                builder.append(", ");
            }
        }
        builder.append(']');
        return builder.toString();
    }

    /** @param x the value to square
     * @return the square of x */
    public static double square(final double x) {
        return x * x;
    }
    
    /** @param x the value to cube
     * @return the cube of x */
    public static double cube(final double x) {
        return x * x * x;
    }
    
    /** @param a a value
     * @param b a value
     * @return the minimum of {@code a and b} */
    public static double min(final double a, final double b) {
        return a < b ? a : b;
    }
    
    /** @param a a value
     * @param b a value
     * @param c a value
     * @return the minimum of {@code a, b, and c} */
    public static double min(final double a, final double b, final double c) {
        return min(min(a, b), c);
    }
    
    /** @param x the {@code double}s from which we want to extract the minimum
     * @return the minimum value among the values of {@code x}. Note:
     *         {@code min() == min(x) == Double.POSITIVE_INFINITY} for any zero-length array {@code x}. */
    public static double min(final double... x) {
        double min = Double.POSITIVE_INFINITY;
        for (int j = 0; j < x.length; j++) {
            min = min(min, x[j]);
        }
        
        return min;
    }
    
    /** @param a a value
     * @param b a value
     * @return the minimum of {@code a and b} */
    public static double max(final double a, final double b) {
        return a > b ? a : b;
    }
    
    /** @param a a value
     * @param b a value
     * @param c a value
     * @return the maximum of {@code a, b, and c} */
    public static double max(final double a, final double b, final double c) {
        return max(max(a, b), c);
    }
    
    /** @param x the {@code double}s from which we want to extract the maximum
     * @return the maximum value among the values of {@code x}. Note:
     *         {@code max() == max(x) == Double.NEGATIVE_INFINITY} for any zero-length array {@code x}. */
    public static double max(final double... x) {
        double max = Double.NEGATIVE_INFINITY;
        for (int j = 0; j < x.length; j++) {
            max = max(max, x[j]);
        }
        
        return max;
    }
    
    /** Compute and return a string showing the non-zero structure of the given {@link ComplexMatrix}. Blank spaces are
     * those which were zero. This is the same as calling {@link #toNonZeroStructureString(ComplexMatrix, double, char)}
     * with {@code minNorm == 0.0} and {@code nonZeroMarker == '*'}.
     * 
     * @param theMatrix the {@link ComplexMatrix}
     * @return the structure string */
    public static String toNonZeroStructureString(final ComplexMatrix theMatrix) {
        return toNonZeroStructureString(theMatrix, 0.0, '*');
    }
    
    /** Compute and return a string showing the non-zero structure of the given {@link ComplexMatrix}. Blank spaces are
     * those which were zero.
     * 
     * @param theMatrix the {@link ComplexMatrix}
     * @param minNorm {@link Complex} values with {@link Complex#norm() norms (a.k.a. magnitudes)} larger than this will
     *        be displayed as larger than zero
     * @param nonZeroMarker how to mark entries which are non-zero
     * @return the structure string */
    public static String toNonZeroStructureString(final ComplexMatrix theMatrix,
                                                  final double minNorm,
                                                  final char nonZeroMarker) {
        return toStructureString(theMatrix, f -> f.norm() > minNorm, nonZeroMarker);
    }
    
    /** Compute and return a string showing the structure of the given {@link FieldMatrix} according to the given
     * {@link Predicate}. Blank spaces are those which evaluated to {@code false}.
     * 
     * @param <F> the {@link Field} type
     * @param theMatrix the {@link FieldMatrix}
     * @param showOrNot when this {@link Predicate} evaluates to {@code true} for an element of {@code theMatrix}, we
     *        include it in the structure string. If it does not, we do not include it.
     * @param passingMarker how to mark entries which for which {@code showOrNot} evaluates to {@code true}
     * @return the structure string */
    public static <F extends FieldElement<F>> String toStructureString(final FieldMatrix<F> theMatrix,
                                                                       final Predicate<F> showOrNot,
                                                                       final char passingMarker) {
        final StringBuilder bldr = new StringBuilder();
        for (int i = 0; i < theMatrix.getRowDimension(); i++) {
            for (int j = 0; j < theMatrix.getColumnDimension(); j++) {
                final F      elt      = theMatrix.getEntry(i, j);
                final String toAppend = showOrNot.test(elt) ? " " + passingMarker : "  ";
                bldr.append(toAppend);
            }
            bldr.append(System.lineSeparator());
        }
        return bldr.toString();
    }
    
    // CHECKSTYLE.OFF: LocalFinalVariableName - it is customary to name matrices using upper-case characters
    
    /** Create a piece of Java code that can be used to recreate the given {@link AnyMatrix} instance
     * 
     * @param anyA the {@link AnyMatrix} for which we want Java code
     * @return the Java code string representative of {@code A} */
    public static String toJavaCode(final AnyMatrix anyA) {
        return toJavaCode(anyA, "A");
    }
    
    /** Create a piece of Java code that can be used to recreate the given {@link AnyMatrix} instance
     * 
     * @param anyA the {@link AnyMatrix} for which we want Java code
     * @param variableName the name to give to the variable
     * @return the Java code string representative of {@code A} */
    public static String toJavaCode(final AnyMatrix anyA, final String variableName) {
        requireNonNull(variableName, "a variable name must be specified");
        final int rowDim = anyA.getRowDimension();
        final int colDim = anyA.getColumnDimension();
        
        final ComplexMatrix A = ComplexMatrix.of(anyA);
        
        final double[][][] reIm = A.toReImParts();
        final double[][]   re   = reIm[0];
        final double[][]   im   = reIm[1];
        
        final String[][] reStrs = Matrix.doubleMatrixToStringMatrix(re, 0);
        final String[][] imStrs = Matrix.doubleMatrixToStringMatrix(im, 0);
        
        final StringBuilder answerBldr = new StringBuilder();
        
        final boolean isReal          = A.isReal(0.0);
        final String  dataArrayStr    = variableName + "Data";
        final String  doubleOrComplex = isReal ? "double" : "Complex";
        answerBldr.append("final ")
                  .append(doubleOrComplex)
                  .append("[][] ")
                  .append(dataArrayStr)
                  .append(" = new ")
                  .append(doubleOrComplex)
                  .append("[][] {")
                  .append(System.lineSeparator());
        
        for (int i = 0; i < rowDim; ++i) {
            answerBldr.append("    { ");
            for (int j = 0; j < colDim; ++j) {
                final String ijString = isReal ? reStrs[i][j]
                                               : "new Complex(" + reStrs[i][j] + "," + imStrs[i][j] + ")";
                answerBldr.append(ijString);
                if (j != colDim - 1) {
                    answerBldr.append(", ");
                }
            }
            
            if (i != rowDim - 1) {
                answerBldr.append(" },");
                answerBldr.append(System.lineSeparator());
            }
            else {
                answerBldr.append(" }");
            }
        }
        
        answerBldr.append(System.lineSeparator())
                  .append("};")
                  .append(System.lineSeparator());
        
        if (isReal && anyA instanceof RealMatrix) { /* we should only write code to create a RealMatrix if the original
                                                     * was not only real in terms of its entries, but also in terms of
                                                     * its actual types */
            answerBldr.append("final RealMatrix ")
                      .append(variableName)
                      .append(" = new Matrix(")
                      .append(dataArrayStr)
                      .append(");");
        }
        else {
            answerBldr.append("final ComplexMatrix ")
                      .append(variableName)
                      .append(" = new ComplexMatrix(")
                      .append(dataArrayStr)
                      .append(");");
        }
        
        return answerBldr.toString();
    }
    
    /** Create a piece of Wolfram Mathematica code that can be used to recreate the given {@link AnyMatrix} instance
     * 
     * @param anyA the {@link AnyMatrix} for which we want Mathematica code
     * @return the Mathematica code string representative of {@code A} */
    public static String toMathematicaCode(final AnyMatrix anyA) {
        return toMathematicaCode(anyA, "A");
    }
    
    /** Create a piece of Wolfram Mathematica code that can be used to recreate the given {@link AnyMatrix} instance
     * 
     * @param anyA the {@link AnyMatrix} for which we want Mathematica code
     * @param variableName the name to give to the variable
     * @return the Mathematica code string representative of {@code A} */
    public static String toMathematicaCode(final AnyMatrix anyA, final String variableName) {
        requireNonNull(variableName, "a variable name must be specified");
        final int rowDim = anyA.getRowDimension();
        final int colDim = anyA.getColumnDimension();
        
        final ComplexMatrix A = ComplexMatrix.of(anyA);
        
        final double[][][] reIm = A.toReImParts();
        final double[][]   re   = reIm[0];
        final double[][]   im   = reIm[1];
        
        final String[][] reStrs = Matrix.doubleMatrixToStringMatrix(re, 0);
        final String[][] imStrs = Matrix.doubleMatrixToStringMatrix(im, 0);
        
        final StringBuilder answerBldr = new StringBuilder();
        
        answerBldr.append(variableName + " = {")
                  .append(System.lineSeparator());
        
        final boolean isReal = A.isReal(0.0);
        for (int i = 0; i < rowDim; ++i) {
            answerBldr.append("    { ");
            for (int j = 0; j < colDim; ++j) {
                final String ijString = isReal ? reStrs[i][j] : reStrs[i][j] + " +" + imStrs[i][j] + " I";
                answerBldr.append(ijString);
                if (j != colDim - 1) {
                    answerBldr.append(", ");
                }
            }
            if (i != rowDim - 1) {
                answerBldr.append(" },");
                answerBldr.append(System.lineSeparator());
            }
            else {
                answerBldr.append(" }");
            }
        }
        
        answerBldr.append(System.lineSeparator())
                  .append("};");
        
        return answerBldr.toString();
    }
    
    /** Create a piece of MATLAB code that can be used to recreate the given {@link AnyMatrix} instance
     * 
     * @param anyA the {@link AnyMatrix} for which we want MATLAB code
     * @return the MATLAB code string representative of {@code A} */
    public static String toMATLABCode(final AnyMatrix anyA) {
        return toMATLABCode(anyA, "A");
    }
    
    /** Create a piece of MATLAB code that can be used to recreate the given {@link AnyMatrix} instance
     * 
     * @param anyA the {@link AnyMatrix} for which we want MATLAB code
     * @param variableName the name to give to the variable
     * @return the MATLAB code string representative of {@code A} */
    public static String toMATLABCode(final AnyMatrix anyA, final String variableName) {
        requireNonNull(variableName, "a variable name must be specified");
        final int rowDim = anyA.getRowDimension();
        final int colDim = anyA.getColumnDimension();
        
        final ComplexMatrix A = ComplexMatrix.of(anyA);
        
        final double[][][] reIm = A.toReImParts();
        final double[][]   re   = reIm[0];
        final double[][]   im   = reIm[1];
        
        final String[][] reStrs = Matrix.doubleMatrixToStringMatrix(re, 0);
        final String[][] imStrs = Matrix.doubleMatrixToStringMatrix(im, 0);
        
        final StringBuilder answerBldr = new StringBuilder();
        
        answerBldr.append(variableName + " = [")
                  .append(System.lineSeparator());
        
        final boolean isReal = A.isReal(0.0);
        for (int i = 0; i < rowDim; ++i) {
            answerBldr.append("    ");
            for (int j = 0; j < colDim; ++j) {
                final String ijString = isReal ? reStrs[i][j] : reStrs[i][j] + " +" + imStrs[i][j] + " * 1i";
                answerBldr.append(ijString);
                if (j != colDim - 1) {
                    answerBldr.append(", ");
                }
            }
            
            answerBldr.append(";");
            
            if (i != rowDim - 1) {
                answerBldr.append(System.lineSeparator());
            }
        }
        
        answerBldr.append(System.lineSeparator())
                  .append("];");
        
        return answerBldr.toString();
    }
    // CHECKSTYLE.ON: LocalFinalVariableName - it is customary to name matrices using upper-case characters

}