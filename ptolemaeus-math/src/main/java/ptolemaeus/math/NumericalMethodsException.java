/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math;

import java.util.function.Supplier;

/**
 * An exception to use in the numericalmethods package
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class NumericalMethodsException extends RuntimeException {
    
    /**
     * Generated
     */
    private static final long serialVersionUID = 2000916143003931183L;
    
    /**
     * Constructs a new {@link NumericalMethodsException} with the specified detail message and cause.
     */
    public NumericalMethodsException() {
        this(null, null);
    }
    
    /**
     * Constructs a new {@link NumericalMethodsException} with the specified detail message and cause.
     *
     * @param msg the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     */
    public NumericalMethodsException(final String msg) {
        this(msg, null);
    }
    
    /**
     * Constructs a new {@link NumericalMethodsException} with the specified detail message and cause.
     *
     * @param msg the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param t the cause (which is saved for later retrieval by the {@link #getCause()} method). (A {@code null} value
     *        is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public NumericalMethodsException(final String msg, final Throwable t) {
        super(msg, t);
    }
    
    /**
     * Get a {@link NumericalMethodsException} {@link Supplier} that can provide an exception with a formatted message
     * 
     * @param format the format string
     * @param args the format string args
     * @return a {@link NumericalMethodsException} {@link Supplier} that can provide an exception with a formatted
     *         message
     */
    public static Supplier<NumericalMethodsException> formatted(final String format,
                                                                final Object... args) {
        return () -> new NumericalMethodsException(String.format(format, args));
    }
}
