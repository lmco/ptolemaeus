/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import org.hipparchus.linear.AnyMatrix;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** An abstract class to hold the results of a QR decomposition, as well as static utility methods.<br>
 * <br>
 * Given an {@code m x n} matrix {@code A}, the {@code QR} decomposition is a product of two matrices {@code Q} and
 * {@code R}, where {@code Q} is unitary (Q<sup>H</sup> = Q^<sup>-1</sup>, where <sup>H</sup> indicates the conjugate
 * transpose) and {@code R} is upper-triangular (a.k.a. right-triangular).<br>
 * <br>
 * If {@code A} is real, the conjugate transpose is equal to the transpose and is written <sup>T</sup>, and {@code Q} is
 * called an orthogonal matrix.<br>
 * <br>
 * We also lazily initialize the so-called "reduced" or "economical" QR factorization as-needed; i.e., when {@code A} is
 * "tall" - that is, when {@code A} has fewer columns than rows - we only need to store part of the Q and R
 * matrices:<br>
 * <br>
 * 
 * In the case that {@code A} is tall, {@code A = QR} where {@code Q} is {@code (m x m)} and {@code R} is
 * {@code (m x n)}. {@code R} is still "upper-tringular" in the sense that it's an upper-triangular matrix with
 * {@code (m - n)} rows of zeros appended to the bottom.<br>
 * <br>
 * 
 * Using this fact, we only need to consider the first {@code n} columns of {@code Q}, so we can partition {@code Q} and
 * {@code R} into {@code Q}<sub>1</sub> and {@code Q}<sub>2</sub> and {@code R}<sub>1</sub> and {@code R}<sub>2</sub> as
 * follows:
 * 
 * <pre>
 * <code>
 * Q = [Q1 Q2]
 * 
 * R = [ R1   = [ R1
 *       R2 ]      0 ]
 * </code>
 * </pre>
 * 
 * (concatenate the {@code Q}s horizontally and concatenate the {@code R}s vertically).<br>
 * <br>
 * 
 * where {@code Q}<sub>1</sub> is composed of the first {@code n} columns of {@code Q} and {@code Q}<sub>2</sub> is
 * composed of the last {@code (m - n)} columns of {@code Q} and, likewise, {@code R}<sub>1</sub> is composed of the
 * first {@code n} rows of {@code R} and {@code R}<sub>2</sub> is a {@code (m - n) x n} zero-matrix; i.e., is composed
 * of the last {@code (m - n)} rows of {@code R}, which are all zero.<br>
 * <br>
 * Then we have {@code A = QR = Q}<sub>1</sub>{@code R}<sub>1</sub> and {@code Q}<sub>1</sub> and {@code R}<sub>1</sub>
 * are called the "economical" or "reduced" forms of {@code Q} and {@code R}. <br>
 * <br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * @see ComplexQRDecomposition
 * @see RealQRDecomposition */
public abstract sealed class QRDecomposition<M extends AnyMatrix> permits RealQRDecomposition, ComplexQRDecomposition {
    
    /** {@code Q} from {@code A = QR} */
    protected final M Q;
    
    /** {@code R} from {@code A = QR} */
    protected final M R;
    
    /** the lazily-initialized reduced (or economical) form of {@link #Q}.
     * 
     * @see RealQRDecomposition#setupReducedFactorization()
     * @see ComplexQRDecomposition#setupReducedFactorization() */
    protected volatile M reducedQ;
    
    /** the lazily-initialized reduced (or economical) form of {@link #R}.
     * 
     * @see RealQRDecomposition#setupReducedFactorization()
     * @see ComplexQRDecomposition#setupReducedFactorization() */
    protected volatile M reducedR;
    
    /** Constructor
     * 
     * @param Q {@code Q} from {@code A = QR}
     * @param R {@code R} from {@code A = QR} */
    protected QRDecomposition(final M Q, final M R) {
        this.Q = Q;
        this.R = R;
        
        if (this.R.isSquare()) { // R is only square when the original A - from A = QR - is square
            this.reducedQ = this.Q;
            this.reducedR = this.R;
        }
        else {
            this.reducedQ = null;
            this.reducedR = null;
        }
    }
    
    /** @return {@code Q} from the decomposition {@code A = QR}. {@code Q} is unitary s.t. {@code Q}<sup>-1</sup> ==
     *         {@code Q}<sup>H</sup> or, equivalently, {@code Q.}{@code Q}<sup>H</sup> ==
     *         {@code Q}<sup>H</sup>{@code .Q} == {@code I}.  Implicitly, {@code Q} is square. */
    @SuppressFBWarnings({"EI"})
    public M getQ() {
        return this.Q;
    }
    
    /** @return {@code R} from the decomposition {@code A = QR}.  {@code R} is upper-triangular. */
    @SuppressFBWarnings({"EI"})
    public M getR() {
        return this.R;
    }
    
    /** @return the lazily-initialized reduced (or economical) form of {@link #Q}. See class-level JavaDoc for details.
     *         If the decomposed matrix was square, this will be the same instance as {@link #getR()}.
     *         
     * @see RealQRDecomposition#setupReducedFactorization()
     * @see ComplexQRDecomposition#setupReducedFactorization() */
    @SuppressFBWarnings({"EI"})
    public M getReducedQ() {
        this.setupReducedFactorization();
        return this.reducedQ;
    }
    
    /** @return the lazily-initialized reduced (or economical) form of {@link #R}. See class-level JavaDoc for details.
     *         If the decomposed matrix was square, this will be the same instance as {@link #getQ()}.
     *         
     * @see RealQRDecomposition#setupReducedFactorization()
     * @see ComplexQRDecomposition#setupReducedFactorization() */
    @SuppressFBWarnings({"EI"})
    public M getReducedR() {
        this.setupReducedFactorization();
        return this.reducedR;
    }
    
    /** Set up the reduced factorization as specified in the class-level JavaDoc. */
    protected abstract void setupReducedFactorization();
    
    @Override
    public String toString() {
        return "Q:%n%s%n%nR:%n%s".formatted(this.getQ(), this.getR());
    }
}
