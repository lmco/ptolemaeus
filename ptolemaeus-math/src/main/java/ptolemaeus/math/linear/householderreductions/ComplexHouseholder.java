/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.SinCos;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;

/** A class to compute and hold {@link Householder} transformations of {@link ComplexVector}s for {@link ComplexMatrix
 * complex matrices}. See {@link Householder} for details and theory.<br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-02-09: write a {@code preAndPostApplyInPlace} using the following ({@code [ , ]} is the
 * outer-product operator):
 * 
 * <pre>
 * <code>
 * housePrePost(A) = (I - 2[w, w<sup>H</sup>])A(I - 2[w, w<sup>H</sup>])
 *                 = (A - 2[w, w<sup>H</sup>]A)(I - 2[w, w<sup>H</sup>])
 *                 = A - 2A[w, w<sup>H</sup>] - 2[w, w<sup>H</sup>]A + 4[w, w<sup>H</sup>]A[w, w<sup>H</sup>]
 *                 = A - 2[Aw, w<sup>H</sup>] - 2[w, w<sup>H</sup>A] + 4[w, w<sup>H</sup>]A[w, w<sup>H</sup>]
 * </code>
 * </pre>
 * 
 * The difficulty is determining how to <em>best</em> reduce <code>[w, w<sup>H</sup>]A[w, w<sup>H</sup>]</code> in a way
 * analogous to how we reduce the other two outer-products (the other two are the same as in the standard pre/post apply
 * methods, see {@link Householder} for details).<br>
 * <br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see Householder
 * @see ComplexQRDecomposition
 * @see ComplexHessenbergReduction
 * @see ComplexSchurDecomposition */
public final class ComplexHouseholder implements Householder<ComplexMatrix> {
    
    /** The dimension of the Householder transformation */
    private final int dim;
    
    /** {@code true} if this Householder transformation is the identity, {@code false} otherwise
     * @see #isNoop() */
    private final boolean isNoop;
    
    /** the real part of the complex Householder vector */
    private final double[] wRe;
    
    /** the imaginary part of the complex Householder vector */
    private final double[] wIm;
    
    /** Standard constructor
     * 
     * @param wRe the real part of the complex Householder vector
     * @param wIm the imaginary part of the complex Householder vector */
    private ComplexHouseholder(final double[] wRe, final double[] wIm) {
        this.dim    = wRe.length;
        this.isNoop = false;
        
        this.wRe  = wRe;
        this.wIm  = wIm;
    }
    
    /** The constructor for NOOP instances.
     * @param dim the dimension of the Householder transformation for which this noop instance was created
     * @see #isNoop() */
    private ComplexHouseholder(final int dim) {
        this.dim    = dim;
        this.isNoop = true;
        this.wRe  = null; 
        this.wIm  = null; 
    }
    
    @Override
    public boolean isNoop() {
        return this.isNoop;
    }
    
    /** Compute the {@link ComplexHouseholder} for the given {@link ComplexVector}. This method
     * assumes {@code hessenbergOffset == 0}. See the {@code interface}-level JavaDoc of {@link Householder} for
     * details.
     * 
     * @param x the {@link ComplexVector}
     * @return the {@link ComplexHouseholder} to transform the given vector into a multiple of the first unit-vector */
    public static ComplexHouseholder of(final ComplexVector x) {
        final double[][] xReIm = x.getDataRef();
        return of(xReIm[0], xReIm[1]);
    }
    
    /** Compute the {@link ComplexHouseholder} for the given real and imaginary parts of a complex vector. This method
     * assumes {@code hessenbergOffset == 0}. See the {@code interface}-level JavaDoc of {@link Householder} for
     * details.
     * 
     * @param xRe the real part of the vector
     * @param xIm the imaginary part of the vector
     * @return the {@link ComplexHouseholder} to transform the given vector into a multiple of the first unit-vector */
    public static ComplexHouseholder of(final double[] xRe, final double[] xIm) {
        final int n = xRe.length;
        Validation.requireEqualsTo(xIm.length, n, "Re and Im parts");
        
        if (isZero(xRe, xIm)) {
            return new ComplexHouseholder(n);
        }
        
        final Complex k = computeK(xRe, xIm);
        
        final double[] uArrRe = xRe.clone();
        final double[] uArrIm = xIm.clone();
        uArrRe[0] -= k.getReal();
        uArrIm[0] -= k.getImaginary();
        
        // @formatter:off
        /* The Hermitian norm is non-zero for x non-zero, but finite precision may result in it being zero anyway; e.g.,
         * 
         * if x < sqrt(Double.MIN_VALUE) * sqrt(2) / 2 = 1.5717277847026288E-162
         * 
         * the norm of the vector (x, 0, 0, ...) will come back as 0.
         * 
         * We elegantly handle a few special cases, but we can't do much in the general case, so this is just the way
         * it is and why it's only a warning here and not in the JavaDoc. */
        // @formatter:on
        
        final double uArrHermNorm = hermitianNorm(uArrRe, uArrIm);
        if (uArrHermNorm == 0.0) {
            return new ComplexHouseholder(n);
        }
        
        for (int i = 0; i < n; i++) { /* In the reference (see Householder interface),
                                       * there is a new variable w := normalized(u) */
            uArrRe[i] /= uArrHermNorm;
            uArrIm[i] /= uArrHermNorm;
        }
        
        return new ComplexHouseholder(uArrRe, uArrIm);
    }
    
    @Override
    public void preApplyInPlace(final ComplexMatrix A, final int rowStart, final int colStart) {
        if (!this.isNoop()) {
            final int n = A.getColumnDimension();
            
            final double[][][] AReIm = A.getDataRef();
            final double[][]   ARe   = AReIm[0];
            final double[][]   AIm   = AReIm[1];
            
            final double[][] wHAReIm = preMultiplySubMatrixByConjugate(this.wRe, this.wIm,
                                                                       ARe,      AIm,
                                                                       rowStart, colStart, n - 1);
            final double[]   wHARe   = wHAReIm[0];
            final double[]   wHAIm   = wHAReIm[1];
            
            /* we need the outer product of w and wA: w.outerProduct(w^H * A)
             * but we can compute the elements of the outer product as-needed, thus: */
            for (int i = 0; i < this.dim; i++) {
                // get the re and im parts of the i^th element of w
                final double wiRe = this.wRe[i];
                final double wiIm = this.wIm[i];
                
                for (int j = 0; j < n - colStart; j++) {
                    // and the re and im parts of the j^th element of w^H * A
                    final double wHAjRe = wHARe[j];
                    final double wHAjIm = wHAIm[j];
                    
                    // and compute the re and im parts of the ij^th element of the outer product:
                    final double wwHAijRe = Numerics.complexXYRe(wiRe, wiIm, wHAjRe, wHAjIm);
                    final double wwHAijIm = Numerics.complexXYIm(wiRe, wiIm, wHAjRe, wHAjIm);
                    
                    A.subtractFromEntryUnsafe(i + rowStart, j + colStart, 2 * wwHAijRe, 2 * wwHAijIm);
                }
            }
        }
    }
    
    /** Compute the result of pre-multiplying a sub-matrix (sub-{@code double[][]}) of {@code A} by the given the
     * complex conjugate of the given row-vector (as a {@code double[]}) without constructing the sub-matrix itself.<br>
     * <br>
     * 
     * Note that the upper-bound of the row indices is implicit; i.e., it's determined by the length of the
     * pre-multiplying vector.
     * 
     * @param wRe the real part of the vector by which we want to pre-multiply the sub-matrix
     * @param wIm the imaginary part of the vector by which we want to pre-multiply the sub-matrix (do not externally
     *        negate for the conjugate; it's done internally).
     * @param ARe the real part of the matrix whose sub-matrix we want to pre-multiply
     * @param AIm the imaginary part of the matrix whose sub-matrix we want to pre-multiply
     * @param rowStart the starting row of the sub-matrix (inclusive)
     * @param colStart the starting column of the sub-matrix (inclusive)
     * @param colEnd the ending column of the sub-matrix (inclusive)
     * @return the result of the pre-multiplication */
    static double[][] preMultiplySubMatrixByConjugate(final double[] wRe,
                                                      final double[] wIm,
                                                      final double[][] ARe,
                                                      final double[][] AIm,
                                                      final int rowStart,
                                                      final int colStart,
                                                      final int colEnd) {
        final int numColsSubMatrix = colEnd - colStart + 1;
        final int wDim             = wRe.length;
        
        final double[] wARe = new double[numColsSubMatrix];
        final double[] wAIm = new double[numColsSubMatrix];
        
        for (int i = rowStart; i < rowStart + wDim; i++) {
            final double   wiRe =  wRe[i - rowStart];
            final double   wiIm = -wIm[i - rowStart]; // negate for conjugate
            
            final double[] AiRe = ARe[i];
            final double[] AiIm = AIm[i];
            
            for (int j = colStart; j <= colEnd; j++) {
                final double AijRe = AiRe[j];
                final double AijIm = AiIm[j];
                
                //                                     a     b     c      d
                final double re = Numerics.complexXYRe(wiRe, wiIm, AijRe, AijIm); // computes a c - b d
                final double im = Numerics.complexXYIm(wiRe, wiIm, AijRe, AijIm); // computes a d + b c
                
                wARe[j - colStart] += re;
                wAIm[j - colStart] += im;
            }
        }
        
        return new double[][] { wARe, wAIm };
    }
    
    @Override
    public void postApplyInPlace(final ComplexMatrix A, final int rowStart, final int colStart) {
        if (!this.isNoop()) {
            final int m = A.getRowDimension();
            
            final double[][][] AReIm = A.getDataRef();
            final double[][]   ARe   = AReIm[0];
            final double[][]   AIm   = AReIm[1];
            
            final double[][] AwReIm = postMultiplySubMatrix(ARe, AIm, this.wRe, this.wIm, rowStart, m - 1, colStart);
            final double[]   AwRe   = AwReIm[0];
            final double[]   AwIm   = AwReIm[1];
            
            /* we need the outer product of Aw and w^H: Aw.outerProduct(wH)
             * but we can compute the elements of the outer product as-needed, thus: */
            for (int i = 0; i < m - rowStart; i++) {
                // get the re and im parts of the i^th element of Aw
                final double AwiRe = AwRe[i];
                final double AwiIm = AwIm[i];
                
                for (int j = 0; j < this.dim; j++) {
                    // get the re and (negative) im parts of the j^th element of w
                    final double wjRe  =  this.wRe[j];
                    final double wjImH = -this.wIm[j]; // negate for the conjugate
                    
                    // and compute the re and im parts of the ij^th element of the outer product:
                    final double AwwHijRe = Numerics.complexXYRe(AwiRe, AwiIm, wjRe, wjImH);
                    final double AwwHijIm = Numerics.complexXYIm(AwiRe, AwiIm, wjRe, wjImH);
                    
                    A.subtractFromEntryUnsafe(i + rowStart, j + colStart, 2 * AwwHijRe, 2 * AwwHijIm);
                }
            }
        }
    }
    
    /** Compute the result of post-multiplying a sub-matrix (sub-{@code double[][]}) of {@code A} by the given
     * column-vector (as a {@code double[]}) without constructing the sub-matrix itself.<br>
     * <br>
     * 
     * Note that the upper-bound of the column indices is implicit; i.e., it's determined by the length of the
     * post-multiplying vector.
     * 
     * @param wRe the real part of the vector by which we want to post-multiply the sub-matrix
     * @param wIm the imaginary part of the vector by which we want to post-multiply the sub-matrix
     * @param ARe the real part of the matrix whose sub-matrix we want to post-multiply
     * @param AIm the imaginary part of the matrix whose sub-matrix we want to post-multiply
     * @param rowStart the starting row of the sub-matrix (inclusive)
     * @param rowEnd the ending row of the sub-matrix (inclusive)
     * @param colStart the starting column of the sub-matrix (inclusive)
     * @return the result of the post-multiplication */
    static double[][] postMultiplySubMatrix(final double[][] ARe,
                                            final double[][] AIm,
                                            final double[] wRe,
                                            final double[] wIm,
                                            final int rowStart,
                                            final int rowEnd,
                                            final int colStart) {
        final int numRowsSubMatrix = rowEnd - rowStart + 1;
        final int wDim             = wRe.length;
        
        final double[] AwRe = new double[numRowsSubMatrix];
        final double[] AwIm = new double[numRowsSubMatrix];
        
        for (int i = rowStart; i <= rowEnd; i++) {
            final double[] AiRe = ARe[i];
            final double[] AiIm = AIm[i];
            
            double AwiRe = 0.0;
            double AwiIm = 0.0;
            for (int j = colStart; j < colStart + wDim; j++) {
                // Awi += wRe[j - colStart] * AiRe[j];
                final double wjRe  = wRe[j - colStart];
                final double wjIm  = wIm[j - colStart];
                final double AijRe = AiRe[j];
                final double AijIm = AiIm[j];
                
                //                                     a     b     c      d
                final double re = Numerics.complexXYRe(wjRe, wjIm, AijRe, AijIm); // computes a c - b d
                final double im = Numerics.complexXYIm(wjRe, wjIm, AijRe, AijIm); // computes a d + b c
                
                AwiRe += re;
                AwiIm += im;
            }
            
            AwRe[i - rowStart] = AwiRe;
            AwIm[i - rowStart] = AwiIm;
        }
        
        return new double[][] { AwRe, AwIm };
    }
    
    @Override
    public ComplexMatrix getTransform() {
        if (this.isNoop()) {
            return ComplexMatrix.identity(this.dim);
        }
        return iMinusTwiceSelfOuterProduct(this.wRe, this.wIm);
    }
    
    /** Compute and return the Householder transformation {@link ComplexMatrix} for {@link ComplexVector x} given
     * {@code hessenbergOffset = 0}.
     * 
     * @param xRe the real part of the vector for which we want to compute a Householder transform
     * @param xIm the imaginary part of the vector for which we want to compute a Householder transform
     * @return the Householder matrix */
    public static ComplexMatrix computeTransform(final double[] xRe, final double[] xIm) {
        return ComplexHouseholder.of(xRe, xIm).getTransform();
    }
    
    /** Compute and return {@code k = }e<sup>(i arg(x<sub>0</sub>))</sup> &#183; (-sqrt(x<sup>H</sup>&#183;x))<br>
     * The expression x<sup>H</sup>&#183;x is the Hermitian dot-product of the complex vector {@code x} with itself.
     * 
     * @param xRe the real      part of the vector for which we want to compute a Householder transform
     * @param xIm the imaginary part of the vector for which we want to compute a Householder transform
     * @return {@link Complex k} to use in {@link #computeTransform(ComplexVector, int)} */
    static Complex computeK(final double[] xRe, final double[] xIm) {
        final double x0Re  = xRe[0];
        final double x0Im  = xIm[0];
        
        if (x0Im == 0.0) {
            if (x0Re < 0.0) {
                return Complex.valueOf(hermitianNorm(xRe, xIm), 0.0);
            }
            return Complex.valueOf(-hermitianNorm(xRe, xIm), 0.0);
        }
        if (x0Re == 0.0) {
            if (x0Im < 0.0) {
                return Complex.valueOf(0.0, hermitianNorm(xRe, xIm));
            }
            return Complex.valueOf(0.0, -hermitianNorm(xRe, xIm));
        }
        
        final double alpha       = FastMath.atan2(x0Im, x0Re); // see Complex::getArgument
        final SinCos sinCosArgX0 = FastMath.sinCos(alpha);
        final double sigma       = -hermitianNorm(xRe, xIm);
        
        return Complex.valueOf(sigma * sinCosArgX0.cos(), sigma * sinCosArgX0.sin()); // e^(i arg(x_0)) * (-sqrt(x^H.x))
    }
    
    /** Compute {@code I - 2[w, w}<sup>H</sup>{@code ]}, where {@code [, ]} denotes the outer product.
     * 
     * @param wRe the array of the real components of a complex vector {@code w}
     * @param wIm the array of the imaginary components of a complex vector {@code w}
     * @return {@code I - 2[w, w}<sup>H</sup>{@code ]}
     * 
     * @see ComplexVector#outerProduct(FieldVector) */
    static ComplexMatrix iMinusTwiceSelfOuterProduct(final double[] wRe, final double[] wIm) {
        final int n = wRe.length;
        final double[][] answerRe = new double[n][];
        final double[][] answerIm = new double[n][];
        for (int i = 0; i < n; i++) {
            final double[] rowRe = new double[n];
            final double[] rowIm = new double[n];
            for (int j = 0; j < n; j++) {
                final double a =  wRe[i];
                final double b =  wIm[i];
                
                final double c =  wRe[j];
                final double d = -wIm[j]; // w^H, so we conjugate here
                
                final double re = Numerics.complexXYRe(a, b, c, d); // computes a c - b d
                final double im = Numerics.complexXYIm(a, b, c, d); // computes a d + b c
                
                if (i == j) { /* we're computing the the identity matrix, minus the outer product, so when i == j we
                               * need to account for the "1" on the diagonal */
                    rowRe[j] = 1 - 2 * re;
                    rowIm[j] =    -2 * im;
                }
                else {
                    rowRe[j] = -2 * re;
                    rowIm[j] = -2 * im;
                }
            }
            
            answerRe[i] = rowRe;
            answerIm[i] = rowIm;
        }
        
        return new ComplexMatrix(answerRe, answerIm,
                                 false, // false -> don't clone the arrays
                                 ComplexMatrix.DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Compute and return the Householder transformation {@link ComplexMatrix} for {@link ComplexVector x} given
     * {@code hessenbergOffset >= 0}.
     * 
     * @param x the vector for which we want to compute a Householder transform
     * @param hessenbergOffset the last index in the vector {@code Q.x} that we want to be non-zero. In other words,
     *        each entry in {@code Q.x} with index strictly greater than this value will be zero.<br>
     *        The name comes from the use of a non-zero value when computing a Hessenberg reduction.
     * @return the Householder matrix */
    public static ComplexMatrix computeTransform(final ComplexVector x, final int hessenbergOffset) {
        final int n = x.getDimension();
        Validation.requireWithinRange(hessenbergOffset, 0, n - 1, "Householder index");
        
        if (hessenbergOffset != 0) {
            final int           startIndex = hessenbergOffset;
            final int           numElts    = x.getDimension() - hessenbergOffset;
            final ComplexVector subX       = x.getSubVector(startIndex, numElts);
            /* Regarding getSubVector, for posterity: contrary to most methods of the type "getSub...", the input to
             * getSubVector is not a range, but a starting index and "numElts", a total number of elements (or
             * equivalently, the dimension of the returned vector).
             * 
             * E.g.: let x = (a, b, c, d, e, f, g), hhi = 3
             * 
             * Then, dim = 7, startIndex = hhi + 1 = 4, and
             * 
             * numElts = dim - (hhi + 1) = dim - hhi - 1 = 7 - 3 - 1 = 3
             * 
             * so, x.getSubVector(startIndex, numElts) = x.getSubVector(4, 3) = (5, 6, 7) */
            
            final ComplexMatrix subHH = computeTransform(subX, 0); // recurse
            
            final ComplexMatrix answer = ComplexMatrix.identity(n, Parallelism.PARALLEL);
            final double[][][] subHHData = subHH.getDataRef();
            answer.setSubMatrix(subHHData[0], subHHData[1], startIndex, startIndex);
            
            return answer;
        }
        
        final double[][] xReIm = x.getCopyOfReImArrays();
        return computeTransform(xReIm[0], xReIm[1]);
    }
    
    /** Determine whether the given complex vector - with real and imaginary parts given separately - is the zero vector
     * 
     * @param xRe the real part of the complex vector
     * @param xIm the imaginary part of the complex vector
     * @return {@code true} if {@code x == 0}, {@code false} otherwise */
    static boolean isZero(final double[] xRe, final double[] xIm) {
        for (int i = 0; i < xRe.length; i++) {
            if (xRe[i] != 0.0 || xIm[i] != 0.0) {
                return false;
            }
        }
        
        return true;
    }
    
    /** Compute the Hermitian norm of the given complex vector, with real and imaginary parts given separately
     * 
     * @param xRe the real part of the complex vector
     * @param xIm the imaginary part of the complex vector
     * @return the Hermitian norm */
    static double hermitianNorm(final double[] xRe, final double[] xIm) {
        double sqHermNorm = 0.0;
        for (int i = 0; i < xRe.length; i++) {
            final double a = xRe[i];
            final double b = xIm[i];
            
            sqHermNorm += Numerics.complexNormSq(a, b);
        }
        
        return FastMath.sqrt(sqHermNorm);
    }
}
