/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import javax.annotation.concurrent.Immutable;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.FieldDecompositionSolver;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;

// CHECKSTYLE.OFF: LineLength: disabled for long URL
/** A class capable of computing the {@link ComplexQRDecomposition QR decomposition} of an {@code m x n}
 * {@link ComplexMatrix complex matrix} {@code A}.<br>
 * <br>
 * 
 * For a readable - but less precise - algorithm description, see
 * <a href="https://en.wikipedia.org/wiki/QR_decomposition#Using_Householder_reflections"> The Wikipedia article on the
 * QR decomposition using Householder reflections</a> <br>
 * <br>
 * 
 * For a detailed and much more technical description, see <a href=
 * "https://www.google.com/books/edition/Introduction_to_Numerical_Analysis/1oDXWLb9qEkC?hl=en&gbpv=1&pg=PA225&printsec=frontcover">
 * Stoer and Bulirsch, Introduction to Numerical Analysis (3rd ed.), p. 226 - 227</a> <br>
 * <br>
 * 
 * TODO: include pivoting for improved stability
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see QRDecomposition
 * @see ComplexHessenbergReduction */
// CHECKSTYLE.ON: LineLength
@Immutable
public non-sealed class ComplexQRDecomposition extends QRDecomposition<ComplexMatrix>
                                               implements FieldDecompositionSolver<Complex> {
    
    /** an object on which we can synchronize when we want to lazily initialize the reduced form of this decomposition
     * in {@link #setupReducedFactorization()} */
    protected final Object mutex;
    
    /** Constructor
     * 
     * @param Q {@code Q} from {@code A = QR}
     * @param R {@code R} from {@code A = QR}
     * 
     * @apiNote this constructor is only {@code protected} so that it {@link ComplexQRDecomposition} can be
     *          {@code extended} (though that seems unlikely) and for easier unit testing. */
    protected ComplexQRDecomposition(final ComplexMatrix Q, final ComplexMatrix R) {
        super(Q, R);
        this.mutex = new Object();
    }
    
    /** Decompose {@code A} into a product of two matrices {@code Q} and {@code R}, where {@code Q} is unitary
     * ({@code Q^H = Q^-1}, where {@code ^H} indicates the conjugate transpose) and {@code R} is upper-triangular
     * (a.k.a. right-triangular).
     * 
     * @param A the {@link FieldMatrix} to decompose. Often square, but can be "tall"; i.e., have more rows than
     *        columns. If neither, an exception is thrown.
     * @return the {@link ComplexQRDecomposition} which holds the results of the decomposition */
    public static ComplexQRDecomposition of(final FieldMatrix<Complex> A) {
        return doInPlace(ComplexMatrix.of(A), true, false); // copy here and use doInPlace with the copy
    }
    
    /** Decompose {@code A} into a product of two matrices {@code Q} and {@code R}, where {@code Q} is unitary
     * ({@code Q^H = Q^-1}, where {@code ^H} indicates the conjugate transpose) and {@code R} is upper-triangular
     * (a.k.a. right-triangular).
     * 
     * @param A the {@link ComplexMatrix} to decompose. Often square, but can be "tall"; i.e., have more rows than
     *        columns. If neither, an exception is thrown.
     * @return the {@link ComplexQRDecomposition} which holds the results of the decomposition */
    public static ComplexQRDecomposition of(final ComplexMatrix A) {
        return doInPlace(A.copy(), true, false); // copy here and use doInPlace with the copy
    }
    
    /** Decompose {@code A} into a product of two matrices {@code Q} and {@code R}, where {@code Q} is unitary
     * ({@code Q^H = Q^-1}, where {@code ^H} indicates the conjugate transpose) and {@code R} is upper-triangular
     * (a.k.a. right-triangular).
     * 
     * @param A the {@link ComplexMatrix} to decompose. Often square, but can be "tall"; i.e., have more rows than
     *        columns. If neither, an exception is thrown.
     * @param doCleanup if {@code true}, we set the sub-diagonal elements of {@link #getR()} to exactly zero so that it
     *        is exactly upper-triangular. This option is included in case the "pure" decomposition is desired. If
     *        {@code false}, do nothing.
     * @param doAdjustSingularities if {@code true}, adjust any diagonal elements of {@link #getR()} that are exactly
     *        zero to {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix) a small, non-zero value}. If
     *        {@code false}, do nothing.
     * @return the {@link ComplexQRDecomposition} which holds the results of the decomposition */
    public static ComplexQRDecomposition of(final ComplexMatrix A,
                                            final boolean doCleanup,
                                            final boolean doAdjustSingularities) {
        return doInPlace(A.copy(), doCleanup, doAdjustSingularities); // copy here and use doInPlace with the copy
    }
    
    /** Decompose {@code A} <em>in place</em> into a product of two matrices {@code Q} and {@code R}, where {@code Q} is
     * unitary ({@code Q^H = Q^-1}, where {@code ^H} indicates the conjugate transpose) and {@code R} is
     * upper-triangular (a.k.a. right-triangular).
     * 
     * @param A The {@link ComplexMatrix} to decompose. Often square, but can be "tall"; i.e., have more rows than
     *        columns. If neither, an exception is thrown.<br>
     *        <em>This matrix will be modified in-place!</em> In the result, it will be the same instance as
     *        {@link ComplexQRDecomposition#getR() R}.
     * @param doCleanup if {@code true}, we set the sub-diagonal elements of {@link #getR()} to exactly zero so that it
     *        is exactly upper-triangular. This option is included in case the "pure" decomposition is desired. If
     *        {@code false}, do nothing.
     * @param doAdjustSingularities if {@code true}, adjust any diagonal elements of {@link #getR()} that are exactly
     *        zero to {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix) a small, non-zero value}. If
     *        {@code false}, do nothing.
     * @return the {@link ComplexQRDecomposition} which holds the results of the decomposition */
    protected static ComplexQRDecomposition doInPlace(final ComplexMatrix A,
                                                      final boolean doCleanup,
                                                      final boolean doAdjustSingularities) {
        Validation.requireGreaterThanEqualTo(A.getRowDimension(), A.getColumnDimension(),
                () -> ("Input matrix A is of dimension %s, but the QR decomposition can only be "
                        + "performed on square (m == n) and tall matrices (m > n)").formatted(A.getDimensionString()));
        
        final int numRows = A.getRowDimension();
        final int numCols = A.getColumnDimension();
        final int n       = FastMath.min(numRows - 1, numCols);
        
        final ComplexHouseholder[] householders = new ComplexHouseholder[n];
        final ComplexMatrix R = A; // notice that we don't make a copy!
        for (int i = 0; i < n; i++) { // R is updated in-place in this loop
            final double[][]         ithColumn   = R.getDoubleSubColumn(i, i, numRows - i);
            final ComplexHouseholder householder = ComplexHouseholder.of(ithColumn[0], ithColumn[1]);
            
            householder.preApplyInPlace(R, i, i);  // modifies R data!!
            householders[i] = householder;         // save the Householder for later
        }
        
        final ComplexMatrix Q = ComplexMatrix.identity(numRows);
        for (int i = n - 1; i >= 0; i--) { // Q is updated in-place in this loop
            final ComplexHouseholder householder = householders[i];
            householder.preApplyInPlace(Q, i, i); // modifies Q data!!
        }
        
        if (doCleanup) {
            cleanup(R);
        }
        
        if (doAdjustSingularities) {
            adjustSingularities(R);
        }
        
        return new ComplexQRDecomposition(Q, R);
    }
    
    /** Set the sub-diagonal elements of {@code R} to exactly zero
     * 
     * @param R the {@link ComplexMatrix} to clean up */
    static void cleanup(final ComplexMatrix R) {
        final int numRows = R.getRowDimension();
        final int numCols = R.getColumnDimension();
        
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < FastMath.min(i, numCols); j++) {
                R.unsafeSetEntry(i, j, 0.0, 0.0);
            }
        }
    }
    
    /** Adjust any diagonal elements of {@link #getR()} that are exactly zero to
     * {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix) a small, non-zero, real value}
     * 
     * @param R the {@link ComplexMatrix} whose diagonal entries we might adjust */
    static void adjustSingularities(final ComplexMatrix R) {
        final int numCols = R.getColumnDimension();
        
        double eps3 = -1.0; /* Calculating eps3 requires computing the L-infinity norm of R, which is an O(n^2)
                             * operation that involves sqrt, and it's unlikely that we'll see entries exactly equal to
                             * zero in practice, so we ensure that we only compute it once, and only if we need it. */
        
        final double[][][] RReIm = R.toReImParts();
        final double[][]   RRe   = RReIm[0];
        final double[][]   RIm   = RReIm[1];
        
        for (int i = 0; i < numCols; i++) {
            final double re = RRe[i][i];
            final double im = RIm[i][i];
            if (re == 0.0 && im == 0.0) {
                if (eps3 == -1.0) {
                    eps3 = Numerics.getInftyNormMatrixEpsilon(R);
                }
                R.unsafeSetEntry(i, i, eps3, 0.0);
            }
        }
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * vector {@code x} that satisfies {@code Ax = y}. See {@link #solve(ComplexMatrix, LinearSolver)} for details.
     * 
     * @param y the right-hand-side vector
     * @return the solution vector {@code y} */
    public ComplexVector solve(final ComplexVector y) {
        return solve(y, LinearSolver.defaultSolver());
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * vector {@code x} that satisfies {@code Ax = y}. See {@link #solve(ComplexMatrix, LinearSolver)} for details.
     * 
     * @param y the right-hand-side vector
     * @param linSlvr the {@link LinearSolver} we want to use to do back-substitution
     * @return the solution vector {@code y} */
    public ComplexVector solve(final ComplexVector y, final LinearSolver linSlvr) {
        final ComplexMatrix Q1 = this.getReducedQ(); // if the original matrix was square, this will be getQ
        final ComplexMatrix R1 = this.getReducedR(); // the same, but getR                                             
        
        // solve for x in R1.x = Q1^T.y
        return linSlvr.solveSquareBackSubstitution(R1, Q1.conjugateTransposeOperate(y));
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * vector {@code X} that satisfies {@code AX = Y}. See {@link #solve(ComplexMatrix, LinearSolver)} for details.
     * 
     * @param Y the right-hand-side matrix
     * @return the solution matrix {@code X} */
    public ComplexMatrix solve(final ComplexMatrix Y) {
        return solve(Y, LinearSolver.defaultSolver());
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * vector {@code X} that satisfies {@code AX = Y}. Note: the process below is described using a matrix RHS, but if
     * you consider a vector RHS as a matrix with a single column, the derivation is the same.
     * 
     * <pre>
     * <code>If A = QR,
     * 
     *  AX = Y
     * QRX = Y
     *  RX = Q<sup>-1</sup>Y
     *  RX = Q<sup>H</sup>Y </code>
     * </pre>
     * 
     * and this can be solved using {@link LinearSolver#solveSquareBackSubstitution(ComplexMatrix, ComplexMatrix)
     * back-substitution}.<br>
     * <br>
     * 
     * We use the reduced forms of {@code Q} and {@code R} which, in the square case, are the same as the complete
     * forms. See the class-level JavaDoc of {@link QRDecomposition} for details.
     * 
     * @param Y the right-hand-side matrix
     * @param linSlvr the {@link LinearSolver} we want to use to do back-substitution
     * @return the solution matrix {@code X} */
    public ComplexMatrix solve(final ComplexMatrix Y, final LinearSolver linSlvr) {
        final ComplexMatrix Q1 = this.getReducedQ(); // if the original matrix was square, this will be getQ
        final ComplexMatrix R1 = this.getReducedR(); // the same, but getR
        
        // solve for X in R1.X = Q1^T.Y
        return linSlvr.solveSquareBackSubstitution(R1, Q1.conjugateTransposeMultiply(Y));
    }
    
    @Override // from (abstract) QRDecomposition
    protected void setupReducedFactorization() {
        if (this.reducedQ == null) {
            synchronized (this.mutex) {
                if (this.reducedQ == null) {
                    final int m = this.Q.getRowDimension();
                    final int n = this.R.getColumnDimension();
                    
                    this.reducedQ = this.Q.getSubMatrix(0, m - 1, 0, n - 1);
                    this.reducedR = this.R.getSubMatrix(0, n - 1, 0, n - 1);
                }
            }
        }
    }
    
    // ======== FieldDecompositionSolver implementations ========
    
    @Override
    public ComplexVector solve(final FieldVector<Complex> y) {
        return this.solve(ComplexVector.of(y));
    }
    
    @Override
    public ComplexMatrix solve(final FieldMatrix<Complex> Y) {
        return this.solve(ComplexMatrix.of(Y));
    }
    
    /** {@inheritDoc}
     * 
     * @return {@code true} iff a main-diagonal element of {@link #getR()} is exactly zero */
    @Override
    public boolean isNonSingular() {
        for (int i = 0; i < this.getR().getMaxRank(); i++) {
            final double entryRe = this.getR().getEntryRe(i, i);
            final double entryIm = this.getR().getEntryIm(i, i);
            if (entryRe == 0.0 && entryIm == 0.0) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public ComplexMatrix getInverse() {
        Numerics.requireSquare(this.getR());
        return this.solve(ComplexMatrix.identity(this.getColumnDimension()));
    }
    
    @Override
    public int getRowDimension() {
        return this.Q.getRowDimension();
    }
    
    @Override
    public int getColumnDimension() {
        return this.R.getColumnDimension();
    }
}
