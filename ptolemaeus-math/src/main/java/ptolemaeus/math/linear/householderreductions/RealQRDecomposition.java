/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

// CHECKSTYLE.OFF: LineLength: disabled for long URL
/** A class capable of computing the {@link RealQRDecomposition QR decomposition} of an {@code m x n} {@link Matrix
 * matrix} {@code A}.<br>
 * <br>
 * 
 * For a readable - but less precise - algorithm description, see
 * <a href="https://en.wikipedia.org/wiki/QR_decomposition#Using_Householder_reflections"> The Wikipedia article on the
 * QR decomposition using Householder reflections</a> <br>
 * <br>
 * 
 * For a detailed and much more technical description, see <a href=
 * "https://www.google.com/books/edition/Introduction_to_Numerical_Analysis/1oDXWLb9qEkC?hl=en&gbpv=1&pg=PA225&printsec=frontcover">
 * Stoer and Bulirsch, Introduction to Numerical Analysis (3rd ed.), p. 226 - 227</a> <br>
 * <br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see QRDecomposition */
// CHECKSTYLE.ON: LineLength
public non-sealed class RealQRDecomposition extends QRDecomposition<Matrix> implements DecompositionSolver {
    
    /** an object on which we can synchronize when we want to lazily initialize the reduced form of this decomposition
     * in {@link #setupReducedFactorization()} */
    protected final Object mutex;
    
    /** Constructor
     * 
     * @param Q {@code Q} from {@code A = QR}
     * @param R {@code R} from {@code A = QR}
     * 
     * @apiNote this constructor is only {@code protected} so that {@link RealQRDecomposition} can be {@code extended}
     *          (though that seems unlikely) and for easier unit testing. */
    protected RealQRDecomposition(final Matrix Q, final Matrix R) {
        super(Q, R);
        this.mutex = new Object();
    }
    
    /** Decompose {@code A} into a product of two matrices {@code Q} and {@code R}, where {@code Q} is
     * orthogonal ({@code Q^T = Q^-1}), and where {@code R} is upper-triangular (a.k.a. right-triangular).
     * 
     * @param A the {@link RealMatrix} to decompose. Often square, but can be "tall"; i.e., have more rows than
     *        columns.  If neither, an exception is thrown.
     * @return the {@link RealQRDecomposition} which holds the results of the decomposition */
    public static RealQRDecomposition of(final RealMatrix A) {
        return doInPlace(Matrix.of(A), true, false);
    }
    
    /** Decompose {@code A} into a product of two matrices {@code Q} and {@code R}, where {@code Q} is
     * orthogonal ({@code Q^T = Q^-1}), and where {@code R} is upper-triangular (a.k.a. right-triangular).
     * 
     * @param A the {@link Matrix} to decompose. Often square, but can be "tall"; i.e., have more rows than
     *        columns.  If neither, an exception is thrown.
     * @return the {@link RealQRDecomposition} which holds the results of the decomposition */
    public static RealQRDecomposition of(final Matrix A) {
        return doInPlace(A.copy(), true, false);
    }
    
    /** Decompose {@code A} into a product of two matrices {@code Q} and {@code R}, where {@code Q} is orthogonal
     * ({@code Q^T = Q^-1}), and where {@code R} is upper-triangular (a.k.a. right-triangular).
     * 
     * @param A the {@link Matrix} to decompose. Often square, but can be "tall"; i.e., have more rows than columns. If
     *        neither, an exception is thrown.
     * @param doCleanup if {@code true}, we set the sub-diagonal elements of {@link #getR()} to exactly zero so that it
     *        is exactly upper-triangular. This option is included in case the "pure" decomposition is desired. If
     *        {@code false}, do nothing.
     * @param doAdjustSingularities if {@code true}, adjust any diagonal elements of {@link #getR()} that are exactly
     *        zero to {@link Numerics#getInftyNormMatrixEpsilon(RealMatrix) a small, non-zero value}. If
     *        {@code false}, do nothing.
     * @return the {@link RealQRDecomposition} which holds the results of the decomposition */
    public static RealQRDecomposition of(final Matrix A,
                                         final boolean doCleanup,
                                         final boolean doAdjustSingularities) {
        return doInPlace(A.copy(), doCleanup, doAdjustSingularities);
    }
    
    /** Decompose {@code A} <em>in place</em> into a product of two matrices {@code Q} and {@code R}, where {@code Q} is
     * orthogonal ({@code Q^T = Q^-1}), and where {@code R} is upper-triangular (a.k.a. right-triangular).
     * 
     * @param A The {@link Matrix} to decompose. Often square, but can be "tall"; i.e., have more rows than columns. If
     *        neither, an exception is thrown.<br>
     *        <em>This matrix will be modified in-place!</em> In the result, it will be the same instance as
     *        {@link RealQRDecomposition#getR() R}.
     * @param doCleanup if {@code true}, we set the sub-diagonal elements of {@link #getR()} to exactly zero so that it
     *        is exactly upper-triangular. This option is included in case the "pure" decomposition is desired. If
     *        {@code false}, do nothing.
     * @param doAdjustSingularities if {@code true}, adjust any diagonal elements of {@link #getR()} that are exactly
     *        zero to {@link Numerics#getInftyNormMatrixEpsilon(RealMatrix) a small, non-zero value}. If
     *        {@code false}, do nothing.
     * @return the {@link RealQRDecomposition} which holds the results of the decomposition */
    protected static RealQRDecomposition doInPlace(final Matrix A,
                                                   final boolean doCleanup,
                                                   final boolean doAdjustSingularities) {
        Validation.requireGreaterThanEqualTo(A.getRowDimension(), A.getColumnDimension(),
                () -> ("Input matrix A is of dimension (%d x %d), but the QR decomposition can only be "
                        + "performed on square (m == n) and tall matrices (m > n)").formatted(A.getRowDimension(),
                                                                                              A.getColumnDimension()));
        
        final int numRows = A.getRowDimension();
        final int numCols = A.getColumnDimension();
        final int n       = FastMath.min(numRows - 1, numCols);
        
        final RealHouseholder[] householders = new RealHouseholder[n];
        final Matrix R = A; // notice that we don't make a copy!
        for (int i = 0; i < n; i++) { // R is updated in-place in this loop
            final double[]        ithColumn   = R.getColumnVector(i).getSubVector(i, numRows - i).toArray();
            final RealHouseholder householder = RealHouseholder.of(ithColumn);
            
            householder.preApplyInPlace(R, i, i);  // modifies R data!!
            householders[i] = householder;
        }
        
        final Matrix Q = Matrix.identity(numRows);
        for (int i = n - 1; i >= 0; i--) { // Q is updated in-place in this loop
            final RealHouseholder householder = householders[i];
            householder.preApplyInPlace(Q, i, i); // modifies Q data!!
        }
        
        if (doCleanup) {
            cleanup(R);
        }
        
        if (doAdjustSingularities) {
            adjustSingularities(R);
        }
        
        return new RealQRDecomposition(Q, R);
    }
    
    /** Set the sub-diagonal elements of {@code R} to exactly zero
     * 
     * @param R the {@link Matrix} to clean up */
    static void cleanup(final Matrix R) {
        final int numRows = R.getRowDimension();
        final int numCols = R.getColumnDimension();
        
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < FastMath.min(i, numCols); j++) {
                R.setEntry(i, j, 0.0);
            }
        }
    }
    
    /** Adjust any diagonal elements of {@link #getR()} that are exactly zero to
     * {@link Numerics#getInftyNormMatrixEpsilon(RealMatrix) a small, non-zero value}
     * 
     * @param R the {@link Matrix} whose diagonal entries we might adjust */
    static void adjustSingularities(final Matrix R) {
        final int numCols = R.getColumnDimension();
        
        double eps3 = -1.0; /* Calculating eps3 requires computing the L-infinity norm of R, which is an O(n^2), and
                             * it's unlikely that we'll see entries exactly equal to zero in practice, so we ensure that
                             * we only compute it once, and only if we need it. */
        
        for (int i = 0; i < numCols; i++) {
            if (R.getEntry(i, i) == 0.0) {
                if (eps3 == -1.0) {
                    eps3 = Numerics.getInftyNormMatrixEpsilon(R);
                }
                R.setEntry(i, i, eps3);
            }
        }
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * vector {@code x} that satisfies {@code AX = Y}<br>
     * <br>
     * 
     * See {@link ComplexQRDecomposition#solve(ComplexMatrix, LinearSolver)} for details. Keep in mind that in the real
     * case, the conjugate transpose is just the transpose.
     * 
     * @param y the right-hand-side vector of the equation. Must have dimension equal to the number of rows in the
     *        original {@code A}.
     * @return the solution vector {@code x}. Will have dimension equal to the number of columns in the original
     *         {@code A}. */
    public NVector solve(final NVector y) {
        return solve(y, LinearSolver.defaultSolver());
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * vector {@code x} that satisfies {@code Ax = y}<br>
     * <br>
     * 
     * See {@link ComplexQRDecomposition#solve(ComplexMatrix, LinearSolver)} for details. Keep in mind that in the real
     * case, the conjugate transpose is just the transpose.
     * 
     * @param y the right-hand-side vector of the equation. Must have dimension equal to the number of rows in the
     *        original {@code A}.
     * @param linSlvr the {@link LinearSolver} to use to solve for {@code x} in {@code Rx = Q}<sup>T</sup>{@code y}
     * @return the solution vector {@code x}. Will have dimension equal to the number of columns in the original
     *         {@code A}. */
    public NVector solve(final NVector y, final LinearSolver linSlvr) {
        final Matrix Q1 = this.getReducedQ(); // if the original matrix was square, this will be getQ
        final Matrix R1 = this.getReducedR(); // the same, but getR
        
        // solve for x in R1.x = Q1^T.y
        return linSlvr.solveSquareBackSubstitution(R1, Q1.operateTranspose(y));
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * matrix {@code X} that satisfies {@code AX = Y}<br>
     * <br>
     * 
     * See {@link ComplexQRDecomposition#solve(ComplexMatrix, LinearSolver)} for details. Keep in mind that in the real
     * case, the conjugate transpose is just the transpose.
     * 
     * @param Y the right-hand-side matrix of the equation. Must have the same number of rows as the original {@code A}.
     * @return the solution matrix {@code X}. Will have as many rows as the original {@code A}, and as many columns as
     *         {@code Y}. */
    public Matrix solve(final Matrix Y) {
        return solve(Y, LinearSolver.defaultSolver());
    }
    
    /** Given {@code this} {@link QRDecomposition} for some original matrix {@code A} s.t. {@code A = QR}, solve for the
     * matrix {@code X} that satisfies {@code AX = Y}<br>
     * <br>
     * 
     * See {@link ComplexQRDecomposition#solve(ComplexMatrix, LinearSolver)} for details. Keep in mind that in the real
     * case, the conjugate transpose is just the transpose.
     * 
     * @param Y the right-hand-side matrix
     * @param linSlvr the {@link LinearSolver} we want to use to do back-substitution
     * @return the solution matrix {@code X} */
    public Matrix solve(final Matrix Y, final LinearSolver linSlvr) {
        final Matrix Q1 = this.getReducedQ(); // if the original matrix was square, this will be getQ
        final Matrix R1 = this.getReducedR(); // the same, but getR
        
        // solve for X in R1.X = Q1^T.Y
        return linSlvr.solveSquareBackSubstitution(R1, Q1.transposeMultiply(Y));
    }
    
    @Override // from (abstract) QRDecomposition
    protected void setupReducedFactorization() {
        if (this.reducedR == null) {
            synchronized (this.mutex) {
                if (this.reducedR == null) {
                    final int m = this.Q.getRowDimension();
                    final int n = this.R.getColumnDimension();
                    
                    this.reducedQ = this.Q.getSubMatrix(0, m - 1, 0, n - 1);
                    this.reducedR = this.R.getSubMatrix(0, n - 1, 0, n - 1);
                }
            }
        }
    }
    
    // ======== DecompositionSolver implementations ========
    
    @Override
    public NVector solve(final RealVector y) {
        return this.solve(NVector.of(y));
    }
    
    @Override
    public Matrix solve(final RealMatrix Y) {
        return this.solve(Matrix.of(Y));
    }
    
    /** {@inheritDoc}
     * 
     * @return {@code true} iff a main-diagonal element of {@link #getR()} is exactly zero */
    @Override
    public boolean isNonSingular() {
        for (int i = 0; i < this.getR().getMaxRank(); i++) {
            if (this.getR().getEntry(i, i) == 0.0) {
                return false;
            }
        }
        
        return true;
    }
    
    @Override
    public Matrix getInverse() {
        Numerics.requireSquare(this.getR());
        return this.solve(Matrix.identity(this.getColumnDimension()));
    }
    
    @Override
    public int getRowDimension() {
        return this.Q.getRowDimension();
    }
    
    @Override
    public int getColumnDimension() {
        return this.R.getColumnDimension();
    }
}
