/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import java.util.function.Consumer;

import javax.annotation.concurrent.Immutable;

import org.hipparchus.complex.Complex;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** A {@link ComplexHessenbergReduction} is a reduction of a {@link ComplexMatrix matrix A} into the product
 * {@code A = QRQ}<sup>H</sup> where {@link ComplexMatrix Q} is unitary and {@code R} is an {@code n}-Hessenberg matrix;
 * i.e., a matrix for which all entries below the the {@code n}th sub diagonals are zero <br>
 * <br>
 * 
 * {@link #getR() R} is typically denoted {@code H} but, to avoid confusion with the conjugate transpose notation in
 * monospaced fonts, I'm sticking with {@code R} ({@code R} due to the similarity of this to the
 * {@link ComplexQRDecomposition}).<br>
 * <br>
 * 
 * {@code A = QRQ}<sup>H</sup> is a similarity transformation on {@code A}, meaning that {@code A} and {@code R} have
 * the same Eigenvalues; i.e., &Lambda;{@code (A) == }&Lambda;{@code (R)}<br>
 * <br>
 * 
 * To compute the {@link ComplexHessenbergReduction} of a {@link ComplexMatrix} {@code A}, we proceed in much the same
 * way as in a {@link ComplexQRDecomposition}, but for each iteration {@code i}, instead of computing a
 * {@link ComplexHouseholder} matrix that "zeroes out" everything in the {@code i}-th column under the diagonal, we use
 * {@link ComplexHouseholder} matrices to zero-out all entries in each column under the {@code n}th
 * <em>sub-diagonal</em>.<br>
 * <br>
 * 
 * In addition, the multiplication in each iteration is different: we compute {@code A_i+1 = Q_i A_i Q_i}<sup>H</sup>,
 * where the {@code Q_i} matrices are the Household rotation matrices for the ith column of {@code A_i}. The final value
 * of {@code A_i} is what we save to {@code R}.<br>
 * <br>
 * 
 * For reference, see [1] <em>Numerical Methods for General and Structured Eigenvalue Problems</em> by Kressner
 * (2006)<br>
 * <br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
@Immutable
public class ComplexHessenbergReduction {
    
    /** an object on which we can synchronize when computing Q in {@link #getQ()} */
    protected final Object mutex;
    
    /** The Hessenberg {@link ComplexMatrix} {@code R} from {@code A = QRQ}<sup>H</sup> */
    private final ComplexMatrix R;
    
    /** the intermediate {@link ComplexHouseholder} instances that were used to reduce the original matrix to
     * n-Hessenberg form. These are cached and used to compute {@link #getQ()} as needed. Once computed, this variable
     * is set to {@code null} to encourage garbage collection in case {@code this} is meant to be long-lived. */
    private volatile ComplexHouseholder[] householders;
    
    /** The unitary {@link ComplexMatrix} {@code Q} from {@code A = QRQ}<sup>H</sup> */
    private volatile ComplexMatrix Q;
    
    /** Constructor
     * 
     * @param householders the intermediate {@link ComplexHouseholder} instances that were used to reduce the original
     *        matrix to n-Hessenberg form
     * @param R {@code R} from {@code A = QRQ}<sup>H</sup>
     *        
     * @apiNote this constructor is only {@code protected} so that {@link ComplexHessenbergReduction} can be
     *          {@code extended} (though that seems unlikely) and for easier unit testing. */
    protected ComplexHessenbergReduction(final ComplexHouseholder[] householders, final ComplexMatrix R) {
        this.mutex        = new Object();
        this.R            = R;
        this.householders = householders;
        this.Q            = null;
    }
    
    /** Constructor
     * 
     * @param R {@code R} from {@code A = QRQ}<sup>H</sup>
     * @param Q {@code Q} from {@code A = QRQ}<sup>H</sup>
     *        
     * @apiNote this constructor is only {@code protected} so that {@link ComplexHessenbergReduction} can be
     *          {@code extended} (though that seems unlikely) and for easier unit testing. */
    protected ComplexHessenbergReduction(final ComplexMatrix Q, final ComplexMatrix R) {
        this.mutex        = new Object();
        this.R            = R;
        this.householders = null;
        this.Q            = Q;
    }
    
    /** Compute a 1-{@link ComplexHessenbergReduction} of {@code A} s.t. {@code A = QRQ}<sup>H</sup> where {@code R} is
     * 1-Hessenberg (all sub-diagonal entries beyond the first sub-diagonal are zero).<br>
     * <br>
     * 
     * E.g.: <br>
     * 
     * <pre>
     * a a a a a a
     * a a a a a a
     * 0 a a a a a
     * 0 0 a a a a
     * 0 0 0 a a a
     * 0 0 0 0 a a
     * </pre>
     * 
     * where {@code a} simply indicates a non-zero value<br>
     * <br>
     * 
     * From [1] <em>Algorithm 1: Reduction to Hessenberg form</em> (generalized w.l.o.g. to A in C<sup>n x n</sup>):<br>
     * <br>
     * 
     * Input: A in C<sup>n x n</sup><br>
     * Output: Matrices Q and R in C<sup>n x n</sup> where Q is Hermitian, and R is a Hessenberg matrix similar to A<br>
     * <br>
     * 
     * Q &lt;- I<sub>n</sub><br>
     * R &lt;- A<br>
     * for j &lt;- 1, ..., n - 2, do<br>
     * &emsp; H &lt;- H<sub>j+1</sub>(A . e<sub>j</sub>)<br>
     * &emsp; Q &lt;- Q . H<br>
     * &emsp; R &lt;- H . R . H (we can neglect the conjugate transpose because H is Hermitian) <br>
     * end for <br>
     * <br>
     * 
     * Where A . e<sub>j</sub> extracts the j<sup>th</sup> column of A, and H<sub>j+1</sub> is a function that computes
     * the Householder transformation to "zero-out" the rows of A . e<sub>j</sub> after row j+1
     * 
     * <br>
     * <br>
     * 
     * @param A the matrix to reduce
     * @return the {@link ComplexHessenbergReduction} */
    public static ComplexHessenbergReduction of(final ComplexMatrix A) {
        final boolean isHermitian = A.isHermitian(Complex.ZERO);
        return doInPlace(null, A.copy(), isHermitian, 1, null);
    }
    
    /** Compute an {@code n}-{@link ComplexHessenbergReduction} of {@code A} s.t. {@code A = QRQ}<sup>H</sup> where
     * {@code R} is {@code n}-Hessenberg (all sub-diagonal entries beyond the first {@code n}-sub-diagonal are
     * zero).<br>
     * <br>
     * 
     * E.g., a 2-Hessenberg matrix: <br>
     * 
     * <pre>
     * a a a a a a
     * a a a a a a
     * a a a a a a
     * 0 a a a a a
     * 0 0 a a a a
     * 0 0 0 a a a
     * </pre>
     * 
     * where {@code a} simply indicates a non-zero value
     * 
     * @param A the matrix to reduce. Must be square (unlike in {@link ComplexQRDecomposition}).
     * @param hessenbergOffset the number of sub-diagonals that we want to preserve
     * @return the {@code n}-{@link ComplexHessenbergReduction} */
    public static ComplexHessenbergReduction of(final ComplexMatrix A, final int hessenbergOffset) {
        final boolean isHermitian = A.isHermitian(Complex.ZERO);
        return doInPlace(null, A.copy(), isHermitian, hessenbergOffset, null);
    }
    
    /** Compute an {@code n}-{@link ComplexHessenbergReduction} of {@code A} s.t. {@code A = QRQ}<sup>H</sup> where
     * {@code R} is {@code n}-Hessenberg (all sub-diagonal entries beyond the first {@code n}-sub-diagonal are
     * zero).<br>
     * <br>
     * 
     * E.g., a 2-Hessenberg matrix: <br>
     * 
     * <pre>
     * a a a a a a
     * a a a a a a
     * a a a a a a
     * 0 a a a a a
     * 0 0 a a a a
     * 0 0 0 a a a
     * </pre>
     * 
     * where {@code a} simply indicates a non-zero value
     * 
     * @param A the matrix to reduce. Must be square (unlike in {@link ComplexQRDecomposition}).
     * @param hessenbergOffset the number of sub-diagonals that we want to preserve
     * @param evaluationMonitor a {@link Consumer} the monitors the reduction. It is updated with value of
     *        {@link #getR() R} on each iteration.
     * @return the {@code n}-{@link ComplexHessenbergReduction} */
    public static ComplexHessenbergReduction of(final ComplexMatrix A,
                                                final int hessenbergOffset,
                                                final Consumer<ComplexMatrix> evaluationMonitor) {
        final boolean isHermitian = A.isHermitian(Complex.ZERO);
        return doInPlace(null, A.copy(), isHermitian, hessenbergOffset, evaluationMonitor);
    }
    
    /** Reduce {@code A} to {@code n}-{@link ComplexHessenbergReduction} form s.t. the original
     * {@code A = QRQ}<sup>H</sup> where {@code R} is {@code n}-Hessenberg (all sub-diagonal entries beyond the first
     * {@code n}-sub-diagonal are zero).<br>
     * <br>
     * 
     * E.g., a 2-Hessenberg matrix: <br>
     * 
     * <pre>
     * a a a a a a
     * a a a a a a
     * a a a a a a
     * 0 a a a a a
     * 0 0 a a a a
     * 0 0 0 a a a
     * </pre>
     * 
     * where {@code a} simply indicates a non-zero value
     * 
     * @param Q the {@link ComplexMatrix} in which we will accumulate {@link #getQ()} <b>This Matrix is not copied and
     *        will be modified!</b> See the {@code implNote} below. This option is useful in performance sensitive
     *        contexts where - e.g. - we don't want to copy the input matrices over and over again.
     * @param R the matrix to reduce. Must be square (unlike in {@link ComplexQRDecomposition}). <b>This Matrix is not
     *        copied and will be modified!</b> This option is useful in performance sensitive contexts where - e.g. - we
     *        don't want to copy the input matrices over and over again.
     * @param isHermitian {@code true} if {@code R} is know to be {@link ComplexMatrix#isHermitian Hermitian},
     *        {@code false} otherwise. <em>Using {@code true} when {@code R} is not Hermitian will result in incorrect
     *        results!!</em> Using {@code false} when {@code R} <em>is</em> Hermitian will potentially degrade quality,
     *        but not significantly.
     * @param hessenbergOffset the number of sub-diagonals that we want to preserve
     * @param evaluationMonitor a {@link Consumer} the monitors the reduction. It is updated with value of
     *        {@link #getR() R} on each iteration.
     * @return the {@code n}-{@link ComplexHessenbergReduction} */
    @SuppressWarnings("null") // using updateQInPlace prevents any NPEs, but Eclipse doesn't understand that
    public static ComplexHessenbergReduction doInPlace(final ComplexMatrix Q,
                                                       final ComplexMatrix R,
                                                       final boolean isHermitian,
                                                       final int hessenbergOffset,
                                                       final Consumer<ComplexMatrix> evaluationMonitor) {
        Numerics.requireSquare(R);
        
        final boolean updateQInPlace = Q != null;
        if (updateQInPlace) {
            Numerics.requireSquare(Q);
            Validation.requireEqualsTo(R.getColumnDimension(), Q.getColumnDimension(), "R and Q dimensions");
        }
        
        final int dim           = R.getColumnDimension();
        final int numEltsOffset = dim - hessenbergOffset;
        final int numIters      = numEltsOffset - 1; // subtract 1 because once we reach the bottom of the matrix
        
        final ComplexHouseholder[] householders = new ComplexHouseholder[numIters];
        
        for (int i = 0; i < numIters; i++) {
            final double[][] colReIm = R.getDoubleSubColumn(i + hessenbergOffset, // starting row index
                                                            i,                    // column index
                                                            numEltsOffset - i);   // num elements to take
            
            final ComplexHouseholder householder = ComplexHouseholder.of(colReIm[0], colReIm[1]);
            
            final int offset   = i + hessenbergOffset;
            final int rowStart = isHermitian ? i : 0;
            householder.preApplyInPlace(R, offset, i);
            householder.postApplyInPlace(R, rowStart, offset);
            
            householders[i] = householder;
            
            if (evaluationMonitor != null) {
                evaluationMonitor.accept(R);
            }
        }
        
        if (updateQInPlace) {
            for (int i = 0; i < numIters; i++) {
                final ComplexHouseholder householder = householders[i];
                final int                offset      = i + hessenbergOffset;
                householder.postApplyInPlace(Q, 0, offset);
            }
            
            return new ComplexHessenbergReduction(Q, R);
        }
        else {
            return new ComplexHessenbergReduction(householders, R);
        }
    }
    
    /** @return {@code Q} from the decomposition {@code A = QRQ}<sup>H</sup>. {@code Q} is unitary s.t.
     *         {@code Q}<sup>-1</sup> == {@code Q}<sup>H</sup> or, equivalently, {@code Q}{@code Q}<sup>H</sup> ==
     *         {@code Q}<sup>H</sup>{@code Q} == {@code I}. Implicitly, {@code Q} is square. */
    @SuppressFBWarnings({ "EI" })
    public ComplexMatrix getQ() {
        if (this.Q == null) {
            synchronized (this.mutex) {
                if (this.Q == null) {
                    final int dim              = R.getColumnDimension();
                    final int numIters         = this.householders.length;
                    final int hessenbergOffset = dim - numIters - 1;
                    
                    final ComplexMatrix tempQ = ComplexMatrix.identity(dim);
                    for (int i = 0; i < numIters; i++) {
                        final ComplexHouseholder householder = this.householders[i];
                        final int                offset      = i + hessenbergOffset;
                        householder.postApplyInPlace(tempQ, 0, offset);
                    }
                    
                    this.Q            = tempQ;
                    this.householders = null;
                }
            }
        }
        return this.Q;
    }
    
    /** @return {@code R} from the decomposition {@code A = QRQ}<sup>H</sup>. {@code R} is {@code n}-Hessenberg, meaning
     *         all sub-diagonal entries beyond the first {@code n}-sub-diagonal are zero. */
    @SuppressFBWarnings({ "EI" })
    public ComplexMatrix getR() {
        return this.R;
    }
    
    @Override
    public String toString() {
        return "Q:%n%s%n%nR:%n%s%n%nQ^H:%n%s".formatted(this.getQ(), this.getR(), this.getQ().conjugateTranspose());
    }
}
