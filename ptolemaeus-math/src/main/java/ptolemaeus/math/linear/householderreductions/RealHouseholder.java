/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.MathArrays;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

/** A class to compute and hold {@link Householder} transformations of {@link NVector}s for {@link Matrix matrices}. See
 * {@link Householder} for details and theory.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see Householder
 * @see RealQRDecomposition */
public final class RealHouseholder implements Householder<Matrix> {
    
    /** The dimension of the Householder transformation */
    private final int dim;
    
    /** {@code true} if this Householder transformation is the identity, {@code false} otherwise */
    private final boolean isNoop;
    
    /** the Householder vector; i.e., {@code w} from <code>I - 2[w, w<sup>H</sup>]</code> */
    private final double[] w;
    
    /** Standard constructor
     * 
     * @param wArr the Householder vector */
    private RealHouseholder(final double[] wArr) {
        this.dim    = wArr.length;
        this.isNoop = false;
        this.w      = wArr;
    }
    
    /** The constructor for NOOP instances 
     * @param dim the dimension of the Householder transformation for which this noop instance was created */
    private RealHouseholder(final int dim) {
        this.dim    = dim;
        this.isNoop = true;
        this.w      = null;
    }
    
    @Override
    public boolean isNoop() {
        return this.isNoop;
    }
    
    /** Compute the {@link RealHouseholder} for the given vector. This method assumes {@code hessenbergOffset == 0}. See
     * the {@code interface}-level JavaDoc of {@link Householder} for details.
     * 
     * @param x the the vector
     * @return the {@link RealHouseholder} to transform the given vector into a multiple of the first unit-vector */
    public static RealHouseholder of(final double[] x) {
        final int n = x.length;
        
        final double xNorm = FastMath.sqrt(Numerics.sumSq(x));
        if (xNorm == 0.0) {
            return new RealHouseholder(n);
        }
        
        final double x0 = x[0];
        final double k  = x0 == 0 ? -xNorm : -FastMath.signum(x0) * xNorm;
        
        final double[] u = x.clone();
        u[0] -= k;
        
        final double uNorm = MathArrays.safeNorm(u);
        if (uNorm == 0.0) { /* handle the case that kei == x, as described in the reference */
            return new RealHouseholder(n);
        }
        
        for (int i = 0; i < n; i++) { /* In the reference (see Householder interface),
                                       * there is a new variable w := normalized(u) */
            u[i] /= uNorm;
        }
        
        return new RealHouseholder(u);
    }
    
    @Override
    public void preApplyInPlace(final Matrix A, final int rowStart, final int colStart) {
        if (!this.isNoop()) {
            final int n = A.getColumnDimension();
            
            final double[] wAArr = preMultiplySubMatrix(this.w, A.getDataRef(), rowStart, colStart, n - 1);
            
            /* we need the outer product of w and wA: w.outerProduct(wA)
             * but we can compute the elements of the outer product as-needed: */
            for (int i = 0; i < this.dim; i++) {
                // get the i^th element of w
                final double wi = this.w[i];
                
                for (int j = 0; j < n - colStart; j++) {
                    // get the j^th element of wA
                    final double wAj = wAArr[j];
                    
                    // and compute the ij^th element of the outer product:
                    final double wwAij = wi * wAj;
                    A.subtractFromEntry(i + rowStart, j + colStart, 2 * wwAij);
                }
            }
        }
    }
    
    /** Compute the result of pre-multiplying a sub-matrix (sub-{@code double[][]}) of {@code A} by the given row-vector
     * (as a {@code double[]}) without constructing the sub-matrix itself.<br>
     * <br>
     * 
     * Note that the upper-bound of the row indices is implicit; i.e., it's determined by the length of the
     * pre-multiplying vector.
     * 
     * @param w the vector by which we want to pre-multiply the sub-matrix
     * @param A the matrix whose sub-matrix we want to pre-multiply
     * @param rowStart the starting row of the sub-matrix (inclusive)
     * @param colStart the starting column of the sub-matrix (inclusive)
     * @param colEnd the ending column of the sub-matrix (inclusive)
     * @return the result of the pre-multiplication */
    static double[] preMultiplySubMatrix(final double[] w,
                                         final double[][] A,
                                         final int rowStart,
                                         final int colStart,
                                         final int colEnd) {
        final int numColsSubMatrix = colEnd - colStart + 1;
        final int wDim             = w.length;
        
        final double[] wA = new double[numColsSubMatrix];
        for (int i = rowStart; i < rowStart + wDim; i++) {
            final double   wi = w[i - rowStart];
            final double[] Ai = A[i];
            for (int j = colStart; j <= colEnd; j++) {
                wA[j - colStart] += wi * Ai[j];
            }
        }
        
        return wA;
    }
    
    @Override
    public void postApplyInPlace(final Matrix A, final int rowStart, final int colStart) {
        if (!this.isNoop()) {
            final int m = A.getRowDimension();
            
            final double[] AwArr = postMultiplySubMatrix(A.getDataRef(), this.w, rowStart, m - 1, colStart);
            
            /* We need the outer product of Aw and w^H: Aw.outerProduct(wH)
             * but we can compute the elements of the outer product as-needed: */
            for (int i = 0; i < m - rowStart; i++) {
                // get the i^th element of Aw
                final double Awi = AwArr[i];
                
                for (int j = 0; j < this.dim; j++) {
                    // get the j^th element of w
                    final double wj = this.w[j];
                    
                    // and compute the ij^th element of the outer product:
                    final double Awwij = Awi * wj;
                    A.subtractFromEntry(i + rowStart, j + colStart, 2 * Awwij);
                }
            }
        }
    }
    
    /** Compute the result of post-multiplying a sub-matrix (sub-{@code double[][]}) of {@code A} by the given
     * column-vector (as a {@code double[]}) without constructing the sub-matrix itself.<br>
     * <br>
     * 
     * Note that the upper-bound of the column indices is implicit; i.e., it's determined by the length of the
     * post-multiplying vector.
     * 
     * @param A the matrix whose sub-matrix we want to post-multiply
     * @param w the vector by which we want to post-multiply the sub-matrix
     * @param rowStart the starting row of the sub-matrix (inclusive)
     * @param rowEnd the ending row of the sub-matrix (inclusive)
     * @param colStart the starting column of the sub-matrix (inclusive)
     * @return the result of the post-multiplication */
    static double[] postMultiplySubMatrix(final double[][] A,
                                          final double[] w,
                                          final int rowStart,
                                          final int rowEnd,
                                          final int colStart) {
        final int numRowsSubMatrix = rowEnd - rowStart + 1;
        final int wDim             = w.length;
        
        final double[] Aw = new double[numRowsSubMatrix];
        for (int i = rowStart; i <= rowEnd; i++) {
            final double[] Ai = A[i];
            
            double Awi = 0.0;
            for (int j = colStart; j < colStart + wDim; j++) {
                Awi += w[j - colStart] * Ai[j];
            }
            
            Aw[i - rowStart] = Awi;
        }
        
        return Aw;
    }
    
    @Override
    public Matrix getTransform() {
        if (this.isNoop()) {
            return Matrix.identity(this.dim);
        }
        
        return iMinusTwiceSelfOuterProduct(this.w);
    }
    
    /** Compute and return the RealHouseholder transformation {@link Matrix} for {@link RealVector x} given
     * {@code hessenbergOffset = 0}.
     * 
     * @param x the vector for which we want to compute a RealHouseholder transform
     * @return the matrix */
    public static Matrix computeTransform(final double[] x) {
        return RealHouseholder.of(x).getTransform();
    }
    
    /** Compute {@code I - 2[w, w}<sup>T</sup>{@code ]}, where {@code [, ]} denotes the outer product.
     * 
     * @param w the array of the real components of the {@code w} vector
     * @return {@code I - 2[w, w}<sup>T</sup>{@code ]}
     * 
     * @see RealVector#outerProduct(RealVector) */
    static Matrix iMinusTwiceSelfOuterProduct(final double[] w) {
        final int n = w.length;
        final double[][] answerArr = new double[n][];
        for (int i = 0; i < n; i++) {
            final double[] row = new double[n];
            for (int j = 0; j < n; j++) {
                final double ith = w[i];
                final double jth = w[j];
                
                if (i == j) { /* we're computing the the identity matrix, minus the outer product, so when i == j we
                               * need to account for the "1" on the diagonal */
                    row[j] = 1 - 2 * ith * jth;
                }
                else {
                    row[j] = -2 * ith * jth;
                }
            }
            answerArr[i] = row;
        }
        
        return new Matrix(answerArr, false); // false -> don't clone the array
    }
    
    /** Compute and return the RealHouseholder transformation {@link Matrix} for {@link RealVector x} given
     * {@code hessenbergOffset >= 0}.
     * 
     * @param x the vector for which we want to compute a RealHouseholder transform
     * @param hessenbergOffset the last index in the vector {@code Q.x} that we want to preserve. In other words, each
     *        entry in {@code Q.x} with index strictly greater than this value will be zero.<br>
     *        The name comes from the use of a non-zero value when computing a Hessenberg reduction.
     * @return the matrix */
    public static Matrix computeTransform(final RealVector x, final int hessenbergOffset) {
        final int n = x.getDimension();
        Validation.requireWithinRange(hessenbergOffset, 0, n - 1, "Householder index");
        
        if (hessenbergOffset != 0) {
            final int        startIndex = hessenbergOffset;
            final int        numElts    = x.getDimension() - hessenbergOffset;
            final RealVector subX       = x.getSubVector(startIndex, numElts);
            /* Regarding getSubVector, for posterity: contrary to most methods of the type "getSub...", the input to
             * getSubVector is not a range, but a starting index and "numElts", a total number of elements (or
             * equivalently, the dimension of the returned vector).
             * 
             * E.g.: let x = (a, b, c, d, e, f, g), hhi = 3
             * 
             * Then, dim = 7, startIndex = hhi + 1 = 4, and
             * 
             * numElts = dim - (hhi + 1) = dim - hhi - 1 = 7 - 3 - 1 = 3
             * 
             * so, x.getSubVector(startIndex, numElts) = x.getSubVector(4, 3) = (5, 6, 7) */
            
            final Matrix subHH = computeTransform(subX, 0); // recurse
            
            final Matrix answer = Matrix.identity(n);
            answer.setSubMatrix(subHH.getDataRef(), startIndex, startIndex);
            
            return answer;
        }
        
        return computeTransform(x.toArray());
    }
    
    /** Determine whether the given vector - as a {@code double[]} - is the zero vector
     * 
     * @param x the vector to check
     * @return {@code true} if {@code x == 0}, {@code false} otherwise */
    static boolean isZero(final double[] x) {
        for (int i = 0; i < x.length; i++) {
            if (x[i] != 0.0) {
                return false;
            }
        }
        
        return true;
    }
}
