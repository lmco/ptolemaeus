/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import org.hipparchus.linear.AnyMatrix;

import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;

//CHECKSTYLE.OFF: LineLength: disabled for long URL
/** A class to compute Householder transformations (a.k.a. Householder reflection).<br>
 * <br>
 * 
 * Note: below I describe the complex-valued case. In the real-valued case, everything is the same, except the
 * Householder matrix is symmetric and orthogonal rather than Hermitian and unitary. In addition, the value of {@code k}
 * is computed differently. In the case that the {@link ComplexVector} argument only has imaginary parts equal to zero,
 * the complex case reduces to the real case in all ways.<br>
 * In either case, if {@code Q}<sup>H</sup> is the {@link ComplexMatrix#conjugateTranspose() conjugate transpose} of
 * {@code Q}, we have {@code Q == Q}<sup>-1</sup>:<br>
 * <br>
 * 
 * {@code Q} is Hermitian iff {@code Q = Q}<sup>H</sup> <br>
 * {@code Q} is unitary iff {@code QQ}<sup>H</sup> {@code == Q}<sup>H</sup>{@code Q == I}<br>
 * Which, together, imply that {@code Q = Q}<sup>-1</sup> or, in words, that {@code Q} is <em>involutory</em>.<br>
 * <br>
 * 
 * In the real case, the above is equivalent to:<br>
 * <br>
 * {@code Q} is Symmetric iff {@code Q = Q}<sup>T</sup> <br>
 * {@code Q} is orthogonal iff {@code QQ}<sup>T</sup> {@code == Q}<sup>T</sup>{@code Q == I}<br>
 * Which, together, imply that {@code Q = Q}<sup>-1</sup>.<br>
 * <br>
 * 
 * So, Householder transformations are matrices {@code Q} s.t. <br>
 * <br>
 * 
 * Q&#183;x = (x<sub>0</sub>, x<sub>1</sub>, ..., x<sub>h-1</sub>, k, 0, 0, ...)<br>
 * <br>
 * 
 * where (x<sub>0</sub>, x<sub>1</sub>, ...) are the elements of the vector {@code x}, {@code h} is the Hessenberg
 * offset (described below), and where {@code k} is defined as<br>
 * <br>
 * 
 * k := e<sup>(i arg(x<sub>0</sub>))</sup>&#183;(-sqrt(x<sup>H</sup>&#183;x))<br>
 * <br>
 * 
 * where the expression x<sup>H</sup>&#183;x is the Hermitian dot-product of the complex vector {@code x} with itself.
 * In the real-valued case, this expression for {@code k} simplifies to {@code k := }sign(x<sub>h</sub>)&#183;||x|| <br>
 * <br>
 * 
 * Hessenberg offset: in this class, the Hessenberg offset is the last index in the vector Q&#183;x that we want to be
 * non-zero. In other words, each entry in Q&#183;x with index strictly greater than this value will be zero.<br>
 * The name comes from the use of a non-zero value when computing a Hessenberg reduction. <br>
 * <br>
 * 
 * When one needs to apply a Householder transformation, however, it is (typically) better to apply it implicitly rather
 * than to construct the matrix and carry out the matrix-matrix multiplication. By manipulating the equation that is
 * used to formulate Householder matrices: <br>
 * <br>
 * 
 * A Householder matrix is of the form <code>P = I - 2[w, w<sup>H</sup>]</code>, where {@code [, ]} is the outerproduct
 * and where <code>w<sup>H</sup></code> merely denotes the conjugate of the {@code w} vector.<br>
 * <br>
 * 
 * To pre-multiply a matrix {@code A} by {@code P}, we can do the following:
 * 
 * <pre> <code>
 * PA = (I - 2[w, w<sup>H</sup>])A
 *    =  A - 2[w, w<sup>H</sup>]A
 *    =  A - 2[w, w<sup>H</sup>A] </code> </pre>
 *    
 * and to post-multiply:
 * 
 * <pre> <code>
 * AP = A(I - 2[w, w<sup>H</sup>])
 *    = A - 2A[w,  w<sup>H</sup>]
 *    = A -  2[Aw, w<sup>H</sup>] </code> </pre>
 * 
 * In each case, we've taken a full matrix-matrix multiplication - <code>O(mn<sup>2</sup>)</code> - and replaced it each
 * with a vector-matrix multiplication - <code>O(n<sup>2</sup>)</code> - and a matrix-matrix subtraction -
 * <code>O(n<sup>2</sup>)</code> <br>
 * <br>
 * 
 * See <a href=
 * "https://www.google.com/books/edition/Introduction_to_Numerical_Analysis/1oDXWLb9qEkC?hl=en&gbpv=1&pg=PA225&printsec=frontcover">
 * Stoer and Bulirsch, Introduction to Numerical Analysis (3rd ed.), p. 225 - 226</a>
 * 
 * @param <M> the supported {@link AnyMatrix matrix type}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see #of(double[])
 * @see #of(double[], double[])
 * 
 * @see ComplexHouseholder
 * @see RealHouseholder
 * @see ComplexQRDecomposition
 * @see ComplexHessenbergReduction
 * @see ComplexSchurDecomposition
 * @see RealQRDecomposition */
//CHECKSTYLE.ON: LineLength
public interface Householder<M extends AnyMatrix> {
    
    /** Apply this {@link Householder} transformation to the given matrix from the left, starting at the specified
     * indices.<br>
     * <br>
     * 
     * @param A the matrix to which we want to apply this Householder transformation
     * @param rowStart apply this {@link Householder} transform to every row from this index down
     * @param colStart apply this {@link Householder} transform to every column from this index to the right
     * 
     * @see ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)
     * @see Matrix#preMultiplyInPlace(Matrix, Matrix, int, int) */
    void preApplyInPlace(M A, int rowStart, int colStart);
    
    /** Apply this {@link Householder} transformation to the given matrix from the right, starting at the specified
     * indices.<br>
     * <br>
     * 
     * @param A the matrix to which we want to apply this Householder transformation
     * @param rowStart apply this {@link Householder} transform to every row from this index down
     * @param colStart apply this {@link Householder} transform to every column from this index to the right
     * 
     * @see ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)
     * @see Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)  */
    void postApplyInPlace(M A, int rowStart, int colStart);
    
    /** This identifies whether {@code this} {@link Householder} transformation has an effect; i.e., whether it is the
     * identity transformation. If it is the identity transformation, {@link #getTransform()} will return the identity
     * matrix, and {@link #preApplyInPlace} and {@link #postApplyInPlace} will have no effect.
     * 
     * @return {@code true} if {@code this} {@link Householder} transformation is the identity transformation,
     *         {@code false} otherwise */
    boolean isNoop();
    
    /** Construct and return the actual Householder transformation matrix. Give a choice, one should prefer
     * {@link #preApplyInPlace} and {@link #postApplyInPlace}.
     * 
     * @return the Householder transformation matrix */
    M getTransform();
    
    /** Compute the {@link ComplexHouseholder} for the given real and imaginary parts of a complex vector. This method
     * assumes {@code hessenbergOffset == 0}. See the {@code interface}-level JavaDoc of {@link Householder} for
     * details.
     * 
     * @param xRe the real part of the vector
     * @param xIm the imaginary part of the vector
     * @return the {@link ComplexHouseholder} to transform the given vector into a multiple of the first unit-vector */
    static ComplexHouseholder of(final double[] xRe, final double[] xIm) {
        return ComplexHouseholder.of(xRe, xIm);
    }
    
    /** Compute the {@link RealHouseholder} for the given vector. This method assumes {@code hessenbergOffset == 0}. See
     * the {@code interface}-level JavaDoc of {@link Householder} for details.
     * 
     * @param x the the vector
     * @return the {@link RealHouseholder} to transform the given vector into a multiple of the first unit-vector */
    static RealHouseholder of(final double[] x) {
        return RealHouseholder.of(x);
    }
}
