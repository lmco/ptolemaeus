/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear;

import org.hipparchus.linear.AnyMatrix;

/** An interface for implementors of matrix slicing
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public interface MatrixSlice extends AnyMatrix {
    
    /** Given a row index of this {@link MatrixSlice}, get the corresponding row index of the source matrix
     * 
     * @param sliceRow the row of this {@link MatrixSlice}
     * @return the row index in the source matrix */
    int getSourceRowIndex(int sliceRow);
    
    /** Given a column index of this {@link MatrixSlice}, get the corresponding column index of the source matrix
     * 
     * @param sliceCol the column of this {@link MatrixSlice}
     * @return the column index in the source matrix */
    int getSourceColIndex(int sliceCol);
}
