/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear;

import java.util.stream.IntStream;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;

import ptolemaeus.commons.BeCareful;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.householderreductions.ComplexQRDecomposition;
import ptolemaeus.math.linear.householderreductions.QRDecomposition;
import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

/** A class of methods that can to solve linear systems of equations over the real and complex fields<br>
 * <br>
 * 
 * Note: the low-level methods of this class use backwards and forwards substitution to solve triangular systems of
 * equations. If the triangular matrix is ill-conditioned and/or is (near) singular, the results of these solve methods
 * will be of poor quality. In the case of singular matrices, the results may contain {@link Double#NaN}s and/or
 * {@link Double#isInfinite() infinities}.<br>
 * <br>
 * 
 * The math works out to be the same in the real and complex versions, but the complex versions of the methods look more
 * complicated just because we have to handle the real and imaginary parts being saved separately, and we do in-place
 * {@link Numerics#complexDivide(double, double, double, double) complex division}.<br>
 * <br>
 * 
 * Additionally, the code is nearly identical between backwards and forwards substitution. The differences are only in
 * the indices. <br>
 * <br>
 * 
 * A note on notation:<br>
 * {@code Sum[f_n, {n, a, b}]} <br>
 * is Wolfram notation and means, in words, "sum the values of f_n for all n from a to b" <br>
 * <br>
 * 
 * TODO Ryan Moser, 2024-12-06: add optional iterative improvement (costs O(N^2)); i.e., if {@code x} is an exact
 * solution to {@code Ax = y} and standard back-substitution results in the perturbed solution {@code x + dx}, solve for
 * the error {@code dx} in {@code Adx = A(x + dx) - y} and adjust by subtracting the approximate error from the original
 * solution {@code x + dx} <br>
 * TODO Ryan Moser, 2024-12-06: handle singularities by deleting problematic rows and columns and finding a best-fit
 * solution in the remaining sub-space<br>
 * TODO Ryan Moser, 2024-12-06: can we cheaply determine a good parallelization thresholds for a given runtime?<br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @implNote the default parallelization thresholds are the values where parallelization performance was better on a
 *           machine with 8 cores, each with 2 logical processors (16 LPs total). In this configuration, the JVM will
 *           spin-up at most 16 - 1 = 15 worker threads to parallelize 16x. If your machine has significantly more or
 *           fewer LPs, and you need to maximize performance, you should tweak these values.
 * @implNote note the difference in parallelization techniques between
 *           {@link #solveSquareBackSubstitution(ComplexMatrix, ComplexMatrix)} and
 *           {@link #solveSquareBackSubstitution(Matrix, Matrix)}.<br>
 *           In the complex implementation, we always compute the transpose of {@code Y} before doing the work. In the
 *           real implementation, we don't bother with the transpose and simply parallelize the solve for each column
 *           vector of {@code Y}.<br>
 *           This discrepancy is due to my (RM) testing that showed that parallelizing by column in the complex case is
 *           as much as <em>four times</em> slower than the transpose method for complex matrices with large max-rank
 *           (100 &lt;= n &lt;= 1000) (max-rank being the maximum possible rank of a matrix of a given size, i.e.,
 *           {@code min(m, n)}).<br>
 *           In the {@link Matrix real matrix} case, the difference isn't nearly so large, but it is in the other
 *           direction; i.e., it's faster to parallelize over the columns of {@code Y}. */
public class LinearSolver {
    
    /** the default instance */
    private static final LinearSolver DEFAULT = new LinearSolver(40, 20);
    
    /** a {@link BeCareful} warning for inverse methods */
    private static final String INV_WARNING =
            "if possible, one should always avoid computing the inverse/pseudoinverse of a matrix directly. "
            + "It's a numerically problematic (unstable) operation, and there are often alternatives that are both "
            + "much more stable, and faster; e.g., if one needs to compute the x s.t. Ax = b, "
            + "don't compute x = A^+ * b but, rather, use a LinearSolver::solve method to avoid it completely.";
    
    /** the error message format for when the dimensions of the system {@code AX = Y} don't agree */
    private static final String SYS_DIM_ERROR_FMT =
            "When solving for X in the equation AX = B, where A is a matrix with size (%d x %d), "
            + "B must have the same number of rows as A, but it has size (%d x %d)";
    
    /** For real matrices with max-rank greater than this, we will parallelize
     * {@link #solveSquareBackSubstitution(Matrix, Matrix)} and
     * {@link #solveSquareForwardSubstitution(Matrix, Matrix)} */
    private final int realParallelTr;
    
    /** For complex matrices with max-rank greater than this, we will parallelize
     * {@link #solveSquareBackSubstitution(ComplexMatrix, ComplexMatrix)} and
     * {@link #solveSquareForwardSubstitution(ComplexMatrix, ComplexMatrix)} */
    private final int complexParallelTr;
    
    /** Constructor
     * 
     * @param realParallelTr For real matrices with max-rank greater than this, we will parallelize
     *        {@link #solveSquareBackSubstitution(Matrix, Matrix)}
     * @param complexParallelTr For complex matrices with max-rank greater than this, we will parallelize
     *        {@link #solveSquareBackSubstitution(ComplexMatrix, ComplexMatrix)} */
    public LinearSolver(final int realParallelTr, final int complexParallelTr) {
        Validation.requireGreaterThanEqualTo(realParallelTr, 0, "realParallelTr");
        Validation.requireGreaterThanEqualTo(complexParallelTr, 0, "complexParallelTr");
        
        this.realParallelTr      = realParallelTr;
        this.complexParallelTr   = complexParallelTr;
    }
    
    /** @return the max-rank threshold for parallelizing {@link #solveSquareBackSubstitution(Matrix, Matrix)} and
     *         {@link #solveSquareForwardSubstitution(Matrix, Matrix)} */
    int getRealParallelThreshold() {
        return this.realParallelTr;
    }
    
    /** @return the max-rank threshold for parallelizing
     *         {@link #solveSquareBackSubstitution(ComplexMatrix, ComplexMatrix)} and
     *         {@link #solveSquareForwardSubstitution(ComplexMatrix, ComplexMatrix)} */
    int getComplexParallelThreshold() {
        return this.complexParallelTr;
    }
    
    /** Get the default {@link LinearSolver}. See class-level JavaDoc for details regarding the default values.
     * 
     * @return the default {@link LinearSolver} with a configuration that's likely good enough */
    public static LinearSolver defaultSolver() {
        return DEFAULT;
    }
    
    /** Solve for the vector {@code x} that satisfies the linear system of equations {@code Ax = y}.<br>
     * <br>
     * 
     * If {@code A} is {@link Matrix#isSquare() square} or {@link Matrix#isTall() tall} (determined or overdetermined),
     * the system is solved first by computing the {@link RealQRDecomposition}, and is then using
     * {@link RealQRDecomposition#solve(NVector)}.<br>
     * <br>
     * 
     * If {@code A} is {@link Matrix#isWide() wide} (underdetermined), we use the {@link RealQRDecomposition} of
     * {@code A}<sup>T</sup> to compute the minimal 2-norm solution amongst the infinite family of solutions. See
     * {@link #solve(FieldMatrix, FieldMatrix)} for an explanation. Note that in the real case - like here - the
     * conjugate transpose is just the transpose.
     * 
     * @param AParam the left-hand-side {@code A} matrix
     * @param yParam the right-hand-side {@code y} vector
     * 
     * @return the solution to the linear system */
    public NVector solve(final RealMatrix AParam, final RealVector yParam) {
        checkLinearSystemDimensions(AParam.getRowDimension(), AParam.getColumnDimension(), yParam.getDimension(), 1);
        
        final Matrix  A = Matrix.of(AParam);
        final NVector y = NVector.of(yParam);
        
        if (A.isWide()) { // A is wide s.t. #rows < #columns, so the system is under-determined
            final Matrix              AT = A.transpose(); // A (m x n) is wide, so A^T (n x m) is tall
            final RealQRDecomposition QR = RealQRDecomposition.of(AT);
            final Matrix              Q1 = QR.getReducedQ();
            final Matrix              R1 = QR.getReducedR();
            
            // solve for u in (R1^T)u = y, i.e., compute u = ((R1^T)^-1)y
            final NVector u = this.solveSquareForwardSubstitution(R1.transpose(), y);
            
            return Q1.operate(u);
        }
        
        return RealQRDecomposition.of(A).solve(y, this);
    }
    
    /** Solve for the matrix {@code X} that satisfies the linear system of equations {@code AX = Y}.<br>
     * <br>
     * 
     * If {@code A} is {@link Matrix#isSquare() square} or {@link Matrix#isTall() tall} (determined or overdetermined),
     * the system is solved first by computing the {@link RealQRDecomposition}, and is then using
     * {@link RealQRDecomposition#solve(Matrix)}.<br>
     * <br>
     * 
     * If {@code A} is {@link Matrix#isWide() wide} (underdetermined), we use the {@link RealQRDecomposition} of
     * {@code A}<sup>T</sup> to compute the minimal 2-norm solution amongst the infinite family of solutions. See
     * {@link #solve(FieldMatrix, FieldMatrix)} for an explanation. Note that in the real case - like here - the
     * conjugate transpose is just the transpose.
     * 
     * @param AParam the left-hand-side {@code A} matrix
     * @param YParam the right-hand-side {@code y} matrix
     * 
     * @return the solution to the linear system */
    public Matrix solve(final RealMatrix AParam, final RealMatrix YParam) {
        checkLinearSystemDimensions(AParam.getRowDimension(), AParam.getColumnDimension(),
                                    YParam.getRowDimension(), YParam.getColumnDimension());
        
        final Matrix A = Matrix.of(AParam);
        final Matrix Y = Matrix.of(YParam);
        
        if (A.isWide()) { // A is wide s.t. #rows < #columns, so the system is under-determined
            final Matrix              AT = A.transpose(); // A (m x n) is wide, so A^T (n x m) is tall
            final RealQRDecomposition QR = RealQRDecomposition.of(AT);
            final Matrix              Q1 = QR.getReducedQ();
            final Matrix              R1 = QR.getReducedR();
            
            // solve for U in (R1^T)U = Y, i.e., compute U = ((R1^T)^-1)U
            final Matrix U = this.solveSquareForwardSubstitution(R1.transpose(), Y);
            
            return Q1.times(U);
        }
        
        return RealQRDecomposition.of(A).solve(Y, this);
    }
    
    /** Solve for the vector {@code x} that satisfies the linear system of equations {@code Ax = y}.<br>
     * <br>
     * 
     * If {@code A} is {@link ComplexMatrix#isSquare() square} or {@link ComplexMatrix#isTall() tall} (determined or
     * overdetermined), the system is solved first by computing the {@link ComplexQRDecomposition}, and is then using
     * {@link ComplexQRDecomposition#solve(ComplexVector)}.<br>
     * <br>
     * 
     * If {@code A} is {@link ComplexMatrix#isWide() wide} (underdetermined), we use the {@link ComplexQRDecomposition}
     * of {@code A}<sup>T</sup> to compute the minimal 2-norm solution amongst the infinite family of solutions. See
     * {@link #solve(FieldMatrix, FieldMatrix)} for an explanation.
     * 
     * @param AParam the left-hand-side {@code A} matrix
     * @param yParam the right-hand-side {@code y} vector
     * 
     * @return the solution to the linear system */
    public ComplexVector solve(final FieldMatrix<Complex> AParam, final FieldVector<Complex> yParam) {
        checkLinearSystemDimensions(AParam.getRowDimension(), AParam.getColumnDimension(), yParam.getDimension(), 1);
        
        final ComplexMatrix A = ComplexMatrix.of(AParam);
        final ComplexVector y = ComplexVector.of(yParam);
        
        if (A.isWide()) { // A is wide s.t. #rows < #columns, so the system is under-determined
            final ComplexMatrix          AT = A.conjugateTranspose(); // A (m x n) is wide, so A^H (n x m) is tall
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(AT);
            final ComplexMatrix          Q1 = QR.getReducedQ();
            final ComplexMatrix          R1 = QR.getReducedR();
            
            // solve for u in (R1^H)u = y, i.e., compute u = ((R1^H)^-1)y
            final ComplexVector u = this.solveSquareForwardSubstitution(R1.conjugateTranspose(), y);
            
            return Q1.operate(u);
        }
        
        return ComplexQRDecomposition.of(A).solve(y, this);
    }
    
    /** Solve for the matrix {@code X} that satisfies the linear system of equations {@code AX = Y}.<br>
     * <br>
     * 
     * If {@code A} is {@link ComplexMatrix#isSquare() square} or {@link ComplexMatrix#isTall() tall} (determined or
     * overdetermined), the system is solved first by computing the {@link ComplexQRDecomposition}, and is then using
     * {@link ComplexQRDecomposition#solve(ComplexMatrix)}.<br>
     * <br>
     * 
     * If {@code A} is {@link Matrix#isWide() wide} (underdetermined), we use the {@link RealQRDecomposition} of
     * {@code A}<sup>T</sup> to compute the minimal 2-norm solution amongst the infinite family of solutions. Note: the
     * process below is described using a matrix RHS, but if you consider a vector RHS as a matrix with a single column,
     * the derivation is the same.
     * 
     * <pre><code> Suppose A is a member of C<sup>m x n</sup> for m {@literal <} n
     * Let QR = A<sup>H</sup> be a reduced {@link QRDecomposition} of A<sup>H</sup>
     * (see the class-level JavaDoc of {@link QRDecomposition} for details on the reduced form)
     * 
     * So, A<sup>H</sup> = QR and A = R<sup>H</sup>Q<sup>H</sup>
     * 
     * Then
     * 
     * AX = Y
     * 
     * R<sup>H</sup>Q<sup>H</sup>X = Y
     * 
     * Q<sup>H</sup>X = (R<sup>H</sup>)<sup>-1</sup>Y
     * 
     * R is upper-triangular (R<sup>H</sup> is lower triangular), so we define
     * 
     * U = (R<sup>H</sup>)<sup>-1</sup>Y
     * 
     * and compute U by solving
     * 
     * R<sup>H</sup>U = Y
     * 
     * using forward substitution, then we have
     * 
     * Q<sup>H</sup>X = U
     * 
     * X = (Q<sup>H</sup>)<sup>-1</sup>U = QU </code></pre>
     * 
     * @param AParam the left-hand-side {@code A} matrix
     * @param YParam the right-hand-side {@code y} matrix
     * 
     * @return the solution to the linear system */
    public ComplexMatrix solve(final FieldMatrix<Complex> AParam, final FieldMatrix<Complex> YParam) {
        checkLinearSystemDimensions(AParam.getRowDimension(), AParam.getColumnDimension(),
                                    YParam.getRowDimension(), YParam.getColumnDimension());
        
        final ComplexMatrix A = ComplexMatrix.of(AParam);
        final ComplexMatrix Y = ComplexMatrix.of(YParam);
        
        if (A.isWide()) { // A is wide s.t. #rows < #columns, so the system is under-determined
            final ComplexMatrix          AT = A.conjugateTranspose(); // A (m x n) is wide, so A^H (n x m) is tall
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(AT);
            final ComplexMatrix          Q1 = QR.getReducedQ();
            final ComplexMatrix          R1 = QR.getReducedR();
            
            // solve for U in (R1^H)U = Y, i.e., compute U = ((R1^H)^-1)Y
            final ComplexMatrix U = this.solveSquareForwardSubstitution(R1.conjugateTranspose(), Y);
            
            return Q1.times(U);
        }
        
        return ComplexQRDecomposition.of(A).solve(Y, this);
    }
    
    /** Check the compatibility of dimensions of a linear system {@code AX = Y} in which we know {@code A} and {@code Y}
     * and want to solve for {@code X}. In particular, ensure that {@code numRowsA == numRowsY}. The other values are
     * included to provide a better error message.
     * 
     * @param numRowsA the number of rows in the LHS matrix
     * @param numColsA the number of columns in the LHS matrix
     * @param numRowsY the number of rows in the RHS matrix or vector
     * @param numColsY the number of columns in the RHS matrix or vector. In the case of a vector, this is
     *        {@code numColsY == 1}. */
    static void checkLinearSystemDimensions(final int numRowsA, final int numColsA,
                                            final int numRowsY, final int numColsY) {
        Validation.requireEqualsTo(numRowsA, numRowsY,
                                   () -> SYS_DIM_ERROR_FMT.formatted(numRowsA, numColsA, numRowsY, numColsY));
    }
    
    /** Given an upper-triangular {@link Matrix} {@code T} and right-hand side {@link NVector} {@code y}, use
     * back-substitution to solve for the {@link NVector} {@code x} that satisfies {@code T.x = y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code y} must have dimension {@code n} and {@code x} will have dimension
     * {@code n}.
     * 
     * @param T the upper-triangular {@link Matrix}. Calling methods <em>must</em> themselves ensure that {@code T} is
     *        square, non-singular, and upper-triangular!
     * @param y the right-hand side of the equation
     * @return {@link NVector x} which satisfies {@code T.x = y}
     *        
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public NVector solveSquareBackSubstitution(final Matrix T, final NVector y) {
        final int n = y.getDimension();
        
        final double[][] TArr = T.getDataRef();
        final double[]   yArr = y.getDataRef();
        
        final double[] xArr = new double[n]; // answer will be written into this array
        
        for (int i = n - 1; i >= 0; i--) {
            final double[] Ti = TArr[i];
            
            double r = yArr[i];
            for (int j = i + 1; j < n; j++) { // iterate to compute yi - Sum[Tij * xj, {j, i + 1, n - 1}]
                r -= xArr[j] * Ti[j];
            }
            
            final double Tii = Ti[i];
            
            // Given       r  = yi - Sum[Tij * xj, {j, i + 1, n - 1}]
            // compute     xi = r / Tii
            final double xi = r / Tii;
            
            xArr[i] = xi;
        }
        
        return new NVector(xArr);
    }
    
    /** Given a lower-triangular {@link Matrix} {@code T} and right-hand side {@link NVector} {@code y}, use
     * forward-substitution to solve for the {@link NVector} {@code x} that satisfies {@code T.x = y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code y} must have dimension {@code n} and {@code x} will have dimension
     * {@code n}.
     * 
     * @param T the lower-triangular {@link Matrix}. Calling methods <em>must</em> themselves ensure that {@code T} is
     *        square, non-singular, and lower-triangular!
     * @param y the right-hand side of the equation
     * @return {@link NVector x} which satisfies {@code T.x = y}
     * 
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public NVector solveSquareForwardSubstitution(final Matrix T, final NVector y) {
        final int n = y.getDimension();
        
        final double[][] TArr = T.getDataRef();
        final double[]   yArr = y.getDataRef();
        
        final double[] xArr = new double[n]; // answer will be written into this array
        
        for (int i = 0; i < n; i++) {
            final double[] Ti = TArr[i];
            
            double r = yArr[i];
            for (int j = 0; j < i + 1; j++) { // iterate to compute yi - Sum[Tij * xj, {j, 0, i}]
                r -= xArr[j] * Ti[j];
            }
            
            final double Tii = Ti[i];
            
            // Given       r  = yi - Sum[Tij * xj, {j, 0, i}]
            // compute     xi = r / Tii
            final double xi = r / Tii;
            
            xArr[i] = xi;
        }
        
        return new NVector(xArr);
    }
    
    /** Given an upper-triangular {@link Matrix} {@code T} and right-hand side {@link Matrix} {@code Y}, use
     * back-substitution to solve for the {@link Matrix} {@code X} that satisfies {@code T.X = Y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code Y} must have {@code n} rows but may have any number of columns. If
     * {@code Y} has {@code k} columns, {@code X} will be {@code (n x k)}.
     * 
     * @param T the upper-triangular {@link Matrix}. Calling methods <em>must</em> themselves ensure that {@code T} is
     *        square, non-singular, and upper-triangular!
     * @param Y the right-hand side of the equation
     * @return {@link Matrix X} which satisfies {@code T.X = Y}
     *        
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public Matrix solveSquareBackSubstitution(final Matrix T, final Matrix Y) {
        final int n = T.getColumnDimension(); // square, #rows in T == #cols in T
        final int k = Y.getColumnDimension(); // #cols in Y
        
        if (n >= this.realParallelTr) {
            final Matrix X = new Matrix(new double[n][k]);
            IntStream.range(0, k).parallel().forEach(col -> {
                final NVector XCol = solveSquareBackSubstitution(T, Y.getColumnVector(col));
                X.setColumnVector(col, XCol);
            });
            
            return X;
        }
        
        /* Note that we use Y transpose and save to X transpose. Doing this allows us to minimize row iterations, which
         * are much more expensive than column iterations; e.g., within the transpRowIdx-loop (transpRowIdx ->
         * transposed row index), we can save the rows of X^T and Y^T (the columns of Y and X) to variables once per
         * transpRowIdx iteration. */
        final double[][] TArr  = T.getDataRef();
        final double[][] YArrT = Y.transpose().getDataRef();
        
        final double[][] XArrT = new double[k][n]; // answer will be written into this array
        
        for (int transpRowIdx = 0; transpRowIdx < k; transpRowIdx++) {
            doMatrixBackSubReal(n, transpRowIdx, TArr, YArrT, XArrT);
        }
        
        return new Matrix(XArrT).transpose();
    }
    
    /** Use back-substitution to solve for a column of the matrix {@code X} that satisfies {@code TX = Y}, and save that
     * data to {@code X^T} directly.<br>
     * <br>
     * 
     * Note that we are actually operating on the transposes (<em>not</em> the conjugate transposes) of {@code X} and
     * {@code Y}. Doing this allows us to minimize row iterations, which are much more expensive than column
     * iterations.<br>
     * <br>
     * 
     * Given {@code x}<sub>k</sub> and {@code y}<sub>k</sub>, the k<sup>th</sup> columns of {@code X} and {@code Y}
     * respectively, the formula for the i<sup>th</sup> element of {@code x}<sub>k</sub> that solves
     * {@code Tx}<sub>k</sub>{@code = y}<sub>k</sub> is as follows:<br>
     * <br>
     * 
     * <pre>
     * <code>
     * x<sub>i</sub> = (y<sub>i</sub> - Sum[T<sub>ij</sub>x<sub>j</sub>, {j, i + 1, n - 1}]) / T<sub>ii</sub>
     * </code>
     * </pre>
     * 
     * @param n the dimension of the square system
     * @param transpRowIdx the index of the transposed row (column of the original) for which we're solving.
     * @param TArr the {@code double[][]} data of the upper-triangular matrix {@code T}
     * @param YArrT the transpose of the {@code double[][]} data of {@code Y}
     * @param XArrT the transpose of the {@code double[][]} data of {@code X}.  This array is modified in-place! */
    private static void doMatrixBackSubReal(final int n,
                                            final int transpRowIdx,
                                            final double[][] TArr,
                                            final double[][] YArrT,
                                            final double[][] XArrT) {
        /* Note the following two variable names reference columns despite iterating over a row index: the row of a
         * transpose is a column of the original, thus the names. */
        final double[] YCol = YArrT[transpRowIdx];
        final double[] XCol = XArrT[transpRowIdx];
        
        for (int i = n - 1; i >= 0; i--) {
            final double[] Ti = TArr[i];
            
            double r = YCol[i];
            for (int j = i + 1; j < n; j++) { // iterate to compute yi - Sum[Tij * xj, {j, i + 1, n - 1}]
                r -= XCol[j] * Ti[j];
            }
            
            final double Tii = Ti[i];
            
            // Given       r  = yi - Sum[Tij * xj, {j, i + 1, n - 1}]
            // compute     xi = r / Tii
            final double xi = r / Tii;
            
            XCol[i] = xi;
        }
    }
    
    /** Given a lower-triangular {@link Matrix} {@code T} and right-hand side {@link Matrix} {@code Y}, use
     * forward-substitution to solve for the {@link Matrix} {@code X} that satisfies {@code T.X = Y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code Y} must have {@code n} rows but may have any number of columns. If
     * {@code Y} has {@code k} columns, {@code X} will be {@code (n x k)}.
     * 
     * @param T the lower-triangular {@link Matrix}. Calling methods <em>must</em> themselves ensure that {@code T} is
     *        square, non-singular, and upper-triangular!
     * @param Y the right-hand side of the equation
     * @return {@link Matrix X} which satisfies {@code T.X = Y}
     *        
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public Matrix solveSquareForwardSubstitution(final Matrix T, final Matrix Y) {
        final int n = T.getColumnDimension(); // square, #rows in T == #cols in T
        final int k = Y.getColumnDimension(); // #cols in Y
        
        if (n >= this.realParallelTr) {
            final Matrix X = new Matrix(new double[n][k]);
            IntStream.range(0, k).parallel().forEach(col -> {
                final NVector XCol = solveSquareForwardSubstitution(T, Y.getColumnVector(col));
                X.setColumnVector(col, XCol);
            });
            
            return X;
        }
        
        /* Note that we use Y transpose and save to X transpose. Doing this allows us to minimize row iterations, which
         * are much more expensive than column iterations; e.g., within the transpRowIdx-loop (transpRowIdx ->
         * transposed row index), we can save the rows of X^T and Y^T (the columns of Y and X) to variables once per
         * transpRowIdx iteration. */
        final double[][] TArr  = T.getDataRef();
        final double[][] YArrT = Y.transpose().getDataRef();
        
        final double[][] XArrT = new double[k][n]; // answer will be written into this array
        
        for (int transpRowIdx = 0; transpRowIdx < k; transpRowIdx++) {
            doMatrixForwardSubReal(n, transpRowIdx, TArr, YArrT, XArrT);
        }
        
        return new Matrix(XArrT).transpose();
    }
    
    /** Use forward-substitution to solve for a column of the matrix {@code X} that satisfies {@code TX = Y}, and save
     * that data to {@code X^T} directly.<br>
     * <br>
     * 
     * Note that we are actually operating on the transposes (<em>not</em> the conjugate transposes) of {@code X} and
     * {@code Y}. Doing this allows us to minimize row iterations, which are much more expensive than column
     * iterations.<br>
     * <br>
     * 
     * Given {@code x}<sub>k</sub> and {@code y}<sub>k</sub>, the k<sup>th</sup> columns of {@code X} and {@code Y}
     * respectively, the formula for the i<sup>th</sup> element of {@code x}<sub>k</sub> that solves
     * {@code Tx}<sub>k</sub>{@code = y}<sub>k</sub> is as follows:<br>
     * <br>
     * 
     * <pre>
     * <code>
     * x<sub>i</sub> = (y<sub>i</sub> - Sum[T<sub>ij</sub>x<sub>j</sub>, {j, 0, i}]) / T<sub>ii</sub>
     * </code>
     * </pre>
     * 
     * @param n the dimension of the square system
     * @param transpRowIdx the index of the transposed row (column of the original) for which we're solving.
     * @param TArr the {@code double[][]} data of the upper-triangular matrix {@code T}
     * @param YArrT the transpose of the {@code double[][]} data of {@code Y}
     * @param XArrT the transpose of the {@code double[][]} data of {@code X}.  This array is modified in-place! */
    private static void doMatrixForwardSubReal(final int n,
                                               final int transpRowIdx,
                                               final double[][] TArr,
                                               final double[][] YArrT,
                                               final double[][] XArrT) {
        /* Note the following two variable names reference columns despite iterating over a row index: the row of a
         * transpose is a column of the original, thus the names. */
        final double[] YCol = YArrT[transpRowIdx];
        final double[] XCol = XArrT[transpRowIdx];
        
        for (int i = 0; i < n; i++) {
            final double[] Ti = TArr[i];
            
            double r = YCol[i];
            for (int j = 0; j < i + 1; j++) { // iterate to compute yi - Sum[Tij * xj, {j, 0, i}]
                r -= XCol[j] * Ti[j];
            }
            
            final double Tii = Ti[i];
            
            // Given       r  = yi - Sum[Tij * xj, {j, 0, i}]
            // compute     xi = r / Tii
            final double xi = r / Tii;
            
            XCol[i] = xi;
        }
    }
    
    /** Given an upper-triangular {@link ComplexMatrix} {@code T} and right-hand side {@link ComplexVector} {@code y},
     * use back-substitution to solve for the {@link ComplexVector} {@code x} that satisfies {@code T.x = y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code y} must have dimension {@code n} and {@code x} will have dimension
     * {@code n}.
     * 
     * @param T the upper-triangular {@link ComplexMatrix}. Calling methods <em>must</em> themselves ensure that
     *        {@code T} is square, non-singular, and upper-triangular!
     * @param y the right-hand side of the equation
     * @return {@link ComplexVector x} which satisfies {@code T.x = y}
     *        
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public ComplexVector solveSquareBackSubstitution(final ComplexMatrix T, final ComplexVector y) {
        final int n = y.getDimension();
        
        final double[][][] TReIm = T.toReImParts();
        final double[][]   TRe   = TReIm[0];
        final double[][]   TIm   = TReIm[1];
        
        final double[][] yReIm = y.getDataRef();  // [2][n]
        final double[] yRe = yReIm[0];
        final double[] yIm = yReIm[1];
        
        final double[] xRe = new double[n];
        final double[] xIm = new double[n];
        
        final double[] divisionSave = new double[2];
        
        for (int i = n - 1; i >= 0; i--) {
            double runningRe = yRe[i];
            double runningIm = yIm[i];
            
            final double[] TRei = TRe[i];
            final double[] TImi = TIm[i];
            
            for (int j = i + 1; j < n; j++) { // iterate to compute yi - Sum[Tij * xj, {j, i + 1, n - 1}]
                final double xjRe = xRe[j];
                final double xjIm = xIm[j];
                
                final double TijRe = TRei[j];
                final double TijIm = TImi[j];
                
                // xj * Tij
                final double xjTijRe = Numerics.complexXYRe(xjRe, xjIm, TijRe, TijIm);
                final double xjTijIm = Numerics.complexXYIm(xjRe, xjIm, TijRe, TijIm);
                
                runningRe -= xjTijRe;
                runningIm -= xjTijIm;
            }
            
            final double TiiRe = TRei[i];
            final double TiiIm = TImi[i];
            
            // compute (a + b i) / (c + d i) where 
            // a + b i = runningRe + runningIm i
            // c + d i =     TiiRe +     TiiIm i
            Numerics.complexDivide(runningRe, runningIm, TiiRe, TiiIm, divisionSave);
            
            xRe[i] = divisionSave[0];
            xIm[i] = divisionSave[1];
        }
        
        return new ComplexVector(xRe, xIm);
    }
    
    /** Given a lower-triangular {@link ComplexMatrix} {@code T} and right-hand side {@link ComplexVector} {@code y},
     * use forward-substitution to solve for the {@link ComplexVector} {@code x} that satisfies {@code T.x = y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code y} must have dimension {@code n} and {@code x} will have dimension
     * {@code n}.
     * 
     * @param T the lower-triangular {@link ComplexMatrix}. Calling methods <em>must</em> themselves ensure that
     *        {@code T} is square, non-singular, and lower-triangular!
     * @param y the right-hand side of the equation
     * @return {@link ComplexVector x} which satisfies {@code T.x = y}
     * 
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public ComplexVector solveSquareForwardSubstitution(final ComplexMatrix T, final ComplexVector y) {
        final int n = y.getDimension();
        
        final double[][][] TReIm = T.toReImParts();
        final double[][]   TRe   = TReIm[0];
        final double[][]   TIm   = TReIm[1];
        
        final double[][] yReIm = y.getDataRef();  // [2][n]
        final double[] yRe = yReIm[0];
        final double[] yIm = yReIm[1];
        
        final double[] xRe = new double[n];
        final double[] xIm = new double[n];
        
        final double[] divisionSave = new double[2];
        
        for (int i = 0; i < n; i++) {
            double runningRe = yRe[i];
            double runningIm = yIm[i];
            
            final double[] TRei = TRe[i];
            final double[] TImi = TIm[i];
            
            for (int j = 0; j < i + 1; j++) { // iterate to compute yi - Sum[Tij * xj, {j, 0, i}]
                final double xjRe = xRe[j];
                final double xjIm = xIm[j];
                
                final double TijRe = TRei[j];
                final double TijIm = TImi[j];
                
                // xj * Tij
                final double xjTijRe = Numerics.complexXYRe(xjRe, xjIm, TijRe, TijIm);
                final double xjTijIm = Numerics.complexXYIm(xjRe, xjIm, TijRe, TijIm);
                
                runningRe -= xjTijRe;
                runningIm -= xjTijIm;
            }
            
            final double TiiRe = TRei[i];
            final double TiiIm = TImi[i];
            
            // compute (a + b i) / (c + d i) where 
            // a + b i = runningRe + runningIm i
            // c + d i =     TiiRe +     TiiIm i
            Numerics.complexDivide(runningRe, runningIm, TiiRe, TiiIm, divisionSave);
            
            xRe[i] = divisionSave[0];
            xIm[i] = divisionSave[1];
        }
        
        return new ComplexVector(xRe, xIm);
    }
    
    /** Given an upper-triangular {@link ComplexMatrix} {@code T} and right-hand side {@link ComplexMatrix} {@code Y},
     * use back-substitution to solve for the {@link ComplexMatrix} {@code X} that satisfies {@code T.X = Y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code Y} must have {@code n} rows but may have any number of columns. If
     * {@code Y} has {@code k} columns, {@code X} will be {@code (n x k)}.
     * 
     * @param T the upper-triangular {@link ComplexMatrix}. Calling methods <em>must</em> themselves ensure that
     *        {@code T} is square, non-singular, and upper-triangular!
     * @param Y the right-hand side of the equation
     * @return {@link ComplexMatrix X} which satisfies {@code T.X = Y}
     *        
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public ComplexMatrix solveSquareBackSubstitution(final ComplexMatrix T, final ComplexMatrix Y) {
        /* Note that we use Y transpose and save to X transpose. Doing this allows us to minimize row iterations, which
         * are much more expensive than column iterations; e.g., within the transpRowIdx-loop (transpRowIdx ->
         * transposed row index), we can save the rows of X^T and Y^T (the columns of Y and X) to variables once per
         * transpRowIdx iteration. */
        
        final int n = T.getColumnDimension(); // square, #rows in T == #cols in T
        final int k = Y.getColumnDimension(); // #cols in Y
        
        final double[][][] TReIm = T.toReImParts();
        final double[][]   TRe   = TReIm[0];
        final double[][]   TIm   = TReIm[1];
        
        final double[][][] YTReIm = Y.transpose().toReImParts();
        final double[][]   YTRe   = YTReIm[0];
        final double[][]   YTIm   = YTReIm[1];
        
        final double[][] XTRe = new double[k][n];
        final double[][] XTIm = new double[k][n];
        
        if (n >= this.complexParallelTr) {
            IntStream.range(0, k).parallel().forEach(transpRowIdx -> {
                doMatrixBackSubComplex(n, transpRowIdx, TRe, TIm, YTRe, YTIm, XTRe, XTIm);
            });
        }
        else {
            for (int transpRowIdx = 0; transpRowIdx < k; transpRowIdx++) {
                doMatrixBackSubComplex(n, transpRowIdx, TRe, TIm, YTRe, YTIm, XTRe, XTIm);
            }
        }

        return new ComplexMatrix(XTRe, XTIm).transpose();
    }
    
    /** Use back-substitution to solve for a column of the matrix {@code X} that satisfies {@code TX = Y}, and save that
     * data to {@code X^T} directly.<br>
     * <br>
     * 
     * Note that we are actually operating on the transposes (<em>not</em> the conjugate transposes) of {@code X} and
     * {@code Y}. Doing this allows us to minimize row iterations, which are much more expensive than column
     * iterations.<br>
     * <br>
     * 
     * Given {@code x}<sub>k</sub> and {@code y}<sub>k</sub>, the k<sup>th</sup> columns of {@code X} and {@code Y}
     * respectively, the formula for the i<sup>th</sup> element of {@code x}<sub>k</sub> that solves
     * {@code Tx}<sub>k</sub>{@code = y}<sub>k</sub> is as follows:<br>
     * <br>
     * 
     * <pre>
     * <code>
     * x<sub>i</sub> = (y<sub>i</sub> - Sum[T<sub>ij</sub>x<sub>j</sub>, {j, i + 1, n - 1}]) / T<sub>ii</sub>
     * </code>
     * </pre>
     * 
     * @param n the dimension of the square system
     * @param transpRowIdx the index of the transposed row (column of the original) for which we're solving.
     * @param TRe the real part of the {@code double[][]} data of the upper-triangular matrix {@code T}
     * @param TIm the imaginary part of the {@code double[][]} data of the upper-triangular matrix {@code T}
     * @param YTRe the transpose of the real part of the {@code double[][]} data of {@code Y}
     * @param YTIm the transpose of the imaginary part of the {@code double[][]} data of {@code Y}
     * @param XTRe the transpose of the real part of the {@code double[][]} data of {@code X}. This array is modified
     *        in-place!
     * @param XTIm the transpose of the imaginary part of the {@code double[][]} data of {@code X}. This array is
     *        modified in-place! */
    private static void doMatrixBackSubComplex(final int n,
                                               final int transpRowIdx,
                                               final double[][] TRe,  final double[][] TIm,
                                               final double[][] YTRe, final double[][] YTIm,
                                               final double[][] XTRe, final double[][] XTIm) {
        /* Note the following four variable names reference columns despite iterating over a row index: the row of a
         * transpose is a column of the original, thus the names. */
        final double[] YColRe = YTRe[transpRowIdx];
        final double[] YColIm = YTIm[transpRowIdx];
        
        final double[] XColRe = XTRe[transpRowIdx];
        final double[] XColIm = XTIm[transpRowIdx];
        
        final double[] divisionSave = new double[2];
        
        for (int i = n - 1; i >= 0; i--) {
            double runningRe = YColRe[i];
            double runningIm = YColIm[i];
            
            final double[] TRei = TRe[i];
            final double[] TImi = TIm[i];
            
            for (int j = i + 1; j < n; j++) { // iterate to compute yi - Sum[Tij * xj, {j, i + 1, n - 1}]
                final double xjRe = XColRe[j];
                final double xjIm = XColIm[j];
                
                final double TijRe = TRei[j];
                final double TijIm = TImi[j];
                
                // xj * Tij
                final double xjTijRe = Numerics.complexXYRe(xjRe, xjIm, TijRe, TijIm);
                final double xjTijIm = Numerics.complexXYIm(xjRe, xjIm, TijRe, TijIm);
                
                runningRe -= xjTijRe;
                runningIm -= xjTijIm;
            }
            
            final double TiiRe = TRei[i];
            final double TiiIm = TImi[i];
            
            // compute (a + b i) / (c + d i) where 
            // a + b i = runningRe + runningIm i
            // c + d i =     TiiRe +     TiiIm i
            Numerics.complexDivide(runningRe, runningIm, TiiRe, TiiIm, divisionSave);
            
            XColRe[i] = divisionSave[0];
            XColIm[i] = divisionSave[1];
        }
        
        XTRe[transpRowIdx] = XColRe;
        XTIm[transpRowIdx] = XColIm;
    }
    
    /** Given a lower-triangular {@link ComplexMatrix} {@code T} and right-hand side {@link ComplexMatrix} {@code Y},
     * use forward-substitution to solve for the {@link ComplexMatrix} {@code X} that satisfies {@code T.X = Y}.<br>
     * <br>
     * 
     * If {@code T} is {@code (n x n)}, {@code Y} must have {@code n} rows but may have any number of columns. If
     * {@code Y} has {@code k} columns, {@code X} will be {@code (n x k)}.
     * 
     * @param T the upper-triangular {@link ComplexMatrix}. Calling methods <em>must</em> themselves ensure that
     *        {@code T} is square, non-singular, and lower-triangular!
     * @param Y the right-hand side of the equation
     * @return {@link ComplexMatrix X} which satisfies {@code T.X = Y}
     *        
     * @implNote this is a low-level, high-performance method, so we do <em>not</em> perform any input checking. The
     *           caller must ensure ahead of time that their systems are of proper dimension.<br>
     *           Bad inputs will silently result in bad outputs! */
    public ComplexMatrix solveSquareForwardSubstitution(final ComplexMatrix T, final ComplexMatrix Y) {
        /* Note that we use Y transpose and save to X transpose. Doing this allows us to minimize row iterations, which
         * are much more expensive than column iterations; e.g., within the transpRowIdx-loop (transpRowIdx ->
         * transposed row index), we can save the rows of X^T and Y^T (the columns of Y and X) to variables once per
         * transpRowIdx iteration. */
        
        final int n = T.getColumnDimension(); // square, #rows in T == #cols in T
        final int k = Y.getColumnDimension(); // #cols in Y
        
        final double[][][] TReIm = T.toReImParts();
        final double[][]   TRe   = TReIm[0];
        final double[][]   TIm   = TReIm[1];
        
        final double[][][] YTReIm = Y.transpose().toReImParts();
        final double[][]   YTRe   = YTReIm[0];
        final double[][]   YTIm   = YTReIm[1];
        
        final double[][] XTRe = new double[k][n];
        final double[][] XTIm = new double[k][n];
        
        if (n >= this.complexParallelTr) {
            IntStream.range(0, k).parallel().forEach(transpRowIdx -> {
                doMatrixForwardSubComplex(n, transpRowIdx, TRe, TIm, YTRe, YTIm, XTRe, XTIm);
            });
        }
        else {
            for (int transpRowIdx = 0; transpRowIdx < k; transpRowIdx++) {
                doMatrixForwardSubComplex(n, transpRowIdx, TRe, TIm, YTRe, YTIm, XTRe, XTIm);
            }
        }

        return new ComplexMatrix(XTRe, XTIm).transpose();
    }
    
    /** Use forward-substitution to solve for a column of the matrix {@code X} that satisfies {@code TX = Y}, and save
     * that data to {@code X^T} directly.<br>
     * <br>
     * 
     * Note that we are actually operating on the transposes (<em>not</em> the conjugate transposes) of {@code X} and
     * {@code Y}. Doing this allows us to minimize row iterations, which are much more expensive than column
     * iterations.<br>
     * <br>
     * 
     * Given {@code x}<sub>k</sub> and {@code y}<sub>k</sub>, the k<sup>th</sup> columns of {@code X} and {@code Y}
     * respectively, the formula for the i<sup>th</sup> element of {@code x}<sub>k</sub> that solves
     * {@code Tx}<sub>k</sub>{@code = y}<sub>k</sub> is as follows:<br>
     * <br>
     * 
     * <pre>
     * <code>
     * x<sub>i</sub> = (y<sub>i</sub> - Sum[T<sub>ij</sub>x<sub>j</sub>, {j, 0, i}]) / T<sub>ii</sub>
     * </code>
     * </pre>
     * 
     * @param n the dimension of the square system
     * @param transpRowIdx the index of the transposed row (column of the original) for which we're solving.
     * @param TRe the real part of the {@code double[][]} data of the lower-triangular matrix {@code T}
     * @param TIm the imaginary part of the {@code double[][]} data of the lower-triangular matrix {@code T}
     * @param YTRe the transpose of the real part of the {@code double[][]} data of {@code Y}
     * @param YTIm the transpose of the imaginary part of the {@code double[][]} data of {@code Y}
     * @param XTRe the transpose of the real part of the {@code double[][]} data of {@code X}. This array is modified
     *        in-place!
     * @param XTIm the transpose of the imaginary part of the {@code double[][]} data of {@code X}. This array is
     *        modified in-place! */
    private static void doMatrixForwardSubComplex(final int n,
                                                  final int transpRowIdx,
                                                  final double[][] TRe,  final double[][] TIm,
                                                  final double[][] YTRe, final double[][] YTIm,
                                                  final double[][] XTRe, final double[][] XTIm) {
        /* Note the following four variable names reference columns despite iterating over a row index: the row of a
         * transpose is a column of the original, thus the names. */
        final double[] YColRe = YTRe[transpRowIdx];
        final double[] YColIm = YTIm[transpRowIdx];
        
        final double[] XColRe = XTRe[transpRowIdx];
        final double[] XColIm = XTIm[transpRowIdx];
        
        final double[] divisionSave = new double[2];
        
        for (int i = 0; i < n; i++) {
            double runningRe = YColRe[i];
            double runningIm = YColIm[i];
            
            final double[] TRei = TRe[i];
            final double[] TImi = TIm[i];
            
            for (int j = 0; j < i + 1; j++) { // iterate to compute yi - Sum[Tij * xj, {j, 0, i}]
                final double xjRe = XColRe[j];
                final double xjIm = XColIm[j];
                
                final double TijRe = TRei[j];
                final double TijIm = TImi[j];
                
                // xj * Tij
                final double xjTijRe = Numerics.complexXYRe(xjRe, xjIm, TijRe, TijIm);
                final double xjTijIm = Numerics.complexXYIm(xjRe, xjIm, TijRe, TijIm);
                
                runningRe -= xjTijRe;
                runningIm -= xjTijIm;
            }
            
            final double TiiRe = TRei[i];
            final double TiiIm = TImi[i];
            
            // compute (a + b i) / (c + d i) where 
            // a + b i = runningRe + runningIm i
            // c + d i =     TiiRe +     TiiIm i
            Numerics.complexDivide(runningRe, runningIm, TiiRe, TiiIm, divisionSave);
            
            XColRe[i] = divisionSave[0];
            XColIm[i] = divisionSave[1];
        }
        
        XTRe[transpRowIdx] = XColRe;
        XTIm[transpRowIdx] = XColIm;
    }
    
    /** Compute the condition number of the given {@link RealMatrix} under the
     * {@link RealMatrix#getFrobeniusNorm() Frobenius norm}.<br><br>
     * 
     * In MATLAB, this is computed using {@code cond(A, 'fro')}
     * 
     * @param A the {@link RealMatrix}
     * @return the condition number */
    public double cond(final RealMatrix A) {
        final RealMatrix invOrPinv = A.isSquare() ? inv(A) : pinv(A);
        return invOrPinv.getFrobeniusNorm() * A.getFrobeniusNorm();
    }
    
    /** Compute the condition number of the given {@link Complex} {@link FieldMatrix} under the
     * {@link ComplexMatrix#getFrobeniusNorm() Frobenius norm}.<br><br>
     * 
     * In MATLAB, this is computed using {@code cond(A, 'fro')}
     * 
     * @param fA the {@link Complex} {@link FieldMatrix}
     * @return the condition number */
    public double cond(final FieldMatrix<Complex> fA) {
        final ComplexMatrix A         = ComplexMatrix.of(fA);
        final ComplexMatrix invOrPinv = A.isSquare() ? inv(A) : pinv(A);
        return invOrPinv.getFrobeniusNorm() * A.getFrobeniusNorm();
    }
    
    /** Compute the inverse of {@code A}, if possible, by solving for {@code X} that satisfies {@code AX = I} where
     * {@code I} is the identity matrix.
     * 
     * @param A the <em>square</em> {@link Matrix} to invert
     * @return A<sup>-1</sup>  */
    @BeCareful(reason = INV_WARNING)
    public Matrix inv(final RealMatrix A) {
        Numerics.requireSquare(A);
        return solve(A, Matrix.identity(A.getColumnDimension()));
    }
    
    /** Compute the Moore-Penrose pseudoinverse of A, denoted A<sup>+</sup>, A an element of R<sup> m x n</sup><br>
     * <br>
     * 
     * See {@link #pinv(ComplexMatrix)} for the mathematical details
     * 
     * @param AParam the {@link RealMatrix} for which we want the pseudoinverse
     * @return the pseudoinverse of A */
    @BeCareful(reason = INV_WARNING)
    public Matrix pinv(final RealMatrix AParam) {
        final Matrix A  = Matrix.of(AParam); // m x n
        final Matrix AT = A.transpose();     // n x m
        
        if (A.isWide()) {
            final RealQRDecomposition QR = RealQRDecomposition.of(AT); // QRdecomp. of the transpose, rather than A
            
            final Matrix R  = QR.getReducedR();
            final Matrix RT = R.transpose();
            
            final Matrix RpinvA = this.solveSquareForwardSubstitution(RT, A); // RHS here is A, rather than A^T
            final Matrix pinvA  = this.solveSquareBackSubstitution(R, RpinvA);
            
            return pinvA.transpose(); // return the transpose
            
            // alternatively, we could do the following, but it requires computing at least one additional transpose
            // return pinv(A.transpose()).transpose();
        }
        
        final RealQRDecomposition QR = RealQRDecomposition.of(A);
        
        final Matrix R  = QR.getReducedR(); // getReducedR is the difference from the square case (getR)
        final Matrix RT = R.transpose();
        
        final Matrix RpinvA = this.solveSquareForwardSubstitution(RT, AT);
        final Matrix pinvA  = this.solveSquareBackSubstitution(R, RpinvA);
        
        return pinvA;
    }
    
    /** Compute the inverse of {@code A}, if possible, by solving for {@code X} that satisfies {@code AX = I} where
     * {@code I} is the identity matrix.
     * 
     * @param A the <em>square</em> {@link ComplexMatrix} to invert
     * @return A<sup>-1</sup> */
    @BeCareful(reason = INV_WARNING)
    public ComplexMatrix inv(final ComplexMatrix A) {
        Numerics.requireSquare(A);
        return solve(A, ComplexMatrix.identity(A.getColumnDimension()));
    }
    
    /** Compute the Moore-Penrose pseudoinverse of A, denoted A<sup>+</sup>, A an element of R<sup> m x n</sup><br>
     * <br>
     * 
     * Note that in the {@link RealMatrix real} case, all conjugate transposes <sup>H</sup> are simply transposes
     * <sup>T</sup><br>
     * <br>
     * 
     * The math and code in each case looks very similar, and we could collapse them with clever use of ternary
     * expressions, but it's much more readable to keep the cases separate as they are. Code comments highlight the
     * differences.<br>
     * <br>
     * 
     * There are three cases to cover:
     * <ol>
     * <li>A {@link ComplexMatrix#isTall()} (m {@literal >} n):<br>
     * Let <code>A = QR</code>, a {@link QRDecomposition} of A<br>
     * In the tall and square cases, the pseudoinverse is A<sup>+</sup> = (A<sup>H</sup>A)<sup>-1</sup>A<sup>H</sup><br>
     * <br>
     * So,
     * 
     * <pre>
     * <code>
     *    A<sup>+</sup> = (A<sup>H</sup>A)<sup>-1</sup>A<sup>H</sup>
     * A<sup>H</sup>AA<sup>+</sup> = A<sup>H</sup>
     * 
     * Using A = QR and A<sup>H</sup> = R<sup>H</sup>Q<sup>H</sup>
     * 
     * A<sup>H</sup>AA<sup>+</sup> = R<sup>H</sup>Q<sup>H</sup>QRA<sup>+</sup>
     * 
     * Q is unitary s.t. Q<sup>-1</sup> = Q<sup>H</sup>, so
     * 
     * A<sup>H</sup>AA<sup>+</sup> = R<sup>H</sup>RA<sup>+</sup>
     * 
     * and we have
     * 
     * R<sup>H</sup>RA<sup>+</sup> = A<sup>H</sup>
     * 
     * From here, we use forward-substitution to solve for the matrix X that satisfies
     * 
     * R<sup>H</sup>X = A<sup>H</sup>
     * 
     * which yields
     * 
     * X = RA<sup>+</sup>
     * 
     * and then we use back-substitution to solve for the matrix Y that satisfies
     * 
     * RY = X = RA<sup>+</sup>
     * 
     * which yields
     * 
     * Y = A<sup>+</sup>
     * 
     * which is our desired result</code>
     * </pre>
     * 
     * </li>
     * 
     * <li>A {@link ComplexMatrix#isSquare()} (m == n)<br>
     * The math in this case is identical to that of the {@link ComplexMatrix#isTall() tall} case. In the tall case, we
     * need the {@link QRDecomposition#getReducedR() reduced} form of {@link QRDecomposition#getR() R}, but in the
     * square case the reduced form is identical to the standard full form, so collapsing the two is trivial.
     * 
     * <br>
     * </li>
     * 
     * <li>A {@link ComplexMatrix#isWide()} (m {@literal <} n)<br>
     * Very similar <em>looking</em> to the tall case, but it's subtly different. <br>
     * First, consider the matrix B = A<sup>H</sup>, and use B through the whole procedure (B = QR, etc.) until...
     * 
     * <pre>
     *  <code>
     * R<sup>H</sup>RB<sup>+</sup> = B<sup>H</sup>
     * 
     * replace B with A<sup>H</sup>:
     * 
     * R<sup>H</sup>R(A<sup>H</sup>)<sup>+</sup> = (A<sup>H</sup>)<sup>H</sup>
     * 
     * R<sup>H</sup>R(A<sup>H</sup>)<sup>+</sup> = A
     * 
     * Now, continue with the last two steps from the tall case and solve for
     * 
     * Y = (A<sup>H</sup>)<sup>+</sup>
     * 
     * then using the fact that conjugate transposition operator commutes with the pseudoinverse operator, we have
     * 
     * Y = (A<sup>+</sup>)<sup>H</sup>
     * 
     * so we simply compute the conjugate transpose of this result and return</code>
     * </pre>
     * 
     * </li>
     * </ol>
     * 
     * @param A the {@link RealMatrix} for which we want the pseudoinverse
     * @return the pseudoinverse of A */
    @BeCareful(reason = INV_WARNING)
    public ComplexMatrix pinv(final ComplexMatrix A) {
        final ComplexMatrix AT = A.conjugateTranspose();   // n x m
        
        if (A.isWide()) {
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(AT); // QRdecomp. A^H, rather than A
            
            final ComplexMatrix R  = QR.getReducedR();
            final ComplexMatrix RT = R.conjugateTranspose();
            
            final ComplexMatrix RpinvA = this.solveSquareForwardSubstitution(RT, A); // RHS here is A, rather than A^H
            final ComplexMatrix pinvA  = this.solveSquareBackSubstitution(R, RpinvA);
            
            return pinvA.conjugateTranspose(); // return the conjugate transpose
            
            // alternatively, we could do the following, but it requires computing at least one additional transpose
            // return pinv(A.conjugateTranspose()).conjugateTranspose();
        }
        
        final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
        
        final ComplexMatrix R  = QR.getReducedR(); // if the original matrix was square, this will be getR
        final ComplexMatrix RT = R.conjugateTranspose();
        
        final ComplexMatrix RpinvA = this.solveSquareForwardSubstitution(RT, AT);
        final ComplexMatrix  pinvA = this.solveSquareBackSubstitution(R, RpinvA);
        
        return pinvA;
    }
}
