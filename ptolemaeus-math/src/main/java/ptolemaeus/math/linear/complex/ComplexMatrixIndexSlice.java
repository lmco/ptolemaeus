/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import ptolemaeus.commons.Validation;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** A {@link ComplexMatrixIndexSlice} represents a mutable contiguous sub-matrix of a {@link ComplexMatrix}. Slices are
 * read-through and write-back; changes to the slice are reflected in the underlying {@link ComplexMatrix}, and
 * vice-versa.
 * 
 * @author Peter Davis, peter.m.davis@lmco.com */
public class ComplexMatrixIndexSlice extends AbstractComplexMatrixSlice {
    
    /** The row index of the source matrix at which this slice starts */
    private final int rowStart;
    
    /** The column index of the source matrix at which this slice starts */
    private final int colStart;
    
    /**
     * Construct a {@link ComplexMatrixIndexSlice} from the provided source matrix and row and column starts and ends, 
     * inclusive.
     * 
     * This will be a contiguous rectangular slice, or "window", of the source matrix.
     * @param sourceMatrix the source matrix that this slice is referencing.
     * @param rowStart the first row index of the source matrix to reference.
     * @param rowEnd the last row index of the source matrix to reference.
     * @param colStart the first column index of the source matrix to reference.
     * @param colEnd the last column index of the source matrix to reference.
     */
    @SuppressFBWarnings(value = "EI2", justification = "sourceMatrix is intentionally mutable")
    public ComplexMatrixIndexSlice(final ComplexMatrix sourceMatrix, final int rowStart, final int rowEnd, 
                                                  final int colStart, final int colEnd) {
        super(sourceMatrix, rowEnd - rowStart + 1, colEnd - colStart + 1);
        Validation.requireGreaterThanEqualTo(rowStart, 0, "rowStart");
        Validation.requireGreaterThanEqualTo(rowEnd, rowStart, "rowEnd");
        Validation.requireGreaterThanEqualTo(colStart, 0, "colStart");
        Validation.requireGreaterThanEqualTo(colEnd, colStart, "colEnd");
        
        this.rowStart = rowStart;
        this.colStart = colStart;
    }

    @Override
    public int getSourceRowIndex(final int sliceRow) {
        return this.rowStart + sliceRow;
    }

    @Override
    public int getSourceColIndex(final int sliceCol) {
        return this.colStart + sliceCol;
    }
}
