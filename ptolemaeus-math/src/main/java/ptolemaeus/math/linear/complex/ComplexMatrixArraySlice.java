/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** A {@link ComplexMatrixArraySlice} represents a mutable sub-matrix of a selection of rows and columns of a
 * {@link ComplexMatrix}. Slices are read-through and write-back; changes to the slice are reflected in the underlying
 * {@link ComplexMatrix}, and vice-versa.
 * 
 * @author Peter Davis, peter.m.davis@lmco.com */
public class ComplexMatrixArraySlice extends AbstractComplexMatrixSlice {

    /** the indices of the rows of the source matrix that this {@link ComplexMatrixArraySlice} references */
    private final int[] rowIdxs;
    
    /** the indices of the columns of the source matrix that this {@link ComplexMatrixArraySlice} references */
    private final int[] colIdxs;
    
    /**
     * Construct a {@link ComplexMatrixArraySlice} from the provided source matrix, row indices, and column indices. 
     * 
     * The order the row and column indices are provided in will be the order the {@link ComplexMatrixArraySlice}
     * indexes them. For example, if one creates 
     * {@code ComplexMatrixSlice slice = new ComplexMatrixSlice(mat, new int[]{4, 9, 6}, new int[]{0, 1, 2})},
     * then the second row of {@code slice} will be the 10th row of {@code mat}. 
     * @param sourceMatrix the source matrix that this slice is referencing.
     * @param rowIdxs the row indices to use for the slice. 
     * @param colIdxs the column indices to use for the slice.
     */
    @SuppressFBWarnings(value = "EI2", justification = "sourceMatrix is intentionally mutable")
    public ComplexMatrixArraySlice(final ComplexMatrix sourceMatrix, final int[] rowIdxs, final int[] colIdxs) {
        super(sourceMatrix, rowIdxs.length, colIdxs.length);
        /* we clone these index arrays so that changes to the argument arrays 
         * don't change the rows and columns we're referencing */
        
        this.rowIdxs = rowIdxs.clone();
        this.colIdxs = colIdxs.clone();
    }
    
    @Override
    public int getSourceRowIndex(final int sliceRow) {
        return this.rowIdxs[sliceRow];
    }
    
    @Override
    public int getSourceColIndex(final int sliceCol) {
        return this.colIdxs[sliceCol];
    }
}
