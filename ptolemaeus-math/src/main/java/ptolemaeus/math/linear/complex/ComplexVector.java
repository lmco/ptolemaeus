/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.hipparchus.Field;
import org.hipparchus.complex.Complex;
import org.hipparchus.complex.ComplexField;
import org.hipparchus.exception.LocalizedCoreFormats;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.exception.NullArgumentException;
import org.hipparchus.linear.ArrayFieldVector;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import ptolemaeus.commons.BeCareful;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.NVector;

/** A {@link ComplexVector} is a vector of {@link Complex} values
 * <p>
 * Internally, each {@link ComplexVector} is composed of two-1D {@code double[]}s {@code this.re} and
 * {@code this.im}, where {@code this.re} holds the real parts of the {@link Complex} values of {@code this},
 * and {@code this.im} holds the imaginary parts. Note that {@link ComplexVector} doesn't store 
 * any {@link Complex} values directly.
 * </p>
 * 
 * <p>
 * Additionally, the input for instantiating a {@link ComplexVector} can be one 2D double array 
 * {@code double[2][]} where the first array {@code double[0][]} is read as the real part {@code this.re} and the 
 * second array {@code double[1][]} is read as the imaginary part {@code this.im}. <br>
 * <br>
 * Note that
 * {@link #getDataRef()} returns this type of 2D array {@code double[2][]} of {@code this.re, this.im} 
 * whereas 
 * {@link #getData()} <strong>creates</strong> a new 1D array of {@link Complex} values from {@code this.re, this.im}.
 * </p>
 * 
 * <br>
 * <b> Example Constructions: </b>
 * <br>
 * 
 * The following three are equivalent:
 *
 * <pre>
 *  final ComplexVector V = new ComplexVector(new double[][] { { 1, 2, 3, 4, 5, 6 }, { 12, 11, 10, 9, 8, 7 } } );
 * </pre>
 * 
 * <pre>
 *  final ComplexVector V = new ComplexVector(new double[] {  1,  2,  3, 4, 5, 6 },
 *                                            new double[] { 12, 11, 10, 9, 8, 7 } );
 * </pre>
 * 
 * <pre>
 *  final ComplexVector V = new ComplexVector(new Complex(1, 12),
 *                                            new Complex(2, 11),
 *                                            new Complex(3, 10),
 *                                            new Complex(4,  9),
 *                                            new Complex(5,  8),
 *                                            new Complex(6,  7));
 * </pre>
 *  
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * @author Catherine Doud, catherine.a.doud@lmco.com */
public class ComplexVector implements FieldVector<Complex> {

    /** Dimension of the instance {@link ComplexVector}
     *  keeping in mind this vector is an element of <code>C<sup>n</sup></code>, where {@code n == length} */
    private final int length;
    
    /** Real part or {@code this} {@link ComplexVector} */
    private final double[] re;
    
    /** Imaginary part of {@code this} {@link ComplexVector} */
    private final double[] im;

    /** Constructor
     * 
     * @param dRe the {@code double[]} real entries of this {@link ComplexVector}. 
     *     Cloned iff {@code clone == true}.
     * @param dIm the {@code double[]} imaginary entries of this {@link ComplexVector}. 
     *     Cloned iff {@code clone == true}.
     * @param clone {@code true} to deeply clone {@code entries}, {@code false} to simple reference it. */
    @SuppressFBWarnings("EI2")
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexVector(final double[] dRe, final double[] dIm, final boolean clone) {
        checkSameDoubleDimension(dRe, dIm);

        this.length = dRe.length;

        if (!clone) {
            this.re = dRe;
            this.im = dIm;
        }
        else {
            this.re = new double[length];
            this.im = new double[length];
    
            for (int i = 0; i < length; i++) {
                this.re[i] = dRe[i];
                this.im[i] = dIm[i];
            }
        }
    }

    /** Constructor
     * <br><br>
     * Construct a {@link ComplexVector} from a real and an imaginary {@code double[]}, cloning the source arrays.
     * 
     * @param dRe the real parts of the entries
     * @param dIm the imaginary parts of the entries */
    public ComplexVector(final double[] dRe, final double[] dIm) {
        this(dRe, dIm, true);
    }

    /** Constructor
     * <br><br>
     * Construct a {@link ComplexVector} from a {@link FieldVector}, cloning the source array, if applicable.
     * 
     * @param vector the {@link Complex} {@link FieldVector} we want to copy into a new {@link ComplexVector} */
    public ComplexVector(final FieldVector<Complex> vector) {
        this(vector, true);
    }

    /** Constructor to construct an empty instance.  All entries will be zero until set otherwise.
     * 
     * @param length the vector length
     * 
     * @apiNote only for use when we can create a new {@link ComplexVector} element-wise and only want to loop over the
     *          elements once */
    public ComplexVector(final int length) {
        if (length <= 0) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.ARRAY_ZERO_LENGTH_OR_NULL_NOT_ALLOWED, length);
        }

        this.length = length;
        this.re = new double[length];
        this.im = new double[length];
    }

    /** Constructor for efficiently creating a {@link ComplexVector} from a {@link FieldVector}
     * 
     * @param vector the {@link FieldVector} we want to copy
     * @param clone {@code true} to always clone the {@code getDataRef()} data arrays, {@code false}
     *        to skip cloning if possible. We can skip cloning when {@code vector instanceof ComplexVector}. Otherwise,
     *        we must always clone the data arrays. This is due to how the real and imaginary parts of the numbers are
     *        stored separately.
     *        
     * @apiNote Users must be careful to not misuse the {@link FieldVector} that was passed in, else changes to its data
     *          array will be reflected in this {@link ComplexVector} instance. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexVector(final FieldVector<Complex> vector, final boolean clone) {
        this.length = vector.getDimension();
        
        if (!clone && vector instanceof ComplexVector toCopy) {
                this.re = toCopy.re;
                this.im = toCopy.im;

        }
        else {
            this.re = new double[length];
            this.im = new double[length];
            
            if (vector instanceof ComplexVector toCopy) {
                for (int i = 0; i < length; i++) {
                    this.re[i] = toCopy.re[i];
                    this.im[i] = toCopy.im[i];
                }
            }
            else {
                for (int i = 0; i < length; i++) {
                        final Complex vi = vector.getEntry(i);
                        this.re[i] = vi.getReal();
                        this.im[i] = vi.getImaginary();                            
                }
            }
        }
    }

    /** If {@code v} is already a {@link ComplexVector}, simply cast it to {@link ComplexVector} and return.<br>
     * Otherwise, efficiently create a new {@link ComplexVector} that wraps the data from the one given.<br>
     * Note: <em>we avoid cloning if we can</em>; i.e.
     * <ul>
     * <li>if {@code v} is already a {@link ComplexVector}, we return it as-is</li>
     * <li>if {@code v} is an {@link ArrayFieldVector}{@code <Complex>}, wrap a <em>reference</em> to
     * the {@code Complex[]} data in a {@link ComplexVector}; i.e., we don't clone the array.</li>
     * <li>otherwise, we must clone the result of {@link FieldVector#toArray()}</li>
     * </ul>
     * 
     * @param v the {@link FieldVector} that we want to wrap in/cast to a {@link ComplexVector}
     * @return the {@link ComplexVector} */
    public static ComplexVector of(final FieldVector<Complex> v) {
        requireNonNull(v, "The vector v must not be null");
        if (v instanceof ComplexVector x) {
            return x;
        }
        if (v instanceof ArrayFieldVector<Complex> x) {
            return new ComplexVector(x, false);
        }
        Validation.requireEqualsTo(v.getField(), ComplexField.getInstance(),
                "can only create a ComplexVector from a FieldVector of the Complex field");
        
        return new ComplexVector(v);
    }

    /** Constructor
     * 
     * @param realVector the {@link RealVector} whose values we want to copy into this {@link ComplexVector} 
     * @param clone {@code true} to always clone the {@code getData()} data array, {@code false} to skip cloning */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexVector(final RealVector realVector, final boolean clone) {
        this(NVector.of(realVector).getDataRef(), new double[realVector.getDimension()], clone);
    }

    
    /** Constructor
     * <br><br>
     * Construct a {@link ComplexVector} from a {@link RealVector}, cloning the source array, if applicable.
     * 
     * @param realVector the {@link RealVector} whose values we want to copy into this {@link ComplexVector} */
    public ComplexVector(final RealVector realVector) {
        this(NVector.of(realVector).getDataRef(), new double[realVector.getDimension()], true);
    }
    
    /** Constructor
     * 
     * @param realVector the real array whose values we want to copy into this {@link ComplexVector} 
     * @param clone {@code true} to always clone the {@code getData()} data array, {@code false} to skip cloning */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexVector(final double[] realVector, final boolean clone) {
        this(realVector, new double[realVector.length], clone);
    }
    
    /** Constructor.  The {@code varargs} array is always cloned.
     * 
     * @param realVector the real array whose values we want to copy into this {@link ComplexVector} */
    public ComplexVector(final double... realVector) {
        this(realVector, new double[realVector.length], true);
    }
    
    /** Constructor
     * 
     * @param entries the {@link Complex} entries of vector */
    public ComplexVector(final Complex... entries) {
        this(asReImArray(entries));
    }

    /** Constructor
     * <br><br>
     * If {@code clone} not specified, assumed to be true
     * 
     * @param reIm the real and imaginary parts of the vector */
    public ComplexVector(final double[][] reIm) {
        this(reIm[0], reIm[1]);
    }

    /** Constructor
     * 
     * @param reIm the real and imaginary parts of the vector 
     * @param clone clone {@code true} to always clone the {@code getDataRef()} data arrays, {@code false}
     *        to skip cloning if possible. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexVector(final double[][] reIm, final boolean clone) {
        this(reIm[0], reIm[1], clone);
    }
    
    /** @return a {@code Complex[]} representative of this {@link ComplexVector} */
    public Complex[] getData() {
        final int index = this.getDimension();
        final Complex[] data = new Complex[index];

        for (int i = 0; i < index; ++i) {
            data[i] = this.getEntry(i);
        }

        return data;
    } 

    /** Get a {@code double[][]} of references to the underlying data arrays.
     *
     * @return the 2-dimensional array of entries where {@code double[0][]} is {@code this.re}
     * and {@code double[1][]} is {@code this.im} */
    public double[][] getDataRef() {
        return new double[][] { this.re, this.im };
    }

    /** Split this {@link ComplexVector} into two {@code double[]}s that represent the real and imaginary parts of the
     * {@link Complex} entries of this {@link ComplexVector}. Note that this does not return the internal data arrays,
     * but copies of them.
     * 
     * @return a {@code double[][]} with two entries: the real and imaginary parts of this {@link ComplexVector} */
    public double[][] getCopyOfReImArrays() {
        final int n = this.getDimension();
        
        final double[] dRe = new double[n];
        final double[] dIm = new double[n];
        
        for (int i = 0; i < n; i++) {
            dRe[i] = this.re[i];
            dIm[i] = this.im[i];
        }
        
        return new double[][] { dRe, dIm };
    }

    /** Compute a 2D {@code (2 x N) double[][]} representation of this {@link ComplexVector}. The first and second
     * indices - {@code [0] and [1]} - respectively contain the real and imaginary parts of the {@link Complex} entries
     * of this vector.
     * 
     * @param v array of complex vectors to return as a {@code double[][]}
     * @return the new {@code (2 x N) double[][]} */
    public static double[][] asReImArray(final Complex[] v) {
        final int n = v.length;
        final double[][] answer = new double[2][n];
        
        for (int i = 0; i < n; i++) {
            answer[0][i] = v[i].getReal();
            answer[1][i] = v[i].getImaginary();
        }
        
        return answer;
    }
    
    /** Conjugate the {@link ComplexVector} 
     * 
     * @return the complex conjugate of this {@link ComplexVector} ; i.e., a {@link ComplexVector} whose elements are
     *         the {@link Complex#conjugate() complex conjugates} of the elements of this {@link ComplexVector} */
    public ComplexVector conjugate() {
        final double[] reH = new double[this.length];
        final double[] imH = new double[this.length]; // H denoting the conjugation
        
        for (int i = 0; i < this.length; i++) {
            reH[i] =  this.re[i];
            imH[i] = -this.im[i];
        }
        
        return new ComplexVector(reH, imH, false);
    }

    /** The {@link Stream} of all entries in this instance of {@link ComplexVector}
     * @return the {@link Stream} of {@link Complex} entries */
    public Stream<Complex> elementStream() {
        return IntStream.range(0, this.getLength()).mapToObj(this::getEntry);
    }
    
    /** If the {@link Complex} entries of this {@link ComplexVector} are all within {@code absEBETol}, then we return an
     * {@link Optional} {@link NVector} that's equal to the real part of this {@link ComplexVector}. Otherwise, we
     * return an empty {@link Optional}.
     * 
     * @param absEBETol the absolute element-by-element tolerance. Elements with imaginary part larger than this will be
     *        considered non-zero.
     * @return the {@link Optional} that may hold an {@link NVector} */
    public Optional<NVector> asReal(final double absEBETol) {
        final double[] dRe    = new double[this.getLength()];
        final double[][] reIm = this.getDataRef();
        
        for (int i = 0; i < this.getLength(); i++) {
            if (!Precision.equals(reIm[1][i], 0.0, absEBETol)) {
                return Optional.empty();
            }
            
            dRe[i] = this.re[i];
        }
        
        return Optional.of(new NVector(dRe));
    }
    
    /** Check to see if all values within this {@link ComplexVector} are real (i.e. all elements have im == 0)
     * @return {@code true} if each entry of this {@link ComplexVector} is real, 
     *         {@code false} otherwise. */
    public boolean isReal() {
        for (double d : im) {
            if (d != 0) {
                return false;
            }
        }

        return true;
    }
    
    /** Check to see if all values within this {@link ComplexVector} are zero
     * @return {@code true} if each entry of this {@link ComplexVector} is zero, 
     *         {@code false} otherwise. */
    public boolean isZero() {
        for (int i = 0; i < this.getLength(); ++i) {
            if ((im[i] != 0) || re[i] != 0) {
                return false;
            }
        }

        return true;
    }
    
    /** Check to see if any values within this {@link ComplexVector} are {@link Double#isNaN()}
     * @return {@code true} if this {@link ComplexVector} has any components that are NaN,
     *         {@code false} otherwise */
    public boolean isNaN() {
        for (int i = 0; i < this.getLength(); ++i) {
            if (Double.isNaN(im[i]) || Double.isNaN(re[i])) {
                return true;
            }
        }

        return false;    
    }
    
    /** Check to see if any values within this {@link ComplexVector} are {@link Double#isInfinite()}
     * @return {@code true} if this {@link ComplexVector} has any components that are infinite,
     *         {@code false} otherwise */
    public boolean isInfinite() {
        for (int i = 0; i < this.getLength(); ++i) {
            if (Double.isInfinite(im[i]) || Double.isInfinite(re[i])) {
                return true;
            }
        }

        return false;    
    }
    
    /** Check to see if all values within this {@link ComplexVector} are {@link #isFinite()}
     * @return {@code true} if this {@link ComplexVector} has only components that are strictly finite
    *               (neither infinite nor NaN), 
     *         {@code false} otherwise */
    public boolean isFinite() {
    for (int i = 0; i < this.getLength(); ++i) {
        if (!Double.isFinite(im[i]) || !Double.isFinite(re[i])) {
            return false;
        }
    }

        return true;
    }

    /** Compute and return the norm of this {@link ComplexVector}. This definition of {@link #norm()} is slightly in
     * conflict with {@link Complex#norm()}. {@link Complex#norm()} computes the square-root of the complex value
     * multiplied by its complex conjugate, which is more in line with {@link #hermitianNorm()}.
     * 
     * <br>
     * <br>
     * 
     * This definition uses the standard definition of the inner-product; i.e.,
     * 
     * <pre>
     * let v = (z1, z2, z3, ...), z_i complex, then v.v (v dot v) is z1*z1 + z2*z2 + ... = a complex value
     * </pre>
     * 
     * @return the norm of this {@link ComplexVector}
     * 
     * @see #normSq()
     * @see #hermitianNorm()
     * @see #hermitianNormSq()
     * @see #dotProduct(FieldVector) */
    public double[] norm() {
        double[] normSq = this.normSq();
        double real = normSq[0];
        double imaginary = normSq[1];

        Numerics.complexSqrt(real, imaginary, normSq); // overwrite normSq
        return normSq;

    }
    
    /** Compute and return the square of the norm of this {@link ComplexVector}. This definition of 
     * {@link ComplexVector#norm()} is slightly in conflict with {@link Complex#norm()}. {@link Complex#norm()} 
     * computed square-root of the complex value multiplied by its complex conjugate, 
     * which is more in line with {@link #hermitianNorm()}.
     * 
     * @return the square of the norm of this {@link ComplexVector}
     * 
     * @see #norm()
     * @see #hermitianNorm()
     * @see #hermitianNormSq()
     * @see #dotProduct(FieldVector) */
    public double[] normSq() {
        double dRe = 0;
        double dIm = 0;

        for (int i = 0; i < length; ++i) {
            double a = this.re[i];
            double b = this.im[i];
            dRe += Numerics.complexXYRe(a, b, a, b);
            dIm += Numerics.complexXYIm(a, b, a, b);
        }

        return new double[] { dRe, dIm };
    }
    
    /** Compute and return the Hermitian norm of this {@link ComplexVector}
     * 
     * <br>
     * <br>
     * 
     * This definition uses the {@link #hermitianInnerProduct(FieldVector) Hermitian inner-product}; i.e.,
     * 
     * <pre>
     * let v = (z1, z2, z3, ...), z_i complex,
     *     then v hDot v (v Hermitian dot v) is z1*conj(z1) + z2*conj(z2) + ... = a real value
     * </pre>
     * 
     * @return the Hermitian norm of this {@link ComplexVector}
     * 
     * @see #norm()
     * @see #normSq()
     * @see #hermitianNormSq()
     * @see #hermitianInnerProduct(FieldVector) */
    public double hermitianNorm() {
        return FastMath.sqrt(this.hermitianNormSq());
    }
    
    /** Compute and return the square of the Hermitian norm of this {@link ComplexVector}
     * 
     * @return the square of the Hermitian norm of this {@link ComplexVector}
     * 
     * @see #norm()
     * @see #normSq()
     * @see #hermitianNorm()
     * @see #hermitianInnerProduct(FieldVector) */
    public double hermitianNormSq() {
        double sumSq = 0.0;
        for (int i = 0; i < this.length; i++) {
            sumSq += Numerics.complexNormSq(this.re[i], this.im[i]);
        }
        
        return sumSq;
    }
    
    /** Compute and return the Hermitian inner-product (dot product) of this {@link ComplexVector} with a
     * {@link Complex} {@link FieldVector}; i.e., the inner-product of {@code this} with the complex conjugate of the
     * argument.
     * 
     * <br>
     * <br>
     * 
     * The Hermitian inner-product is defined as:
     * 
     * <br>
     * <br>
     * 
     * <pre>
     * let v1, v2 be vectors over a Complex field,
     *     then v1 hDot v2 (v1 Hermitian dot v2) is v1_1*conj(v2_1) + v1_2*conj(v2_2) + ... = a real value
     * 
     * where vi_j is the j-th element of the i-th vector, and conj(v) is the takes the conjugate
     * 
     * </pre>
     * 
     * @param v the {@link Complex} {@link FieldVector} with which we want to compute the Hermitian inner-product
     * @return the Hermitian inner-product (dot product) of this {@link ComplexVector} with a {@link Complex}
     *         {@link FieldVector} */
    public Complex hermitianInnerProduct(final FieldVector<Complex> v) {
        checkVectorDimensions(this, v);
        final double[][] vReIm = of(v).getDataRef();
        final double[]   vRe = vReIm[0];
        final double[]   vIm = vReIm[1];

        double dotRe = 0.0;
        double dotIm = 0.0;

        for (int i = 0; i < this.length; i++) {
            final double a =  this.re[i];
            final double b =  this.im[i];
            final double c =  vRe[i];
            final double d = -vIm[i]; // negate for conjugate

            dotRe += Numerics.complexXYRe(a, b, c, d);
            dotIm += Numerics.complexXYIm(a, b, c, d);
        }
        return new Complex(dotRe, dotIm);
    }
    
    /** Compute and return sum of the {@link Numerics#complex1Norm(Complex) 1-norms} of the components of {@code this}
     * {@link ComplexVector}. This is simply the sum of the absolute values of the real and imaginary parts
     * of each of the components of this vector.
     * 
     * @return the sum of the sum of the {@link Numerics#complex1Norm(Complex) 1-norms} of the components of
     *         {@code this} {@link ComplexVector} */
    public double sumComplex1Norm() {
        double sum = 0.0;
        for (int i = 0; i < this.getDimension(); i++) {
            sum += Numerics.complex1Norm(this.re[i], this.im[i]);
        }
        
        return sum;
    }
    
    /** Check to ensure {@link FieldVector} {@code v} has same length (dimension) as {@link FieldVector} {@code cV}.
     * @param cV the {@link FieldVector} with desired length
     * @param v the {@link FieldVector} we want to check the length of */
    void checkVectorDimensions(final FieldVector<Complex> cV, final FieldVector<Complex> v) {
        final int l = v.getDimension();
        if (cV.getDimension() != l) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH, l);
        }
    }
    
    /** Check if an index is valid for {@code this} {@link ComplexVector}.
     *
     * @param index Index to check.
     * @exception MathIllegalArgumentException if the index is not valid. */
    void checkIndex(final int index) throws MathIllegalArgumentException {
        if (index < 0 || index >= this.length) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.INDEX,
                                          index, 0, this.length - 1);
        }
    }

    /** Check if {@code double[]}s have the same length.
     *
     * @param left Left hand side vector.
     * @param right Right hand side vector.
     * @throws MathIllegalArgumentException if doubles don't have the same length */
    static void checkSameDoubleDimension(final double[] left, final double[] right)
        throws MathIllegalArgumentException {
        if (left.length != right.length) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH,
                                                   left.length, right.length);
        }
    }

    
    /** Compute and return the <i>Hermitian angle</i> &theta;<sub>H</sub>between {@code this} and {@code v2} (radians)
     * 
     * Per <a href= "https://link.springer.com/article/10.1023/A:1012692601098"> Angles in Complex Vector Spaces</a>,
     * 
     * <br>
     * <br>
     * 
     * As in any real vector space the cosine of the (Hermitian) angle between two vectors a, b ∈ V<sub>C</sub> can be
     * defined to be the ratio between the length of the orthogonal projection (with respect to the Hermitian product)
     * of, say, the vector a onto the vector b to the length of the vector a itself.
     * 
     * <br>
     * 
     * @param v2 the vector between which we want to determine an angle
     * @return the <i>Hermitian angle</i> &theta;<sub>H</sub>between {@code this} and {@code v2} (radians)
     * 
     * @see #cosHermitianAngleTo(ComplexVector) */
    public double hermitianAngleTo(final ComplexVector v2) {
        return FastMath.acos(this.cosHermitianAngleTo(v2));
    }
    
    /** Compute and return {@code cos(}&theta;<sub>H</sub>{@code )}, where &theta;<sub>H</sub> is the <i>Hermitian
     * angle</i> between {@code this} and {@code v2}<br><br>
     * 
     * 
     * 
     * <br><br>
     * 
     * Per <a href= "https://link.springer.com/article/10.1023/A:1012692601098"> Angles in Complex Vector Spaces</a>,
     * 
     * <br>
     * <br>
     * 
     * As in any real vector space the cosine of the (Hermitian) angle between two vectors a, b ∈ V<sub>C</sub> can be
     * defined to be the ratio between the length of the orthogonal projection (with respect to the Hermitian product)
     * of, say, the vector a onto the vector b to the length of the vector a itself.<br>
     * 
     * @param v we want to determine an angle between {@code this} and {@code v}
     * @return {@code cos(}&theta;<sub>H</sub>{@code )}, where &theta;<sub>H</sub> is the <i>Hermitian angle</i> between
     *         {@code this} and {@code v2}
     * 
     * @see #hermitianAngleTo(ComplexVector) */
    public double cosHermitianAngleTo(final ComplexVector v) {
        if (this.isZero() || v.isZero()) {
            throw new NumericalMethodsException("Cannot compute the angle between a vector and the zero vector");
        }
        
        final double hNormADotB = this.hermitianInnerProduct(v).norm();
        final double normAnormB = this.hermitianNorm() * v.hermitianNorm();
        final double answer     = hNormADotB / normAnormB;
        
        return FastMath.max(-1.0, FastMath.min(1.0, answer)); // bind to [-1.0, 1.0] to handle numerical imprecision
    }
    
    /** @return this {@link ComplexVector}, normalized by its {@link #hermitianNorm() Hermitian norm} */
    public ComplexVector normalizedHermitian() {
        return this.dividedBy(this.hermitianNorm());
    }
    
    /** Add another {@link ComplexVector} to {@code this}
     * 
     * @param v the {@link ComplexVector} to add
     * @return {@code this + v}
     * 
     * @see #add(ComplexVector) */
    public ComplexVector plus(final FieldVector<Complex> v) {
        return this.add(ComplexVector.of(v));
    }
    
    /** Subtract another {@link ComplexVector} from {@code this}
     * 
     * @param v the {@link ComplexVector} to subtract
     * @return {@code this - v}
     * 
     * @see #subtract(FieldVector) */
    public ComplexVector minus(final FieldVector<Complex> v) {
        return this.subtract(ComplexVector.of(v));
    }
    
    /** Multiply {@code this} by some scalar {@code z}
     * 
     * @param z the scalar multiple
     * @return {@code z * this}
     * 
     * @see #mapMultiply(Complex) */
    public ComplexVector times(final Complex z) {
        return this.mapMultiply(z.getReal(), z.getImaginary());
    }

    /** Multiply {@code this} by the given complex number {@code z} where 
     * {@code z = zRe + zIm i} 
     * 
     * @param zRe the real part of the scalar multiple
     * @param zIm the imaginary part of the scalar multiple
     * @return {@code z * this}
     * 
     * @see #mapMultiply(Complex) */
    public ComplexVector times(final double zRe, final double zIm) {
        return this.mapMultiply(zRe, zIm);
    }

    /** Multiply this by some scalar {@code d}
     * 
     * @param d the scalar multiple
     * @return {@code d * this}
     * 
     * @see #mapMultiply(Complex) */
    public ComplexVector times(final double d) {
        return this.mapMultiply(d);
    }
    
    /** @return a new {@link ComplexVector} whose elements are the negative of those of {@code this} */
    public ComplexVector negate() {
        return this.mapMultiply(-1);
    }

    /** Multiply individual entry of {@code this} by real number {@code d}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index (length)
     * @param d number to multiply entry by */
    public void multiplyEntry(final int index, final double d) {
        checkIndex(index);

        re[index] *= d;     
        im[index] *= d;
    }

    @Override
    public ComplexVector mapMultiply(final Complex z) {
        return this.copy().mapMultiplyToSelf(z.getReal(), z.getImaginary());
    }

    /** Map a multiplication operation to each entry in a <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param d real value to multiply all entries by
     * @return {@code this * d}. */
    public ComplexVector mapMultiply(final double d) {
        return this.copy().mapMultiplyToSelf(d);
    }

    /** Map a multiplication operation to each entry in a <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param zRe real part of the value to multiply all entries by
     * @param zIm imaginary part of the value to multiply all entries by
     * @return {@code this * (zRe + zIm i} */
    public ComplexVector mapMultiply(final double zRe, final double zIm) {
        return this.copy().mapMultiplyToSelf(zRe, zIm);
    }

    @Override
    public ComplexVector mapMultiplyToSelf(final Complex d) {
        return this.mapMultiplyToSelf(d.getReal(), d.getImaginary());
    }

    /** Map a multiplication operation to each entry of {@code this}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param zRe the real part of the value to multiply all entries by
     * @param zIm the imaginary part of the value to multiply all entries by
     * @return for convenience, return {@code this} */
    public ComplexVector mapMultiplyToSelf(final double zRe, final double zIm) {
        for (int i = 0; i < length; ++i) {
            multiplyEntry(i, zRe, zIm);
        }
        return this;
    }

    /** Map a multiplication operation to each entry of {@code this}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param d the real value to multiply all entries by
     * @return for convenience, return {@code this} */
    public ComplexVector mapMultiplyToSelf(final double d) {
        for (int i = 0; i < length; ++i) {
            this.re[i] *= d;
            this.im[i] *= d;
        }

        return this;
    }

    /** Multiply individual entry of {@code this} by complex number {@code d}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index (length)
     * @param z complex number to multiply entry by */
    public void multiplyEntry(final int index, final Complex z) {
        multiplyEntry(index, z.getReal(), z.getImaginary());
    }

    /** Multiply individual entry of {@code this} by complex number {@code z} where {@code z = zRe + zIm i}.
     * 
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index (length)
     * @param zRe real part of number to multiply entry by 
     * @param zIm imaginary part of number to multiply entry by */
    public void multiplyEntry(final int index, final double zRe, final double zIm) {
        checkIndex(index);

        final double a = this.re[index];
        final double b = this.im[index];
        final double c = zRe;
        final double d = zIm;

        this.re[index] = Numerics.complexXYRe(a, b, c, d); // computes a c - b d       
        this.im[index] = Numerics.complexXYIm(a, b, c, d); // computes a d + b c
    }
    
    /** Divide the elements of this {@link ComplexVector} by the given {@link Complex}
     * 
     * @param z the divisor
     * @return {@code (1 / z) * this}
     * 
     * @see #mapDivide(Complex) */
    public ComplexVector dividedBy(final Complex z) {
        return this.mapDivide(z.getReal(), z.getImaginary());
    }

    /** Divide the elements of this {@link ComplexVector} by the given complex number {@code z} where 
     * {@code z = zRe + zIm i} 
     * 
     * @param zRe the real part of the value to multiply all entries by
     * @param zIm the imaginary part of the value to multiply all entries by
     * @return {@code (1 / z) * this}
     * 
     * @see #mapDivide(Complex) */
    public ComplexVector dividedBy(final double zRe, final double zIm) {
        return this.mapDivide(zRe, zIm);
    }

    /** Divide the elements of this {@link ComplexVector} by the given real number {@code d}
     * 
     * @param d the value to multiply all entries by
     * @return {@code (1 / z) * this}
     * 
     * @see #mapDivide(Complex) */
    public ComplexVector dividedBy(final double d) {
        return dividedBy(d, 0);
    }

    @Override
    public ComplexVector mapDivide(final Complex d) {
        return this.copy().mapDivideToSelf(d.getReal(), d.getImaginary());
    }

    /** Map a division operation to each entry in a <strong>new</strong> instance of {@link ComplexVector}.
     * {@code z = zRe + zIm i} 
     * 
     * @param zRe real part of value
     * @param zIm imaginary part of value
     * @return {@code this / z} */
    public ComplexVector mapDivide(final double zRe, final double zIm) {
        return this.copy().mapDivideToSelf(zRe, zIm);
    }

    /** Map a division operation to each entry in a <strong>new</strong> instance of {@link ComplexVector}.
     * {@code z = d + 0i} 
     * 
     * @param d the scalar multiple
     * @return {@code this / d}
     * 
     * @see #mapDivide(Complex) */
    public ComplexVector mapDivide(final double d) {
        return this.mapDivide(d, 0);
    }

    @Override
    public ComplexVector mapDivideToSelf(final Complex z) {
        return this.mapDivideToSelf(z.getReal(), z.getImaginary());
    }

    /** Map a division operation to each entry of {@code this}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param zRe the real part of the value to divide all entries by
     * @param zIm the imaginary part of the value to divide all entries by
     * @return for convenience, return {@code this} */
    public ComplexVector mapDivideToSelf(final double zRe, final double zIm) {
        if (zRe == 0 && zIm == 0) {
            throw new IllegalArgumentException("Attempted to divide by zero");
        }

        final double[] saveTo = new double[2];
        for (int i = 0; i < this.getLength(); ++i) {
            final double a = this.re[i];
            final double b = this.im[i];        
            final double c = zRe;
            final double d = zIm;
            
            Numerics.complexDivide(a, b, c, d, saveTo); // computes (ac + bd)/(c^2 + d^2);
            
            this.re[i] = saveTo[0];
            this.im[i] = saveTo[1];
        }
        return this;
    }


    /** Divide individual entry of {@code this} by complex number {@code z} where {@code z = zRe + dIm i}.
     * 
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index (length) the entry to divide by designated {@code z}
     * @param z complex number to divide by
     */
    public void divideEntry(final int index, final Complex z) {
        divideEntry(index, z.getReal(), z.getImaginary());
    }

    /** Divide individual entry of {@code this} by complex number {@code z} where {@code z = zRe + dIm i}.
     * 
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index (length) the entry to divide by designated {@code z}
     * @param zRe real part of complex number to divide by
     * @param zIm imaginary part of complex number to divide by
     */
    public void divideEntry(final int index, final double zRe, final double zIm) {
        checkIndex(index);
        
        if (zRe == 0 && zIm == 0) {
            throw new IllegalArgumentException("Attempted to divide by zero");
        }

        final double a = re[index];
        final double b = im[index];        
        final double c = zRe;
        final double d = zIm;
        
        final double[] div = Numerics.complexDivide(a, b, c, d); // computes (ac + bd)/(c^2 + d^2);
        
        re[index] = div[0];
        im[index] = div[1];
    }

    /** Divide individual entry of {@code this} by real number {@code d}.
     * 
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index (length) the entry to divide by designated {@code factor}
     * @param d real number to divide by
     */
    public void divideEntry(final int index, final double d) {
        checkIndex(index);
        
        if (d == 0) {
            throw new IllegalArgumentException("Attempted to divide by zero");
        }

        re[index] /= d;
        im[index] /= d;
    }

    @Override
    public ComplexVector add(final FieldVector<Complex> v) {
        return this.add(ComplexVector.of(v));
    }

    /** Adds a {@link ComplexVector} to {@code this} in a <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param v {@link ComplexVector} to add to {@code this}
     * @return complex vector {@code this + v} */
    public ComplexVector add(final ComplexVector v) {
        checkVectorDimensions(this, v);
        final ComplexVector c = this.copy();
        final double[][] cReIm = v.getDataRef();
        final double[] cRe = cReIm[0];
        final double[] cIm = cReIm[1];
        for (int i = 0; i < this.getLength(); ++i) {
            c.addEntry(i, cRe[i], cIm[i]);
        }

        return c;
    }

    /** Adds a complex number to specified entry of {@code this}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index index of this {@link ComplexVector} to add {@code z} to
     * @param z {@link Complex} number to add into entry */
    public void addEntry(final int index, final Complex z) {
        addEntry(index, z.getReal(), z.getImaginary());
    }

    /** Adds a complex number represented as a {@code double[]} to specified entry.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index index of this {@link ComplexVector} to add {@code z} to
     * @param zRe the real portion of the number to add into entry
     * @param zIm the imaginary portion of the number to add into entry */
    public void addEntry(final int index, final double zRe, final double zIm) {
        checkIndex(index);

        re[index] += zRe;     
        im[index] += zIm;
    }

    @Override
    public ComplexVector mapAdd(final Complex z) {
        return mapAdd(z.getReal(), z.getImaginary());
    }

    /** Map an addition operation to each entry of {@code this} in a 
     * <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param zRe real value to be added to each entry
     * @param zIm imaginary value to be added to each entry
     * @return {@code this + z} where {@code z = zRe + zIm i}
     * @throws NullArgumentException if {@code z} is {@code null}. */
    public ComplexVector mapAdd(final double zRe, final double zIm) {
        return this.copy().mapAddToSelf(zRe, zIm);
    }

    /** Map an addition operation to each entry of {@code this} in a 
     * <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param d real value to be added to each entry
     * @return {@code this + z} where {@code z = d + 0 i}
     * @throws NullArgumentException if {@code z} is {@code null}. */
    public ComplexVector mapAdd(final double d) {
        return mapAdd(d, 0);
    }

    @Override
    public ComplexVector mapAddToSelf(final Complex z) {
        return mapAddToSelf(z.getReal(), z.getImaginary());
    }

    /** Map an addition operation to each entry of {@code this}.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param zRe real value to be added to each entry
     * @param zIm imaginary value to be added to each entry
     * @return {@code this + z} where {@code z = zRe + zIm i} */
    public ComplexVector mapAddToSelf(final double zRe, final double zIm) {
        for (int i = 0; i < length; ++i) {
            this.re[i] += zRe;
            this.im[i] += zIm;
        }
        return this;
    }
    
    @Override
    public ComplexVector subtract(final FieldVector<Complex> v) {
        return this.subtract(ComplexVector.of(v));
    }


    /** Subtracts a {@link ComplexVector} from {@code this}.
     * <p>This instance is <strong>not</strong> changed by this method.</p>
     * 
     * @param v index of this {@link ComplexVector} to subtract {@code z} from
     * @return complex vector {@code this + v} */
    public ComplexVector subtract(final ComplexVector v) {
        checkVectorDimensions(this, v);
        final ComplexVector c = this.copy();
        final double[][] vReIm = v.mapMultiply(-1).getDataRef();

        for (int i = 0; i < this.getLength(); ++i) {
            c.addEntry(i, vReIm[0][i], vReIm[1][i]);
        }

        return c;
    }

    @Override
    public ComplexVector mapSubtract(final Complex z) {
        return mapSubtract(z.getReal(), z.getImaginary());
    }

    /** Map a subtraction operation to each entry in a <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param zRe the real part of the value to multiply all entries by
     * @param zIm the imaginary part of the value to multiply all entries by     
     * @return {@code this - z} where {@code z = zRe + zIm i} */
    public ComplexVector mapSubtract(final double zRe, final double zIm) {
        return this.copy().mapSubtractToSelf(zRe, zIm);
    }

    /** Map a subtraction operation to each entry in a <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param d the real part of the value to multiply all entries by
     * @return {@code this - z} where {@code z = zRe + zIm i} */
    public ComplexVector mapSubtract(final double d) {
        return this.copy().mapSubtractToSelf(d, 0);
    }

    @Override
    public ComplexVector mapSubtractToSelf(final Complex d) {
        return mapSubtractToSelf(d.getReal(), d.getImaginary());
    }

    /** Map a subtraction operation to each entry.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param zRe the real part of the value to multiply all entries by
     * @param zIm the imaginary part of the value to multiply all entries by
     * @return for convenience, return {@code this}*/
    public ComplexVector mapSubtractToSelf(final double zRe, final double zIm) {
        for (int i = 0; i < length; ++i) {
            this.re[i] -= zRe;
            this.im[i] -= zIm;
        }
        return this;
    }
    
    @Override
    public ComplexMatrix outerProduct(final FieldVector<Complex> v) {
        final double[][] vReIm    = of(v).getDataRef();
        final double[][] thisReIm = this.getDataRef();

        final double[] vRe = vReIm[0];
        final double[] vIm = vReIm[1];
        final int n = v.getDimension();

        final double[][] answerRe = new double[n][];
        final double[][] answerIm = new double[n][];
        for (int i = 0; i < n; i++) {
            final double[] rowRe = new double[n];
            final double[] rowIm = new double[n];
            
            final double[] thisRowRe = thisReIm[0];
            final double[] thisRowIm = thisReIm[1];
            for (int j = 0; j < n; j++) {
                final double a = thisRowRe[i];
                final double b = thisRowIm[i];
                final double c = vRe[j];
                final double d = vIm[j];

                rowRe[j] = Numerics.complexXYRe(a, b, c, d);
                rowIm[j] = Numerics.complexXYIm(a, b, c, d);
            }
            
            answerRe[i] = rowRe;
            answerIm[i] = rowIm;
        }

        return new ComplexMatrix(answerRe, answerIm,
                                 false, // false -> don't clone the arrays
                                 ComplexMatrix.DEFAULT_CMATRIX_PARALLELISM);
    }

    @Override
    public ComplexVector getSubVector(final int index, final int n) {
        final double[][] subReIm = getSubVectorDouble(index, n);
        return new ComplexVector(subReIm[0], subReIm[1], false); // no need to clone
    }

    /** Get a subvector from consecutive elements.
     * 
     * @param index index of first element.
     * @param n number of elements to be retrieved.
     * @return a {@code double[2][n]} containing n elements. */
    public double[][] getSubVectorDouble(final int index, final int n) {
        checkIndex(index + n - 1);
         
        final double[][] thisReIm = this.getDataRef();
        final double[]   thisRe   = thisReIm[0];
        final double[]   thisIm   = thisReIm[1];

        final double[] dRe = new double[n];
        final double[] dIm = new double[n];

        for (int i = index; i < (n + index); ++i) {
            dRe[i - index] = thisRe[i];
            dIm[i - index] = thisIm[i];
        }

        return new double[][] { dRe, dIm };
    }

    
    @Override
    public Field<Complex> getField() {
        return ComplexField.getInstance();
    }

    @Override
    public ComplexVector copy() {
        return new ComplexVector(this, true);
    }

    @Override
    /** For {@code x = a + bi}, compute {@code 1 / x}
     * 
     * <pre>
     *  <code>
     *     1       a + bi       1     a - bi               a - bi
     *    --- = ------------ = --- x -------- = --------------------------
     *     x       a + bi       x     a - bi    a<sup>2</sup> - abi + abi - b<sup>2</sup>i<sup>2</sup>
     *  </code>
     * </pre> */
    public ComplexVector mapInv() {
        return this.copy().mapInvToSelf();
    }

    @Override
    /** For {@code x = a + bi}, compute {@code 1 / x}
     * 
     * <pre>
     *  <code>
     *     1       a + bi       1     a - bi               a - bi
     *    --- = ------------ = --- x -------- = --------------------------
     *     x       a + bi       x     a - bi    a<sup>2</sup> - abi + abi - b<sup>2</sup>i<sup>2</sup>
     *  </code>
     * </pre> */
    public ComplexVector mapInvToSelf() {
        final double[] divSave = new double[2];

        for (int i = 0; i < length; ++i) {
            final double c = this.re[i];
            final double d = this.im[i];

            Numerics.complexDivide(1, 0, c, d, divSave);
            this.re[i] = divSave[0];
            this.im[i] = divSave[1];
        }

        return this;
    }

    @Override
    public ComplexVector ebeMultiply(final FieldVector<Complex> v) {
        checkVectorDimensions(this, v);

        final double[][] vReIm = of(v).getDataRef();
        final double[]   vRe   = vReIm[0];
        final double[]   vIm   = vReIm[1];

        final ComplexVector answer = new ComplexVector(this);
        final double[][] thisReIm = this.getDataRef();
        final double[]   thisRe = thisReIm[0];
        final double[]   thisIm = thisReIm[1];


        for (int i = 0; i < length; ++i) {
            final double a = thisRe[i];
            final double b = thisIm[i];
            final double c = vRe[i];
            final double d = vIm[i];

            final double[] multSave = Numerics.complexMultiply(a, b, c, d);
            answer.re[i] = multSave[0];
            answer.im[i] = multSave[1];
        }
        return answer;

    }

    @Override
    public ComplexVector ebeDivide(final FieldVector<Complex> v) {
        checkVectorDimensions(this, v);

        final double[][] vReIm = of(v).getDataRef();
        final double[]   vRe   = vReIm[0];
        final double[]   vIm   = vReIm[1];

        final ComplexVector answer = new ComplexVector(this);
        final double[][] thisReIm = this.getDataRef();
        final double[]   thisRe = thisReIm[0];
        final double[]   thisIm = thisReIm[1];

        final double[] divSave = new double[2];

        for (int i = 0; i < length; ++i) {
            final double a = thisRe[i];
            final double b = thisIm[i];
            final double c = vRe[i];
            final double d = vIm[i];

            Numerics.complexDivide(a, b, c, d, divSave); // overwrite divSave each iteration
            answer.re[i] = divSave[0];
            answer.im[i] = divSave[1];
        }
        return answer;
    }

    @Override
    public Complex dotProduct(final FieldVector<Complex> v) {
        checkVectorDimensions(this, v);

        final double[][] vReIm = of(v).getDataRef();
        final double[]   vRe   = vReIm[0];
        final double[]   vIm   = vReIm[1];

        double dotRe = 0;
        double dotIm = 0;

        for (int i = 0; i < this.getLength(); ++i) {
            final double a = this.re[i];
            final double b = this.im[i];
            final double c = vRe[i];
            final double d = vIm[i];

            dotRe += Numerics.complexXYRe(a, b, c, d); // a c - b d
            dotIm += Numerics.complexXYIm(a, b, c, d); // a d + b c
            
        }

        return new Complex(dotRe, dotIm);
    }

    @Override
    public ComplexVector projection(final FieldVector<Complex> v) {
        throw new UnsupportedOperationException("ComplexVector::projection is not yet supported");
    }

    @Override
    public Complex getEntry(final int index) {
        checkIndex(index);
        return new Complex(this.re[index], this.im[index]);
    }
    
    /** Get the real part of the entry at the given index
     * 
     * @param index the index of the entry for which we want the real part
     * @return the real part of the entry at the given index */
    public double getEntryRe(final int index) {
        return this.re[index];
    }
    
    /** Get the imaginary part of the entry at the given index
     * 
     * @param index the index of the entry for which we want the imaginary part
     * @return the imaginary part of the entry at the given index */
    public double getEntryIm(final int index) {
        return this.im[index];
    }
    
    @Override
    public void setEntry(final int index, final Complex value) {
        setEntry(index, value.getReal(), value.getImaginary());
    }

    /** Set a single element.
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index element index.
     * @param valueRe new real portion of the value for the element.
     * @param valueIm the imaginary portion of the value for the element.
     */
    public void setEntry(final int index, final double valueRe, final double valueIm) {
        checkIndex(index);
        unsafeSetEntry(index, valueRe, valueIm);
    }

    /** Set a single element without checking indices against the dimensions of {@code this}.
     * <p>This should only be used when it is already known that the indices are in bound.</p>
     * <p>This instance <strong>is</strong> changed by this method.</p>
     * 
     * @param index element index.
     * @param valueRe new real portion of the value for the element.
     * @param valueIm the imaginary portion of the value for the element.
     */
    public void unsafeSetEntry(final int index, final double valueRe, final double valueIm) {
        re[index] = valueRe;
        im[index] = valueIm;
    }

    @Override
    public int getDimension() {
        return this.length;
    }

    /** Call for {@link #getDimension()} 
     * @return length of the {@link ComplexVector} */
    public int getLength() {
        return getDimension();
    }

    @Override
    public ComplexVector append(final FieldVector<Complex> v) {
        final int n    = this.getLength();
        final int newN = v.getDimension() + n;

        final double[][] vReIm = of(v).getDataRef();
        ComplexVector answer = new ComplexVector(newN);

        for (int i = 0; i < n; ++i) {
            answer.unsafeSetEntry(i, this.re[i], this.im[i]);
        }
        for (int i = n; i < newN; ++i) {
            answer.unsafeSetEntry(i, vReIm[0][i - n], vReIm[1][i - n]);
        }

        return answer;
    }

    @Override
    public ComplexVector append(final Complex d) {
        return append(d.getReal(), d.getImaginary());
    }

    /** Construct a vector by appending a complex value {@code z} where {@code z = zRe + zIm i} to {@code this}
     * in a <strong>new</strong> instance of {@link ComplexVector}.
     * 
     * @param zRe real part of value to append.
     * @param zIm imaginary part of value to append
     * @return a new vector
     */
    public ComplexVector append(final double zRe, final double zIm) {
        final int n = this.getLength();
        ComplexVector answer = new ComplexVector(n + 1);
        
        for (int i = 0; i < n; ++i) {
            answer.re[i] = this.re[i];
            answer.im[i] = this.im[i];
        }
        answer.re[n] = zRe;
        answer.im[n] = zIm;

        return answer;
    }

    @Override
    public void setSubVector(final int index, final FieldVector<Complex> v) {
        final int vLength = v.getDimension();
        checkIndex(index);
        checkIndex(index + vLength - 1);
        final double[][] vReIm = of(v).getDataRef();
        final double[]   vRe   = vReIm[0];
        final double[]   vIm   = vReIm[1];

        for (int i = index; i < vLength + index; ++i) {
            this.re[i] = vRe[i - index];
            this.im[i] = vIm[i - index];
        }
    }

    @Override
    public void set(final Complex value) {
        set(value.getReal(), value.getImaginary());
    }

    /** Set all elements to a single complex value {@code z} where {@code z = zRe + zIm i}.
     * @param zRe real portion of single value to set for all elements
     * @param zIm imaginary portion of single value to set for all elements */
    public void set(final double zRe, final double zIm) {
        for (int i = 0; i < length; ++i) {
            setEntry(i, zRe, zIm);
        }
    }

    
    /** @param length the (complex) length of the vector to return
     * @return a zero-vector of the given length */
    public static ComplexVector zero(final int length) {
        return new ComplexVector(new double[length], new double[length], false);
    }

    @Override
    public Complex[] toArray() {
        return this.getData();
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(this.re), Arrays.hashCode(this.im));
    }
    
    @Override
    public String toString() {
        return this.toString(false);
    }

    /** @return a string representation of {@code this} {@link ComplexVector} as a column matrix */
    public String toColumnString() {
        return this.toString(true);
    }
    
    /** Create and return a string representation of this as either a row or column matrix.
     * 
     * @param asColumn {@code true} if we want a column matrix representation, {@code false} otherwise.
     * @return a string representation of this as either a row or column matrix */
    public String toString(final boolean asColumn) {
        final Complex[][]   asMatrixArr = new Complex[][] { this.getData() };
        final ComplexMatrix asMatrix    = new ComplexMatrix(asMatrixArr);
        final ComplexMatrix toString    = asColumn ? asMatrix.transpose() : asMatrix;
        
        return toString.toString();
    }

    /** Test for equality with another {@link ComplexVector}.
     * If both the real and imaginary parts of two complex vectors are exactly the same, and neither contains 
     * {@code Double.NaN}, the two Complex objects are considered to be equal.
     * The behavior is the mimicked from JDK's {@link Double#equals(Object)
     * Double}:
     * <ul>
     *  <li>
     *   Instances constructed with different representations of zero 
     *   (i.e. either "0" or "-0") are <em>not</em> considered to be equal.
     *  </li>
     * </ul>
     *
     * @param object {@link ComplexVector} to test for equality with this instance.
     * @return {@code true} if the arrays are equal, {@code false} if object is {@code null}, not an instance of 
     * {@link ComplexVector}, or not equal to this instance. */
    @Override
    public boolean equals(final Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof FieldVector)) {
            return false;
        }
        if (object instanceof ComplexVector) {
            ComplexVector v = (ComplexVector) object;
            double[][] array1 = this.getDataRef();
            double[][] array2 = v.getDataRef();
            for (int index = 0; index < this.getLength(); ++index) {
                if (array1[0][index] != array2[0][index] || array1[1][index] != array2[1][index]) {
                        return false;
                }
            }   
        }
        else {
            double[][] array1 = this.getDataRef();
            @SuppressWarnings("unchecked")
            FieldVector<Complex> array2 = (FieldVector<Complex>) object;
            for (int index = 0; index < this.getLength(); ++index) {
                final Complex array2Entry = array2.getEntry(index);
                if (array1[0][index] != array2Entry .getReal() 
                        || array1[1][index] != array2Entry.getImaginary()) {
                        return false;
                }
            }   
        }

        return true;
    }

    /** Determine equivalence between {@code this} and {@code other}. Equivalence is determined by element-by-element
     * equality within the given absolute tolerances.
     * 
     * @param other the other {@link ComplexMatrix}
     * @param realAbsEps the absolute tolerance for the equality between the {@link Complex#getReal() real parts} of the
     *        entries.
     * @param imgAbsEps the absolute tolerance for the equality between the {@link Complex#getImaginary() imaginary
     *        parts} of the entries.
     * @return {@code true} if the elements of {@code this} and {@code other} are equal within the given absolute
     *         tolerances, {@code false} otherwise */
    public boolean absEquivalentTo(final ComplexVector other, final double realAbsEps, final double imgAbsEps) {
        Objects.requireNonNull(other, "other may not be null");
        
        if (this == other) {
            return true;
        }
        
        final int dim = this.getLength();
        if (other.getLength() != dim) {
            return false;
        }
        
        final double[][] thisReIm  = this.getDataRef();
        final double[][] otherReIm = other.getDataRef();
        for (int i = 0; i < dim; ++i) {
            if (!Precision.equals(thisReIm[0][i], otherReIm[0][i], realAbsEps)
                    || !Precision.equals(thisReIm[1][i], otherReIm[1][i], imgAbsEps)) {
                return false;
            }
        }

        return true;
    }
}