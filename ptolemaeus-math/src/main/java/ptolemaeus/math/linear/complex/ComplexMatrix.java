/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.concurrent.NotThreadSafe;

import org.hipparchus.Field;
import org.hipparchus.complex.Complex;
import org.hipparchus.complex.ComplexField;
import org.hipparchus.exception.LocalizedCoreFormats;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.exception.NullArgumentException;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.Array2DRowFieldMatrix;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldMatrixChangingVisitor;
import org.hipparchus.linear.FieldMatrixPreservingVisitor;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.commons.BeCareful;
import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.householderreductions.ComplexHouseholder;
import ptolemaeus.math.linear.real.Matrix;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

// CHECKSTYLE.OFF: LineLength
/** A {@link ComplexMatrix} is a {@link FieldMatrix} with {@link Complex} entries<br>
 * <br>
 * 
 * Internally, each {@link ComplexMatrix} is composed of two 2D double arrays {@code double[numRows][numCols]}
 * {@code this.Re} and {@code this.Im}, where {@code this.Re} holds the real parts of the {@link Complex} values of this
 * (note that this doesn't actually store any {@link Complex} directly), and {@code this.Im} holds the imaginary
 * parts.<br>
 * <br>
 * 
 * Additionally, the input for a {@link ComplexMatrix} can be composed of one 3D double array
 * {@code double[2][numRows][numCols]} where the first array {@code double[0][][]} is read as the real part
 * {@code this.Re} and the second array {@code double[1][][]} is read as the imaginary part {@code this.Im}.
 * {@link ComplexMatrix#getDataRef()} returns this type of 3D array {@code double[2][][]} of {@code this.Re, this.Im }.
 * <br>
 * <br>
 * 
 * Notice that this is {@link NotThreadSafe}. As of yet, <em>no</em> implementation of {@link RealMatrix} nor
 * {@link FieldMatrix} is thread-safe.<br>
 * <br>
 * 
 * Example constructions:<br>
 * <br>
 * 
 * The following two are equivalent:
 * 
 * <pre>
 * final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1 }, // Re
 *                                                            { 1, 2, 2, 2 },
 *                                                            { 0, 2, 3, 3 },
 *                                                            { 0, 0, 3, 4 } },
 *                                           new double[][] { { -1, -1, -1, -1 }, // Im
 *                                                            { -1, -2, -2, -2 },
 *                                                            { 0,  -2, -3, -3 },
 *                                                            { 0,   0, -3, -4 } } });
 * </pre>
 * 
 * <pre>
 * final ComplexMatrix A = new ComplexMatrix(new Complex[][] {
 *     { Complex.valueOf(1, -1), Complex.valueOf(1, -1), Complex.valueOf(1, -1), Complex.valueOf(1, -1) },
 *     { Complex.valueOf(1, -1), Complex.valueOf(2, -2), Complex.valueOf(2, -2), Complex.valueOf(2, -2) },
 *     { Complex.valueOf(0,  0), Complex.valueOf(2, -2), Complex.valueOf(3, -3), Complex.valueOf(3, -3) },
 *     { Complex.valueOf(0,  0), Complex.valueOf(0,  0), Complex.valueOf(3, -3), Complex.valueOf(4, -4) } }
 * });
 * </pre>
 * 
 * TODO Ryan Moser, 2025-01-15: write {@code preMultiplySubMatrix(ComplexVector, rowStart, colStart)} and
 * {@code postMultiplySubMatrix(ComplexVector, rowStart, colStart)} to that we can remove more calls to
 * {@link #getSubMatrix(int, int, int, int)}, esp. in {@link ComplexHouseholder}<br>
 * <br>
 * 
 * TODO Ryan Moser, 2024: write an unmodifiable view for instances of this class<br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-01-06: refactor the operate (matrix-vector multiply/Level-2 BLAS) family of methods to do the
 * multiplication without copying any vectors/data arrays.<br>
 * Currently, we copy the vectors/data arrays to be multiplied into instances of {@link ComplexMatrix} so that we can
 * use {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}. This is definitely
 * not a bottleneck, but it would be good to do it properly:
 * <ul>
 * <li>{@link #operate(Complex[])}</li>
 * <li>{@link #operate(FieldVector)}</li>
 * <li>{@link #conjugateTransposeOperate(ComplexVector)}</li>
 * </ul>
 * <br>
 * <br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * @author Catherine Doud, catherine.a.doud@lmco.com */
// CHECKSTYLE.ON: LineLength
public class ComplexMatrix implements FieldMatrix<Complex> {
    
    /** Number of rows in the instance of {@link ComplexMatrix} */
    private final int numRows;
    /** Number of columns in the instance of {@link ComplexMatrix} */
    private final int numCols;
    /** The <em>maximum possible</em> rank of a matrix of this size: {@code min(numRows, numCols)}. This is equivalent
     * to the diagonal dimension of a matrix. */
    private final int maxRank;
    
    /** Real part of the {@link ComplexMatrix} */
    private final double[][] Re;
    /** Imaginary part of the {@link ComplexMatrix} */
    private final double[][] Im;
    
    /** The {@link Parallelism} to use when none is specified */
    public static final Parallelism DEFAULT_CMATRIX_PARALLELISM = Parallelism.SEQUENTIAL;
    
    /** the level of {@link Parallelism} to use when performing matrix operations with this {@link ComplexMatrix} */
    private final Parallelism parallelism;
    
    /** Create a {@link ComplexMatrix} by specifying the {@code double[][]} entries
     * 
     * @param entriesRe the {@code double[]][]} real entries of this {@link ComplexMatrix}.  Always cloned.
     * @param entriesIm the {@code double[]][]} im entries of this {@link ComplexMatrix}.  Always cloned. */
    public ComplexMatrix(final double[][] entriesRe, final double[][] entriesIm) {
        this(entriesRe, entriesIm, true, DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Create a {@link ComplexMatrix} by specifying the {@link Complex} entries
     * 
     * @param entriesRe the {@code double[]][]} real entries of this {@link ComplexMatrix}.  Always cloned.
     * @param entriesIm the {@code double[]][]} im entries of this {@link ComplexMatrix}.  Always cloned. 
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}. */
    public ComplexMatrix(final double[][] entriesRe, final double[][] entriesIm, final Parallelism parallel) {
        this(entriesRe, entriesIm, true, parallel);
    }

    /** Create a {@link ComplexMatrix} by specifying the {@code double[][]} real and imaginary entries
     * 
     * @param dRe the {@code double[][]} real entries of this {@link ComplexMatrix}. 
     *     Cloned iff {@code clone == true}.
     * @param dIm the {@code double[][]} imaginary entries of this {@link ComplexMatrix}. 
     *     Cloned iff {@code clone == true}.
     * @param clone {@code true} to deeply clone {@code entries}, {@code false} to simple reference it.
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}. */
    @SuppressFBWarnings("EI2")
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexMatrix(final double[][] dRe, final double[][] dIm,
                         final boolean clone, final Parallelism parallel) {
        checkSameDoubleDimension(dRe, dIm);
        
        this.numRows = dRe.length;
        this.numCols = dRe[0].length;
        this.maxRank = FastMath.min(this.numRows, this.numCols);
        
        if (!clone) {
            this.Re = dRe;
            this.Im = dIm;
        }
        else {
            this.Re = new double[numRows][numCols];
            this.Im = new double[numRows][numCols];
            
            for (int i = 0; i < this.numRows; i++) {
                final double[] ithRowReFrom = dRe[i];
                final double[] ithRowImFrom = dIm[i];
                final double[] ithRowReTo   = this.Re[i];
                final double[] ithRowImTo   = this.Im[i];
                
                for (int j = 0; j < this.numCols; j++) {
                    ithRowReTo[j] = ithRowReFrom[j];
                    ithRowImTo[j] = ithRowImFrom[j];
                }
            }
        }
        
        this.parallelism = requireNonNull(parallel, "A Parallelism must be specified");
    }
    
    /** Create a {@link ComplexMatrix} by specifying the {@link Complex} entries
     * 
     * @param entries the {@link Complex} entries of this {@link ComplexMatrix}.  Always cloned. */
    public ComplexMatrix(final Complex[][] entries) {
        this(entries, DEFAULT_CMATRIX_PARALLELISM);
    }

    /** Create a {@link ComplexMatrix} by specifying the {@link Complex} entries
     * 
     * @param entries the {@link Complex} entries of this {@link ComplexMatrix}.  Cloned iff {@code clone == true}.
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}. */
    public ComplexMatrix(final Complex[][] entries, final Parallelism parallel) {
        this(entries.length, entries[0].length, parallel);

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                final Complex ij = entries[i][j];
                this.Re[i][j] = ij.getReal();
                this.Im[i][j] = ij.getImaginary();
            }
        }
    }

    /** Create a {@link ComplexMatrix} from a {@link RealMatrix} by copying the real entries into new {@link Complex}
     * entries which only have non-zero real components.
     * 
     * @param copyFrom the {@link RealMatrix} from which we want to copy */
    public ComplexMatrix(final RealMatrix copyFrom) {
        this(copyFrom, DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Create a {@link ComplexMatrix} from a {@link RealMatrix} by copying the real entries into new {@link Complex}
     * entries which only have non-zero real components.
     * 
     * @param copyFrom the {@link RealMatrix} from which we want to copy
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}. */
    public ComplexMatrix(final RealMatrix copyFrom, final Parallelism parallel) {
        this(copyFrom.getRowDimension(), copyFrom.getColumnDimension(), parallel);
        for (int i = 0; i < copyFrom.getRowDimension(); i++) {
            for (int j = 0; j < copyFrom.getColumnDimension(); j++) {
                this.Re[i][j] = copyFrom.getEntry(i, j);
                // this.Im[i][j] = 0.0; (this.Im is already all zeroes)
            }
        }
    }
    
    /** Create a {@link ComplexMatrix} from a {@link RealMatrix} by copying the real entries into new {@link Complex}
     * entries which only have non-zero real components.
     * 
     * @param copyFrom the {@link RealMatrix} from which we want to copy */
    public ComplexMatrix(final double[][] copyFrom) {
        this(copyFrom, DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Create a {@link ComplexMatrix} from a {@link RealMatrix} by copying the real entries into new {@link Complex}
     * entries which only have non-zero real components. All imaginary components in this {@link ComplexMatrix} will
     * be set to zero.
     * 
     * @param copyFrom the {@link RealMatrix} from which we want to copy
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}. */
    public ComplexMatrix(final double[][] copyFrom, final Parallelism parallel) {
        this(copyFrom.length, copyFrom[0].length, parallel);
        for (int i = 0; i < copyFrom.length; i++) {
            for (int j = 0; j < copyFrom[0].length; j++) {
                this.Re[i][j] = copyFrom[i][j];
                this.Im[i][j] = 0.0;
            }
        }
    }
    
    /** Constructor for efficiently creating a {@link ComplexMatrix} from a {@link FieldMatrix}
     * 
     * @param matrix the {@link FieldMatrix} we want to copy
     * @param clone {@code true} to always clone the clone the {@link FieldMatrix#getData() data array}, {@code false}
     *        to skip cloning if possible. We can skip cloning when {@code matrix instanceof ComplexMatrix}. Otherwise,
     *        we must always clone the data arrays. This is due to how the real and imaginary parts of the numbers are
     *        stored separately.
     *        
     * @apiNote Users must be careful to not misuse the {@link FieldMatrix} that was passed in, else changes to its data
     *          array will be reflected in this {@link ComplexMatrix} instance. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexMatrix(final FieldMatrix<Complex> matrix, final boolean clone) {
        this(matrix, clone, DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Constructor for efficiently creating a {@link ComplexMatrix} from a {@link FieldMatrix}
     * 
     * @param matrix the {@link FieldMatrix} we want to copy
     * @param clone {@code true} to always clone the clone the {@link FieldMatrix#getData() data array}, {@code false}
     *        to skip cloning if possible. We can skip cloning when {@code matrix instanceof ComplexMatrix}. Otherwise,
     *        we must always clone the data arrays. This is due to how the real and imaginary parts of the numbers are
     *        stored separately.
     * 
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}.
     *        
     * @apiNote Users must be careful to not misuse the {@link FieldMatrix} that was passed in, else changes to its data
     *          array will be reflected in this {@link ComplexMatrix} instance. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public ComplexMatrix(final FieldMatrix<Complex> matrix, final boolean clone, final Parallelism parallel) {
        this.numRows = matrix.getRowDimension();
        this.numCols = matrix.getColumnDimension();
        this.maxRank = FastMath.min(this.numRows, this.numCols);
        this.parallelism = requireNonNull(parallel, "A Parallelism must be specified");
        
        if (!clone && matrix instanceof ComplexMatrix toCopy) {
            this.Re = toCopy.Re;
            this.Im = toCopy.Im;
        }
        else {
            this.Re = new double[numRows][numCols];
            this.Im = new double[numRows][numCols];
            
            if (matrix instanceof ComplexMatrix toCopy) {
                for (int i = 0; i < numRows; i++) {
                    for (int j = 0; j < numCols; j++) {
                        this.Re[i][j] = toCopy.getEntryRe(i, j);
                        this.Im[i][j] = toCopy.getEntryIm(i, j);
                    }
                }
            }
            else {
                for (int i = 0; i < numRows; i++) {
                    for (int j = 0; j < numCols; j++) {
                        final Complex ij = matrix.getEntry(i, j);
                        this.Re[i][j] = ij.getReal();
                        this.Im[i][j] = ij.getImaginary();
                    }
                }
            }
        }
    }
    
    /** Constructor to construct an empty instance.  All entries will be zero until set otherwise.
     * 
     * @param rowDimension the number of rows
     * @param columnDimension the number of columns
     * 
     * @apiNote only for use when we can create a new {@link ComplexMatrix} element-wise and only want to loop over the
     *          elements once */
    public ComplexMatrix(final int rowDimension, final int columnDimension) {
        this(rowDimension, columnDimension, DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Constructor to construct an empty instance.  All entries will be zero until set otherwise.
     * 
     * @param rowDimension the number of rows
     * @param columnDimension the number of columns
     * @param parallel the level of {@link Parallelism}. See
     *        {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}.
     * 
     * @apiNote only for use when we can create a new {@link ComplexMatrix} element-wise and only want to loop over the
     *          elements once */
    public ComplexMatrix(final int rowDimension, final int columnDimension, final Parallelism parallel) {
        this.numRows = rowDimension;
        this.numCols = columnDimension;
        this.maxRank = FastMath.min(this.numRows, this.numCols);
        this.Re = new double[rowDimension][columnDimension];
        this.Im = new double[rowDimension][columnDimension];

        this.parallelism = requireNonNull(parallel, "A Parallelism must be specified");
    }

    /**
     * A protected constructor for subclassing
     * @param parallel the level of {@link Parallelism}.
     */
    protected ComplexMatrix(final Parallelism parallel) {
        this.parallelism = requireNonNull(parallel, "A Parallelism must be specified");
        this.numRows = 0;
        this.numCols = 0;
        this.maxRank = 0;
        this.Re = null;
        this.Im = null;
    }
    
    /** Either create a new {@link ComplexMatrix} that wraps the data from the one given, or return {@code A} cast to
     * {@link ComplexMatrix}. <em>We avoid cloning if we can</em>; i.e.
     * <ul>
     * <li>if {@code A} is already a {@link ComplexMatrix}, we return it as-is</li>
     * <li>if it's a {@link FieldMatrix}, we don't clone the internal data reference if we can avoid it (see
     * {@link ComplexMatrix#extractData(FieldMatrix)})</li>
     * <li>if it's a {@link RealMatrix}, we must clone regardless</li>
     * </ul>
     * 
     * @param A the {@link AnyMatrix} that we want to wrap in a {@link ComplexMatrix}
     * @return the {@link ComplexMatrix} */
    @SuppressWarnings("unchecked") // we check the field type, so it's safe
    public static ComplexMatrix of(final AnyMatrix A) {
        requireNonNull(A, "The matrix A must not be null");
        if (A instanceof ComplexMatrix M) {
            return M;
        }
        if (A instanceof FieldMatrix M) {
            Validation.requireEqualsTo(M.getField(), ComplexField.getInstance(),
                    "can only create a ComplexMatrix from a Array2DRowFieldMatrix of the Complex field");
            return new ComplexMatrix(M, false);
        }
        if (A instanceof RealMatrix M) {
            return new ComplexMatrix(M);
        }
        throw new UnsupportedOperationException("Unable to create ComplexMatrix from type "
                                                + A.getClass().getSimpleName());
    }
    
    /** @return the level of {@link Parallelism} that we will attempt to use with this {@link ComplexMatrix}
     *
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public Parallelism getParallelism() {
        return this.parallelism;
    }
    
    /** @return the conjugate transpose of this {@link ComplexMatrix}; i.e., a {@link ComplexMatrix} whose entries are
     *         both the {@link #transpose()} and the {@link Complex#conjugate() complex conjugates} of the entries of
     *         this */
    public ComplexMatrix conjugateTranspose() {

        final ComplexMatrix answer  = new ComplexMatrix(getColumnDimension(), getRowDimension(), this.parallelism);

        for (int i = 0; i < getRowDimension(); i++) {
            for (int j = 0; j < getColumnDimension(); j++) {
                answer.Re[j][i] = getEntryRe(i, j);
                answer.Im[j][i] = -1 * getEntryIm(i, j);
            }
        }

        return answer;
    }

    /** Compute and return the Frobenius norm of this {@link ComplexMatrix}. The Frobenius norm is the square-root of
     * the sum of the absolute squares of the elements of the matrix.
     * 
     * See <a href="https://mathworld.wolfram.com/FrobeniusNorm.html">Wolfram Mathworld: Frobenius Norm</a>
     * 
     * @return the Frobenius norm of this {@link ComplexMatrix} */
    public double getFrobeniusNorm() { // name conforms to naming convention established by RealMatrix interface
        double sumSqs = 0.0;
        for (int i = 0; i < this.getRowDimension(); i++) {
            for (int j = 0; j < this.getColumnDimension(); j++) {
                final double a = getEntryRe(i, j);
                final double b = getEntryIm(i, j);
                sumSqs += Numerics.complexNormSq(a, b);
            }
        }
        
        return FastMath.sqrt(sumSqs);
    }
    
    /** Compute and return the <a href="http://mathworld.wolfram.com/MaximumAbsoluteColumnSumNorm.html"> maximum
     * absolute column sum norm</a> - the L<sub>1</sub> norm - of {@code this} {@link ComplexMatrix}. Following LAPACK,
     * we use the {@link Numerics#complexNorm(double, double) complex modulus} as the absolute value over
     * the complex numbers.
     * 
     * @return norm L<sub>1</sub> norm of {@code this} {@link ComplexMatrix} */
    public double getNorm1() { // name conforms to naming convention established by RealMatrix interface
        final double[] runningColSums = new double[this.getColumnDimension()];
        for (int i = 0; i < this.getRowDimension(); i++) {
            for (int j = 0; j < this.getColumnDimension(); j++) {
                final double ijRe = this.getEntryRe(i, j);
                final double ijIm = this.getEntryIm(i, j);
                runningColSums[j] += Numerics.complexNorm(ijRe, ijIm);
            }
        }
        
        return Numerics.max(runningColSums);
    }
    
    /** Compute and return the L<sub>2</sub> norm - a.k.a. the 2-norm - of this {@link ComplexMatrix}. The L<sub>2</sub>
     * norm is also known as the {@link #getFrobeniusNorm() Frobenius norm}.
     *
     * @return norm L<sub>2</sub> norm of {@code this} {@link ComplexMatrix}
     * 
     * @see #getFrobeniusNorm() */
    public double getNorm2() { // name conforms to naming convention established by RealMatrix interface
        return getFrobeniusNorm();
    }
    
    /** Compute and return the
     * <a href="http://mathworld.wolfram.com/MaximumAbsoluteRowSumNorm.html"> maximum absolute row sum norm</a>
     * - the L<sub>&infin;</sub> norm - of {@code this} {@link ComplexMatrix}. Following LAPACK,
     * we use the {@link Numerics#complexNorm(double, double) complex modulus} as the absolute value over
     * the complex numbers.
     * 
     * @return norm (L<sub>&infin;</sub>) norm of {@code this} {@link ComplexMatrix} */
    public double getNormInfty() { // name conforms to naming convention established by RealMatrix interface
        double maxRowSum = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < this.getRowDimension(); i++) {
            double rowSum = 0.0;
            for (int j = 0; j < this.getColumnDimension(); j++) {
                final double ijRe = this.getEntryRe(i, j);
                final double ijIm = this.getEntryIm(i, j);
                rowSum += Numerics.complexNorm(ijRe, ijIm);
            }
            
            maxRowSum = Numerics.max(maxRowSum, rowSum);
        }
        
        return maxRowSum;
    }
    
    /** @param absEBETol the absolute element-by-element tolerance. Elements with imaginary part larger than this will
     *        be considered non-zero.
     * @return true if this has only real-value elements, false otherwise
     *        
     * @see #isReal(ComplexMatrix, double) */
    public boolean isReal(final double absEBETol) {
        return isReal(this, absEBETol);
    }
    
    /** If this {@link ComplexMatrix#isReal(double) is real}, then return a {@link Optional} holding a {@link Matrix}
     * that equals the real part of this {@link ComplexMatrix}. Otherwise, return an empty {@link Optional}.
     * 
     * @param absEBETol the absolute element-by-element tolerance. Elements with imaginary part larger than this will be
     *        considered non-zero.
     * @return a {@link Optional} holding a {@link Matrix} that equals the real part of this {@link ComplexMatrix} if
     *         this {@link ComplexMatrix#isReal(double) is real} within the tolerance. Otherwise, return an empty
     *         {@link Optional}. */
    public Optional<Matrix> asReal(final double absEBETol) {
        if (!this.isReal(absEBETol)) {
            return Optional.empty();
        }
        
        return Optional.of(new Matrix(this.Re));
    }
    
    /** Determine whether this {@link ComplexMatrix} is symmetric to within the given relative tolerance
     * 
     * @param relEBETol the relative element-wise tolerance. See
     *        {@link Numerics#equalsRelEps(double, double, double)} for an explanation of relative
     *        tolerances.
     * @return {@code false} if this {@link ComplexMatrix} is either not {@link AnyMatrix#isSquare() square} or not
     *         symmetric, {@code true} otherwise */
    public boolean isSymmetric(final Complex relEBETol) {
        return isSymmetric(this, relEBETol);
    }
    
    /** Determine whether {@link ComplexMatrix this} is both real and symmetric to within the given <em>absolute</em>
     * tolerance.
     * 
     * @param absTol the <em>absolute</em> tolerance when checking whether elements are symmetric and real
     * @return {@code false} if the {@link ComplexMatrix this} is either not {@link AnyMatrix#isSquare() square} or not
     *         real-symmetric, {@code true} otherwise */
    public boolean isRealSymmetric(final double absTol) {
        return isRealSymmetric(this, absTol);
    }
    
    /** Determine whether this {@link ComplexMatrix} is Hermitian to within the given relative tolerance. Hermitian
     * meaning equal to its own conjugate transpose.
     * 
     * @param relEBETol the relative element-wise tolerance. See
     *        {@link Numerics#equalsRelEps(double, double, double)} for an explanation of relative
     *        tolerances.
     * @return {@code false} if this {@link ComplexMatrix} is either not {@link AnyMatrix#isSquare() square} or not
     *         Hermitian, {@code true} otherwise */
    public boolean isHermitian(final Complex relEBETol) {
        return isHermitian(this, relEBETol);
    }

    /** Add the matrix {@code B} to {@code this}.<br><br>
     * 
     * Pass-through to {@link #add(FieldMatrix)}.
     * 
     * @param B the {@link ComplexMatrix} we want to add to this
     * @return {@code A + B}, where {@code A == this} */
    public ComplexMatrix plus(final FieldMatrix<Complex> B) {
        return this.add(B);
    }

    /** Subtract the matrix {@code B} from {@code this}.<br><br>
     * 
     * Pass-through to {@link #subtract(FieldMatrix)}.
     * 
     * @param B the {@link ComplexMatrix} we want to subtract from this
     * @return {@code A - B}, where {@code A == this} */
    public ComplexMatrix minus(final FieldMatrix<Complex> B) {
        return this.subtract(B);
    }
    
    /** Post-multiply {@code this} with the matrix {@code B}.<br><br>
     * 
     * Pass-through to {@link #multiply(FieldMatrix)}.
     * 
     * @param B the matrix by which we want to post-multiply {@code this}
     * @return {@code A.B}, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public ComplexMatrix times(final FieldMatrix<Complex> B) {
        return this.multiply(B);
    }
    
    /** Post-multiply {@code this} with the transpose of the matrix {@code B}.<br><br>
     * 
     * Pass-through to {@link #multiplyTransposed(FieldMatrix)}.
     * 
     * @param B the matrix whose transpose by which we want to post-multiply {@code this}
     * @return A.B<sup>T</sup>, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public ComplexMatrix timesTransposeOf(final FieldMatrix<Complex> B) {
        return this.multiplyTransposed(B);
    }
    
    /** Post-multiply {@code this} with the conjugate transpose of the matrix {@code B}.<br><br>
     * 
     * Pass-through to {@link #multiplyConjugateTransposed(FieldMatrix)}.
     * 
     * @param B the matrix whose conjugate transpose by which we want to post-multiply {@code this}
     * @return A.B<sup>H</sup>, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public ComplexMatrix timesConjugateTransposeOf(final FieldMatrix<Complex> B) {
        return this.multiplyConjugateTransposed(B);
    }
    
    // A.B
    
    @Override
    public ComplexMatrix multiply(final FieldMatrix<Complex> B) {
        return multiply(this, false, false,
                        B,    false, false,
                        this.parallelism);
    }
    
    // A.B^T
    
    /** Post-multiply {@code this} with the transpose of the matrix {@code B}
     * 
     * @param B the matrix whose transpose by which we want to post-multiply {@code this}
     * @return A.B<sup>T</sup>, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    @Override
    public ComplexMatrix multiplyTransposed(final FieldMatrix<Complex> B) {
        return multiply(this, false, false,
                        B,    true,  false,
                        this.parallelism);
    }
    
    // A^T.B
    
    /** Post-multiply the transpose of {@code this} with the matrix {@code B}
     * 
     * @param B the matrix by which we want to post-multiply the transpose of {@code this}
     * @return A<sup>T</sup>.B, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    @Override
    public ComplexMatrix transposeMultiply(final FieldMatrix<Complex> B) {
        return multiply(this, true,  false,
                        B,    false, false,
                        this.parallelism);
    }
    
    // A.B^H
    
    /** Compute and return {@code this.B}<sup>H</sup> without actually constructing the conjugate transpose.<br>
     * H denotes the conjugate transpose.
     * 
     * @param B the matrix whose conjugate transpose by which we want to pre-multiply by {@code this}
     * @return {@code this.B}<sup>H</sup> */
    public ComplexMatrix multiplyConjugateTransposed(final FieldMatrix<Complex> B) {
        return multiply(this, false, false,
                        B,    true,   true,
                        this.parallelism);
    }
    
    // A^H.B
    
    /** Compute and return {@code this}<sup>H</sup>{@code .B} without actually constructing the conjugate transpose.<br>
     * H denotes the conjugate transpose.
     * 
     * @param B the matrix which we want to pre-multiply by the conjugate transpose of this
     * @return {@code this}<sup>H</sup>{@code .B} */
    public ComplexMatrix conjugateTransposeMultiply(final FieldMatrix<Complex> B) {
        return multiply(this, true,  true,
                        B,    false, false,
                        this.parallelism);
    }
    
    // B.A
    
    @Override
    public ComplexMatrix preMultiply(final FieldMatrix<Complex> B) {
        return multiply(B,    false, false,
                        this, false, false,
                        this.parallelism);
    }
    
    // x.A
    
    /** Pre-multiply {@code this} by the vector {@code v}.<br><br>
     * 
     * Pass-through to {@link #preMultiply(Complex[])}.
     * 
     * @param v the vector by which we want to pre-multiply {@code this}
     * @return {@code v.A}, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    @Override
    public ComplexVector preMultiply(final FieldVector<Complex> v) {
        return new ComplexVector(this.preMultiply(v.toArray()));
    }
    
    /** Pre-multiply {@code this} by the vector {@code v} as a {@link Complex Complex[]}
     * 
     * @param v the vector by which we want to pre-multiply {@code this}
     * @return {@code v.A} as a {@link Complex Complex[]}, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    @Override
    public Complex[] preMultiply(final Complex[] v) {
        final Complex[][] vTData = new Complex[][] {v}; // size 1 x v.length
        
        final ComplexMatrix vMatrix = new ComplexMatrix(vTData, Parallelism.PARALLEL); /* This is v as a row matrix. The
                                                                                        * choice of PARALLEL here is
                                                                                        * irrelevant. */
        final ComplexMatrix answerMatrix = multiply(vMatrix, false, false,
                                                    this,    false, false,
                                                    this.parallelism);
        return answerMatrix.getRow(0);
    }

    // A.x
    
    /** Operate on the given vector according to {@code this} {@link ComplexMatrix}.<br><br>
     * 
     * Pass-through to {@link #operate(Complex[])}.
     * 
     * @param vec the vector on which we want {@code this} to operate
     * @return {@code A.v}, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    @Override
    public ComplexVector operate(final FieldVector<Complex> vec) {
        final ComplexVector v = ComplexVector.of(vec);
        final double[][] vTReImData = v.getDataRef();
        
        /* vT is v as a row matrix (note the ^T). Thus we use true for "transposeB" which tells the method to multiply
         * by V^T^T == V, but it's done efficiently. */
        final ComplexMatrix vT = new ComplexMatrix(new double[][] { vTReImData[0] },
                                                   new double[][] { vTReImData[1] },
                                                   false,
                                                   this.parallelism);
        
        final ComplexMatrix answerMatrix = multiply(this, false, false,
                                                    vT,   true,  false,
                                                    this.parallelism);
        
        final double[][] onlyColumn = answerMatrix.getDoubleSubColumn(0, 0, answerMatrix.getRowDimension());
        return new ComplexVector(onlyColumn[0], onlyColumn[1]);
    }
    
    /** Operate on the given vector - as a {@link Complex Complex[]} - according to {@code this}
     * {@link ComplexMatrix}
     * 
     * @param v the vector on which we want {@code this} to operate
     * @return {@code A.v} as a {@link Complex Complex[]}, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    @Override
    public Complex[] operate(final Complex[] v) {
        final Complex[][] vTData = new Complex[][] { v }; // size 1 x v.length
        
        /* vT is v as a row matrix (note the ^T). Thus we use true for "transposeB" which tells the method to multiply
         * by V^T^T == V, but it's done efficiently. */
        final ComplexMatrix vT = new ComplexMatrix(vTData, this.parallelism);
        final ComplexMatrix answerMatrix = multiply(this, false, false,
                                                    vT,   true,  false,
                                                    this.parallelism);
        return answerMatrix.getColumn(0);
    }
    
    /** Operate on the given vector according to the {@link #conjugateTranspose() conjugate transpose} of {@code this}
     * {@link ComplexMatrix}, but without actually constructing the {@link #conjugateTranspose() conjugate transpose}.
     * 
     * @param v the vector on which we want {@code this} to operate
     * @return {@code A.v}, where {@code A == this}
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public ComplexVector conjugateTransposeOperate(final ComplexVector v) {
        /* vT is v as a row matrix (note the ^T). Thus we use true for "transposeB" which tells the method to multiply
         * by V^T^T == V, but it's done efficiently. */
        final ComplexMatrix vT = ComplexMatrix.fromRows(v);
        final ComplexMatrix answerMatrix = multiply(this, true, true,
                                                    vT,   true, false,
                                                    this.parallelism);
        return answerMatrix.getColumnVector(0);
    }
    
    /** Multiply matrices {@code A} and {@code B} (with possible transposition and/or conjugation), and possibly make
     * use of the parallelizability of matrix multiplication.<br>
     * <br>
     * 
     * This multiplication method offers at several significant performance improvements over the standard
     * {@link Array2DRowFieldMatrix#multiply(Array2DRowFieldMatrix)}:
     * 
     * <ol>
     * 
     * <li>Firstly, this method of multiplication avoids creating huge numbers of {@link Complex} instances by carrying
     * out all {@link Complex} multiplications using {@code double}s explicitly.<br>
     * <br>
     * Given two {@link Array2DRowFieldMatrix} instances {@code A} - {@code N x M} - and {@code B} - {@code M x K} -
     * with complex entries, computing {@code A.B} results in {@code N * K * M} calls to
     * {@link Complex#multiply(Complex)} and {@code N * K * (M - 1)} calls to {@link Complex#add(Complex)}, both of
     * which create a new {@link Complex} instance, and we keep {@code N * K} of them: one for each entry.<br>
     * <br>
     * 
     * So, in total, we create and immediately throw away
     * {@code N * K * M + N * K * (M - 1) - N * K == 2 * (N * K * (M - 1))} intermediate and unnecessary {@link Complex}
     * instances.<br>
     * <br>
     * 
     * E.g., multiplying a {@code 8x16} matrix by a {@code 16x4} results in
     * {@code 8 * 4 * 16 + 8 * 4 * (16 - 1) - 8 * 4 == 2 * (8 * 4 * (16 - 1)) == 960} new intermediate and unnecessary
     * {@link Complex} instances.<br>
     * <br>
     * 
     * This implementation avoids <em>all</em> of those, instantiating exclusively those {@link Complex} instances that
     * are needed once for each entry. <br>
     * <br>
     * 
     * </li>
     * 
     * <li>In case we want to perform a multiplication of the form {@code A^T.B} or {@code A.B^T}, this method doesn't
     * actually perform the transposition and instead simply adjusts the indices when pulling data from the data arrays
     * for multiplication.<br>
     * <br>
     * </li>
     * 
     * <li>Similarly for conjugation, we don't actually compute the conjugate matrix, we just negate the imaginary part
     * of the entry when doing the multiplication<br>
     * <br>
     * 
     * <li>Optionally, parallelization offers (up to) a flat {@code x N} performance improvement, where {@code N} is the
     * number of available cores on machine to which the JVM has access. This gain is only unrealized when there is high
     * thread contention.<br>
     * <br>
     * </li>
     * </ol>
     * 
     * <br>
     * <br>
     * 
     * Parallelizability is due to the fact that {@code AB_ij} - the {@code ij}-th entry of the product {@code AB} - is
     * the dot product of the {@code i}-th row of {@code A} with the {@code j}-th column of {@code B}, all of which can
     * be computed independently of one another.
     * 
     * @param A the left {@link ComplexMatrix}
     * @param transposeA {@code true} if we want to consider {@code A}<sup>T</sup>, {@code false} to consider {@code A}
     *        as-is.
     * @param conjugateA {@code true} if we want to consider the {@link Complex#conjugate() complex conjugation} of
     *        {@code A}, {@code false} to consider {@code A} as-is.
     * @param B the right {@link ComplexMatrix}
     * @param transposeB {@code true} if we want to consider {@code B}<sup>T</sup>, {@code false} to consider {@code B}
     *        as-is.
     * @param conjugateB {@code true} if we want to consider the {@link Complex#conjugate() complex conjugation} of
     *        {@code B}, {@code false} to consider {@code B} as-is.
     * @param parallelism the {@link Parallelism} with which we want to execute the multiplication
     * @return the product {@code A.B} */
    public static ComplexMatrix multiply(final FieldMatrix<Complex> A,
                                         final boolean transposeA,
                                         final boolean conjugateA,
                                         final FieldMatrix<Complex> B,
                                         final boolean transposeB,
                                         final boolean conjugateB,
                                         final Parallelism parallelism) {
        checkMultiplyDimensions(A, transposeA, B, transposeB);

        final int ANumRows = transposeA ? A.getColumnDimension() : A.getRowDimension();    // # rows in A or A^T
        final int BNumCols = transposeB ? B.getRowDimension()    : B.getColumnDimension(); // # columns in B or B^T

        final double[][][] AData = extractDoubleData(A);
        final double[][][] BData = extractDoubleData(B);

        final double[][] ADataRe = AData[0];
        final double[][] ADataIm = AData[1];
        
        final double[][] BDataRe = BData[0];
        final double[][] BDataIm = BData[1];


        final ComplexMatrix answer  = new ComplexMatrix(ANumRows, BNumCols, parallelism);
        
        if (parallelism == Parallelism.PARALLEL) { // conditionally set the parallel flag
            final Stream<int[]> ijPairs =
                    IntStream.range(0, ANumRows)                                                 // IntStream
                             .mapToObj(row -> IntStream.range(0, BNumCols)
                                                      .mapToObj(col -> new int[] { row, col })) // Stream<Stream<int[]>>
                             .flatMap(Function.identity())                                       // Stream<int[]>
                             .parallel();
            
            ijPairs.forEach(rowCol -> { // exec. forEach pair, in parallel
                final int row = rowCol[0];
                final int col = rowCol[1];
                computeAndSaveMultEntry(row, col,
                                        ADataRe, ADataIm, transposeA, conjugateA,
                                        BDataRe, BDataIm, transposeB, conjugateB,
                                        answer);
            });
        }
        else { /* if sequential, we forgo the Stream entirely - rather than just conditionally set the parallel flag -
                * for the sake of simplicity */
            for (int row = 0; row < ANumRows; row++) {
                for (int col = 0; col < BNumCols; col++) {
                    computeAndSaveMultEntry(row, col,
                                            ADataRe, ADataIm, transposeA, conjugateA,
                                            BDataRe, BDataIm, transposeB, conjugateB,
                                            answer);
                }
            }
        }
        
        return answer;
    }
    
    /** Extract from {@code A} the {@code double[][][]} representation of its entries. If {@code A instanceof}
     * {@link ComplexMatrix}, we simply return the real and imaginary array fields of the instance.
     * 
     * @param A the {@link FieldMatrix} from which we want to extract the {@code double[][][]} data
     * @return the {@code double[][][]} data */
    static double[][][] extractDoubleData(final FieldMatrix<Complex> A) {
        if (A.getClass().equals(ComplexMatrix.class)) {
            final ComplexMatrix cast = (ComplexMatrix) A;
            return new double[][][] { cast.Re, cast.Im };
        }

        final int numRows = A.getRowDimension();
        final int numCols = A.getColumnDimension();

        final double[][][] answer = new double[2][numRows][numCols];

        if (A.getClass().equals(AbstractComplexMatrixSlice.class)) {
            final AbstractComplexMatrixSlice cast = (AbstractComplexMatrixSlice) A;
            for (int i = 0; i < numRows; i++) { 
                for (int j = 0; j < numCols; j++) { 
                    answer[0][i][j] = cast.getEntryRe(i, j);
                    answer[1][i][j] = cast.getEntryIm(i, j);
                }
            }
        }
        else {
            for (int i = 0; i < numRows; i++) { 
                for (int j = 0; j < numCols; j++) { 
                    final Complex entry = A.getEntry(i, j);
                    answer[0][i][j] = entry.getReal();
                    answer[1][i][j] = entry.getImaginary();
                }
            }
        }
        
        return answer;
    }
    
    /** Check to ensure that the given {@link ComplexMatrix matrices} dimensions allow for matrix multiplication
     * when taking into consideration whether either is being transposed
     * 
     * @param A the left matrix in the multiplication
     * @param transposeA {@code true} if we want to consider {@code A^T}, {@code false} to consider {@code A} as-is
     * @param B the right matrix in the multiplication
     * @param transposeB {@code true} if we want to consider {@code B^T}, {@code false} to consider {@code B} as-is */
    static void checkMultiplyDimensions(final AnyMatrix A, final boolean transposeA,
                                        final AnyMatrix B, final boolean transposeB) {

        final int leftNumCols = transposeA ? A.getRowDimension()    : A.getColumnDimension();
        final int rghtNumRows = transposeB ? B.getColumnDimension() : B.getRowDimension();

        if (leftNumCols != rghtNumRows) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH,
                                                   leftNumCols, rghtNumRows);
        }
    }

    /** Given two matrices {@code A} and {@code B} being multiplied as {@code A.B} (with possible transposition and/or
     * conjugation of either), compute entry for the given row and column, and save that value.<br>
     * <br>
     * 
     * This method is only safe to use when it is known in advance that no indices will be overwritten, as in
     * {@link #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}.<br>
     * <br>
     * 
     * When using any of the transposition {@code boolean}s, there's no need need to actually compute the transpose, we
     * can just adjust our indices when pulling data from {@code AData} or {@code BData}. Similarly for the conjugation
     * {@code boolean}s, except instead of adjusting the indices, we simply negate the imaginary part of the entry.
     * 
     * @param row the row of the entry to compute
     * @param col the column of the entry to compute
     * @param ADataRe the real parts of the entries of {@code A}
     *      equivalent to the output of MATLAB function real(A)
     * @param ADataIm the imaginary parts of the entries of {@code A}
     *      equivalent to the output of MATLAB function imag(A)
     * @param transposeA {@code A} if we want to use the transpose of {@code A} rather than {@code A} itself
     * @param conjugateA {@code A} if we want to use the {@link Complex#conjugate() complex conjugate} of {@code A}
     *        rather than {@code A} itself
     * @param BDataRe the real parts of the entries of {@code B}
     *      equivalent to the output of MATLAB function real(B)
     * @param BDataIm the imaginary parts of the entries of {@code B}
     *      equivalent to the output of MATLAB function imag(B)
     * @param transposeB {@code B} if we want to use the transpose of {@code B} rather than {@code B} itself
     * @param conjugateB {@code B} if we want to use the {@link Complex#conjugate() complex conjugate} of {@code B}
     *        rather than {@code B} itself
     * @param ABData the {@link ComplexMatrix} to which we will save the value computed here. Of course, this
     *        means that this is modified in place! */
    // CHECKSTYLE.OFF: ParameterNumber
    private static void computeAndSaveMultEntry(final int row, final int col,
                                                final double[][] ADataRe, final double[][] ADataIm,
                                                final boolean transposeA, final boolean conjugateA,
                                                final double[][] BDataRe, final double[][] BDataIm, 
                                                final boolean transposeB, final boolean conjugateB,
                                                final ComplexMatrix ABData) {
        // determine # multiplications to be carried out for this entry; i.e. the "n" in (m x n) . (n x k) :
        final int dotProductLength = transposeA ? ADataRe.length     // # rows of original A
                                                : ADataRe[0].length; // # cols of original A
        double sumRe = 0.0;
        double sumIm = 0.0;
        for (int i = 0; i < dotProductLength; i++) {
            final double AEntryReal      = transposeA ? ADataRe[i][row] : ADataRe[row][i];
            final double AEntryImaginary = transposeA ? ADataIm[i][row] : ADataIm[row][i];

            if (AEntryReal == 0 && AEntryImaginary == 0) {
                continue;
            }

            final double BEntryReal      = transposeB ? BDataRe[col][i] : BDataRe[i][col];
            final double BEntryImaginary = transposeB ? BDataIm[col][i] : BDataIm[i][col];

            if (BEntryReal == 0 && BEntryImaginary == 0) {
                continue;
            }

            final double a = AEntryReal;
            final double b = conjugateA ? -AEntryImaginary : AEntryImaginary;

            final double c = BEntryReal;
            final double d = conjugateB ? -BEntryImaginary : BEntryImaginary;

            final double re = Numerics.complexXYRe(a, b, c, d); // computes a c - b d
            final double im = Numerics.complexXYIm(a, b, c, d); // computes a d + b c

            sumRe += re;
            sumIm += im;
        }

        ABData.Re[row][col] = sumRe;
        ABData.Im[row][col] = sumIm;
    }
    // CHECKSTYLE.ON: ParameterNumber

    /** Post-multiply in-place (i.e., modify directly) {@code A} by {@code smallB} starting at the given indices<br>
     * <br>
     * 
     * This is equivalent to padding {@code smallB} with an identity matrix which makes it compatible with
     * post-multiplication of {@code A}, placing {@code smallB} at the given indices, and post-multiplying {@code A}
     * with that padded matrix.<br>
     * <br>
     * 
     * We don't actually need to construct the aforementioned padded matrix, however, and can instead cleverly compute
     * the changes to {@code A}, as this process only changes a subset of the columns of {@code A}.<br>
     * <br>
     * 
     * E.g.,
     * 
     * <pre>
     * Let startRow = 2, startColumn = 3,
     * 
     * smallB = 
     *     . . .
     *     . . .
     * 
     * and A =
     *     . . . . . . . 
     *     . . . . . . . 
     *       . . . . . . 
     *         . . . . . 
     *           . . . . 
     *             . . . 
     *               . .
     * 
     * </pre>
     * 
     * after calling {@link #postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}, {@code A} will have the form
     * 
     * <pre>
     * 
     * A = 
     *     . . . . . . .         1                     . . . x x x .
     *     . . . . . . .           1                   . . . x x x .
     *       . . . . . .             1 . . .             . . x x x .
     *         . . . . .    *          . . .      =        . x x x .
     *           . . . .                 1                   x x x .
     *             . . .                   1                 x x x .
     *               . .                     1               x x x .
     * </pre>
     * 
     * where the {@code x}'s mark the entries {@code A} that have changed<br>
     * <br>
     * 
     * @param A the {@link ComplexMatrix} that we want to post-multiply <b>in-place</b> by the smaller matrix
     *        {@code smallB}.
     * @param smallB the matrix - with dimensions at most the same as the dimensions of the transpose of {@code A} - by
     *        which we will post-multiply {@code A}. The greater the difference in dimension, the better the performance
     *        gain vs. the standard multiplication.
     * @param startRowIndex the row index where we want to consider {@code smallB} w.r.t. the padding
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     *        
     * @see #preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public static void postMultiplyInPlace(final ComplexMatrix A,
                                           final ComplexMatrix smallB,
                                           final int startRowIndex,
                                           final int startColIndex) {
        final int ANumRows = A.getRowDimension();
        final int ANumCols = A.getColumnDimension();
        
        final int BNumRows = smallB.getRowDimension();
        final int BNumCols = smallB.getColumnDimension();
        
        if (ANumCols < Numerics.max(BNumRows + startRowIndex, BNumCols + startColIndex)) {
            /* we only check against the number of columns in `A` because `smallB` is implicitly a square matrix with
             * dimension compatible with post-multiplication of `A` */
            throw NumericalMethodsException.formatted("Matrix A (%d x %d) cannot be post-multiplied in-place "
                                                      + "by smallB (%d x %d) when starting at indices (%d, %d)",
                                                      ANumRows,
                                                      ANumCols,
                                                      BNumRows,
                                                      BNumCols,
                                                      startRowIndex,
                                                      startColIndex).get();
        }
        
        final double[][][] ADataRef = A.getDataRef();
        final double[][][] BDataRef = smallB.getDataRef();

        final double[][] ADataRe = ADataRef[0];
        final double[][] ADataIm = ADataRef[1];

        final double[][] BDataRe = BDataRef[0];
        final double[][] BDataIm = BDataRef[1];

        final int colLimit = startColIndex + BNumCols;
        final int rowLimit = startRowIndex + BNumRows;
        
        final double[][] newAColsRe = new double[ANumRows][colLimit - startColIndex];
        final double[][] newAColsIm = new double[ANumRows][colLimit - startColIndex];
        
        // Possibly TODO Ryan Moser, 2024-07-13: parallelize this in the same way as the standard ::multiply
        for (int row = 0; row < ANumRows; row++) {
            final double[] ADataRowRe = ADataRe[row];
            final double[] ADataRowIm = ADataIm[row];
            
            final double[] newADataRowRe = newAColsRe[row];
            final double[] newADataRowIm = newAColsIm[row];
            
            for (int col = startColIndex; col < colLimit; col++) {
                computePostMultiplyInPlaceEntry(startRowIndex,
                                                startColIndex,
                                                col,
                                                ADataRowRe,    ADataRowIm,
                                                BDataRe,       BDataIm,
                                                newADataRowRe, newADataRowIm,
                                                rowLimit);
            }
        }
        
        A.setSubMatrix(newAColsRe, newAColsIm, 0, startColIndex);
    }

    /** Compute and store an entry for the new columns computed in
     * {@link #postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}
     * 
     * @param startRowIndex the row index where we want to consider {@code smallB} w.r.t. the padding
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     * @param col the column for which we're computing an entry (no offset)
     * @param ADataRefRowRe the data reference for the row of the real parts of the matrix being post-multiplied
     * @param ADataRefRowIm the data reference for the row of the imaginary parts of the matrix being post-multiplied
     * @param BDataRefRe the data reference for the real parts of the matrix doing the post-multiplication
     * @param BDataRefIm the data reference for the imaginary parts of the matrix doing the post-multiplication
     * @param newAColsRowRe the new columns of the real part of the matrix being post-multiplied
     * @param newAColsRowIm the new columns of the imaginary part of the matrix being post-multiplied
     * @param rowLimit we don't consider elements of the dot product beyond this, as they are all zero. We pass this
     *        value in rather than compute it in place so that we only have to do it once. */
    // CHECKSTYLE.OFF: ParameterNumber
    private static void computePostMultiplyInPlaceEntry(final int startRowIndex,
                                                        final int startColIndex,
                                                        final int col,
                                                        final double[]   ADataRefRowRe, final double[]   ADataRefRowIm,
                                                        final double[][] BDataRefRe,    final double[][] BDataRefIm,
                                                        final double[]   newAColsRowRe, final double[]   newAColsRowIm,
                                                        final int rowLimit) {
        double sumRe = 0.0;
        double sumIm = 0.0;
        boolean coversTheDiagonal = false;
        final int BDataColIndex = col - startColIndex;
        for (int i = startRowIndex; i < rowLimit; i++) {
            coversTheDiagonal |= (i == col);
            
            final double a = ADataRefRowRe[i];
            final double b = ADataRefRowIm[i];
            if (a == 0 && b == 0) {
                continue;
            }
            
            final double c = BDataRefRe[i - startRowIndex][BDataColIndex];
            final double d = BDataRefIm[i - startRowIndex][BDataColIndex];
            if (c == 0 && d == 0) {
                continue;
            }
            
            final double re = Numerics.complexXYRe(a, b, c, d); // computes a c - b d
            final double im = Numerics.complexXYIm(a, b, c, d); // computes a d + b c
            
            sumRe += re;
            sumIm += im;
        }

        /* the following if-block ensures that we include the implicit identity element in current column of the
         * "smallB" matrix. We need to do so whenever the non-zero (implicit) values of the column do not themselves
         * cover the diagonal element of that column (i.e., the element of the column which intersects the diagonal of
         * the implicit identity padding on "smallB") */
        if (!coversTheDiagonal) { /* TODO Ryan Moser, 2024-07-15: rewrite how we account for this so that we add this
                                   * value first, as it is likely smaller than the values we sum when computing the
                                   * row.vector dot product and because we know determine this in advance. I'm leaving
                                   * it at this moment only because I'm tired of fighting with indices. */
            sumRe += ADataRefRowRe[col];
            sumIm += ADataRefRowIm[col];
        }
        
        newAColsRowRe[col - startColIndex] = sumRe;
        newAColsRowIm[col - startColIndex] = sumIm;
    }
    // CHECKSTYLE.OFF: ParameterNumber
    
    /** Pre-multiply in-place (i.e., modify directly) {@code B} by {@code smallA} starting at the given indices.<br>
     * <br>
     * 
     * This is equivalent to padding {@code smallA} with an identity matrix which makes it compatible with
     * pre-multiplication of {@code B}, placing {@code smallA} at the given indices, and pre-multiplying {@code B} with
     * that padded matrix.<br>
     * <br>
     * 
     * We don't actually need to construct the aforementioned padded matrix, however, and can instead cleverly compute
     * the changes to {@code B}, as this process only changes a subset of the rows of {@code B}.<br>
     * <br>
     * 
     * E.g.,
     * 
     * <pre>
     * Let startRow = 2, startColumn = 3,
     * 
     * smallA = 
     *     . . .
     *     . . .
     * 
     * and B =
     *     . . . . . . . 
     *     . . . . . . . 
     *       . . . . . . 
     *         . . . . . 
     *           . . . . 
     *             . . . 
     *               . .
     * 
     * </pre>
     * 
     * after calling {@link #preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}, {@code B} will have the form
     * 
     * <pre>
     * 
     * B = 
     *     1                     . . . . . . .         . . . . . . .
     *       1                   . . . . . . .         . . . . . . .
     *         1 . . .             . . . . . .         x x x x x x x
     *           . . .      *        . . . . .    =    x x x x x x x
     *             1                   . . . .               . . . .
     *               1                   . . .                 . . .
     *                 1                   . .                   . .
     * 
     * </pre>
     * 
     * where the {@code x}'s mark the entries {@code B} that have changed<br>
     * <br>
     * 
     * @param smallA the matrix - with dimensions at most the same as the dimensions of the transpose of {@code B} - by
     *        which we will pre-multiply {@code B}. The greater the difference in dimension, the better the performance
     *        gain vs. the standard multiplication.
     * @param B the {@link ComplexMatrix} that we want to pre-multiply <b>in-place</b> by the smaller matrix
     *        {@code smallA}.
     * @param startRowIndex the row index where we want to consider {@code smallB} w.r.t. the padding
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     * 
     * @see #postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)
     * @see #multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism) */
    public static void preMultiplyInPlace(final ComplexMatrix smallA,
                                          final ComplexMatrix B,
                                          final int startRowIndex,
                                          final int startColIndex) {
        final int ANumRows = smallA.getRowDimension();
        final int ANumCols = smallA.getColumnDimension();
        
        final int BNumRows = B.getRowDimension();
        final int BNumCols = B.getColumnDimension();
        
        if (BNumRows < Numerics.max(ANumRows + startRowIndex, ANumCols + startColIndex)) {
            /* we only check against the number of rows in `B` because `smallA` is implicitly a square matrix with
             * dimension compatible with pre-multiplication of `B` */
            throw NumericalMethodsException.formatted("Matrix B (%d x %d) cannot be pre-multiplied in-place "
                                                      + "by smallA (%d x %d) when starting at indices (%d, %d)",
                                                      BNumRows,
                                                      BNumCols,
                                                      ANumRows,
                                                      ANumCols,
                                                      startRowIndex,
                                                      startColIndex).get();
        }
        
        final double[][][] ADataRef = smallA.getDataRef();
        final double[][][] BDataRef = B.getDataRef();
        
        final double[][] ADataRe = ADataRef[0];
        final double[][] ADataIm = ADataRef[1];

        final double[][] BDataRe = BDataRef[0];
        final double[][] BDataIm = BDataRef[1];

        final int colLimit = startColIndex + ANumCols;
        final int rowLimit = startRowIndex + ANumRows;
        
        final double[][] newBRowsRe = new double[rowLimit - startRowIndex][BNumCols];
        final double[][] newBRowsIm = new double[rowLimit - startRowIndex][BNumCols];

        // Possibly TODO Ryan Moser, 2024-07-13: parallelize this in the same way as the standard ::multiply
        for (int row = startRowIndex; row < rowLimit; row++) {
            final int ADataRowIndex = row - startRowIndex;
            
            final double[] ADataRowRe = ADataRe[ADataRowIndex];
            final double[] ADataRowIm = ADataIm[ADataRowIndex];
            
            final double[] newBDataRowRe = newBRowsRe[ADataRowIndex];
            final double[] newBDataRowIm = newBRowsIm[ADataRowIndex];
            
            for (int col = 0; col < BNumCols; col++) {
                computePreMultiplyInPlaceEntry(startColIndex,
                                               row,
                                               col,
                                               ADataRowRe,    ADataRowIm,
                                               BDataRe,       BDataIm,
                                               newBDataRowRe, newBDataRowIm,
                                               colLimit);
            }
        }
        
        B.setSubMatrix(newBRowsRe, newBRowsIm, startRowIndex, 0);
    }
    
    /** Compute and entry for the new columns computed in
     * {@link #preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}
     * 
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     * @param row the row for which we're computing an entry (no offset)
     * @param col the column for which we're computing an entry (no offset)
     * @param ADataRefRowRe the data reference for the row of the real parts of the matrix being pre-multiplied
     * @param ADataRefRowIm the data reference for the row of the imaginary parts of the matrix being pre-multiplied
     * @param BDataRefRe the data reference for the real parts of the matrix doing the pre-multiplication
     * @param BDataRefIm the data reference for the imaginary parts of the matrix doing the pre-multiplication
     * @param newBRowRe the new row of the real part of the matrix being pre-multiplied
     * @param newBRowIm the new row of the imaginary part of the matrix being pre-multiplied
     * @param colLimit we don't consider elements of the dot product beyond this, as they are all zero. We pass this
     *        value in rather than compute it in place so that we only have to do it once. */
    // CHECKSTYLE.OFF: ParameterNumber
    private static void computePreMultiplyInPlaceEntry(final int startColIndex,
                                                       final int row,
                                                       final int col,
                                                       final double[]   ADataRefRowRe, final double[]   ADataRefRowIm,
                                                       final double[][] BDataRefRe,    final double[][] BDataRefIm,
                                                       final double[]   newBRowRe,     final double[]   newBRowIm,
                                                       final int colLimit) {
        double sumRe = 0.0;
        double sumIm = 0.0;
        boolean coversTheDiagonal = false;
        for (int i = startColIndex; i < colLimit; i++) {
            coversTheDiagonal |= (i == row);
            
            final double a = ADataRefRowRe[i - startColIndex];
            final double b = ADataRefRowIm[i - startColIndex];
            if (a == 0 && b == 0) {
                continue;
            }
            
            final double c = BDataRefRe[i][col];
            final double d = BDataRefIm[i][col];
            if (c == 0 && d == 0) {
                continue;
            }
            
            final double re = Numerics.complexXYRe(a, b, c, d); // computes a c - b d
            final double im = Numerics.complexXYIm(a, b, c, d); // computes a d + b c
            
            sumRe += re;
            sumIm += im;
        }
        
        /* the following if-block ensures that we include the implicit identity element in current row of the "smallA"
         * matrix. We need to do so whenever the non-zero (implicit) values of the row do not themselves cover the
         * diagonal element of that row (i.e., the element of the row which intersects the diagonal of the implicit
         * identity padding on "smallA") */
        if (!coversTheDiagonal) { /* TODO Ryan Moser, 2024-07-15: rewrite how we account for this so that we add this
                                   * value first, as it is likely smaller than the values we sum when computing the
                                   * row.vector dot product and because we know determine this in advance. I'm leaving
                                   * it at this moment only because I'm tired of fighting with indices. */
            sumRe += BDataRefRe[row][col];
            sumIm += BDataRefIm[row][col];
        }
        
        newBRowRe[col] = sumRe;
        newBRowIm[col] = sumIm;
    }
    // CHECKSTYLE.ON: ParameterNumber

    @Override
    public boolean isSquare() {
        return this.getRowDimension() == this.getColumnDimension();
    }

    @Override
    public int getRowDimension() {
        return this.numRows;
    }

    @Override
    public int getColumnDimension() {
        return this.numCols;
    }

    /**
     * Check if the row index is valid and throw an exception if not.
     * @param row the row index to check.
     */
    public void checkRowIndex(final int row) {
        MatrixUtils.checkRowIndex(this, row);
    }

    /**
     * Check if the column index is valid and throw an exception if not.
     * @param col the column index.
     */
    public void checkColumnIndex(final int col) {
        MatrixUtils.checkColumnIndex(this, col);
    }
    
    /** @return the <em>maximum possible</em> rank of a matrix of this size: {@code min(numRows, numCols)}. This is
     *         equivalent to the diagonal dimension of a matrix. */
    public int getMaxRank() {
        return this.maxRank;
    }
    
    /** @return {@code true} if {@code this} has strictly fewer columns than it does rows, {@code false} otherwise
     * 
     * @see #isSquare()
     * @see #isWide() */
    public boolean isTall() {
        return this.getColumnDimension() < this.getRowDimension();
    }
    
    /** @return {@code true} if {@code this} has strictly fewer rows than it does columns, {@code false} otherwise
     * 
     * @see #isSquare()
     * @see #isTall() */
    public boolean isWide() {
        return this.getRowDimension() < this.getColumnDimension();
    }
    
    /**
     * Check if {@code double[][]'s'} have the same number of rows and columns.
     *
     * @param left Left hand side matrix.
     * @param right Right hand side matrix.
     * @throws MathIllegalArgumentException if doubles don't have the same number of rows or columns
     */
    static void checkSameDoubleDimension(final double[][] left, final double[][] right)
        throws MathIllegalArgumentException {
        if (left.length != right.length) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH,
                                                   left.length, right.length);
        }
        else if (left[0].length != right[0].length) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH,
                                                   left[0].length, right[0].length);
        }
    }


    @Override
    public Field<Complex> getField() {
        return ComplexField.getInstance();
    }

    @Override
    public ComplexMatrix createMatrix(final int rowDimension, final int columnDimension) {
        return new ComplexMatrix(rowDimension, columnDimension);
    }
    
    // TODO Ryan Moser, 2024-07-02: make parallel START
    
    /** Divide each entry of this by {@code z}
     * 
     * @param z the divisor
     * @return {@code (1 / z) * this} */
    public ComplexMatrix dividedBy(final Complex z) {
        final ComplexMatrix answer = new ComplexMatrix(this.getRowDimension(), this.getColumnDimension(), 
                                                       this.parallelism);
        
        for (int i = 0; i < getRowDimension(); i++) {
            for (int j = 0; j < getColumnDimension(); j++) {
                final Complex ijByZ = this.getEntry(i, j).divide(z); /* Someday TODO Ryan Moser, 2024-09-016:
                                                                      * performance here would be improved by copying
                                                                      * the implementation of Complex::divide locally
                                                                      * (or into a static utility), but skipping
                                                                      * creating a new Complex instance and instead
                                                                      * setting the Re and Im parts separately. This
                                                                      * method is not yet a performance bottleneck,
                                                                      * though. */
                answer.Re[i][j] = ijByZ.getReal();
                answer.Im[i][j] = ijByZ.getImaginary();
            }
        }
        
        return answer;
    }
    
    /** Divide each entry of this by {@code x}
     * 
     * @param x the divisor
     * @return {@code (1 / x) * this} */
    public ComplexMatrix dividedBy(final double x) {
        final ComplexMatrix answer = new ComplexMatrix(this.getRowDimension(), this.getColumnDimension(), 
                                                       this.parallelism);
        
        for (int i = 0; i < getRowDimension(); i++) {
            for (int j = 0; j < getColumnDimension(); j++) {
                if (x == 0 && this.getEntry(i, j).isZero()) {
                    throw new NullArgumentException();
                }
                answer.Re[i][j] = getEntryRe(i, j) / x;
                answer.Im[i][j] = getEntryIm(i, j) / x;
            }
        }
        
        return answer;
    }
    
    @Override
    public ComplexMatrix add(final FieldMatrix<Complex> m) {
        final ComplexMatrix answer = new ComplexMatrix(this.getRowDimension(), this.getColumnDimension(), 
                                                       this.parallelism);
        MatrixUtils.checkAdditionCompatible(this, m);

        if (m instanceof ComplexMatrix cast) {
            for (int i = 0; i < getRowDimension(); i++) {
                for (int j = 0; j < getColumnDimension(); j++) {
                    answer.Re[i][j] = this.getEntryRe(i, j) + cast.getEntryRe(i, j);
                    answer.Im[i][j] = this.getEntryIm(i, j) + cast.getEntryIm(i, j);
                }
            }
        }
        else {
            for (int i = 0; i < getRowDimension(); i++) {
                for (int j = 0; j < getColumnDimension(); j++) {
                    final Complex ij = m.getEntry(i, j);
                    answer.Re[i][j] = this.getEntryRe(i, j) + ij.getReal();
                    answer.Im[i][j] = this.getEntryIm(i, j) + ij.getImaginary();
                }
            }
        }
        
        return answer;
    }
    
    @Override
    public ComplexMatrix copy() {
        return new ComplexMatrix(this, true);
    }
    
    @Override
    public ComplexMatrix power(final int p) {
        final Complex[][] complexData = this.getData();
        final FieldMatrix<Complex> answer = new Array2DRowFieldMatrix<>(complexData).power(p);
        
        return new ComplexMatrix(answer, false);
    }
    
    @Override
    public ComplexMatrix scalarAdd(final Complex d) {
        return this.scalarAdd(d.getReal(), d.getImaginary());
    }

    /** Increment each entry of this matrix.<br>
     * The same as {@link #scalarAdd(Complex)}, but we take the real and imaginary parts separately.
     * 
     * @param re the real part to add to each entry
     * @param im the imaginary part to add to each entry
     * @return the new {@link ComplexMatrix} */
    public ComplexMatrix scalarAdd(final double re, final double im) {
        final ComplexMatrix answer  = new ComplexMatrix(getRowDimension(), getColumnDimension(), this.parallelism);
    
        for (int i = 0; i < getRowDimension(); i++) {
            for (int j = 0; j < getColumnDimension(); j++) {
                answer.Re[i][j] = getEntryRe(i, j) + re;
                answer.Im[i][j] = getEntryIm(i, j) + im;
            }
        }
        return answer;
    }

    @Override
    public ComplexMatrix scalarMultiply(final Complex scalar) {
        final double realPart = scalar.getReal();
        final double imaginaryPart = scalar.getImaginary();
        return this.scalarMultiply(realPart, imaginaryPart);
    }

    /**
     * Multiply each entry by {@code d}.
     *
     * @param d Value to multiply all entries by.
     * @return {@code d} * {@code this}.
     */
    public ComplexMatrix scalarMultiply(final double d) {
        final ComplexMatrix answer  = new ComplexMatrix(getRowDimension(), getColumnDimension(), this.parallelism);
    
        for (int i = 0; i < getRowDimension(); i++) {
            for (int j = 0; j < getColumnDimension(); j++) {
                answer.Re[i][j] = getEntryRe(i, j) * d;
                answer.Im[i][j] = getEntryIm(i, j) * d;
            }
        }

        return answer;
    }

    /**
     * Multiply each entry by a scalar consisting of 
     *  {@code double realPart} and {@code double imaginaryPart}.
     * 
     * @param realPart {@code double} real part of scalar to multiply all entries by.
     * @param imaginaryPart {@code double} imaginary part of scalar to multiply all entries by.
     * @return {@code scalar} * {@code this}.
     */
    public ComplexMatrix scalarMultiply(final double realPart, final double imaginaryPart) {
        final ComplexMatrix answer  = new ComplexMatrix(getRowDimension(), getColumnDimension(), this.parallelism);
    
        final double c = realPart;
        final double d = imaginaryPart;
        for (int i = 0; i < getRowDimension(); i++) {
            for (int j = 0; j < getColumnDimension(); j++) {
                final double a = getEntryRe(i, j);
                final double b = getEntryIm(i, j);
        
                answer.Re[i][j] = Numerics.complexXYRe(a, b, c, d); // computes a c - b d       
                answer.Im[i][j] = Numerics.complexXYIm(a, b, c, d); // computes a d + b c
            }
        }

        return answer;
    }

    @Override
    public ComplexMatrix subtract(final FieldMatrix<Complex> m) {
        MatrixUtils.checkSubtractionCompatible(this, m);
        final ComplexMatrix answer = new ComplexMatrix(this.getRowDimension(), this.getColumnDimension(),
                                                       this.parallelism);
        
        if (m instanceof ComplexMatrix cast) {
            for (int i = 0; i < getRowDimension(); i++) {
                for (int j = 0; j < getColumnDimension(); j++) {
                    answer.Re[i][j] = this.getEntryRe(i, j) - cast.Re[i][j];
                    answer.Im[i][j] = this.getEntryIm(i, j) - cast.Im[i][j];
                }
            }
            
            return answer;
        }
        else {
            for (int i = 0; i < getRowDimension(); i++) {
                for (int j = 0; j < getColumnDimension(); j++) {
                    final Complex ij = m.getEntry(i, j);
                    answer.Re[i][j] = this.getEntryRe(i, j) - ij.getReal();
                    answer.Im[i][j] = this.getEntryIm(i, j) - ij.getImaginary();
                }
            }
        }
        
        return answer;
    }
    
    // TODO Ryan Moser, 2024-07-02: make parallel END
    
    @Override
    public ComplexMatrix getSubMatrix(final int startRow,
                                     final int endRow,
                                     final int startColumn,
                                     final int endColumn) {
        final double[][][] subMatrixData = getSubMatrixData(startRow, endRow, startColumn, endColumn);
        return new ComplexMatrix(subMatrixData[0], subMatrixData[1], false, parallelism);
    }
    
    /** Get the {@code double[][][]} data array of a sub-matrix of this {@link ComplexMatrix}. Rows and columns are
     * counted from 0 to n - 1.
     *
     * @param startRow Initial row index
     * @param endRow Final row index (inclusive)
     * @param startColumn Initial column index
     * @param endColumn Final column index (inclusive)
     * @return the {@code double[][][]} containing the data of the specified rows and columns */
    public double[][][] getSubMatrixData(final int startRow,
                                         final int endRow,
                                         final int startColumn,
                                         final int endColumn) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        final int rowCount    = endRow - startRow + 1;
        final int columnCount = endColumn - startColumn + 1;
        
        final double[][][] subMatrixData = new double[2][rowCount][columnCount];
        this.copySubMatrix(startRow, endRow, startColumn, endColumn, subMatrixData[0], subMatrixData[1]);
        
        return subMatrixData;
    }
    
    /** Get the {@code double[][][]} data array of a sub-matrix of this {@link ComplexMatrix}. Rows and columns are
     * counted from 0 to n - 1.
     *
     * @param selectedRows Initial row index -> Final row index (inclusive)
     * @param selectedColumns Initial column index -> Final column index (inclusive)
     * @return the {@code double[][][]} containing the data of the specified rows and columns.
     * @implNote the implementation and JavaDoc are nearly identical to the that of
     *           {@link Array2DRowFieldMatrix#getSubMatrix(int, int, int, int)}
     * @see Array2DRowFieldMatrix#getSubMatrix(int, int, int, int) */
    public double[][][] getSubMatrixData(final int[] selectedRows, final int[] selectedColumns) {
        MatrixUtils.checkSubMatrixIndex(this, selectedRows, selectedColumns);
        
        final double[][][] subMatrixData = new double[2][selectedRows.length][selectedColumns.length];
        this.copySubMatrix(selectedRows, selectedColumns, subMatrixData[0], subMatrixData[1]);
        
        return subMatrixData;
    }

    /**
     * @param startRow the start row of the slice
     * @param endRow the end row of the slice
     * @param startColumn the start column of the slice
     * @param endColumn the end column of the slice
     * @return a {@link ComplexMatrixIndexSlice} derived from {@code this}
     */
    public ComplexMatrixIndexSlice slice(final int startRow,
                                         final int endRow,
                                         final int startColumn,
                                         final int endColumn) {
        return new ComplexMatrixIndexSlice(this, startRow, endRow, startColumn, endColumn);
    }

    /**
     * @param rows the rows to include in the slice, in order
     * @param columns the columns to include in the slice, in order
     * @return a {@link ComplexMatrixArraySlice} derived from {@code this}
     */
    public ComplexMatrixArraySlice slice(final int[] rows, final int[] columns) {
        return new ComplexMatrixArraySlice(this, rows, columns);
    }

    /**
     * Get a reference to the underlying data array.
     * This methods returns fresh copy of internal data.
     *
     * @return the 3-dimensional array of entries where {@code double[0][][]} is {@code this.Re}
     * and {@code double[1][][]} is {@code this.Im}
     */
    public double[][][] getDataRef() {
        return new double[][][] { this.Re, this.Im }; 
    }

    /**
     * Get a reference to the underlying data array.
     * This methods returns fresh copy of internal data.
     * 
     * @return the 2-dimensional array of complex entries.
     */
    @Override
    public Complex[][] getData() {
        final Complex[][] data = new Complex[this.getRowDimension()][this.getColumnDimension()];
        for (int i = 0; i < data.length; ++i) {
            final Complex[] dataI = data[i];
            for (int j = 0; j < dataI.length; ++j) {
                dataI[j] = this.getEntry(i, j);
            }
        }

        return data;
    }

    @Override
    public ComplexMatrix getSubMatrix(final int[] selectedRows, final int[] selectedColumns) { 
        final double[][][] subMatrixData = getSubMatrixData(selectedRows, selectedColumns);
        return new ComplexMatrix(subMatrixData[0], subMatrixData[1], false, this.parallelism);
    }

    @Override
    public void copySubMatrix(final int startRow,    final int endRow, 
                              final int startColumn, final int endColumn, 
                              final Complex[][] destination) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        final int rowsCount    = endRow + 1 - startRow;
        final int columnsCount = endColumn + 1 - startColumn;
        if ((destination.length < rowsCount) || (destination[0].length < columnsCount)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   destination.length, destination[0].length,
                                                   rowsCount, columnsCount);
        }
        
        for (int i = startRow; i <= endRow; i++) {
            for (int j = startColumn; j <= endColumn; j++) {
                destination[i - startRow][j - startColumn] = this.getEntry(i, j);
            }
        }
    }

    @Override
    public void copySubMatrix(final int[] selectedRows, 
                              final int[] selectedColumns, 
                              final Complex[][] destination) {
        MatrixUtils.checkSubMatrixIndex(this, selectedRows, selectedColumns);
        if ((destination.length < selectedRows.length) 
                || (destination[0].length < selectedColumns.length)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   destination.length, destination[0].length,
                                                   selectedRows.length, selectedColumns.length);
        }
        
        for (int i = 0; i < selectedRows.length; i++) {
            final Complex[] destinationI = destination[i];
            for (int j = 0; j < selectedColumns.length; j++) {
                destinationI[j] = getEntry(selectedRows[i], selectedColumns[j]);
            }
        }
    }

    /** Copy a sub-matrix of this {@link ComplexMatrix} into the destination array. The same as
     * {@link #copySubMatrix(int, int, int, int, Complex[][])}, but copy into a {@code double[][]} representation
     * instead.
     * 
     * @param startRow Initial row index.
     * @param endRow Final row index (inclusive).
     * @param startColumn Initial column index.
     * @param endColumn Final column index (inclusive).
     * @param destinationRe The array where the real parts of submatrix data should be copied 
     * @param destinationIm The array where the imaginary parts of submatrix data should be copied  */
    public void copySubMatrix(final int startRow,    final int endRow, 
                              final int startColumn, final int endColumn, 
                              final double[][] destinationRe, final double[][] destinationIm) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        checkSameDoubleDimension(destinationRe, destinationIm);
        final int rowsCount    = endRow + 1 - startRow;
        final int columnsCount = endColumn + 1 - startColumn;
        if ((destinationRe.length < rowsCount) || (destinationRe[0].length < columnsCount)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   destinationRe.length, destinationRe[0].length,
                                                   rowsCount, columnsCount);
        }
        
        for (int i = startRow; i <= endRow; i++) {
            for (int j = startColumn; j <= endColumn; j++) {
                destinationRe[i - startRow][j - startColumn] = this.getEntryRe(i, j);
                destinationIm[i - startRow][j - startColumn] = this.getEntryIm(i, j);
            }
        }
    }
    
    /** Copy a sub-matrix of this {@link ComplexMatrix} into the destination array. The same as
     * {@link #copySubMatrix(int[], int[], Complex[][])}, but copy into two {@code double[][]} representation instead.
     * 
     * @param selectedRows the row indices to copy
     * @param selectedColumns the column indices to copy
     * @param destinationRe {@code double[][]} where the real part of the submatrix data should be copied 
     * @param destinationIm {@code double[][]} where the imaginary part of the submatrix data should be copied
     *      (if larger than rows/columns counts, only the upper-left part will be used) 
     * */
    public void copySubMatrix(final int[] selectedRows, 
                              final int[] selectedColumns, 
                              final double[][] destinationRe,
                              final double[][] destinationIm) {
        MatrixUtils.checkSubMatrixIndex(this, selectedRows, selectedColumns);
        checkSameDoubleDimension(destinationRe, destinationIm);
        if ((destinationRe.length < selectedRows.length) 
                || (destinationRe[0].length < selectedColumns.length)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   destinationRe.length, destinationRe[0].length,
                                                   selectedRows.length, selectedColumns.length);
        }
        
        for (int i = 0; i < selectedRows.length; i++) {
            for (int j = 0; j < selectedColumns.length; j++) {
                destinationRe[i][j] = this.getEntryRe(selectedRows[i], selectedColumns[j]);
                destinationIm[i][j] = this.getEntryIm(selectedRows[i], selectedColumns[j]);
            }
        }
    }

    @Override
    public void setSubMatrix(final Complex[][] subMatrix, 
                             final int row, 
                             final int column) {
        final int nRows = subMatrix.length;
        if (nRows == 0) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.AT_LEAST_ONE_ROW);
        }

        final int nCols = subMatrix[0].length;
        if (nCols == 0) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.AT_LEAST_ONE_COLUMN);
        }

        for (int r = 1; r < nRows; ++r) {
            if (subMatrix[r].length != nCols) {
                throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH,
                                                       nCols, subMatrix[r].length);
            }
        }

        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        this.checkRowIndex(nRows + row - 1);
        this.checkColumnIndex(nCols + column - 1);

        for (int i = 0; i < nRows; ++i) {
            for (int j = 0; j < nCols; ++j) {
                unsafeSetEntry(row + i, column + j, subMatrix[i][j].getReal(), subMatrix[i][j].getImaginary());
            }
        }
    }

    /**
     * Set the entries to starting at ({@code row},{@code column}) in {@code this}
     * to the values designated in sub-matrix {@code subMatrix}
     * 
     * @param subMatrixRe array with desired re values
     * @param subMatrixIm array with desired im values
     * @param row the row to start setting the sub-matrix
     * @param column the column to start setting the sub-matrix
     * @throws MathIllegalArgumentException if {@code subMatrix} has one row
     * @throws MathIllegalArgumentException if {@code subMatrix} has one column
     * @throws MathIllegalArgumentException if the array size does not match
     */
    public void setSubMatrix(final double[][] subMatrixRe,
                             final double[][] subMatrixIm, 
                             final int row, 
                             final int column) {
        checkSameDoubleDimension(subMatrixRe, subMatrixIm);

        final int nRows = subMatrixRe.length;
        if (nRows == 0) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.AT_LEAST_ONE_ROW);
        }

        final int nCols = subMatrixRe[0].length;
        if (nCols == 0) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.AT_LEAST_ONE_COLUMN);
        }

        for (int r = 1; r < nRows; ++r) {
            if (subMatrixRe[r].length != nCols) {
                throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH,
                                                        nCols, subMatrixRe[r].length);
            }
        }

        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        this.checkRowIndex(nRows + row - 1);
        this.checkColumnIndex(nCols + column - 1);
        
        for (int i = 0; i < nRows; ++i) {
            final double[] subMatrixRei = subMatrixRe[i];
            final double[] subMatrixImi = subMatrixIm[i];
            for (int j = 0; j < nCols; ++j) {
                unsafeSetEntry(row + i, column + j, subMatrixRei[j], subMatrixImi[j]);
            }
        }
    }

    @Override
    public ComplexMatrix getRowMatrix(final int row) {
        this.checkRowIndex(row);
        final int nCols = getColumnDimension();
        final double[][][] out = new double[2][1][nCols];
        for (int i = 0; i < nCols; ++i) {
            out[0][0][i] = this.getEntryRe(row, i);
            out[1][0][i] = this.getEntryIm(row, i);
        }

        return new ComplexMatrix(out[0], out[1]);
    }

    @Override
    public void setRowMatrix(final int row, final FieldMatrix<Complex> matrix) {
        this.checkRowIndex(row);
        final int nCols = getColumnDimension();
        if ((matrix.getRowDimension() != 1) 
                || (matrix.getColumnDimension() != nCols)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   matrix.getRowDimension(), matrix.getColumnDimension(),
                                                   1, nCols);
        }
        if (matrix instanceof ComplexMatrix cast) {
            final double[][][] castData = cast.getDataRef();
            this.setRowMatrix(row, castData[0], castData[1]);
        }
        else {
            for (int i = 0; i < nCols; ++i) {
                setEntry(row, i, matrix.getEntry(0, i));
            }
        }
    }

    /** Set the entries in row number {@code row} as a row matrix.<br>
     * The same as {@link #setRowMatrix(int, FieldMatrix)}, but with a {@code double[][]} representation.
     *
     * @param row Row to be set
     * @param arrRe real values of row matrix 
     * @param arrIm imaginary values of row matrix
     *  (must have one row and the same number of columns as the instance). */
    public void setRowMatrix(final int row, final double[][] arrRe, final double[][] arrIm) {
        this.checkRowIndex(row);
        checkSameDoubleDimension(arrRe, arrIm);
        final int nCols = arrRe[0].length;
        if ((arrRe.length != 1) 
                || (arrRe[0].length != nCols)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   arrRe.length, arrRe[0].length,
                                                   1, nCols);
        }
        for (int i = 0; i < nCols; ++i) {
            unsafeSetEntry(row, i, arrRe[0][i], arrIm[0][i]);
        }
    }

    @Override
    public ComplexMatrix getColumnMatrix(final int column) {
        this.checkColumnIndex(column);
        final int nRows = getRowDimension();

        final ComplexMatrix out = createMatrix(nRows, 1);
        for (int i = 0; i < nRows; ++i) {
            final Complex iCol = getEntry(i, column);
            out.unsafeSetEntry(i, 0, iCol.getReal(), iCol.getImaginary());
        }

        return out;
    }
    
    @Override
    public void setColumnMatrix(final int column, final FieldMatrix<Complex> matrix) {
        if (matrix instanceof ComplexMatrix cast) {
            final double[][][] castData = cast.getDataRef();
            this.setColumnMatrix(column, castData[0], castData[1]);
        }
        else {
            this.checkColumnIndex(column);
            final int nRows = getRowDimension();

            if ((matrix.getRowDimension() != nRows) 
                    || (matrix.getColumnDimension() != 1)) {
                throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                       matrix.getRowDimension(), matrix.getColumnDimension(),
                                                       nRows, 1);
            }

            for (int i = 0; i < nRows; ++i) {
                setEntry(i, column, matrix.getEntry(i, 0));
            }
        }
    }

    /** Set the entries in column number {@code column} as a column matrix.<br>
     * The same as {@link #setColumnMatrix(int, FieldMatrix)}, but with a {@code double[][]} representation.
     *
     * @param column Column to be set.
     * @param arrRe real values of column matrix 
     * @param arrIm imaginary values of column matrix
     *  (must have one column and the same number of rows as the instance). */
    public void setColumnMatrix(final int column, final double[][] arrRe, final double[][] arrIm) {
        this.checkColumnIndex(column);
        checkSameDoubleDimension(arrRe, arrIm);
        final int nRows = arrRe.length;
        if ((arrRe.length != nRows) 
                || (arrRe[0].length != 1)) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   arrRe.length, arrRe[0].length,
                                                   nRows, 1);
        }
        for (int i = 0; i < nRows; ++i) {
            unsafeSetEntry(i, column, arrRe[i][0], arrIm[i][0]);
        }
    }

    @Override
    public ComplexVector getRowVector(final int row) {
        return new ComplexVector(getRow(row));
    }

    @Override
    public void setRowVector(final int row, final FieldVector<Complex> vector) {
        this.checkRowIndex(row);
        final int nCols = getColumnDimension();
        if (vector.getDimension() != nCols) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   1, vector.getDimension(),
                                                   1, nCols);
        }
        for (int i = 0; i < nCols; ++i) {
            setEntry(row, i, vector.getEntry(i));
        }
    }

    @Override
    public ComplexVector getColumnVector(final int column) {
        return new ComplexVector(getColumn(column));
    }

    @Override
    public void setColumnVector(final int column, final FieldVector<Complex> vector) {
        this.checkColumnIndex(column);
        final int nRows = getRowDimension();
        if (vector.getDimension() != nRows) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   vector.getDimension(), 1,
                                                   nRows, 1);
        }
        for (int i = 0; i < nRows; ++i) {
            setEntry(i, column, vector.getEntry(i));
        }

    }

    @Override
    public Complex[] getRow(final int row) {
        this.checkRowIndex(row);
        final int nCols = getColumnDimension();
        final Complex[] out = new Complex[nCols];
        
        final Complex[][] complexRow = this.getData();
        System.arraycopy(complexRow[row], 0, out, 0, nCols);

        return out;
    }

    @Override
    public void setRow(final int row, final Complex[] array) {
        this.checkRowIndex(row);
        final int nCols = getColumnDimension();
        if (array.length != nCols) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   1, array.length, 1, nCols);
        }

        for (int i = 0; i < nCols; ++i) {
            setEntry(row, i, array[i]);
        }
    }

    @Override
    public Complex[] getColumn(final int column) {
        this.checkColumnIndex(column);
        final int nRows = getRowDimension();
        final Complex[] out = new Complex[nRows];
        for (int i = 0; i < nRows; ++i) {
            out[i] = getEntry(i, column);
        }

        return out;
    }
    
    /** Return a {@code double[][]} representation of a sub-column of this {@link ComplexMatrix}. This mimics the
     *  functionality of {@link ComplexVector#getSubVector(int, int)}.
     * @param startRowInc the starting row index of the column (inclusive)
     * @param colIndex the index of the column from which we want to extract a sub-column
     * @param numElts the number of elements to extract in the column
     * 
     * @return the {@code double[][]} */
    public double[][] getDoubleSubColumn(final int startRowInc, final int colIndex, final int numElts) {
        this.checkColumnIndex(colIndex);
        this.checkRowIndex(startRowInc);
        this.checkRowIndex(startRowInc + numElts - 1);
        
        final double[][] answer = new double[2][numElts];
        for (int i = 0; i < numElts; i++) {
            answer[0][i] = getEntryRe(i + startRowInc, colIndex);
            answer[1][i] = getEntryIm(i + startRowInc, colIndex);
        }
        
        return answer;
    }
    
    @Override
    public void setColumn(final int column, final Complex[] array) {
        this.checkColumnIndex(column);
        final int nRows = getRowDimension();
        if (array.length != nRows) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.DIMENSIONS_MISMATCH_2x2,
                                                   array.length, 1, nRows, 1);
        }
        for (int i = 0; i < nRows; ++i) {
            setEntry(i, column, array[i]);
        }
    }
    
    @Override
    public Complex getEntry(final int row, final int column) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);

        return new Complex(getEntryRe(row, column), getEntryIm(row, column));
    }
    
    /** Get the real part of the entry at {@code (row, column)}
     * 
     * @param row the row to query
     * @param column the column to query
     * @return the real part of the entry at {@code (row, column)} */
    public double getEntryRe(final int row, final int column) {
        return this.Re[row][column];
    }
    
    /** Get the imaginary part of the entry at {@code (row, column)}
     * 
     * @param row the row to query
     * @param column the column to query
     * @return the imaginary part of the entry at {@code (row, column)} */
    public double getEntryIm(final int row, final int column) {
        return this.Im[row][column];
    }
    
    @Override
    public void setEntry(final int row, final int column, final Complex value) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        
        unsafeSetEntryRe(row, column, value.getReal());
        unsafeSetEntryIm(row, column, value.getImaginary());
    }
    
    /** Set an entry of this {@link ComplexMatrix} without manually checking the indices against the dimensions of this.
     * This should only be used when it is already known that the indices are in bound.
     * 
     * @param row the row of the entry to be set
     * @param column the column of the entry to be set
     * @param re the real portion of the complex value to set
     * @param im the imaginary portion of the complex value to set */
    public void unsafeSetEntry(final int row, final int column, final double re, final double im) {
        unsafeSetEntryRe(row, column, re);
        unsafeSetEntryIm(row, column, im);
    }

    /** Set the real component of an entry of this {@link ComplexMatrix} without manually checking the indices against 
     * the dimensions of this. This should only be used when it is already known that the indices are in bound.
     * 
     * @param row the row of the entry to be set
     * @param column the column of the entry to be set
     * @param re the real portion of the complex value to set */
    public void unsafeSetEntryRe(final int row, final int column, final double re) {
        this.Re[row][column] = re;
    }

    /** Set the imaginary component of an entry of this {@link ComplexMatrix} without manually checking the indices 
     * against the dimensions of this. This should only be used when it is already known that the indices are in bound.
     * 
     * @param row the row of the entry to be set
     * @param column the column of the entry to be set
     * @param im the imaginary portion of the complex value to set */
    public void unsafeSetEntryIm(final int row, final int column, final double im) {
        this.Im[row][column] = im;
    }

    /** Increment desired value in {@code this} by {@code Complex} value
     * 
     * @param row n row to set value
     * @param column m column to set value
     * @param increment Complex value
     */
    @Override
    public void addToEntry(final int row, final int column, final Complex increment) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        addToEntryUnsafe(row, column, increment.getReal(), increment.getImaginary());
    }

    /** Increment desired value in {@code this} by a complex value specified as real and imaginary parts
     * 
     * @param row n row to set value
     * @param column m column to set value
     * @param re the real portion of the increment
     * @param im the imaginary portion of the increment
     */
    public void addToEntry(final int row, final int column, final double re, final double im) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        this.addToEntryUnsafe(row, column, re, im);
    }
    
    /** Increment desired value in {@code this} by a complex value specified as real and imaginary parts. Does not do
     * any manual index checking. This should only be used when we know in advance that the indices are safe.
     * 
     * @param row n row to set value
     * @param column m column to set value
     * @param re the real portion of the increment
     * @param im the imaginary portion of the increment */
    public void addToEntryUnsafe(final int row, final int column, final double re, final double im) {
        unsafeSetEntryRe(row, column, getEntryRe(row, column) + re);
        unsafeSetEntryIm(row, column, getEntryIm(row, column) + im);
    }
    
    /** Subtract from the specified entry by a complex value specified as separate real and imaginary parts.
     * 
     * @param row n row to set value
     * @param column m column to set value
     * @param re the real part of the complex subtraction
     * @param im the imaginary part of the complex subtraction */
    public void subtractFromEntry(final int row, final int column, final double re, final double im) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        subtractFromEntryUnsafe(row, column, re, im);
    }
    
    /** Subtract from the specified entry by a complex value specified as separate real and imaginary parts.<br>
     * Does not do any manual index checking. This should only be used when we know in advance that the indices are
     * safe.
     * 
     * @param row n row to set value
     * @param column m column to set value
     * @param re the real part of the complex subtraction
     * @param im the imaginary part of the complex subtraction */
    public void subtractFromEntryUnsafe(final int row, final int column, final double re, final double im) {
        addToEntryUnsafe(row, column, -re, -im);
    }
    
    @Override
    public void multiplyEntry(final int row, final int column, final Complex factor) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);

        final double a = getEntryRe(row, column);
        final double b = getEntryIm(row, column);
        
        final double c = factor.getReal();
        final double d = factor.getImaginary();

        unsafeSetEntryRe(row, column, Numerics.complexXYRe(a, b, c, d)); // computes a c - b d       
        unsafeSetEntryIm(row, column, Numerics.complexXYIm(a, b, c, d)); // computes a d + b c
    }

    /** Multiply an entry in the specified row and column.
     *
     * @param row Row location of entry to be set.
     * @param column Column location of entry to be set.
     * @param re the real part of the complex factor
     * @param im the imaginary part of the complex factor
     * @throws MathIllegalArgumentException if the row or column index is not valid.
     */
    public void multiplyEntry(final int row, final int column, final double re, final double im) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);

        final double a = getEntryRe(row, column);
        final double b = getEntryIm(row, column);
        
        final double c = re;
        final double d = im;

        unsafeSetEntryRe(row, column, Numerics.complexXYRe(a, b, c, d)); // computes a c - b d       
        unsafeSetEntryIm(row, column, Numerics.complexXYIm(a, b, c, d)); // computes a d + b c
    }
    
    /** Multiply an entry in the specified row and column by a real number.
     *
     * @param row Row location of entry to be set.
     * @param column Column location of entry to be set.
     * @param factor real-valued multiplication factor for the current matrix entry in {@code (row,column)}. */
    public void multiplyEntry(final int row, final int column, final double factor) {
        this.checkRowIndex(row);
        this.checkColumnIndex(column);
        
        unsafeSetEntryRe(row, column, getEntryRe(row, column) * factor);
        unsafeSetEntryIm(row, column, getEntryIm(row, column) * factor);
    }
    
    @Override
    public ComplexMatrix transpose() {
        final int rows = this.getRowDimension();
        final int cols = this.getColumnDimension();
        
        final ComplexMatrix answer  = new ComplexMatrix(cols, rows, this.parallelism);
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                answer.unsafeSetEntry(col, row, getEntryRe(row, col), getEntryIm(row, col));
            }
        }

        return answer;
    }
    
    @Override
    public Complex getTrace() {
        Numerics.requireSquare(this);
        final int nRows = getRowDimension();
        double traceRe = 0.0;
        double traceIm = 0.0;
        for (int i = 0; i < nRows; ++i) {
            traceRe += getEntryRe(i, i);
            traceIm += getEntryIm(i, i);
        }
        
        return new Complex(traceRe, traceIm);
    }
    
    /** Extract the {@link Complex Complex[][] data array} from a {@link FieldMatrix}. If {@code A} is actually an
     * instance of {@link Array2DRowFieldMatrix}, we use {@link Array2DRowFieldMatrix#getDataRef()}, which allows us to
     * forgo unnecessary cloning of the data array. Otherwise, we simply use {@link FieldMatrix#getData()}, which makes
     * no guarantees about cloning. In the case of {@link Array2DRowFieldMatrix}, {@link Array2DRowFieldMatrix#getData()
     * getData} <em>does</em> make a clone.
     * 
     * @param A the {@link Complex} {@link FieldMatrix} from which we want a data array
     * @return the data array */
    static Complex[][] extractData(final FieldMatrix<Complex> A) {
        return A instanceof Array2DRowFieldMatrix<Complex> cast ? cast.getDataRef() : A.getData();
    }

    @Override
    public Complex walkInRowOrder(final FieldMatrixPreservingVisitor<Complex> visitor) {
        return this.walkInRowOrder(visitor, 0, this.getRowDimension() - 1, 0, this.getColumnDimension() - 1);
    }
    
    @Override
    public Complex walkInRowOrder(final FieldMatrixPreservingVisitor<Complex> visitor, 
                                  final int startRow, final int endRow,
                                  final int startColumn, final int endColumn) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        visitor.start(getRowDimension(), getColumnDimension(),
                      startRow, endRow, startColumn, endColumn);
        for (int row = startRow; row <= endRow; ++row) {
            for (int column = startColumn; column <= endColumn; ++column) {
                visitor.visit(row, column, getEntry(row, column));
            }
        }

        return visitor.end();
    }
    
    @Override
    public Complex walkInRowOrder(final FieldMatrixChangingVisitor<Complex> visitor) {
        return this.walkInRowOrder(visitor, 0, this.getRowDimension() - 1, 0, this.getColumnDimension() - 1);
    }
    
    @Override
    public Complex walkInRowOrder(final FieldMatrixChangingVisitor<Complex> visitor, 
                                  final int startRow, final int endRow,
                                  final int startColumn, final int endColumn) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        visitor.start(getRowDimension(), getColumnDimension(),
                        startRow, endRow, startColumn, endColumn);
        for (int row = startRow; row <= endRow; ++row) {
            for (int column = startColumn; column <= endColumn; ++column) {
                final Complex oldValue = getEntry(row, column);
                final Complex newValue = visitor.visit(row, column, oldValue);
                setEntry(row, column, newValue);
            }
        }

        return visitor.end();
    }
    
    @Override
    public Complex walkInColumnOrder(final FieldMatrixPreservingVisitor<Complex> visitor) {
        return this.walkInColumnOrder(visitor, 0, this.getRowDimension() - 1, 0, this.getColumnDimension() - 1);
    }

    @Override
    public Complex walkInColumnOrder(final FieldMatrixPreservingVisitor<Complex> visitor, 
                                     final int startRow, final int endRow,
                                     final int startColumn, final int endColumn) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        visitor.start(getRowDimension(), getColumnDimension(),
                        startRow, endRow, startColumn, endColumn);
        for (int column = startColumn; column <= endColumn; ++column) {
            for (int row = startRow; row <= endRow; ++row) {
                visitor.visit(row, column, getEntry(row, column));
            }
        }

        return visitor.end();
    }
    
    @Override
    public Complex walkInColumnOrder(final FieldMatrixChangingVisitor<Complex> visitor) {
        return this.walkInColumnOrder(visitor, 0, this.getRowDimension() - 1, 0, this.getColumnDimension() - 1);
    }
    
    @Override
    public Complex walkInColumnOrder(final FieldMatrixChangingVisitor<Complex> visitor, 
                                     final int startRow, final int endRow,
                                     final int startColumn, final int endColumn) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        visitor.start(getRowDimension(), getColumnDimension(),
                        startRow, endRow, startColumn, endColumn);
        for (int column = startColumn; column <= endColumn; ++column) {
            for (int row = startRow; row <= endRow; ++row) {
                final Complex oldValue = getEntry(row, column);
                final Complex newValue = visitor.visit(row, column, oldValue);
                setEntry(row, column, newValue);
            }
        }

        return visitor.end();
    }
    
    @Override
    public Complex walkInOptimizedOrder(final FieldMatrixChangingVisitor<Complex> visitor) {
        return walkInRowOrder(visitor);
    }

    @Override
    public Complex walkInOptimizedOrder(final FieldMatrixPreservingVisitor<Complex> visitor) {
        return walkInRowOrder(visitor);
    }

    @Override
    public Complex walkInOptimizedOrder(final FieldMatrixChangingVisitor<Complex> visitor, 
                                        final int startRow, final int endRow,
                                        final int startColumn, final int endColumn) {
        return walkInRowOrder(visitor, startRow, endRow, startColumn, endColumn);
    }

    @Override
    public Complex walkInOptimizedOrder(final FieldMatrixPreservingVisitor<Complex> visitor, 
                                        final int startRow, final int endRow,
                                        final int startColumn, final int endColumn) {
        return walkInRowOrder(visitor, startRow, endRow, startColumn, endColumn);
    }


    /** Determine equivalence between {@code this} and {@code other}. Equivalence is determined by element-by-element
     * equality within the given absolute tolerances.
     * 
     * @param other the other {@link ComplexMatrix}
     * @param realAbsEps the absolute tolerance for the equality between the {@link Complex#getReal() real parts} of the
     *        entries.
     * @param imgAbsEps the absolute tolerance for the equality between the {@link Complex#getImaginary() imaginary
     *        parts} of the entries.
     * @return {@code true} if the elements of {@code this} and {@code other} are equal within the given absolute
     *         tolerances, {@code false} otherwise */
    public boolean absEquivalentTo(final ComplexMatrix other, final double realAbsEps, final double imgAbsEps) {
        Objects.requireNonNull(other, "other may not be null");
        
        if (this == other) {
            return true;
        }
        
        final int rows = this.getRowDimension();
        final int cols = this.getColumnDimension();
        if (other.getColumnDimension() != cols || other.getRowDimension() != rows) {
            return false;
        }
        
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                final Complex thisIJ = this.getEntry(row, col);
                final Complex otherIJ = other.getEntry(row, col);
                if (!Precision.equals(thisIJ.getReal(), otherIJ.getReal(), realAbsEps)
                        || !Precision.equals(thisIJ.getImaginary(), otherIJ.getImaginary(), imgAbsEps)) {
                    return false;
                }
            }
        }

        return true;
    }
    
    /** {@inheritDoc}<br><br>
     * 
     * Example:
     * 
     * <br><br>
     * Matrix data
     * 
     * <pre>
     * new Complex[][] { { new Complex(2, 2), Complex.ZERO, Complex.INF },
     *                   { Complex.ZERO, new Complex(3, 3), Complex.NaN },
     *                   { new Complex(123123123123123.123123123123123, 123.123),
     *                     new Complex(123.123, 123123123123123.123123123123123),
     *                     new Complex(123123.123123, 456456.456456) } }
     * </pre>
     * 
     * results in
     * 
     * <pre>
     *                2.0  + i   2.0  ,    0.0   + i               0.0 ,         Infinity + i        Infinity
     *                0.0  + i   0.0  ,    3.0   + i               3.0 ,              NaN + i             NaN
     *  123123123123123.12 + i 123.123,  123.123 + i 123123123123123.12,  123123.123123   + i 456456.456456
     * </pre>
     * 
     * @see Matrix#toString()
     * @see Matrix#doubleMatrixToStringMatrix(double[][], double)
     * 
     * @implSpec we create the String representation by first splitting this {@link ComplexMatrix} into two
     *           {@code double[][]}s with the same dimension, converting those two {@code double[][]}s into
     *           {@code String[][]}s using {@link Matrix#doubleMatrixToStringMatrix(double[][], double)}, and then
     *           concatenating the entries of each into a single String representing a complex entry. */
    @Override
    public String toString() {
        return this.toString(0.0, 0.0);
    }
    
    @Override
    public int hashCode() {
        return Arrays.deepHashCode(this.Re) + Arrays.deepHashCode(this.Im);
    }
    
    @Override
    public boolean equals(final Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof FieldMatrix)) {
            return false;
        }
        
        if (object instanceof ComplexMatrix) {
            ComplexMatrix m = (ComplexMatrix) object;
            final int nRows = getRowDimension();
            final int nCols = getColumnDimension();
            if (m.getColumnDimension() != nCols || m.getRowDimension() != nRows) {
                return false;
            }
            for (int row = 0; row < nRows; ++row) {
                for (int col = 0; col < nCols; ++col) {
                    if (this.Re[row][col] != m.Re[row][col] || this.Im[row][col] != m.Im[row][col]) {
                        return false;
                    }
                }
            }
        }
        else {
            @SuppressWarnings("unchecked")
            FieldMatrix<Complex> m = (FieldMatrix<Complex>) object;
            final int nRows = getRowDimension();
            final int nCols = getColumnDimension();
            if (m.getColumnDimension() != nCols || m.getRowDimension() != nRows) {
                return false;
            }
            for (int row = 0; row < nRows; ++row) {
                for (int col = 0; col < nCols; ++col) {
                    final Complex ij = m.getEntry(row, col);
                    if (this.Re[row][col] != ij.getReal()
                            || this.Im[row][col] != ij.getImaginary()) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
    
    /** Split this {@link ComplexMatrix} into two {@code double[][]}s that represent the real and imaginary parts of the
     * {@link Complex} entries of this {@link ComplexMatrix}. Note that this does not return the internal data arrays,
     * but copies of them.
     * 
     * @return a {@code double[][][]} with two entries: the real and imaginary parts of this {@link ComplexMatrix} */
    public double[][][] toReImParts() {
        final int rowDim = this.getRowDimension();
        final int colDim = this.getColumnDimension();
        
        final double[][] re = new double[rowDim][colDim];
        final double[][] im = new double[rowDim][colDim];
        
        for (int i = 0; i < rowDim; i++) {
            for (int j = 0; j < colDim; j++) {
                re[i][j] = getEntryRe(i, j);
                im[i][j] = getEntryIm(i, j);
            }
        }
        
        return new double[][][] { re, im };
    }
    
    /** The same as {@link #toString()}, but with a lower bound on what values will be shown as non-zero
     * 
     * @param reLowerBound the lower bound for the real parts of {@link Complex} entries
     * @param imLowerBound the lower bound for the imaginary parts of {@link Complex} entries
     * @return the string representation of this {@link ComplexMatrix} 
     * */
    public String toString(final double reLowerBound, final double imLowerBound) {
        final int rowDim = this.getRowDimension();
        final int colDim = this.getColumnDimension();
        
        final double[][] re = this.Re;
        final double[][] im = this.Im;
        
        boolean isReal = true;
        for (int i = 0; i < rowDim; ++i) {
            for (int j = 0; j < colDim; ++j) {
                isReal &= FastMath.abs(im[i][j]) <= imLowerBound;
            }
        }
        
        if (isReal) {
            return new Matrix(re).toString(reLowerBound);
        }
        
        final String[][] reStrs = Matrix.doubleMatrixToStringMatrix(re, reLowerBound);
        final String[][] imStrs = Matrix.doubleMatrixToStringMatrix(im, imLowerBound);
        
        final StringBuilder answer = new StringBuilder();
        for (int i = 0; i < rowDim; ++i) {
            for (int j = 0; j < colDim; ++j) { // now we just concatenate everything with some new-lines
                final String ijString = reStrs[i][j] + " +" + imStrs[i][j] + " i"; /* each string already starts with a
                                                                                    * space, so we don't need an extra
                                                                                    * one after the "+" */
                answer.append(ijString);
                if (j != colDim - 1) {
                    answer.append(", ");
                }
            }
            if (i != rowDim - 1) {
                answer.append(System.lineSeparator());
            }
        }
        
        return answer.toString();
    }
    
    /** @return a string of the form {@code (n x m)} where {@code n == } {@link #getRowDimension()
     *         this.getRowDimension()} and {@code m ==} {@link #getColumnDimension() this.getColumnDimension()}. */
    public String getDimensionString() {
        return new StringBuilder().append("(")
                                  .append(this.getRowDimension())
                                  .append(" x ")
                                  .append(this.getColumnDimension())
                                  .append(")")
                                  .toString();
    }
    
    /** Extract the diagonal elements of {@code A} as a {@link ComplexVector}. If {@code A} is rectangular, we extract
     * as many diagonal elements as we can (the minimum between {@link #getRowDimension()} and
     * {@link #getColumnDimension()}).
     * 
     * @param A the {@link ComplexMatrix} from which we want to extract the diagonal
     * @return the {@link ComplexVector} of diagonal elements of {@code A} */
    public static ComplexVector extractDiagonalVector(final ComplexMatrix A) {
        return new ComplexVector(extractDiagonal(A));
    }
    
    /** Extract the diagonal elements of {@code A} as a {@link Complex Complex[]}. If {@code A} is rectangular, we
     * extract as many diagonal elements as we can (the minimum between {@link #getRowDimension()} and
     * {@link #getColumnDimension()}).
     * 
     * @param A the {@link ComplexMatrix} from which we want to extract the diagonal
     * @return the {@link Complex Complex[]} of diagonal elements of {@code A} */
    public static Complex[] extractDiagonal(final ComplexMatrix A) {
        final int diagonalDim = FastMath.min(A.getRowDimension(), A.getColumnDimension());
        final Complex[] diagArr = new Complex[diagonalDim];
        for (int i = 0; i < diagonalDim; i++) {
            diagArr[i] = A.getEntry(i, i);
        }
        
        return diagArr;
    }
    
    /** Create a new diagonal {@link ComplexMatrix} with the specified {@link Complex} diagonal elements
     * 
     * @param diag the diagonal elements
     * @return the new diagonal {@link ComplexMatrix} */
    public static ComplexMatrix diagonal(final ComplexVector diag) {
        return diagonal(diag.getData());
    }
    
    /** Create a new diagonal {@link ComplexMatrix} with the specified {@link Complex} diagonal elements
     * 
     * @param diag the diagonal elements
     * @return the new diagonal {@link ComplexMatrix} */
    public static ComplexMatrix diagonal(final Complex[] diag) {
        final ComplexMatrix A = identity(diag.length);
        for (int i = 0; i < diag.length; i++) {
            final Complex ii = diag[i];
            A.unsafeSetEntry(i, i, ii.getReal(), ii.getImaginary());
        }
        
        return A;
    }
    
    /** Create a {@link ComplexMatrix} from an array ({@code varargs}) of {@link ComplexVector row vectors}.<br>
     * <br>
     * 
     * Each {@link ComplexVector} must be of the same dimension or an exception will be thrown in
     * {@link Array2DRowFieldMatrix#setRowVector(int, FieldVector)}
     * 
     * @param rows the rows of the {@link ComplexMatrix}. There must be at least one.
     * @return the new {@link ComplexMatrix} */
    public static ComplexMatrix fromRows(final ComplexVector... rows) {
        Validation.requireGreaterThanEqualTo(rows.length, 1, "rows");
        
        final ComplexMatrix answer = new ComplexMatrix(rows.length, rows[0].getDimension());
        for (int i = 0; i < rows.length; i++) {
            answer.setRowVector(i, rows[i]);
        }
        
        return answer;
    }
    
    /** Create a {@link ComplexMatrix} from an array ({@code varargs}) of {@link ComplexVector columns vectors}.<br>
     * <br>
     * 
     * Each {@link ComplexVector} must be of the same dimension or an exception will be thrown in
     * {@link Array2DRowFieldMatrix#setColumnVector(int, FieldVector)}
     * 
     * @param columns the columns of the {@link ComplexMatrix}. There must be at least one.
     * @return the new {@link ComplexMatrix} */
    public static ComplexMatrix fromColumns(final ComplexVector... columns) {
        Validation.requireGreaterThanEqualTo(columns.length, 1, "columns");
        
        final ComplexMatrix answer = new ComplexMatrix(columns[0].getDimension(), columns.length);
        for (int i = 0; i < columns.length; i++) {
            answer.setColumnVector(i, columns[i]);
        }
        
        return answer;
    }
    
    /** Compute and return the identity {@link ComplexMatrix} of rank {@code dim}
     * 
     * @param dim the dimension
     * @return the identity {@link ComplexMatrix} of rank {@code dim} */
    public static ComplexMatrix identity(final int dim) {
        return identity(dim, DEFAULT_CMATRIX_PARALLELISM);
    }
    
    /** Compute and return the identity {@link ComplexMatrix} of rank {@code dim}
     * 
     * @param dim the dimension
     * @param parallel the level of {@link Parallelism} to associate with the newly created identity matrix
     * @return the identity {@link ComplexMatrix} of rank {@code dim} */
    public static ComplexMatrix identity(final int dim, final Parallelism parallel) {
        Validation.requireGreaterThanEqualTo(dim, 1, "dimension");
        final Complex[][] arrMatrix = new Complex[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                arrMatrix[i][j] = i == j ? Complex.ONE : Complex.ZERO;
            }
        }
        
        return new ComplexMatrix(arrMatrix, parallel);
    }
    
    /**  Create and return a {@link ComplexMatrix} with dimensions {@code (m x n)} whose entries are all {@code 1.0}
     * 
     * @param m the number of rows
     * @param n the number of columns
     * @return a {@code (m x n)} {@link ComplexMatrix} whose entries are all {@code 1.0} */
    public static ComplexMatrix ones(final int m, final int n) {
        return constant(1.0, 0.0, m, n);
    }
    
    /** Create and return a {@link ComplexMatrix} with dimensions {@code (m x n)} whose entries are all
     * {@code re + im * i}
     * 
     * @param re the real part to assign to each entry
     * @param im the imaginary part to assign to each entry
     * @param m the number of rows
     * @param n the number of columns
     * @return a {@code (m x n)} {@link ComplexMatrix} whose entries are all {@code re + im * i} */
    public static ComplexMatrix constant(final double re, final double im, final int m, final int n) {
        final ComplexMatrix constant = new ComplexMatrix(m, n);
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                constant.unsafeSetEntry(i, j, re, im);
            }
        }
        return constant;
    }
    
    /** Create and return a {@link Matrix} with dimensions {@code (m x n)} whose entries are all {@code 0.0}
     * 
     * @param m the number of rows
     * @param n the number of columns
     * 
     * @return a zero {@link ComplexMatrix} with dimensions {@code (m x n)} whose entries are all {@code 0.0} */
    public static ComplexMatrix zero(final int m, final int n) {
        return new ComplexMatrix(m, n);
    }
    
    /** Concatenate the given {@link ComplexMatrix complex matrices} diagonally.<br>
     * <br>
     * 
     * E.g. (empty spaces are zeroes)
     * 
     * <pre>
     * If A = 
     * . . . . . 
     * . . . . . 
     *   . . . . 
     *     . . . 
     *       . . 
     *  
     * and B =
     * . . . . .
     * . . . . .
     * 
     * and C = 
     * .
     * .
     * .
     * .
     * 
     * then blockDiagonalConcatenate(A, B, C) =
     * 
     * . . . . . 
     * . . . . . 
     *   . . . . 
     *     . . . 
     *       . . 
     *           . . . . .
     *           . . . . .
     *                     .
     *                     .
     *                     .
     *                     .
     * </pre>
     * 
     * @param As the {@link ComplexMatrix}s to concatenate
     * @return the concatenated matrix
     * 
     **/
    public static ComplexMatrix blockDiagonalConcatenate(final ComplexMatrix... As) {
        int numRows = 0;
        int numCols = 0;
        for (final ComplexMatrix A : As) {
            numRows += A.getRowDimension();
            numCols += A.getColumnDimension();
        }

        final ComplexMatrix answer = zero(numRows, numCols);
        
        int currentRow = 0;
        int currentCol = 0;
        for (final ComplexMatrix A : As) {
            final double[][][] AData = A.getDataRef();
            answer.setSubMatrix(AData[0], AData[1], currentRow, currentCol);
            
            currentRow += A.getRowDimension();
            currentCol += A.getColumnDimension();
        }
        
        return answer;
    }
    
    /** Determine whether each element of the given {@link ComplexMatrix} is real to within the given absolute tolerance
     * 
     * @param A the {@link ComplexMatrix} to check
     * @param absEBETol the absolute element-wise tolerance. Elements with imaginary part larger than this will be
     *        considered non-zero.
     * @return {@code false} if the {@link ComplexMatrix} is not real-valued, {@code true} otherwise */
    public static boolean isReal(final ComplexMatrix A, final double absEBETol) {
        final int rowDim = A.getRowDimension();
        final int colDim = A.getColumnDimension();
        for (int i = 0; i < rowDim; i++) {
            for (int j = 0; j < colDim; j++) {
                if (!Precision.equals(A.Im[i][j], 0.0, absEBETol)) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /** Determine whether the given {@link ComplexMatrix} is symmetric to within the given relative tolerance
     * 
     * @param A the {@link ComplexMatrix} to check
     * @param relEBETol the relative element-wise tolerance. See
     *        {@link Numerics#equalsRelEps(double, double, double)} for an explanation of relative
     *        tolerances.
     * @return {@code false} if the {@link ComplexMatrix} is either not {@link AnyMatrix#isSquare() square} or not
     *         symmetric, {@code true} otherwise */
    public static boolean isSymmetric(final ComplexMatrix A, final Complex relEBETol) {
        if (!A.isSquare()) {
            return false;
        }
        
        final int dim = A.getRowDimension();
        for (int i = 0; i < dim; i++) {
            for (int j = i + 1; j < dim; j++) {
                final Complex entryIJ = A.getEntry(i, j);
                final Complex entryJI = A.getEntry(j, i);
                if (!Numerics.equalsRelEps(entryIJ, entryJI, relEBETol)) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /** Determine whether the given {@link ComplexMatrix} is both real and symmetric to within the given
     * <em>absolute</em> tolerance.
     * 
     * @param A the {@link ComplexMatrix} to check
     * @param absTol the <em>absolute</em> tolerance when checking whether elements are symmetric and real
     * @return {@code false} if the {@link ComplexMatrix} is either not {@link AnyMatrix#isSquare() square} or not
     *         real-symmetric, {@code true} otherwise */
    public static boolean isRealSymmetric(final ComplexMatrix A, final double absTol) {
        if (!A.isSquare()) {
            return false;
        }
        
        final int dim = A.getRowDimension();
        for (int i = 0; i < dim; i++) {
            if (!Precision.equals(A.Im[i][i], 0.0, absTol)) { // short-circuit on diagonal entries
                return false;
            }
            
            for (int j = i + 1; j < dim; j++) {
                final double AijRe = A.Re[i][j]; // Re(A(i, j))
                final double AijIm = A.Im[i][j]; // Im(A(i, j))
                
                final double AjiRe = A.Re[j][i]; // Re(A(j, i))
                final double AjiIm = A.Im[j][i]; // Im(A(j, i))
                if (!Precision.equals(AijIm, 0.0, absTol)             // check (i, j) isReal
                        || !Precision.equals(AjiIm, 0.0, absTol)      // check (j, i) isReal
                        || !Precision.equals(AijRe, AjiRe, absTol)) { // check (i, j) == (j, i)
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /** Determine whether the given {@link ComplexMatrix} is Hermitian to within the given relative tolerance. Hermitian
     * meaning equal to its own conjugate transpose.
     * 
     * @param A the {@link ComplexMatrix} to check
     * @param relEBETol the relative element-wise tolerance. See
     *        {@link Numerics#equalsRelEps(double, double, double)} for an explanation of relative
     *        tolerances.
     * @return {@code false} if the {@link ComplexMatrix} is either not {@link AnyMatrix#isSquare() square} or not
     *         Hermitian, {@code true} otherwise */
    public static boolean isHermitian(final ComplexMatrix A, final Complex relEBETol) {
        if (!A.isSquare()) {
            return false;
        }
        
        final int dim = A.getRowDimension();
        for (int i = 0; i < dim; i++) {
            for (int j = i + 1; j < dim; j++) {
                final Complex entryIJ = A.getEntry(i, j);
                final Complex entryJI = A.getEntry(j, i).conjugate();
                if (!Numerics.equalsRelEps(entryIJ, entryJI, relEBETol)) {
                    return false;
                }
            }
        }
        
        return true;
    }
}
