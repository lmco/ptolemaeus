/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.complex;

import java.util.Objects;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.MatrixUtils;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.linear.MatrixSlice;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** This {@code abstract} covers the boilerplate code of {@link ComplexMatrix} {@link MatrixSlice} implementations,
 * leaving strictly the interface methods of {@link MatrixSlice} to be implemented by concrete classes.
 * 
 * @author Peter Davis, peter.m.davis@lmco.com */
public abstract class AbstractComplexMatrixSlice extends ComplexMatrix implements MatrixSlice {
    
    /** The original {@link ComplexMatrix} that this slice is referencing. */
    private final ComplexMatrix sourceMatrix;
    
    /** The row dimension of the slice */
    private final int rowDimension;
    
    /** The column dimension of the slice */
    private final int colDimension;
    
    /** Constructor 
     * @param sourceMatrix the source matrix that this slice is referencing
     * @param rowDimension the row dimension (number of rows) in this slice
     * @param colDimension the column dimension (number of columns) in this slice */
    @SuppressFBWarnings(value = "EI2", justification = "sourceMatrix is intentionally mutable")
    protected AbstractComplexMatrixSlice(final ComplexMatrix sourceMatrix,
                                         final int rowDimension,
                                         final int colDimension) {
        super(sourceMatrix.getParallelism()); // default NPE
        
        this.sourceMatrix = sourceMatrix;
        this.rowDimension = Validation.requireGreaterThan(rowDimension, 0, "rowDimension");
        this.colDimension = Validation.requireGreaterThan(colDimension, 0, "colDimension");
    }
    
    @Override
    public int getRowDimension() {
        return this.rowDimension;
    }

    @Override
    public int getColumnDimension() {
        return this.colDimension;
    }

    @Override
    public void checkRowIndex(final int row) {
        MatrixUtils.checkRowIndex(this, row);
    }

    @Override
    public void checkColumnIndex(final int col) {
        MatrixUtils.checkColumnIndex(this, col);
    }

    @Override
    public ComplexMatrix createMatrix(final int rowDim, final int colDim) {
        throw new UnsupportedOperationException("Cannot create a ComplexMatrixSlice with no source matrix");
    }

    @Override
    public double getEntryRe(final int row, final int col) {
        return sourceMatrix.getEntryRe(this.getSourceRowIndex(row), this.getSourceColIndex(col));
    }

    @Override
    public double getEntryIm(final int row, final int col) {
        return sourceMatrix.getEntryIm(this.getSourceRowIndex(row), this.getSourceColIndex(col));
    }

    @Override
    public void unsafeSetEntryRe(final int row, final int col, final double value) {
        sourceMatrix.unsafeSetEntryRe(this.getSourceRowIndex(row), this.getSourceColIndex(col), value);
    }

    @Override
    public void unsafeSetEntryIm(final int row, final int col, final double value) {
        sourceMatrix.unsafeSetEntryIm(this.getSourceRowIndex(row), this.getSourceColIndex(col), value);
    }

    @Override
    public Complex[][] getData() {
        final Complex[][] out = new Complex[rowDimension][colDimension];
        for (int i = 0; i < rowDimension; i++) {
            for (int j = 0; j < colDimension; j++) {
                out[i][j] = getEntry(i, j);
            }
        }
        return out;
    }

    @Override
    public ComplexMatrix copy() {
        return new ComplexMatrix(getData());
    }

    @Override
    public double[][][] getDataRef() {
        throw new UnsupportedOperationException("ComplexMatrixSlice cannot provide a data ref.");
    }

    @Override
    public ComplexMatrix getSubMatrix(final int rowStartParam,
                                      final int rowEndParam,
                                      final int colStartParam,
                                      final int colEndParam) {
        MatrixUtils.checkSubMatrixIndex(this, rowStartParam, rowEndParam, colStartParam, colEndParam);
        final int rowCount    = rowEndParam - rowStartParam + 1;
        final int columnCount = colEndParam - colStartParam + 1;
        final Complex[][] outData  = new Complex[rowCount][columnCount];
        
        for (int i = 0; i < rowCount; ++i) {
            for (int j = 0; j < columnCount; j++) {
                outData[i][j] = getEntry(i, j);
            }
        }
        return new ComplexMatrix(outData);
    }

    /**
     * Slices are considered equal if their data matches exactly, but do not need to reference the same 
     * source matrix.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }

        AbstractComplexMatrixSlice other = (AbstractComplexMatrixSlice) obj;
        if (other.rowDimension != this.rowDimension 
                || other.colDimension != this.colDimension) {
            return false;
        }

        for (int row = 0; row < this.rowDimension; ++row) {
            for (int col = 0; col < this.colDimension; ++col) {
                if (!getEntry(row, col).equals(other.getEntry(row, col))) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceMatrix, rowDimension, colDimension);
    }
}
