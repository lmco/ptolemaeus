/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.eigen;

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** An {@link EigenSystem} but with strictly real Eigenvalues and Eigenvectors; e.g., the Eigen system of a
 * real-symmetric matrix.<br>
 * <br>
 * 
 * TODO Ryan Moser, 2024-12-03: refactor {@link RealEigenSystem} into a class s.t. we can instantiate either with a
 * {@link List} of {@link RealEigenPair}s <em>or</em> with the Eigenvalues and Eigenvectors specified separately as they
 * are now, and lazily initialize the other.
 * 
 * @param eigenvalues the Eigen<em>values</em> of a {@link Matrix}
 * @param eigenvectorMatrix a {@link Matrix} whose columns are the Eigen<em>vectors</em> of a {@link Matrix}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
@SuppressFBWarnings(value = { "EI", "EI2" }, justification = "matrices are understood to be mutable")
public record RealEigenSystem(double[] eigenvalues, Matrix eigenvectorMatrix) {
    
    /** The indices of the columns of the matrices are assumed to correspond to each other (Eigenvalue with
     * corresponding Eigenvector)
     * 
     * @param eigenvalues the Eigen<em>values</em> of a matrix
     * @param eigenvectorMatrix {@link Matrix} whose columns are the Eigen<em>vectors</em> of {@code A} */
    public RealEigenSystem {
        requireNonNull(eigenvalues, "the eigenvalues must be specified");
        requireNonNull(eigenvectorMatrix, "the eigenVectorMatrix may be empty, but must not be null");
        
        Validation.requireEqualsTo(eigenvalues.length,
                                   eigenvectorMatrix.getColumnDimension(),
                                   () -> ("There must be exactly one "
                                          + "Eigenvalue for each Eigenvector, "
                                          + "but there were %d and %d respectively").formatted(eigenvalues.length,
                                                                               eigenvectorMatrix.getColumnDimension()));
        
        Numerics.requireSquare(eigenvectorMatrix);
    }
    
    /** Get the {@code i}<sup>th</sup> {@link RealEigenPair} of this {@link RealEigenSystem}
     * 
     * @param i the index of the pair to retrieve
     * @return the {@link RealEigenPair} */
    public RealEigenPair getEigenPair(final int i) {
        return new RealEigenPair(this.eigenvalues[i], new NVector(this.eigenvectorMatrix.getColumn(i)));
    }
    
    @Override
    public final String toString() {
        final StringBuilder bldr = new StringBuilder();
        
        bldr.append("Eigenvalues:")
            .append(System.lineSeparator())
            .append(new NVector(this.eigenvalues).toColumnString())
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .append("Eigenvectors:")
            .append(System.lineSeparator())
            .append(this.eigenvectorMatrix());
        
        return bldr.toString();
    }
    
    @Override
    public final int hashCode() {
        return Objects.hash(Arrays.hashCode(this.eigenvalues), this.eigenvectorMatrix);
    }
    
    @Override
    public final boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof RealEigenSystem cast) {
            return Arrays.equals(this.eigenvalues, cast.eigenvalues)
                    && this.eigenvectorMatrix.equals(cast.eigenvectorMatrix);
        }
        
        return false;
    }
}
