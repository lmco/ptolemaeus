/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.eigen;

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.hipparchus.complex.Complex;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.Matrix;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** An {@link EigenSystem} for a {@link ComplexMatrix} {@code A} consists of the {@link #eigenvalues()}: an array of
 * {@link Complex} Eigen<em>values</em> of {@code A}, and the {@link #eigenvectorMatrix()}, a {@link ComplexMatrix}
 * whose columns are the Eigen<em>vectors</em> of {@code A}<br>
 * <br>
 * 
 * The indices of the {@link #eigenvalues()} array and columns of the matrices are assumed to correspond to each other
 * (Eigenvalue with corresponding Eigenvector).<br>
 * <br>
 * 
 * TODO Ryan Moser, 2024-12-03: refactor {@link EigenSystem} into a class s.t. we can instantiate either with a
 * {@link List} of {@link EigenPair}s <em>or</em> with the Eigenvalues and Eigenvectors specified separately as they are
 * now, and lazily initialize the other.
 * 
 * @param eigenvalues the Eigen<em>values</em> of a matrix
 * @param eigenvectorMatrix {@link ComplexMatrix} whose columns are the Eigen<em>vectors</em> of {@code A}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
@SuppressFBWarnings(value = { "EI", "EI2" }, justification = "matrices are understood to be mutable")
public record EigenSystem(Complex[] eigenvalues, ComplexMatrix eigenvectorMatrix) {
    
    /** The indices of the columns of the matrices are assumed to correspond to each other (Eigenvalue with
     * corresponding Eigenvector)
     * 
     * @param eigenvalues the Eigen<em>values</em> of a matrix
     * @param eigenvectorMatrix {@link ComplexMatrix} whose columns are the Eigen<em>vectors</em> of {@code A} */
    public EigenSystem {
        requireNonNull(eigenvalues, "the eigenvalues must be specified");
        requireNonNull(eigenvectorMatrix, "the eigenVectorMatrix may be empty, but must not be null");
        
        Validation.requireEqualsTo(eigenvalues.length,
                                   eigenvectorMatrix.getColumnDimension(),
                                   () -> ("There must be exactly one "
                                          + "Eigenvalue for each Eigenvector, "
                                          + "but there were %d and %d respectively").formatted(eigenvalues.length,
                                                                               eigenvectorMatrix.getColumnDimension()));
        
        Numerics.requireSquare(eigenvectorMatrix);
    }
    
    /** @return a {@link ComplexVector} containing Eigenvalues */
    public ComplexVector getEigenvalueVector() {
        return new ComplexVector(this.eigenvalues);
    }
    
    /** Return the {@code i}<sup>th</sup> {@link EigenPair} of this {@link EigenSystem}
     * 
     * @param i the index of the {@link EigenPair} to retrieve
     * @return the {@link EigenPair} */
    public EigenPair getEigenPair(final int i) {
        return new EigenPair(this.eigenvalues[i], this.eigenvectorMatrix.getColumnVector(i));
    }
    
    /** @return a diagonal {@link ComplexMatrix} whose diagonal entries are {@link #eigenvalues()} */
    public ComplexMatrix getEigenvalueMatrix() {
        return ComplexMatrix.diagonal(this.eigenvalues);
    }
    
    /** Compute an return a {@link RealEigenSystem} equivalent to this {@link EigenSystem} wrapped in an
     * {@link Optional} iff neither the Eigenvalues nor the Eigenvectors have any elements with imaginary part greater
     * than {@code absMaxIm}. Otherwise, return an empty {@link Optional}.
     * 
     * @param absMaxIm the maximum allowable imaginary part of an Eigenvalue or Eigenvector component that we will
     *        consider zero
     * @return the {@link RealEigenSystem} wrapped in an {@link Optional}, or an empty {@link Optional}, as described
     * 
     * @see EigenPair#asRealEigenPair(double) */
    public Optional<RealEigenSystem> asRealEigenSystem(final double absMaxIm) {
        final int n = this.eigenvalues.length;
        
        final double[] realEigenvalues  = new double[n];
        final Matrix   realEigenvectors = new Matrix(new double[n][n]);
        
        for (int i = 0; i < n; i++) {
            final EigenPair               pair    = this.getEigenPair(i);
            final Optional<RealEigenPair> optReal = pair.asRealEigenPair(absMaxIm);
            if (optReal.isEmpty()) {
                return Optional.empty();
            }
            
            final RealEigenPair realPair = optReal.get();
            
            realEigenvalues[i] = realPair.eigenvalue();
            realEigenvectors.setColumnVector(i, realPair.eigenvector());
        }
        
        return Optional.of(new RealEigenSystem(realEigenvalues, realEigenvectors));
    }
    
    @Override
    public final String toString() {
        final StringBuilder bldr = new StringBuilder();
        
        bldr.append("Eigenvalues:")
            .append(System.lineSeparator())
            .append(this.getEigenvalueVector().toColumnString())
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .append("Eigenvectors:")
            .append(System.lineSeparator())
            .append(this.eigenvectorMatrix());
        
        return bldr.toString();
    }
    
    @Override
    public final int hashCode() {
        return Objects.hash(Arrays.deepHashCode(this.eigenvalues), this.eigenvectorMatrix);
    }
    
    @Override
    public final boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof EigenSystem cast) {
            return Arrays.equals(this.eigenvalues, cast.eigenvalues)
                    && this.eigenvectorMatrix.equals(cast.eigenvectorMatrix);
        }
        
        return false;
    }
}
