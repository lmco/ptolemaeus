/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.eigen;

import java.util.function.Consumer;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.balancing.BalanceMode;
import ptolemaeus.math.linear.balancing.MatrixBalancer;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;
import ptolemaeus.math.linear.schur.EigenvectorFailureMode;
import ptolemaeus.math.linear.schur.SchurMode;

/** This class contains methods to compute an {@link EigenSystem} for single matrices (the Eigenvalue problem).<br>
 * <br>
 * 
 * <b>Important:</b> the methods used by this class do not - currently - include optimizations for sparse matrices!<br>
 * <br>
 * 
 * TODO (someday) Ryan Moser, 2024-08-30: write the algorithms (likely an implementation of the QZ algorithm to compute
 * the generalized Schur form) required to solve the generalized Eigenvalue problem {@code Av = lBv}.<br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see ComplexSchurDecomposition */
public final class Eigen {
    
    /** the default number of iterations of the inverse iteration algorithm to perform when computing Eigenvectors in
     * {@link ComplexSchurDecomposition#computeEigenvectorInverseIteration} */
    public static final int DEFAULT_EIG_VECT_ITERS = 10;
    
    /** do nothing */
    private Eigen() { /* do nothing */ }
    
    /** Compute the Eigenvalues for {@link RealMatrix} {@code A}. By default, we
     * 
     * <ul>
     * <li>balance in the {@link BalanceMode#ONE_NORM 1-norm} before computing the {@link ComplexSchurDecomposition},
     * and</li>
     * <li>use {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} to compute the max allowable iterations of
     * the QR algorithm</li>
     * </ul>
     * 
     * @param realA the {@link RealMatrix} {@code A}
     * @return a {@link ComplexVector} of the Eigenvalues of {@code A}. Despite being a {@link ComplexVector}, the
     *         Eigenvalues themselves may still be real or very nearly real, with negligible
     *         {@link Complex#getImaginary() imaginary components} (see {@link ComplexVector#asReal(double)}). */
    public static ComplexVector eigenvalues(final RealMatrix realA) {
        final ComplexMatrix A = ComplexMatrix.of(realA);
        final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        return eigenvalues(A, BalanceMode.ONE_NORM, maxIters, null);
    }
    
    /** Compute the Eigenvalues for the {@link Complex}-valued {@link FieldMatrix} {@code A}. By default, we
     * 
     * <ul>
     * <li>balance in the {@link BalanceMode#ONE_NORM 1-norm} before computing the {@link ComplexSchurDecomposition},
     * and</li>
     * <li>use {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} to compute the max allowable iterations of
     * the QR algorithm</li>
     * </ul>
     * 
     * @param fieldA the {@link RealMatrix} {@code A}
     * @return a {@link ComplexVector} of the Eigenvalues of {@code A} */
    public static ComplexVector eigenvalues(final FieldMatrix<Complex> fieldA) {
        final ComplexMatrix A = ComplexMatrix.of(fieldA);
        final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        return eigenvalues(A, BalanceMode.ONE_NORM, maxIters, null);
    }
    
    /** Compute the Eigenvalues for the Eigenvalue problem {@code Av = lv} as a {@link ComplexVector}
     * 
     * @param argA the {@link ComplexMatrix} {@code A}
     * @param balanceMode the {@link BalanceMode} to use. Using {@code null} is the same as {@link BalanceMode#NONE}.
     * @param maxIters the maximum iterations to consider (see
     *        {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} for the default)
     * @param evaluationMonitor a {@link ComplexMatrix} {@link Consumer} to update with the current solution as we
     *        iterate in {@link ComplexSchurDecomposition}. May be {@code null}. Including this severely impacts
     *        performance! See {@link ComplexSchurDecomposition#of(ComplexMatrix, SchurMode, int, Consumer)}.
     * @return a {@link ComplexVector} of the Eigenvalues of {@code A} */
    public static ComplexVector eigenvalues(final ComplexMatrix argA,
                                            final BalanceMode balanceMode,
                                            final int maxIters,
                                            final Consumer<ComplexMatrix> evaluationMonitor) {
        Numerics.requireNonNaN(argA); // O(n^2) search for NaN to fail-fast
        
        final ComplexMatrix A = argA.copy();
        MatrixBalancer.doInPlace(A, balanceMode); /* In-place modification; we've already copied A, so this is safe.
                                                   * Also, we ignore the output of scales because balancing is a
                                                   * similarity transformation and, thus, the Eigenvalues are
                                                   * untouched. */
        
        return getSchur(A, true, maxIters, evaluationMonitor).getEigenvalues();
    }
    
    /** Compute an {@link EigenSystem} for {@link RealMatrix} {@code A}. By default, we
     * 
     * <ul>
     * <li>balance in the {@link BalanceMode#ONE_NORM 1-norm} before computing the
     * {@link ComplexSchurDecomposition},</li>
     * <li>use {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} to compute the max allowable iterations of
     * the QR algorithm,</li>
     * <li>use {@link #DEFAULT_EIG_VECT_ITERS} iterations of inverse iteration when computing Eigenvectors,</li>
     * <li>use an absolute tolerance of {@code 0.0} when determining whether the input matrix is
     * {@link ComplexMatrix#isRealSymmetric(double) real-symmetric}, and</li>
     * <li>use {@link LinearSolver#defaultSolver()} when solving systems of linear equations</li>
     * </ul>
     * 
     * @param realA the {@link RealMatrix} {@code A}
     * @return the {@link EigenSystem} for the Eigenvalue problem {@code Av = lv}. Despite the {@link EigenSystem} being
     *         composed of {@link ComplexMatrix complex matrices} the Eigenvalues and Eigenvectors themselves may still
     *         be real or very nearly real, with negligible {@link Complex#getImaginary() imaginary components}. */
    public static EigenSystem eigenSystem(final RealMatrix realA) {
        final ComplexMatrix A = ComplexMatrix.of(realA);
        final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        return eigenSystem(A,
                           BalanceMode.ONE_NORM,
                           maxIters,
                           DEFAULT_EIG_VECT_ITERS,
                           0.0,
                           LinearSolver.defaultSolver(),
                           null,
                           EigenvectorFailureMode.THROW);
    }
    
    /** Compute an {@link EigenSystem} for {@link Complex}-valued {@link FieldMatrix} {@code A}. By default, we
     * 
     * <ul>
     * <li>balance in the {@link BalanceMode#ONE_NORM 1-norm} before computing the
     * {@link ComplexSchurDecomposition},</li>
     * <li>use {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} to compute the max allowable iterations of
     * the QR algorithm,</li>
     * <li>use {@link #DEFAULT_EIG_VECT_ITERS} iterations of inverse iteration when computing Eigenvectors,</li>
     * <li>use an absolute tolerance of {@code 0.0} when determining whether the input matrix is
     * {@link ComplexMatrix#isRealSymmetric(double) real-symmetric}, and</li>
     * <li>use {@link LinearSolver#defaultSolver()} when solving systems of linear equations</li>
     * </ul>
     * 
     * @param fieldA the {@link Complex}-valued {@link FieldMatrix} {@code A}
     * @return the {@link EigenSystem} for the Eigenvalue problem {@code Av = lv} */
    public static EigenSystem eigenSystem(final FieldMatrix<Complex> fieldA) {
        final ComplexMatrix A = ComplexMatrix.of(fieldA);
        final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        return eigenSystem(A,
                           BalanceMode.ONE_NORM,
                           maxIters,
                           DEFAULT_EIG_VECT_ITERS,
                           0.0,
                           LinearSolver.defaultSolver(),
                           null,
                           EigenvectorFailureMode.THROW);
    }
    
    /** Compute the {@link EigenSystem} for the Eigenvalue problem {@code Av = lv}
     * 
     * @param argA the {@link ComplexMatrix} {@code A}
     * @param balanceMode the {@link BalanceMode} to use. Using {@code null} is the same as {@link BalanceMode#NONE}.
     * @param eigenvalueMaxIters the maximum iterations to consider (see
     *        {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} for the default)
     * @param eigenvectorIters the number of times to run the inverse iteration routine. Note: this is not an
     *        upper-bound, we will perform <em>exactly</em> this many iterations. Must be greater than zero. The default
     *        is {@link #DEFAULT_EIG_VECT_ITERS}.
     * @param realSymmAbsTol the tolerance to use when determining whether the input matrix is
     *        {@link ComplexMatrix#isRealSymmetric(double) real-symmetric}. The default is {@code 0.0}
     * @param linearSolver the {@link LinearSolver} to use when doing inverse iteration when computing the Eigenvectors.
     *        The default is {@link LinearSolver#defaultSolver()}.
     * @param evalMonitor a {@link ComplexMatrix} {@link Consumer} to update with the current iteration count and
     *        current solution as we iterate. May be - and the default is - {@code null}.
     * @param evecFailureMode specify what to do if we fail to compute an Eigenvector. The default is
     *        {@link EigenvectorFailureMode#THROW}.
     * @return the {@link EigenSystem} for the Eigenvalue problem {@code Av = lv} */
    public static EigenSystem eigenSystem(final ComplexMatrix argA,
                                          final BalanceMode balanceMode,
                                          final int eigenvalueMaxIters,
                                          final int eigenvectorIters,
                                          final double realSymmAbsTol,
                                          final LinearSolver linearSolver,
                                          final Consumer<ComplexMatrix> evalMonitor,
                                          final EigenvectorFailureMode evecFailureMode) {
        Numerics.requireNonNaN(argA); // O(n^2) search for NaN to fail-fast
        
        final MatrixBalancer balancer = MatrixBalancer.balance(argA, balanceMode);
        final ComplexMatrix  A        = balancer.getBalanced();
        
        final ComplexSchurDecomposition schur             = getSchur(A, false, eigenvalueMaxIters, evalMonitor);
        final Complex[]                 eigenvalues       = schur.getEigenvaluesArr();
        final ComplexMatrix             eigenvectorMatrix = schur.computeEigenvectors(A,
                                                                                      eigenvectorIters,
                                                                                      realSymmAbsTol,
                                                                                      linearSolver,
                                                                                      evecFailureMode).orElseThrow();
        
        balancer.adjustEigenvectors(eigenvectorMatrix); /* in-place modification. eigenvectorMatrix isn't stored
                                                         * elsewhere, so we don't need to worry about corrupting data
                                                         * somewhere else. If no balancing is necessary, each entry is
                                                         * simply multiplied by 1.0. */
        
        return new EigenSystem(eigenvalues, eigenvectorMatrix);
    }
    
    /** Compute and return a {@link ComplexSchurDecomposition} for {@link ComplexMatrix} {@code A}
     * 
     * @param A the {@link ComplexMatrix} {@code A}
     * @param justEigenvalues {@code true} if we only want to compute the Eigenvalues, {@code false} to compute a full
     *        {@link ComplexSchurDecomposition}
     * @param maxIters specify the maximum iterations to consider
     * @param evaluationMonitor a {@link ComplexMatrix} {@link Consumer} to update with the current solution as we
     *        iterate in {@link ComplexSchurDecomposition}. May be {@code null}. Including this severely impacts
     *        performance! See {@link ComplexSchurDecomposition#of(ComplexMatrix, SchurMode, int, Consumer)}.
     * @return a {@link ComplexSchurDecomposition} of {@link ComplexMatrix} {@code A} */
    private static ComplexSchurDecomposition getSchur(final ComplexMatrix A,
                                                      final boolean justEigenvalues,
                                                      final int maxIters,
                                                      final Consumer<ComplexMatrix> evaluationMonitor) {
        Numerics.requireSquare(A);
        final SchurMode schurMode = justEigenvalues ? SchurMode.EIGENVALUES : SchurMode.SCHUR_FORM;
        return ComplexSchurDecomposition.of(A, schurMode, maxIters, evaluationMonitor);
    }
}
