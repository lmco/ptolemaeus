/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.eigen;

import static java.util.Objects.requireNonNull;

import java.util.Optional;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.NVector;

/** An {@link EigenPair} holds an Eigenvalue and the Eigenvector associated with that Eigenvalue
 * 
 * @param eigenvalue the {@link Complex Eigenvalue}. Never {@code null}.
 * @param eigenvector the {@link ComplexVector Eigenvector}. Never {@code null}.
 *        
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public record EigenPair(Complex eigenvalue, ComplexVector eigenvector) {
    
    /** An {@link EigenPair} holds an Eigenvalue and the Eigenvector associated with that Eigenvalue
     * 
     * @param eigenvalue the {@link Complex Eigenvalue}. Never {@code null}.
     * @param eigenvector the {@link ComplexVector Eigenvector}. Never {@code null}. */
    public EigenPair {
        requireNonNull(eigenvalue,  "must specify an Eigenvalue");
        requireNonNull(eigenvector, "must specify an Eigenvector");
        
        Validation.requireFalse(eigenvalue.isNaN(), "Eigenvalues may not be NaN");
    }
    
    /** Possibly get the {@link RealEigenPair} equivalent to this {@link EigenPair}.<br>
     * <br>
     * 
     * If neither of {@link #eigenvalue()} nor {@link #eigenvector()} have any {@link Complex#getImaginary() imaginary}
     * components larger than {@code absMaxIm}, we consider those imaginary parts exactly zero and return an
     * {@link Optional} holding the {@link RealEigenPair} with the real components of both. Otherwise, we return an
     * empty {@link Optional}.
     * 
     * @param absMaxIm the maximum allowable imaginary part of an Eigenvalue or Eigenvector component
     * @return the {@link Optional} {@link RealEigenPair} */
    public Optional<RealEigenPair> asRealEigenPair(final double absMaxIm) {
        if (FastMath.abs(this.eigenvalue.getImaginary()) > absMaxIm) {
            return Optional.empty();
        }
        
        final int dim = this.eigenvector.getDimension();
        final double[] realEigvecArr = new double[dim];
        for (int i = 0; i < dim; i++) {
            final Complex ith = this.eigenvector.getEntry(i);
            if (FastMath.abs(ith.getImaginary()) > absMaxIm) {
                return Optional.empty();
            }
            
            realEigvecArr[i] = ith.getReal();
        }
        
        final RealEigenPair realPair = new RealEigenPair(this.eigenvalue.getReal(), new NVector(realEigvecArr));
        return Optional.of(realPair);
    }
}
