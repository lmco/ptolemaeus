/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.householderreductions.ComplexHessenbergReduction;
import ptolemaeus.math.linear.householderreductions.ComplexHouseholder;
import ptolemaeus.math.linear.householderreductions.ComplexQRDecomposition;
import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

// CHECKSTYLE.OFF: LineLength - the references make the lines too long
/** See {@link SchurHelper} - the workhorse of this package - for implementation details
 * 
 * <br>
 * <br>
 * 
 * Compute the {@link ComplexSchurDecomposition} of a square {@link ComplexMatrix} {@code A = QUQ}<sup>H</sup>, where
 * {@code Q} is unitary and {@code U} is upper-triangular. Because {@code Q} is a unitary similarity transformation,
 * {@code A} and {@code U} have the same Eigenvalues. <br>
 * <br>
 * 
 * At a high-level, QR algorithms compute the Eigenvalues of the input matrix and accumulate unitary transformations
 * that were used to get to that point in {@code Q}, and compute {@code U} at the end as
 * {@code U = Q}<sup>H</sup>{@code AQ}. <br>
 * <br>
 * 
 * These algorithms don't compute {@code U} directly because we use deflations (see {@link QRDeflations}) to reduce
 * "deflate" the problem into smaller sub problems, which makes the difference between tractable and intractable for
 * even moderately large matrices. <br>
 * <br>
 * 
 * TODO Ryan Moser, 2024-08-19: implement aggressive early deflation. It should be easy enough. Follow the algorithm
 * specifications in [2] and [4] below. Ask me - RM - for a starting point, because I have some of this done already<br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-01-28: {@link #computeEigenvectors} doesn't
 * actually do what LAPACK does in the case of a Schur decomposition, rather, the complete {@code U} matrix is computed
 * in-place, and the column vectors are used to compute the Eigenvectors. It's not clear that this is a detrimental
 * difference, but we should do it eventually if for no other reason than to be consistent. Doing this requires doing
 * the following TODO <br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-02-09: refactor to be more like LAPACK. As-is, we accumulate {@link SchurHelper#currentQ} and
 * {@link SchurHelper#currentU} in blocks regardless of the the {@link SchurMode}, but in LAPACK, if the {@code Q}
 * factor is requested explicitly, the full upper-triangular form of {@code U} is computed in-place and, likewise, the
 * full {@code Q} is updated at every step, rather than accumulated in block multiplications; e.g., how we do it at the
 * end of {@link #solve}. <br>
 * <br>
 * 
 * Algorithm description:
 * 
 * <br>
 * <br>
 * 
 * The first step is to ensure that the input matrix is already upper-Hessenberg, or reduce it to upper-Hessenberg using
 * a full {@link ComplexHessenbergReduction}. Upper-Hessenberg matrices have the form
 * 
 * <pre>
 * H = 
 * 
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . .
 *       . . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .
 *                           . . .
 *                             . .
 * </pre>
 * 
 * where {@code .}s represent arbitrary {@link Complex} elements and empty spots are zeros.
 * 
 * <br>
 * <br>
 * 
 * Now in upper-Hessenberg form, we perform an iteration that implicitly computes a shifted QR step (see references for
 * details, I don't want to put it all here).
 * 
 * <br>
 * <br>
 * 
 * Here, we do a large-bulge multi-shift QR sweep to create a single large "shift" transformation that results in a
 * single large bulge on the first sub-diagonal of the Hessenberg form. Empirically - according to [1] and [3], using
 * more than 10 shifts with a large-bulge sweep iteration results in significant so-called shift-blurring, severely
 * impacting the rate of convergence. Thus {@link QRShifts#computeNumShiftsPerBulge(int)} does not return more than
 * {@code 10}. <br>
 * <br>
 * 
 * The shift transformation is a {@link ComplexHouseholder} matrix for the first column of the matrix polynomial
 * shift-function {@code P(U)}:
 * 
 * <br>
 * <br>
 * 
 * {@code P(U) = (U - }&lambda;<sub>m</sub>{@code I) .
 * (U - }&lambda;<sub>m-1</sub>{@code I) .
 * (U - }&lambda;<sub>m-2</sub>{@code I)
 * ...
 * (U - }&lambda;<sub>2</sub>{@code I) .
 * (U - }&lambda;<sub>1</sub>{@code I)}<br>
 * <br>
 * 
 * where {@code m} is the number of "shifts" to apply, but we do it <em>implicitly</em>, thus avoiding the O((n^3)^m)
 * cost of evaluating the full that polynomial. See
 * {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)} details<br>
 * <br>
 * 
 * We only need the non-trivial (m+1)x(m+1) block of the Householder matrix to apply it to {@code U}, because the
 * Householder is really just an identity matrix with a (m+1)x(m+1) non-identity block on its diagonal and, in this
 * case, in the first place on the diagonal, and we can use
 * {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} and
 * {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} to do this efficiently:<br>
 * <br>
 * 
 * <pre>
 * . . .
 * . . .
 * . . .
 *       1 
 *         1 
 *           1 
 *             1 
 *               1 
 *                 1 
 *                   1 
 *                     1 
 *                       1
 *                         1
 *                           1
 *                             1
 *                               1
 * </pre>
 * 
 * Applying this unitary similarity transformation to {@code H} yields a "bulge" (the elements which do not conform to
 * the upper-Hessenberg form):
 * 
 * <pre>
 * H = 
 * 
 * m m m
 * . . . . . . . . . . . . . . . .  m
 * b b b . . . . . . . . . . . . .  m
 * b b b . . . . . . . . . . . . .  m
 * b b b . . . . . . . . . . . . .
 *       . . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .
 *                           . . .
 *                             . .
 * </pre>
 * 
 * Now we need to use a series of unitary similarity transforms to bring {@code H} back into upper-Hessenberg form. This
 * process is called "bulge chasing". It's called this because at each step of the reduction (as one can see when using
 * an evaluation monitor) the bulge is "shifted down" one place along the first sub-diagonal until it "falls off":<br>
 * <br>
 * 
 * <ul>
 * <li>in this example, we're using two shifts. This has a special name: the implicit double-shift QR algorithm
 * <li>
 * <li>{@code m}s mark the columns and rows modified in each step of {@link ComplexHessenbergReduction} (note that .'s
 * that are in these entries are not unchanged, they're just not relevant to the illustration)</li>
 * <li>{@code b}s are used to mark the elements of a (2+1)x(2+1) == 3x3 "bulge" as it moves after each
 * multiplication</li>
 * </ul>
 * 
 * <pre>
 *   m m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .  m
 *   b b b . . . . . . . . . . . .  m
 *   b b b . . . . . . . . . . . .  m
 *   b b b . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .
 *                           . . .
 *                             . .
 * </pre>
 * 
 * <pre>
 *     m m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .  m
 *     b b b . . . . . . . . . . .  m
 *     b b b . . . . . . . . . . .  m
 *     b b b . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .
 *                           . . .
 *                             . .
 * </pre>
 * 
 * <pre>
 *       m m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . .  m
 *       b b b . . . . . . . . . .  m
 *       b b b . . . . . . . . . .  m
 *       b b b . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .
 *                           . . .
 *                             . .
 * </pre>
 * 
 * etc.
 * 
 * <pre>
 *                       m m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . .
 *       . . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .  m
 *                       b b b . .  m
 *                       b b b . .  m
 *                       b b b . .
 *                             . .
 * </pre>
 * 
 * <pre>
 *                         m m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . .
 *       . . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .  m
 *                         b b b .  m
 *                         b b b .  m
 *                         b b b .
 * </pre>
 * 
 * <pre>
 *                           m m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . .
 *       . . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .  m
 *                           b b b  m
 *                           b b b  m
 * </pre>
 * 
 * <pre>
 *                             m m
 * . . . . . . . . . . . . . . . .
 * . . . . . . . . . . . . . . . .
 *   . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . .
 *       . . . . . . . . . . . . .
 *         . . . . . . . . . . . .
 *           . . . . . . . . . . .
 *             . . . . . . . . . .
 *               . . . . . . . . .
 *                 . . . . . . . .
 *                   . . . . . . .
 *                     . . . . . .
 *                       . . . . .
 *                         . . . .
 *                           . . .  m
 *                             b b  m
 * </pre>
 * 
 * And now we're back to upper-Hessenberg form!
 * 
 * <br>
 * <br>
 * 
 * After each sweep, we check whether we can "deflate" the problem; i.e., break the problem into two (or more, in
 * general) sub-problems that can be solved independently, which allows us to ignore updating large portions of the
 * matrix. See {@link QRDeflations} for details.<br>
 * <br>
 * 
 * This proceeds until we've deflated to a series of small upper-Hessenberg matrices along the diagonal of {@code H},
 * each of which we solve in the same way. This recursive procedure continues until we deflate totally into {@code 1x1}
 * and {@code 2x2} matrices along the main diagonal of {@code H}. The {@code 1x1}s are Eigenvalues themselves, and the
 * {@code 2x2}s are matrices with complex conjugate pairs of Eigenvalues of {@code H}, which we can reduce in closed
 * form. <br>
 * <br>
 * 
 * Book/paper references:<br>
 * 
 * <ul>
 * <li>[1] Numerical Methods for General and Structured Eigenvalue Problems, Kressner</li>
 * <li>[2] Matrix Computations, 4th Edition, Golub and Van Load</li>
 * <li>[3] <a href=
 * "https://www.researchgate.net/publication/2847350_The_Multishift_QR_Algorithm_Part_I_Maintaining_Well-Focused_Shifts_and_Level_3_Performance">
 * The Multishift QR Algorithm. Part I: Maintaining Well-Focused Shifts and Level 3 Performance</a></li>
 * <li>[4] <a href=
 * "https://www.researchgate.net/publication/2849210_The_Multishift_QR_Algorithm_Part_II_Aggressive_Early_Deflation">
 * The Multishift QR Algorithm. Part II: Aggressive Early Deflation</a></li>
 * <li>[5] <a href="https://www.netlib.org/lapack/lawnspdf/lawn122.pdf">A New Deflation Criterion for the QR
 * Algorithm</a></li>
 * </ul>
 * 
 * LAPACK v3.12.0 references:<br>
 * 
 * <ul>
 * <li><a href="https://github.com/Reference-LAPACK/lapack/blob/v3.12.0/SRC/zhseqr.f"> ZHSEQR</a></li>
 * <li><a href="https://github.com/Reference-LAPACK/lapack/blob/v3.12.0/SRC/zlahqr.f"> ZLAHQR</a></li>
 * <li><a href="https://github.com/Reference-LAPACK/lapack/blob/v3.12.0/SRC/zlaqr0.f"> ZLAQR0</a></li>
 * <li><a href="https://github.com/Reference-LAPACK/lapack/blob/v3.12.0/SRC/zlaqr5.f"> ZLAQR5</a></li>
 * <li><a href="https://github.com/Reference-LAPACK/lapack/blob/v3.12.0/SRC/iparmq.f"> IPARMQ</a></li>
 * </ul>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see SchurHelper
 * @see QRShifts
 * @see QRDeflations
 * @see SchurMode */
// CHECKSTYLE.ON: LineLength
public class ComplexSchurDecomposition {
    
    /** A {@link Logger} for logging */
    private static final Logger LOGGER = LogManager.getLogger(ComplexSchurDecomposition.class);
    
    /** We use this constant so that it's easier to experiment with setting it on and off, as it's not something that
     * should be user-configurable. LAPACK operates in a way equivalent to this being enabled. */
    static final boolean SET_EXACT_ZEROES = true;
    
    /** We multiply the dimension of the square input matrix by this to compute the max number of iterations to
     * consider. This is the technique used by each reference I've read, but there is not necessarily a convergence
     * guarantee, thus the value is configurable. */
    private static final int MAX_ITERS_SCALE = 30;
    
    /** When {@link #computeEigenvectors computing Eigenvectors}, if the dimension of the input matrix is greater than
     * or equal to this, we'll parallelize the routine. */
    static final int PARALLEL_THRESHOLD = 96;
    
    /** a 1x1 identity matrix */
    private static final ComplexMatrix ID_1X1 = ComplexMatrix.identity(1);
    
    /** a 2x2 identity matrix */
    private static final ComplexMatrix ID_2X2 = ComplexMatrix.identity(2);
    
    /** the {@link SchurMode} in which we computed a {@link ComplexSchurDecomposition} */
    private final SchurMode mode;
    
    /** The unitary {@link ComplexMatrix} {@code Q} in {@code A = QUQ}<sup>-1</sup>{@code == QUQ}<sup>H</sup> */
    private final ComplexMatrix Q;
    
    /** The upper-triangular, unitarily similar to the original {@code A} in
     * {@code A = QUQ}<sup>-1</sup>{@code == QUQ}<sup>H</sup> */
    private final ComplexMatrix U;
    
    /** The vector of Eigenvalues for the original {@link ComplexMatrix} that's been decomposed. Ideally, these will be
     * be identical to the diagonal of {@link #getU()}, but practically they will be slightly different due to numerical
     * imprecision. */
    private final ComplexVector eigenvalues;
    
    /** Constructor
     * 
     * @param mode the {@link SchurMode} in which we computed a {@link ComplexSchurDecomposition}
     * @param Q the unitary {@link ComplexMatrix} {@code Q} from {@code A = QUQ}<sup>-1</sup>{@code == QUQ}<sup>H</sup>
     * @param U the upper-triangular {@link ComplexMatrix} {@code U} from
     *        {@code A = QUQ}<sup>-1</sup>{@code == QUQ}<sup>H</sup>
     * @param eigenvalues the Eigenvalues of the original {@link ComplexMatrix} {@code A = QUQ}<sup>H</sup> */
    ComplexSchurDecomposition(
                              final SchurMode mode,
                              final ComplexMatrix Q,
                              final ComplexMatrix U,
                              final ComplexVector eigenvalues) {
        this.mode        = mode;
        this.Q           = Q;
        this.U           = U;
        this.eigenvalues = eigenvalues;
    }
    
    /** @return the {@link SchurMode} used to compute {@code this} {@link ComplexSchurDecomposition} */
    public SchurMode getSchurMode() {
        return this.mode;
    }
    
    /** @return an {@link Optional} holding the unitary {@code Q} from the Schur decomposition
     *         {@code A = QUQ}<sup>-1</sup>{@code == QUQ}<sup>H</sup>. If {@link SchurMode#EIGENVALUES} was used, this
     *         {@link Optional} will be empty. */
    public Optional<ComplexMatrix> getQ() {
        return Optional.ofNullable(this.Q).map(ComplexMatrix::copy);
    }
    
    /** @return an {@link Optional} holding the upper-triangular {@code U} from the Schur decomposition
     *         {@code A = QUQ}<sup>-1</sup>{@code == QUQ}<sup>H</sup>. If {@link SchurMode#EIGENVALUES} was used, this
     *         {@link Optional} will be empty. If {@link SchurMode#BLOCK_SCHUR_FORM} was used, this will be in block
     *         Schur form. See {@link SchurMode#BLOCK_SCHUR_FORM} for details.
     *         
     * @implNote {@code U} is actually computed after the fact as {@code U == Q}<sup>H</sup>{@code AQ}, so the diagonal
     *           entries of {@code U} may not match {@link #getEigenvalues()} exactly */
    public Optional<ComplexMatrix> getU() {
        return Optional.ofNullable(this.U).map(ComplexMatrix::copy);
    }
    
    /** @return the array of Eigenvalues of the original matrix. These are always computed. */
    public Complex[] getEigenvaluesArr() {
        return this.eigenvalues.getData();
    }
    
    /** @return the Eigenvalues of the original matrix. These are always computed. */
    public ComplexVector getEigenvalues() {
        return new ComplexVector(this.eigenvalues);
    }
    
    /** @return if {@link #Q} and {@link #U} are present, compute {@code QUQ}<sup>H</sup>, which should be equal (within
     *         tolerance) to the original matrix that was decomposed. Otherwise, throw an exception.
     *         
     * @apiNote this is only {@code package-private} because it's not useful outside of testing */
    ComplexMatrix computeQUQH() {
        // below, ensure that Q and U are present.  You can't have one without the other.
        final ComplexMatrix notOptionalQHere = this.getQ().orElseThrow();
        return notOptionalQHere.times(this.U).timesConjugateTransposeOf(notOptionalQHere);
    }
    
    /** Compute the {@link ComplexSchurDecomposition} of the given {@link ComplexMatrix A}. If
     * {@link ComplexMatrix#isReal(double) A has exclusively real entries}, then we return the
     * {@link SchurMode#BLOCK_SCHUR_FORM real Schur form} of {@code A}. Else, we return the {@link SchurMode#SCHUR_FORM
     * completely upper-Triangular Schur form}.
     * 
     * @param A the {@link ComplexMatrix} for which we want a {@link ComplexSchurDecomposition}
     * @return the {@link ComplexSchurDecomposition} of {@code A} */
    public static ComplexSchurDecomposition of(final ComplexMatrix A) {
        final SchurMode schurMode = A.isReal(0.0) ? SchurMode.BLOCK_SCHUR_FORM : SchurMode.SCHUR_FORM;
        return of(A, schurMode, chooseMaxIters(A), null);
    }
    
    /** Compute the {@link ComplexSchurDecomposition} of the given {@link ComplexMatrix A}
     * 
     * @param A the {@link ComplexMatrix} for which we want a {@link ComplexSchurDecomposition}
     * @param schurMode the solve mode to use.  See {@link SchurMode} for details.
     * @return the {@link ComplexSchurDecomposition} of {@code A} */
    public static ComplexSchurDecomposition of(final ComplexMatrix A, final SchurMode schurMode) {
        return of(A, schurMode, chooseMaxIters(A), null);
    }
    
    /** Compute the {@link ComplexSchurDecomposition} of the given {@link ComplexMatrix A}, with additional fine-tuning
     * available.
     * 
     * @param A the {@link ComplexMatrix} for which we want a {@link ComplexSchurDecomposition}
     * @param schurMode the solve mode to use. See {@link SchurMode} for details.
     * @param maxIters the maximum iterations we will consider at the top level. Recursive calls use a value computed
     *        as-needed ({@link #chooseMaxIters(AnyMatrix)}).
     * @param evaluationMonitor a {@link ComplexMatrix} {@link Consumer} that's updated with changes to the {@code U}
     *        matrix. An evaluation monitor is a debugging/maintenance tool and should not be used by default, as it
     *        will be very performance intensive.
     * @return the {@link ComplexSchurDecomposition} of {@code A}. */
    public static ComplexSchurDecomposition of(final ComplexMatrix A,
                                               final SchurMode schurMode,
                                               final int maxIters,
                                               final Consumer<ComplexMatrix> evaluationMonitor) {
        final boolean isHermitian = ComplexMatrix.isHermitian(A, Complex.ZERO);
        final ComplexMatrix ACopy = A.copy(); /* A must be copied before we start, as the matrix passed into 
                                               * SchurHelper is modified as we iterate */
        final SchurHelper schur = new SchurHelper(ACopy,
                                                  isHermitian,
                                                  schurMode,
                                                  maxIters,
                                                  evaluationMonitor,
                                                  SET_EXACT_ZEROES,
                                                  0);
        
        boolean alreadyUpperHessenberg = true;
        for (int i = 2; alreadyUpperHessenberg && i < schur.dim; i++) {
            for (int j = 0; alreadyUpperHessenberg && j < i - 1; j++) {
                alreadyUpperHessenberg &= A.getEntry(i, j).equals(Complex.ZERO);
            }
        }
        
        return solve(A, schur, alreadyUpperHessenberg);
    }
    
    /** Choose a max iterations we should use when computing the {@link ComplexSchurDecomposition} of the given
     * {@link AnyMatrix}.
     * 
     * @param A the matrix
     * @return the max default max iterations for a matrix of the given size */
    public static int chooseMaxIters(final AnyMatrix A) {
        return MAX_ITERS_SCALE * A.getRowDimension();
    }
    
    /** @param A the original {@link ComplexMatrix} for which we're computing a {@link ComplexSchurDecomposition}. This
     *        is passed around as is for two reasons. First, we want to be able to report matrices that cause unexpected
     *        failure. Second, dynamically deciding when to copy and not copy is far too messy with the recursion.
     * @param schur the {@link SchurHelper} instance that holds the data that's updated as we iterate; i.e., this is
     *        updated <em>in place</em>!
     * @param alreadyUpperHessenberg the {@code true} whether {@link SchurHelper#currentU schur.currentU} is already
     *        upper-Hessenberg, {@code false} otherwise.
     * @return the {@link ComplexSchurDecomposition} of {@link SchurHelper#currentU schur.currentU}
     *         <em>in-place</em>! */
    // CHECKSTYLE.OFF: MethodLength
    static ComplexSchurDecomposition solve(final ComplexMatrix A,
                                           final SchurHelper schur,
                                           final boolean alreadyUpperHessenberg) {
        if (schur.dim == 1) { // 1x1
            final ComplexVector dim1Vec;
            if (schur.isHermitian) {
                dim1Vec = new ComplexVector(schur.currentU.getEntry(0, 0).getReal());
            }
            else {
                dim1Vec = new ComplexVector(schur.currentU.getEntry(0, 0));
            }
            
            return new ComplexSchurDecomposition(schur.schurMode,
                                                 ID_1X1,
                                                 schur.currentU,
                                                 dim1Vec);
            // no need to update the evaluation monitor
        }
        if (schur.dim == 2) { // 2x2
            final ComplexSchurDecomposition answer2x2 = compute2x2ClosedForm(schur);
            if (schur.hasEvalMonitor) {
                schur.updateUBlock(answer2x2.U, 0, 0);
            }
            return answer2x2;
        }
        
        // at this point, the shape (non-zero structure) of schur.currentU could be anything
        
        // (3+)x(3+)
        if (!alreadyUpperHessenberg) {
            schur.doHessenbergReduction();
        }
        
        // @formatter:off
        /** Once here, schur.currentU is necessarily upper-Hessenberg:
         * . . . . . . . . . . . . . . . .
         * . . . . . . . . . . . . . . . .
         *   . . . . . . . . . . . . . . .
         *     . . . . . . . . . . . . . .
         *       . . . . . . . . . . . . .
         *         . . . . . . . . . . . .
         *           . . . . . . . . . . .
         *             . . . . . . . . . .
         *               . . . . . . . . .
         *                 . . . . . . . .
         *                   . . . . . . .
         *                     . . . . . .
         *                       . . . . .
         *                         . . . .
         *                           . . .
         *                             . . */
        // @formatter:on
        for (int count = 0; count < schur.maxIters; count++) {
            final ComplexMatrix[] deflations = QRDeflations.computeDeflations(schur.currentU,
                                                                              schur.numShiftsPerBulge,
                                                                              schur.smlnum);
            if (deflations == null) { // no deflation opportunities detected
                schur.applyShiftBulge(); // apply bulge
                
                // @formatter:off
                /* . . . . . . . . . . . . . . . .
                 * b b b b . . . . . . . . . . . . <--- bulge entries
                 * b b b b . . . . . . . . . . . .      "
                 * b b b b . . . . . . . . . . . .      "
                 * b b b b . . . . . . . . . . . .      "
                 *         . . . . . . . . . . . .
                 *           . . . . . . . . . . .
                 *             . . . . . . . . . .
                 *               . . . . . . . . .
                 *                 . . . . . . . .
                 *                   . . . . . . .
                 *                     . . . . . .
                 *                       . . . . .
                 *                         . . . .
                 *                           . . .
                 *                             . . */
                
                schur.chaseShiftBulge(); // Back to upper-Hessenberg
                
                /* . . . . . . . . . . . . . . . .
                 * . . . . . . . . . . . . . . . .
                 *   . . . . . . . . . . . . . . .
                 *     . . . . . . . . . . . . . .
                 *       . . . . . . . . . . . . .
                 *         . . . . . . . . . . . .
                 *           . . . . . . . . . . .
                 *               . . . . . . . . . <-- if we're lucky, we'll find a deflation opportunity
                 *               . . . . . . . . .
                 *                 . . . . . . . .
                 *                   . . . . . . .
                 *                     . . . . . .
                 *                       . . . . .
                 *                         . . . .
                 *                           . . .
                 *                             . . */
                // @formatter:on
            }
            else {
                if (schur.hasEvalMonitor) {
                    /* If using an evaluation monitor, we assume that the best possible performance is not required, so
                     * we do some extra work. Once we enter this else-statement, currentU is only partially updated
                     * (that's the whole point of the deflation), so we can no longer use it, thus it's o.k. to modify
                     * it for reporting purposes. */
                    
                    // @formatter:off
                    /** stackedDecomposed has the form of two decoupled upper-Hessenberg matrices; e.g.,
                     *    . . . . . 
                     *    . . . . . 
                     *      . . . . 
                     *        . . . 
                     *          . . 
                     *              . . . .
                     *              . . . .
                     *                . . .
                     *                  . . 
                     */
                    // @formatter:on
                    final ComplexMatrix stackedDecomposed = ComplexMatrix.blockDiagonalConcatenate(deflations);
                    schur.updateUBlock(stackedDecomposed, 0, 0);
                }
                
                final int numDeflations = deflations.length; // == 2
                /* currently, this is always exactly 2, but everything here is easily generalized to K > 2 deflations
                 * (create the Helper instances sequentially, solve in parallel, construct solution sequentially). In
                 * fact, that's how I (RM) initially wrote it, solving the deflated problems in parallel. I found,
                 * however, that it is almost never the case that more than one deflation opportunity appears at once
                 * and, indeed, this a typical assumption in descriptions of the implicitly-shifted QR algorithms */
                
                final ComplexVector eigenvalues = new ComplexVector(schur.dim);
                
                int currDiagIndex = 0;
                for (int i = 0; i < numDeflations; i++) {
                    final ComplexMatrix deflation = deflations[i];
                    
                    final Consumer<ComplexMatrix> subEvalMont;
                    if (schur.hasEvalMonitor) {
                        /* these Consumers update A with the iterations of this algorithm applied to its deflations, and
                         * it does this nested arbitrarily many times (or at least until there's a stack overflow, but
                         * that's infeasible). */
                        
                        final int fCurrDiagIndex = currDiagIndex;
                        subEvalMont = subUpdate -> {
                            schur.updateUBlock(subUpdate, fCurrDiagIndex, fCurrDiagIndex);
                        };
                    }
                    else {
                        subEvalMont = null;
                    }
                    
                    final SchurHelper deflatedSchur = new SchurHelper(deflation,
                                                                      schur.isHermitian,
                                                                      schur.schurMode,
                                                                      chooseMaxIters(deflation),
                                                                      subEvalMont,
                                                                      SET_EXACT_ZEROES,
                                                                      schur.recursiveDepth + 1);
                    
                    // recurse:
                    final ComplexSchurDecomposition subDecomp = solve(A, deflatedSchur, true);
                    
                    // modifies Q data!
                    schur.possiblyPostMultiplyQ(subDecomp.Q, currDiagIndex); // right-update for Q
                    // modifies Q data!
                    
                    eigenvalues.setSubVector(currDiagIndex, subDecomp.eigenvalues);
                    
                    currDiagIndex += deflation.getColumnDimension();
                }
                
                if (schur.schurMode == SchurMode.EIGENVALUES) {
                    return new ComplexSchurDecomposition(schur.schurMode, null, null, eigenvalues);
                }
                
                // we only need to compute U when we're about to return from the first call in the stack
                final ComplexMatrix Q = schur.currentQ;
                final ComplexMatrix U = schur.recursiveDepth != 0 ? null : Q.conjugateTransposeMultiply(A).times(Q);
                
                return new ComplexSchurDecomposition(schur.schurMode, schur.currentQ, U, eigenvalues);
            }
        }
        
        throw NumericalMethodsException.formatted(
                                  "The QR algorithm failed to converge in %d iterations for the following matrix:%n%s",
                                  schur.maxIters,
                                  Numerics.toJavaCode(A)).get();
    }
    // CHECKSTYLE.ON: MethodLength
    
    /** Compute and return a {@link ComplexSchurDecomposition} for the given {@code 2x2} {@link ComplexMatrix}<br>
     * <br>
     * 
     * See {@link #compute2x2ClosedForm(ComplexMatrix, boolean, SchurMode, boolean)}
     * 
     * @param schur the {@link SchurHelper} for which we want to compute a closed-form solution
     * @return the closed-form solution */
    private static ComplexSchurDecomposition compute2x2ClosedForm(final SchurHelper schur) {
        return compute2x2ClosedForm(schur.currentU, schur.isHermitian, schur.schurMode, schur.hasEvalMonitor);
    }
    
    /** Compute and return a {@link ComplexSchurDecomposition} for the given {@code 2x2} {@link ComplexMatrix}<br>
     * <br>
     * 
     * We compute (construct, really) a closed-form expression using an Eigenvector of {@code M} and a vector orthogonal
     * to that Eigenvector. These two vectors allow us to construct a solution because - when concatenated as column
     * vectors of a matrix - they necessarily result in a unitary (orthogonal) triangularization of {@code M}; i.e., a
     * Schur decomposition.<br>
     * <br>
     * 
     * See <a href="https://www.home.uni-osnabrueck.de/mfrankland/Math416/Math416_SchurDecomposition.pdf"> this PDF</a>
     * for an example of this construction.<br>
     * <br>
     * 
     * TODO Ryan Moser, 2024-08-07: see TODO in
     * {@link QRShifts#computeEigenTrailingPrincipal2x2(ComplexMatrix, boolean)}<br>
     * 
     * @param M the {@code 2x2} matrix for which we want the {@link ComplexSchurDecomposition}
     * @param isHermitian {@code true} if the overall system {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) is
     *        Hermitian}, false otherwise. We can pass this value through rather than compute it because any diagonal
     *        sub-matrix of a Hermitian matrix is also Hermitian.
     * @param schurMode the {@link SchurMode}, which determines how much work to do here
     * @param hasEvalMonitor {@code true} if we need to consider an evaluation monitor, {@code false} otherwise
     * @return the {@link ComplexSchurDecomposition} */
    static ComplexSchurDecomposition compute2x2ClosedForm(final ComplexMatrix M,
                                                          final boolean isHermitian,
                                                          final SchurMode schurMode,
                                                          final boolean hasEvalMonitor) {
        final ComplexVector eigenValues2x2 = QRShifts.computeEigenTrailingPrincipal2x2(M, isHermitian);
        if (schurMode == SchurMode.EIGENVALUES && !hasEvalMonitor) {
            return new ComplexSchurDecomposition(schurMode, null, null, eigenValues2x2);
        }
        
        if (schurMode == SchurMode.BLOCK_SCHUR_FORM) {
            return new ComplexSchurDecomposition(schurMode, ID_2X2, M, eigenValues2x2);
        }
        
        final Complex l1 = eigenValues2x2.getEntry(1); // choose an Eigenvalue - this can probably be done intelligently
        final Complex c  = M.getEntry(1, 0);
        
        final Complex[] eigenvectorArr; // first, construct the 2D Eigenvector associated with the given Eigenvalue
        if (c.isZero()) {
            final Complex b = M.getEntry(0, 1);
            if (b.isZero()) { // b == c == 0
                eigenvectorArr = new Complex[] { Complex.ONE, Complex.ZERO };
            }
            else {            // c == 0
                final Complex a = M.getEntry(0, 0);
                eigenvectorArr = new Complex[] { b, l1.subtract(a) };
            }
        }
        else {                // c != 0
            final Complex d = M.getEntry(1, 1);
            eigenvectorArr = new Complex[] {l1.subtract(d), c};
        }
        
        ComplexVector eigenvector = new ComplexVector(eigenvectorArr[0], eigenvectorArr[1]);
        final Complex hermitianNorm = Complex.valueOf(eigenvector.hermitianNorm());
        eigenvector = eigenvector.dividedBy(hermitianNorm);
        
        /* eVecOrth1 is equal to the following, but is more efficient and avoids any potential precision loss due to the
         * double negation of {@code b}: eigenvectorArr[1].conjugate().negate();
         * 
         * Proof: -conj(a + b i) == -(a - b i) == -a + b i */
        final Complex eVecOrth1 = // vector orthogonal to eigenvector
                Complex.valueOf(-eigenvectorArr[1].getRealPart(),       // -a
                                 eigenvectorArr[1].getImaginaryPart()); //  b i
        final Complex eVecOrth2 = eigenvectorArr[0].conjugate();
        ComplexVector orth2Eigenv = new ComplexVector(eVecOrth1, eVecOrth2);
        orth2Eigenv = orth2Eigenv.dividedBy(hermitianNorm);
        
        final Complex[][] QArr = new Complex[][] {
            { eigenvector.getEntry(0), orth2Eigenv.getEntry(0) },
            { eigenvector.getEntry(1), orth2Eigenv.getEntry(1) }
        }; // ^ an Eigenvector,        ^ orthogonal to that Eigenvector
        
        /* by construction, Q is a unitary matrix (Q^-1 == Q^H) */
        
        final ComplexMatrix Q = new ComplexMatrix(QArr);
        final ComplexMatrix U = Q.conjugateTransposeMultiply(M).times(Q);
        
        return new ComplexSchurDecomposition(schurMode, Q, U, eigenValues2x2);
    }
    
    /** Compute the Eigenvectors of {@code A} - the original {@link ComplexMatrix} for which {@code this} is a
     * {@link ComplexSchurDecomposition} - using {@link #computeEigenvectorInverseIteration inverse iteration}<br>
     * <br>
     * 
     * If {@code this} was not created with {@link SchurMode#SCHUR_FORM}, we return an {@link Optional empty Optional}
     * <br>
     * <br>
     * 
     * The {@link #getEigenvalues()} and the Eigenvectors computed here are indexed the same way; e.g., to get the
     * {@code i}<sup>th</sup> Eigenvalue and Eigenvector pair:
     * 
     * <pre>
     * <code>
     * final ComplexVector eigenvalues  = schur.getEigenvalues();
     * final ComplexMatrix eigenvectors = schur.computeEigenvectors(...).orElseThrow();
     * 
     * final Complex       eigenvalue  = eigenvalues.getEntry(i);
     * final ComplexVector eigenvector = new ComplexVector(eigenvectors.getColumn(i));
     * </code>
     * </pre>
     * 
     * As with all Eigenvector algorithms, it is possible that we fail to compute all of them, because (e.g.) we find
     * duplicates, fail to properly converge, etc. To mitigate this as best we can, the initial guesses are the mutually
     * orthogonal column vectors of {@link #getQ()} ({@code Q} is unitary, which implies orthogonal column vectors).
     * 
     * @param A the {@link ComplexMatrix} for which {@code this} is a {@link ComplexSchurDecomposition}
     * @param numIters the number of times to run the inverse iteration routine. Note: this is not an upper-bound, we
     *        will perform <em>exactly</em> this many iterations.  Must be greater than zero.
     * @param realSymmAbsTol the tolerance used when checking whether {@code A} is
     *        {@link ComplexMatrix#isRealSymmetric(double) real-symmetric}. This is also for checking whether an
     *        Eigenvalue is zero, in which case we return the {@link ComplexVector#zero(int) zero-vector}
     * @param solver solver the {@link LinearSolver} to use when solving the system
     *        <code> (A - &lambda;I)v<sub>i+1</sub> = v<sub>i</sub> </code>
     * @param eigvecFailMode the {@link EigenvectorFailureMode} that dictates how we'll handle cases where we fail to
     *        compute an Eigenvector
     * @return an {@link Optional empty Optional} if {@code this} was not created with {@link SchurMode#SCHUR_FORM}.
     *         Otherwise, return a {@link ComplexMatrix} whose columns are the Eigenvectors of {@code A} */
    public Optional<ComplexMatrix> computeEigenvectors(final ComplexMatrix A,
                                                       final int numIters,
                                                       final double realSymmAbsTol,
                                                       final LinearSolver solver,
                                                       final EigenvectorFailureMode eigvecFailMode) {
        Validation.requireGreaterThan(numIters, 0, "numIters");
        if (this.mode != SchurMode.SCHUR_FORM) {
            return Optional.empty();
        }
        
        /* some constants as computed in LAPACK's ZLAEIN - inverse iteration - though it starts with the Hessenberg form
         * computed before we start iterating the QR algorithm as in "solve" here */
        final int    n      = this.Q.getColumnDimension();
        final double rootn  = FastMath.sqrt(1.0 * n);
        final double growTo = 1e-1 / rootn;
        final double eps3   = Numerics.getInftyNormMatrixEpsilon(A);
        final double rtemp  = eps3 / (rootn + 1.0);
        
        // do the (possible) Eigenvalue perturbations separately so that we can parallelize the Eigenvector work
        final List<Complex> perturbedEig = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            if (this.eigenvalues.getEntry(i).norm() <= eps3) {
                perturbedEig.add(this.eigenvalues.getEntry(i));
            }
            else {
                final Complex perturbed = possiblyPerturb(this.eigenvalues.getEntry(i), perturbedEig, eps3);
                perturbedEig.add(perturbed);
            }
        } // "perturbedEig" must not be modified after this point!
        
        final ComplexMatrix eigenvectorMatrix = ComplexMatrix.zero(n, n);
        
        IntStream stream = IntStream.range(0, n);
        if (n > PARALLEL_THRESHOLD) {
            stream = stream.parallel();
        }
        
        stream.forEach(i -> {
            final Complex eigenvalue = perturbedEig.get(i);
            
            if (eigenvalue.norm() <= eps3) {
                eigenvectorMatrix.setColumnVector(i, ComplexVector.zero(n));
            }
            else {
                EigenvectorResult eigvecResult = null;
                for (int attempt = 0; attempt < n; attempt++) {
                    final ComplexVector initialGuessVec;
                    if (attempt == 0) {
                        initialGuessVec = this.Q.getColumnVector(i).copy();
                    }
                    else {
                        initialGuessVec = constructInitGuess(n, attempt, eps3, rtemp, rootn);
                    }
                    
                    eigvecResult = computeEigenvectorInverseIteration(A,
                                                                      eigenvalue,
                                                                      initialGuessVec,
                                                                      growTo,
                                                                      numIters,
                                                                      realSymmAbsTol,
                                                                      solver,
                                                                      eigvecFailMode);
                    if (eigvecResult.pass()) {
                        break;
                    }
                }
                
                // eigvecResult is never null, but SpotBugs doesn't seem to get that
                Objects.requireNonNull(eigvecResult);
                eigenvectorMatrix.setColumnVector(i, eigvecResult.eigenvector());
            }
        });
        
        return Optional.of(eigenvectorMatrix);
    }
    
    /** If the Eigenvalue for which we want an Eigenvector is too close to one we've already done, perturb it and
     * return. Otherwise, return the instance as-is.<br>
     * <br>
     * 
     * The following is the FORTRAN that LAPACK uses to do the same thing. {@code cabs1} is
     * {@link Numerics#complex1Norm(Complex)}, and {@code eps3} is
     * {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix)}.
     * 
     * <pre>
     * <code>
     * DO 70 i = k - 1, kl, -1
     *     IF( SELECT( i ) .AND. cabs1( w( i )-wk ).LT.eps3 ) THEN
     *         wk = wk + eps3
     * </code>
     * </pre>
     * 
     * @param eigenvalue the Eigenvalue for which we want an Eigenvector
     * @param alreadyPerturbed the Eigenvalues we've already (possibly) perturbed
     * @param matrixEpsilon the {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix) matrix epsilon}
     * @return the Eigenvalue, possibly perturbed */
    static Complex possiblyPerturb(final Complex eigenvalue,
                                   final Collection<Complex> alreadyPerturbed,
                                   final double matrixEpsilon) {
        for (final Complex z : alreadyPerturbed) {
            final Complex diff  = eigenvalue.subtract(z);
            final double  cabs1 = Numerics.complex1Norm(diff);
            if (cabs1 < matrixEpsilon) {
                return eigenvalue.add(matrixEpsilon); // adds to the real part
            }
        }
        
        return eigenvalue;
    }
    
    // CHECKSTYLE.OFF: LineLength - long URL
    /** On iterations after the first in
     * {@link #computeEigenvectors}, we use this method to construct
     * new initial guesses. It's not often needed - vectors of the {@link #getQ()} matrix are orthogonal and work well -
     * but this is the standard procedure (mostly - see TODOs).<br>
     * <br>
     * 
     * The following is the FORTRAN that LAPACK uses to do the same thing.
     * 
     * <pre>
     * <code>
     * rtemp = eps3 / ( rootn+one )
     * v( 1 ) = eps3
     * DO 100 i = 2, n
     *    v( i ) = rtemp
     * 100    CONTINUE
     * v( n-its+1 ) = v( n-its+1 ) - eps3*rootn
     * </code>
     * </pre>
     * 
     * @param n the dimension of the system
     * @param attempt the attempt number
     * @param eps3 the {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix)}
     * @param rtemp a value defined in LAPACK's <a href=
     *        "https://www.netlib.org/lapack/explore-html/db/dbc/group__laein_ga077e85f98e15ff7cecfc7e968a3a9861.html#ga077e85f98e15ff7cecfc7e968a3a9861">ZLAEIN</a>
     *        used to construct initial Eigenvector guesses
     * @param rootn a value defined in LAPACK's <a href=
     *        "https://www.netlib.org/lapack/explore-html/db/dbc/group__laein_ga077e85f98e15ff7cecfc7e968a3a9861.html#ga077e85f98e15ff7cecfc7e968a3a9861">ZLAEIN</a>
     *        used to construct initial Eigenvector guesses
     * @return the initial guess */
    static ComplexVector constructInitGuess(final int n,
                                            final int attempt,
                                            final double eps3,
                                            final double rtemp,
                                            final double rootn) {
        final ComplexVector initialGuessVec = ComplexVector.zero(n);
        initialGuessVec.set(rtemp, 0.0);                                // set everything to rtemp
        initialGuessVec.setEntry(0, eps3, 0.0);                         // set the first entry to eps3
        initialGuessVec.addEntry(n - attempt - 1, -eps3 * rootn, 0.0); // set an attempt-dependent val
        
        return initialGuessVec;
    }
    // CHECKSTYLE.ON: LineLength - long URL
    
    /** Use inverse iteration to compute an Eigenvector for the given Eigenvalue
     * 
     * @param A the {@link ComplexMatrix} for which we want Eigenvectors
     * @param eigenvalue the Eigenvalue for which we want an Eigenvector
     * @param initialGuessVec the initial guess
     * @param growTo if the inverse iteration procedure doesn't result in an Eigenvector guess which has norm this much
     *        larger than the previous guess, then we declare a failure.
     * @param numIters the number of times to run the inverse iteration routine. Note: this is not an upper-bound, we
     *        will perform <em>exactly</em> this many iterations.  Must be greater than zero.
     * @param realSymmAbsTol the tolerance used when checking whether {@code A} is
     *        {@link ComplexMatrix#isRealSymmetric(double) real-symmetric}
     * @param solver the {@link LinearSolver} to use when solving the system
     *        <code> (A - &lambda;I)v<sub>i+1</sub> = v<sub>i</sub> </code> in the iteration
     * @param eigvecFailMode the {@link EigenvectorFailureMode} determines how to handle the case that we fail to
     *        compute an Eigenvector
     * @return an Eigenvector of {@code A}. In all cases, if we successfully compute an Eigenvector, we return it. All
     *         successful results will be normalized. */
    private static EigenvectorResult computeEigenvectorInverseIteration(final ComplexMatrix A,
                                                                        final Complex eigenvalue,
                                                                        final ComplexVector initialGuessVec,
                                                                        final double growTo,
                                                                        final int numIters,
                                                                        final double realSymmAbsTol,
                                                                        final LinearSolver solver,
                                                                        final EigenvectorFailureMode eigvecFailMode) {
        final EigenvectorResult answer;
        
        if (A.isRealSymmetric(realSymmAbsTol)) {
            answer = doRealInverseIteration(A, eigenvalue, initialGuessVec, growTo, numIters, solver);
        }
        else {
            answer = doComplexInverseIteration(A, eigenvalue, initialGuessVec, growTo, numIters, solver);
        }
        
        if (!answer.pass()) {
            LOGGER.warn(() -> getWarningLogMessage(A, eigenvalue, initialGuessVec, realSymmAbsTol));
            eigvecFailMode.handleFailure(answer.eigenvector());
        }
        
        return answer;
    }
    
    /** Compute a complex-valued Eigenvector using the inverse iteration method. <br>
     * See {@link #computeEigenvectorInverseIteration} for details.
     * 
     * @param A the {@link ComplexMatrix} for which we want an Eigenvector
     * @param eigenvalue the Eigenvalue for which we want a corresponding Eigenvector
     * @param initialGuessVec the initial guess
     * @param growTo if the inverse iteration procedure doesn't result in an Eigenvector guess which has norm this much
     *        larger than the previous guess, then we declare a failure.
     * @param numIters the number of times to run the inverse iteration routine. Note: this is not an upper-bound, we
     *        will perform <em>exactly</em> this many iterations.  Must be greater than zero.
     * @param solver the {@link LinearSolver} to use when solving the system
     *        <code> (A - &lambda;I)v<sub>i+1</sub> = v<sub>i</sub> </code> in the iteration
     * @return the {@link ComplexVector Eigenvector} */
    private static EigenvectorResult doComplexInverseIteration(final ComplexMatrix A,
                                                               final Complex eigenvalue,
                                                               final ComplexVector initialGuessVec,
                                                               final double growTo,
                                                               final int numIters,
                                                               final LinearSolver solver) {
        final int n = initialGuessVec.getDimension();
        final ComplexMatrix AmlambdaI = A.copy();
        for (int i = 0; i < n; i++) {
            AmlambdaI.subtractFromEntry(i, i, eigenvalue.getReal(), eigenvalue.getImaginary());
        }
        
        final ComplexQRDecomposition QR = ComplexQRDecomposition.of(AmlambdaI, true, true);
        
        boolean       pass        = false;
        ComplexVector eigenvector = initialGuessVec;
        for (int i = 0; i < numIters; i++) { // do inverse iteration
            final ComplexVector nextGuess      = QR.solve(eigenvector, solver);
            final ComplexVector nextGuessNormd = nextGuess.normalizedHermitian();
            
            pass        |= nextGuess.sumComplex1Norm() >= growTo;
            eigenvector  = nextGuessNormd;
        }
        
        eigenvector = adjustComplexEigenvector(eigenvector);
        
        return new EigenvectorResult(eigenvector, pass);
    }
    
    /** Multiply the given {@link ComplexVector Eigenvector} by the complex conjugate of its largest entry - largest in
     * terms of the {@link Numerics#complexNorm(double, double) complex 2-norm} - so that it's real valued, and
     * {@link ComplexVector#normalizedHermitian() re-normalize} after.<br>
     * <br>
     * 
     * In all, this results in us multiplying the vector by two scalars which each belong to the complex plane (one of
     * which is strictly real), and Eigenvectors are unique only up to a scaling factor over their underlying field (in
     * this case, the complex plane), so the result is also an Eigenvector.
     * 
     * @param toBeScaled the Eigenvector
     * @return the scaled Eigenvector. Will have {@link ComplexVector#hermitianNorm() Hermitian norm} {@code == 1.0} */
    static ComplexVector adjustComplexEigenvector(final ComplexVector toBeScaled) {
        final int n = toBeScaled.getDimension();
        
        int    maxIndex   = 0;
        double maxSq2Norm = 0.0;
        for (int i = 0; i < n; i++) {
            final double iRe = toBeScaled.getEntryRe(i);
            final double iIm = toBeScaled.getEntryIm(i);
            
            final double entryiNormSq = Numerics.complexNormSq(iRe, iIm);
            if (entryiNormSq > maxSq2Norm) {
                maxSq2Norm = entryiNormSq;
                maxIndex   = i;
            }
        }
        
        final double maxEntryRe = toBeScaled.getEntryRe(maxIndex);
        final double maxEntryIm = toBeScaled.getEntryIm(maxIndex);
        
        return toBeScaled.times(maxEntryRe, -maxEntryIm).normalizedHermitian(); // negate for conjugate
    }
    
    /** Compute a real-valued Eigenvector using the inverse iteration method. This method assumes that the caller has
     * already ensured that we can limit the search to real-valued vectors.<br>
     * See {@link #computeEigenvectorInverseIteration} for details.
     * 
     * @param compA the {@link ComplexMatrix} for which we want an Eigenvector. We assume that the caller has already
     *        checked to ensure that all entries of {@code A} are real-valued. Any non-real values will be ignored.
     * @param compEig the Eigenvalue for which we want a corresponding Eigenvector. We assume that the caller has
     *        already checked to ensure that {@code eigenvalue} is real-valued. Any non-real values will be ignored.
     * @param compGuessVec the initial guess. Any non-real values will be ignored.
     * @param growTo if the inverse iteration procedure doesn't result in an Eigenvector guess which has norm this much
     *        larger than the previous guess, then we declare a failure.
     * @param numIters the number of times to run the inverse iteration routine. Note: this is not an upper-bound, we
     *        will perform <em>exactly</em> this many iterations.  Must be greater than zero.
     * @param solver the {@link LinearSolver} to use when solving the system
     *        <code> (A - &lambda;I)v<sub>i+1</sub> = v<sub>i</sub> </code> in the iteration
     * @return the {@link ComplexVector Eigenvector} */
    private static EigenvectorResult doRealInverseIteration(final ComplexMatrix compA,
                                                            final Complex compEig,
                                                            final ComplexVector compGuessVec,
                                                            final double growTo,
                                                            final int numIters,
                                                            final LinearSolver solver) {
        
        // we're assuming that the caller has already ensured we can expect real-valued results
        final NVector realInitGuess = compGuessVec.asReal(Double.POSITIVE_INFINITY).orElseThrow();
        final double  eigenvalue    = compEig.getReal();
        
        final Matrix AmlambdaI = compA.asReal(Double.POSITIVE_INFINITY).orElseThrow();
        for (int i = 0; i < realInitGuess.getDimension(); i++) {
            AmlambdaI.subtractFromEntry(i, i, eigenvalue);
        }
        
        final RealQRDecomposition QR = RealQRDecomposition.of(AmlambdaI, true, true);
        
        boolean pass        = false;
        NVector eigenvector = realInitGuess;
        for (int i = 0; i < numIters; i++) { // do inverse iteration
            final NVector nextGuess      = QR.solve(eigenvector, solver);
            final NVector nextGuessNormd = nextGuess.normalized();
            
            double absSum = 0.0;
            for (int entry = 0; entry < nextGuess.getDimension(); entry++) {
                absSum += FastMath.abs(nextGuess.getEntry(entry));
            }
            
            pass        |= absSum >= growTo;
            eigenvector  = nextGuessNormd;
        }
        
        return new EigenvectorResult(new ComplexVector(eigenvector), pass);
    }
    
    /** Get the {@link Level#WARN} {@link Logger logging} message for when computing an Eigenvector fails
     * 
     * @param A the {@link ComplexMatrix} for which we attempted to compute an Eigenvector
     * @param eigenvalue the Eigenvalue for which we attempted to compute a corresponding Eigenvector
     * @param initialGuessVec the initial guess at the Eigenvector
     * @param realSymmAbsTol the tolerance used when checking whether {@code A} is
     *        {@link ComplexMatrix#isRealSymmetric(double) real-symmetric}
     * @return the warning string */
    static String getWarningLogMessage(final ComplexMatrix A,
                                       final Complex eigenvalue,
                                       final ComplexVector initialGuessVec,
                                       final double realSymmAbsTol) {
        final String matrixStr = Numerics.toJavaCode(A, "A");
        final String vectorStr = Numerics.toJavaCode(ComplexMatrix.fromColumns(initialGuessVec),
                                                                  "initialGuessVec");
        final String fmt = """
                Failed to compute an Eigenvector. Consider submitting a bug report with
                
                Complex Schur input-matrix:
                %s
                
                Eigenvector guess:
                %s
                
                Eigenvalue: %s
                
                realSymmAbsTol: %e
                """;
        
        return fmt.formatted(matrixStr, vectorStr, eigenvalue, realSymmAbsTol);
    }
    
    /** The result of computing an Eigenvector
     * 
     * @param eigenvector the Eigenvector candidate
     * @param pass {@code true} if the Eigenvector is acceptable, {@code false} otherwise
     * @author Ryan Moser, Ryan.T.Moser@lmco.com */
    private static record EigenvectorResult(ComplexVector eigenvector, boolean pass) { }
}
