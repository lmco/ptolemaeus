/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import java.util.function.Consumer;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.householderreductions.ComplexHessenbergReduction;
import ptolemaeus.math.linear.householderreductions.ComplexHouseholder;

/**The workhorse of the {@code schur} package.<br>
 * <br>
 * 
 * Instances of {@link SchurHelper} maintain working/in-progress {@link ComplexSchurDecomposition} data and modifies
 * itself to perform the <i>Implicit Multi-Shift QR Algorithm</i><br>
 * <br>
 * 
 * This class was a static inner class of {@link ComplexSchurDecomposition}, and it should be treated as such. This is
 * the reason that many of the fields here are not {@code private}, but only {@code package-private}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see ComplexSchurDecomposition
 * @see QRShifts
 * @see QRDeflations
 * @see SchurMode */
final class SchurHelper {
    
    /** The original {@link ComplexMatrix} for which we're computing a {@link ComplexSchurDecomposition} */
    // final ComplexMatrix originalA;
    
    /** The {@link SchurMode} to use */
    final SchurMode schurMode;
    
    /** The max allowable iterations in one call to {@link ComplexSchurDecomposition#solve} */
    final int maxIters;

    /** A small number for detecting allowable deflations in {@link QRDeflations}, as in the LAPACK spec. */
    final double smlnum;

    /** {@code true} if {@link #evaluationMonitor} {@code != null}, {@code false} otherwise */
    final boolean hasEvalMonitor;

    /** The dimension of {@link #currentQ}, and {@link #currentU} */
    final int dim;
    
    /** The current accumulated unitary matrix. Used to compute {@link #currentU} once converged. */
    final ComplexMatrix currentQ;
    
    /** The current {@code U} matrix whose form depends on {@link #schurMode}. This is actually {@link QRDeflations
     * deflated} as we iterate and, thus, is not actually complete when the iteration converges. Instead, we compute
     * {@code U = Q}<sup>H</sup>{@code AQ} */
    final ComplexMatrix currentU;
    
    /** The number of shifts to use for each shift bulges */
    final int numShiftsPerBulge;
    
    /** The dimension of the "principal bulge" (i.e., measured from the main diagonal) on U before we reach the bottom
     * right corner, where it becomes smaller */
    final int diagBulgeDim;
    
    /** the current depth of recursive calls to {@link ComplexSchurDecomposition#solve}.
     * {@code recursiveDepth == 0} means we are <em>not</em> in a recursive call, {@code recursiveDepth == 1} means we
     * have recursed once, {@code recursiveDepth == 2} means we have recursed twice (i.e., a recursive call has recursed
     * once), etc. */
    final int recursiveDepth;
    
    /** {@code true} if the overall system {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) is Hermitian},
     * {@code false} otherwise. We can pass this value through rather than compute it because any diagonal sub-matrix of
     * a Hermitian matrix is also Hermitian.<br>
     * <br>
     * 
     * If {@code true}, we take special care throughout to ensure that the Eigenvalues returned are strictly real */
    final boolean isHermitian;
    
    /** The evaluation monitor that tracks changes in {@link #currentU} */
    private final Consumer<ComplexMatrix> evaluationMonitor;
    
    /** whether we want to use {@link #setSubHessenbergEntriesToZero(boolean)} after {@link #doHessenbergReduction()}
     * and {@link #chaseShiftBulge()}.  The default is {@code true}, and that agrees with LAPACK. */
    private final boolean setExactZeroes;
    
    /** Constructor
     * 
     * @param currentU the {@link ComplexMatrix} over which we're iterating. This is <em>not</em> copied and
     *        <em>will</em> be modified!
     * @param isHermitian {@code true} if the overall system {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) is
     *        Hermitian}, false otherwise. We can pass this value through rather than compute it because any diagonal
     *        sub-matrix of a Hermitian matrix is also Hermitian.
     * @param schurMode the {@link SchurMode} to use
     * @param maxIters the max allowable iterations in one call to {@link ComplexSchurDecomposition#solve}
     * @param evaluationMonitor the evaluation monitor that tracks changes in {@link #currentU}
     * @param setExactZeroes whether we want to use {@link #setSubHessenbergEntriesToZero(boolean)} after
     *        {@link #doHessenbergReduction()} and {@link #chaseShiftBulge()}. The default is {@code true}, and that
     *        agrees with LAPACK.
     * @param recursiveDepth the current depth of recursive calls to {@link ComplexSchurDecomposition#solve}. See
     *        {@link #recursiveDepth} for details. */
    SchurHelper(final ComplexMatrix currentU,
                final boolean isHermitian,
                final SchurMode schurMode,
                final int maxIters,
                final Consumer<ComplexMatrix> evaluationMonitor,
                final boolean setExactZeroes,
                final int recursiveDepth) {
        this.isHermitian       = isHermitian;
        this.evaluationMonitor = evaluationMonitor;
        this.schurMode         = schurMode;
        this.maxIters          = maxIters;
        this.setExactZeroes    = setExactZeroes;
        this.recursiveDepth    = recursiveDepth;
        
        this.dim            = currentU.getRowDimension();
        this.hasEvalMonitor = this.evaluationMonitor != null;
        
        this.smlnum = Numerics.getMatrixSmlNum(currentU);
        
        this.currentU = currentU;
        this.currentQ = this.schurMode == SchurMode.EIGENVALUES ? null : ComplexMatrix.identity(this.dim);
        
        this.numShiftsPerBulge = QRShifts.computeNumShiftsPerBulge(this.dim);
        this.diagBulgeDim      = this.numShiftsPerBulge + 2;
    }
    
    /** If this {@link #hasEvalMonitor}, update {@link #evaluationMonitor} with {@link #currentU} */
    private void possiblyUpdateEvaluationMonitor() {
        if (this.hasEvalMonitor) {
            this.evaluationMonitor.accept(this.currentU);
        }
    }
    
    /** If {@link #schurMode schurMode == EIGENVALUES}, then we don't need to accumulate the unitary transformations in
     * {@link #currentQ}. So, if {@link #schurMode schurMode != EIGENVALUES}, use
     * {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} to post-multiply in-place
     * {@link #currentQ} by {@code smallM} starting at given diagonal element.
     * 
     * @param smallM the small {@link ComplexMatrix}. Must be smaller than or, at most, the same size as
     *        {@link #currentQ}
     * @param diagIndex the diagonal index from which we want to the self-multiply */
    void possiblyPostMultiplyQ(final ComplexMatrix smallM, final int diagIndex) {
        if (this.schurMode != SchurMode.EIGENVALUES) {
            // modifies Q data!
            ComplexMatrix.postMultiplyInPlace(this.currentQ, smallM, diagIndex, diagIndex); // right-update for Q
            // modifies Q data!
        }
    }
    
    /** If {@link #schurMode schurMode == EIGENVALUES}, then we don't need to accumulate the unitary transformations in
     * {@link #currentQ}. So, if {@link #schurMode schurMode != EIGENVALUES}, use
     * {@link ComplexHouseholder#postApplyInPlace} apply the transformation to {@link #currentQ}.
     * 
     * @param householder the small {@link ComplexMatrix}. Must be smaller than or, at most, the same size as
     *        {@link #currentQ}
     * @param diagIndex the diagonal index from which we want to the self-multiply */
    void possiblyPostApplyHouseholderToQ(final ComplexHouseholder householder, final int diagIndex) {
        if (this.schurMode != SchurMode.EIGENVALUES) {
            // modifies Q data!
            householder.postApplyInPlace(this.currentQ, 0, diagIndex);
            // modifies Q data!
        }
    }
    
    /** Reduce {@link #currentU} to upper-Hessenberg using a full {@link ComplexHessenbergReduction}, and possibly
     * accumulate the unitary transformations used in {@link #currentQ}.<br>
     * <br>
     * 
     * This is a very expensive operation, so it should only be used when we know in advance that {@link #currentU} is
     * <em>not</em> in some other more convenient form, like bulge-Hessenberg. */
    void doHessenbergReduction() {
        // currentU and currentQ updated in-place!
        ComplexHessenbergReduction.doInPlace(this.currentQ, this.currentU, this.isHermitian, 1, this.evaluationMonitor);
        // currentU and currentQ updated in-place!
        
        if (this.setExactZeroes) {
            if (this.isHermitian) {
                this.setSubSupHessenbergEntriesToZero(false);
            }
            else {
                this.setSubHessenbergEntriesToZero(false);
            }
        }
        
        this.possiblyUpdateEvaluationMonitor();
    }
    
    /** Set (some or all) entries below the first sub-diagonal to exactly {@code 0 + 0i}.<br>
     * <br>
     * 
     * See the class-level JavaDoc of {@link ComplexSchurDecomposition} for a description of the bulge-chase scheme.
     * 
     * @param justBulgeChaseEntries {@code true} to only set the values disturbed by the bulge chase, {@code false} to
     *        set all sub-Hessenberg entries. */
    void setSubHessenbergEntriesToZero(final boolean justBulgeChaseEntries) {
        if (justBulgeChaseEntries) {
            for (int i = 2; i < this.diagBulgeDim; i++) {
                for (int j = 0; j < this.dim; j++) {
                    final int rowIndex = i + j;
                    if (rowIndex >= this.dim) {
                        break;
                    }
                    this.currentU.unsafeSetEntry(rowIndex, j, 0, 0);
                }
            }
        }
        else {
            for (int j = 0; j < this.dim; j++) {
                for (int i = j + 2; i < this.dim; i++) {
                    this.currentU.unsafeSetEntry(i, j, 0, 0);
                }
            }
        }
    }
    
    /** Set (some or all) entries below the first sub-diagonal and above the first super-diagonal to exactly
     * {@code 0 + 0i}.<br>
     * <br>
     * 
     * See the class-level JavaDoc of {@link ComplexSchurDecomposition} for a description of the bulge-chase scheme.
     * 
     * @param justBulgeChaseEntries {@code true} to only set the values disturbed by the bulge chase, {@code false} to
     *        set all sub-Hessenberg and super-Hessenberg entries. */
    void setSubSupHessenbergEntriesToZero(final boolean justBulgeChaseEntries) {
        if (justBulgeChaseEntries) {
            for (int i = 2; i < this.diagBulgeDim; i++) {
                for (int j = 0; j < this.dim; j++) {
                    final int rowIndex = i + j;
                    if (rowIndex >= this.dim) {
                        break;
                    }
                    this.currentU.unsafeSetEntry(rowIndex, j, 0, 0);
                    this.currentU.unsafeSetEntry(j, rowIndex, 0, 0);
                }
            }
        }
        else {
            for (int j = 0; j < this.dim; j++) {
                for (int i = j + 2; i < this.dim; i++) {
                    this.currentU.unsafeSetEntry(i, j, 0, 0);
                    this.currentU.unsafeSetEntry(j, i, 0, 0);
                }
            }
        }
    }
    
    /** Update a block of {@link #currentU} to the given {@link ComplexMatrix}
     * 
     * @param block the block to set
     * @param rowStart the row from which the block should start
     * @param colStart the column from which the block should start
     * 
     * @apiNote Be careful with this! Wholesale updating a sub-matrix properly is non-trivial and, indeed, there are
     *          better ways to accomplish the same thing; e.g., see
     *          {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} and
     *          {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}<br>
     *          <br>
     * 
     *          As of writing, this is only used when we know that we no longer need to maintain certain elements of
     *          {@link #currentU}. */
    void updateUBlock(final ComplexMatrix block, final int rowStart, final int colStart) {
        final double[][][] blockData = block.getDataRef();
        this.currentU.setSubMatrix(blockData[0], blockData[1], rowStart, colStart);
        this.possiblyUpdateEvaluationMonitor();
    }
    
    /** Compute and apply a shift-polynomial {@link ComplexHouseholder} {@link ComplexMatrix} matrix to the upper-left
     * corner of {@link #currentQ} (if necessary) and {@link #currentU}.
     * 
     * @see QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector) */
    void applyShiftBulge() {
        final ComplexVector x = QRShifts.computeFirstColumnOfShiftPolynomial(this.currentU,
                                                                             this.isHermitian,
                                                                             this.numShiftsPerBulge,
                                                                             this.recursiveDepth);
        final double[][]         xReImArr    = x.getDataRef();
        final ComplexHouseholder householder = ComplexHouseholder.of(xReImArr[0], xReImArr[1]);
        this.applyShiftBulge(householder);
    }
    
    /** Apply a {@link ComplexMatrix complex} {@link ComplexHouseholder} matrix - to the upper-left-corner of
     * {@link #currentU} (from both sides) and from the left to {@link #currentQ} - to introduce a shift bulge. This
     * method only exists separate from {@link #applyShiftBulge()} for easier testing.
     * 
     * @param householder the {@link ComplexMatrix complex} {@link ComplexHouseholder} matrix */
    void applyShiftBulge(final ComplexHouseholder householder) {
        // modifies Q data!
        this.possiblyPostApplyHouseholderToQ(householder, 0); // right-update for Q
        // modifies Q data!
        
        // modifies U data!
        householder.preApplyInPlace(this.currentU, 0, 0);
        householder.postApplyInPlace(this.currentU, 0, 0);
        // modifies U data!
        
        
        this.possiblyUpdateEvaluationMonitor();
    }
    
    /** Chase a shift bulge from top to bottom (and off) of {@link #currentU} */
    void chaseShiftBulge() {
        this.chaseShiftBulge(0, this.dim - 2);
        
        if (this.setExactZeroes) {
            if (this.isHermitian) {
                this.setSubSupHessenbergEntriesToZero(true);
            }
            else {
                this.setSubHessenbergEntriesToZero(true);
            }
            this.possiblyUpdateEvaluationMonitor();
        }
    }
    
    /** Once a shift-bulge has been applied to {@link #currentU}, chase the bulge from and to the given indices. This
     * method - the version that allows us to specify a range - exists because it's necessary for the multi-bulge QR
     * sweep. It was implemented, but I (RM) removed it due to lack-luster performance (see TODOs in
     * {@link ComplexSchurDecomposition}).<br>
     * <br>
     * 
     * It's also nice to have for testing.<br>
     * <br>
     * 
     * See the class-level JavaDoc of {@link ComplexSchurDecomposition} for a description of the bulge-chase scheme.
     * 
     * @param diagBulgeStart the start position of the bulge when measured from the <em>main</em> diagonal. This
     *        requires that you know the current bulge location.<br>
     *        <br>
     * 
     *        In the references in {@link ComplexSchurDecomposition}, the bulge is indexed in two ways: measured from
     *        the first sub-diagonal, and measured from the main diagonal itself. What makes the most sense is context
     *        dependent. Here, we're measuring the bulge from the main diagonal.
     * @param numIndicesToPush the number of indices we want to push chase the bulge */
    void chaseShiftBulge(final int diagBulgeStart, final int numIndicesToPush) {
        int subDiagBulgeDim = 2 * this.numShiftsPerBulge - 1;
        for (int column = diagBulgeStart; column < diagBulgeStart + numIndicesToPush; column++) {
            final int exclIndexOfLastColumnValue = column + 1 + subDiagBulgeDim;
            if (exclIndexOfLastColumnValue > this.dim) {
                subDiagBulgeDim -= (exclIndexOfLastColumnValue % this.dim);
            }
            
            this.chaseFromColumn(column);
        }
    }
    
    /** Assuming that the bulge in {@link #currentU} starts on the given column, this method will chase it to the right
     * and down one.
     * 
     * @param column the column where the bugle currently starts */
    void chaseFromColumn(final int column) {
        final int diagIndex     = column + 1;
        final int numEltsFomCol = FastMath.min(this.dim - diagIndex, this.diagBulgeDim - 1);
        
        final double[][]         firstBulgeColumnD = this.currentU.getDoubleSubColumn(diagIndex, column, numEltsFomCol);
        final ComplexHouseholder householder       = ComplexHouseholder.of(firstBulgeColumnD[0],
                                                                           firstBulgeColumnD[1]);
        
        // modifies Q data!
        this.possiblyPostApplyHouseholderToQ(householder, diagIndex); // right-update for Q
        // modifies Q data!
        
        final int rowStart = this.isHermitian ? column : 0;
        
        // modifies U data!  L+R updates for U
        householder.preApplyInPlace(this.currentU, diagIndex, column);
        householder.postApplyInPlace(this.currentU, rowStart, diagIndex);
        // modifies U data!
        
        this.possiblyUpdateEvaluationMonitor();
    }
}

