/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import ptolemaeus.math.linear.complex.ComplexMatrix;

/** {@link SchurMode} determines to what extent want to compute the complete {@link ComplexSchurDecomposition}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see ComplexSchurDecomposition
 * @see SchurHelper
 * @see QRShifts
 * @see QRDeflations */
public enum SchurMode {
                       
    /** Use {@link #EIGENVALUES} to only compute the Eigenvalues of the given {@link ComplexMatrix}. This allows us to
     * forgo accumulating the unitary transformation matrix {@link ComplexSchurDecomposition#getQ()} */
    EIGENVALUES,
    
    /** {@link #BLOCK_SCHUR_FORM} tells {@link ComplexSchurDecomposition} that we don't want to reduce the input
     * {@link ComplexMatrix} all the way to upper-triangular but, rather, block triangular, where the main diagonal of
     * {@link ComplexSchurDecomposition#getU()} is a series of {@code 1 x 1} and {@code 2 x 2} matrices, where the
     * {@code 1 x 1} blocks are Eigenvalues and the {@code 2 x 2} blocks represent complex conjugate Eigenvalues.<br>
     * <br>
     * 
     * E.g.:
     * 
     * <pre>
     * . . . . . . . . . . . . . . . .
     *   . . . . . . . . . . . . . . .
     *     . . . . . . . . . . . . . .
     *     . . . . . . . . . . . . . .
     *         . . . . . . . . . . . .
     *           . . . . . . . . . . .
     *             . . . . . . . . . .
     *               . . . . . . . . .
     *               . . . . . . . . .
     *                   . . . . . . .
     *                   . . . . . . .
     *                       . . . . .
     *                         . . . .
     *                           . . .
     *                           . . .
     *                               .
     * </pre>
     * 
     * For real-valued input matrices, this form is called the "real Schur form",
     * {@link ComplexSchurDecomposition#getQ()} and {@link ComplexSchurDecomposition#getU()} will also be
     * real-valued. */
    BLOCK_SCHUR_FORM,
    
    /** {@link #SCHUR_FORM} tells {@link ComplexSchurDecomposition} to compute the full complex triangularization of the
     * input matrix */
    SCHUR_FORM
}
