/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.linear.complex.ComplexVector;

/** The {@link EigenvectorFailureMode}s determine how we handle cases in
 * {@link ComplexSchurDecomposition#computeEigenvectors} when we fail
 * to adequately compute the Eigenvectors.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public enum EigenvectorFailureMode {
    
    /** on failure, throw an exception */
    THROW {
        @Override
        public ComplexVector handleFailure(final ComplexVector failedEigenvector) {
            throw new NumericalMethodsException("Failed to adequately compute Eigenvector");
        }
    },
    
    /** on failure, return the zero-{@link ComplexVector}' */
    ZERO {
        @Override
        public ComplexVector handleFailure(final ComplexVector failedEigenvector) {
            return ComplexVector.zero(failedEigenvector.getDimension());
        }
    },
    
    /** on failure, return the best we could do */
    AS_IS {
        @Override
        public ComplexVector handleFailure(final ComplexVector failedEigenvector) {
            return failedEigenvector;
        }
    };
    
    /** Handle the result of trying and failing to compute an {@link ComplexVector Eigenvector}
     * 
     * @param failedEigenvector the best we could do at computing an Eigenvector
     * @return the handled result */
    public abstract ComplexVector handleFailure(ComplexVector failedEigenvector);
}
