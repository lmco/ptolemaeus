/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;

import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;

/** {@code static} utility methods for computing the shifts in the implicit M-shifted QR algorithm used in
 * {@link ComplexSchurDecomposition}. Everything here - the class included - is {@code package-private} so that I can
 * avoid input checking; i.e., it's only ever expected to be used with {@link ComplexSchurDecomposition}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see ComplexSchurDecomposition
 * @see SchurHelper
 * @see QRDeflations
 * @see SchurMode */
final class QRShifts {
    
    /** The constant {@code 2} saved as a {@link Complex} */
    private static final Complex TWO = Complex.valueOf(2.0);
    
    /** The constant {@code 4} saved as a {@link Complex} */
    private static final Complex FOUR = Complex.valueOf(4.0);
    
    /** do nothing */
    private QRShifts() { /* do nothing */ }
    
    /** Compute the number of shifts to use given the dimension of a system.<br>
     * <Br>
     * 
     * If {@code dim < 100}, we return the largest even number smaller-than-or-equal-to {@code sqrt(dim)}. If
     * {@code dim > 100}, we return {@code 10}.<br>
     * <br>
     * 
     * We limit this to {@code 10} because, as noted in [2] and [3] in {@link ComplexSchurDecomposition} (and others),
     * using too many shifts severely impacts the convergence rate due to so-called "shift blurring". {@code 10} is what
     * is mentioned as a reasonable upper-bound.<br>
     * <br>
     * 
     * Also, we want to always return an even number of shifts because they often come in complex conjugate pairs, and
     * this leads to a more efficient iteration. This is also true in the case of complex matrices which can have
     * non-paired complex values Eigenvalues, but even then 1x1 matrices will deflate by themselves, allowing us to make
     * efficient use of conjugate pairs once once they do.<br>
     * <br>
     * 
     * For real matrices, strictly using conjugate pairs actually ensures that we stay totally in real arithmetic. We
     * don't have that limitation here, but does work out that way and is how purely real-valued implementations of this
     * QR shift algorithm work.
     * 
     * @param dim the dimension of the candidate matrix
     * @return the number of shifts to use in the implicit M-shifted QR algorithm */
    static int computeNumShiftsPerBulge(final int dim) {
        if (dim < 16) {
            return 2; // this results in the "implicit double-shift" QR step
        }
        if (dim < 36) {
            return 4;
        }
        if (dim < 64) {
            return 6;
        }
        if (dim < 100) {
            return 8;
        }
        
        return 10;
    }
    
    /** This method efficiently computes and returns the first column of the shift polynomial<br>
     * <br>
     * 
     * {@code P(A) = (A - }&lambda;<sub>m</sub>{@code I) .
     * (A - }&lambda;<sub>m-1</sub>{@code I) .
     * (A - }&lambda;<sub>m-2</sub>{@code I)
     * ...
     * (A - }&lambda;<sub>2</sub>{@code I) .
     * (A - }&lambda;<sub>1</sub>{@code I)}<br>
     * <br>
     * 
     * for some <b>upper-Hessenberg</b> matrix {@code A}, and the {@code m} Eigenvalues &lambda;<sub>i</sub> of the
     * trailing {@code m x m} sub-matrix of the {@code n x n} {@code A}.<br>
     * <br>
     * 
     * We are able to compute the first column of the shift polynomial, as we can make use of the Hessenberg structure
     * of {@code A} to skip unnecessary computations.<br>
     * <br>
     * 
     * See {@link #computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)} for details.
     * 
     * @param H the {@code n x n} {@link ComplexMatrix} for which we want to evaluate the shift polynomial. {@code A} is
     *        assumed to be upper-Hessenberg.
     * @param isHermitian {@code true} if the overall system {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) is
     *        Hermitian}, false otherwise. We can pass this value through rather than compute it because any diagonal
     *        sub-matrix of a Hermitian matrix is also Hermitian.
     * @param m the dimension of the trailing square sub-matrix of {@code A} for which we will compute {@code m}
     *        Eigenvalues to apply as shifts. Note: if {@code m > 2}, we will recursively use
     *        {@link ComplexSchurDecomposition} to compute these Eigenvalues. There must be fewer shifts than the
     *        dimension of {@code A}.
     * @param recursiveDepth the current depth of recursive calls to
     *        {@link ComplexSchurDecomposition#solve}. See {@link SchurHelper#recursiveDepth} for
     *        details.
     * @return the first column of the shift polynomial as described above */
    static ComplexVector computeFirstColumnOfShiftPolynomial(final ComplexMatrix H,
                                                             final boolean isHermitian,
                                                             final int m,
                                                             final int recursiveDepth) {
        final ComplexVector shifts = computeShifts(H, isHermitian, m, recursiveDepth);
        return computeFirstColumnOfShiftPolynomial(H, shifts);
    }
    
    /** This method efficiently computes and returns the first column of the shift polynomial<br>
     * <br>
     * 
     * {@code P(H) = (H - }&lambda;<sub>m</sub>{@code I) .
     * (H - }&lambda;<sub>m-1</sub>{@code I) .
     * (H - }&lambda;<sub>m-2</sub>{@code I)
     * ...
     * (H - }&lambda;<sub>2</sub>{@code I) .
     * (H - }&lambda;<sub>1</sub>{@code I)}<br>
     * <br>
     * 
     * for the {@code m} shifts &lambda;<sub>i</sub> provided.<br>
     * <br>
     * 
     * We are able to compute the first column of the shift polynomial without computing the full product by making use
     * of the upper-Hessenberg structure of {@code H} to skip unnecessary computations.<br>
     * <br>
     * 
     * E.g.,
     * 
     * <pre>
     * 
     * Let A = 
     * 
     *     h00 h01 h02 h03 h04 h05 h06 h07 h08 h09
     *     h10 h11 h12 h13 h14 h15 h16 h17 h18 h19
     *         h21 h22 h23 h24 h25 h26 h27 h28 h29
     *             h32 h33 h34 h35 h36 h37 h38 h39
     *                 h43 h44 h45 h46 h47 h48 h49
     *                     h54 h55 h56 h57 h58 h59
     *                         h65 h66 h67 h68 h69
     *                             h76 h77 h78 h79
     *                                 h87 h88 h89
     *                                     h98 h99
     * 
     * which with Eigenvalues (y9, y8, ..., y0)
     * 
     * Then, given e1 = (1, 0, ..., 0) for R^dim(A) == R^10 define
     * 
     *       / (H - y0.I).e1    , i = 0
     * p_i = | 
     *       \ (H - yi.I).p_i-1 , i > 0
     * 
     * 
     * We can compute the first vector of p_10 - implicitly ignoring values we know in advance
     * to be zero - by evaluating from right to left:
     * 
     * p_0 = (H - y0.I).e1 = (h00 - y0 + 0 + 0...,       (p_00,
     *                        h10      + 0 + 0...,   =    p_01,
     *                        0,                          0,
     *                        ...)                        ...)
     * 
     * p_1 = (H - y1.I).p_0 = ((h00 - y1).p_00 + h01.p_01        + 0 + 0...,       (p_10,
     *                                h10.p_00 + (h11 - y1).p_01 + 0 + 0...,   =    p_11,
     *                                                  h21.p_01 + 0 + 0...,        p_12,   
     *                         0,                                                   0, 
     *                         ...)                                                 ...)
     * 
     * p_2 = (H - y2.I).p_1 = ((h00 - y2).p_10 + h01.p_11        + h02.p_12        + 0 + 0...,
     *                                h10.p_10 + (h11 - y2).p_11 + h12.p_12        + 0 + 0...,
     *                                                  h21.p_11 + (h22 - y2).p_12 + 0 + 0...,
     *                         0,
     *                         ...)
     * 
     * and this trend of only needing to consider one additional row continues until we run out of shift values to apply
     * </pre>
     * 
     * Of course, rather than attempting to simplify this expression into a closed form for a given {@code i}, we
     * instead cleverly make use of matrix multiplication, only adding a row when necessary (which is necessarily
     * equivalent, any <em>may</em> be equivalent in terms of FLOPs).<br>
     * <br>
     * 
     * @param H the {@code n x n} {@link ComplexMatrix} <em>in upper-Hessenberg form</em> for which we want to evaluate
     *        the shift polynomial. {@code H} <em>must</em> be upper-Hessenberg for this to work!
     * @param shifts the shifts to use in the polynomial. We will iterate over these in reverse. There must be fewer
     *        shifts than two less than the dimension of {@code A}, but we don't manually enforce this. Instead, an
     *        exception will be thrown due to dimension mismatch when calling
     *        {@link ComplexMatrix#getSubMatrix(int, int, int, int)}.
     * 
     * @return the first column of the shift polynomial as described above */
    static ComplexVector computeFirstColumnOfShiftPolynomial(final ComplexMatrix H, final ComplexVector shifts) {
        final int numShifts = shifts.getDimension();
        
        ComplexVector p = new ComplexVector(new double[] { 1.0 }, new double[1], false);
        for (int i = 1; i <= numShifts; i++) {
            final Complex shift = shifts.getEntry(numShifts - i);
            final double shiftRe = shift.getReal();
            final double shiftIm = shift.getImaginary();
            
            final ComplexMatrix AMinusLambdaI = H.getSubMatrix(0, i, 0, i - 1);
            /* As demonstrated in the JavaDoc above, we only need to consider rectangular sub-matrix of H - starting at
             * 2x1 and increasing by one in each dimension each iteration - this automatically results in us adding a
             * row to the resulting vector response, so there's no need to do any special accounting aside from manually
             * excluding the last entry which is always zero (it's identically zero if H is perfectly
             * upper-Hessenberg). */
            
            for (int diagIndex = 0; diagIndex < AMinusLambdaI.getColumnDimension(); diagIndex++) {
                AMinusLambdaI.subtractFromEntry(diagIndex, diagIndex, shiftRe, shiftIm);
            }
            
            p = AMinusLambdaI.operate(p);
        }
        
        return p.getSubVector(0, numShifts + 1);
    }
    
    /** Compute and return the {@code m} Eigenvalues of the trailing principal {@code m x m} sub-matrix of {@code A}.
     * 
     * @param H the {@code n x n} {@link ComplexMatrix} <em>in upper-Hessenberg form</em> for which we want to evaluate
     *        the shift polynomial. {@code H} <em>must</em> be upper-Hessenberg for this to work!
     * @param isHermitian {@code true} if the overall system {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) is
     *        Hermitian}, false otherwise. We can pass this value through rather than compute it because any diagonal
     *        sub-matrix of a Hermitian matrix is also Hermitian.
     * @param m dimension of the trailing square sub-matrix of {@code A} for which we will compute Eigenvalues to apply
     *        as shifts
     * @param recursiveDepth the current depth of recursive calls to
     *        {@link ComplexSchurDecomposition#solve}. See {@link SchurHelper#recursiveDepth} for
     *        details.
     * @return the {@code m = max(2, mRequested)} Eigenvalues of the trailing principal {@code m x m} sub-matrix of
     *         {@code A}
     * @apiNote we assume this is never called if {@code n < 3}, because that should have been handled by
     *          {@link ComplexSchurDecomposition#solve} */
    static ComplexVector computeShifts(final ComplexMatrix H,
                                       final boolean isHermitian,
                                       final int m,
                                       final int recursiveDepth) {
        final int n = H.getColumnDimension();
        
        if (m <= 2) {
            return computeEigenTrailingPrincipal2x2(H, isHermitian);
        }
        
        final int from = n - m; // regarding index offsets: n - 1 - (m - 1) = n - 1 - m + 1 = n - m
        final int to   = n - 1;
        
        final ComplexMatrix lowerRight      = H.getSubMatrix(from, to, from, to);
        final int           subMaxIters     = ComplexSchurDecomposition.chooseMaxIters(lowerRight);
        final SchurHelper   lowerRightSchur = new SchurHelper(lowerRight,
                                                              isHermitian,
                                                              SchurMode.EIGENVALUES,
                                                              subMaxIters,
                                                              null,
                                                              ComplexSchurDecomposition.SET_EXACT_ZEROES,
                                                              recursiveDepth + 1);
        
        /* construct a SchurHelper instance directly to circumvent some work that we know to be unnecessary at this
         * point */
        final ComplexSchurDecomposition subSchur = ComplexSchurDecomposition.solve(H, lowerRightSchur, true);
        return subSchur.getEigenvalues();
    }
    
    /** Compute the Eigenvalues of the trailing, principal {@code 2 x 2} sub-matrix of {@code M}; i.e., the
     * lower-right-most {@code 2 x 2} diagonal block of {@code M}<br>
     * <br>
     * 
     * TODO Ryan Moser, 2024-08-07: attempt to generalize the following to {@code 2x2} {@link ComplexMatrix complex
     * matrices}:<br>
     * <br>
     * 
     * <i>Accurate computation of Eigenvalues and real Schur form of 2x2 real matrices</i><br>
     * Proceedings of the Second NICONET Workshop on "Numerical Control Software: SLICOT, a Useful Tool in Industry"
     * 
     * @param M the {@link ComplexMatrix} for whose {@code 2 x 2} sub-matrix we want the Eigenvalues
     * @param isHermitian {@code true} if the overall system {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) is
     *        Hermitian}, false otherwise. We can pass this value through rather than compute it because any diagonal
     *        sub-matrix of a Hermitian matrix is also Hermitian.
     * @return the Eigenvalues of the trailing, principal {@code 2 x 2} sub-matrix of {@code M} */
    static ComplexVector computeEigenTrailingPrincipal2x2(final ComplexMatrix M, final boolean isHermitian) {
        final int lastIndex = M.getRowDimension() - 1;
        
        final Complex a = M.getEntry(lastIndex - 1, lastIndex - 1);
        final Complex b = M.getEntry(lastIndex - 1, lastIndex);
        final Complex c = M.getEntry(lastIndex,     lastIndex - 1);
        final Complex d = M.getEntry(lastIndex,     lastIndex);
        
        final Complex a2     = a.multiply(a);
        final Complex twoad  = TWO.multiply(a).multiply(d);
        final Complex fourbc = FOUR.multiply(b).multiply(c);
        final Complex d2     = d.multiply(d);
        final Complex aPlusd = a.add(d);
        
        final Complex sqrt    = FastMath.sqrt(a2.subtract(twoad).add(fourbc).add(d2));
        final Complex lambda1 = (sqrt.add(aPlusd)).multiply(0.5);            // (+sqrt + a + d) / 2
        final Complex lambda2 = ((sqrt.negate()).add(aPlusd)).multiply(0.5); // (-sqrt + a + d) / 2
        
        if (isHermitian) {
            return new ComplexVector(new double[] { lambda1.getReal(), lambda2.getReal() },
                                     new double[2],
                                     false);
        }
        
        return new ComplexVector(new Complex[] { lambda1, lambda2 });
    }
}
