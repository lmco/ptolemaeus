/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import static ptolemaeus.math.commons.Numerics.complex1Norm;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;

/** {@code static} utilities for computing and detecting allowable deflations in the QR algorithm used by
 * {@link ComplexSchurDecomposition}.
 * 
 * <br>
 * <br>
 * 
 * Deflations:
 * 
 * <br>
 * <br>
 * 
 * In {@link ComplexSchurDecomposition} we always start with an initial conversion into upper-Hessenberg form (if not
 * already done), which looks like this (empty entries are zeroes):
 * 
 * <pre>
 *     . . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . . . .
 *       . . . . . . . . . . . . . . .
 *         . . . . . . . . . . . . . .
 *           . . . . . . . . . . . . .
 *             . . . . . . . . . . . .
 *               . . . . . . . . . . .
 *                 . . . . . . . . . .
 *                   . . . . . . . . .
 *                     . . . . . . . .
 *                       . . . . . . .
 *                         . . . . . .
 *                           . . . . .
 *                             . . . .
 *                               . . .
 *                                 . .
 * </pre>
 * 
 * Suppose that, as we iterate, we a few of entries of the first sub-diagonal - those with indices of the form (i, i +
 * 1) - are "zeroed-out", like so:
 * 
 * <pre>
 *     . . . . . . . . . . . . . . . .
 *     . . . . . . . . . . . . . . . .
 *         . . . . . . . . . . . . . .
 *         . . . . . . . . . . . . . .
 *           . . . . . . . . . . . . .
 *             . . . . . . . . . . . .
 *               . . . . . . . . . . .
 *                   . . . . . . . . .
 *                   . . . . . . . . .
 *                     . . . . . . . .
 *                       . . . . . . .
 *                         . . . . . .
 *                             . . . .
 *                             . . . .
 *                               . . .
 *                                 . .
 * </pre>
 * 
 * This can be "deflated" into several sub-Eigenvalue problems ({@link ComplexSchurDecomposition Schur decompositions},
 * in this case) which can be solved independently, and are already in upper-Hessenberg form:
 * 
 * <pre>
 *     . . 
 *     . . 
 *         . . . . . 
 *         . . . . . 
 *           . . . . 
 *             . . . 
 *               . . 
 *                   . . . . . 
 *                   . . . . . 
 *                     . . . . 
 *                       . . . 
 *                         . . 
 *                             . . . .
 *                             . . . .
 *                               . . .
 *                                 . .
 * </pre>
 * 
 * The iterative portion of the algorithm stops once we've decomposed the original matrix into a block-diagonal matrix
 * whose block entries are all 2x2 and 1x1.<br>
 * <br>
 * 
 * Lastly, while it is possible that we can find more than one deflation at a time, we will almost always only find one;
 * thus, once we see the opportunity for a deflation, we simply return the two sub-problems that can be solved
 * independently.<br>
 * <br>
 * 
 * [1] <a href="https://www.netlib.org/lapack/lawnspdf/lawn122.pdf">A New Deflation Criterion for the QR Algorithm</a>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see ComplexSchurDecomposition
 * @see SchurHelper
 * @see QRShifts
 * @see SchurMode */
final class QRDeflations {
    
    /** Machine epsilon, saved as a constant here for shorter lines */
    private static final double ULP = Numerics.MACHINE_EPSILON;
    
    /** do nothing */
    private QRDeflations() { /* do nothing */ }
    
    /** Compute deflations of {@code M} according to the deflation criterion described in [1]. See eqs. 1.1 and 3.1 and
     * the paragraph just after eq. 3.1, as used by LAPACK's ZLAHQR.
     * 
     * @param H the {@link ComplexMatrix} for which we want to compute deflations. <b>Must be upper-Hessenberg!</b>
     * @param m the number of shifts that we're considering in this implicit M-shifted QR iteration
     * @param smlnum the value of {@link SchurHelper#smlnum}
     * @return a {@link ComplexMatrix}{@code []} of the deflations of {@code M}, or {@code null} if unable to deflate */
    static ComplexMatrix[] computeDeflations(final ComplexMatrix H,
                                             final int m,
                                             final double smlnum) {
        final int n                   = H.getColumnDimension();
        final int mthReverseDiagIndex = FastMath.max(0, n - m - 2); // n - (m + 1) - 1 = n - m - 2
        final int lastIndexExcl       = n - 1; /* we can only search up to the (n - 1)^th diagonal
                                                * because we check the entries of the first
                                                * sub-diagonal */
        
        final ComplexMatrix[] deflationsFromM = computeDeflations(H,
                                                                  smlnum,
                                                                  mthReverseDiagIndex,
                                                                  lastIndexExcl);
        if (deflationsFromM != null) {
            return deflationsFromM;
        }
        
        return computeDeflations(H, smlnum, 0, mthReverseDiagIndex - 1);
    }
    
    /** The same as {@link #computeDeflations(ComplexMatrix, int, double)}, but we only check within the given range of
     * indices
     * 
     * @param H the {@link ComplexMatrix} for which we want to compute deflations. <b>Must be upper-Hessenberg!</b>
     * @param smlnum the value of {@link SchurHelper#smlnum}
     * @param fromIndex the index where we should start the search, inclusive
     * @param toExcl the index where we should end the search, exclusive
     * @return a {@link ComplexMatrix}{@code []} of the deflations of {@code M}, or {@code null} if unable to deflate */
    static ComplexMatrix[] computeDeflations(final ComplexMatrix H,
                                             final double smlnum,
                                             final int fromIndex,
                                             final int toExcl) {
        final int dim = H.getColumnDimension();
        
        for (int i = fromIndex; i < toExcl; i++) {
            final boolean canDeflate = canDeflate(H, i, smlnum);
            if (canDeflate) { // deflation detected
                final ComplexMatrix deflationAbove = H.getSubMatrix(0,     i,       0,     i);
                final ComplexMatrix deflationBelow = H.getSubMatrix(i + 1, dim - 1, i + 1, dim - 1);
                
                return new ComplexMatrix[] { deflationAbove, deflationBelow };
            }
        }
        
        return null;
    }
    
    /** Determine whether we can deflate the given {@link ComplexMatrix} at the given diagonal index according to [1],
     * eqs. 1.1 and 3.1., as in LAPACK's <a href="https://github.com/Reference-LAPACK/lapack/blob/v3.12.0/SRC/zlahqr.f">
     * ZLAHQR</a>:<br>
     * 
     * <pre>
     * 1.1:
     *   |h(i, i-1)| <= machEps * frobNorm(A)
     * 
     * 3.1:
     *   |h(i, i-1)||h(i-1, i)| < machEps * |h(i, i)||h(i, i) - h(i-1, i-1)|
     * </pre>
     * 
     * Note that the implementation here and in LAPACK is not exactly this, but it's similar. More specifically, we (and
     * LAPACK) are testing against {@code ULP * tst}, where {@code tst} is (usually) the sum of the 1-norms of elements
     * on the diagonal. If {@code tst == 0} initially, we add the norms of a few different elements that we can use to
     * determine deflatability. <br>
     * <br>
     * 
     * An illustration of the elements of the matrix we're using:
     * 
     * <pre>
     * 
     *  .
     *    .
     *      .
     *        .
     * h(i,i-1) h(i,   i) h(i,   i+1)
     *          h(i+1, i) h(i+1, i+1)
     *                    h(i+2, i+1)  .
     *                                   .
     *                                     .
     *                                       .
     *                                         .
     * </pre>
     * 
     * where .'s represents arbitrary diagonal elements.<br>
     * <br>
     * 
     * The FORTRAN from ZLAHQR that we're trying to mimic (lines 341 - 365 in v3.12.0) is below.<br>
     * Note: when comparing the FORTRAN to the Java:
     * <ul>
     * <li>the statement {@code GO TO 50} in the FORTRAN is equivalent to returning {@code true} in this impl.,</li>
     * <li>FORTRAN {@code ilo == 0},</li>
     * <li>FORTRAN {@code ihi = dim(H)},</li>
     * <li>On the left are the labels {@code A, B, C, D,} and {@code E} that I've added to make it easier to compare the
     * FORTRAN and Java. Each label has a corresponding label in the Java on the line preceding the equivalent check,
     * equation, etc.</li>
     * </ul>
     * <br>
     * 
     * <pre>
     * A |             IF( cabs1( h( k, k-1 ) ).LE.smlnum )
     *   |      $         GO TO 50
     * B |             tst = cabs1( h( k-1, k-1 ) ) + cabs1( h( k, k ) )
     *   |             IF( tst.EQ.zero ) THEN
     * C |                IF( k-2.GE.ilo )
     *   |      $            tst = tst + abs( dble( h( k-1, k-2 ) ) )
     * D |                IF( k+1.LE.ihi )
     *   |      $            tst = tst + abs( dble( h( k+1, k ) ) )
     *   |             END IF
     *   | *           ==== The following is a conservative small subdiagonal
     *   | *           .    deflation criterion due to Ahues & Tisseur (LAWN 122,
     *   | *           .    1997). It has better mathematical foundation and
     *   | *           .    improves accuracy in some examples.  ====
     * E |             IF( abs( dble( h( k, k-1 ) ) ).LE.ulp*tst ) THEN
     *   |                ab = max( cabs1( h( k, k-1 ) ), cabs1( h( k-1, k ) ) )
     *   |                ba = min( cabs1( h( k, k-1 ) ), cabs1( h( k-1, k ) ) )
     *   |                aa = max( cabs1( h( k, k ) ),
     *   |      $              cabs1( h( k-1, k-1 )-h( k, k ) ) )
     *   |                bb = min( cabs1( h( k, k ) ),
     *   |      $              cabs1( h( k-1, k-1 )-h( k, k ) ) )
     *   |                s = aa + ab
     *   |                IF( ba*( ab / s ).LE.max( smlnum,
     *   |      $             ulp*( bb*( aa / s ) ) ) )GO TO 50
     *   |             END IF
     * </pre>
     * 
     * @param H the {@link ComplexMatrix} to test
     * @param i the index of the diagonal element for which we want to examine the first sub-diagonal element
     * @param smlnum the value of {@link SchurHelper#smlnum}
     * @return {@code true} if we can deflate at this index, {@code false} otherwise
     * 
     * @implNote this is one of the few methods in this package that attempts to follow LAPACK so closely; this is due
     *           to the fact that they've worked out the edge cases, and missing deflation opportunities results in the
     *           algorithm failing to converge.
     * @implNote notice that LAPACK (and, thus, we) actually use the
     *           {@link Numerics#complex1Norm(Complex) 1-norm} of the
     *           complex numbers, rather than the usual {@link Complex#norm() 2-norm}. The reason for this is likely
     *           performance, as the {@code 1-norm} doesn't require the use of {@link FastMath#sqrt(double)}. */
    static boolean canDeflate(final ComplexMatrix H, final int i, final double smlnum) {
        final int k   = i + 1; // we define and use 'k' rather than 'i' to more closely follow the FORTRAN
        final int ilo = 0;
        final int ihi = H.getColumnDimension() - 1;
        
        final Complex subDiagElt     = H.getEntry(k, k - 1);
        final double  subDiagEltNorm = complex1Norm(subDiagElt);
        
        // A
        if (subDiagEltNorm <= smlnum) {
            return true;
        }
        
        final Complex prevDiagElt = H.getEntry(k - 1, k - 1);
        final Complex diagElt     = H.getEntry(k,     k);
        
        final double prevDiagEltNorm = complex1Norm(prevDiagElt);
        final double diagEltNorm     = complex1Norm(diagElt);
        
        // B
        double tst = prevDiagEltNorm + diagEltNorm;
        if (tst == 0.0) {
            // C
            if (k - 2 >= ilo) {
                tst = tst + FastMath.abs(H.getEntry(k - 1, k - 2).getReal());
            }
            // D
            if (k + 1 <= ihi) {
                tst = tst + FastMath.abs(H.getEntry(k + 1, k).getReal());
            }
        }
        
        /* @formatter:off
         * A comment from the FORTRAN code:
         *     ==== The following is a conservative small subdiagonal
         *     .    deflation criterion due to Ahues & Tisseur (LAWN 122,
         *     .    1997). It has better mathematical foundation and
         *     .    improves accuracy in some examples.  ====
         * @formatter:on */
        // E
        if (FastMath.abs(subDiagElt.getReal()) <= ULP * tst) {
            /* We define new variables just for ease of comparison; the compiler will elide these immediately. The names
             * identify the matrix indices corresponding to those elements; i.e. hkkm1 -> H(k, k - 1) or, in words,
             * "H sub k, k minus one". */
            final Complex hkm1km1 = prevDiagElt;
            final Complex hkkm1   = subDiagElt; // "H sub k, k minus one"
            final Complex hkm1k   = H.getEntry(k - 1, k);
            final Complex hkk     = diagElt;
            
            final double ab = FastMath.max(complex1Norm(hkkm1), complex1Norm(hkm1k));
            final double ba = FastMath.min(complex1Norm(hkkm1), complex1Norm(hkm1k));
            final double aa = FastMath.max(complex1Norm(hkk),   complex1Norm(hkm1km1.subtract(hkk)));
            final double bb = FastMath.min(complex1Norm(hkk),   complex1Norm(hkm1km1.subtract(hkk)));
            
            final double s = aa + ab;
            
            final double lhs = ba * (ab / s);
            final double rhs = FastMath.max(smlnum, ULP * (bb * (aa / s)));
            
            return lhs <= rhs;
        }
        
        return false;
    }
}
