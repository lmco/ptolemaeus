/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.real;

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.stream.DoubleStream;

import org.apache.commons.collections4.IterableUtils;

import org.hipparchus.linear.ArrayRealVector;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.MathArrays;

import ptolemaeus.commons.BeCareful;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.realdomains.IExtent;
import ptolemaeus.math.realdomains.NPoint;

/** An n-dimensional vector<br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-01-06: change {@link NVector} to simply {@code extends} {@link RealVector}<br>
 * TODO Ryan Moser, 2025-01-06: write an unmodifiable view for instances of this class
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class NVector extends ArrayRealVector implements IExtent {
    
    /** generated */
    private static final long serialVersionUID = -7581454715147691258L;
    
    /**
     * Constructor
     * 
     * @param point the point of the vector
     */
    public NVector(final NPoint point) {
        super(point.getComponents(), false); // false tells it not to clone the array: getComponents already clones
    }
    
    /**
     * Constructor that takes a double[]
     * 
     * @param components the components of the point of the {@link NVector}
     */
    @SafeVarargs
    public NVector(final double... components) {
        this(components, true); // true tells it to clone the array
    }
    
    /** Constructor that takes a double[] and optionally clone the data array
     * 
     * @param components the components of the point of the {@link NVector}
     * @param clone {@code true} to clone the array, {@code false} to simply copy the reference. <b>Be careful!</b> If
     *        the array is not cloned, external changes to the array will be reflected in this instance. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public NVector(final double[] components, final boolean clone) {
        super(components, clone);
    }
    
    /**
     * Constructor to convert from {@link RealVector} to {@link NVector}
     * 
     * @param vector the point of the vector
     */
    public NVector(final RealVector vector) {
        this(vector, true);
    }
    
    /** Constructor for efficiently creating an {@link NVector} from a {@link RealVector}
     * 
     * @param vector the {@link RealVector} we want to convert
     * @param clone {@code true} to clone the array, {@code false} to simply copy the reference. <b>Be careful!</b> If
     *        the array is not cloned, external changes to the array will be reflected in this instance.<br>
     *        <br>
     * 
     *        Additionally, if {@code vector} is not an instance of {@link ArrayRealVector}, the data array is always
     *        cloned, because we will use {@link RealVector#toArray()} to get the data, which always copies the internal
     *        data array. We avoid any inefficiency in double-copying the array by passing {@code clone == true} only if
     *        {@code vector instanceof ArrayRealVector && clone}.
     * 
     * @apiNote Users must be careful to not misuse the {@link RealVector} that was passed in, else changes to its data
     *          array will be reflected in this {@link Matrix} instance. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public NVector(final RealVector vector, final boolean clone) {
        super(vector instanceof ArrayRealVector arv ? arv.getDataRef() : vector.toArray(),
              vector instanceof ArrayRealVector && clone);
    }
    
    /** Get a {@link NVector} representation of the given {@link RealVector} {@code x}. If possible, this method avoids
     * cloning any arrays or creating any new instances. In particular:
     * <ol>
     * <li>if {@code x instanceof NVector}, we return {@code x} as-is</li>
     * <li>else if {@code x instanceof ArrayRealVector}, we return a new instance of {@link NVector} <em>without</em>
     * cloning the {@link ArrayRealVector#getDataRef() data reference}</li>
     * <li>else, we return a new {@link NVector} created using {@link RealVector#toArray()}. {@code toArray} returns a
     * copy, so we don't re-copy it.</li>
     * </ol>
     * 
     * @param v the {@link RealVector} for which we want a 
     * @return the {@link NVector} answer as described */
    public static NVector of(final RealVector v) {
        requireNonNull(v, "The vector v must not be null");
        if (v instanceof NVector x) {
            return x;
        }
        if (v instanceof ArrayRealVector x) {
            return new NVector(x.getDataRef(), false);
        }
        return new NVector(v.toArray(), false);
    }
    
    @Override
    public int getEmbeddedDimensionality() {
        return this.getDataRef().length;
    }
    
    @Override
    public int getDimensionality() {
        return 1;
    }
    
    @Override
    public NPoint getEndPoint1() {
        return NPoint.getZero(this.getEmbeddedDimensionality()); // getZero caches results
    }
    
    @Override
    public NPoint getEndPoint2() {
        return this.getPoint();
    }
    
    @Override
    public double getLength() {
        return this.getMagnitude();
    }
    
    /**
     * @return the {@link NPoint} at the point of this {@link NVector}
     */
    public NPoint getPoint() {
        return new NPoint(this.getDataRef()); // no .clone() because the NPoint constructor clones the array
    }
    
    @Override
    public double getNorm() {
        return this.getMagnitude();
    }
    
    /**
     * @return the magnitude of this {@link NVector}
     */
    public double getMagnitude() {
        final double magSqr = this.magnitudeSquared();
        if (magSqr == 0.0 || Double.isInfinite(magSqr)) { /* the normal (fast) calc. may have suffered from
                                                           * over-/under-flow, so lets try a more careful approach */
            return MathArrays.safeNorm(this.getComponents());
        }
        return FastMath.sqrt(magSqr);
    }
    
    /**
     * @return the square of the magnitude of this {@link NVector}
     */
    public double magnitudeSquared() {
        double sumSqr = 0.0;
        for (int i = 0; i < this.getDimension(); i++) {
            sumSqr += Numerics.square(this.getEntry(i));
        }
        return sumSqr;
    }
    
    /**
     * @return a unit {@link NVector} in the direction of this
     */
    public NVector normalized() {
        final double norm = this.getMagnitude();
        Validation.requireGreaterThan(norm, 0.0, "NVector magnitude");
        final double[] tipComponents = new double[this.getEmbeddedDimensionality()];
        this.embeddedDimensionalityIntStream()
            .forEach(i -> tipComponents[i] = this.getComponent(i) / norm);
        return new NVector(tipComponents);
    }
    
    /**
     * Compute the dot product of this with the provided {@link NVector}
     * 
     * @param other the other {@link NVector}
     * @return the dot product of this with the provided {@link NVector}
     */
    public double dot(final NVector other) {
        this.verifyCompatibleDimensionality(other);
        return this.embeddedDimensionalityIntStream()
                   .mapToDouble(i -> this.getComponent(i) * other.getComponent(i))
                   .sum();
    }
    
    /**
     * Add {@code this} and another {@link NVector}
     * 
     * @param other the other {@link NVector}
     * @return {@code this} plus another {@link NVector}
     */
    public NVector plus(final NVector other) {
        this.verifyCompatibleDimensionality(other);
        return new NVector(this.embeddedDimensionalityIntStream()
                               .mapToDouble(i -> this.getComponent(i) + other.getComponent(i))
                               .toArray());
    }
    
    /**
     * Subtract another {@link NVector} from this {@code this}
     * 
     * @param other the other {@link NVector}
     * @return {@code this} minus another {@link NVector}
     */
    public NVector minus(final NVector other) {
        this.verifyCompatibleDimensionality(other);
        return this.plus(other.times(-1.0));
    }
    
    /**
     * Multiply this {@link NVector} by a scalar
     * 
     * @param scalar the scalar by which to multiply this
     * @return this {@link NVector} by a scalar
     */
    public NVector times(final double scalar) {
        return new NVector(this.embeddedDimensionalityIntStream()
                               .mapToDouble(this::getComponent)
                               .map(d -> scalar * d)
                               .toArray());
    }
    
    /**
     * @return the signum {@link NVector} of {@code this}; i.e., the vector which has as each of its components -1, 0,
     *         or 1 given the signum of the value of the corresponding component in the original vector
     */
    public NVector signum() {
        final double[] signumComponents = this.embeddedDimensionalityIntStream()
                                              .mapToDouble(this::getComponent)
                                              .map(FastMath::signum)
                                              .toArray();
        return new NVector(signumComponents);
    }
    
    /**
     * @return get the components of this {@link NVector}'s {@link NPoint}
     */
    public double[] getComponents() {
        return this.toArray();
    }
    
    /**
     * @param position the position in this {@link NVector}'s {@link NPoint}'s components
     * @return the value at 'position' in this {@link NVector}'s {@link NPoint}'s components
     */
    public double getComponent(final int position) {
        return this.getDataRef()[position];
    }
    
    /** @return a string representation of this {@link NVector} as a column {@link Matrix} */
    public String toColumnString() {
        final double[][] rowMatArr = new double [][] { this.getDataRef() };
        return new Matrix(rowMatArr).transpose().toString();
    }
    
    @Override
    public String toString() {
        return toString("%s", ", ", "<", ">");
    }
    
    /** Create a string representation of this {@link NVector} using the given format string for its components
     * 
     * @param doubleFormatString the format string
     * @return the string representation 
     * 
     * @see #toString(String, String, String, String) */
    public String toString(final String doubleFormatString) {
        return this.toString(doubleFormatString, ", ", "<", ">");
    }
    
    /** Create a string representation of this {@link NVector} using the given format string, delimiter, prefix, and
     * suffix.<br>
     * <br>
     * 
     * E.g.: if this has components {@code 1, 2, 3, 4}, and @ {@code doubleFormatString == "%s"}, code delimiter == ",
     * "}, {@code prefix == "<"}, and {@code suffix == ">"}, the resulting string will be {@code <1.0, 2.0, 3.0, 4.0>}
     * 
     * @param doubleFormatString the format string to format {@code double}s as strings; e.g., "%.10f", "%s", etc.
     * @param delimiter the string to delimit elements
     * @param prefix the prefix, prepended to the string representation
     * @param suffix the suffix, appended to the string representation
     * @return the string representation of this {@link NVector} */
    public String toString(final String doubleFormatString,
                           final String delimiter,
                           final String prefix,
                           final String suffix) {
        final Iterable<String> itrbl = DoubleStream.of(this.getComponents())
                                                   .mapToObj(d -> String.format(doubleFormatString, d))
                                                   .toList();
        return IterableUtils.toString(itrbl, String::valueOf, delimiter, prefix, suffix);
    }
    
    @Override
    public Matrix outerProduct(final RealVector v) {
        return new Matrix(super.outerProduct(v), false);
    }
    
    @Override
    public NVector copy() {
        return new NVector(this); // this constructor always copies
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.getDataRef());
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        
        final NVector other = (NVector) obj;
        return Arrays.equals(this.getDataRef(), other.getDataRef());
    }
    
    /** Get zero-vector in dimension {@code dim}
     * 
     * @param dim the dimension
     * @return the zero-vector in dimension {@code dim} */
    public static NVector getZero(final int dim) {
        Validation.requireGreaterThanEqualTo(dim, 0, "Cannot get a vector with dimension less than zero");
        return new NVector(new double[dim]);
    }
}
