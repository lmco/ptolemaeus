/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.real;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;

import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.exception.NullArgumentException;
import org.hipparchus.linear.AbstractRealMatrix;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.Array2DRowRealMatrix;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.commons.BeCareful;
import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.householderreductions.RealHouseholder;

/** A local extension of Hipparchus' {@link Array2DRowRealMatrix} with some convenience methods and additional
 * functionality. E.g., an advanced {@link #toString()} (see also {@link #toString(double)}) and the
 * {@link #solve(RealMatrix)} series of methods. <br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-01-15: write {@code preMultiplySubMatrix(NVector, rowStart, colStart)} and
 * {@code postMultiplySubMatrix(NVector, rowStart, colStart)} to that we can remove more calls to
 * {@link #getSubMatrix(int, int, int, int)}, esp. in {@link RealHouseholder}.<br>
 * <br>
 * 
 * TODO Ryan Moser, 2024-12-06: change {@link Matrix} to simply {@code implement} {@link RealMatrix}<br>
 * <br>
 * 
 * TODO Ryan Moser, 2025-01-02: implement a {@code static} multiply method analogous to
 * {@link ComplexMatrix#multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}, and use
 * that to override the {@code instance} multiply methods like {@link #multiply(RealMatrix)},
 * {@link #transposeMultiply(Array2DRowRealMatrix)}, etc. <br>
 * <br>
 * 
 * TODO Ryan Moser, 2024: write an unmodifiable view for instances of this class
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class Matrix extends Array2DRowRealMatrix {
    
    /** generated */
    private static final long serialVersionUID = 8472582244953668648L;
    
    /** the threshold to pass into {@link #formatDoubleForColumn(double, int, int, int)} when called from
     * {@link #toString()} */
    private static final int SCI_FMT_THRESHOLD = 30;
    
    /** The <em>maximum possible</em> rank of a matrix of this size: {@code min(numRows, numCols)}. This is equivalent
     * to the diagonal dimension of a matrix. */
    private final int maxRank;
    
    /** Create a new {@code RealMatrix} using the input array as the underlying data array.
     * <p>
     * The input array is copied, not referenced.
     * </p>
     *
     * @param d Data for the new matrix.
     * @throws MathIllegalArgumentException if {@code d} is not rectangular.
     * @throws MathIllegalArgumentException if {@code d}'s row or column dimension is zero.
     * @throws NullArgumentException if {@code d} is {@code null}.
     * @see Array2DRowRealMatrix#Array2DRowRealMatrix(double[][], boolean)
     * @see Array2DRowRealMatrix#Array2DRowRealMatrix(double[][]) */
    public Matrix(final double[][] d) {
        super(d, true);
        this.maxRank = FastMath.min(d.length, d[0].length);
    }
    
    /** Create a new {@code RealMatrix} using the input array as the underlying data array. The array is copied only if
     * specified that it must be; i.e., if {@code clone == true}.
     *
     * @param d Data for the new matrix.
     * @param clone clone {@code true} to clone the array, {@code false} to simply copy the reference. <b>Be
     *        careful!</b> If the array is not cloned, external changes to the array will be reflected in this instance.
     *        
     * @throws MathIllegalArgumentException if {@code d} is not rectangular or if {@code d}'s row or column dimension is
     *         zero.
     * @throws NullArgumentException if {@code d} is {@code null}.
     * @see Array2DRowRealMatrix#Array2DRowRealMatrix(double[][], boolean)
     * @see Array2DRowRealMatrix#Array2DRowRealMatrix(double[][]) */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public Matrix(final double[][] d, final boolean clone) {
        super(d, clone);
        this.maxRank = FastMath.min(d.length, d[0].length);
    }
    
    /** Constructor for efficiently creating a {@link Matrix} from a {@link RealMatrix}
     * 
     * @param matrix the {@link RealMatrix} we want to convert
     * @param clone {@code true} to clone the array, {@code false} to simply copy the reference. <b>Be careful!</b> If
     *        the array is not cloned, external changes to the array will be reflected in this instance.<br>
     *        <br>
     * 
     *        Additionally, if {@code matrix} is not an instance of {@link Array2DRowRealMatrix}, the data array will
     *        always be copied at least once, as we must use {@link RealMatrix#getData()} - rather than
     *        {@link Array2DRowRealMatrix#getDataRef()}.<br>
     *        <br>
     * 
     *        Thus in the case that {@code matrix} is not an instance of {@link Array2DRowRealMatrix} <em>and</em>
     *        {@code clone == true}, we will copy the data array twice, because it is not specified whether
     *        {@link RealMatrix#getData()} must return a copy.
     *        
     * @apiNote Users must be careful to not misuse the {@link RealMatrix} that was passed in, else changes to its data
     *          array will be reflected in this {@link Matrix} instance. */
    @BeCareful(reason = Numerics.OPTIONAL_CLONE_WARNING)
    public Matrix(final RealMatrix matrix, final boolean clone) {
        super(matrix instanceof Array2DRowRealMatrix ? ((Array2DRowRealMatrix) matrix).getDataRef() : matrix.getData(),
              clone);
        this.maxRank = FastMath.min(matrix.getRowDimension(), matrix.getColumnDimension());
    }
    
    /** If possible, get a {@link Matrix} representation of the given {@link AnyMatrix A}. This method attempts to avoid
     * cloning any arrays or creating any new instances. In particular:
     * 
     * <ol>
     * <li>If {@code A instanceof} {@link Matrix}, return A as-is</li>
     * <li>else if {@code A instanceof} {@link Array2DRowRealMatrix}, return a new {@link Matrix} that points to the
     * {@link Array2DRowRealMatrix#getDataRef() the data array} of {@code A}</li>
     * <li>else if {@code A instanceof} {@link RealMatrix}, return a new {@link Matrix} that wraps
     * {@link RealMatrix#getData()} of {@code A}. {@link RealMatrix#getData()} will usually make a copy, so expect a
     * copy if this is the case.</li>
     * <li>lastly, if {@code A instanceof} {@link ComplexMatrix}, return the result of
     * {@link ComplexMatrix#asReal(double)} with zero tolerance. If any imaginary parts are not exactly zero, this
     * throws an exception.</li>
     * </ol>
     * 
     * @param A the {@link AnyMatrix} for which we want a {@link Matrix}
     * @return the {@link Matrix} */
    public static Matrix of(final AnyMatrix A) {
        requireNonNull(A, "The matrix A must not be null");
        if (A instanceof Matrix M) {
            return M;
        }
        if (A instanceof Array2DRowRealMatrix M) {
            return new Matrix(M.getDataRef(), false);
        }
        if (A instanceof RealMatrix M) {
            return new Matrix(M.getData(), false);
        }
        if (A instanceof ComplexMatrix M) {
            return M.asReal(0.0).orElseThrow(() ->
                    new UnsupportedOperationException("The supplied ComplexMatrix has non-zero imaginary parts"));
        }
        throw new UnsupportedOperationException("Unable to create ComplexMatrix from type "
                                                + A.getClass().getSimpleName());
    }
    
    /** @return the <em>maximum possible</em> rank of a matrix of this size: {@code min(numRows, numCols)}. This is
     *         equivalent to the diagonal dimension of a matrix. */
    public int getMaxRank() {
        return this.maxRank;
    }
    
    /** Post-multiply this by {@code A}
     * 
     * @param A the post-multiplication {@link Matrix}
     * @return {@code this.A} */
    public Matrix times(final Matrix A) {
        return this.multiply(A);
    }
    
    /** Add {@code A} to this
     * 
     * @param A the {@link Matrix} to add
     * @return {@code this + A}} */
    public Matrix plus(final Matrix A) {
        return this.add(A);
    }
    
    /** Subtract {@code A} from this
     * 
     * @param A the {@link Matrix} to subtract
     * @return {@code this - A}} */
    public Matrix minus(final Matrix A) {
        return this.subtract(A);
    }
    
    @Override
    public Matrix transpose() {
        return new Matrix(super.transpose(), false);
    }
    
    @Override
    public Matrix add(final Array2DRowRealMatrix m) {
        return new Matrix(super.add(m), false);
    }
    
    @Override
    public Matrix add(final RealMatrix m) {
        return new Matrix(super.add(m), false);
    }
    
    @Override
    public Matrix subtract(final Array2DRowRealMatrix m) {
        return new Matrix(super.subtract(m), false);
    }
    
    @Override
    public Matrix subtract(final RealMatrix m) {
        return new Matrix(super.subtract(m), false);
    }
    
    @Override
    public Matrix multiply(final Array2DRowRealMatrix m) {
        return new Matrix(super.multiply(m), false);
    }
    
    @Override
    public Matrix multiply(final RealMatrix m) {
        return new Matrix(super.multiply(m), false);
    }
    
    @Override
    public Matrix transposeMultiply(final Array2DRowRealMatrix m) {
        return new Matrix(super.transposeMultiply(m), false);
    }
    
    @Override
    public Matrix transposeMultiply(final RealMatrix m) {
        return new Matrix(super.transposeMultiply(m), false);
    }
    
    @Override
    public Matrix multiplyTransposed(final Array2DRowRealMatrix m) {
        return Matrix.of(super.multiplyTransposed(m));
    }
    
    @Override
    public Matrix multiplyTransposed(final RealMatrix m) {
        return Matrix.of(super.multiplyTransposed(m));
    }
    
    @Override
    public Matrix scalarMultiply(final double d) {
        return new Matrix(super.scalarMultiply(d), false);
    }
    
    @Override
    public NVector operate(final RealVector v)  {
        return new NVector(super.operate(v), false);
    }
    
    @Override
    public Matrix power(final int p) {
        return new Matrix(super.power(p), false);
    }
    
    @Override
    public NVector operateTranspose(final RealVector x) {
        final Matrix xColumn = new Matrix(new double[][] { x.toArray() }).transpose();
        return this.transposeMultiply(xColumn).getColumnVector(0);
    }
    
    @Override
    public Matrix copy() {
        return new Matrix(super.copy(), false); // super::copy clones the data array, so we don't need to
    }
    
    @Override
    public NVector getRowVector(final int row) {
        return new NVector(super.getRowVector(row), false);
    }
    
    @Override
    public NVector getColumnVector(final int column) {
        return new NVector(super.getColumnVector(column), false);
    }
    
    @Override
    public Matrix getSubMatrix(final int startRow, final int endRow, final int startColumn, final int endColumn) {
        MatrixUtils.checkSubMatrixIndex(this, startRow, endRow, startColumn, endColumn);
        final int rowCount    = endRow - startRow + 1;
        final int columnCount = endColumn - startColumn + 1;
        
        final double[][] thisData = this.getDataRef();
        final double[][] outData  = new double[rowCount][columnCount];
        
        for (int i = 0; i < rowCount; ++i) {
            final double[] thisRow = thisData[startRow + i];
            final double[] outRow  = outData[i];
            for (int j = 0; j < columnCount; j++) {
                outRow[j] = thisRow[startColumn + j];
            }
        }
        
        return new Matrix(outData, false);
    }

    /**
     * @param startRow the start row of the slice
     * @param endRow the end row of the slice
     * @param startColumn the start column of the slice
     * @param endColumn the end column of the slice
     * @return a {@link MatrixSlice} derived from {@code this}
     */
    public MatrixSlice slice(final int startRow, final int endRow, final int startColumn, final int endColumn) {
        return new MatrixSlice(this, startRow, endRow, startColumn, endColumn);
    }

    /**
     * @param rows the rows to include in the slice, in order
     * @param columns the columns to include in the slice, in order
     * @return a {@link MatrixSlice} derived from {@code this}
     */
    public MatrixSlice slice(final int[] rows, final int[] columns) {
        return new MatrixSlice(this, rows, columns);
    }
    
    @Override
    public NVector preMultiply(final RealVector v) {
        return NVector.of(super.preMultiply(v));
    }
    
    /** @return {@code true} if {@code this} has strictly fewer columns than it does rows, {@code false} otherwise
     * 
     * @see #isSquare()
     * @see #isWide() */
    public boolean isTall() {
        return this.getColumnDimension() < this.getRowDimension();
    }
    
    /** @return {@code true} if {@code this} has strictly fewer rows than it does columns, {@code false} otherwise
     * 
     * @see #isSquare()
     * @see #isTall() */
    public boolean isWide() {
        return this.getRowDimension() < this.getColumnDimension();
    }
    
    /** Subtract {@code decrement} from the specified entry.<br>
     * <br>
     * 
     * TODO Ryan Moser, 2025-01-15: once {@link Matrix} is rewritten to maintain the data array itself, rewrite this
     * method to do the subtraction itself rather than using {@link #addToEntry(int, int, double)}
     * 
     * @param row n row to set value
     * @param column m column to set value
     * @param decrement the */
    public void subtractFromEntry(final int row, final int column, final double decrement) {
        this.addToEntry(row, column, -decrement);
    }
    
    /** Post-multiply in-place (i.e., modify directly) {@code A} by {@code smallB} starting at the given indices<br>
     * <br>
     * 
     * This is equivalent to padding {@code smallB} with an identity matrix which makes it compatible with
     * post-multiplication of {@code A}, placing {@code smallB} at the given indices, and post-multiplying {@code A}
     * with that padded matrix.<br>
     * <br>
     * 
     * We don't actually need to construct the aforementioned padded matrix, however, and can instead cleverly compute
     * the changes to {@code A}, as this process only changes a subset of the columns of {@code A}.<br>
     * <br>
     * 
     * E.g.,
     * 
     * <pre>
     * Let startRow = 2, startColumn = 3,
     * 
     * smallB = 
     *     . . .
     *     . . .
     * 
     * and A =
     *     . . . . . . . 
     *     . . . . . . . 
     *       . . . . . . 
     *         . . . . . 
     *           . . . . 
     *             . . . 
     *               . .
     * 
     * </pre>
     * 
     * after calling {@link #postMultiplyInPlace(Matrix, Matrix, int, int)}, {@code A} will have the form
     * 
     * <pre>
     * 
     * A = 
     *     . . . . . . .         1                     . . . x x x .
     *     . . . . . . .           1                   . . . x x x .
     *       . . . . . .             1 . . .             . . x x x .
     *         . . . . .    *          . . .      =        . x x x .
     *           . . . .                 1                   x x x .
     *             . . .                   1                 x x x .
     *               . .                     1               x x x .
     * </pre>
     * 
     * where the {@code x}'s mark the entries {@code A} that have changed<br>
     * <br>
     * 
     * @param A the {@link Matrix} that we want to post-multiply <b>in-place</b> by the smaller matrix {@code smallB}.
     * @param smallB the matrix - with dimensions at most the same as the dimensions of {@code A} transpose - by which
     *        we will post-multiply {@code A}. The greater the difference in dimension, the better the performance gain
     *        vs. the standard multiplication.
     * @param startRowIndex the row index where we want to consider {@code smallB} w.r.t. the padding
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     *        
     * @see #preMultiplyInPlace(Matrix, Matrix, int, int) */
    public static void postMultiplyInPlace(final Matrix A,
                                           final Matrix smallB,
                                           final int startRowIndex,
                                           final int startColIndex) {
        final int ANumRows = A.getRowDimension();
        final int ANumCols = A.getColumnDimension();
        
        final int BNumRows = smallB.getRowDimension();
        final int BNumCols = smallB.getColumnDimension();
        
        if (ANumCols < Numerics.max(BNumRows + startRowIndex, BNumCols + startColIndex)) {
            /* we only check against the number of columns in `A` because `smallB` is implicitly a square matrix with
             * dimension compatible with post-multiplication of `A` */
            throw NumericalMethodsException.formatted("Matrix A (%d x %d) cannot be post-multiplied in-place "
                                                      + "by smallB (%d x %d) when starting at indices (%d, %d)",
                                                      ANumRows,
                                                      ANumCols,
                                                      BNumRows,
                                                      BNumCols,
                                                      startRowIndex,
                                                      startColIndex).get();
        }
        
        final double[][] AData =      A.getDataRef();
        final double[][] BData = smallB.getDataRef();

        final int colLimit = startColIndex + BNumCols;
        final int rowLimit = startRowIndex + BNumRows;
        
        final double[][] newACols = new double[ANumRows][colLimit - startColIndex];
        
        for (int row = 0; row < ANumRows; row++) {
            final double[] ADataRow    = AData[row];
            final double[] newADataRow = newACols[row];
            
            for (int col = startColIndex; col < colLimit; col++) {
                computePostMultiplyInPlaceEntry(startRowIndex,
                                                startColIndex,
                                                col,
                                                ADataRow,
                                                BData,
                                                newADataRow,
                                                rowLimit);
            }
        }
        
        A.setSubMatrix(newACols, 0, startColIndex);
    }

    /** Compute and store an entry for the new columns computed in
     * {@link #postMultiplyInPlace(Matrix, Matrix, int, int)}<br>
     * <br>
     * 
     * TODO Ryan Moser, 2024-12-11: test whether manually skipping zeros actually offers improved performance. The
     * answer seems simple, but we need to consider whether reducing the branches allows the JVM to better optimize the
     * byte-code.
     * 
     * @param startRowIndex the row index where we want to consider {@code smallB} w.r.t. the padding
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     * @param col the column for which we're computing an entry (no offset)
     * @param ADataRefRow the data reference for the row of the matrix being post-multiplied
     * @param BDataRef the data reference for the matrix doing the post-multiplication
     * @param newAColsRow the new columns of the matrix being post-multiplied
     * @param rowLimit we don't consider elements of the dot product beyond this, as they are all zero. We pass this
     *        value in rather than compute it in place so that we only have to do it once. */
    private static void computePostMultiplyInPlaceEntry(final int startRowIndex,
                                                        final int startColIndex,
                                                        final int col,
                                                        final double[]   ADataRefRow,
                                                        final double[][] BDataRef,   
                                                        final double[]   newAColsRow,
                                                        final int rowLimit) {
        double sumRe = 0.0;
        boolean coversTheDiagonal = false;
        final int BDataColIndex = col - startColIndex;
        for (int i = startRowIndex; i < rowLimit; i++) {
            coversTheDiagonal |= (i == col);
            
            final double a = ADataRefRow[i];
            final double b = BDataRef[i - startRowIndex][BDataColIndex];
            if (a == 0 && b == 0) {
                continue;
            }
            
            sumRe += a * b;
        }

        /* the following if-block ensures that we include the implicit identity element in current column of the
         * "smallB" matrix. We need to do so whenever the non-zero (implicit) values of the column do not themselves
         * cover the diagonal element of that column (i.e., the element of the column which intersects the diagonal of
         * the implicit identity padding on "smallB") */
        if (!coversTheDiagonal) { /* TODO Ryan Moser, 2024-07-15: rewrite how we account for this so that we add this
                                   * value first, as it is likely smaller than the values we sum when computing the
                                   * row.vector dot product and because we know determine this in advance. I'm leaving
                                   * it at this moment only because I'm tired of fighting with indices. */
            sumRe += ADataRefRow[col];
        }
        
        newAColsRow[col - startColIndex] = sumRe;
    }
    
    /** Pre-multiply in-place (i.e., modify directly) {@code B} by {@code smallA} starting at the given indices.<br>
     * <br>
     * 
     * This is equivalent to padding {@code smallA} with an identity matrix which makes it compatible with
     * pre-multiplication of {@code B}, placing {@code smallA} at the given indices, and pre-multiplying {@code B} with
     * that padded matrix.<br>
     * <br>
     * 
     * We don't actually need to construct the aforementioned padded matrix, however, and can instead cleverly compute
     * the changes to {@code B}, as this process only changes a subset of the rows of {@code B}.<br>
     * <br>
     * 
     * E.g.,
     * 
     * <pre>
     * Let startRow = 2, startColumn = 3,
     * 
     * smallA = 
     *     . . .
     *     . . .
     * 
     * and B =
     *     . . . . . . . 
     *     . . . . . . . 
     *       . . . . . . 
     *         . . . . . 
     *           . . . . 
     *             . . . 
     *               . .
     * 
     * </pre>
     * 
     * after calling {@link #preMultiplyInPlace(Matrix, Matrix, int, int)}, {@code B} will have the form
     * 
     * <pre>
     * 
     * B = 
     *     1                     . . . . . . .         . . . . . . .
     *       1                   . . . . . . .         . . . . . . .
     *         1 . . .             . . . . . .         x x x x x x x
     *           . . .      *        . . . . .    =    x x x x x x x
     *             1                   . . . .               . . . .
     *               1                   . . .                 . . .
     *                 1                   . .                   . .
     * 
     * </pre>
     * 
     * where the {@code x}'s mark the entries {@code B} that have changed<br>
     * <br>
     * 
     * @param smallA the matrix - with dimensions at most the same as the dimensions of the transpose {@code B} - by
     *        which we will pre-multiply {@code B}. The greater the difference in dimension, the better the performance
     *        gain vs. the standard multiplication.
     * @param B the {@link Matrix} that we want to pre-multiply <b>in-place</b> by the smaller matrix {@code smallA}.
     * @param startRowIndex the row index where we want to consider {@code smallB} w.r.t. the padding
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     *        
     * @see #postMultiplyInPlace(Matrix, Matrix, int, int) */
    public static void preMultiplyInPlace(final Matrix smallA,
                                          final Matrix B,
                                          final int startRowIndex,
                                          final int startColIndex) {
        final int ANumRows = smallA.getRowDimension();
        final int ANumCols = smallA.getColumnDimension();
        
        final int BNumRows = B.getRowDimension();
        final int BNumCols = B.getColumnDimension();
        
        if (BNumRows < Numerics.max(ANumRows + startRowIndex, ANumCols + startColIndex)) {
            /* we only check against the number of rows in `B` because `smallA` is implicitly a square matrix with
             * dimension compatible with pre-multiplication of `B` */
            throw NumericalMethodsException.formatted("Matrix B (%d x %d) cannot be pre-multiplied in-place "
                                                      + "by smallA (%d x %d) when starting at indices (%d, %d)",
                                                      BNumRows,
                                                      BNumCols,
                                                      ANumRows,
                                                      ANumCols,
                                                      startRowIndex,
                                                      startColIndex).get();
        }
        
        final double[][] AData = smallA.getDataRef();
        final double[][] BData =      B.getDataRef();

        final int colLimit = startColIndex + ANumCols;
        final int rowLimit = startRowIndex + ANumRows;
        
        final double[][] newBRows = new double[rowLimit - startRowIndex][BNumCols];
        
        for (int row = startRowIndex; row < rowLimit; row++) {
            final int ADataRowIndex = row - startRowIndex;
            
            final double[] ADataRow    = AData[ADataRowIndex];
            final double[] newBDataRow = newBRows[ADataRowIndex];
            
            for (int col = 0; col < BNumCols; col++) {
                computePreMultiplyInPlaceEntry(startColIndex,
                                               row,
                                               col,
                                               ADataRow,
                                               BData,
                                               newBDataRow,
                                               colLimit);
            }
        }
        
        B.setSubMatrix(newBRows, startRowIndex, 0);
    }
    
    /** Compute and entry for the new columns computed in
     * {@link #preMultiplyInPlace(Matrix, Matrix, int, int)}<br>
     * <br>
     * 
     * TODO Ryan Moser, 2024-12-11: test whether manually skipping zeros actually offers improved performance. The
     * answer seems simple, but we need to consider whether reducing the branches allows the JVM to better optimize the
     * byte-code.
     * 
     * @param startColIndex the column index where we want to consider {@code smallB} w.r.t. the padding
     * @param row the row for which we're computing an entry (no offset)
     * @param col the column for which we're computing an entry (no offset)
     * @param ADataRefRow the data reference for the row of the matrix being pre-multiplied
     * @param BDataRef the data reference for the matrix doing the pre-multiplication
     * @param newBRow the new row of the matrix being pre-multiplied
     * @param colLimit we don't consider elements of the dot product beyond this, as they are all zero. We pass this
     *        value in rather than compute it in place so that we only have to do it once. */
    // CHECKSTYLE.OFF: ParameterNumber
    private static void computePreMultiplyInPlaceEntry(final int startColIndex,
                                                       final int row,
                                                       final int col,
                                                       final double[]   ADataRefRow,
                                                       final double[][] BDataRef,
                                                       final double[]   newBRow,
                                                       final int colLimit) {
        double sumRe = 0.0;
        boolean coversTheDiagonal = false;
        for (int i = startColIndex; i < colLimit; i++) {
            coversTheDiagonal |= (i == row);
            
            final double a = ADataRefRow[i - startColIndex];
            final double b = BDataRef[i][col];
            if (a == 0 || b == 0) {
                continue;
            }
            
            final double re = a * b;
            
            sumRe += re;
        }
        
        /* the following if-block ensures that we include the implicit identity element in current row of the "smallA"
         * matrix. We need to do so whenever the non-zero (implicit) values of the row do not themselves cover the
         * diagonal element of that row (i.e., the element of the row which intersects the diagonal of the implicit
         * identity padding on "smallA") */
        if (!coversTheDiagonal) { /* TODO Ryan Moser, 2024-07-15: rewrite how we account for this so that we add this
                                   * value first, as it is likely smaller than the values we sum when computing the
                                   * row.vector dot product and because we know determine this in advance. I'm leaving
                                   * it at this moment only because I'm tired of fighting with indices. */
            sumRe += BDataRef[row][col];
        }
        
        newBRow[col] = sumRe;
    }
    // CHECKSTYLE.ON: ParameterNumber
    
    /** Solve the linear equation {@code AX = B} for the matrix X where A is {@code this}.
     *
     * @param rhs right-hand side of the equation {@code AX = B}
     * @return a matrix X that minimizes the two norm of {@code AX - B} */
    public Matrix solve(final RealMatrix rhs) {
        return new Matrix(Numerics.computeAInvB(this, rhs), false);
    }
    
    /** Solve the linear equation {@code Ax = b} for the vector x where A is {@code this}.
     *
     * @param rhs right-hand side of the equation {@code Ax = B}
     * @return a vector <b>x</b> that minimizes the two norm of {@code Ax - B} */
    public NVector solve(final RealVector rhs) {
        return new NVector(Numerics.computeAInvB(this, rhs));
    }
    
    /** Determine equivalence between {@code this} and {@code other}. Equivalence is determined by element-by-element
     * equality within the given absolute tolerance.
     * 
     * @param other the other {@link RealMatrix}
     * @param absTol the absolute tolerance for entry equality; i.e., if two entries are less than this far apart, we
     *        consider them equal.
     * @return {@code true} if the elements of {@code this} and {@code other} are equal within the given tolerance,
     *         {@code false} otherwise
     * 
     * @see AbstractRealMatrix#equals(Object) */
    public boolean absEquivalentTo(final RealMatrix other, final double absTol) {
        if (this == other) {
            return true;
        }
        
        final int nRows = this.getRowDimension();
        final int nCols = this.getColumnDimension();
        if (other.getColumnDimension() != nCols || other.getRowDimension() != nRows) {
            return false;
        }
        
        for (int row = 0; row < nRows; ++row) {
            for (int col = 0; col < nCols; ++col) {
                if (!Precision.equals(this.getEntry(row, col), other.getEntry(row, col), absTol)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /** {@inheritDoc}<br><br>
     * 
     * Examples:
     * 
     * <br><br>
     * Matrix data
     * 
     * <pre>
     * new double[][] { { 1, 2, 3, 4, 5 },
     *                  { 2, 3, 4, 5, 1 },
     *                  { 3, 4, 5, 1, 2 },
     *                  { 4, 5, 1, 2, 3 },
     *                  { 5, 1, 2, 3, 4 } }
     * </pre>
     * 
     * results in 
     * 
     * <pre>
     *  1.0 2.0 3.0 4.0 5.0
     *  2.0 3.0 4.0 5.0 1.0
     *  3.0 4.0 5.0 1.0 2.0
     *  4.0 5.0 1.0 2.0 3.0
     *  5.0 1.0 2.0 3.0 4.0
     * </pre>
     * 
     * <br><br>
     * Matrix data
     * 
     * <pre>
     * new double[][] { { 0.63829787234043, 1.914,     1.27659574468085 },
     *                  { 0.95744680851064, 2.87234,   1.91489361702128 },
     *                  { 1.27659574468085, 3.8297872, 2.553            },
     *                  { 0.95744680851064, 2.87,      1.91489361702128 },
     *                  { 0.95744680851064, 2.1231231, 2.123123123123123123123123123123 } }
     * </pre>
     * 
     * results in 
     * 
     * <pre>
     *  0.63829787234043 1.914     1.27659574468085 
     *  0.95744680851064 2.87234   1.91489361702128 
     *  1.27659574468085 3.8297872 2.553            
     *  0.95744680851064 2.87      1.91489361702128 
     *  0.95744680851064 2.1231231 2.123123123123123
     * </pre>
     * 
     * <br><br>
     * Matrix data
     * 
     * <pre>
     * new double[][] { { -Long.MAX_VALUE,  1.27659574468085,  1.91489361702128 },
     *                  { 0.9574468,        1.91489361702128,  Double.MIN_NORMAL },
     *                  { 1.2765957446,     2.553,             3.82978723404255 },
     *                  { 0.95744680851064, -Double.MAX_VALUE, 2.87234042553191 },
     *                  { Double.MAX_VALUE, 1.91489361702128,  2.123123123123123123123123123123 } }
     * </pre>
     * 
     * results in 
     * 
     * <pre>
     *  -9.2233720368547760e+18   1.2765957446808500e+00   1.9148936170212800e+00 
     *   9.5744680000000000e-01   1.9148936170212800e+00   2.2250738585072014e-308
     *   1.2765957446000000e+00   2.5530000000000000e+00   3.8297872340425500e+00 
     *   9.5744680851064000e-01  -1.7976931348623157e+308  2.8723404255319100e+00 
     *   1.7976931348623157e+308  1.9148936170212800e+00   2.1231231231231230e+00 
     * </pre>
     * 
     * <br><br>
     * Matrix data
     * 
     * <pre>
     * new double[][] { { -1_000_000.0001,      1.00000001,              -1.00000001   },
     *                  {  1_000_000.0002,    -10.0002000002,            10.0002000002 },
     *                  {  1_000_000.0003,    100.0003,                     Double.NaN },
     *                  { -1_000.0004,      -1000.0004,       Double.NEGATIVE_INFINITY },
     *                  {  1_000.0005,      10000.0005,       Double.POSITIVE_INFINITY } }
     * </pre>
     * 
     * results in 
     * 
     * <pre>
     *  -1000000.0001     1.00000001   -1.00000001  
     *   1000000.0002   -10.0002000002 10.0002000002
     *   1000000.0003   100.0003                 NaN
     *     -1000.0004 -1000.0004           -Infinity
     *      1000.0005 10000.0005            Infinity
     * </pre>
     */
    @Override
    public String toString() {
        return this.toString(0.0); // by default, no lower bound
    }
    
    /** The same as {@link #toString()}, but with a lower bound on what values will be shown as non-zero
     * 
     * @param lowerBound if an entry's absolute value is smaller than this, we will print zero for that entry
     * @return the String representation of this
     * 
     * @see #toString() */
    public String toString(final double lowerBound) {
        final int rowDim = this.getRowDimension();
        final int colDim = this.getColumnDimension();
        
        final String[][] stringMatrix = doubleMatrixToStringMatrix(this.getDataRef(), lowerBound);
        
        final StringBuilder answer = new StringBuilder();
        for (int i = 0; i < rowDim; ++i) {
            for (int j = 0; j < colDim; ++j) { // now we just concatenate everything with some new-lines
                answer.append(stringMatrix[i][j]);
            }
            if (i != rowDim - 1) {
                answer.append(System.lineSeparator());
            }
        }
        return answer.toString();
    }
    
    /** For the given {@code double[][]}, create a {@code String[][]} whose entries are String representations of the
     * doubles, formatted so that decimals and spacing align when printed row by column.
     * 
     * @param input the {@code double[][]}
     * @param lowerBound if an entry's absolute value is smaller than this, we will print zero for that entry
     * @return the {@code String[][]} */
    public static String[][] doubleMatrixToStringMatrix(final double[][] input, final double lowerBound) {
        final int rowDim = input.length;
        final int colDim = input[0].length;
        
        final double[][] m = new double[rowDim][colDim];
        for (int i = 0; i < rowDim; i++) {
            for (int j = 0; j < colDim; j++) {
                if (FastMath.abs(input[i][j]) < lowerBound) {
                    m[i][j] = 0.0;
                }
                else {
                    m[i][j] = input[i][j];
                }
            }
        }
        
        final String[][] stringMatrix = new String[rowDim][colDim];
        for (int j = 0; j < colDim; ++j) { /* for each column, we first find the longest integer and fraction part of
                                            * the BigDecimal::toPlainString representation of over each entry in the
                                            * column */
            int longestIntPart = 1;
            int longestFrcPart = 1;
            for (int i = 0; i < rowDim; ++i) { /* notice that we're iterating over the rows in the internal loop, rather
                                                * than the usual rows-then-columns. That's because the goal is only to
                                                * align the columns, which does not necessitate that each column have
                                                * the same width. */
                final double entry = m[i][j];
                
                if (Double.isInfinite(entry) || Double.isNaN(entry)) {
                    /* The string representations are "Infinity", "-Infinity", and "NaN" */
                    
                    /* here, we only want to (possibly) increase the size of whichever is already larger. This helps
                     * prevent making the integer or fractional parts longer than they need to be */
                    if (longestIntPart > longestFrcPart) {
                        longestIntPart = FastMath.max(longestIntPart, Double.toString(entry).length());
                    }
                    else {
                        longestFrcPart = FastMath.max(longestFrcPart, Double.toString(entry).length());
                    }
                }
                else {
                    final String entryToPlainString = BigDecimal.valueOf(entry).toPlainString();
                    /* ^ BigDecimal::toPlanString never uses scientific notation, so we don't need to worry about 'E',
                     * but we do need to consider whether there is a decimal, because it's excluded when using
                     * BigDecimal::ofValue if the number is large enough */
                    
                    final int periodIndex = entryToPlainString.indexOf('.');
                    
                    if (periodIndex == -1) {
                        final String intPart = entryToPlainString;
                        longestIntPart = FastMath.max(longestIntPart, intPart.length());
                    }
                    else {
                        final String intPart = entryToPlainString.substring(0, periodIndex);  // [0, index)
                        final String frcPart = entryToPlainString.substring(periodIndex + 1); // [index + 1, length)
                        
                        longestIntPart = FastMath.max(longestIntPart, intPart.length());
                        longestFrcPart = FastMath.max(longestFrcPart, frcPart.length());
                    }
                }
            }
            
            boolean allEndInDot0 = true;
            for (int i = 0; i < rowDim; ++i) { // then for each entry in the column create and save a String
                final double entry    = m[i][j];
                final String ijString = Matrix.formatDoubleForColumn(entry,
                                                                     longestIntPart,
                                                                     longestFrcPart,
                                                                     SCI_FMT_THRESHOLD);
                stringMatrix[i][j] = ijString;
                
                allEndInDot0 &= ijString.endsWith(".0");
            }
            
            if (allEndInDot0) { /* if all values in a single column end with ".0", replace all of them with the empty
                                 * string. Else, keep the ".0" there. */
                for (int i = 0; i < rowDim; ++i) {
                    stringMatrix[i][j] = stringMatrix[i][j].replace(".0", "");
                }
            }
        }
        
        return stringMatrix;
    }
    
    /** Format the given double so that repeated calls - with varied {@code x} and constant {@code longestIntPart},
     * {@code longestFrcPart}, and {@code sciFmtThreshold} - are aligned for readability.<br>
     * <br>
     * 
     * If the numbers are big or small enough s.t. the sum of {@code longestIntPart + longestFrcPart} is greater than
     * {@code sciFmtThreshold}, then returned String will use scientific notation (e.g., 1.2e-30).
     * 
     * @param x the double to format
     * @param longestIntPartLength the longest length of the string representing of the integral part among all numbers
     *        in the a given column of a matrix
     * @param longestFrcPartLength the longest length of the string representing of the fractional part among all
     *        numbers in the a given column of a matrix
     * @param sciFmtThreshold if the sum of {@code longestIntPart + longestFrcPart} is greater than this, the returned
     *        String will use scientific notation (e.g., 1.2e-30). Otherwise, the returned String will have lengths that
     *        match the longest parts respectively.
     * @return the string representation
     *        
     * @implNote Regarding the use of 24 in the format string:
     *        
     *           The longest string we'll ever need to format, when using scientific notation, is the string
     *           representation of (-Double.MAX_VALUE + 1), which has 24 characters (including E-324)
     *           
     *           <pre>
     * Double.MAX_VALUE     String:  1.7976931348623157E+308
     * Double.MAX_NORMAL    String:  2.2250738585072014E-308 (smallest normal number)
     *-Double.MAX_VALUE + 1 String: -1.7976931348623157E+308
     * Double.MIN_VALUE     String:  4.9E-324                (smallest supported subnormal number)
     *           </pre>
     */
    public static String formatDoubleForColumn(final double x,
                                               final int longestIntPartLength,
                                               final int longestFrcPartLength,
                                               final int sciFmtThreshold) {
        Validation.requireGreaterThan(longestIntPartLength, 0, "longestIntPart");
        Validation.requireGreaterThan(longestFrcPartLength, 0, "longestFrcPart");
        Validation.requireGreaterThan(sciFmtThreshold, 0, "sciFmtThreshold");
        
        final int total = longestIntPartLength + longestFrcPartLength;
        
        if (Double.isInfinite(x) || Double.isNaN(x)) {
            final String infNaNFmt;
            if (total >= sciFmtThreshold) {
                infNaNFmt = " %24s";
            }
            else { // add 2: one for the decimal, and one for the positive/negative offset that we don't do here
                infNaNFmt = "%" + (2 + total) + "s";
            }
            
            return infNaNFmt.formatted(x);
        }
        
        if (total >= sciFmtThreshold) { // use scientific notation and left-justify; e.g., "  1.1000000000000000e+00 "
            final String leftJustified = " %- 24.16e";
            return leftJustified.formatted(x);
        }
        else {
            final BigDecimal bdX            = BigDecimal.valueOf(x);
            final String[]   bothParts      = bdX.toPlainString().split("\\.");
            final String     integerPart    = bothParts[0];
            final String     fractionalPart = bothParts.length == 1 ? "0" : bothParts[1];
            
            // the following is left split up for readability
            final String fmt = " "
                             + "%" + longestIntPartLength +  "s" // right-justify the integral part
                             + "."
                             + "%-" + longestFrcPartLength + "s"; // left-justify the fractional part
            
            final String answer = fmt.formatted(integerPart, fractionalPart);
            return answer;
        }
    }
    
    /** Get the diagonal piece of a {@link RealMatrix} as a diagonal {@link Matrix}
     * 
     * @param m the {@link RealMatrix} whose diagonal we want
     * @return the diagonal piece of a {@link RealMatrix} as a diagonal {@link Matrix} */
    public static Matrix getDiagonalPiece(final RealMatrix m) {
        Validation.requireTrue(m.isSquare(), "cannot get the diagonal of a non-square matrix");
        final int height = m.getColumnDimension();
        final double[][] answer = new double[height][height];
        for (int i = 0; i < height; i++) {
            answer[i][i] = m.getEntry(i, i);
        }
        
        return new Matrix(answer);
    }
    
    /** Return a diagonal matrix, with the values of 'd' along the diagonal.
     * 
     * @param d the values to be placed along the diagonal
     * @return the newly-created Matrix */
    public static Matrix diagonal(final double[] d) {
        int n = d.length;
        final double[][] answer = new double[n][n];
        for (int i = 0; i < n; ++i) {
            answer[i][i] = d[i];
        }
        return new Matrix(answer);
    }
    
    /** Compute and return the identity {@link Matrix} of rank {@code dim}
     * 
     * @param dim the dimension
     * @return the identity {@link Matrix} of rank {@code dim} */
    public static Matrix identity(final int dim) {
        Validation.requireGreaterThanEqualTo(dim, 1, "dimension");
        final double[][] arrMatrix = new double[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                arrMatrix[i][j] = i == j ? 1 : 0;
            }
        }
        
        return new Matrix(arrMatrix);
    }
    
    /** Create and return a {@link Matrix} with dimensions {@code (m x n)} whose entries are all {@code 1.0}
     * 
     * @param m the number of rows
     * @param n the number of columns
     * @return a {@code (m x n)} {@link Matrix} whose entries are all {@code 1.0} */
    public static Matrix ones(final int m, final int n) {
        return constant(1.0, m, n);
    }
    
    /** Create and return a {@link Matrix} with dimensions {@code (m x n)} whose entries are all {@code c}
     * 
     * @param c the value to assign to each entry
     * @param m the number of rows
     * @param n the number of columns
     * @return a {@code (m x n)} {@link Matrix} whose entries are all {@code c} */
    public static Matrix constant(final double c, final int m, final int n) {
        final double[][] constArr = new double[m][n];
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                constArr[i][j] = c;
            }
        }
        
        return new Matrix(constArr);
    }
    
    /** Create and return a {@link Matrix} with dimensions {@code (m x n)} whose entries are all {@code 0.0}
     * 
     * @param m the number of rows
     * @param n the number of columns
     * @return a {@code (m x n)} {@link Matrix} whose entries are all {@code 0.0} */
    public static Matrix zero(final int m, final int n) {
        return new Matrix(new double[m][n]);
    }
}
