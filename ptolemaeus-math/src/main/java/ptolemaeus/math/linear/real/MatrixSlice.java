/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.real;

import java.util.Objects;
import java.util.function.IntUnaryOperator;

import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.util.FastMath;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import ptolemaeus.commons.Validation;

/**
 * {MatrixSlice} represents a mutable window, or 2D slice, of a {@link Matrix}. Slices are read-through and write-back; 
 * changes to the slice are reflected in the underlying {@link Matrix}, and vice-versa.<br>
 * TODO (Peter 01/29/25): There's lots of methods from the {@link Matrix} hierarchy that still need to be overriden for
 * safe use, but we may be breaking {@link Matrix} out of the hipparchus matrix hierarchy.
 * @author Peter Davis, peter.m.davis@lmco.com
 */
@SuppressWarnings("serial")
public class MatrixSlice extends Matrix {
    
    /**
     * The source matrix that this slice is referencing.
     */
    private final Matrix sourceMatrix;

    /**
     * The {@link IntUnaryOperator} to map from row indices for this {@link MatrixSlice} to it's 
     * source {@link Matrix}'s row indices.
     */
    private final IntUnaryOperator rowMapper;

    /**
     * The {@link IntUnaryOperator} to map from row indices for this {@link MatrixSlice} to it's 
     * source {@link Matrix}'s row indices.
     */
    private final IntUnaryOperator colMapper;

    /**
     * The row dimension of the slice
     */
    private final int rowDimension;

    /**
     * The column dimension of the slice
     */
    private final int colDimension;

    /**
     * Construct a {@link MatrixSlice} from the provided source matrix, row indices, and column indices. 
     * 
     * The order the row and column indices are provided in will be the order the {@link MatrixSlice} indexes them. For 
     * example, if one creates {@code MatrixSlice slice = new MatrixSlice(mat, new int[]{4, 9, 6}, new int[]{0, 1, 2})},
     * then the second row of {@code slice} will be the 10th row of {@code mat}. 
     * @param sourceMatrix the source matrix that this slice is referencing.
     * @param rowIdxs the row indices to use for the slice. 
     * @param colIdxs the column indices to use for the slice.
     */
    @SuppressFBWarnings(value = "EI2", justification = "sourceMatrix is intentionally mutable")
    public MatrixSlice(final Matrix sourceMatrix, final int[] rowIdxs, final int[] colIdxs) {
        super(sourceMatrix.getDataRef(), false);
        this.sourceMatrix = sourceMatrix;
        // we clone these index arrays so that changes to the argument arrays don't change the rows and columns we're
        // referencing
        final int[] rowIdxsClone = rowIdxs.clone();
        final int[] colIdxsClone = colIdxs.clone();
        rowMapper = i -> rowIdxsClone[i];
        colMapper = i -> colIdxsClone[i];
        this.rowDimension = rowIdxsClone.length;
        this.colDimension = colIdxsClone.length;
    }

    /**
     * Construct a {@link MatrixSlice} from the provided source matrix and row and column starts and ends, inclusive.
     * 
     * This will be a contiguous rectangular slice, or "window", of the source matrix.
     * @param sourceMatrix the source matrix that this slice is referencing.
     * @param rowStart the first row index of the source matrix to reference.
     * @param rowEnd the last row index of the source matrix to reference.
     * @param colStart the first column index of the source matrix to reference.
     * @param colEnd the last column index of the source matrix to reference.
     */
    @SuppressFBWarnings(value = "EI2", justification = "sourceMatrix is intentionally mutable")
    public MatrixSlice(final Matrix sourceMatrix, final int rowStart, final int rowEnd, 
                                                  final int colStart, final int colEnd) {
        super(sourceMatrix.getDataRef(), false);
        Validation.requireGreaterThanEqualTo(rowStart, 0, "rowStart");
        Validation.requireGreaterThanEqualTo(rowEnd, rowStart, "rowEnd");
        Validation.requireGreaterThanEqualTo(colStart, 0, "colStart");
        Validation.requireGreaterThanEqualTo(colEnd, colStart, "colEnd");
        this.sourceMatrix = sourceMatrix;
        this.rowMapper = i -> i + rowStart;
        this.colMapper = i -> i + colStart;
        this.rowDimension = rowEnd - rowStart + 1;
        this.colDimension = colEnd - colStart + 1;
    }
    
    /**
     * Create a slice from a given source matrix, dimensions, row mapper, and column mapper.
     * @param sourceMatrix the source matrix that this slice is referencing.
     * @param rowDimension the number of rows this slice has
     * @param colDimension the number of columns this slice has
     * @param rowMapper the mapping function for row indices
     * @param colMapper the mapping function for column indices
     */
    MatrixSlice(final Matrix sourceMatrix, final int rowDimension, final int colDimension, 
                        final IntUnaryOperator rowMapper, final IntUnaryOperator colMapper) {
        super(sourceMatrix.getDataRef(), false);
        this.sourceMatrix = sourceMatrix;
        this.rowDimension = rowDimension;
        this.colDimension = colDimension;
        this.rowMapper = rowMapper;
        this.colMapper = colMapper;
    }

    @Override
    public int getRowDimension() {
        return this.rowDimension;
    }

    @Override
    public int getColumnDimension() {
        return this.colDimension;
    }

    @Override
    public Matrix createMatrix(final int rowDim, final int colDim) {
        throw new UnsupportedOperationException("Cannot create a MatrixSlice with no source matrix");
    }

    @Override
    public double getEntry(final int row, final int col) {
        return sourceMatrix.getEntry(rowMapper.applyAsInt(row), colMapper.applyAsInt(col));
    }

    @Override
    public void setEntry(final int row, final int col, final double value) {
        sourceMatrix.setEntry(rowMapper.applyAsInt(row), colMapper.applyAsInt(col), value);
    }

    @Override
    public void addToEntry(final int row, final int col, final double value) {
        super.addToEntry(rowMapper.applyAsInt(row), colMapper.applyAsInt(col), value);
    }

    @Override
    public void multiplyEntry(final int row, final int col, final double value) {
        super.multiplyEntry(rowMapper.applyAsInt(row), colMapper.applyAsInt(col), value);
    }

    @Override
    public double[][] getData() {
        final double[][] out = new double[rowDimension][colDimension];
        for (int i = 0; i < rowDimension; i++) {
            for (int j = 0; j < colDimension; j++) {
                out[i][j] = getEntry(i, j);
            }
        }
        return out;
    }

    @Override
    public Matrix copy() {
        return new Matrix(getData(), false);
    }

    @Override
    public double[][] getDataRef() {
        throw new UnsupportedOperationException("MatrixSlice cannot provide a data ref.");
    }

    @Override
    public Matrix getSubMatrix(final int rowStart, final int rowEnd, final int colStart, final int colEnd) {
        MatrixUtils.checkSubMatrixIndex(this, rowStart, rowEnd, colStart, colEnd);
        final int rowCount    = rowEnd - rowStart + 1;
        final int columnCount = colEnd - colStart + 1;
        final double[][] outData  = new double[rowCount][columnCount];
        
        for (int i = 0; i < rowCount; ++i) {
            for (int j = 0; j < columnCount; j++) {
                outData[i][j] = getEntry(i, j);
            }
        }
        return new Matrix(outData, false);
    }

    @Override
    public Matrix getSubMatrix(final int[] rows, final int[] cols) {
        final int[] minMaxRows = getMinAndMax(rows);
        final int[] minMaxCols = getMinAndMax(cols);
        MatrixUtils.checkSubMatrixIndex(this, minMaxRows[0], minMaxRows[1], minMaxCols[0], minMaxCols[1]);
        final int rowCount    = rows.length;
        final int columnCount = cols.length;
        final double[][] outData  = new double[rowCount][columnCount];
        
        for (int i = 0; i < rowCount; ++i) {
            for (int j = 0; j < columnCount; j++) {
                outData[i][j] = getEntry(rows[i], cols[j]);
            }
        }
        return new Matrix(outData, false);
    }

    /** Get the smallest and largest values of the given {@code int[]}
     * 
     * @param arr the {@code int[]}
     * @return the smallest and largest values */
    private int[] getMinAndMax(final int[] arr) {
        Objects.requireNonNull(arr, "arr cannot be null");
        Validation.requireGreaterThan(arr.length, 0, "arr.length");
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            min = FastMath.min(min, arr[i]);
            max = FastMath.max(max, arr[i]);
        }
        return new int[] {min, max};
    }

    /**
     * Equals method. Slices are considered equal if their data matches exactly, but do not need to reference the same 
     * source matrix.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }

        MatrixSlice other = (MatrixSlice) obj;
        if (other.rowDimension != this.rowDimension 
                || other.colDimension != this.colDimension) {
            return false;
        }

        for (int row = 0; row < this.rowDimension; ++row) {
            for (int col = 0; col < this.colDimension; ++col) {
                if (getEntry(row, col) != other.getEntry(row, col)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(sourceMatrix, rowDimension, colDimension);
    }
}
