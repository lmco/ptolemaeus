/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.balancing;

import ptolemaeus.math.linear.eigen.Eigen;
import ptolemaeus.math.linear.householderreductions.ComplexHessenbergReduction;

/** A {@link BalanceMode} dictates how - if at all - to balance a matrix before computing its Eigenvalues when using
 * {@link Eigen}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public enum BalanceMode {
    
    /** Disable balancing altogether */
    NONE,
    
    /** Balance the matrix in the 1-norm - i.e., the sum of the absolute values of the components - of the rows and
     * column vectors of a matrix, <em>ignoring the diagonal elements.</em> This is done in the name of efficiency and
     * is indeed the standard method of balancing; e.g., see any of the LAPACK
     * <a href="https://www.netlib.org/lapack//explore-html/df/df3/group__gebal.html"> GEBAL</a> implementations.<br>
     * <br>
     * 
     * This mode infamously has a deficiency in that it ruins the <em>Eigenvalue condition number</em> of
     * {@link ComplexHessenbergReduction Hessenberg} matrices. */
    ONE_NORM,
    
    /** Balance the matrix in the 2-norm - i.e., the square-root of the sum of the squares of the components - of the
     * rows and column vectors of a matrix, and <em>do not</em> ignore the diagonal entries. This results in a more
     * strict stopping criterion (strict in the sense that it stops the iteration sooner) that can help prevent
     * over-balancing.<br>
     * <br>
     * 
     * Importantly, this mode is less likely to ruin the balancing of {@link ComplexHessenbergReduction Hessenberg}
     * matrices. */
    TWO_NORM;
}
