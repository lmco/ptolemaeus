/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.balancing;

import java.util.Arrays;

import org.hipparchus.util.FastMath;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;

/** A balanced matrix is one whose corresponding row and column norms are equal; i.e., norm(row<sub>i</sub>) ==
 * norm(col<sub>i</sub>). A balanced matrix will (typically) help reduce the Eigenvalue condition number, and will
 * always have a minimal 2-norm itself.<br>
 * <br>
 * 
 * To balance a matrix, we use similarity transforms (a change of basis) so that we don't disturb the Eigenvalues, and
 * we can get very close to a balanced matrix without introducing numerical noise by only ever multiplying and dividing
 * by the machine base (2, in this case), and this is what {@link MatrixBalancer} does.<br>
 * <br>
 * 
 * For more information, see the introduction to the paper linked below.<br><br>
 * 
 * Note: we do <em>not</em> permute the matrix here isolate to Eigenvalues.<br>
 * <br>
 * 
 * There are two choices available when balancing a matrix: {@link BalanceMode#ONE_NORM balancing the 1-norm} and
 * {@link BalanceMode#TWO_NORM balancing the 2-norm}.<br>
 * <br>
 * 
 * The classic option is to balance the 1-norm. This has been the default in LAPACK (and, thus, MATLAB) since at least
 * the 1990's, but it's not necessarily optimal. <br>
 * <br>
 * 
 * The implementation follows that of <i>Numerical Recipes 3rd Ed.</i>, section 11.6.1: Balancing<br>
 * <br>
 * 
 * The slight modification to the algorithm to both balance the 2-norm and include the diagonal elements was proposed in
 * <a href="https://arxiv.org/pdf/1401.5766"> ON MATRIX BALANCING AND EIGENVECTOR COMPUTATION</a>. Their data and the
 * tests I've written here suggest that this is reasonable, but I (RM) cannot find a published version of the paper.
 * I've reached out to the authors to determine why.<br>
 * <br>
 * 
 * TODO Ryan Moser, 2024-10-21: update when we hear back
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public final class MatrixBalancer {
    
    /** The machine number base */
    private static final double RADIX = 2;
    
    /** The square of the machine number base */
    private static final double SQRDX = RADIX * RADIX;
    
    /** if a balancing factor doesn't reduce a row + column norm sum by a factor of one minus this value, we ignore it
     * and continue. Balancing stops once we no longer find any adequate reductions */
    private static final double S_SCALE = 0.95;
    
    /** A balanced {@link ComplexMatrix} that's similar to the original */
    private final ComplexMatrix balanced;
    
    /** To undo the effects of balancing Eigenvectors, we maintain this {@code double[]} of scalars computed while
     * balancing.  This array is indexed by row number. */
    private final double[] eigenvectorScale;
    
    /** Constructor
     * 
     * @param balanced the balanced {@link ComplexMatrix}
     * @param eigenvectorScale the array of scales we need if we want to undo the effects of balancing on
     *        Eigenvectors */
    private MatrixBalancer(final ComplexMatrix balanced, final double[] eigenvectorScale) {
        this.eigenvectorScale = eigenvectorScale;
        this.balanced         = balanced;
    }
    
    /** @return the array of scales to undo the effects of balancing on Eigenvectors, indexed by row number. */
    public double[] getScale() {
        return this.eigenvectorScale.clone();
    }
    
    /** @return the balanced {@link ComplexMatrix} */
    public ComplexMatrix getBalanced() {
        return this.balanced.copy();
    }
    
    /** Undo the effects of balancing on the Eigenvectors of the originally balanced matrix. Modification is done
     * in-place.
     * 
     * @param eigenvectors the {@link ComplexMatrix} whose columns are the Eigenvectors of the matrix that was balanced.
     *        Modified in-place! */
    public void adjustEigenvectors(final ComplexMatrix eigenvectors) {
        adjustEigenvectors(eigenvectors, this.eigenvectorScale);
    }
    
    /** Undo the effects of balancing on the Eigenvectors of the originally balanced matrix. Modification is done
     * in-place.
     * 
     * @param eigenvectors the {@link ComplexMatrix} whose columns are the Eigenvectors of the matrix that was balanced.
     *        Modified in-place!
     * @param scale scalars computed while balancing that we'll use to undo the effects of balancing */
    public static void adjustEigenvectors(final ComplexMatrix eigenvectors, final double[] scale) {
        final int n = eigenvectors.getColumnDimension();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                eigenvectors.multiplyEntry(i, j, scale[i]);
            }
        }
        
        for (int j = 0; j < n; j++) {
            final ComplexVector ithEigvec = eigenvectors.getColumnVector(j);
            if (ithEigvec.isZero()) {
                continue;
            }
            
            final ComplexVector reNormalized = ithEigvec.normalizedHermitian();
            
            eigenvectors.setColumnVector(j, reNormalized);
        }
    }
    
    /** Compute a matrix similar to {@code A} - similar in the sense that the Eigenvalues are unchanged - which has
     * better scaling than {@code A}. This often reduces the error in Eigenvalue calculations that can result due to
     * poorly scaled matrices.
     * 
     * @param A the {@link ComplexMatrix} to balance
     * @param mode the {@link BalanceMode} to use
     * @return the {@link MatrixBalancer} instance */
    public static MatrixBalancer balance(final ComplexMatrix A, final BalanceMode mode) {
        final ComplexMatrix copy  = A.copy();
        final double[]      scale = doInPlace(copy, mode);
        return new MatrixBalancer(copy, scale);
    }
    
    /** {@link #balance(ComplexMatrix, BalanceMode) Balance} {@code A} in-place. This means that the data of {@code A}
     * is modified directly.
     * 
     * @param A the {@link ComplexMatrix} to balance
     * @param mode the {@link BalanceMode} to use
     * @return an array of scales that can be used to undo the effects of scaling on the Eigenvectors of {@code A} */
    public static double[] doInPlace(final ComplexMatrix A, final BalanceMode mode) {
        Numerics.requireSquare(A);
        
        final int      n     = A.getColumnDimension();
        final double[] scale = new double[n];
        Arrays.fill(scale, 1.0);
        if (mode == null || mode == BalanceMode.NONE) {
            return scale;
        }
        
        final boolean use2Norm = mode == BalanceMode.TWO_NORM;
        
        final double[][][] ReIm = A.getDataRef();
        final double[][]   Re   = ReIm[0];
        final double[][]   Im   = ReIm[1];
        
        boolean done = false;
        while (!done) {
            done = true;
            
            for (int i = 0; i < n; i++) {
                // first, compute running row and column p-norms r and c (p is either 1 or 2)
                
                double r = 0.0;
                double c = 0.0;
                
                if (use2Norm) {
                    for (int j = 0; j < n; j++) {
                        r += Numerics.complexNormSq(Re[i][j], Im[i][j]); // i, j
                        c += Numerics.complexNormSq(Re[j][i], Im[j][i]); // j, i
                    }
                    
                    r = FastMath.sqrt(r);
                    c = FastMath.sqrt(c);
                }
                else {
                    for (int j = 0; j < n; j++) {
                        if (i != j) { // skip the diagonal elements; see BalancingMode
                            r += FastMath.abs(Re[i][j]) + FastMath.abs(Im[i][j]); // i, j
                            c += FastMath.abs(Re[j][i]) + FastMath.abs(Im[j][i]); // j, i
                        }
                    }
                }
                
                if (r != 0.0 && c != 0.0) { // if both are non-zero
                    final double s  = c + r;
                    
                    double f = 1.0;
                    double g = r / RADIX;
                    
                    // find the integer power of the machine RADIX that comes closest to balancing the matrix
                    while (c < g) {
                        f *= RADIX;
                        c *= SQRDX;
                    }
                    
                    g = r * RADIX;
                    while (c > g) {
                        f /= RADIX;
                        c /= SQRDX;
                    }
                    
                    if ((c + r) / f < S_SCALE * s) {
                        done = false;
                        g    = 1.0 / f;
                        
                        scale[i] *= f; // save the scale so we can use it later to undo the effects on Eigenvectors
                        
                        // apply the similarity transform
                        for (int j = 0; j < n; j++) {
                            A.multiplyEntry(i, j, g);
                        }
                        for (int j = 0; j < n; j++) {
                            A.multiplyEntry(j, i, f);
                        }
                    }
                }
            }
        }
        
        return scale;
    }
}
