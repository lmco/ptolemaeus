/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

import java.util.Optional;
import java.util.function.Function;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.SingularValueDecomposition;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;

/** A class to hold basic constant information for a {@link WeightedLeastSquaresProblem}<br>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class WeightedLeastSquaresConstants {
    
    /** Default epsilon to use if none is specified */
    public static final double DEFAULT_EPSILON = 1e-6;
    
    /** Default max unchanged iterations we will allow before terminating the algorithm */
    public static final int DEFAULT_MAX_UNIMPROVED = 100;
    
    /** Default max iterations before we terminate the algorithm */
    public static final int DEFAULT_MAX_ITERATIONS = 1_000;
    
    /** A {@link RealQRDecomposition QR} {@link DecompositionSolver} factory.<br>
     * <br>
     * 
     * Much faster than computing a {@link SingularValueDecomposition}, but it can be less robust against singularities
     * in the {@code J^T W J} matrix.<br>
     * <br>
     * 
     * In addition, testing shows this outperforming the SVD in minimizing the 2-norm of the residuals when the problem
     * doesn't have an exact solution. Whether this has basis in theory or is simply an artifact of implementation, I
     * (RM) am unsure.<br>
     * <br>
     * 
     * In case of singularities, it's worth considering both {@link #SVD_SOLVER} and a different
     * {@link RealQRDecomposition} mode: {@code A -> RealQRDecomposition.of(A, true, true)}. This alternative will
     * adjust singularities to something extremely small so that the {@link RealQRDecomposition#solve(Matrix)} methods
     * may be used. This is what's done in <em>Inverse Iteration</em> when computing Eigenvectors where
     * near-singularities are deliberately introduced (see {@link ComplexSchurDecomposition#computeEigenvectors}). */
    public static final Function<RealMatrix, DecompositionSolver> QR_SOLVER = RealQRDecomposition::of;
    
    /** An {@link SingularValueDecomposition SVD} {@link DecompositionSolver} factory.<br>
     * <br>
     * 
     * The {@link SingularValueDecomposition} <em>should</em> be more robust against possible singularities in the
     * {@code J^T W J} matrix, but it's much slower than {@link RealQRDecomposition}. */
    public static final Function<RealMatrix, DecompositionSolver> SVD_SOLVER =
            A -> new SingularValueDecomposition(A).getSolver();
    
    /** The default {@link DecompositionSolver} factory (the same as {@link #QR_SOLVER}) */
    public static final Function<RealMatrix, DecompositionSolver> DEFAULT_DECOMP_SOLVER = QR_SOLVER;
    
    /** The weights {@link Matrix} to consider */
    private final Matrix weights;
    
    /** If the sum of the squares of the residuals is less than this, we declare convergence */
    private final double epsilonSqr;
    
    /** Max number of iterations to consider during solving */
    private final int maxIterations;
    
    /** The max allowable iterations where the solution is unimproved */
    private final int maxUnimprovedIters;
    
    /** A {@link Function} that maps {@link RealMatrix matrices} to {@link DecompositionSolver}s that we can use to
     * solve systems of linear equations */
    private final Function<RealMatrix, DecompositionSolver> decompSolverFactory;
    
    /** @param outputDim the output dimension of the function under consideration in the
     *        {@link WeightedLeastSquaresProblem} for which we are creating a {@link WeightedLeastSquaresConstants}
     * @param weightsParam the weights {@link Matrix} to consider
     * @param epsilonParam if the sum of the squares of the residuals is less than this squared, we declare convergence
     * @param maxIterationsParam the number of iterations to consider during solving
     * @param maxUnimprovedItersParam the max allowable iterations where the solution is unimproved
     * @param decompSolverFactory a {@link Function} that maps {@link RealMatrix matrices} to
     *        {@link DecompositionSolver}s that we can use to solve systems of linear equations. If {@code null}, we use
     *        {@link #DEFAULT_DECOMP_SOLVER}. */
    public WeightedLeastSquaresConstants(final int outputDim,
                                         final double[] weightsParam,
                                         final Double epsilonParam,
                                         final Integer maxIterationsParam,
                                         final Integer maxUnimprovedItersParam,
                                         final Function<RealMatrix, DecompositionSolver> decompSolverFactory) {
        double[] weightsArray = Optional.ofNullable(weightsParam)
                                        .orElseGet(() -> Numerics.getFilledArray(outputDim, 1.0))
                                        .clone();
        this.weights = Matrix.diagonal(weightsArray);
        
        double epsilon = Optional.ofNullable(epsilonParam).orElse(DEFAULT_EPSILON);
        this.epsilonSqr = epsilon * epsilon;
        
        this.maxIterations      = Optional.ofNullable(maxIterationsParam).orElse(DEFAULT_MAX_ITERATIONS);
        this.maxUnimprovedIters = Optional.ofNullable(maxUnimprovedItersParam).orElse(DEFAULT_MAX_UNIMPROVED);
        
        Numerics.requireSquare(this.weights);
        
        this.decompSolverFactory = Optional.ofNullable(decompSolverFactory).orElse(DEFAULT_DECOMP_SOLVER);
    }
    
    /** @return the weights {@link Matrix} */
    public Matrix getWeights() {
        return weights;
    }
    
    /** @return the square of the epsilon requirement */
    public double getEpsilonSqr() {
        return epsilonSqr;
    }
    
    /** @return the max iterations to run */
    public int getMaxIterations() {
        return maxIterations;
    }
    
    /** @return the max allowable unimproving iterations */
    public int getMaxUnimprovedIters() {
        return maxUnimprovedIters;
    }
    
    /** @return a {@link Function} that maps {@link RealMatrix matrices} to {@link DecompositionSolver}s that we can use
     *         to solve systems of linear equations */
    public Function<RealMatrix, DecompositionSolver> getDecompositionSolverFactory() {
        return this.decompSolverFactory;
    }
}
