/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.UnaryOperator;

import ptolemaeus.math.commons.Numerics;

/**
 * A solution to a {@link WeightedLeastSquaresProblem}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class WeightedLeastSquaresSolution implements Comparable<WeightedLeastSquaresSolution> {
    
    /**
     * The point at which we are evaluating the residuals of a function
     */
    private final double[] point;
    
    /**
     * The residuals of a function at `point`
     */
    private final double[] residuals;
    
    /**
     * The sum of the squares of the residuals
     */
    private final double sumSqrResiduals;
    
    /**
     * Set before returning from the algorithm
     */
    private StopType stopType;
    
    /**
     * Set before returning from the algorithm iff it converged
     */
    private Long iterationsToStop;
    
    /**
     * @param f the function which we are minimizing
     * @param pointParam the point at which we will evaluate 'f'
     * @param targetPoint the desired value of 'f'
     */
    public WeightedLeastSquaresSolution(final UnaryOperator<double[]> f,
                                        final double[] pointParam,
                                        final double[] targetPoint) {
        this.point = Objects.requireNonNull(pointParam, "pointParam must be specified").clone();
        this.residuals = Numerics.getResiduals(f, pointParam, targetPoint);
        this.sumSqrResiduals = Numerics.sumSq(this.residuals);
    }
    
    /**
     * @return the point of this
     */
    public double[] getPoint() {
        return this.point.clone();
    }
    
    /**
     * @return the residuals of this
     */
    public double[] getResiduals() {
        return this.residuals.clone();
    }
    
    /**
     * @return the sum of the squares of the residuals of this
     */
    public double getSumSquareResiduals() {
        return this.sumSqrResiduals;
    }
    
    /**
     * @return the {@link StopType} of this {@link WeightedLeastSquaresSolution}
     */
    public StopType getStopType() {
        return this.stopType;
    }
    
    /**
     * @param stopTypeParam the {@link StopType} to set
     */
    public void setStopType(final StopType stopTypeParam) {
        this.stopType = stopTypeParam;
    }
    
    /**
     * set the number of iterations it took to converge if the algorithm stop, whether it converged or not
     * @param iterationsToStop the number of iterations it took to stop, , whether it converged or not
     */
    public void setIterationsToStop(final long iterationsToStop) {
        this.iterationsToStop = iterationsToStop;
    }
    
    /** @return the iterations it took to solve the problem to which this is a solution or {@link Long#MAX_VALUE} if
     *         unable to converge */
    public long getIterationsToStop() {
        return Optional.ofNullable(this.iterationsToStop).orElse(Long.MAX_VALUE);
    }
    
    @Override
    public int compareTo(final WeightedLeastSquaresSolution other) {
        if (other == null) {
            return -1;
        }
        
        return Double.compare(this.sumSqrResiduals, other.sumSqrResiduals);
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (this.stopType != null) {
            builder.append(String.format("%24s,", this.stopType.getMessage()));
            final String formatString;
            if (this.stopType.equals(StopType.EPSILON_CONVERGENCE)) {
                formatString = " %d iterations to converge, ";
            }
            else {
                formatString = " %d iterations to stop, ";
            }
            builder.append(String.format(formatString, this.iterationsToStop));
        }
        builder.append(
                String.format(
                        "point: %s, residuals: %s, sumSqRes: %.12f",
                        Numerics.doubleArrayToString(this.point, 10),
                        Numerics.doubleArrayToString(this.residuals, 10),
                        this.sumSqrResiduals));
        return builder.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(this.residuals), this.sumSqrResiduals);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final WeightedLeastSquaresSolution other = (WeightedLeastSquaresSolution) obj;
        return Arrays.equals(this.residuals, other.residuals)
               && Double.doubleToLongBits(this.sumSqrResiduals) == Double.doubleToLongBits(other.sumSqrResiduals);
    }
}