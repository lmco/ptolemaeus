/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

import java.util.Objects;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.VectorField;

/**
 * A class to hold a representation of a weighted least squares problem.  
 * 
 * @param <T> A type of {@link WeightedLeastSquaresConstants}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class WeightedLeastSquaresProblem<T extends WeightedLeastSquaresConstants> {
    
    /**
     * The model function whose difference from the target values we want to minimize
     */
    private final VectorField f;
    
    /** the Jacobian {@link MultivariateMatrixValuedFunction} of {@link #getF() f} */
    private final MultivariateMatrixValuedFunction jacobian;
    
    /**
     * The target values we want the output of f to match
     */
    private final double[] targetValues;
    
    /**
     * The initial guess for the parameters of f
     */
    private final double[] initialGuess;
    
    /**
     * The {@link WeightedLeastSquaresConstants} of this {@link WeightedLeastSquaresProblem}
     */
    private final T constants;
    
    /**
     * Constructor
     * 
     * @param fParam the {@link VectorField} we are optimizing
     * @param jacobian the Jacobian {@link MultivariateMatrixValuedFunction} of {@link #getF() f}
     * @param initialGuessParam initial guess for the parameters of f
     * @param targetValuesParam target values we want the output of f to match
     * @param constantsParam {@link WeightedLeastSquaresConstants} of this {@link WeightedLeastSquaresProblem}
     */
    public WeightedLeastSquaresProblem(final VectorField fParam,
                                       final MultivariateMatrixValuedFunction jacobian,
                                       final double[] initialGuessParam,
                                       final double[] targetValuesParam,
                                       final T constantsParam) {
        this.f            = Objects.requireNonNull(fParam, "the function may not be null");
        this.targetValues = Objects.requireNonNull(targetValuesParam, "target values may not be null").clone();
        this.initialGuess = Objects.requireNonNull(initialGuessParam, "initial guess may not be null").clone();
        this.constants    = Objects.requireNonNull(constantsParam, "constants may not be null");
        this.jacobian     = Objects.requireNonNull(jacobian, "A Jacobian function must be specified");
        
        this.f.validateDimensions(this.initialGuess);
        
        Validation.requireEqualsTo(this.targetValues.length,
                                   this.f.getOutputDimension(),
                                   () -> ("The length of the target values array (%d)"
                                          + " must be the same as the output"
                                          + " dimension of the function (%d)").formatted(this.targetValues.length,
                                                                                         this.f.getOutputDimension()));
        
        Validation.requireEqualsTo(this.targetValues.length,
                                   this.constants.getWeights().getColumnDimension(),
                                   () -> ("The length of the target values array (%d)"
                                          + " must be the same as the dimension"
                                          + " of the weight matrix (%d)").formatted(this.targetValues.length,
                                                                                    this.constants.getWeights()
                                                                                              .getColumnDimension()));
    }
    
    /**
     * @return the function which we want to match {@link #getTargetValues()} as closely as possible
     */
    public VectorField getF() {
        return f;
    }
    
    /** @return the Jacobian {@link MultivariateMatrixValuedFunction} of {@link #getF() f} */
    public MultivariateMatrixValuedFunction getJacobian() {
        return this.jacobian;
    }
    
    /**
     * @return  the values we want the function to match
     */
    public double[] getTargetValues() {
        return targetValues.clone();
    }
    
    /**
     * @return the initial guess at a solution
     */
    public double[] getInitialGuess() {
        return initialGuess.clone();
    }
    
    /**
     * @return the {@link WeightedLeastSquaresConstants} of this {@link WeightedLeastSquaresProblem}
     */
    public T getConstants() {
        return this.constants;
    }
}
