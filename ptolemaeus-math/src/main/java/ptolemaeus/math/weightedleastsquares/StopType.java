/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

/**
 * An enum which identifies the way an approximation iteration stopped. The order of declaration here matters because we
 * need to compare {@link StopType}s and the natural ordering of the values of an enum is the same as the order of
 * declaration of those values. We want {@link #EPSILON_CONVERGENCE} greater than {@link #UNIMPROVED_CONVERGENCE}
 * greater than {@link #MAX_ITERATIONS}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public enum StopType {
    
    /**
     * {@link StopType#MAX_ITERATIONS} means we stopped after hitting the max allowable number of iterations.
     * This is least optimal as it implies that we were still improving out solutions even far into the search
     * indicating very slow convergence.
     */
    MAX_ITERATIONS("Max iterations limit met"),
    
    /**
     * {@link StopType#UNIMPROVED_CONVERGENCE} means we stopped because we hit the max unimproved steps: we
     * iterated the algorithm without improvement for some number of iterations. This means we have gotten stuck
     * in an optimum (local or global).
     */
    UNIMPROVED_CONVERGENCE("Max unimproved iterations reached: stuck in local optimum"),
    
    /** {@link StopType#LAMBDA_LIMIT} means that we stopped because the Levenberg parameter, denoted using lambda, 
     * reached zero or infinity. */
    LAMBDA_LIMIT("The Levenberg parameter - lambda - equaled 0 or infinity"),
    
    /**
     * {@link StopType#EPSILON_CONVERGENCE} means that we stopped because the sum of the squares of the
     * residuals met the epsilon requirement. This is optimal.
     */
    EPSILON_CONVERGENCE("Epsilon requirement met"),
    
    /**
     * {@link StopType#ERROR} means that we stopped because there was an error/an exception was thrown and
     * we could not recover
     */
    ERROR("There was an error from which we could not recover");
    
    /**
     * the message associated with this {@link StopType}
     */
    private final String message;
    
    /** @param messageParam the message to include with the {@link StopType} */
    StopType(final String messageParam) {
        this.message = messageParam;
    }
    
    /**
     * @return the message associated with this {@link StopType}
     */
    public String getMessage() {
        return message;
    }
}
