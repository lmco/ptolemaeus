/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresConstants;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresProblem;

/**
 * A class to hold the constants needed when using {@link LevenbergMarquardt}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public final class LevenbergMarquardtConstants extends WeightedLeastSquaresConstants {
    
    /** The default {@link #getAlpha()} value for well-behaved/easy problems */
    public static final double DEFAULT_ALPHA_EASY = 0.75;
    
    /** The default {@link #getAlpha()} value for poorly-behaved/hard problems */
    public static final double DEFAULT_ALPHA_HARD = 0.1;
    
    /** the default {@link #getH() h} value */
    public static final double DEFAULT_H = 0.1;
    
    /**
     * The initial conditions of the lambda in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d = J<sup>T</sup>WR when
     * computing the "next step" in the Levenberg-Marquardt Algorithm
     */
    private final LevenbergMarquardtParameter lambda;
    
    /** @see #getAlpha() */
    private final double alpha;
    
    /** @see #getH() */
    private final double directionalDerivativeH;
    
    /** @see #getHSqrd() */
    private final double hSqrd;
    
    /** @param outputDim the output dimension of the function under consideration in the
     *        {@link WeightedLeastSquaresProblem} for which we are creating a {@link LevenbergMarquardtConstants}
     * @param weightsParam the weights {@link Matrix} to consider
     * @param epsilonParam if the sum of the squares of the residuals is less than this squared, we declare convergence
     * @param maxIterationsParam the number of iterations to consider during solving
     * @param maxUnimprovedItersParam the max allowable iterations where the solution is unimproved
     * @param decompSolverFactory a {@link Function} that maps {@link RealMatrix matrices} to
     *        {@link DecompositionSolver}s that we can use to solve systems of linear equations. If {@code null}, we use
     *        {@link WeightedLeastSquaresConstants#DEFAULT_DECOMP_SOLVER}.
     * @param lambda the initial conditions of the lambda in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d =
     *        J<sup>T</sup>WR when computing the "next step" in the Levenberg-Marquardt Algorithm
     * @param alpha the {@code alpha} value. See {@link #getAlpha()}. The authors in the linked paper have found that
     *        0.1 is a good value for problematic problems and 0.75 is good in general for well-behaved problems.
     * @param h the directional derivative {@code h}. The authors of the paper link in {@link #getAlpha()} have found
     *        0.1 works well in general. */
    // CHECKSTYLE.OFF: ParameterNumber
    public LevenbergMarquardtConstants(final int outputDim,
                                       final double[] weightsParam,
                                       final Double epsilonParam,
                                       final Integer maxIterationsParam,
                                       final Integer maxUnimprovedItersParam,
                                       final Function<RealMatrix, DecompositionSolver> decompSolverFactory,
                                       final LevenbergMarquardtParameter lambda,
                                       final double alpha,
                                       final double h) {
        super(outputDim,
              weightsParam,
              epsilonParam,
              maxIterationsParam,
              maxUnimprovedItersParam,
              decompSolverFactory);
        this.lambda                 = Objects.requireNonNull(lambda, "lambda must be specified");
        this.alpha                  = Validation.requireGreaterThan(alpha, 0.0, "alpha");
        this.directionalDerivativeH = Validation.requireGreaterThan(h, 0.0, "h");
        this.hSqrd                  = h * h;
    }
    // CHECKSTYLE.ON: ParameterNumber
    
    /**
     * Get a simple {@link LevenbergMarquardtConstants}
     * 
     * @param outputDim the output dimension of the function under consideration in the
     *        {@link WeightedLeastSquaresProblem} for which we are creating a {@link LevenbergMarquardtConstants}
     * @return the new {@link LevenbergMarquardtConstants}
     */
    public static LevenbergMarquardtConstants getSimple(final int outputDim) {
        final double[] weights = getDoubleArrayFilled(outputDim, 1.0);
        return new LevenbergMarquardtConstants(outputDim,
                                               weights,
                                               null, // epsilon                   (null -> use default)
                                               null, // max total iterations      "
                                               null, // max unimproved iterations "
                                               LevenbergMarquardtConstants.DEFAULT_DECOMP_SOLVER,
                                               LevenbergMarquardtParameter.getDefault(),
                                               DEFAULT_ALPHA_EASY,
                                               DEFAULT_H);
    }
    
    /**
     * Get an array with the specified size filled with the specified value
     * 
     * @param size the size of the array to create
     * @param value the values with with to fill the array
     * @return the new array
     */
    static double[] getDoubleArrayFilled(final int size, final double value) {
        final double[] answer = new double[size];
        Arrays.fill(answer, value);
        return answer;
    }
    
    /**
     * @return the initial conditions of the lambda in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d =
     *         J<sup>T</sup>WR when computing the "next step" in the Levenberg-Marquardt Algorithm
     * @implNote this method returns the result of calling {@link LevenbergMarquardtParameter#freshCopy()} on this
     *           instances instance of {@link LevenbergMarquardtParameter}
     */
    public LevenbergMarquardtParameter getLambda() {
        return this.lambda.freshCopy();
    }
    
    /** We will only accept a step, dTheta, if {@code alpha <= 2 * |dTheta2| / |dTheta1|}, where
     * {@code dTheta = dTheta1 + dTheta2}. See equation (15) of <a href="https://arxiv.org/abs/1201.5885">Improvements
     * to the Levenberg-Marquardt Algorithm for Nonlinear Least-Squares Minimization</a> for details.
     * 
     * @return alpha */
    public double getAlpha() {
        return this.alpha;
    }
    
    /** @return the {@code h} value to use when computing the directional derivative */
    public double getH() {
        return this.directionalDerivativeH;
    }
    
    /** @return {@link #getH() h} * {@link #getH() h} */
    double getHSqrd() {
        return this.hSqrd;
    }
}
