/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.Pair;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.realdomains.NPoint;
import ptolemaeus.math.weightedleastsquares.StopType;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresSolution;

// CHECKSTYLE.OFF: LineLength
/** A class to perform the Levenberg-Marquardt method for non-linear least squares minimization using a function
 * <p>
 * f : R<sup>N</sup> -&gt; R<sup>M</sup>
 * </p>
 * at provided target values
 * <p>
 * Sources:
 * </p>
 * <ul>
 * <li>[1] <a href="https://arxiv.org/abs/1201.5885">Improvements to the Levenberg-Marquardt Algorithm for Nonlinear
 * Least-Squares Minimization</a></li>
 * <li>[2] <a href="https://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm">Levenberg-Marquardt
 * Algorithm</a></li>
 * </ul>
 * Algorithm description
 * <p>
 * As with all non-linear least-squares optimization methods, we seek to minimize the sum of the square of the residuals
 * (SSR) of a function f : R<sup>N</sup> -&gt; R<sup>M</sup> with an initial input guess and a target value. the
 * Levenberg-Marquardt algorithm uses a damping parameter, &lambda;, to modify stepping size and interpolate between
 * gradient descent and Gauss-Newton.
 * </p>
 * <p>
 * The damping parameter comes into play when solving for d in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d =
 * J<sup>T</sup>WR.
 * </p>
 * <p>
 * When we compute a "next step" - x<sub>n+1</sub> if the current step is x<sub>n</sub> - the SSR may be equal to, less
 * than, or greater than what is was at x<sub>n</sub>. Typically, Weighted Least Squares optimizations accept only steps
 * which decrease the SSR, but this often will push the algorithms into local optima where they will can stuck. To avoid
 * getting stuck in local optima, the algorithm should, on occasion, be allowed to accept steps which increase the SSR.
 * To do this, we want to accept SSR-increasing steps if the the angle between the previous and current proposed
 * next-step vectors is acute and the increase is not too great.
 * </p>
 * <p>
 * The proposed, and implemented, criterion for deciding whether to accept a step is as follows:
 * </p>
 * <p>
 * If the step decreases the SSR, then we accept it.
 * </p>
 * <p>
 * Otherwise, we accept the step if (1 - B<sub>i</sub>) * C<sub>i+1</sub> &lt;= C<sub>i</sub>, where C<sub>i</sub> is
 * the cost - the SSR - at step i and B<sub>i</sub> is the cosine of the angle between the proposed next-step and
 * previous next-step vectors.
 * </p>
 * <p>
 * Regarding &lambda;, we increase it when we reject a solution and decrease it when we accept a solution
 * </p>
 * <p>
 * See section 4 of [1] for detailed justification of our "next step" acceptance and section 2.1 for a discussion on
 * increasing/decreasing lambda.
 * </p>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class LevenbergMarquardt {
    
    /** A {@link Logger} for logging */
    private static final Logger LOGGER = LogManager.getLogger(LevenbergMarquardt.class);
    
    /** The {@link LevenbergMarquardtConstants} we need while solving */
    private final LevenbergMarquardtConstants constants;
    
    /** The target values of {@link #f}. We store this so that we don't need to pay the {@code .clone} performance tax
     * when getting the array from {@link #constants} */
    private final double[] targetValues;
    
    /** The function for which we need to find a solution */
    private final VectorField f;
    
    /** The jacobian of {@link #f}. We store this so we only have to compute it once */
    private final MultivariateMatrixValuedFunction jacobian;
    
    /** An incrementor we use to keep track of the number of unimproved steps */
    private final AtomicInteger unimprovedInc;
    
    /** An incrementor we use to keep track of the number of iterations */
    private final AtomicInteger iterationsInc;
    
    /** The {@link LevenbergMarquardtParameter}, what is usually denoted &lambda; in the literature. We maintain this
     * variable rather than use {@link LevenbergMarquardtConstants#getLambda()} because it returns a copy, so it
     * wouldn't track updates due to every call returning a new instance. */
    private final LevenbergMarquardtParameter lambda;
    
    /** A "best so far" {@link WeightedLeastSquaresSolution}s {@link Consumer} */
    private final Consumer<WeightedLeastSquaresSolution> bestSoFarConsumer;
    
    /** A {@link Consumer} for the final - best - {@link WeightedLeastSquaresSolution}. This will be called exactly
     * called once at final very end of the optimization. */
    private final Consumer<WeightedLeastSquaresSolution> finalBestConsumer;
    
    /** The best solution we've found so far. We keep track of this because we allow the algorithm to accept worsening
     * steps occasionally. We have to be sure to keep track of the best-so-far to ensure that, if the algorithm
     * diverges, we can still return a reasonable value. */
    private WeightedLeastSquaresSolution currentBest;
    
    /** Set-up the {@link LevenbergMarquardt} instance with a {@link LevenbergMarquardtProblem}
     * 
     * @param lmProblem the {@link LevenbergMarquardtProblem} we need to solve
     * @param bestSoFarConsumer this {@link Consumer} will be used to consume the best solution every time a new best
     *        solution is found. May be {@code null}.
     * @param finalBestConsumer a {@link Consumer} for the final - best - {@link WeightedLeastSquaresSolution}. This
     *        will be called exactly once at final very end of the optimization. May be {@code null}. */
    LevenbergMarquardt(final LevenbergMarquardtProblem lmProblem,
                       final Consumer<WeightedLeastSquaresSolution> bestSoFarConsumer,
                       final Consumer<WeightedLeastSquaresSolution> finalBestConsumer) {
        Objects.requireNonNull(lmProblem, "cannot solve a null problem");
        this.constants         = lmProblem.getConstants();
        this.targetValues      = lmProblem.getTargetValues();
        this.f                 = lmProblem.getF();
        this.jacobian          = lmProblem.getJacobian();
        this.unimprovedInc     = new AtomicInteger(0);
        this.iterationsInc     = new AtomicInteger(0);
        this.lambda            = this.constants.getLambda();
        this.currentBest       = null;
        this.bestSoFarConsumer = bestSoFarConsumer == null ? ignrd -> { } : bestSoFarConsumer;
        this.finalBestConsumer = finalBestConsumer == null ? ignrd -> { } : finalBestConsumer;
        
        // set first currentBest solution
        final WeightedLeastSquaresSolution first = new WeightedLeastSquaresSolution(this.f,
                                                                                    lmProblem.getInitialGuess(),
                                                                                    this.targetValues);
        this.possiblyUpdateCurrentBest(first);
    }
    
    /** Solve the least squares problem using the Levenberg-Marquardt method. Do not log exceptions.
     * 
     * @param lmProblem the {@link LevenbergMarquardtProblem} to solve
     * @return the solution to the least squares problem */
    public static WeightedLeastSquaresSolution solve(final LevenbergMarquardtProblem lmProblem) {
        return solve(lmProblem, false);
    }
    
    /** Solve the least squares problem using the Levenberg-Marquardt method.
     * 
     * @param lmProblem the {@link LevenbergMarquardtProblem} to solve
     * @param logExceptions if true, then exceptions get logged in {@link #LOGGER}; if false, then they are not
     * @return the solution to the least squares problem */
    public static WeightedLeastSquaresSolution solve(final LevenbergMarquardtProblem lmProblem,
                                                     final boolean logExceptions) {
        return new LevenbergMarquardt(lmProblem, ignored -> { }, ignored -> { }).solve(logExceptions);
    }
    
    /** Solve the least squares problem using the Levenberg-Marquardt method.
     * 
     * @param lmProblem the {@link LevenbergMarquardtProblem} to solve
     * @param bestSoFarConsumer this {@link Consumer} will be used to consume the best solution every time a new best
     *        solution is found. May be {@code null}.
     * @param finalBestConsumer a {@link Consumer} for the final - best - {@link WeightedLeastSquaresSolution}. This
     *        will be called exactly once at final very end of the optimization. May be {@code null}.
     * @param logExceptions if true, then exceptions get logged in {@link #LOGGER}; if false, then they are not
     * @return the solution to the least squares problem */
    public static WeightedLeastSquaresSolution solve(final LevenbergMarquardtProblem lmProblem,
                                                     final Consumer<WeightedLeastSquaresSolution> bestSoFarConsumer,
                                                     final Consumer<WeightedLeastSquaresSolution> finalBestConsumer,
                                                     final boolean logExceptions) {
        return new LevenbergMarquardt(lmProblem, bestSoFarConsumer, finalBestConsumer).solve(logExceptions);
    }
    
    /** Find the inputs {@code x} for {@link MultivariateVectorValuedFunction f} s.t.
     * ||{@code f(x) - targetValues}||<sub>2</sub> is minimal; i.e., solve the given non-linear system and, if we can't
     * find a solution, minimize the Euclidean norm of the error vector.<br>
     * <br>
     * 
     * This is the quickest way to use this class, there is much that can be tweaked using the more complex
     * {@code solve} methods; e.g., {@link #solve(LevenbergMarquardtProblem, Consumer, Consumer, boolean)} <br>
     * <br>
     * 
     * This simple construction should be enough for most applications, but to maximize performance, you'll need to
     * create a {@link LevenbergMarquardtProblem} with custom tweaked {@link LevenbergMarquardtConstants} explicitly.
     * E.g., <br>
     * <br>
     * 
     * <ul>
     * <li>{@link LevenbergMarquardtConstants#getMaxIterations() max iterations},</li>
     * <li>{@link LevenbergMarquardtConstants#getMaxUnimprovedIters() max unimproved iterations},</li>
     * <li>{@link LevenbergMarquardtConstants#getDecompositionSolverFactory() the decomposition factory},</li>
     * <li>{@link LevenbergMarquardtConstants#getAlpha() alpha},</li>
     * <li>{@link LevenbergMarquardtConstants#getH() h}, etc.</li>
     * </ul>
     * 
     * @param f search for optimal inputs to this function
     * @param initialGuess an initial guess
     * @param targetValues the desired output vector
     * 
     * @return the solution to the least squares problem */
    public static WeightedLeastSquaresSolution solve(final MultivariateVectorValuedFunction f,
                                                     final double[] initialGuess,
                                                     final double[] targetValues) {
        final LevenbergMarquardtProblem simple =
                LevenbergMarquardtProblem.getSimple(f, initialGuess, targetValues, Parallelism.SEQUENTIAL);
        return new LevenbergMarquardt(simple, null, null).solve(false);
    }
    
    /** Solve the least squares problem using the Levenberg-Marquardt method.
     * 
     * @param logExceptions if true, then exceptions get logged in {@link #LOGGER}; if false, then they are not
     * @return the solution to the least squares problem */
    WeightedLeastSquaresSolution solve(final boolean logExceptions) {
        WeightedLeastSquaresSolution xn = this.currentBest;
        NVector xnDecStep = null;
        while (true) {
            // Start end-check block.  Decide whether to keep iterating.
            final StopType stopType;
            if (this.currentBest.getSumSquareResiduals() <= this.constants.getEpsilonSqr()) {
                LOGGER.trace("Stopping a Levenberg-Marquardt optimization because we converged");
                stopType = StopType.EPSILON_CONVERGENCE;
            }
            else if (Double.isInfinite(this.lambda.get()) || this.lambda.get() == 0.0) {
                LOGGER.debug("Stopping a Levenberg-Marquardt optimization because lambda equaled 0 or infinity");
                stopType = StopType.LAMBDA_LIMIT;
            }
            else if (this.iterationsInc.get() >= this.constants.getMaxIterations()) {
                LOGGER.trace("Stopping a Levenberg-Marquardt because we hit the max iterations limit");
                stopType = StopType.MAX_ITERATIONS;
            }
            else if (this.unimprovedInc.get() >= this.constants.getMaxUnimprovedIters()) {
                LOGGER.trace("Stopping a Levenberg-Marquardt because we hit the max unimproved iterations limit");
                stopType = StopType.UNIMPROVED_CONVERGENCE;
            }
            else {
                stopType = null;
            }
            
            if (stopType != null) {
                this.currentBest.setStopType(stopType);
                this.currentBest.setIterationsToStop(this.iterationsInc.get());
                break;
            }
            // Finish end-check block.  If we make it here, we'll do at least one more iteration.
            
            this.iterationsInc.incrementAndGet();
            // Notice that xnP1Pair is not final.  This is because it might be reset in the following if-block.  Be careful.
            Pair<WeightedLeastSquaresSolution, NVector> xnP1Pair = calculateNextPoint(xn, logExceptions);
            if (xnDecStep == null && xnP1Pair == null) {
                xnP1Pair = handleFirstIterationError(logExceptions);
                
                if (xnP1Pair == null) { /* this is a special error condition that we check outside of the end-check
                                         * block (see the start of the outer while-loop) */
                    xn.setStopType(StopType.ERROR);
                    break;
                }
            }
            
            if (xnP1Pair == null) {
                this.unimprovedInc.incrementAndGet();
                this.lambda.scaleUp(); // the first of two places that we can call scaleUp (aside from handleFirstIterationError)
                continue;
            }
            
            final boolean acceptXnP1;
            final WeightedLeastSquaresSolution xnP1 = xnP1Pair.getFirst();
            if (xnP1.getSumSquareResiduals() >= xn.getSumSquareResiduals()) {
                this.unimprovedInc.incrementAndGet();
                
                final NVector xnP1DecStep = xnP1Pair.getSecond();
                if (xnDecStep == null) {
                    acceptXnP1 = true;
                }
                else { // See the class-level JavaDoc and the linked paper for explanation
                    final double scaledXnP1SSR = (1.0 - xnP1DecStep.cosAngleTo(xnDecStep)) * xnP1.getSumSquareResiduals();
                    acceptXnP1 = scaledXnP1SSR <= xn.getSumSquareResiduals();
                }
            }
            else {
                if (xnP1.getSumSquareResiduals() <= this.currentBest.getSumSquareResiduals()) {
                    this.unimprovedInc.set(0);
                }
                
                acceptXnP1 = true;
            }
            
            if (acceptXnP1) {
                xn = xnP1;
                xnDecStep = xnP1Pair.getSecond();
                this.possiblyUpdateCurrentBest(xnP1); // the only place aside from the first call where we call update (aside from handleFirstIterationError)
                this.lambda.scaleDown(); // the only place that we can call scaleDown (aside from handleFirstIterationError)
            }
            else {
                this.lambda.scaleUp(); // the second of two places that we can call scaleUp (aside from handleFirstIterationError)
            }
            
            LOGGER.trace(() -> getTraceMessage(xnP1)); 
        }
        
        this.finalBestConsumer.accept(this.currentBest);
        return this.currentBest;
    }
    
    /**
     * <p>
     * Handle a first-iteration error.
     * </p>
     * <p>
     * If we're on the first iteration and {@code xnP1Pair} is {@code null}, then we have a special case
     * (first-iteration failure) where we must try some unorthodox means to work our way to an initial "next step." To
     * do this, we try finding a {@code lambda} that works by modifying lambda outside of the standard procedure (i.e.,
     * depending on whether we improved/got worse).
     * </p>
     * 
     * @param logExceptions {@code true} if we want to log exceptions, {@code false} otherwise.
     * @return a possibly updated {@code Pair<WeightedLeastSquaresSolution, NVector>} solution pair */
    Pair<WeightedLeastSquaresSolution, NVector> handleFirstIterationError(final boolean logExceptions) {
        Pair<WeightedLeastSquaresSolution, NVector> xnP1Pair = null;
        while (xnP1Pair == null && this.lambda.get() > 0) {
            this.lambda.scaleDown();
            xnP1Pair = calculateNextPoint(this.currentBest, logExceptions);
        }
        
        if (xnP1Pair == null) {
            this.lambda.reset();
        }
        
        while (xnP1Pair == null && this.lambda.get() < Double.POSITIVE_INFINITY) {
            this.lambda.scaleUp();
            xnP1Pair = calculateNextPoint(this.currentBest, logExceptions);
        }
        
        return xnP1Pair;
    }
    
    /** Return the status message for trace-level logging
     * 
     * @param xnP1 current solution
     * @return the status message */
    String getTraceMessage(final WeightedLeastSquaresSolution xnP1) {
        return String.format("Best SSR: %.20f, current SSR: %.20f, lambda: %.20f, iters: %d, unimprovedIters: %d",
                             this.currentBest.getSumSquareResiduals(),
                             xnP1.getSumSquareResiduals(),
                             this.lambda.get(),
                             this.iterationsInc.get(),
                             this.unimprovedInc.get());
    }
    
    /** Possibly update {@link #currentBest} with the next-step solution. If {@link #currentBest} == null or the SSR of
     * the solution at step x<sub>n+1</sub> is less than the SSR of {@link #currentBest}, replace {@link #currentBest}
     * with the solution at step x<sub>n+1</sub>
     * 
     * @param xnP1 the solution at step x<sub>n+1</sub> */
    private void possiblyUpdateCurrentBest(final WeightedLeastSquaresSolution xnP1) {
        if (this.currentBest == null || xnP1.compareTo(this.currentBest) < 0) {
            this.currentBest = xnP1;
            this.bestSoFarConsumer.accept(this.currentBest);
        }
    }
    
    /** Using x<sub>n</sub>, calculate x<sub>n+1</sub>. This is where we calculate the vector that will take us to the
     * next step (x<sub>n+1</sub>). If the proposed vector takes us outside of the input domain, we scale it back so
     * that it is contained within the boundary of the input domain.
     * 
     * @param xnSoln the current solution point
     * @param logExceptions if true, then exceptions get logged in {@link #LOGGER}; if false, then they are not
     * @return the next point paired with the stepping vector or {@code null} if the descending step is calculated to be
     *         null or if an exception is thrown. The values contained within the {@link Pair} will never be null. */
    Pair<WeightedLeastSquaresSolution, NVector> calculateNextPoint(final WeightedLeastSquaresSolution xnSoln,
                                                                   final boolean logExceptions) {
        Pair<WeightedLeastSquaresSolution, NVector> answer = null;
        try {
            final NVector descendingStep = computeDescendingStep(xnSoln);
            if (descendingStep != null) {
                final NPoint xn = new NPoint(xnSoln.getPoint());
                final NPoint xnP1 = xn.plus(descendingStep);
                
                NPoint xnP1Adjusted = xnP1;
                if (!this.f.getInputDomain().contains(xnP1)) {
                    xnP1Adjusted = this.f.getInputDomain().getBoundaryPoint(xn, xnP1);
                    LOGGER.trace("Proposed next-step adjusted from {} to {}", xnP1, xnP1Adjusted);
                }
                
                final WeightedLeastSquaresSolution xnP1Soln =
                        new WeightedLeastSquaresSolution(this.f, xnP1Adjusted.getComponents(), this.targetValues);
                
                answer = new Pair<>(xnP1Soln, descendingStep);
            }
        }
        catch (Exception ex) {
            if (logExceptions) {
                LOGGER.catching(ex);
            }
            LOGGER.debug(() -> String.format("There was an exception thrown during Levenberg-Marquardt optimization.  "
                                             + "Current solution: %s",
                                             xnSoln));
        }
        return answer;
    }
    
    /** Compute the descending step, a step that will decrease the SSR, that the algorithm should take:
     * <p>
     * d&theta; = d&theta;<sub>1</sub> + d&theta;<sub>2</sub> (see equation (14) of [1])
     * </p>
     * 
     * where:
     * <ul>
     * <li>d&theta;<sub>1</sub> is the standard Levenberg-Marquardt step (see equation (11) of [1]), and</li>
     * <li>d&theta;<sub>2</sub> is the geodesic acceleration correction (see equations (14) (just above), (18), and (19)
     * of [1])</li>
     * </ul>
     * 
     * @param currSoln the current {@link WeightedLeastSquaresSolution}
     * @return d&theta; = d&theta;<sub>1</sub> + d&theta;<sub>2</sub> (see equation (14) of [1]) */
    private NVector computeDescendingStep(final WeightedLeastSquaresSolution currSoln) {
        final double[] theta           = currSoln.getPoint();
        final Matrix   jacobianOfFAtXn = this.jacobian.apply(theta); // J
        final Matrix   jTransposeW     = jacobianOfFAtXn.transposeMultiply(this.constants.getWeights()); // JT * W
        final Matrix   jTWJDamped      = jTransposeW.multiply(jacobianOfFAtXn); /* JTWJ is only damped once we've
                                                                                 * updated the diagonal below */
        
        // jTWJDamped modified in-place!
        final double currLambda = this.lambda.get();
        for (int i = 0; i < jTWJDamped.getMaxRank(); i++) {
            final double ithDiagOld = jTWJDamped.getEntry(i, i);
            final double ithDiagNew = ithDiagOld + ithDiagOld * currLambda;
            /* Do not simplify the expression of ithDiagNew to
             * 
             * ithDiagNew = ithDiagOld * (1.0 + currLambda)
             * 
             * because, for lambda < machEps / 2, this will cause pre-mature rounding and sub-optimal results! */
            
            jTWJDamped.setEntry(i, i, ithDiagNew);
        }
        // jTWJDamped modified in-place!
        
        final DecompositionSolver solver = this.constants.getDecompositionSolverFactory().apply(jTWJDamped);
        
        final NVector r       = new NVector(currSoln.getResiduals()); // r
        final NVector dTheta1 = computeDTheta(jTransposeW, solver, r);
        final NVector rPPOn2  = computeNegativeHalfDirectionalSecondDerivative(theta, dTheta1); // -r" / 2
        final NVector dTheta2 = computeDTheta(jTransposeW, solver, rPPOn2);
        
        return computeDTheta(dTheta1, dTheta2, this.constants.getAlpha());
    }
    
    /** Compute d&theta; = d&theta;<sub>1</sub> + d&theta;<sub>2</sub> (see equation (14) of [1]). If
     * {@link LevenbergMarquardtConstants#getAlpha() alpha} is less than
     * 
     * <p>
     * 2 * |d&theta;<sub>2</sub>| / |d&theta;<sub>1</sub>|
     * </p>
     * 
     * then we return d&theta;<sub>1</sub>. Else we return d&theta;<sub>1</sub> + d&theta;<sub>2</sub>
     * 
     * @param dTheta1 d&theta;<sub>1</sub> is the standard Levenberg-Marquardt step (see equation (11) of [1])
     * @param dTheta2 d&theta;<sub>2</sub> is the geodesic acceleration correction (see equations (14) (just above),
     *        (18), and (19) of [1])
     * @param alpha the
     * @return If {@link LevenbergMarquardtConstants#getAlpha() alpha} is less than
     *        
     *         <p>
     *         2 * |d&theta;<sub>2</sub>| / |d&theta;<sub>1</sub>|
     *         </p>
     * 
     *         then we return d&theta;<sub>1</sub>. Else we return d&theta;<sub>1</sub> + d&theta;<sub>2</sub> */
    static NVector computeDTheta(final NVector dTheta1, final NVector dTheta2, final double alpha) {
        final double ratio = 2D * dTheta2.getLength() / dTheta1.getLength();
        if (alpha < ratio) { // see equation 15 of [1]
            return dTheta1;
        }
        
        return dTheta1.plus(dTheta2);
    }
    
    /** See just before equation (14) and equations (11), (18), and (19) of [1]
     * 
     * <p>
     * Compute the solution to d&theta; in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d&theta; = J<sup>T</sup>WV
     * </p>
     * <p>
     * Where V is some vector passed in as an argument
     * </p>
     * <p>
     * A note on J<sup>T</sup>WJ:
     * </p>
     * 
     * <pre>
     * The diagonal of the Matrix JTJ (i.e. the Gramian of the Jacobian) is the vector whose ith component is the
     * inner-product of the ith column of the Jacobian with itself:
     * 
     * Diag(Gramian(J(x1, ..., xn))) = (||df/dx1||^2, ..., ||df/dxn||^2)
     * 
     * so the components of this vector tell us about the magnitude and direction of the change in f as we vary xi.
     * 
     * Here, we have a "weighted Gramian" as we are multiplying by a weight matrix, but that just weights the
     * vector.
     * </pre>
     * 
     * See section 2.2 of [1] for an explanation of the damping matrix Diag(J<sup>T</sup>WJ)
     * 
     * @param jTW J<sup>T</sup>W
     * @param jTWJDamped J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ)
     * @param v some vector
     * @return solution to d&theta; in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d&theta; = J<sup>T</sup>WV */
    private static NVector computeDTheta(final Matrix jTW, final DecompositionSolver jTWJDamped, final RealVector v) {
        return NVector.of(jTWJDamped.solve(jTW.operate(v)));
    }
    
    /** Compute (-1/2) times the second directional derivative of {@code r} - {@code r"} - at {@code theta} in the 
     * direction of {@code dTheta1}. Note: the residuals function {@code r = f - y}, where {@code f} is the 
     * objective function and {@code y} is the constant target vector so J<sub>r</sub> = J<sub>f</sub> and, of course, 
     * {@code r" = f"}
     * 
     * See equations (18) and (19) of [1]
     * 
     * @param thetaArr the current solution point data array
     * @param dTheta1 the value of dTheta1. See {@link #computeDescendingStep(WeightedLeastSquaresSolution)}
     * @return the second directional derivative of {@code r}: {@code r"} */
    private NVector computeNegativeHalfDirectionalSecondDerivative(final double[] thetaArr, final NVector dTheta1) {
        final NVector  dTheta1H    = dTheta1.times(this.constants.getH());
        final double[] dTheta1HArr = dTheta1H.getDataRef();
        
        final int n = this.f.getInputDimension();
        final double[] thetaPlusDTheta1H = new double[n];
        final double[] thetaMnusDTheta1H = new double[n];
        for (int i = 0; i < n; i++) {
            thetaPlusDTheta1H[i] = thetaArr[i] + dTheta1HArr[i];
            thetaMnusDTheta1H[i] = thetaArr[i] - dTheta1HArr[i];
        }
        
        final double[] fThetaArr             = this.f.apply(thetaArr);
        final double[] fThetaPlusHDTheta1Arr = this.f.apply(thetaPlusDTheta1H);
        final double[] fThetaMnusHDTheta1Arr = this.f.apply(thetaMnusDTheta1H);
        
        final double twiceHSqrd = 2 * this.constants.getHSqrd();
        
        final int      m      = this.f.getOutputDimension();
        final double[] answer = new double[m];
        for (int i = 0; i < m; i++) {
            final double numerator = fThetaPlusHDTheta1Arr[i] - 2 * fThetaArr[i] + fThetaMnusHDTheta1Arr[i];
            answer[i] = -1 * numerator / twiceHSqrd;
        }
        
        return new NVector(answer);
    }
}
// CHECKSTYLE.ON: LineLength
