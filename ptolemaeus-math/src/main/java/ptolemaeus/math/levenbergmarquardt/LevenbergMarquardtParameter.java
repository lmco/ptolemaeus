/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import ptolemaeus.commons.Validation;

/**
 * A class to manage the lambda parameter in {@link LevenbergMarquardt} when solving for d in (J<sup>T</sup>WJ +
 * &lambda;Diag(J<sup>T</sup>WJ))d = J<sup>T</sup>WR
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class LevenbergMarquardtParameter {
    
    /**
     * The initial lambda value when solving for d in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d =
     * J<sup>T</sup>WR
     */
    private final double originalLambda;
    
    /**
     * We will multiply lambda by this when we call {@link #scaleUp()}
     */
    private final double upScale;
    
    /**
     * We will divide lambda by this when we call {@link #scaleDown()}
     */
    private final double downScale;
    
    /**
     * The current lambda value when solving for d in (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d =
     * J<sup>T</sup>WR
     */
    private double lambda;
    
    /**
     * Constructor
     * 
     * @param initialLambda the initial lambda value
     * @param upScale we will multiply lambda by this when we reject a "next step" in 
     * {@link LevenbergMarquardt#solve(boolean)}
     * @param downScale we will divide lambda by this when we accept a "next step" in 
     * {@link LevenbergMarquardt#solve(boolean)}
     */
    public LevenbergMarquardtParameter(final double initialLambda, final double upScale, final double downScale) {
        Validation.requireGreaterThanEqualTo(initialLambda, 0, "lambda must be greater than zero");
        Validation.requireGreaterThanEqualTo(upScale, 1, "upScale must be greater than or equal to one");
        Validation.requireGreaterThanEqualTo(downScale, 1, "downScale must be greater than or equal to one");
        this.originalLambda = initialLambda;
        this.lambda = initialLambda;
        this.upScale = upScale;
        this.downScale = downScale;
    }
    
    /**
     * @return a copy of this as it was before it was used
     */
    LevenbergMarquardtParameter freshCopy() {
        return new LevenbergMarquardtParameter(this.originalLambda, this.upScale, this.downScale);
    }
    
    /**
     * @return the current lambda value for (J<sup>T</sup>WJ + &lambda;Diag(J<sup>T</sup>WJ))d = J<sup>T</sup>WR
     */
    double get() {
        return this.lambda;
    }
    
    /**
     * Increment the number of times we will scale-up lambda by {@link #upScale}
     */
    void scaleUp() {
        this.lambda *= this.upScale;
    }
    
    /**
     * Increment the number of times we will scale-down lambda by {@link #downScale}
     */
    void scaleDown() {
        this.lambda /= this.downScale;
    }
    
    /**
     * Reset lambda to its original value: {@link #originalLambda}
     */
    void reset() {
        this.lambda = this.originalLambda;
    }
    
    /**
     * @return as default {@link LevenbergMarquardtParameter} that works in most circumstances
     */
    public static LevenbergMarquardtParameter getDefault() {
        return new LevenbergMarquardtParameter(1D, 2D, 3D);
    }
}
