/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import java.util.Optional;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresProblem;

/**
 * A {@link LevenbergMarquardtProblem} is a {@link WeightedLeastSquaresProblem} which requires
 * {@link LevenbergMarquardtConstants}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class LevenbergMarquardtProblem extends WeightedLeastSquaresProblem<LevenbergMarquardtConstants> {
    
    /**
     * Constructor
     * 
     * @param fParam the {@link VectorField} we are optimizing
     * @param jacobian the Jacobian {@link MultivariateMatrixValuedFunction} of {@link #getF() f}
     * @param initialGuessParam initial guess for the parameters of f
     * @param targetValuesParam target values we want the output of f to match
     * @param constantsParam {@link LevenbergMarquardtConstants} of this {@link LevenbergMarquardtProblem}
     */
    public LevenbergMarquardtProblem(final VectorField fParam,
                                     final MultivariateMatrixValuedFunction jacobian,
                                     final double[] initialGuessParam,
                                     final double[] targetValuesParam,
                                     final LevenbergMarquardtConstants constantsParam) {
        super(fParam, jacobian, initialGuessParam, targetValuesParam, constantsParam);
    }
    
    /**
     * Constructor
     * 
     * @param fParam the {@link VectorField} we are optimizing
     * @param initialGuessParam initial guess for the parameters of f
     * @param targetValuesParam target values we want the output of f to match
     * @param constantsParam {@link LevenbergMarquardtConstants} of this {@link LevenbergMarquardtProblem}
     */
    public LevenbergMarquardtProblem(final VectorField fParam,
                                     final double[] initialGuessParam,
                                     final double[] targetValuesParam,
                                     final LevenbergMarquardtConstants constantsParam) {
        super(fParam,
              Numerics.getJacobianFunction(fParam,
                                           Optional.empty(),
                                           initialGuessParam.length,
                                           targetValuesParam.length,
                                           null),
              initialGuessParam,
              targetValuesParam,
              constantsParam);
    }
    
    /** Get a simple {@link LevenbergMarquardtProblem}
     * 
     * @param f the objective function for which we want an optimized input
     * @param initialGuess the initial guess
     * @param targetValues the target values
     * @param parallelism the {@link Parallelism} with which we want to compute the Jacobian. If {@code null}, we
     *        default to {@link Parallelism#SEQUENTIAL} in
     *        {@link Numerics#getJacobianFunction(MultivariateVectorValuedFunction, Optional,
     *                                                         int, int, Parallelism)}.
     * @return the new {@link LevenbergMarquardtProblem}
     *        
     * @apiNote This uses {@link LevenbergMarquardtConstants#getSimple(int)}, which is o.k. when the objective function
     *          is well behaved. In other cases, however, you should specify your own which has been tweaked for your
     *          use-case. */
    public static LevenbergMarquardtProblem getSimple(final MultivariateVectorValuedFunction f,
                                                      final double[] initialGuess,
                                                      final double[] targetValues,
                                                      final Parallelism parallelism) {
        final int iDim = initialGuess.length;
        final int oDim = targetValues.length;
        
        final MultivariateMatrixValuedFunction numericalJacobian =
                Numerics.getJacobianFunction(f, Optional.empty(), iDim, oDim, parallelism);
        
        return new LevenbergMarquardtProblem(new VectorField(f, iDim, oDim),
                                             numericalJacobian,
                                             initialGuess,
                                             targetValues,
                                             LevenbergMarquardtConstants.getSimple(oDim));
    }
}
