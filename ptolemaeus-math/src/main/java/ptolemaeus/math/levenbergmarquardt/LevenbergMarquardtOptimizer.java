/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import static java.util.Objects.requireNonNull;

import java.util.Optional;
import java.util.function.Function;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.optim.nonlinear.vector.leastsquares.LeastSquaresOptimizer;
import org.hipparchus.optim.nonlinear.vector.leastsquares.LeastSquaresProblem;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresConstants;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresSolution;

/** A Hipparchus {@link LeastSquaresOptimizer} implementation using our {@link LevenbergMarquardt} implementation.
 * 
 * @apiNote This implementation has obvious inefficiencies, like superfluous {@code double[]}-to-vector and
 *          {@code double[][]}-to-matrix conversions, but it's clean. If we find that we're creating a bottle-neck in
 *          performance due to this, we can optimize then.
 * @implNote This optimization changes the problem from one where we're seeking target values to one where we seek a
 *           zero; i.e., {@link LevenbergMarquardt} will solve {@code f(x) = y}, but this optimization passes the
 *           algorithm the equivalent {@code f(x) - y = g(x) = 0}. We do this simply because it's convenient, as
 *           {@link LeastSquaresProblem} implementations (strangely) do not provide the observation values.
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class LevenbergMarquardtOptimizer implements LeastSquaresOptimizer {
    
    /** if the sum of the squares of the residuals is less than this squared, we declare convergence */
    private final double epsilon;
    /** the maximum number of iterations before we quit */
    private final int maxNumIterations;
    /** the maximum number of unimproved iterations before we quit */
    private final int maxNumUnimprovedIters;
    /** the dxs to use estimating values of the Jacobian */
    private final double[] dxs;
    /** a {@link Function} that maps {@link RealMatrix matrices} to {@link DecompositionSolver}s that we can use to
     * solve systems of linear equations */
    private final Function<RealMatrix, DecompositionSolver> decompFact;
    
    /** Simple constructor that uses {@link #LevenbergMarquardtOptimizer(double, int, int, double[], Function)} with
     * default convergence-related values from {@link WeightedLeastSquaresConstants} and a numerical approximation to
     * the jacobian using {@link Numerics#getJacobianFunction}. */
    public LevenbergMarquardtOptimizer() {
        this(WeightedLeastSquaresConstants.DEFAULT_EPSILON,
             WeightedLeastSquaresConstants.DEFAULT_MAX_ITERATIONS,
             WeightedLeastSquaresConstants.DEFAULT_MAX_UNIMPROVED,
             null,
             WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER);
    }
    
    /** Constructor
     * 
     * @param epsilon if the sum of the squares of the residuals is less than this squared, we declare convergence
     * @param maxNumIterations the maximum number of iterations before we quit
     * @param maxNumUnimprovedIters the maximum number of unimproved iterations before we quit
     * @param dxs the dxs to use estimating values of the Jacobian. May be null. If null, then we compute (approx)
     *        differentials on-the-fly in the {@link Numerics#getJacobianFunction Jacobian function}.
     * @param decompFact a {@link Function} that maps {@link RealMatrix matrices} to {@link DecompositionSolver}s that
     *        we can use to solve systems of linear equations. May not be null {@code null}. */
    public LevenbergMarquardtOptimizer(final double epsilon,
                                       final int maxNumIterations,
                                       final int maxNumUnimprovedIters,
                                       final double[] dxs,
                                       final Function<RealMatrix, DecompositionSolver> decompFact) {
        Validation.requireGreaterThan(epsilon, 0D, "epsilon");
        Validation.requireGreaterThan(maxNumIterations, 0, "numIterations");
        Validation.requireGreaterThan(maxNumUnimprovedIters, 0, "numUnimprovedIters");
        Validation.requireGreaterThanEqualTo(maxNumIterations,
                                  maxNumUnimprovedIters,
                                  "numIterations must be greater than or equal to numUnimprovedIterations");
        requireNonNull(decompFact, "decompFact");
        
        this.epsilon = epsilon;
        this.maxNumIterations = maxNumIterations;
        this.maxNumUnimprovedIters = maxNumUnimprovedIters;
        this.dxs = dxs == null ? null : dxs.clone();
        this.decompFact = decompFact;
    }
    
    @Override
    public Optimum optimize(final LeastSquaresProblem leastSquaresProblem) {
        final int inputDimension = leastSquaresProblem.getParameterSize();
        final int outputDimension = leastSquaresProblem.getObservationSize();
        final VectorField fParam =
                new VectorField(v -> leastSquaresProblem.evaluate(new NVector(v))
                                                        .getResiduals()
                                                        .toArray(),
                                inputDimension,
                                outputDimension,
                                null);
        
        final MultivariateMatrixValuedFunction jacobian =
                Numerics.getJacobianFunction(fParam,
                                                          Optional.ofNullable(this.dxs),
                                                          inputDimension,
                                                          outputDimension,
                                                          Parallelism.PARALLEL);
        
        final double[] targetValuesParam = Numerics.getFilledArray(outputDimension, 0D);
        final double[] initialGuessParam = leastSquaresProblem.getStart().toArray();
        
        final double[] weights = Numerics.getFilledArray(outputDimension, 1.0);
        final LevenbergMarquardtConstants constantsParam =
                new LevenbergMarquardtConstants(outputDimension,
                                                weights,
                                                this.epsilon,
                                                this.maxNumIterations,
                                                this.maxNumUnimprovedIters,
                                                this.decompFact,
                                                LevenbergMarquardtParameter.getDefault(),             // 1, x2, /3
                                                LevenbergMarquardtConstants.DEFAULT_ALPHA_EASY,       // 0.75
                                                LevenbergMarquardtConstants.DEFAULT_H);               // 0.1
        
        final LevenbergMarquardtProblem lmp = new LevenbergMarquardtProblem(fParam,
                                                                            jacobian,
                                                                            initialGuessParam,
                                                                            targetValuesParam,
                                                                            constantsParam);
        final WeightedLeastSquaresSolution wlsSoln = LevenbergMarquardt.solve(lmp);
        
        final NVector solutionPoint = new NVector(wlsSoln.getPoint());
        final LeastSquaresProblem.Evaluation eval = leastSquaresProblem.evaluate(solutionPoint);
        final int numIterations = (int) FastMath.min(Integer.MAX_VALUE, wlsSoln.getIterationsToStop());
        final int numFunctionCallsProxy = numIterations; /* we do not (yet) count function calls, so we just use the
                                                          * number of iterations as a proxy */
        return LeastSquaresOptimizer.Optimum.of(eval, numFunctionCallsProxy, numIterations);
    }

    /** @return epsilon. If the sum of the squares of the residuals is less than this squared, we declare
     *         convergence. */
    public double getEpsilon() {
        return this.epsilon;
    }

    /** @return the maximum number of iterations before we quit */
    public int getMaxNumIterations() {
        return this.maxNumIterations;
    }

    /** @return the maximum number of unimproved iterations before we quit */
    public int getMaxNumUnimprovedIters() {
        return this.maxNumUnimprovedIters;
    }
    
    /** @return the DXs used to compute the Jacobian
     * @apiNote not a copy! */
    double[] getDXs() {
        return this.dxs;
    }
    
    /** @return the {@link DecompositionSolver} factory */
    Function<RealMatrix, DecompositionSolver> getDecompositionFactory() {
        return this.decompFact;
    }
}
