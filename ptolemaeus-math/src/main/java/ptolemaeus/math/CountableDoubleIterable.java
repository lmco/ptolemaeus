/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.IntToDoubleFunction;
import java.util.stream.DoubleStream;
import java.util.stream.StreamSupport;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ArrayUtils;

import org.hipparchus.random.RandomDataGenerator;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;

/**
 * A class with factory methods that create countable double {@link Iterable}s or, a countable set (though we do not
 * enforce that there may not be duplicates) whose elements are double-precision numbers. For any countable set there
 * exists a bijection between that set and the positive integers (natural numbers). In this context, that bijection is
 * defined by the {@link IntToDoubleFunction} index function and, considering we are necessarily dealing with a finite
 * collection, we are free to use indices less than zero.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public final class CountableDoubleIterable implements Iterable<Double> {
    
    /**
     * The {@link IntToDoubleFunction} that produces the value at each iteration
     */
    private final IntToDoubleFunction indexFunction;
    
    /**
     * The index at which we should start the iteration
     */
    private final int firstIndex;
    
    /**
     * {@code true} if we want to include the first index in the iteration, {@code false} otherwise
     */
    private final boolean firstInclusive;
    
    /**
     * The index at which we should finish the iteration
     */
    private final int lastIndex;
    
    /**
     * {@code true} if we want to include the last index in the iteration, {@code false} otherwise
     */
    private final boolean lastInclusive;
    
    /**
     * the increment, either {@code -1}, {@code 0}, or {@code 1}, which will allow us to get the next value in the
     * {@link Iterator}
     */
    private final int increment;
    
    /**
     * {@code true} if this is empty, false otherwise. Empty in this context means there are no elements over which we
     * can iterate.
     */
    private final boolean isEmpty;
    
    /**
     * @param indexFunction the {@link IntToDoubleFunction} that produces the value at each iteration
     * @param firstIndex the index at which we should start the iteration
     * @param firstInclusive {@code true} if we want to include the first index, {@code false} otherwise
     * @param lastIndex the index at which we should finish the iteration
     * @param lastInclusive {@code true} if we want to include the last index, {@code false} otherwise
     */
    CountableDoubleIterable(final IntToDoubleFunction indexFunction,
                          final int firstIndex,
                          final boolean firstInclusive,
                          final int lastIndex,
                          final boolean lastInclusive) {
        this.indexFunction = indexFunction;
        this.increment = Integer.signum(lastIndex - firstIndex);
        this.firstInclusive = firstInclusive;
        this.lastInclusive = lastInclusive;
        this.firstIndex = this.firstInclusive ? firstIndex : firstIndex + this.increment;
        this.lastIndex = this.lastInclusive ? lastIndex : lastIndex - this.increment;
        
        if (lastIndex == firstIndex) {
            if (!this.firstInclusive || !this.lastInclusive) {
                throw new IllegalArgumentException("Cannot exclude first or last if they are equal");
            }
            this.isEmpty = false; // one element
        }
        else if (FastMath.abs(lastIndex - firstIndex) == 1) {
            // exclude first and last -> zero elements
            // exclude first xor last -> one element
            // exclude neither -> two elements
            this.isEmpty = !this.firstInclusive && !this.lastInclusive;
        }
        else {
            this.isEmpty = false;
        }
    }
    
    /**
     * Get an empty {@link CountableDoubleIterable}
     * 
     * @return a {@link CountableDoubleIterable} with no elements over which you can iterate
     */
    public static CountableDoubleIterable empty() {
        return new CountableDoubleIterable(null, 0, false, 1, false);
    }
    
    /**
     * Get a {@link CountableDoubleIterable} which returns a single value {@code numValues} times
     * 
     * @param constant the constant value we want the {@link CountableDoubleIterable} to produce
     * @param numValues the number of times the {@link CountableDoubleIterable} should produce {@code constant}.
     *        {@code >= 0}
     * @return a {@link CountableDoubleIterable} which produces a constant value {@code numValues} times
     */
    public static CountableDoubleIterable constant(final double constant, final int numValues) {
        Validation.requireGreaterThanEqualTo(numValues, 0, "numValues");
        if (numValues == 0) {
            return empty();
        }
        
        return new CountableDoubleIterable(i -> constant, 0, true, numValues, false);
    }
    
    /**
     * Get a {@link CountableDoubleIterable} which can return one value one time
     * 
     * @param constant the value to return
     * @return a {@link CountableDoubleIterable} with which can return one value once
     */
    public static CountableDoubleIterable singleton(final double constant) {
        return new CountableDoubleIterable(i -> constant, 0, true, 1, false);
    }
    
    /**
     * Get a {@link CountableDoubleIterable} returning {@code numValues} uniformly distributed among [{@code first},
     * {@code last}].
     * 
     * @param first the first value of the {@link CountableDoubleIterable}. This value is always included and first if
     *        {@code numValues} &gt;= 1.
     * @param last the value of the {@link CountableDoubleIterable}. This value is always included and last if
     *        {@code numValues} &gt;= 2.
     * @param numValues the number of values total that we want uniformly distributed. {@code >= 0}
     * @return a {@link CountableDoubleIterable} returning {@code numValues} uniformly distributed among [{@code first},
     *         {@code last}]. Empty if {@code numValues} == 0.
     */
    public static CountableDoubleIterable uniform(final double first, final double last, final int numValues) {
        Validation.requireGreaterThanEqualTo(numValues, 0, "numValues");
        if (numValues == 0) {
            return empty();
        }
        if (numValues == 1) {
            return fromCollection(List.of(first));
        }
        
        final double delta = (last - first) / (numValues - 1);
        return new CountableDoubleIterable(i -> i * delta + first, 0, true, numValues - 1, true);
    }
    
    /**
     * Get a {@link CountableDoubleIterable} which returns {@code numValues} values randomly spaced throughout
     * [{@code min}, {@code max}]
     * 
     * @param min the minimum value
     * @param max the maximum value
     * @param seed the random seed
     * @param numValues the number of values to generate. {@code >= 0}
     * @return a {@link CountableDoubleIterable} which returns {@code numValues} values randomly spaced throughout
     *         [{@code min}, {@code max}]
     */
    public static CountableDoubleIterable monteCarlo(final double min,
                                                     final double max,
                                                     final long seed,
                                                     final int numValues) {
        Validation.requireGreaterThanEqualTo(numValues, 0, "numValues");
        if (numValues == 0) {
            return empty();
        }
        final RandomDataGenerator rdr = new RandomDataGenerator(seed);
        return new CountableDoubleIterable(i -> rdr.nextUniform(min, max), 0, true, numValues, false);
    }
    
    /**
     * Get a {@link CountableDoubleIterable} which when iterated returns the results of raising {@code base} to the
     * power of each integer in [{@code firstPower}, {@code lastPower}] in encounter order.
     * 
     * @param base the base of the exponent
     * @param firstPower the first power we will use
     * @param lastPower the last power we will use
     * @return a {@link CountableDoubleIterable} which when iterated return the results of raising {@code base} to the
     *         power of each integer in [{@code firstPower}, {@code lastPower}] in encounter order.
     */
    public static CountableDoubleIterable exponential(final double base, final int firstPower, final int lastPower) {
        return new CountableDoubleIterable(i -> FastMath.pow(base, i), firstPower, true, lastPower, true);
    }
    
    /**
     * Get a {@link CountableDoubleIterable} which returns exactly the values in the provided range in the encounter
     * order
     * 
     * @param range the range of values to return
     * @return a {@link CountableDoubleIterable} which returns exactly the values in the provided range in the encounter
     *         order
     */
    public static CountableDoubleIterable fromCollection(final Collection<Double> range) {
        Objects.requireNonNull(range, "collection range must be non-null");
        if (range.isEmpty()) {
            return empty();
        }
        final double[] array = ArrayUtils.toPrimitive(range.toArray(Double[]::new));
        return new CountableDoubleIterable(i -> array[i], 0, true, array.length, false);
    }
    
    /**
     * Get a custom {@link CountableDoubleIterable}
     * 
     * @param f the {@link IntToDoubleFunction} to produce values
     * @param firstIndex the first index to which we will apply {@code f}.  This index is always included.
     * @param lastIndex the last index to which we will apply {@code f}.  This index is always included.
     * @return a {@link CountableDoubleIterable} which will produce values calculated by {@code f} for each integer in
     *         [{@code firstIndex}, {@code lastIndex}]
     */
    public static CountableDoubleIterable custom(final IntToDoubleFunction f,
                                                 final int firstIndex,
                                                 final int lastIndex) {
        return new CountableDoubleIterable(Objects.requireNonNull(f, "f must be specified"),
                                           firstIndex,
                                           true,
                                           lastIndex,
                                           true);
    }
    
    /**
     * @return a {@link DoubleStream} of this {@link CountableDoubleIterable}
     */
    public DoubleStream stream() {
        return StreamSupport.doubleStream(this.spliterator(), false);
    }
    
    @Override
    public Spliterator.OfDouble spliterator() {
        return Spliterators.spliteratorUnknownSize(this.iterator(), Spliterator.ORDERED);
    }
    
    @Override
    public String toString() {
        return IterableUtils.toString(this);
    }
    
    /**
     * {@inheritDoc} by virtue of being an {@link Iterator}, this is not thread-safe unless manually, externally,
     * synchronized.
     */
    @Override
    public PrimitiveIterator.OfDouble iterator() {
        if (this.isEmpty) {
            return getEmptyIterator();
        }
        return getNonEmptyIterator();
    }
    
    /**
     * @return an empty {@link java.util.PrimitiveIterator.OfDouble}
     */
    private static PrimitiveIterator.OfDouble getEmptyIterator() {
        return new PrimitiveIterator.OfDouble() {
            
            @Override
            public boolean hasNext() {
                return false;
            }
            
            @Override
            public double nextDouble() {
                throw new NoSuchElementException("CountableDoubleIterable is empty; cannot iterate!");
            }
        };
    }
    
    /**
     * @return a non-empty {@link java.util.PrimitiveIterator.OfDouble}
     */
    private PrimitiveIterator.OfDouble getNonEmptyIterator() {
        return new PrimitiveIterator.OfDouble() {
            
            /**
             * The current {@link Iterator} index
             */
            private int currentIndex = CountableDoubleIterable.this.firstIndex;
            
            @Override
            public boolean hasNext() {
                if (CountableDoubleIterable.this.increment == 1) {
                    return this.currentIndex <= CountableDoubleIterable.this.lastIndex;
                }
                return this.currentIndex >= CountableDoubleIterable.this.lastIndex;
            }
            
            @Override
            public double nextDouble() {
                if (!this.hasNext()) {
                    throw new NoSuchElementException("CountableDoubleIterable ran out of elements!");
                }
                
                final double result = CountableDoubleIterable.this.indexFunction.applyAsDouble(this.currentIndex);
                this.currentIndex = this.currentIndex + CountableDoubleIterable.this.increment;
                return result;
            }
        };
    }
}
