/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.OptionalDouble;
import java.util.stream.Stream;

import org.hipparchus.complex.Complex;
import org.hipparchus.special.Gamma;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Quadratic;
import ptolemaeus.math.commons.Quadratic.QuadraticRoots;
import ptolemaeus.math.linear.real.NVector;

/**
 * An n-dimensional ball (generalization of a "solid sphere")
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class NBall extends AbstractRealNDomain {
    
    /**
     * The square-root of PI
     */
    private static final double SQRT_PI = FastMath.sqrt(FastMath.PI);
    
    /**
     * The {@link NPoint} center of the ball
     */
    private final NPoint center;
    
    /**
     * The radius of the ball
     */
    private final double radius;
    
    /**
     * Constructor.
     * 
     * @param embeddedDimension the dimension in which this is embedded
     * @param center the {@link NPoint} center of the ball
     * @param radius the radius of the ball
     * @param epsilon a small absolute {@code double} tolerance that we can use to compare whether {@code double}
     *        values are equal according to {@link Precision#equals(double, double, double)}
     */
    public NBall(final int embeddedDimension, final NPoint center, final double radius, final double epsilon) {
        super(embeddedDimension,
              requireNonNull(center, "a center point must be specified").getEmbeddedDimensionality(),
              epsilon);
        this.center = center;
        this.radius = radius;
    }
    
    /**
     * Constructor.
     * 
     * @param center the {@link NPoint} center of the ball
     * @param radius the radius of the ball
     */
    public NBall(final NPoint center, final double radius) {
        this(center.getEmbeddedDimensionality(), center, radius, AbstractRealNDomain.DEFAULT_EPSILON);
    }
    
    /**
     * @return the center of this {@link NBall}
     */
    public NPoint getCenter() {
        return this.center;
    }

    /**
     * @return the radius of this {@link NBall}
     */
    public double getRadius() {
        return this.radius;
    }
    
    /**
     * <p>
     * Measure formula taken from: <a href="https://dlmf.nist.gov/5.19#E4">5.19(iii) N-Dimensional Sphere</a>.
     * </p>
     * {@inheritDoc}
     */
    @Override
    protected double computeMeasure() {
        final double numerator = FastMath.pow(this.radius * SQRT_PI, this.getDimensionality());
        final double denominator = Gamma.gamma(this.getDimensionality() / 2.0 + 1);
        return numerator / denominator;
    }
    
    @Override
    public boolean contains(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        return this.center.distanceTo(point) - this.radius <= this.epsilon();
    }
    
    /**
     * This method computes the point on the surface of an {@link NBall} which is intersected by a line through two
     * points - one interior and one exterior. If the second point is also interior, we simply return the second point.
     * 
     * <p>
     * Derivation:
     * </p>
     * A Sphere in n-space is the set of all points
     * 
     * <p>
     * S<sub>n</sub> = {x : ||x - c|| = r, x, c &#8712; R<sup>n</sup>, r &#8712; R}
     * </p>
     * 
     * or
     * 
     * <p>
     * S<sub>n</sub> = the set of x &#8712; R<sup>n</sup> which satisfy Sum[(x<sub>i</sub> - c<sub>i</sub>)<sup>2</sup>,
     * 0 &lt;= i &lt;= n] = r<sup>2</sup>
     * </p>
     * 
     * A line in R<sup>n</sup> which intersects two points a, b &#8712; R<sup>n</sup> can be written as
     * 
     * <p>
     * L<sub>a,b</sub>(t) = a + (b - a)t
     * </p>
     * 
     * The intersection of S<sub>n</sub> and the line L<sub>a,b</sub>(t) is the set of all t &#8712; R which satisfy the
     * following which is the component-wise substitution of the line function into the equation of the n-sphere:
     * 
     * <p>
     * Sum[(a<sub>i</sub> + (b<sub>i</sub> - a<sub>i</sub>)t - c<sub>i</sub>)<sup>2</sup>, 0 &lt;= i &lt;= n] =
     * r<sup>2</sup>
     * </p>
     * 
     * let d<sub>i</sub> = b<sub>i</sub> - a<sub>i</sub>, g<sub>i</sub> = c<sub>i</sub> - a<sub>i</sub> (assume i-range
     * specification for brevity)
     * 
     * <p>
     * Sum[(d<sub>i</sub>t - g<sub>i</sub>)<sup>2</sup>] = r<sup>2</sup>
     * </p>
     * 
     * <p>
     * Sum[d<sub>i</sub><sup>2</sup>t<sup>2</sup> - 2g<sub>i</sub>d<sub>i</sub>t + g<sub>i</sub><sup>2</sup>] =
     * r<sup>2</sup>
     * </p>
     * 
     * <p>
     * Sum[d<sub>i</sub><sup>2</sup>]t<sup>2</sup> - Sum[2g<sub>i</sub>d<sub>i</sub>]t + Sum[g<sub>i</sub><sup>2</sup>]
     * - r<sup>2</sup> = 0
     * </p>
     * <p>
     * and this is a quadratic equation in t! We solve for this using the quadratic formula and plug the 0, 1, or t's
     * into the line equation L<sub>a,b</sub>(t) = a + (b - a)t to find the points at which the line intersects the
     * {@link NBall}
     * </p>
     * 
     * Derivation found here: <a href="https://math.stackexchange.com/a/151082/773972">Calculating Line Intersection
     * with Hypersphere Surface in R<sup>n</sup></a>
     */
    @Override
    public NPoint getBoundaryPoint(final NPoint a, final NPoint b) {
        this.validateBoundaryPointParams(a, b);
        
        if (this.contains(b)) {
            return b;
        }
        
        final NPoint d = a.getVectorTo(b).getPoint();
        final NPoint g = a.getVectorTo(this.center).getPoint();
        
        final double quadraticCoefficient = this.dimensionalityIntStream()
                                                .mapToDouble(d::getComponent)
                                                .map(c -> c * c)
                                                .sum();
        
        final double linearCoefficient = (-2.0)
                                         * this.dimensionalityIntStream()
                                               .mapToDouble(i -> g.getComponent(i) * d.getComponent(i))
                                               .sum();
        
        final double constantCoefficient = -(this.radius * this.radius)
                                           + this.dimensionalityIntStream()
                                                 .mapToDouble(g::getComponent)
                                                 .map(c -> c * c)
                                                 .sum();
        
        final QuadraticRoots roots = Quadratic.computeQuadraticRoots(quadraticCoefficient,
                                                                     linearCoefficient,
                                                                     constantCoefficient);
        if (!roots.areReal()) {
            final String msg = """
                    Unable to find the roots of the quadratic which would define where the boundary point lies
                    along the line segment from a to b.  The roots found were complex, which implies that both query 
                    points were outside of the NBall, despite the earlier check confirming that a is indeed contained 
                    in the NBall.  Manual debugging is necessary.
                    
                    %s
                    a = %s
                    b = %s
                    """.formatted(this, a, b);
            throw new NumericalMethodsException(msg);
        }
        
        /* We know that 'a' is internal, so there should be exactly one solution within [0, 1] given the equation of the
         * line that we're using: a + (b - a)t = a(1 - t) + bt which reaches a at t == 0 and b at t == 1 */
        final OptionalDouble possibleSoln =
                Stream.of(roots.smallerRoot(), roots.largerRoot())
                      .mapToDouble(Complex::getRealPart)
                      .filter(soln -> -this.epsilon() <= soln && soln <= 1.0 + this.epsilon())
                      .max();
        if (possibleSoln.isEmpty()) { /* It's likely that finite precision issues are pushing 't' outside of [0, 1].
                                       * Things to check are the length of the line and proximity to the boundary. We
                                       * know that there must be a solution because 'a' is contained and 'b' is not,
                                       * so we just use the other method to find a point from the center to b. Assuming
                                       * that both 'a' and 'b' are near the boundary of this, then this should yield a
                                       * point near the actual intersection. */
            return this.getBoundaryPoint(b);
        }
        
        return new NLineSegment(a, b).apply(possibleSoln.getAsDouble());
    }
    
    /**
     * Compute the point on the boundary of this which intersects the {@link NVector} from this {@link NBall}'s center
     * to the provided {@link NPoint} {@code point}
     * 
     * @param point the query {@link NPoint}
     * @return the point on the boundary of this which intersects the {@link NVector} from this {@link NBall}'s center
     *         to the provided {@link NPoint} {@code point} or {@code point} if {@code point} is internal.
     */
    public NPoint getBoundaryPoint(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        if (this.contains(point)) {
            return point;
        }
        
        return this.center.plus(this.center.getVectorTo(point).normalized().times(this.radius));
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append("NBall c = ")
                                  .append(this.center)
                                  .append(" r = ")
                                  .append(this.radius)
                                  .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.center, this.radius);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final NBall other = (NBall) obj;
        return Objects.equals(this.center, other.center)
               && Double.doubleToLongBits(this.radius) == Double.doubleToLongBits(other.radius);
    }
}
