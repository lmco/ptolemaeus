/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import java.util.Objects;
import java.util.stream.IntStream;

import ptolemaeus.math.NumericalMethodsException;

/**
 * Anything which has dimensionality itself and is embedded in some space with dimensionality
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public interface HasDimensionality {
    
    /**
     * @return the dimension in which this {@link HasDimensionality} resides. This is not necessarily the same as
     *         {@link #getDimensionality()}.
     * @see #getDimensionality()
     */
    int getEmbeddedDimensionality();
    
    /**
     * <p>
     * Get the dimensionality of this {@link HasDimensionality}. This is not necessarily the same as
     * {@link #getEmbeddedDimensionality()}; i.e. the dimensionality of the space in which this
     * {@link HasDimensionality} is embedded.
     * </p>
     * <p>
     * Consider a plane embedded in 3-space. The plane has dimensionality '2' but is embedded in 3-space.
     * </p>
     *
     * @return the dimensionality of this {@link HasDimensionality}
     */
    int getDimensionality();
    
    /**
     * Validate that this and another {@link HasDimensionality} are compatible. For any two compatible
     * {@link HasDimensionality}s, we should be able to query one for the containment of an element of the other.
     * <p>
     * For example, we cannot query a plane in 2-space for containment of a point in 3-space nor can we query a cube in
     * 4-space for containment of a point in 3 space.
     * </p>
     * <p>
     * We do this because every space of dimension {@code n} has <sub>n</sub>C<sub>m</sub> subspaces of dimension
     * {@code m} and it is not clear which a caller means by querying a 4-space with any 3-space point.
     * </p>
     * 
     * @param other the other {@link HasDimensionality}
     */
    default void verifyCompatibleDimensionality(final HasDimensionality other) {
        if (other == null || this.getEmbeddedDimensionality() != other.getEmbeddedDimensionality()) {
            final String msg = "%s is incompatible with %s".formatted(this.toString(), Objects.toString(other));
            throw new NumericalMethodsException(msg);
        }
    }
    
    /**
     * @return an {@link IntStream} over range the integer range [0, {@link #getDimensionality()})
     */
    default IntStream dimensionalityIntStream() {
        return IntStream.range(0, this.getDimensionality());
    }
    
    /**
     * @return an {@link IntStream} over range the integer range [0, {@link #getEmbeddedDimensionality()})
     */
    default IntStream embeddedDimensionalityIntStream() {
        return IntStream.range(0, this.getEmbeddedDimensionality());
    }
}
