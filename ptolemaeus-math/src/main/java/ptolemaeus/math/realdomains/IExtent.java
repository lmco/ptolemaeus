/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.linear.real.NVector;

/**
 * Instances of classes that implement {@link IExtent} have a one-dimensional extent in a real-valued space with
 * arbitrarily many dimensions (see {@link NVector} and {@link NLineSegment}).
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public interface IExtent extends HasDimensionality {
    
    /**
     * @return the first end-point of this {@link IExtent}
     */
    NPoint getEndPoint1();
    
    /**
     * @return the second end-point of this {@link IExtent}
     */
    NPoint getEndPoint2();
    
    /**
     * @return the length of this extent
     */
    double getLength();
    
    /**
     * Determine the angle (radians) from this {@link IExtent} to the {@code other}
     * 
     * @param other the other {@link IExtent}
     * @return the angle (radians) from this {@link IExtent} to the {@code other}
     */
    default double angleTo(final IExtent other) {
        return FastMath.acos(this.cosAngleTo(other));
    }
    
    /**
     * Determine the cosine of the angle (radians) from this {@link IExtent} to the {@code other}. If one of the
     * {@link IExtent}s has {@link #getLength()} == 0, we adhere to the convention that they are orthogonal and return
     * {@code 0.0}.
     * 
     * @param other the other {@link IExtent}
     * @return the cosine of the angle (radians) from this {@link IExtent} to the {@code other}. If one of the
     *         {@link IExtent}s has {@link #getLength()} == 0, return {@code 0.0}.
     */
    default double cosAngleTo(final IExtent other) {
        final NPoint common = this.getCommonPoint(other);
        final NVector vector2Common1 = common.getVectorTo(this.getOtherPoint(common));
        final NVector vector2Common2 = common.getVectorTo(other.getOtherPoint(common));
        
        final double dot = vector2Common1.dot(vector2Common2);
        final double magTimes = vector2Common1.getMagnitude() * vector2Common2.getMagnitude();
        
        if (Precision.equals(magTimes, 0.0)) {
            return 0.0;
        }
        
        return dot / magTimes;
    }
    
    /**
     * get the endpoint of {@code this} {@link IExtent} that is not the provided one
     * 
     * @param other the other {@link NPoint}
     * @return the endpoint of {@code this} {@link IExtent} that is not the provided one
     */
    default NPoint getOtherPoint(final NPoint other) {
        if (this.getEndPoint1().equals(other)) {
            return this.getEndPoint2();
        }
        if (this.getEndPoint2().equals(other)) {
            return this.getEndPoint1();
        }
        final String msg = "%s is not an end-point of %s".formatted(other, this);
        throw new NumericalMethodsException(msg);
    }
    
    /**
     * Get the endpoint of {@code this} and {@code other} that are the same or throw an exception if there is none
     * 
     * @param other the other {@link IExtent}
     * @return the endpoint of {@code this} and {@code other} that are the same or throw an exception if there is none
     */
    default NPoint getCommonPoint(final IExtent other) {
        if (this.getEndPoint1().equals(other.getEndPoint1()) || this.getEndPoint1().equals(other.getEndPoint2())) {
            return this.getEndPoint1();
        }
        if (this.getEndPoint2().equals(other.getEndPoint1()) || this.getEndPoint2().equals(other.getEndPoint2())) {
            return this.getEndPoint2();
        }
        
        final String msg = "%s does not share a common point with %s".formatted(this, other);
        throw new NumericalMethodsException(msg);
    }
}
