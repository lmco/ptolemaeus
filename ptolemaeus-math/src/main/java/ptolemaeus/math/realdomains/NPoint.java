/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javax.annotation.concurrent.Immutable;

import org.apache.commons.lang3.ArrayUtils;

import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.linear.real.NVector;

/**
 * An point in an {@link RealNDomain}
 * 
 * @author Ryan T. Moser, Ryan.T.Moser@lmco.com
 */
@Immutable
public class NPoint extends AbstractRealNDomain implements Serializable {
    
    /** generated */
    private static final long serialVersionUID = 7609785838930665287L;

    /**
     * A function to create zero-points
     */
    private static final Function<Integer, NPoint> GET_ZERO_FUNCTION = i -> new NPoint(new double[i]);
    
    /**
     * A mapping of dimensions to their zero-points
     */
    private static final Map<Integer, NPoint> ZEROES_CACHE;
    static {
        ZEROES_CACHE = new ConcurrentHashMap<>();
        for (int i = 0; i < 10; i++) {
            ZEROES_CACHE.computeIfAbsent(i, GET_ZERO_FUNCTION);
        }
    }
    
    /**
     * This {@link NPoint}'s components in n = components.length space
     */
    private final double[] components;
    
    /**
     * varargs constructor
     * 
     * @param componentsParam the components we want to use for the {@link NPoint} we are creating
     */
    @SafeVarargs
    public NPoint(final double... componentsParam) {
        super(requireNonNull(componentsParam, "components must be specified").length, 0, 0D);
        this.components = componentsParam.clone();
     }
    
    /**
     * List constructor
     * 
     * @param componentsParam the components we want to use for the {@link NPoint} we are creating
     */
    public NPoint(final List<Double> componentsParam) {
        this(ArrayUtils.toPrimitive(Objects.requireNonNull(componentsParam, "components must be specified")
                                           .toArray(Double[]::new)));
    }
    
    @Override
    public boolean contains(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        return this.equals(point);
    }
    
    @Override
    public NPoint getBoundaryPoint(final NPoint internal, final NPoint point) {
        this.validateBoundaryPointParams(internal, point);
        return this;
    }
    
    /**
     * @return get the components of this {@link NPoint}
     */
    public double[] getComponents() {
        return this.components.clone();
    }
    
    /**
     * @param position the position in this {@link NPoint}'s components
     * @return the value at 'position' in this {@link NPoint}'s components
     */
    public double getComponent(final int position) {
        if (position >= this.getEmbeddedDimensionality()) {
            throw new IllegalArgumentException(
                    "Queried position must be less than the dimension of the space in which the NPoint resides");
        }
        
        return this.components[position];
    }
    
    /**
     * Compute the squared distance from this point to another
     * 
     * @param other the other point
     * @return the squared distance from this to the other point
     */
    public double sqrDistanceTo(final NPoint other) {
        this.verifyCompatibleDimensionality(other);
        double answer = 0.0;
        for (int i = 0; i < this.getEmbeddedDimensionality(); i++) {
            final double delta = this.components[i] - other.components[i];
            answer += delta * delta;
        }
        
        return answer;
    }
    
    /**
     * Compute the distance from this point to another
     * 
     * @param other the other point
     * @return the distance from this to the other point
     */
    public double distanceTo(final NPoint other) {
        return FastMath.sqrt(sqrDistanceTo(other));
    }
    
    /**
     * @return this but as an {@link NVector}
     */
    public NVector asVector() {
        return new NVector(this);
    }
    
    /**
     * @param vector the {@link NVector} to add to this
     * @return the result of adding {@code vector} to {@code this}
     */
    public NPoint plus(final NVector vector) {
        this.verifyCompatibleDimensionality(vector);
        final double[] newComponents = new double[this.getEmbeddedDimensionality()];
        this.embeddedDimensionalityIntStream()
            .forEach(i -> newComponents[i] = this.components[i] + vector.getComponent(i));
        return new NPoint(newComponents);
    }
    
    /**
     * @param other the {@link NPoint} to add to this
     * @return the result of adding {@code other} to {@code this}
     */
    public NPoint plus(final NPoint other) {
        this.verifyCompatibleDimensionality(other);
        final double[] newComponents = new double[this.getEmbeddedDimensionality()];
        this.embeddedDimensionalityIntStream()
            .forEach(i -> newComponents[i] = this.components[i] + other.components[i]);
        return new NPoint(newComponents);
    }
    
    /**
     * @param scalar the scalar by which we wish to multiply this
     * @return the result of multiplying this {@link NPoint}, component-wise, by {@code scalar}
     */
    public NPoint times(final double scalar) {
        final double[] newComponents = new double[this.getEmbeddedDimensionality()];
        this.embeddedDimensionalityIntStream().forEach(i -> newComponents[i] = this.components[i] * scalar);
        return new NPoint(newComponents);
    }
    
    /**
     * Get the {@link NVector} from this to {@code other}
     * 
     * @param other the other {@link NPoint}
     * @return the {@link NVector} from this to {@code other}
     */
    public NVector getVectorTo(final NPoint other) {
        this.verifyCompatibleDimensionality(other);
        final double[] vectorComponents = new double[this.getEmbeddedDimensionality()];
        this.embeddedDimensionalityIntStream()
            .forEach(i -> vectorComponents[i] = other.components[i] - this.components[i]);
        return new NVector(vectorComponents);
    }
    
    /**
     * Get the origin in dimension {@code dim}
     * 
     * @param dim the dimension
     * @return the origin in dimension {@code dim}
     */
    public static NPoint getZero(final int dim) {
        Validation.requireGreaterThanEqualTo(dim, 0, "Cannot get a point with dimension less than zero");
        return ZEROES_CACHE.computeIfAbsent(dim, GET_ZERO_FUNCTION);
    }
    
    @Override
    protected double computeMeasure() {
        return 0.0;
    }
    
    @Override
    public String toString() {
        return this.asVector().toString("%s", ", ", "(", ")");
    }
    
    /** Create a string representation of this {@link NPoint} using the given format string for its components
     * 
     * @param doubleFormatString the format string
     * @return the string representation 
     * 
     * @see #toString(String, String, String, String) */
    public String toString(final String doubleFormatString) {
        return this.toString(doubleFormatString, ", ", "(", ")");
    }
    
    /** Create a string representation of this {@link NPoint} using the given format string, delimiter, prefix, and
     * suffix.<br>
     * <br>
     * 
     * E.g.: if this has components {@code 1, 2, 3, 4}, and @ {@code doubleFormatString == "%s"}, code delimiter == ",
     * "}, {@code prefix == "("}, and {@code suffix == ")"}, the resulting string will be {@code (1.0, 2.0, 3.0, 4.0)}
     * 
     * @param doubleFormatString the format string to format {@code double}s as strings; e.g., "%.10f", "%s", etc.
     * @param delimiter the string to delimit elements
     * @param prefix the prefix, prepended to the string representation
     * @param suffix the suffix, appended to the string representation
     * @return the string representation of this {@link NPoint} */
    public String toString(final String doubleFormatString,
                           final String delimiter,
                           final String prefix,
                           final String suffix) {
        return this.asVector().toString(doubleFormatString, delimiter, prefix, suffix);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.components);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final NPoint other = (NPoint) obj;
        return Arrays.equals(this.components, other.components);
    }
    
    /**
     * Determine equality given a tolerance magnitude
     * 
     * @param other we want to determine whether {@code other} is equal to {@code this}
     * @param tolerance the absolute tolerance
     * @return true if equal within tolerance, false otherwise
     */
    public boolean equals(final NPoint other, final double tolerance) {
        this.verifyCompatibleDimensionality(other);
        return this.sqrDistanceTo(other) < tolerance * tolerance;
    }
}
