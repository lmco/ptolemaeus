/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import ptolemaeus.commons.Validation;

/**
 * A solid, n-dimensional domain. If finite, an {@link RealNDomain} includes its limiting surface. As implied,
 * {@link RealNDomain}s may be infinite. See {@link RN}.
 * 
 * @author Ryan T. Moser, Ryan.T.Moser@lmco.com
 */
public interface RealNDomain extends HasDimensionality {
    
    /**
     * Get the measure of this domain. Measure in this context is the generalization of volume, area, length, etc.
     * 
     * See <a href="https://mathworld.wolfram.com/MeasureTheory.html"> MeasureTheory</a>
     * 
     * @return the measure of this {@link RealNDomain}
     */
    double getMeasure();
    
    /**
     * Determine whether this contains the provided point. We assume that on the surface implies inclusion.
     * 
     * @param point the point for which we are evaluating containment
     * @return {@code true} if this {@link RealNDomain} contains the {@link NPoint}, {@code false} otherwise
     */
    boolean contains(NPoint point);
    
    /**
     * Find a point on the boundary of this {@link RealNDomain} which lies on the vector between {@code internal} and
     * {@code point}. If {@code point} is internal, return {@code point}.
     * 
     * @param internal a {@link NPoint} for which {@link RealNDomain#contains(NPoint)} will return true
     * @param point the point which is possibly external
     * @return a point on the boundary of this {@link RealNDomain} which lies on the vector between {@code internal} and
     *         {@code point}. If {@code point} is internal, return {@code point}.
     * @implNote implementations should be able to handle cases where {@code internal} is on the boundary or, due to
     *           finite precision issues, appears slightly outside of the {@link RealNDomain} without throwing an
     *           exception.
     */
    NPoint getBoundaryPoint(NPoint internal, NPoint point);
    
    /**
     * Validate that a {@link RealNDomain} and two {@link NPoint}s are valid queries for
     * {@link RealNDomain#getBoundaryPoint(NPoint, NPoint)}
     * 
     * @param internal the internal {@link NPoint}
     * @param point the possibly external {@link NPoint}
     */
    default void validateBoundaryPointParams(final NPoint internal, final NPoint point) {
        this.verifyCompatibleDimensionality(internal);
        this.verifyCompatibleDimensionality(point);
        Validation.requireTrue(this.contains(internal), () -> "internal was not contained in this");
    }
}
