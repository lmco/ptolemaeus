/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

import org.hipparchus.util.Precision;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.commons.Validation;

/** A simple implementation of {@link RealNDomain} to hold dimension and measure information. We assume self-consistent
 * dimension information.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @implNote we have these non-{@code final} fields because not every implementation can set the dimensionality values
 *           in a call to a super constructor, and thus must set them using a setter (e.g., in
 *           {@link NOrthotope#NOrthotope(Set)} we have to compute the dimensionality in a way incompatible with the
 *           super call being the first thing in the constructor). Internally, we use {@link OptionalInt} and
 *           {@link OptionalDouble} because they are safer and more convenient than using {@code boxed primatives}. */
public abstract class AbstractRealNDomain implements RealNDomain {
    
    /** a default value for {@link #epsilon} */
    protected static final double DEFAULT_EPSILON = 1e-12;
    
    /** the dimensionality of the space in which {@code this} is embedded.
     * 
     * @see AbstractRealNDomain the class-level JavaDoc */
    private OptionalInt embeddedDimensionality;
    
    /** the dimensionality of {@code this}
     * 
     * @see AbstractRealNDomain the class-level JavaDoc */
    private OptionalInt domainDimensionality;
    
    /** a small absolute {@code double} tolerance that we can use to compare whether {@code double} values are equal
     * according to {@link Precision#equals(double, double, double)}
     * 
     * @see AbstractRealNDomain the class-level JavaDoc */
    private OptionalDouble epsilon;
    
    /**
     * The measure of this {@link AbstractRealNDomain} stored as long bits so that we can make use of {@link AtomicLong}
     */
    private volatile double measure;
    
    /**
     * Empty constructor. All optional, ideally, will be set before implementation construction is finished.
     */
    protected AbstractRealNDomain() {
        this.embeddedDimensionality = OptionalInt.empty();
        this.domainDimensionality = OptionalInt.empty();
        this.measure = -1D;
        this.epsilon = OptionalDouble.empty();
    }
    
    /**
     * @param embeddedDimensionality the dimensionality of the space in which {@code this} is embedded
     * @param domainDimensionality the dimensionality of {@code this}
     * @param epsilon a small absolute {@code double} tolerance that we can use to compare whether {@code double}
     * values are equal according to {@link Precision#equals(double, double, double)}
     */
    protected AbstractRealNDomain(final int embeddedDimensionality,
                                  final int domainDimensionality,
                                  final double epsilon) {
        Validation.requireGreaterThanEqualTo(embeddedDimensionality,
                                                       domainDimensionality,
                                                       "embeddedDimensionality");
        Validation.requireGreaterThanEqualTo(embeddedDimensionality, 0, "embeddedDimensionality");
        Validation.requireGreaterThanEqualTo(domainDimensionality, 0, "domainDimensionality");
        Validation.requireGreaterThanEqualTo(epsilon, 0D, "domainDimensionality");
        this.embeddedDimensionality = OptionalInt.of(embeddedDimensionality);
        this.domainDimensionality = OptionalInt.of(domainDimensionality);
        this.measure = -1D;
        this.epsilon = OptionalDouble.of(epsilon);
    }
    
    @Override
    public int getEmbeddedDimensionality() {
        return this.embeddedDimensionality.orElseThrow(notSpecifiedExcpSupplier("embeddedDimensionality"));
    }
    
    @Override
    public int getDimensionality() {
        return this.domainDimensionality.orElseThrow(notSpecifiedExcpSupplier("domainDimensionality"));
    }
    
    /** @return a small absolute {@code double} tolerance that we can use to compare whether {@code double} values are
     *         equal according to {@link Precision#equals(double, double, double)} */
    protected double epsilon() {
        return this.epsilon.orElseThrow(notSpecifiedExcpSupplier("epsilon"));
    }
    
    @Override
    public double getMeasure() {
        if (this.measure == -1D) {
            /* The contract of computeMeasure declares that any implementation is "well behaved" in that for any
             * instance of an AbstractNDomain, `abs`, abs.computeMeasure() will always return the same value. Because of
             * this, there is no need to synchronize: threads may overwrite each other if they make it into this if
             * block before the value is set by another thread, but the overwritten value will always be the same and,
             * once it's set, all threads outside of this if-block will never be able to enter this if-block, allowing
             * for performance improvements. */
            this.measure = Validation.requireGreaterThanEqualTo(this.computeMeasure(), 0.0, "measure");
        }
        return this.measure;
    }
    
    /** Compute the measure of this {@link AbstractRealNDomain} from scratch. Any concrete implementation of
     * {@link AbstractRealNDomain#computeMeasure()} must be well behaved in the sense that for any given instance, it
     * always returns the same value.
     * 
     * @return the measure of this {@link AbstractRealNDomain} */
    protected abstract double computeMeasure();
    
    /**
     * @param dim the dimensionality of the space in which {@code this} is embedded
     */
    protected void setEmbeddedDimensionality(final int dim) {
        if (this.embeddedDimensionality.isPresent()) {
            alreadySpecifiedException("embeddedDimensionality");
        }
        
        this.embeddedDimensionality = OptionalInt.of(dim);
    }
    
    /**
     * @param dim the dimensionality of {@code this}
     */
    protected void setDimensionality(final int dim) {
        if (this.domainDimensionality.isPresent()) {
            alreadySpecifiedException("domainDimensionality");
        }
        
        this.domainDimensionality = OptionalInt.of(dim);
    }
    
    /** @param epsilon the epsilon of {@code this} */
    protected void setEpsilon(final double epsilon) {
        if (this.epsilon.isPresent()) {
            alreadySpecifiedException("domainDimensionality");
        }
        
        this.epsilon = OptionalDouble.of(epsilon);
    }
    
    /**
     * @param name the name of the requested value
     * @return an {@link Exception} {@link Supplier} for when a value is requested before it's specified
     */
    private static Supplier<RuntimeException> notSpecifiedExcpSupplier(final String name) {
        return () -> new NumericalMethodsException(String.format("%s was requested before it was specified", name));
    }
    
    /**
     * Throw when the value has already been set
     * 
     * @param name the name of the value
     */
    private static void alreadySpecifiedException(final String name) {
        throw new NumericalMethodsException(String.format("%s was already set", name));
    }
}
