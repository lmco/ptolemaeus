/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import java.util.Objects;
import java.util.Optional;
import java.util.function.DoubleFunction;

import org.hipparchus.linear.RealVector;
import org.hipparchus.util.Precision;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

/**
 * A line segment in an n-dimensional space. Being {@link DoubleFunction}s, {@link NLineSegment}s allow us to linearly
 * interpolate or extrapolate for points on the infinite extension of this {@link NLineSegment}.
 * {@link NLineSegment#apply}(0) will return {@link #getEndPoint1()} and {@link NLineSegment#apply}(1) will return
 * {@link #getEndPoint2()}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class NLineSegment extends AbstractRealNDomain implements IExtent, DoubleFunction<NPoint> {
    
    /**
     * The first end-point of this {@link NLineSegment}
     */
    private final NPoint endPt1;
    
    /**
     * The second end-point of this {@link NLineSegment}
     */
    private final NPoint endPt2;
    
    /**
     * @param embeddedDimension the dimension in which this is embedded
     * @param endPt1 the first end-point of this {@link NLineSegment}
     * @param endPt2 the second end-point of this {@link NLineSegment}
     * @param epsilon a small absolute {@code double} tolerance that we can use to compare whether {@code double}
     *        values are equal according to {@link Precision#equals(double, double, double)}
     */
    public NLineSegment(final int embeddedDimension, final NPoint endPt1, final NPoint endPt2, final double epsilon) {
        super(embeddedDimension, 1, epsilon);
        Objects.requireNonNull(endPt1, "end-points must be specified");
        endPt1.verifyCompatibleDimensionality(endPt2);
        this.endPt1 = endPt1;
        this.endPt2 = endPt2;
    }
    
    /**
     * @param endPt1 the first end-point of this {@link NLineSegment}
     * @param endPt2 the second end-point of this {@link NLineSegment}
     */
    public NLineSegment(final NPoint endPt1, final NPoint endPt2) {
        this(Objects.requireNonNull(endPt1, "end-points must be specified").getEmbeddedDimensionality(),
             endPt1,
             endPt2,
             AbstractRealNDomain.DEFAULT_EPSILON);
    }
    
    @Override
    public boolean contains(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        return Precision.equals(this.endPt1.distanceTo(point) + this.endPt2.distanceTo(point),
                                this.getMeasure(),
                                this.epsilon());
    }
    
    @Override
    public NPoint getBoundaryPoint(final NPoint internal, final NPoint point) {
        this.validateBoundaryPointParams(internal, point);
        
        final double pointToEndPt1 = point.sqrDistanceTo(this.endPt1);
        final double pointToEndPt2 = point.sqrDistanceTo(this.endPt2);
        
        return pointToEndPt1 <= pointToEndPt2 ? this.endPt1 : this.endPt2;
    }
    
    @Override
    public NPoint getEndPoint1() {
        return this.endPt1;
    }
    
    @Override
    public NPoint getEndPoint2() {
        return this.endPt2;
    }
    
    @Override
    public double getLength() {
        return this.getMeasure();
    }
    
    @Override
    protected double computeMeasure() {
        return this.endPt1.distanceTo(this.endPt2);
    }
    
    /**
     * This returns {@link #getEndPoint1()} for {@code value == 0.0} and {@link #getEndPoint2()} for
     * {@code value == 1.0} and linearly interpolates or extrapolates for values other than those.
     * 
     * {@inheritDoc}
     */
    @Override
    public NPoint apply(final double value) {
        return this.getEndPoint1().plus(this.getEndPoint1().getVectorTo(this.getEndPoint2()).times(value));
    }
    
    /**
     * Calculate the point on this NLineSegment closest to {@code point}. We return {@code point} if it lies on
     * {@code this} {@link NLineSegment}
     * 
     * @param point the {@link NPoint} for which we want to find the closest point on this {@link NLineSegment}
     * @return the point on this NLineSegment closest to {@code point} or {@code point} if it lies on {@code this}
     *         {@link NLineSegment}
     */
    public NPoint getClosestPoint(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        if (this.contains(point)) {
            return point;
        }
        
        final NVector q = this.endPt1.getVectorTo(point);
        final NVector v = this.endPt1.getVectorTo(this.endPt2);
        
        final double vMagnSq = v.magnitudeSquared();
        if (vMagnSq < this.epsilon()) { // Degenerate line segment
            return this.endPt1;
        }
        
        final double lambda = q.dot(v) / vMagnSq;
        if (lambda < 0) {
            return this.endPt1;
        }
        else if (lambda > 1) {
            return this.endPt2;
        }
        else {
            return this.apply(lambda);
        }
    }
    
    /**
     * <p>
     * Calculate the intersection of two {@link NLineSegment}s. Note that, as n increases, it is more-and-more unlikely
     * that two line segments will intersect.
     * </p>
     * Derivation:
     * <p>
     * Consider two line segments in n-dimensional space, L1 and L2; L1 connecting a and b, l2 connecting g and d. We
     * can express those lines as the vector-values of the following functions as we allow their independent variables
     * to vary over [0, 1]:
     * </p>
     * <p>
     * L<sub>1</sub>(t) = <b>a</b> + (<b>b</b>-<b>a</b>)t and L<sub>2</sub>(u) = <b>g</b> + (<b>d</b>-<b>g</b>)u
     * </p>
     * Setting these equal, we have
     * <p>
     * L<sub>1</sub>(t) = L<sub>2</sub>(u) -&gt; <b>a</b> + (<b>b</b>-<b>a</b>)t = <b>g</b> + (<b>d</b>-<b>g</b>)u
     * </p>
     * Rearranging:
     * <p>
     * (<b>b</b>-<b>a</b>)t - (<b>d</b>-<b>g</b>)u = <b>g</b> - <b>a</b>
     * </p>
     * then
     * <p>
     * (<b>b</b>-<b>a</b>)t + (<b>g</b>-<b>d</b>)u = <b>g</b> - <b>a</b>
     * </p>
     * Expanding:
     * 
     * | b<sub>0</sub> - a<sub>0</sub> |      | g<sub>0</sub> - d<sub>0</sub> |     | g<sub>0</sub> - a<sub>0</sub> |
     * | b<sub>1</sub> - a<sub>1</sub> |      | g<sub>1</sub> - d<sub>1</sub> |     | g<sub>1</sub> - a<sub>1</sub> |
     *      .               .               .
     *      .      t +      .      u  =     .
     *      .               .               .
     * | b<sub>n</sub> - a<sub>n</sub> |      | g<sub>n</sub> - d<sub>n</sub> |     | g<sub>n</sub> - a<sub>n</sub> |
     * 
     * Rearrange again:
     * 
     * | b<sub>0</sub> - a<sub>0</sub>          g<sub>0</sub> - d<sub>0</sub> |        | g<sub>0</sub> - a<sub>0</sub> |
     * | b<sub>1</sub> - a<sub>1</sub>          g<sub>1</sub> - d<sub>1</sub> |        | g<sub>1</sub> - a<sub>1</sub> |
     *      .               .        |t|        .
     *      .        +      .        |u| =      .
     *      .               .                   .
     * | b<sub>n</sub> - a<sub>n</sub>          g<sub>n</sub> - d<sub>n</sub> |        | g<sub>n</sub> - a<sub>n</sub> |
     * 
     * Simplified:
     * 
     * |<b>ab</b> <b>dg</b>||t| = <b>ag</b>
     *        |u|
     * 
     * and this linear system can be solved using {@link Matrix#solve(RealVector)}
     * 
     * @param other the {@link NLineSegment} with which we want to find an intersection
     * @return the intersection of {@code this} with {@code other}
     */
    public Optional<NPoint> calculateIntersection(final NLineSegment other) {
        this.verifyCompatibleDimensionality(other);
        
        final NPoint a = this.getEndPoint1();
        final NPoint b = this.getEndPoint2();
        final NPoint g = other.getEndPoint1();
        final NPoint d = other.getEndPoint2();
        
        final NVector ab = a.getVectorTo(b);
        final NVector dg = d.getVectorTo(g);
        final NVector rhsVector = a.getVectorTo(g); // a * g is the RHS
        
        // transpose to get the Matrix as a column vector Matrix
        final Matrix lhsMatrix = new Matrix(new double[][] { ab.getComponents(), dg.getComponents() }).transpose();
        final NVector solution = lhsMatrix.solve(rhsVector);
        
        final RealVector verification = lhsMatrix.operate(solution);
        if (!Numerics.vectorElementwiseEquals(verification, rhsVector, this.epsilon())) {
            return Optional.empty(); /* over-determined systems may have solutions which are the "best possible" which
                                      * does not imply intersection. If a solution does not work, exactly, then there is
                                      * no solution and we return an empty Optional. */
        }
        
        final double t = solution.getEntry(0);
        final double u = solution.getEntry(1);
        if (t < 0.0 || t > 1.0 || u < 0.0 || u > 1.0) { // solution is not within both line segments
            return Optional.empty();
        }
        
        return Optional.of(a.plus(ab.times(t)));
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append("[")
                                  .append(this.endPt1)
                                  .append(", ")
                                  .append(this.endPt2)
                                  .append("]")
                                  .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.endPt1, this.endPt2);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final NLineSegment other = (NLineSegment) obj;
        return Objects.equals(this.endPt1, other.endPt1) && Objects.equals(this.endPt2, other.endPt2);
    }
}
