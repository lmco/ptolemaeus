/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

/**
 * An {@link RealNDomain} representing the n-dimensional euclidean space
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class RN extends AbstractRealNDomain {
    
    /**
     * Constructor
     * 
     * @param dim the dimension
     */
    public RN(final int dim) {
        super(dim, dim, 0.0);
    }
    
    @Override
    public boolean contains(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        return true;
    }
    
    @Override
    public NPoint getBoundaryPoint(final NPoint internal, final NPoint point) {
        this.validateBoundaryPointParams(internal, point);
        return point;
    }

    @Override
    protected double computeMeasure() {
        return Double.POSITIVE_INFINITY;
    }
}
