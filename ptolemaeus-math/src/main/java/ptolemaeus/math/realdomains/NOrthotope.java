/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hipparchus.linear.LUDecomposition;
import org.hipparchus.util.Combinations;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.commons.Validation;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.commons.collections.MapUtils;
import ptolemaeus.commons.collections.SetUtils;
import ptolemaeus.commons.collections.StreamUtils;

/**
 * This class holds the information of an n-orthotope (it is the Cartesian product of n edges). This can be thought
 * of as an "n-dimensional rectangle" or an n-box: all edges, if touching, are orthogonal.
 * 
 * @author Ryan T. Moser, Ryan.T.Moser@lmco.com
 */
public class NOrthotope extends AbstractRealNDomain {
    
    /**
     * The set of edges such that, when the Cartesian product is taken between each of them, we get this
     * {@link NOrthotope}
     */
    private final Set<NLineSegment> edges;
    
    /**
     * The derived vertices of this {@link NOrthotope}
     */
    private final Set<NPoint> vertices;
    
    /**
     * The local coordinate system's origin
     */
    private final NPoint localOrigin;
    
    /**
     * The local axes which originate at {@link #localOrigin}
     */
    private final List<NVector> localAxes;
    
    /**
     * the mapping of all vertices to the edges that contain those vertices as an end-point
     */
    private final Map<NPoint, Set<NLineSegment>> verticesToTheirEdges;
    
    /**
     * The center point of this {@link NOrthotope}
     */
    private final NPoint centerPoint;
    
    /**
     * <p>
     * A {@link Matrix} which can be used project vectors into the local coordinate system.
     * </p>
     * <p>
     * Multiplying this {@link Matrix} by a vector, <b>v</b>,results in a vector whose components are the extent of
     * <b>v</b> in the direction of each row-vector. In this case, the row vectors are representative of the local axes
     * with unit length and, naturally, they are orthogonal to one another. This all means that multiplying this matrix
     * by a vector results in a vector projected into this {@link NOrthotope}'s local coordinate system.
     * </p>
     * <p>
     * However, multiplying this matrix by an {@link NPoint} does not project that point into the local coordinate
     * system, because it does not account for the origin shift.
     * </p>
     * <p>
     * The ordering of the row-vectors is arbitrary, so the matrix might have a determinant of -1
     * </p>
     */
    private final Matrix projectionMatrix;
    
    /**
     * The point opposite the local origin which is part of {@code this} {@link NOrthotope} in the projected space
     */
    private final NPoint oppositeLocalOrigin;
    
    /**
     * <p>
     * A cache of vertices to a map of the combinations of edges that meet at that vertex to the
     * ({@link #getDimensionality()} - 1) dimensional {@link NOrthotope}s that those edges represent.
     * </p>
     * <p>
     * Each vertex in this {@link NOrthotope} also belongs to smaller (lower-dimensional) {@link NOrthotope}s; for
     * example, the vertex of a cube is also a vertex of 3 faces and 3 edges. Each of the smaller {@link NOrthotope}s is
     * built from some subset of the edges adjacent to the vertex. {@link #vertex2Vectors2Facets} is a mapping from the
     * vertex to the subsets of edge-vectors to the resulting smaller {@link NOrthotope}s of dimension one less than the
     * dimensionality of this.
     * </p>
     */
    private final Map<NPoint, Map<Collection<NVector>, NOrthotope>> vertex2Vectors2Facets;
    
    /**
     * Pairs of doubles (embeddedDimensionality by 2 double[][]) representing the min and max value that this
     * {@link NOrthotope} takes in each dimension of its embedded space
     */
    private final double[][] boundingBoxIntervals;
    
    /**
     * A constructor for which you must specify the edges of the {@link NOrthotope} you wish to generate. The
     * {@link NLineSegment}s do not need to be specified in any particular order.
     * 
     * @param edges the edges of the {@link NOrthotope}
     */
    public NOrthotope(final Set<NLineSegment> edges) {
        this(edges, AbstractRealNDomain.DEFAULT_EPSILON);
    }
    
    /**
     * A constructor for which you must specify the edges of the {@link NOrthotope} you wish to generate. The
     * {@link NLineSegment}s do not need to be specified in any particular order.
     * 
     * @param edges the edges of the {@link NOrthotope}
     * @param epsilon a small absolute {@code double} tolerance that we can use to compare whether {@code double}
     *        values are equal according to {@link Precision#equals(double, double, double)}
     */
    public NOrthotope(final Set<NLineSegment> edges, final double epsilon) {
        if (edges == null || edges.isEmpty()) {
            throw new ValidationException("must specift at least one edge");
        }
        
        this.setEpsilon(epsilon);
        
        this.vertex2Vectors2Facets = new ConcurrentHashMap<>();
        this.edges = SetUtils.copyOf(edges);
        this.vertices = SetUtils.copyOf(this.edges.stream()
                                        .flatMap(edge -> Stream.of(edge.getEndPoint1(), edge.getEndPoint2()))
                                        .collect(Collectors.toCollection(LinkedHashSet::new)));
        
        this.verticesToTheirEdges = new LinkedHashMap<>();
        for (final NLineSegment edge : this.edges) {
            this.verticesToTheirEdges.computeIfAbsent(edge.getEndPoint1(), ignrd -> new LinkedHashSet<>()).add(edge);
            this.verticesToTheirEdges.computeIfAbsent(edge.getEndPoint2(), ignrd -> new LinkedHashSet<>()).add(edge);
        }
        
        // this.verticesToTheirEdges is always non-empty because this.edges is always non-empty
        final Map.Entry<NPoint, Set<NLineSegment>> entry = this.verticesToTheirEdges.entrySet()
                                                                                    .stream()
                                                                                    .findFirst()
                                                                                    .orElseThrow();
        
        this.localOrigin = entry.getKey();
        this.localAxes = entry.getValue()
                              .stream()
                              .map(line -> this.localOrigin.getVectorTo(line.getOtherPoint(this.localOrigin)))
                              .toList();
        this.projectionMatrix = new Matrix(this.localAxes.stream()
                                                         .map(NVector::normalized)
                                                         .map(NVector::getComponents)
                                                         .toArray(double[][]::new));
        this.oppositeLocalOrigin = new NPoint(this.localAxes.stream().mapToDouble(NVector::getMagnitude).toArray());
        this.centerPoint = this.vertices.stream().reduce(NPoint::plus).orElseThrow().times(1.0 / this.vertices.size());
        
        this.setDimensionality(entry.getValue().size());
        this.setEmbeddedDimensionality(entry.getKey().getEmbeddedDimensionality());
        
        this.boundingBoxIntervals = new double[this.getEmbeddedDimensionality()][2];
        for (int i = 0; i < this.getEmbeddedDimensionality(); i++) {
            // For each dimension, find the largest component of each vertex in that dimension
            final int finalI = i;
            this.boundingBoxIntervals[i][0] = this.vertices.stream()
                                                           .mapToDouble(vertex -> vertex.getComponent(finalI))
                                                           .min()
                                                           .orElseThrow();
            this.boundingBoxIntervals[i][1] = this.vertices.stream()
                                                           .mapToDouble(vertex -> vertex.getComponent(finalI))
                                                           .max()
                                                           .orElseThrow();
        }
    }
    
    /**
     * @return the vertices of this {@link NOrthotope}
     */
    public Set<NPoint> getVertices() {
        return this.vertices; // already immutable
    }
    
    /**
     * @return the edges of this {@link NOrthotope}
     */
    public Set<NLineSegment> getEdges() {
        return this.edges; // already immutable
    }
    
    /**
     * @return the center {@link NPoint} of this {@link NOrthotope}
     */
    public NPoint getCenter() {
        return this.centerPoint;
    }
    
    /**
     * @return the mapping of vertices to their abutting edges
     */
    public Map<NPoint, Set<NLineSegment>> getVerticesToEdgesMap() {
        return MapUtils.copyOf(this.verticesToTheirEdges);
    }
    
    @Override
    protected double computeMeasure() {
        /* To compute the measure (N-dimensional volume analogue) of an NOrthotope, you can take the lengths of all of
         * the edges that meet at any one vertex and multiply them together. Here, with this stream, we find the value
         * of the first key-value pair in the vertices-to-edges map, which is, of course, the Set of edges that meet at
         * that vertex, and multiply their lengths. */
        return this.verticesToTheirEdges.values()
                                        .stream()
                                        .findFirst()
                                        .orElseThrow()
                                        .parallelStream()
                                        .mapToDouble(NLineSegment::getMeasure)
                                        .reduce(1.0, (d1, d2) -> d1 * d2);
    }
    
    /**
     * <p>
     * To determine whether {@code this} contains a point, we first check the bounding box of this {@link NOrthotope}.
     * If not in the bounding box, it's definitely not within the orthotope. If {@link #getEmbeddedDimensionality()} is
     * greater than {@link #getDimensionality()}, we determine whether they share the same N-space. If so, then the
     * point may be contained in this. If not, it is not contained in this.
     * </p>
     * <p>
     * Lastly, we project the point to our local coordinate system and determine containment by the normal means
     * (bounding box, but oriented).
     * </p>
     * <p>
     * {@inheritDoc}
     * </p>
     */
    @Override
    public boolean contains(final NPoint point) {
        this.verifyCompatibleDimensionality(point);
        
        if (!this.boundingBoxContains(point)) {
            return false;
        }
        
        if (this.getEmbeddedDimensionality() > this.getDimensionality() && !existsInSameNSpace(point)) {
            /* If this NOrthotope is an N-dimensional subset of N-dimensional space, then we don't need to check whether
             * the point is in the same space, because the call to verifyCompatibleDimensionality() has already verified
             * that the point is also in N-dimensional space. However, if this NOrthotope has dimensionality M < N then
             * we must check that the point is in the same M-dimensional subspace as this. */
            return false;
        }
        
        final NPoint projectedPoint = this.globalToLocal(point);
        return this.dimensionalityIntStream()
                   .allMatch(i -> projectedPoint.getComponent(i) >= 0.0
                                  && projectedPoint.getComponent(i) <= this.oppositeLocalOrigin.getComponent(i));
    }
    
    /**
     * Check to determine whether a point is contains in the bounding box of this
     * 
     * @param point the point to check
     * @return true if the point is in the bounding box of this, false otherwise
     */
    boolean boundingBoxContains(final NPoint point) {
        return this.dimensionalityIntStream()
                   .allMatch(i -> this.boundingBoxIntervals[i][0] <= point.getComponent(i)
                                  && this.boundingBoxIntervals[i][1] >= point.getComponent(i));
    }
    
    /**
     * Let n = {@link #getDimensionality()}, m = {@link #getEmbeddedDimensionality()}
     * <p>
     * Determine whether {@code point} exists in the same n-dimensional subspace as this.
     * </p>
     * <p>
     * First, we take a vertex of {@code this} and find all of the vertices connected to it through the edges of which
     * that vertex is a part; e.g., If we have square ABCD, then starting from vertex A we would find B and D.
     * </p>
     * <p>
     * We then compute the vectors from {@code point} to all of those vectors (A, B, and D above) and arrange them into
     * an {@code (n+1) x m} matrix 'A' whose rows are the vectors. {@code (n+1)} because we have a vector from
     * {@code point} to the first vertex and every vertex adjacent to it (P to A, P to B, and P to D, in the example).
     * </p>
     * <p>
     * Finding the Gramian of this Matrix (1) (AA<sup>T</sup>, because it is non-square) and then finding the
     * determinant yields the square of the {@code (n+1)}-volume of the parallelotope formed by the (n+1) vectors. If
     * this square {@code (n+1)}-volume is non-zero, the point and {@code this} cannot exist in the same n-dimensional
     * space as, in general, a k-volume cannot exist in a (l &lt; k)-space. If it is zero, then they exist in the same
     * {@code n} (or smaller) dimensional space.
     * </p>
     * <p>
     * 1. A Matrix A of real vectors times its transpose, (AA<sup>T</sup>), is the Gramian of A. The determinant of the
     * Gramian of A is the square of the n-volume of the parallelotope spanned by the vectors of A:
     * <a href="https://en.wikipedia.org/wiki/Gram_matrix#Gram_determinant">Gram Determinant</a>
     * </p>
     * 
     * @param point we want to determine whether this {@link NPoint} exists in the same n-space as {@code this}
     *        {@link NOrthotope}
     * @return true if the point and this exist in the same n-space, false otherwise
     */
    boolean existsInSameNSpace(final NPoint point) {
        final NPoint vertex = this.getVertices().stream().findFirst().orElseThrow();
        /* The following is a Stream of each point adjacent to 'vertex' in this NOrthotope. We get the edges to which
         * 'vertex' belongs and get the endpoints on those edges which are not 'vertex' itself. */
        final Stream<NPoint> otherPoints = this.getVerticesToEdgesMap()
                                               .get(vertex)
                                               .stream()
                                               .map(line -> line.getOtherPoint(vertex));
        
        /* We now take that Stream add to it 'vertex' and compute the vector from 'point' to those vertices. The next
         * two mappings map to the double[] components of each vector's tip. Lastly, we assemble the double[]s into a
         * double[][] such that double[i][] returns the ith double[] that was stored in the double[][], hence, the
         * double[][] is a matrix of row-vectors. The Matrix constructor takes as its argument a row-vector double[][],
         * so this is convenient. */
        final double[][] arrayVectorsToPoint = Stream.concat(Stream.of(vertex), otherPoints)
                                                     .map(point::getVectorTo)
                                                     .map(NVector::getComponents)
                                                     .toArray(double[][]::new);
        
        final Matrix vectors2Point = new Matrix(arrayVectorsToPoint);
        final double det = new LUDecomposition(vectors2Point.multiplyTransposed(vectors2Point)).getDeterminant();
        return Precision.equals(det, 0.0, this.epsilon());
    }
    
    /**
     * {@inheritDoc}
     * 
     * @implNote The boundary point equals {@code internal + t * (point - internal)} for some value of t. Each dimension
     *           of the local coordinate system adds an upper bound on t (unless {@code point - internal} is parallel to
     *           the corresponding local axis vector). If {@code point - internal} has a negative component in that
     *           local dimension, then an upper bound on {@code t} is the ratio of
     *           {@code (local origin - internal) / (point - internal)} in that dimension. If {@code point - internal}
     *           has a positive component in that local dimension, then an upper bound on {@code t} is the ratio of
     *           {@code (oppositeLocalOrigin - internal) / (point - internal)} in that dimension.
     */
    @Override
    public NPoint getBoundaryPoint(final NPoint internal, final NPoint point) {
        if (this.getEmbeddedDimensionality() != this.getDimensionality()) {
            throw NumericalMethodsException.formatted("getBoundaryPoint does not currently support "
                                                          + "NOrthotopes with unequal embedded "
                                                          + "dimensionality and dimensionality.  %d != %d",
                                                      this.getEmbeddedDimensionality(),
                                                      this.getDimensionality()).get();
        }
        
        this.validateBoundaryPointParams(internal, point);
        if (this.contains(point)) {
            return point;
        }
        
        final NPoint projectedInternal = this.globalToLocal(internal);
        final NPoint projectedExternal = this.globalToLocal(point);
        
        final NVector internal2Point = projectedInternal.getVectorTo(projectedExternal);
        final NVector internal2Furthest = projectedInternal.getVectorTo(this.oppositeLocalOrigin);
        
        double t = Double.POSITIVE_INFINITY;
        for (int i = 0; i < projectedInternal.getEmbeddedDimensionality(); i++) {
            final double internal2PointComponent = internal2Point.getComponent(i);
            if (internal2PointComponent == 0.0) {
                continue;
            }
            
            final double component = internal2PointComponent < 0.0 ? projectedInternal.getComponent(i)
                                                                   : internal2Furthest.getComponent(i);
            final double componentRatio = FastMath.abs(component / internal2PointComponent);
            t = FastMath.min(t, componentRatio);
        }
        
        final NPoint projectedAnswer = new NLineSegment(projectedInternal, projectedExternal).apply(t);
        return this.localToGlobal(projectedAnswer);
    }
    
    /**
     * Translate a global {@link NPoint} to its equivalent {@link NPoint} in the local coordinate system
     * 
     * @param toTranslate the {@link NPoint} to translate
     * @return {@code toTranslate} translated to its equivalent {@link NPoint} in the local coordinate system
     */
    private NPoint globalToLocal(final NPoint toTranslate) {
        return new NPoint(this.projectionMatrix.operate(this.localOrigin.getVectorTo(toTranslate)).toArray());
    }
    
    /**
     * Translate a local {@link NPoint} to its equivalent {@link NPoint} in the global coordinate system
     * 
     * @param toTranslate the {@link NPoint} to translate
     * @return {@code toTranslate} translated to its equivalent {@link NPoint} in the global coordinate system
     */
    private NPoint localToGlobal(final NPoint toTranslate) {
        return this.localOrigin.plus(this.projectionMatrix.solve(toTranslate.asVector()));
    }
    
    /**
     * Get an axis-aligned {@link NOrthotope} with a {@link List} of intervals
     * 
     * @param intervals the intervals we want to use to create an axis-aligned {@link NOrthotope}
     * @return an axis-aligned {@link NOrthotope}
     * @implNote We gather all of the endpoints defined by the provided intervals and then perform the Cartesian product
     *           of all of those sets to produce the vertices of the axis-aligned {@link NOrthotope}. With the vertices,
     *           we find and match all of those which differ in exactly one component and create NLineSegments as the
     *           edges for the {@link NOrthotope}.
     */
    public static NOrthotope createAxisAligned(final List<DoubleInterval> intervals) {
        if (intervals == null || intervals.isEmpty()) {
            throw new ValidationException("intervals must be specified");
        }
        intervals.forEach(interval ->
                Validation.requireGreaterThan(interval.getLength(),
                                                        0.0,
                                                        "interval"));
        final int dimension = intervals.size();
        final List<NVector> cartesianProductVectors = new LinkedList<>();
        for (int i = 0; i < dimension; i++) {
            final double[] components = new double[dimension];
            Arrays.fill(components, 0.0);
            
            components[i] = intervals.get(i).getLength();
            cartesianProductVectors.add(new NVector(components));
        }
        
        final NPoint commonPoint = new NPoint(intervals.stream().mapToDouble(DoubleInterval::min).toArray());
        return cartesianProductOfVectors(commonPoint, cartesianProductVectors);
    }
    
    /**
     * Get an axis-aligned {@link NOrthotope} with a collection of doubles representing the minima and maxima of
     * intervals for each axis.
     * 
     * @param minMaxes the min/max interval values. Must specify an even number of values. Each odd-indexed value must
     *        be less than the next even-indexed value.
     * @return an axis-aligned {@link NOrthotope}
     * @implNote We gather all of the endpoints defined by the provided intervals and then perform the Cartesian product
     *           of all of those sets to produce the vertices of the axis-aligned {@link NOrthotope}. With the vertices,
     *           we find and match all of those which differ in exactly one component and create NLineSegments as the
     *           edges for the {@link NOrthotope}.
     */
    public static NOrthotope createAxisAligned(final double... minMaxes) {
        Validation.requireTrue(minMaxes.length % 2 == 0, "must specify an even number of values");
        final List<DoubleInterval> intervals = new LinkedList<>();
        for (int i = 0; i < minMaxes.length; i += 2) {
            intervals.add(new DoubleInterval(minMaxes[i], minMaxes[i + 1]));
        }
        
        return createAxisAligned(intervals);
    }
    
    /**
     * Get the unit hypercube of a particular dimension
     * 
     * @param dim the dimension for which we want the unit hypercube
     * @return the unit hypercube of a particular dimension
     */
    public static NOrthotope createUnitHypercube(final int dim) {
        Validation.requireGreaterThanEqualTo(dim, 1, "dimension");
        
        final List<DoubleInterval> intervals = new LinkedList<>();
        for (int i = 0; i < dim; i++) {
            intervals.add(new DoubleInterval(0.0, 1.0));
        }
        
        return createAxisAligned(intervals);
    }
    
    /**
     * Get the ({@code dimensionality})-facets of {@code this}
     * 
     * @param dimensionality the dimensionality of the facets of this that we want
     * @return the facets of this of dimension {@code dimensionality}
     */
    public Stream<NOrthotope> getFacets(final int dimensionality) {
        if (dimensionality < 1 || dimensionality > this.getDimensionality()) {
            throw new NumericalMethodsException("Can only request facets of dimensionality greater than 1 and "
                                                + "less than the dimensionality of the object itself");
        }
        
        if (dimensionality == 1) {
            return this.edges.stream().map(SetUtils::of).map(NOrthotope::new);
        }
        
        if (this.getDimensionality() == dimensionality) {
            return Stream.of(this);
        }
        
        return this.getVertices()
                   .parallelStream()
                   .flatMap(this::getNMinus1FacetsAtVertex)
                   .distinct()
                   .flatMap(north -> north.getFacets(dimensionality))
                   .distinct();
    }
    
    /**
     * <p>
     * Get the ({@link #getDimensionality()}-1) dimensional facets of this {@link NOrthotope}'s boundary that abut
     * {@code vertex}
     * </p>
     * <p>
     * The facets each exist in distinct "hyperplanes;" i.e. they all exist in the same
     * {@link #getDimensionality()}-space as this, but different subspaces of dimension {@link #getDimensionality()} - 1
     * </p>
     * <p>
     * Each facet is the (modified) Cartesian product of all-but-one of the edges that meet at the query vertex. For
     * each edge, we exclude that edge and perform the Cartesian product of each remaining edge to get a facet.
     * </p>
     * 
     * @param vertex the vertex for which we want abutting facets
     * @return the ({@link #getDimensionality()}-1) dimensional facets of this {@link NOrthotope}'s boundary that abut
     *         {@code vertex}
     * @implNote We want to keep processing here to a minimum because it's expensive stuff, so we have a cache of
     *           vertices to vectors at those vertices to the orthotopes created when we consider a vertex and its
     *           vectors. In addition, we only Stream the Cartesian product results here so that the caller can
     *           intelligently short-circuit if applicable. So we have the vertex key as a parameter, we create the
     *           vertexVectors key, then we need that complicated mess of computeIfAbsents to deal with the nested Map
     *           cache.
     */
    // CHECKSTYLE.OFF: LineLength
    Stream<NOrthotope> getNMinus1FacetsAtVertex(final NPoint vertex) {
        final List<NVector> vertexEdges = this.verticesToTheirEdges.get(vertex)
                                                                   .stream()
                                                                   .map(edge -> edge.getOtherPoint(vertex))
                                                                   .map(vertex::getVectorTo)
                                                                   .collect(Collectors.toCollection(ArrayList::new));
        final Combinations intArrCombos = new Combinations(vertexEdges.size(), vertexEdges.size() - 1);
        final Function<int[], List<NVector>> intArrToCombination = intArrCombo -> {
            final List<NVector> combo = new ArrayList<>(intArrCombo.length);
            for (int i = 0; i < intArrCombo.length; i++) {
                final int index = intArrCombo[i];
                combo.add(vertexEdges.get(index));
            }
            return combo;
        };
        
        final Function<List<NVector>, NOrthotope> combinationToNOrthotope = 
                combination -> this.vertex2Vectors2Facets.computeIfAbsent(vertex, i -> new ConcurrentHashMap<>())
                                                         .computeIfAbsent(combination, i -> cartesianProductOfVectors(vertex, combination));
        
        return StreamUtils.sequential(intArrCombos)
                          .map(intArrToCombination)
                          .map(combinationToNOrthotope);
    }
    // CHECKSTYLE.ON: LineLength
    
    /**
     * Create the {@link NOrthotope} that is the result of taking the Cartesian product of each of the provided
     * {@link NVector}s anchored at {@code baseVertex}.
     * 
     * @param baseVertex the anchoring vertex of the {@link NOrthotope} being created
     * @param vectors the {@link NVector}s to include in the product
     * @return the {@link NOrthotope} that is the result of taking the product of each of the provided {@link NVector}s
     *         which is anchored to {@code baseVertex}
     * @implNote in this context, Cartesian product is an abuse of notation/jargon/etc. The result of this method is the
     *           result of starting at {@code baseVertex}, extending that by the first {@link NVector} in the
     *           {@link Collection} to get a line, extending that by the next vector in the collection to get a
     *           rectangle, etc.
     * @see Numerics#cartesianProduct(List) this for a set-theoretical definition of the Cartesian product.
     */
    static NOrthotope cartesianProductOfVectors(final NPoint baseVertex, final Collection<NVector> vectors) {
        NOrthotope current = null;
        for (final NVector vector : vectors) {
            if (current == null) {
                final NLineSegment firstEdge = new NLineSegment(baseVertex, baseVertex.plus(vector));
                current = new NOrthotope(SetUtils.of(firstEdge));
            }
            else {
                current = current.cartesianProduct(vector);
            }
        }
        
        return current;
    }
    
    /**
     * Compute the result of the Cartesian product of this {@link NOrthotope} with the provided {@link NVector}.
     * 
     * @param vector the {@link NVector} with which we wish to perform a Cartesian product.
     * @return the result of the Cartesian product of this {@link NOrthotope} with the provided {@link NLineSegment}
     * @implNote The algorithm here should work for vector and NDomains of any type and orientation - that have vertices
     *           - but for the sake of the NOrthotope contract we will not allow non-orthogonal translation.
     * @see Numerics#cartesianProduct(List)
     * @see NOrthotope#cartesianProductOfVectors(NPoint, Collection)
     */
    public NOrthotope cartesianProduct(final NVector vector) {
        Objects.requireNonNull(vector, "segment must be specified");
        Validation.requireEqualsTo(vector.getEmbeddedDimensionality(),
                             this.getEmbeddedDimensionality(),
                             "cannot perform a Cartesian product with a vector of different embedded dimensionality");
        
        final Set<NLineSegment> newEdges = new LinkedHashSet<>();
        for (final NLineSegment edge : this.edges) {
            final NPoint vertex1 = edge.getEndPoint1();
            final NPoint vertex2 = edge.getEndPoint2();
            final NPoint newVertex1 = vertex1.plus(vector);
            final NPoint newVertex2 = vertex2.plus(vector);
            
            addSegment(vertex1, vertex2, newEdges);
            addSegment(vertex1, newVertex1, newEdges);
            addSegment(vertex2, newVertex2, newEdges);
            addSegment(newVertex1, newVertex2, newEdges);
        }
        return new NOrthotope(newEdges);
    }
    
    /**
     * Possibly add a new {@link NLineSegment} to the {@code handled} set. We only add to existing set if the segment,
     * or its flipped equivalent, is in none of them.
     * 
     * @param endPt1 the first end-point
     * @param endPt2 the second end-point
     * @param handled the set of {@link NLineSegment} edges that we have already created
     */
    private static void addSegment(final NPoint endPt1, final NPoint endPt2, final Set<NLineSegment> handled) {
        final NLineSegment segment1 = new NLineSegment(endPt1, endPt2);
        final NLineSegment segment2 = new NLineSegment(endPt2, endPt1);
        if (!handled.contains(segment1) && !handled.contains(segment2)) {
            handled.add(segment1);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder answer = new StringBuilder();
        answer.append("edges:").append(System.lineSeparator());
        this.edges.forEach(i -> answer.append("    ").append(i.toString()).append(System.lineSeparator()));
        
        answer.append("vertices:").append(System.lineSeparator());
        this.vertices.forEach(v -> {
            answer.append("    ");
            for (int i = 0; i < v.getEmbeddedDimensionality(); i++) {
                answer.append(String.format("%.3f, ", v.getComponent(i)));
            }
            
            answer.append(System.lineSeparator());
        });
        
        answer.append("dimensionality: ")
              .append(this.getDimensionality())
              .append(", embedded dimensionality: ")
              .append(this.getEmbeddedDimensionality())
              .append(", measure: ")
              .append(this.getMeasure())
              .append(System.lineSeparator());
        return answer.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.vertices); // the vertices uniquely determine the NOrthotope
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        
        final NOrthotope other = (NOrthotope) obj;
        return this.vertices.equals(other.vertices);
    }
}
