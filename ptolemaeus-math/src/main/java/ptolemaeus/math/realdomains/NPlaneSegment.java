/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import java.util.Iterator;
import java.util.Optional;

import org.hipparchus.util.Precision;

import ptolemaeus.commons.collections.SetUtils;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

/**
 * A class to represent rectangular plane segments of any embedded dimensionality. We only support rectangular plane
 * segments at the moment because this class extends from {@link NOrthotope}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public final class NPlaneSegment extends NOrthotope {
    
    /**
     * Constructor. Specify four edges to get a {@link NPlaneSegment}. See the class-level JavaDoc
     * ({@link NPlaneSegment}) to see why all edges must be orthogonal to the edges that they touch.
     * 
     * @param edge1 the first edge of the plane segment
     * @param edge2 the second edge of the plane segment
     * @param edge3 the third edge of the plane segment
     * @param edge4 the fourth edge of the plane segment
     */
    public NPlaneSegment(final NLineSegment edge1,
                         final NLineSegment edge2,
                         final NLineSegment edge3,
                         final NLineSegment edge4) {
        this(edge1, edge2, edge3, edge4, AbstractRealNDomain.DEFAULT_EPSILON);
    }
    
    /**
     * Constructor. Specify four edges to get a {@link NPlaneSegment}. See the class-level JavaDoc
     * ({@link NPlaneSegment}) to see why all edges must be orthogonal to the edges that they touch.
     * 
     * @param edge1 the first edge of the plane segment
     * @param edge2 the second edge of the plane segment
     * @param edge3 the third edge of the plane segment
     * @param edge4 the fourth edge of the plane segment
     * @param epsilon a small absolute {@code double} tolerance that we can use to compare whether {@code double}
     *        values are equal according to {@link Precision#equals(double, double, double)}
     */
    public NPlaneSegment(final NLineSegment edge1,
                         final NLineSegment edge2,
                         final NLineSegment edge3,
                         final NLineSegment edge4,
                         final double epsilon) {
        super(SetUtils.of(edge1, edge2, edge3, edge4), epsilon);
    }
    
    /**
     * <p>
     * Compute the line-plane intersection of this {@link NPlaneSegment} and the line formed by the provided points.
     * </p>
     * <p>
     * To do this, we first check if {@code this} contains either endpoint of the line. If it does, we return that
     * endpoint. Else, we solve the following overdetermined system of equations. If there is a solution to the
     * overdetermined system, then we check that the solution produces a point on the line segment and in the plane
     * segment. If it does, we return the point; if not, we return an empty {@link Optional}.
     * </p>
     * <p>
     * Vector equation of a line: L<sub>q0, q1</sub>(t) = q0 + <b>q0q1</b>t
     * </p>
     * <p>
     * Vector equation of a plane: P<sub>p0, p1, p2</sub>(u, v) = p0 + <b>p0p1</b>u + <b>p0p2</b>v where t, u, v &#8712;
     * [0, 1] for points within the line segment or plane segment.
     * </p>
     * <p>
     * and <b>l0l1</b>, <b>p0p1</b>, and <b>p0p2</b> are the vectors (q0 - q1), (p1 - p0), and (p2 - p0) respectively
     * </p>
     * Set these equal and rearrange into a column-matrix expression:
     *
     * <pre>
     * [p0q0] = [p0p1 p0p2 q1q0] | u |
     *                           | v |
     *                           | t |
     * </pre>
     * 
     * @param q0 the first point of the line on which we wish to find an intersection
     * @param q1 the second point of the line on which we wish to find an intersection
     * @return the line-plane intersection of this {@link NOrthotope} - assumed to have {@link #getDimensionality()} ==
     *         2 and, hence, be a plane - and the line formed by the provided points
     */
    // CHECKSTYLE.OFF: LineLength
    public Optional<NPoint> calculatePlaneLineIntersection(final NPoint q0, final NPoint q1) {
        this.verifyCompatibleDimensionality(q0);
        this.verifyCompatibleDimensionality(q1);
        
        final NPoint possibleSoln = intersectsCoplanarOrParallel(q0, q1);
        if (possibleSoln != null) {
            return Optional.of(possibleSoln);
        }
        
        // gather the 5 points: the 2 from the line we already have, these are the 3 from the plane
        final Iterator<NPoint> verticesItr = this.getVertices().iterator();
        final NPoint intersection = calculatePlaneLineIntersection(q0,
                                                                   q1,
                                                                   verticesItr.next(),
                                                                   verticesItr.next(),
                                                                   verticesItr.next(),
                                                                   this.epsilon());
        
        if (intersection == null) {
            return Optional.empty();
        }
        
        return Optional.of(intersection);
    }
    // CHECKSTYLE.ON: LineLength
    
    /**
     * <p>
     * Check to see whether this {@link NPlaneSegment} contains either endpoint of the {@link NLineSegment} formed by
     * the two provided {@link NPoint}s. If it does, return that point in an {@link Optional}.
     * </p>
     * <p>
     * If neither point is contained, we check to see if the line-segment is parallel to the plane-segment and
     * intersects the plane-segment on one or more of its edges. If it does, we return the first intersection with an
     * edge that we find.
     * </p>
     * <p>
     * If none of the above, we return {@code null}
     * </p>
     * 
     * @param q0 the first query point
     * @param q1 the second query point
     * @return the intersection point or {@code null}
     */
    private NPoint intersectsCoplanarOrParallel(final NPoint q0, final NPoint q1) {
        if (this.contains(q0)) {
            return q0;
        }
        
        if (this.contains(q1)) {
            return q1;
        }
        
        for (final NLineSegment edge : this.getEdges()) {
            final Optional<NPoint> answer = edge.calculateIntersection(new NLineSegment(q0, q1));
            if (answer.isPresent()) {
                return answer.get();
            }
        }
        
        return null;
    }
    
    /**
     * <p>
     * Calculate the intersection {@link NPoint} of a {@link NLineSegment} and a planar {@link NOrthotope}. We assume
     * that each point is distinct and that the strictly the planar points are coplanar; i.e., the line and plane are
     * not coincident - we assume that we have handled this degenerate case prior to calling this method using
     * {@link #intersectsCoplanarOrParallel(NPoint, NPoint)}
     * </p>
     * <p>
     * Self-derived , but you can find a derivation that matches this here:
     * <a href="https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection">Line-Plane Intersection - Parametric
     * Form</a>.
     * </p>
     * Short explanation:
     *
     * <pre>
     * Vector equation of a line: L<sub>q0, q1</sub>(t) = q0 + <b>q0q1</b>t
     * Vector equation of a plane: P<sub>p0, p1, p2</sub>(u, v) = p0 + <b>p0p1</b>u + <b>p0p2</b>v
     * where t, u, v &#8712; [0, 1]
     * and <b>q0q1</b>, <b>p0p1</b>, and <b>p0p2</b> are the vectors (q0 - q1), (p1 - p0), and (p2 - p0) respectively
     * </pre>
     *
     * Set these equal and rearrange into a column-matrix expression:
     *
     * <pre>
     * [p0q0] = [p0p1 p0p2 q1q0] | u |
     *                           | v |
     *                           | t |
     * </pre>
     *
     * This can then be solved in the usual way.
     *
     * @param q0 the first point of the line
     * @param q1 the second point of the line
     * @param p0 one point in the plan
     * @param p1 another point in the plan
     * @param p2 another another point in the plan
     * @param epsilon a small absolute {@code double} tolerance that we can use to compare whether {@code double}
     *        values are equal according to {@link Precision#equals(double, double, double)}
     * @return the intersection {@link NPoint} of a {@link NLineSegment} and a planar {@link NOrthotope}
     */
    static NPoint calculatePlaneLineIntersection(final NPoint q0,
                                                 final NPoint q1,
                                                 final NPoint p0,
                                                 final NPoint p1,
                                                 final NPoint p2,
                                                 final double epsilon) {
        // Create the left-hand side of the matrix equation Ax = B.  Transpose to get a column-vector matrix.
        final Matrix lhs = new Matrix(new double[][] { p0.getVectorTo(p1).getComponents(),
                                                       p0.getVectorTo(p2).getComponents(),
                                                       q1.getVectorTo(q0).getComponents() }).transpose();
        
        final NVector rhs = p0.getVectorTo(q0); // already a column vector
        
        final NVector solutionMatrix = lhs.solve(rhs);
        final double[] solution = solutionMatrix.getComponents(); /* The solution vector will contain the answers for
                                                                   * each variable is the order that they appear in the
                                                                   * RHS Matrix; i.e., u, v, t, where u, v are the
                                                                   * variables of the planar expression and t is the
                                                                   * variable of the linear expression. */
        
        final double min = Numerics.min(solution[0], solution[1], solution[2]);
        final double max = Numerics.max(solution[0], solution[1], solution[2]);
        
        if (min < 0 - Numerics.MACHINE_EPSILON || max > 1 + Numerics.MACHINE_EPSILON) {
            /* if t is not in [0, 1], the intersection does not lie on the line segment. Similarly, if u or v are not in
             * [0, 1], the intersection does not lie on the plane segment defined by the three provided planar points.
             * We allow for very slight wiggle-room because of finite precision limitations. */
            return null;
        }
        
        final NPoint planarIntersection = p0.plus(p0.getVectorTo(p1).times(solution[0]))
                                            .plus(p0.getVectorTo(p2).times(solution[1]));
        final NPoint answer = q0.plus(q0.getVectorTo(q1).times(solution[2]));
        
        if (!answer.equals(planarIntersection, epsilon)) {
            /* The matrix problem was overdetermined and solution represents the best possible solution but not an exact
             * match.  Possible that the plane-segment and line-segment are skew in a 4+ dimensional space. */
            return null;
        }
        
        return answer;
    }
}
