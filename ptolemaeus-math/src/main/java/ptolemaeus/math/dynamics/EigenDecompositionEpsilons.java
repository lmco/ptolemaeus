/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.dynamics;

import org.hipparchus.linear.ComplexEigenDecomposition;
import org.hipparchus.linear.RealMatrix;

/** A record to hold all of the epsilons that {@link ComplexEigenDecomposition} uses
 * 
 * @param epsilonEigenVectorsEquality threshold below which Eigenvectors are considered equal in
 *        {@link ComplexEigenDecomposition#ComplexEigenDecomposition(RealMatrix, double, double, double)}
 * @param epsilon threshold below which double values are considered equal in
 *        {@link ComplexEigenDecomposition#ComplexEigenDecomposition(RealMatrix, double, double, double)}
 *        
 *        <br>
 *        <br>
 * 
 *        From the {@link ComplexEigenDecomposition} documentation:<br>
 *        This is to ensure the normalized eigenvectors found using inverse iteration are different from each other. If
 *        \(min(|e_i-e_j|,|e_i+e_j|)\) is smaller than this threshold, the algorithm considers it has found again an
 *        already known vector, so it drops it and attempts a new inverse iteration with a different start vector. This
 *        value should be much larger than {@code epsilon} which is used for convergence
 *        
 * @param epsilonAVVDCheck Epsilon criteria for final {@code AV = VD} check in
 *        {@link ComplexEigenDecomposition#ComplexEigenDecomposition(RealMatrix, double, double, double)}
 *        
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public record EigenDecompositionEpsilons(double epsilonEigenVectorsEquality,
                                         double epsilon,
                                         double epsilonAVVDCheck) {
    
    /** The default {@link EigenDecompositionEpsilons} */
    public static final EigenDecompositionEpsilons DEFAULT =
            new EigenDecompositionEpsilons(ComplexEigenDecomposition.DEFAULT_EIGENVECTORS_EQUALITY,
                                           ComplexEigenDecomposition.DEFAULT_EPSILON,
                                           ComplexEigenDecomposition.DEFAULT_EPSILON_AV_VD_CHECK);
}
