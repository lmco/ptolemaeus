/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.dynamics;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.LocalizedCoreFormats;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.ComplexEigenDecomposition;
import org.hipparchus.linear.OrderedComplexEigenDecomposition;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.eigen.EigenPair;

/** A {@link StateTransitionMatrix state transition matrix} associated with a given duration is a matrix which
 * multiplies a state vector to yield the result of the evolution of that state vector for that duration given a
 * dynamical system. Evaluated for zero time, the the STM is the identity matrix.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see StateTransitionMatrixFunction */
public class StateTransitionMatrix {
    
    /** the state transition matrix itself */
    protected final RealMatrix matrix;
    /** the lazily-initialized {@link OrderedComplexEigenDecomposition Eigen decomposition} of this
     * {@link StateTransitionMatrix} */
    private final AtomicReference<OrderedComplexEigenDecomposition> eigenDecomp;
    /** the {@link EigenPair}s associated with this {@link StateTransitionMatrix} */
    private final AtomicReference<List<EigenPair>> eigenPairs;
    /** the record of {@link EigenDecompositionEpsilons epsilons} that we'll pass to
     * {@link ComplexEigenDecomposition} */
    private final EigenDecompositionEpsilons eps;
    
    /** Constructor
     * 
     * @param matrix the state transition matrix itself
     * @param eps the record of {@link EigenDecompositionEpsilons epsilons} that we'll pass to
     *        {@link ComplexEigenDecomposition} */
    protected StateTransitionMatrix(final RealMatrix matrix, final EigenDecompositionEpsilons eps) {
        requireNonNull(matrix, "a matrix must be specified");
        if (!matrix.isSquare()) {
            throw new MathIllegalArgumentException(LocalizedCoreFormats.NON_SQUARE_MATRIX,
                                                   matrix.getRowDimension(),
                                                   matrix.getColumnDimension());
        }
        
        this.matrix      = matrix;
        this.eigenDecomp = new AtomicReference<>(null);
        this.eigenPairs  = new AtomicReference<>(null);
        this.eps         = eps;
    }
    
    /** @return the state transition matrix itself, unwrapped from this instance */
    public RealMatrix getMatrix() {
        return this.matrix.copy();
    }
    
    /** @return the {@link OrderedComplexEigenDecomposition} of this {@link StateTransitionMatrix} */
    public OrderedComplexEigenDecomposition getEigenDecomposition() {
        if (this.eigenDecomp.get() == null) {
            this.eigenDecomp.set(this.computeEigenDecomposition());
        }
        
        return this.eigenDecomp.get();
    }
    
    /** Compute and return the {@link OrderedComplexEigenDecomposition} of this {@link StateTransitionMatrix}
     * @return the {@link OrderedComplexEigenDecomposition} of this {@link StateTransitionMatrix} */
    protected OrderedComplexEigenDecomposition computeEigenDecomposition() {
        return new OrderedComplexEigenDecomposition(this.matrix,
                                                    this.eps.epsilonEigenVectorsEquality(),
                                                    this.eps.epsilon(),
                                                    this.eps.epsilonAVVDCheck());
    }
    
    /** @return the {@link EigenPair}s associated with this {@link StateTransitionMatrix} */
    public List<EigenPair> getEigenPairs() {
        if (this.eigenPairs.get() == null) {
            this.eigenPairs.set(this.computeEigenPairs());
        }
        return this.eigenPairs.get();
    }
    
    /** Compute and return the {@link EigenPair}s associated with this {@link StateTransitionMatrix}
     * @return the {@link EigenPair}s associated with this {@link StateTransitionMatrix} */
    protected List<EigenPair> computeEigenPairs() {
        final OrderedComplexEigenDecomposition eignedDecomp = this.getEigenDecomposition();
        final Complex[] evs = eignedDecomp.getEigenvalues();
        
        final List<EigenPair> ePairs = new ArrayList<>(this.matrix.getColumnDimension());
        for (int i = 0; i < evs.length; i++) {
            final ComplexVector eigenVector = new ComplexVector(eignedDecomp.getEigenvector(i));
            final EigenPair ePair = new EigenPair(evs[i], eigenVector);
            ePairs.add(ePair);
        }
        
        return List.copyOf(ePairs);
    }
}
