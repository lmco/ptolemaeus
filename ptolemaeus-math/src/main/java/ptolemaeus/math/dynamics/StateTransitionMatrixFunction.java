/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.dynamics;

import static java.util.Objects.requireNonNull;

import java.util.Optional;

import org.hipparchus.linear.ComplexEigenDecomposition;
import org.hipparchus.linear.OrderedComplexEigenDecomposition;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;

/** A {@link StateTransitionMatrixFunction} computes {@link StateTransitionMatrix state transition matrices} for the
 * given {@link DynamicalSystem}.
 * 
 * <br>
 * <br>
 * 
 * The state transition matrix of a {@link DynamicalSystem} is the linearization of the {@link DynamicalSystem}
 * evaluated for some time and at some point; i.e., the Jacobian of the system w.r.t spatial coordinates and evaluated
 * for the given duration.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class StateTransitionMatrixFunction {
    
    /** the {@link DynamicalSystem} for which we want to compute {@link StateTransitionMatrix state transition
     * matrices} */
    final DynamicalSystem dynamicalSystem;
    /** the (approximate) differentials to use when computing the Jacobian of the dynamical system w.r.t. varying input
     * parameters */
    private final double[] dxs;
    
    /** the record of {@link EigenDecompositionEpsilons epsilons} that we'll pass to
     * {@link ComplexEigenDecomposition} */
    private final EigenDecompositionEpsilons eps;
    
    /** the {@link Parallelism} with which we want to compute the constituents gradients of the
     * {@link StateTransitionMatrix}.  {@link Parallelism#PARALLEL} is a safe bet. */
    private final Parallelism parallelism;
    
    /** Constructor
     * 
     * @param dynamicalSystem the {@link DynamicalSystem} for which we want to compute {@link StateTransitionMatrix
     *        state transition matrices} */
    public StateTransitionMatrixFunction(final DynamicalSystem dynamicalSystem) {
        this(dynamicalSystem, null, EigenDecompositionEpsilons.DEFAULT, null);
    }
    
    /** Constructor
     * 
     * @param dynamicalSystem the {@link DynamicalSystem} for which we want to compute {@link StateTransitionMatrix
     *        state transition matrices}
     * @param dxs the (approximate) differentials to use when computing the {@link StateTransitionMatrix}.
     *        May be {@code null}.  If {@code null}, we use default, auto-scaling DXs 
     *        (see {@link Numerics#getJacobianFunction(MultivariateVectorValuedFunction,
     *                                                              Optional, int, int, Parallelism)})
     * @param eps the record of {@link EigenDecompositionEpsilons epsilons} that we'll pass to
     *        {@link ComplexEigenDecomposition}
     * @param parallelism the {@link Parallelism} with which we want to compute the constituents gradients of the
     * {@link StateTransitionMatrix}.  {@link Parallelism#PARALLEL} is a safe bet.  If {@code null}, we default to 
     * {@link Parallelism#SEQUENTIAL} in 
     * {@link Numerics#getJacobianFunction(MultivariateVectorValuedFunction, Optional,
     *                                                  int, int, Parallelism)}.
     * 
     * @see OrderedComplexEigenDecomposition#OrderedComplexEigenDecomposition(RealMatrix, double, double, double) */
    public StateTransitionMatrixFunction(final DynamicalSystem dynamicalSystem,
                                         final double[] dxs,
                                         final EigenDecompositionEpsilons eps,
                                         final Parallelism parallelism) {
        this.dynamicalSystem = dynamicalSystem;
        this.dxs             = dxs == null ? null : dxs.clone();
        this.eps             = eps;
        this.parallelism     = parallelism;
    }
    
    /** Compute the {@link StateTransitionMatrix} at the given time for the given initial condition
     * 
     * @param t the time at which we want to compute the {@link StateTransitionMatrix}
     * @param initialCondition the initial condition
     * @return the {@link StateTransitionMatrix} at the given time for the given initial condition */
    public StateTransitionMatrix apply(final double t, final double[] initialCondition) {
        requireNonNull(initialCondition, "An initial condition array must be specified");
        
        final int dim = initialCondition.length;
        final MultivariateVectorValuedFunction f = x -> this.dynamicalSystem.evaluate(t, x);
        final MultivariateMatrixValuedFunction jacobian =
                Numerics.getJacobianFunction(f, Optional.ofNullable(this.dxs), dim, dim, this.parallelism);
        final RealMatrix stm = jacobian.apply(initialCondition);
        
        return new StateTransitionMatrix(stm, this.eps);
    }
}
