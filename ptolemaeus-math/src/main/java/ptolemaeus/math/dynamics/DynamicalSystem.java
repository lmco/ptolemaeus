/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.dynamics;

/** A {@link DynamicalSystem} takes initial conditions and a time and returns the state of the system evaluated at that
 * time (given the initial conditions).
 * 
 * The value {@code time} does not necessarily have to be {@code time}, but any real-valued input. {@code time} is just
 * the common use-case and convenient for discussion.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
@FunctionalInterface
public interface DynamicalSystem {
    
    /** Evaluate the dynamical system at the time {@code t} given the initial conditions {@code x}
     * @param t the time at which we want the state
     * @param x the initial conditions
     * 
     * @return the state of this {@link DynamicalSystem} at time {@code t} */
    double[] evaluate(double t, double[]x);
}
