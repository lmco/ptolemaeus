/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import java.util.function.DoubleUnaryOperator;

import ptolemaeus.math.realdomains.RealNDomain;

/**
 * A function f : R -&gt; R
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class UnivariateScalarValuedFunction extends AbstractRealFunction implements DoubleUnaryOperator {
    
    /**
     * the {@link DoubleUnaryOperator} this this mirrors
     */
    private final DoubleUnaryOperator f;
    
    /**
     * @param f the {@link DoubleUnaryOperator} that this mirrors
     * @param inputDomain the input domain of this {@link UnivariateScalarValuedFunction}
     */
    public UnivariateScalarValuedFunction(final DoubleUnaryOperator f, final RealNDomain inputDomain) {
        super(1, 1, inputDomain);
        this.f = f;
    }
    
    /**
     * @param f the {@link DoubleUnaryOperator} this this mirrors
     */
    public UnivariateScalarValuedFunction(final DoubleUnaryOperator f) {
        this(f, null);
    }
    
    @Override
    public double applyAsDouble(final double operand) {
        return f.applyAsDouble(operand);
    }
}
