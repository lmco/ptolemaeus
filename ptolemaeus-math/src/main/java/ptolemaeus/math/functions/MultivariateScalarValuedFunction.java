/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import java.util.function.Function;
import java.util.function.ToDoubleFunction;

import ptolemaeus.math.realdomains.RN;
import ptolemaeus.math.realdomains.RealNDomain;

/**
 * A function f : R<sup>N</sup> -&gt; R
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class MultivariateScalarValuedFunction extends AbstractRealFunction
                                              implements ToDoubleFunction<double[]>, Function<double[], Double> {
    
    /**
     * The underlying function of this {@link MultivariateScalarValuedFunction}
     */
    private final ToDoubleFunction<double[]> f;
    
    /**
     * @param f the underlying function of the {@link MultivariateScalarValuedFunction}
     * @param inputDimension the input dimension of the {@link MultivariateScalarValuedFunction}
     */
    public MultivariateScalarValuedFunction(final ToDoubleFunction<double[]> f, final int inputDimension) {
        super(inputDimension, 1, null);
        this.f = f;
    }
    
    /**
     * @param f the underlying function of the {@link MultivariateScalarValuedFunction}
     * @param inputDomain the input domain of this {@link MultivariateScalarValuedFunction} or null to use the
     *        appropriate {@link RN}
     */
    public MultivariateScalarValuedFunction(final ToDoubleFunction<double[]> f, final RealNDomain inputDomain) {
        super(inputDomain.getEmbeddedDimensionality(), 1, inputDomain);
        this.f = f;
    }
    
    @Override
    public double applyAsDouble(final double[] x) {
        validateDimensions(x);
        return f.applyAsDouble(x);
    }

    @Override
    public Double apply(final double[] x) {
        return applyAsDouble(x);
    }
}
