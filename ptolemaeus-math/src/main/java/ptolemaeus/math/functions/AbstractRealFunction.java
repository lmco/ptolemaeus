/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import java.util.Optional;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.realdomains.RN;
import ptolemaeus.math.realdomains.RealNDomain;

/**
 * An abstract class to hold information common to real-valued functions
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public abstract class AbstractRealFunction {
    
    /**
     * An error message for when a function input is provided that does not match the input dimension
     */
    private static final String DIMENSION_MISMATCH_MSG = "The number of components in an input for a "
                                                         + "AbstractRealFunction must be the same as the "
                                                         + "dimension of the domain of the AbstractRealFunction";
    
    /**
     * The input dimension of this {@link AbstractRealFunction}
     */
    private final int inputDim;
    
    /**
     * The output dimension of this {@link AbstractRealFunction}
     */
    private final int outputDim;
    
    /**
     * The input domain of this {@link AbstractRealFunction}. The function may or may not evaluate without issue outside
     * of this domain, but it must evaluate without throwing exceptions within this domain.
     */
    private final RealNDomain inputDomain;
    
    /**
     * @param inputDimension the input dimension of the {@link AbstractRealFunction}
     * @param outputDimension the output dimension of the {@link AbstractRealFunction}
     * @param inputDomain the input domain of this {@link AbstractRealFunction} or null to use the appropriate
     *        {@link RN}. The function may or may not evaluate without issue outside of this domain, but it must
     *        evaluate without throwing exceptions within this domain.
     */
    AbstractRealFunction(final int inputDimension, final int outputDimension, final RealNDomain inputDomain) {
        this.inputDim = Validation.requireGreaterThan(inputDimension, 0, "input dimension");
        this.outputDim = Validation.requireGreaterThan(outputDimension, 0, "output dimension");
        this.inputDomain = Optional.ofNullable(inputDomain).orElseGet(() -> new RN(this.inputDim));
        Validation.requireEqualsTo(this.inputDim, this.inputDomain.getEmbeddedDimensionality(), "input dimension");
    }
    
    /**
     * @return the input dimension of this {@link AbstractRealFunction}
     */
    public int getInputDimension() {
        return this.inputDim;
    }
    
    /**
     * @return output dimension of this {@link AbstractRealFunction}
     */
    public int getOutputDimension() {
        return this.outputDim;
    }
    
    /** @return the input {@link RealNDomain} of this {@link AbstractRealFunction}. Never {@code null}. The function may
     *         or may not evaluate without issue outside of this domain, but it must evaluate without throwing
     *         exceptions within this domain. */
    public RealNDomain getInputDomain() {
        return this.inputDomain;
    }
    
    /**
     * Validate that the dimension of an array is compatible with the dimension of the input of this
     * {@link AbstractRealFunction}
     * 
     * @param x the array to validate
     */
    public void validateDimensions(final double[] x) {
        if (this.inputDim != x.length) {
            throw new IllegalArgumentException(DIMENSION_MISMATCH_MSG);
        }
    }
}
