/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import static java.util.Objects.requireNonNull;

import ptolemaeus.math.realdomains.RN;
import ptolemaeus.math.realdomains.RealNDomain;

/** A function f : R<sup>N</sup> -&gt; R<sup>M</sup>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class VectorField extends AbstractRealFunction implements MultivariateVectorValuedFunction {
    
    /** The underlying function of this {@link VectorField} */
    private final MultivariateVectorValuedFunction f;
    
    /** @param fParam the underlying function of the {@link VectorField}
     * @param inputDimension the input dimension of the {@link VectorField}
     * @param outputDimension the output dimension of the {@link VectorField} */
    public VectorField(final MultivariateVectorValuedFunction fParam,
                       final int inputDimension,
                       final int outputDimension) {
        this(fParam, inputDimension, outputDimension, null);
    }
    
    /** @param fParam the underlying function of the {@link VectorField}
     * @param inputDimension the input dimension of the {@link VectorField}
     * @param outputDimension the output dimension of the {@link VectorField}
     * @param inputDomain the input domain of this {@link VectorField} or null to use the appropriate {@link RN}
     * @apiNote no validation is done on the {@code jacobian} parameter. <b>If it is wrong, it may go unnoticed!</b> */
    public VectorField(final MultivariateVectorValuedFunction fParam,
                       final int inputDimension,
                       final int outputDimension,
                       final RealNDomain inputDomain) {
        super(inputDimension, outputDimension, inputDomain);
        this.f = requireNonNull(fParam, "a function must be specified");
    }
    
    @Override
    public double[] apply(final double[] x) {
        validateDimensions(x);
        return f.apply(x);
    }
}
