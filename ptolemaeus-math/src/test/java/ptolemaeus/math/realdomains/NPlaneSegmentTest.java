/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.collections.SetUtils;

/**
 * Test {@link NPlaneSegment}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class NPlaneSegmentTest {
    
    /** Test {@link NPlaneSegment#NPlaneSegment(NLineSegment, NLineSegment, NLineSegment, NLineSegment, double)} and
     *       {@link NPlaneSegment#NPlaneSegment(NLineSegment, NLineSegment, NLineSegment, NLineSegment)} */
    @Test
    void testConstructors() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        final NPlaneSegment plane1 = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPlaneSegment plane2 = new NPlaneSegment(seg1, seg2, seg3, seg4, 1e-9);
        
        assertEquals(plane1.getEdges(), SetUtils.of(seg1, seg2, seg3, seg4));
        assertEquals(AbstractRealNDomain.DEFAULT_EPSILON, plane1.epsilon());
        
        assertEquals(plane2.getEdges(), SetUtils.of(seg1, seg2, seg3, seg4));
        assertEquals(1e-9, plane2.epsilon());
    }
    
    /** Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint, NPoint, NPoint, NPoint, double)} where
     * the line intersects the plane perpendicularly at a vertex */
    @Test
    void testCalculatePlaneIntersection1() {
        final NPoint intersection = NPlaneSegment.calculatePlaneLineIntersection(new NPoint(1, 1, -1), // line point 1
                                                                                 new NPoint(1, 1, 1),  // line point 2
                                                                                 new NPoint(1, 1, 0),  // plane point 1
                                                                                 new NPoint(2, 2, 0),  // plane point 2
                                                                                 new NPoint(1, 3, 0),  // plane point 3
                                                                                 AbstractRealNDomain.DEFAULT_EPSILON);
        assertEquals(new NPoint(1, 1, 0), intersection);
    }
    
    /** Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint, NPoint, NPoint, NPoint, double)} where
     * the line intersects the plane - perpendicular to the XY-plane - perpendicularly at the center */
    @Test
    void testCalculatePlaneIntersection2() {
        final NPoint intersection = NPlaneSegment.calculatePlaneLineIntersection(new NPoint(1, 2, 0), // line point 1
                                                                                 new NPoint(1, 2, 2), // line point 2
                                                                                 new NPoint(1, 1, 1), // plane point 1
                                                                                 new NPoint(2, 2, 1), // plane point 2
                                                                                 new NPoint(1, 3, 1), // plane point 3
                                                                                 AbstractRealNDomain.DEFAULT_EPSILON);
        assertEquals(new NPoint(1, 2, 1), intersection);
    }
    
    /** Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint, NPoint, NPoint, NPoint, double)} where
     * the line intersects the plane - perpendicular to the XY-plane - perpendicularly at the center but where we also
     * must reduce the vectors to a lower dimensionality */
    @Test
    void testCalculatePlaneIntersection3() {
        final NPoint intersection = NPlaneSegment.calculatePlaneLineIntersection(new NPoint(1, 2, 0), // line point 1
                                                                                 new NPoint(1, 2, 2), // line point 2
                                                                                 new NPoint(1, 1, 1), // plane point 1
                                                                                 new NPoint(2, 2, 1), // plane point 2
                                                                                 new NPoint(1, 3, 1), // plane point 3
                                                                                 AbstractRealNDomain.DEFAULT_EPSILON);
        assertEquals(new NPoint(1, 2, 1), intersection);
    }
    
    /** Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint, NPoint, NPoint, NPoint, double)} */
    @Test
    void testCalculatePlaneIntersection4() {
        final NPoint intersection = NPlaneSegment.calculatePlaneLineIntersection(new NPoint(1, 1, 1), // line point 1
                                                                                 new NPoint(11, 11, 11), // line point 2
                                                                                 new NPoint(15, 0, 0), // plane point 1
                                                                                 new NPoint(0, 15, 0), // plane point 2
                                                                                 new NPoint(0, 0, 15), // plane point 3
                                                                                 AbstractRealNDomain.DEFAULT_EPSILON);
        final NPoint expected = new NPoint(5, 5, 5);
        assertTrue(expected.equals(intersection, 1e-12),
                   () -> String.format("expected: %s, actual: %s", expected, intersection));
    }
    
    /**
     * Test an easy 3D case of {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)}
     */
    @Test
    void testCalculatePlaneIntersection3DEasy() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(2, 2, 1);
        final NPoint point2 = new NPoint(2, 2, -1);
        
        testCalculatePlaneIntersection(plane, point1, point2, new NPoint(2, 2, 0));
    }
    
    /**
     * Test an easy 5D case of {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)}. The Plan and points
     * are in a shared 2D space so we just return a point on the line
     */
    @Test
    void testCalculatePlaneIntersection5DEasy() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 1, 1, 0), new NPoint(0, 0, 2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(0, 0, 2, 2, 0), new NPoint(0, 0, 1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(0, 0, 1, 3, 0), new NPoint(0, 0, 0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 0, 0, 2, 0), new NPoint(0, 0, 1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(0, 0, 1, 2, 0);
        final NPoint point2 = new NPoint(0, 0, 1, 2.1, 0);
        
        testCalculatePlaneIntersection(plane, point1, point2, new NPoint(0, 0, 1, 2, 0));
    }
    
    /**
     * The same as the last test, but we instead have only one point on the line contained in the plane
     */
    @Test
    void testCalculatePlaneIntersection5DEasyAgain() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 1, 1, 0), new NPoint(0, 0, 2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(0, 0, 2, 2, 0), new NPoint(0, 0, 1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(0, 0, 1, 3, 0), new NPoint(0, 0, 0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 0, 0, 2, 0), new NPoint(0, 0, 1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(0, 0, 0, 0, 0);
        final NPoint point2 = new NPoint(0, 0, 1, 2, 0);
        
        testCalculatePlaneIntersection(plane, point1, point2, new NPoint(0, 0, 1, 2, 0));
    }
    
    /**
     * The same as the last test, but without any intersection
     */
    @Test
    void testCalculatePlaneIntersection5DEasyAgainAgain() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 1, 1, 0), new NPoint(0, 0, 2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(0, 0, 2, 2, 0), new NPoint(0, 0, 1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(0, 0, 1, 3, 0), new NPoint(0, 0, 0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 0, 0, 2, 0), new NPoint(0, 0, 1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(0, 0, 0, 0, 0);
        final NPoint point2 = new NPoint(0, 0, 0.1, 0.1, 0);
        
        testCalculatePlaneIntersection(plane, point1, point2, null);
    }
    
    /**
     * Tests a hard 5D case of {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)}. It's hard in the
     * sense that there are no short-cuts in the algorithm that we can take.
     */
    @Test
    void testCalculatePlaneIntersection5DHard() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 1, 1, 1), new NPoint(3, 1, 1, 1, 1));
        final NLineSegment seg2 = new NLineSegment(new NPoint(3, 1, 1, 1, 1), new NPoint(3, 1, 3, 1, 3));
        final NLineSegment seg3 = new NLineSegment(new NPoint(3, 1, 3, 1, 3), new NPoint(1, 1, 3, 1, 3));
        final NLineSegment seg4 = new NLineSegment(new NPoint(1, 1, 3, 1, 3), new NPoint(1, 1, 1, 1, 1));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        NOrthotopeTest.validate(plane);
        
        final NPoint point1 = new NPoint(2, 1, 1, 1, 2);
        final NPoint point2 = new NPoint(2, 1, 4, 1, 2);
        
        testCalculatePlaneIntersection(plane, point1, point2, new NPoint(2, 1, 2, 1, 2));
    }
    
    /**
     * The same as the last test, but no intersection
     */
    @Test
    void testCalculatePlaneIntersection5DHardNoIntersection() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 1, 1, 1), new NPoint(3, 1, 1, 1, 1));
        final NLineSegment seg2 = new NLineSegment(new NPoint(3, 1, 1, 1, 1), new NPoint(3, 1, 3, 1, 3));
        final NLineSegment seg3 = new NLineSegment(new NPoint(3, 1, 3, 1, 3), new NPoint(1, 1, 3, 1, 3));
        final NLineSegment seg4 = new NLineSegment(new NPoint(1, 1, 3, 1, 3), new NPoint(1, 1, 1, 1, 1));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        NOrthotopeTest.validate(plane);
        
        final NPoint point1 = new NPoint(0, 1, 0, 1, 0);
        final NPoint point2 = new NPoint(0, 1, 1, 1, 0);
        
        testCalculatePlaneIntersection(plane, point1, point2, null);
    }
    
    /**
     * The same as the last test, but no intersection because they don't share a 3D sub-space
     */
    @Test
    void testCalculatePlaneIntersection5DHardNoIntersection2() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 1, 1, 1), new NPoint(3, 1, 1, 1, 1));
        final NLineSegment seg2 = new NLineSegment(new NPoint(3, 1, 1, 1, 1), new NPoint(3, 1, 3, 1, 3));
        final NLineSegment seg3 = new NLineSegment(new NPoint(3, 1, 3, 1, 3), new NPoint(1, 1, 3, 1, 3));
        final NLineSegment seg4 = new NLineSegment(new NPoint(1, 1, 3, 1, 3), new NPoint(1, 1, 1, 1, 1));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        NOrthotopeTest.validate(plane);
        
        final NPoint point1 = new NPoint(1, 2, 3, 4, 5);
        final NPoint point2 = new NPoint(6, 7, 8, 9, 0);
        
        testCalculatePlaneIntersection(plane, point1, point2, null);
    }
    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} with points whose line does not
     * intersect the plane
     */
    @Test
    void testCalculatePlaneIntersectionNoIntersection() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(10, 10, 10);
        final NPoint point2 = new NPoint(15, 15, 15);
        
        assertTrue(plane.calculatePlaneLineIntersection(point1, point2).isEmpty());
    }
    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} with points in the plane that are
     * equal.
     */
    @Test
    void testCalculatePlaneIntersectionEqualPoints() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        
        testCalculatePlaneIntersection(plane, plane.getCenter(), plane.getCenter(), plane.getCenter());
    }
    
    /**
     * A 4D test case of {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)}
     */
    @Test
    void testCalculatePlaneIntersection4D() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(1, 0, 0, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(1, 0, 0, 0), new NPoint(1, 0, 0, 1));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 0, 0, 1), new NPoint(0, 0, 0, 1));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 0, 0, 1), new NPoint(0, 0, 0, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        
        final NPoint point1 = new NPoint(0.5, -1, 0, 0.5);
        final NPoint point2 = new NPoint(0.5, 1, 0, 0.5);
        
        final NPoint expected = new NPoint(0.5, 0, 0, 0.5);
        
        testCalculatePlaneIntersection(plane, point1, point2, expected);
    }
    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} with points not in the plane that are
     * equal.
     */
    @Test
    void testCalculatePlaneIntersectionEqualPointsNoIntersection() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(10, 10, 10);
        final NPoint point2 = new NPoint(10, 10, 10);
        
        assertTrue(plane.calculatePlaneLineIntersection(point1, point2).isEmpty());
    }
    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} where the line and plane are coplanar
     * but not intersecting and where the line and plane are a parallel and non-coincident.
     */
    @Test
    void testCalculatePlaneIntersectionNoIntersectionCoplanarParallel() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(1, 0, 1));
        final NLineSegment seg2 = new NLineSegment(new NPoint(1, 0, 1), new NPoint(1, 1, 1));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 1, 1), new NPoint(0, 1, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 1, 0), new NPoint(0, 0, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(0, 2, 0);
        final NPoint point2 = new NPoint(1, 2, 1);
        assertTrue(plane.calculatePlaneLineIntersection(point1, point2).isEmpty());

        final NPoint point3 = new NPoint(1, 0, 0);
        final NPoint point4 = new NPoint(2, 0, 1);
        assertTrue(plane.calculatePlaneLineIntersection(point3, point4).isEmpty());
    }
    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} where the line and plane skew
     */
    @Test
    void testCalculatePlaneIntersectionNoIntersectionSkew() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(1, 0, 1, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(1, 0, 1, 0), new NPoint(1, 1, 1, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 1, 1, 0), new NPoint(0, 1, 0, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 1, 0, 0), new NPoint(0, 0, 0, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(0, 0.5, 0.5, 1);
        final NPoint point2 = new NPoint(1, 0.5, 0.5, 1);
        assertTrue(plane.calculatePlaneLineIntersection(point1, point2).isEmpty());
        
        final NPoint point3 = new NPoint(0, 0.5, 0.5, 0);
        final NPoint point4 = new NPoint(1, 0.5, 0.5, 0);
        assertTrue(plane.calculatePlaneLineIntersection(point3, point4).isPresent());
    }
    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} where the line intersects the plane, is
     * parallel to the plane, but does not have any end-points in the plane-segment.
     */
    @Test
    void testCalculatePlaneIntersectionNoIntersectionParallelCrossing() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(2, 0, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 0, 0), new NPoint(2, 2, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(0, 2, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(0, 0, 0));
        final NPlaneSegment plane = new NPlaneSegment(seg1, seg2, seg3, seg4);
        final NPoint point1 = new NPoint(-1, 1, 0);
        final NPoint point2 = new NPoint(3, 1, 0);
        assertTrue(plane.calculatePlaneLineIntersection(point1, point2).isPresent());
    }

    
    /**
     * Test {@link NPlaneSegment#calculatePlaneLineIntersection(NPoint, NPoint)} with an {@link NPlaneSegment} and two
     * {@link NPoint}s defining a {@link NLineSegment}
     * 
     * @param plane the {@link NPlaneSegment} with which we want to check intersection
     * @param linePoint1 the first point defining the line segment
     * @param linePoint2 the second point defining the line segment
     * @param expected the expected {@link NPoint} of intersection
     */
    private static void testCalculatePlaneIntersection(final NPlaneSegment plane,
                                                       final NPoint linePoint1,
                                                       final NPoint linePoint2,
                                                       final NPoint expected) {
        final Optional<NPoint> intersection = plane.calculatePlaneLineIntersection(linePoint1, linePoint2);
        if (expected != null) {
            assertTrue(intersection.isPresent(), "expected an intersection, but there wasn't one");
            final NPoint intersectPt = intersection.orElseThrow();
            assertTrue(plane.contains(intersectPt)); // these contains checks are the important ones
            assertTrue(new NLineSegment(linePoint1, linePoint2).contains(intersectPt));
            assertTrue(expected.equals(intersectPt, 1e-12),
                       () -> String.format("expected: %s, actual: %s", expected, intersectPt));
        }
        else {
            assertTrue(intersection.isEmpty(), "expected no intersection, but there was one");
        }
    }
}
