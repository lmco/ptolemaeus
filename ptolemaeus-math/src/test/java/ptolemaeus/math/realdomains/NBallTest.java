/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.hipparchus.util.FastMath;

import ptolemaeus.math.NumericalMethodsException;

/**
 * Tests {@link NBall}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class NBallTest {
    
    /**
     * Test {@link NBall#getBoundaryPoint(NPoint)}
     */
    @Test
    void testGetBoundaryPointOneArg() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 10);
        
        assertEquals(new NPoint(1, 1, 1, 1), ball.getBoundaryPoint(new NPoint(1, 1, 1, 1)));
        
        assertEquals(new NPoint(1, 1, 1, 11), ball.getBoundaryPoint(new NPoint(1, 1, 1, 20)));
        assertEquals(new NPoint(1, 1, 11, 1), ball.getBoundaryPoint(new NPoint(1, 1, 20, 1)));
        assertEquals(new NPoint(1, 11, 1, 1), ball.getBoundaryPoint(new NPoint(1, 20, 1, 1)));
        assertEquals(new NPoint(11, 1, 1, 1), ball.getBoundaryPoint(new NPoint(20, 1, 1, 1)));
        
        assertEquals(new NPoint(6, 6, 6, 6), ball.getBoundaryPoint(new NPoint(20, 20, 20, 20)));
        /* the ball centered at (1, 1, 1, 1) and the query point is in a direction s.t. the rotation from each main axis
         * is the same so the vector on the boundary is (1, 1, 1, 1) + (5, 5, 5, 5) */
    }
    
    /**
     * Test {@link NBall#getBoundaryPoint(NPoint, NPoint)} against {@link NBall#getBoundaryPoint(NPoint)} using the
     * center of the {@link NBall} as the interior point.
     */
    // CHECKSTYLE.OFF: LineLength
    @Test
    void testGetBoundaryPointTwoArgs1() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 10);
        assertEquals(ball.getBoundaryPoint(new NPoint(1, 1, 1, 20)), ball.getBoundaryPoint(ball.getCenter(), new NPoint(1, 1, 1, 20)));
        assertEquals(ball.getBoundaryPoint(new NPoint(1, 1, 20, 1)), ball.getBoundaryPoint(ball.getCenter(), new NPoint(1, 1, 20, 1)));
        assertEquals(ball.getBoundaryPoint(new NPoint(1, 20, 1, 1)), ball.getBoundaryPoint(ball.getCenter(), new NPoint(1, 20, 1, 1)));
        assertEquals(ball.getBoundaryPoint(new NPoint(20, 1, 1, 1)), ball.getBoundaryPoint(ball.getCenter(), new NPoint(20, 1, 1, 1)));
        assertEquals(ball.getBoundaryPoint(new NPoint(20, 20, 20, 20)), ball.getBoundaryPoint(ball.getCenter(), new NPoint(20, 20, 20, 20)));
    }
    // CHECKSTYLE.ON: LineLength
    
    /**
     * Test {@link NBall#getBoundaryPoint(NPoint, NPoint)} against {@link NBall#getBoundaryPoint(NPoint)} <i>not</i>
     * using the center of the {@link NBall} as the interior point.
     */
    // CHECKSTYLE.OFF: LineLength
    @Test
    void testGetBoundaryPointTwoArgs2() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 10);
        
        assertEquals(new NPoint(1, 1, 1, 1), ball.getBoundaryPoint(new NPoint(-1, -1, -1, -1), new NPoint(1, 1, 1, 1)));
        
        testBoundary2Args(ball,
                          new NPoint(-1, -1, -1, -1),
                          new NPoint(-100, -100, -100, -100),
                          new NPoint(-4, -4, -4, -4));
        
        testBoundary2Args(ball,
                          new NPoint(1, 2, 3, 4),
                          new NPoint(100, 0, 0, 0),
                          new NPoint(10.463543763293996, 1.8088172977112325, 2.7132259465668485, 3.617634595422465));
    }
    // CHECKSTYLE.ON: LineLength
    
    /**
     * Test to ensure that we get the expected answer and that the answer is {@link NBall#getRadius()} from
     * {@link NBall#getCenter()}
     * 
     * @param ball the {@link NBall}
     * @param internal the internal {@link NPoint}
     * @param external the external {@link NPoint}
     * @param expected the expected answer {@link NPoint}
     */
    private static void testBoundary2Args(final NBall ball,
                                          final NPoint internal,
                                          final NPoint external,
                                          final NPoint expected) {
        final NPoint answer = ball.getBoundaryPoint(internal, external);
        assertEquals(expected, answer);
        assertEquals(ball.getRadius(), answer.distanceTo(ball.getCenter()));
        assertTrue(new NLineSegment(internal, external).contains(expected),
                   "the line segment did not contain the boundary point");
    }
    
    /**
     * Test {@link NBall#NBall(NPoint, double)}
     */
    @Test
    void testNBallConstructor() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 10);
        assertEquals(new NPoint(1, 1, 1, 1), ball.getCenter());
        assertEquals(10, ball.getRadius());
        assertEquals(4, ball.getEmbeddedDimensionality());
        assertEquals("NBall c = (1.0, 1.0, 1.0, 1.0) r = 10.0", ball.toString());
        assertEquals(4, ball.getDimensionality());
        assertEquals(AbstractRealNDomain.DEFAULT_EPSILON, ball.epsilon());
        
        final NBall ball2 = new NBall(10, new NPoint(1, 1, 1, 1), 10, 1e-9);
        assertEquals(4, ball2.getDimensionality());
        assertEquals(10, ball2.getEmbeddedDimensionality());
        assertEquals(1e-9, ball2.epsilon());
    }
    
    /**
     * Test {@link NBall#getMeasure()} for a 1-dimensional ball (a line segment)
     */
    @Test
    void testGetMeasure1() {
        final NBall ball = new NBall(new NPoint(1), 10);
        assertEquals(20, ball.getMeasure(), 1e-14);
    }
    
    /**
     * Test {@link NBall#getMeasure()} for a 2-dimensional ball (a disk)
     */
    @Test
    void testGetMeasure2() {
        final NBall ball = new NBall(new NPoint(1, 1), 10);
        assertEquals(FastMath.PI * ball.getRadius() * ball.getRadius(),
                     ball.getMeasure(),
                     1e-12);
    }
    
    /**
     * Test {@link NBall#getMeasure()} for a 3-dimensional ball (a ball or solid sphere)
     */
    @Test
    void testGetMeasure3() {
        final NBall ball = new NBall(new NPoint(1, 1, 1), 10);
        assertEquals((4.0 / 3.0) * FastMath.PI * ball.getRadius() * ball.getRadius() * ball.getRadius(),
                     ball.getMeasure(),
                     1e-11);
    }
    
    /**
     * Test {@link NBall#getMeasure()} for a 4-dimensional ball (a hyper-ball or solid hyper-sphere)
     */
    @Test
    void testGetMeasure4() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 10);
        assertEquals(49348.022005446765, ball.getMeasure());
    }
    
    /**
     * Test {@link NBall#contains(NPoint)}
     */
    @Test
    void testContains() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 1);
        assertTrue(ball.contains(new NPoint(1, 1, 1, 1)));
        assertTrue(ball.contains(new NPoint(1.4, 1.4, 1.4, 1.4)));
        assertTrue(ball.contains(new NPoint(1.5, 1.5, 1.5, 1.5))); // on the surface
        
        assertFalse(ball.contains(new NPoint(1.6, 1.5, 1.5, 1.5)));
        
        assertThrows(NumericalMethodsException.class, () -> ball.contains(new NPoint(1)));
    }
    
    
    /**
     * Test {@link NBall#equals(Object)}
     */
    @Test
    void testEquals() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 1);
        final NBall eq = new NBall(new NPoint(1, 1, 1, 1), 1);
        final NBall neq1 = new NBall(new NPoint(1, 1, 1, 1), 2);
        final NBall neq2 = new NBall(new NPoint(2, 1, 1, 1), 1);
        
        assertEquals(ball, ball);
        assertEquals(ball, eq);
        
        assertNotEquals(ball, neq1);
        assertNotEquals(ball, neq2);
        
        assertNotEquals(ball, null);
        assertNotEquals(ball, new Object());
    }
    
    /**
     * Test {@link NBall#hashCode()}
     */
    @Test
    void testHashCode() {
        final NBall ball = new NBall(new NPoint(1, 1, 1, 1), 1);
        assertEquals(1168432224, ball.hashCode());
    }
}