/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import org.hipparchus.util.FastMath;

import ptolemaeus.math.NumericalMethodsException;

/**
 * Tests {@link NLineSegment}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class NLineSegmentTest {
    
    /**
     * An epsilon for checking point equality
     */
    private static final double POINT_EQUALITY_EPSILON = 1e-12;
    
    /** Test {@link NLineSegment#NLineSegment(int, NPoint, NPoint, double)} and
     *       {@link NLineSegment#NLineSegment(NPoint, NPoint)} */
    @Test
    void testConstructors() {
        final NLineSegment s1 = new NLineSegment(new NPoint(1, 2, 3, 4, 5), new NPoint(11, 12, 13, 14, 15));
        final NLineSegment s2 = new NLineSegment(10, new NPoint(1, 2, 3, 4, 5), new NPoint(11, 12, 13, 14, 15), 1e-9);
        
        assertEquals(new NPoint(1, 2, 3, 4, 5), s1.getEndPoint1());
        assertEquals(new NPoint(11, 12, 13, 14, 15), s1.getEndPoint2());
        assertEquals(1, s1.getDimensionality());
        assertEquals(5, s1.getEmbeddedDimensionality());
        assertEquals(AbstractRealNDomain.DEFAULT_EPSILON, s1.epsilon());
        
        assertEquals(new NPoint(1, 2, 3, 4, 5), s2.getEndPoint1());
        assertEquals(new NPoint(11, 12, 13, 14, 15), s2.getEndPoint2());
        assertEquals(1, s2.getDimensionality());
        assertEquals(10, s2.getEmbeddedDimensionality());
        assertEquals(1e-9, s2.epsilon());
    }
    
    /**
     * Test {@link NLineSegment#getBoundaryPoint(NPoint, NPoint)}
     */
    @Test
    void testGetBoundaryPoint() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 2, 3, 4, 5), new NPoint(11, 12, 13, 14, 15));
        assertEquals(line.getEndPoint1(), line.getBoundaryPoint(new NPoint(3, 4, 5, 6, 7), new NPoint(1, 1, 1, 1, 1)));
        assertEquals(line.getEndPoint2(),
                     line.getBoundaryPoint(new NPoint(3, 4, 5, 6, 7), new NPoint(10, 10, 10, 10, 10)));
    }
    
    /**
     * Test {@link NLineSegment#getClosestPoint(NPoint)}
     */
    @Test
    void testGetClosestDegenerateLine() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 2, 3, 4, 5), new NPoint(1, 2, 3, 4, 5));
        assertEquals(new NPoint(1, 2, 3, 4, 5), line.getClosestPoint(new NPoint(0, 0, 0, 0, 0)));
        assertEquals(new NPoint(1, 2, 3, 4, 5), line.getClosestPoint(new NPoint(1, 1, 1, 1, 1)));
    }
    
    /**
     * Test {@link NLineSegment#apply(double)}
     */
    @Test
    void testApply() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 2, 3, 4, 5), new NPoint(11, 12, 13, 14, 15));
        assertEquals(new NPoint(1, 2, 3, 4, 5), line.apply(0));
        assertEquals(new NPoint(11, 12, 13, 14, 15), line.apply(1));
        assertEquals(new NPoint(6, 7, 8, 9, 10), line.apply(0.5));
        assertEquals(new NPoint(21, 22, 23, 24, 25), line.apply(2));
        assertEquals(new NPoint(-9, -8, -7, -6, -5), line.apply(-1));
    }
    
    /**
     * Test a simple 2D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines intersect
     */
    @Test
    void testCalculateIntersection2D() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 0), new NPoint(1, 2));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 1), new NPoint(2, 1));
        
        testIntersection(line1, line2, new NPoint(1, 1));
    }
    
    /**
     * Test a simple 2D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do not intersect
     */
    @Test
    void testCalculateIntersection2DNoIntersection() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 0), new NPoint(1, 2));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 5), new NPoint(2, 5));
        
        testIntersection(line1, line2, null);
    }
    
    /**
     * Test a 3D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do intersect
     */
    @Test
    void testCalculateIntersection3D() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 1, 1), new NPoint(11, 11, 11));
        final NLineSegment line2 = new NLineSegment(new NPoint(6, 0, 6), new NPoint(6, 10, 6));
        
        testIntersection(line1, line2, new NPoint(6, 6, 6));
    }
    
    /**
     * Test a 3D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do not intersect
     */
    @Test
    void testCalculateIntersection3DNoIntersection() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 1, 1), new NPoint(11, 11, 11));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(-1, -1, -1));
        
        testIntersection(line1, line2, null);
    }
    
    /**
     * Test a 4D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do intersect
     */
    @Test
    void testCalculateIntersection4D() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 1, 1, 1), new NPoint(11, 11, 11, 11));
        final NLineSegment line2 = new NLineSegment(new NPoint(6, 6, 6, 0), new NPoint(6, 6, 6, 10));
        
        testIntersection(line1, line2, new NPoint(6, 6, 6, 6));
    }
    
    /**
     * Test a 4D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do intersect
     */
    @Test
    void testCalculateIntersection4D2() {
        final NLineSegment line1 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(10, 10, 10, 10));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 0, 10, 10), new NPoint(10, 10, 0, 0));
        
        testIntersection(line1, line2, new NPoint(5, 5, 5, 5));
    }
    
    /**
     * Test a 4D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do not intersect because
     * they have no overlap in any dimension
     */
    @Test
    void testCalculateIntersection4DNoIntersection() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 1, 1, 1), new NPoint(11, 11, 11, 11));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(-1, -1, -1, -1));
        
        testIntersection(line1, line2, null);
    }
    
    /**
     * Test a 4D case {@link NLineSegment#calculateIntersection(NLineSegment)} where the lines do not intersect because
     * they are skew lines
     */
    @Test
    void testCalculateIntersection4DNoIntersectionSkew() {
        final NLineSegment line1 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(10, 10, 10, 10));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 0, 11, 10), new NPoint(10, 10, 0, 0));
        
        testIntersection(line1, line2, null);
    }
    
    /**
     * Test {@link NLineSegment#calculateIntersection(NLineSegment)}
     * 
     * @param l1 the first line
     * @param l2 the second line
     * @param expected the expected intersection {@link NPoint}
     */
    private static void testIntersection(final NLineSegment l1, final NLineSegment l2, final NPoint expected) {
        final Optional<NPoint> intersection = l1.calculateIntersection(l2);
        if (expected == null) {
            assertTrue(intersection.isEmpty(),
                       () -> String.format("Expected no intersection between %s and %s, but there was one: %s",
                                           l1,
                                           l2,
                                           intersection.get()));
        }
        else {
            assertTrue(intersection.isPresent(),
                       () -> String.format("Expected intersection between %s and %s, but there was none.", l1, l2));
            
            final NPoint actIntersection = intersection.get();
            assertTrue(expected.equals(actIntersection, 1e-12));
            assertTrue(l1.contains(actIntersection),
                       () -> String.format("Line1 %s does not contain the point of intersection", l1, actIntersection));
            assertTrue(l2.contains(actIntersection),
                       () -> String.format("Line2 %s does not contain the point of intersection", l2, actIntersection));
        }
    }
    
    /**
     * Test {@link NLineSegment#getClosestPoint(NPoint)} with a line in 2D which is on a major axis
     */
    @Test
    void testGetClosestPoint() {
        final NLineSegment line = new NLineSegment(new NPoint(0, 0), new NPoint(1, 0));
        assertEquals(new NPoint(0, 0), line.getClosestPoint(new NPoint(0, 0)));
        assertEquals(new NPoint(0.5, 0), line.getClosestPoint(new NPoint(0.5, 0)));
        assertEquals(new NPoint(1, 0), line.getClosestPoint(new NPoint(1, 0)));
        
        assertEquals(new NPoint(0.5, 0), line.getClosestPoint(new NPoint(0.5, 0.5)));
    }
    
    /**
     * Test {@link NLineSegment#getClosestPoint(NPoint)} with a line in 3D which is on a major axis
     */
    @Test
    void testGetClosestPoint3D() {
        final NLineSegment line = new NLineSegment(new NPoint(0, 0, 0), new NPoint(0, 0, 1));
        assertEquals(new NPoint(0, 0, 0), line.getClosestPoint(new NPoint(0, 0, 0)));
        assertEquals(new NPoint(0, 0, 0.5), line.getClosestPoint(new NPoint(0, 0, 0.5)));
        assertEquals(new NPoint(0, 0, 1), line.getClosestPoint(new NPoint(0, 0, 1)));
        
        final NPoint expected = new NPoint(0, 0, 0.5);
        final NPoint query = new NPoint(0.5, 0.5, 0.5);
        testNDCase(line, query, expected);
    }
    
    /**
     * Test {@link NLineSegment#getClosestPoint(NPoint)} with a line in 3D which is not on a major axis
     */
    @Test
    void testGetClosestPoint3DComplex() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1, 1), new NPoint(5, 5, 5));
        assertEquals(new NPoint(1, 1, 1), line.getClosestPoint(new NPoint(1, 1, 1)));
        assertEquals(new NPoint(3, 3, 3), line.getClosestPoint(new NPoint(3, 3, 3)));
        assertEquals(new NPoint(5, 5, 5), line.getClosestPoint(new NPoint(5, 5, 5)));
        
        assertEquals(new NPoint(5, 5, 5), line.getClosestPoint(new NPoint(20, 0, 0)));
        assertEquals(new NPoint(1, 1, 1), line.getClosestPoint(new NPoint(0, 0, -10)));
        
        final NPoint expected1 = new NPoint(1 + 2.0 / 3.0, 1 + 2.0 / 3.0, 1 + 2.0 / 3.0);
        final NPoint query1 = new NPoint(0, 2.5, 2.5);
        testNDCase(line, query1, expected1);
        
        final NPoint expected2 = new NPoint(3, 3, 3);
        final NPoint query2 = new NPoint(1, 1, 7);
        testNDCase(line, query2, expected2);
    }
    
    /**
     * Test {@link NLineSegment#getClosestPoint(NPoint)} with a line in 6D which is not on a major axis
     */
    @Test
    void testGetClosestPoint6D() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1, 1, 1, 1, 1),
                                                   new NPoint(10, 10, 10, 10, 10, 10));
        assertEquals(new NPoint(1, 1, 1, 1, 1, 1),
                     line.getClosestPoint(new NPoint(1, 1, 1, 1, 1, 1)));
        assertEquals(new NPoint(5.5, 5.5, 5.5, 5.5, 5.5, 5.5),
                     line.getClosestPoint(new NPoint(5.5, 5.5, 5.5, 5.5, 5.5, 5.5)));
        assertEquals(new NPoint(10, 10, 10, 10, 10, 10),
                     line.getClosestPoint(new NPoint(10, 10, 10, 10, 10, 10)));
        
        assertEquals(new NPoint(10, 10, 10, 10, 10, 10),
                     line.getClosestPoint(new NPoint(10_000, 0, 0, 0, 0, 0)));
        assertEquals(new NPoint(1, 1, 1, 1, 1, 1),
                     line.getClosestPoint(new NPoint(0, 0, 0, 0, 0, -10_000)));
        
        testNDCase(line,
                   new NPoint(10, 10, 10, 1, 1, 1),
                   new NPoint(5.5, 5.5, 5.5, 5.5, 5.5, 5.5));
    }
    
    /**
     * A convenience method to test the ND {@link NLineSegment#getClosestPoint(NPoint)} case with the provided
     * {@link NLineSegment}, expected {@link NPoint}, and query {@link NPoint} when we have a non-zero tolerance.
     * 
     * @param line {@link NLineSegment}
     * @param query the query {@link NPoint}
     * @param expected the expected {@link NPoint}
     */
    private static void testNDCase(final NLineSegment line, final NPoint query, final NPoint expected) {
        final NPoint answer = line.getClosestPoint(query);
        for (int i = 0; i < line.getEmbeddedDimensionality(); i++) {
            final int finalI = i;
            assertEquals(expected.getComponent(i),
                         answer.getComponent(i),
                         POINT_EQUALITY_EPSILON,
                         () -> String.format("Expected %s but was %s: different at position %s",
                                             expected,
                                             answer,
                                             finalI));
        }
    }
    
    /**
     * Test {@link NLineSegment#NLineSegment(NPoint, NPoint)}
     */
    @Test
    void testNLineSegmentConstructor() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        assertEquals(new NPoint(1, 1), line.getEndPoint1());
        assertEquals(new NPoint(2, 2), line.getEndPoint2());
        assertEquals(FastMath.sqrt(2), line.getMeasure());
        assertEquals(line.getMeasure(), line.getLength());
        assertEquals(2, line.getEmbeddedDimensionality());
        assertEquals("[(1.0, 1.0), (2.0, 2.0)]", line.toString());
        assertEquals(1, line.getDimensionality());
    }
    
    /**
     * Test {@link NLineSegment#equals(Object)}
     */
    @Test
    void testEquals() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        final NLineSegment eq = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        
        final NLineSegment neq1 = new NLineSegment(new NPoint(2, 1), new NPoint(2, 2));
        final NLineSegment neq2 = new NLineSegment(new NPoint(1, 1), new NPoint(3, 2));
        
        assertEquals(line, line);
        assertEquals(line, eq);
        
        assertNotEquals(line, neq1);
        assertNotEquals(line, neq2);
        
        assertNotEquals(line, null);
        assertNotEquals(line, new Object());
    }
    
    /**
     * Test {@link NLineSegment#hashCode()}
     */
    @Test
    void testHashCode() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        assertEquals(-1040155679, line.hashCode());
    }
    
    /**
     * Test {@link NLineSegment#contains(NPoint)}
     */
    @Test
    void testContains() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1, 1, 1), new NPoint(2, 2, 2, 2));
        assertTrue(line.contains(new NPoint(1, 1, 1, 1)));
        assertTrue(line.contains(new NPoint(2, 2, 2, 2)));
        
        assertTrue(line.contains(new NPoint(1.5, 1.5, 1.5, 1.5)));
        assertTrue(line.contains(new NPoint(1.6, 1.6, 1.6, 1.6)));
        
        assertFalse(line.contains(new NPoint(1.7, 1.6, 1.6, 1.6)));
    }
    
    /**
     * Test {@link IExtent#getCommonPoint(IExtent)}
     */
    @Test
    void testGetCommonPoint() {
        final NLineSegment line1 = new NLineSegment(new NPoint(1, 1, 1, 1), new NPoint(2, 2, 2, 2));
        final NLineSegment line2 = new NLineSegment(new NPoint(1, 1, 1, 1), new NPoint(3, 3, 3, 3));
        final NLineSegment line3 = new NLineSegment(new NPoint(2, 2, 2, 2), new NPoint(2, 2, 2, 2));
        
        assertEquals(new NPoint(1, 1, 1, 1), line1.getCommonPoint(line2));
        assertEquals(new NPoint(2, 2, 2, 2), line1.getCommonPoint(line3));
        assertThrows(NumericalMethodsException.class, () -> line2.getCommonPoint(line3));
    }
    
    /**
     * Test {@link IExtent#getOtherPoint(NPoint)}
     */
    @Test
    void testGetOtherPoint() {
        final NLineSegment line = new NLineSegment(new NPoint(1, 1, 1, 1), new NPoint(2, 2, 2, 2));
        assertEquals(new NPoint(2, 2, 2, 2), line.getOtherPoint(new NPoint(1, 1, 1, 1)));
        assertEquals(new NPoint(1, 1, 1, 1), line.getOtherPoint(new NPoint(2, 2, 2, 2)));
        assertThrows(NumericalMethodsException.class, () -> line.getOtherPoint(new NPoint(3, 3, 3, 3)));
    }
    
    /**
     * Test {@link IExtent#angleTo(IExtent)} and {@link IExtent#cosAngleTo(IExtent)}
     */
    @Test
    void testAngleTo() {
        final NLineSegment line1 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(1, 0, 0));
        final NLineSegment line2 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(0, 1, 0));
        final NLineSegment line3 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(0.5, 0.5, 0));
        final NLineSegment line4 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(0.5, 0.5, 0.5));
        assertEquals(FastMath.PI / 2.0, line1.angleTo(line2));
        assertEquals(FastMath.PI / 4.0, line1.angleTo(line3), 1e-13);
        assertEquals(0.9553166181245092, line1.angleTo(line4));
        assertEquals(FastMath.cos(0.9553166181245092), line1.cosAngleTo(line4));
        
        final NLineSegment line5 = new NLineSegment(new NPoint(1, 0, 0), new NPoint(1, 1, 0));
        assertEquals(FastMath.PI / 2.0, line5.angleTo(line1));
        
        assertThrows(NumericalMethodsException.class, () -> line2.angleTo(line5)); // they don't share an endpoint
        
        final NLineSegment line6 = new NLineSegment(new NPoint(0, 0, 0), new NPoint(0, 0, 0));
        assertEquals(FastMath.PI / 2.0, line1.angleTo(line6)); /* we say that the 0-vector is orthogonal to all vectors
                                                                * including itself */
    }
}