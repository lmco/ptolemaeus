/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import org.apache.commons.collections4.IterableUtils;

import org.hipparchus.util.Combinations;
import org.hipparchus.util.FastMath;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.commons.collections.SetUtils;

/**
 * Tests {@link NOrthotope}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
// CHECKSTYLE.OFF: LineLength
class NOrthotopeTest {
    
    /** Test to ensure that {@link AbstractRealNDomain#setDimensionality(int)},
     *                     {@link AbstractRealNDomain#setEmbeddedDimensionality(int)}, and
     *                     {@link AbstractRealNDomain#setEpsilon(double)}
     * cannot be used more than once */
    @Test
    void testCannotSetTwive() {
        final NOrthotope north = NOrthotope.createUnitHypercube(3);
        
        // these values are all set by the NOrthotope constructor, and can each only be set once
        assertThrows(NumericalMethodsException.class, () -> north.setDimensionality(123));
        assertThrows(NumericalMethodsException.class, () -> north.setEmbeddedDimensionality(456));
        assertThrows(NumericalMethodsException.class, () -> north.setEpsilon(0.789));
    }
    
    /**
     * Test {@link RealNDomain#validateBoundaryPointParams(NPoint, NPoint)}
     */
    @Test
    void testValidateBoundaryPointParams() {
        final NOrthotope north = NOrthotope.createUnitHypercube(3);
        
        assertDoesNotThrow(() -> north.validateBoundaryPointParams(new NPoint(0.5, 0.5, 0.5),
                                                                   new NPoint(0.5, 0.5, 0.5)));
        assertDoesNotThrow(() -> north.validateBoundaryPointParams(new NPoint(0.5, 0.5, 0.5), new NPoint(2, 2, 2)));
        
        assertThrows(ValidationException.class,
                     () -> north.validateBoundaryPointParams(new NPoint(2, 2, 2), new NPoint(0.5, 0.5, 0.5)));
        assertThrows(ValidationException.class,
                     () -> north.validateBoundaryPointParams(new NPoint(2, 2, 2), new NPoint(2, 2, 2)));
        
        assertThrows(NumericalMethodsException.class,
                     () -> north.validateBoundaryPointParams(new NPoint(1, 1, 1, 1), new NPoint(1, 1, 1)));
        assertThrows(NumericalMethodsException.class,
                     () -> north.validateBoundaryPointParams(new NPoint(1, 1, 1), new NPoint(1, 1, 1, 1)));
    }
    
    /**
     * Test {@link HasDimensionality#dimensionalityIntStream()} and
     * {@link HasDimensionality#embeddedDimensionalityIntStream()}
     */
    @Test
    void testIntStreamMethods() {
        final NOrthotope north = NOrthotope.createUnitHypercube(5);
        validate(north);
        
        assertEquals(5, north.dimensionalityIntStream().count());
        assertEquals(5, north.embeddedDimensionalityIntStream().count());
    }
    
    /**
     * Test the 2D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)}
     */
    @Test
    void testGetBoundaryPoint2D() {
        final NOrthotope north = NOrthotope.createAxisAligned(1, 5, 1, 3);
        validate(north);
        
        testGetBoundaryPoint(north, new NPoint(2, 2), new NPoint(2, 2), new NPoint(2, 2));
        testGetBoundaryPoint(north, new NPoint(3, 2), new NPoint(7, 2), new NPoint(5, 2));
        testGetBoundaryPoint(north, new NPoint(3, 2), new NPoint(7, 4), new NPoint(5, 3));
    }
    
    /**
     * Test the 2D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)} where we have points with negative
     * components
     */
    @Test
    void testGetBoundaryPoint2DNegative() {
        final NOrthotope north = NOrthotope.createAxisAligned(-2, 2, -3, 9);
        validate(north);
        
        testGetBoundaryPoint(north, new NPoint(0, 0), new NPoint(4, 18), new NPoint(2, 9));
        testGetBoundaryPoint(north, new NPoint(-0.5, -2.25), new NPoint(4, 18), new NPoint(2, 9));
        testGetBoundaryPoint(north, new NPoint(1, 1.5), new NPoint(-4, -6), new NPoint(-2, -3));
    }
    
    /**
     * Test the 3D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)}
     */
    @Test
    void testGetBoundaryPoint3D() {
        final NOrthotope north = NOrthotope.createUnitHypercube(3);
        validate(north);
        
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(0.7, 0.7, 0.7), new NPoint(0.7, 0.7, 0.7));
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(2, 2, 2), new NPoint(1, 1, 1));
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(2, 0.5, 0.5), new NPoint(1, 0.5, 0.5));
        testGetBoundaryPoint(north, new NPoint(0.7, 0.7, 0.7), new NPoint(0.7, 1.7, 0.7), new NPoint(0.7, 1, 0.7));
    }
    
    /**
     * Test the 3D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)} where the orthotope is not axis aligned
     */
    @Test
    void testGetBoundaryPoint3DNotAxisAligned() {
        /* The example is a rectangular prism with height = 2, width = 2, and length = 2. It is a square rotated 45
         * degrees with a vertex on (1, 1, 0) extended to a depth/length of 2. */
        final NOrthotope north = getExample();
        
        testGetBoundaryPoint(north, new NPoint(1, 2, 1), new NPoint(1, 2, -1), new NPoint(1, 2, 0));
        testGetBoundaryPoint(north, new NPoint(1.5, 1.5, 1), new NPoint(3.5, 3.5, 1), new NPoint(2, 2, 1));
        testGetBoundaryPoint(north, new NPoint(0.5, 2, 1.5), new NPoint(1.5, 4, 2.5), new NPoint(1, 3, 2));
    }
    
    /**
     * Test the 4D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)}.
     */
    @Test
    void testGetBoundaryPoint4D() {
        final NOrthotope north = NOrthotope.createUnitHypercube(4);
        validate(north);
        
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(0.7, 0.7, 0.7, 0.7), new NPoint(0.7, 0.7, 0.7, 0.7));
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(2, 2, 2, 2), new NPoint(1, 1, 1, 1));
        testGetBoundaryPoint(north,
                             new NPoint(0.7, 0.7, 0.7, 0.7),
                             new NPoint(0.7, 1.7, 0.7, 1.7),
                             new NPoint(0.7, 1, 0.7, 1));
        
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(2, 0.5, 0.5, 0.5), new NPoint(1, 0.5, 0.5, 0.5));
    }
    
    /**
     * Test the 4D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)} with the orthotope not anchored to the
     * major axes
     */
    @Test
    void testGetBoundaryPoint4DNotAnchoredToAxis() {
        final NOrthotope north = NOrthotope.createAxisAligned(-2, 3, -5, 7, -11, 13, -17, 19);
        validate(north);
        
        testGetBoundaryPoint(north, new NPoint(0, 0, 0, 0), new NPoint(6, 14, 26, 38), new NPoint(3, 7, 13, 19));
        testGetBoundaryPoint(north,
                             new NPoint(-1.5, -3.5, -6.5, -9.5),
                             new NPoint(6, 14, 26, 38),
                             new NPoint(3, 7, 13, 19));
        
        // the first point in the next is the center point
        testGetBoundaryPoint(north, new NPoint(0.5, 1, 2, 2), new NPoint(10.5, 1, 2, 2), new NPoint(3, 1, 2, 2));
    }
    
    /**
     * Test {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)} with a 3D orthotope (it's a 3-cube) embedded in a four
     * dimensional space. This is not currently supported so we check that an exception is thrown.
     */
    @Test
    void testGetBoundaryPoint3DEmbeddedIn4D() {
        // below, we create a cube with no height (y-axis), but a z-depth and w-depth
        final double sideLength = FastMath.sqrt(2);
        final NOrthotope north =
                new NOrthotope(SetUtils.of(new NLineSegment(new NPoint(0,  0, 0, 0),  new NPoint(1,  0, 0, 1)),
                                           new NLineSegment(new NPoint(1,  0, 0, 1),  new NPoint(0,  0, 0, 2)),
                                           new NLineSegment(new NPoint(0,  0, 0, 2),  new NPoint(-1, 0, 0, 1)),
                                           new NLineSegment(new NPoint(-1, 0, 0, 1),  new NPoint(0,  0, 0, 0)),
                                           // the prior four segments form the "bottom" of the cube
                                           new NLineSegment(new NPoint(0,  0, sideLength, 0),  new NPoint(1,  0, sideLength, 1)),
                                           new NLineSegment(new NPoint(1,  0, sideLength, 1),  new NPoint(0,  0, sideLength, 2)),
                                           new NLineSegment(new NPoint(0,  0, sideLength, 2),  new NPoint(-1, 0, sideLength, 1)),
                                           new NLineSegment(new NPoint(-1, 0, sideLength, 1),  new NPoint(0,  0, sideLength, 0)),
                                           // the prior four segments form the "top" of the cube
                                           new NLineSegment(new NPoint(0,  0, 0, 0),  new NPoint(0,  0, sideLength, 0)),
                                           new NLineSegment(new NPoint(1,  0, 0, 1),  new NPoint(1,  0, sideLength, 1)),
                                           new NLineSegment(new NPoint(0,  0, 0, 2),  new NPoint(0,  0, sideLength, 2)),
                                           new NLineSegment(new NPoint(-1, 0, 0, 1),  new NPoint(-1, 0, sideLength, 1))));
                                           // the prior four segments form the edges which connect the top and bottom
        validate(north);
        
        assertThrows(NumericalMethodsException.class,
                     () -> testGetBoundaryPoint(north,
                                                new NPoint(0, 0, sideLength / 2, 1),
                                                new NPoint(0, 0, sideLength / 2, 10),
                                                new NPoint(0, 0, sideLength / 2, 2)));
    }
    
    /**
     * Test the 5D case of {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)}.
     */
    @Test
    void testGetBoundaryPoint5D() {
        final NOrthotope north = NOrthotope.createUnitHypercube(5);
        validate(north);
        
        testGetBoundaryPoint(north,
                             north.getCenter(),
                             new NPoint(0.7, 0.7, 0.7, 0.7, 0.7),
                             new NPoint(0.7, 0.7, 0.7, 0.7, 0.7));
        testGetBoundaryPoint(north, north.getCenter(), new NPoint(2, 2, 2, 2, 2), new NPoint(1, 1, 1, 1, 1));
        testGetBoundaryPoint(north,
                             new NPoint(0.7, 0.7, 0.7, 0.7, 0.7),
                             new NPoint(0.7, 1.7, 0.7, 1.7, 0.7),
                             new NPoint(0.7, 1, 0.7, 1, 0.7));
        testGetBoundaryPoint(north,
                             north.getCenter(),
                             new NPoint(2, 0.5, 0.5, 0.5, 0.5),
                             new NPoint(1, 0.5, 0.5, 0.5, 0.5));
    }
    
    /**
     * Test {@link NOrthotope#getBoundaryPoint(NPoint, NPoint)}
     * 
     * @param north the {@link NOrthotope}
     * @param p1 the first {@link NPoint}
     * @param p2 the second {@link NPoint}
     * @param expected the expected boundary {@link NPoint}
     */
    private static void testGetBoundaryPoint(final NOrthotope north,
                                             final NPoint p1,
                                             final NPoint p2,
                                             final NPoint expected) {
        final NPoint actual = north.getBoundaryPoint(p1, p2);
        assertTrue(expected.equals(actual, 1e-12),
                   () -> String.format("Expected boundary point %s but was %s.  p1 = %s, p2 = %s, north = %s",
                                       expected,
                                       actual,
                                       p1,
                                       p2,
                                       north));
    }
    
    /**
     * Test {@link NOrthotope#NOrthotope(Set)}
     */
    @Test
    void testNOrthotopeConstructor() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2), new NPoint(1, 3));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3), new NPoint(0, 2));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2), new NPoint(1, 1));
        final NOrthotope north = new NOrthotope(SetUtils.of(seg1, seg2, seg3, seg4));
        validate(north);
        
        assertEquals(2, north.getMeasure(), 1e-15); // each side has length sqrt(2)
        assertEquals(SetUtils.of(seg1, seg2, seg3, seg4), north.getEdges());
        assertEquals(SetUtils.of(new NPoint(1, 1), new NPoint(2, 2), new NPoint(1, 3), new NPoint(0, 2)),
                     north.getVertices());
        assertEquals(2, north.getEmbeddedDimensionality());
        assertEquals(2, north.getDimensionality());
        assertEquals(AbstractRealNDomain.DEFAULT_EPSILON, north.epsilon());
    }
    
    /**
     * Test {@link NOrthotope#NOrthotope(Set, double)}
     */
    @Test
    void testNOrthotopeConstructor2() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1, 0, 0, 0), new NPoint(2, 2, 0, 0, 0));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2, 0, 0, 0), new NPoint(1, 3, 0, 0, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3, 0, 0, 0), new NPoint(0, 2, 0, 0, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2, 0, 0, 0), new NPoint(1, 1, 0, 0, 0));
        final NOrthotope north = new NOrthotope(SetUtils.of(seg1, seg2, seg3, seg4), 1e-9);
        validate(north);
        
        assertEquals(2, north.getMeasure(), 1e-15); // each side has length sqrt(2)
        assertEquals(SetUtils.of(seg1, seg2, seg3, seg4), north.getEdges());
        assertEquals(SetUtils.of(new NPoint(1, 1, 0, 0, 0),
                                 new NPoint(2, 2, 0, 0, 0),
                                 new NPoint(1, 3, 0, 0, 0),
                                 new NPoint(0, 2, 0, 0, 0)), north.getVertices());
        assertEquals(5, north.getEmbeddedDimensionality());
        assertEquals(2, north.getDimensionality());
        assertEquals(1e-9, north.epsilon());
    }
    
    /**
     * Test {@link NOrthotope#createAxisAligned(List)}. We know that the edges of an {@link NOrthotope} uniquely define
     * the shape. We show here that using {@link NOrthotope#createAxisAligned(List)} and specifying the edges explicitly
     * using the constructor result in {@link NOrthotope}s which are equal according to
     * {@link NOrthotope#equals(Object)} which checks to ensure that the edges are equal. Doing this, we show that
     * testing {@link NOrthotope#NOrthotope(Set)} also tests {@link NOrthotope}s generated using
     * {@link NOrthotope#createAxisAligned(List)}.
     */
    @Test
    void testGetAxisAligned() {
        final NOrthotope axisAlignedFromIntervals = NOrthotope.createAxisAligned(0, 1, 0, 1, 0, 1, 0, 1);
        validate(axisAlignedFromIntervals);
        
        final NLineSegment ls00 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(0, 0, 0, 1));
        final NLineSegment ls01 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(0, 0, 1, 0));
        final NLineSegment ls02 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(0, 1, 0, 0));
        final NLineSegment ls03 = new NLineSegment(new NPoint(0, 0, 0, 0), new NPoint(1, 0, 0, 0));
        final NLineSegment ls04 = new NLineSegment(new NPoint(0, 0, 0, 1), new NPoint(0, 0, 1, 1));
        final NLineSegment ls05 = new NLineSegment(new NPoint(0, 0, 0, 1), new NPoint(0, 1, 0, 1));
        final NLineSegment ls06 = new NLineSegment(new NPoint(0, 0, 0, 1), new NPoint(1, 0, 0, 1));
        final NLineSegment ls07 = new NLineSegment(new NPoint(0, 0, 1, 0), new NPoint(0, 0, 1, 1));
        final NLineSegment ls08 = new NLineSegment(new NPoint(0, 0, 1, 0), new NPoint(0, 1, 1, 0));
        final NLineSegment ls09 = new NLineSegment(new NPoint(0, 0, 1, 0), new NPoint(1, 0, 1, 0));
        final NLineSegment ls10 = new NLineSegment(new NPoint(0, 0, 1, 1), new NPoint(0, 1, 1, 1));
        final NLineSegment ls11 = new NLineSegment(new NPoint(0, 0, 1, 1), new NPoint(1, 0, 1, 1));
        final NLineSegment ls12 = new NLineSegment(new NPoint(0, 1, 0, 0), new NPoint(0, 1, 0, 1));
        final NLineSegment ls13 = new NLineSegment(new NPoint(0, 1, 0, 0), new NPoint(0, 1, 1, 0));
        final NLineSegment ls14 = new NLineSegment(new NPoint(0, 1, 0, 0), new NPoint(1, 1, 0, 0));
        final NLineSegment ls15 = new NLineSegment(new NPoint(0, 1, 0, 1), new NPoint(0, 1, 1, 1));
        final NLineSegment ls16 = new NLineSegment(new NPoint(0, 1, 0, 1), new NPoint(1, 1, 0, 1));
        final NLineSegment ls17 = new NLineSegment(new NPoint(0, 1, 1, 0), new NPoint(0, 1, 1, 1));
        final NLineSegment ls18 = new NLineSegment(new NPoint(0, 1, 1, 0), new NPoint(1, 1, 1, 0));
        final NLineSegment ls19 = new NLineSegment(new NPoint(0, 1, 1, 1), new NPoint(1, 1, 1, 1));
        final NLineSegment ls20 = new NLineSegment(new NPoint(1, 0, 0, 0), new NPoint(1, 0, 0, 1));
        final NLineSegment ls21 = new NLineSegment(new NPoint(1, 0, 0, 0), new NPoint(1, 0, 1, 0));
        final NLineSegment ls22 = new NLineSegment(new NPoint(1, 0, 0, 0), new NPoint(1, 1, 0, 0));
        final NLineSegment ls23 = new NLineSegment(new NPoint(1, 0, 0, 1), new NPoint(1, 0, 1, 1));
        final NLineSegment ls24 = new NLineSegment(new NPoint(1, 0, 0, 1), new NPoint(1, 1, 0, 1));
        final NLineSegment ls25 = new NLineSegment(new NPoint(1, 0, 1, 0), new NPoint(1, 0, 1, 1));
        final NLineSegment ls26 = new NLineSegment(new NPoint(1, 0, 1, 0), new NPoint(1, 1, 1, 0));
        final NLineSegment ls27 = new NLineSegment(new NPoint(1, 0, 1, 1), new NPoint(1, 1, 1, 1));
        final NLineSegment ls28 = new NLineSegment(new NPoint(1, 1, 0, 0), new NPoint(1, 1, 0, 1));
        final NLineSegment ls29 = new NLineSegment(new NPoint(1, 1, 0, 0), new NPoint(1, 1, 1, 0));
        final NLineSegment ls30 = new NLineSegment(new NPoint(1, 1, 0, 1), new NPoint(1, 1, 1, 1));
        final NLineSegment ls31 = new NLineSegment(new NPoint(1, 1, 1, 0), new NPoint(1, 1, 1, 1));
        
        final NOrthotope axisAlignedFromEdges = new NOrthotope(SetUtils.of(ls00,
                                                                           ls01,
                                                                           ls02,
                                                                           ls03,
                                                                           ls04,
                                                                           ls05,
                                                                           ls06,
                                                                           ls07,
                                                                           ls08,
                                                                           ls09,
                                                                           ls10,
                                                                           ls11,
                                                                           ls12,
                                                                           ls13,
                                                                           ls14,
                                                                           ls15,
                                                                           ls16,
                                                                           ls17,
                                                                           ls18,
                                                                           ls19,
                                                                           ls20,
                                                                           ls21,
                                                                           ls22,
                                                                           ls23,
                                                                           ls24,
                                                                           ls25,
                                                                           ls26,
                                                                           ls27,
                                                                           ls28,
                                                                           ls29,
                                                                           ls30,
                                                                           ls31));
        validate(axisAlignedFromEdges);
        assertEquals(axisAlignedFromIntervals, axisAlignedFromEdges);
    }
    
    /**
     * Test {@link NOrthotope#createAxisAligned(List)} with non-unit-length sides.
     */
    @Test
    void testGetAxisAlignedNonUnit() {
        final NOrthotope north = NOrthotope.createAxisAligned(-1, 2, -3, 4, -5, 6, -7, 8);
        final String expected = String.join(System.lineSeparator(),
                                            "edges:",
                                            "    [(-1.0, -3.0, -5.0, -7.0), (2.0, -3.0, -5.0, -7.0)]",
                                            "    [(-1.0, -3.0, -5.0, -7.0), (-1.0, -3.0, -5.0, 8.0)]",
                                            "    [(2.0, -3.0, -5.0, -7.0), (2.0, -3.0, -5.0, 8.0)]",
                                            "    [(-1.0, -3.0, -5.0, 8.0), (2.0, -3.0, -5.0, 8.0)]",
                                            "    [(-1.0, -3.0, -5.0, -7.0), (-1.0, -3.0, 6.0, -7.0)]",
                                            "    [(-1.0, -3.0, 6.0, -7.0), (-1.0, -3.0, 6.0, 8.0)]",
                                            "    [(-1.0, -3.0, -5.0, 8.0), (-1.0, -3.0, 6.0, 8.0)]",
                                            "    [(2.0, -3.0, -5.0, -7.0), (2.0, -3.0, 6.0, -7.0)]",
                                            "    [(2.0, -3.0, 6.0, -7.0), (2.0, -3.0, 6.0, 8.0)]",
                                            "    [(2.0, -3.0, -5.0, 8.0), (2.0, -3.0, 6.0, 8.0)]",
                                            "    [(-1.0, -3.0, 6.0, -7.0), (2.0, -3.0, 6.0, -7.0)]",
                                            "    [(-1.0, -3.0, 6.0, 8.0), (2.0, -3.0, 6.0, 8.0)]",
                                            "    [(-1.0, -3.0, -5.0, -7.0), (-1.0, 4.0, -5.0, -7.0)]",
                                            "    [(-1.0, 4.0, -5.0, -7.0), (-1.0, 4.0, -5.0, 8.0)]",
                                            "    [(-1.0, -3.0, -5.0, 8.0), (-1.0, 4.0, -5.0, 8.0)]",
                                            "    [(-1.0, 4.0, -5.0, -7.0), (-1.0, 4.0, 6.0, -7.0)]",
                                            "    [(-1.0, 4.0, 6.0, -7.0), (-1.0, 4.0, 6.0, 8.0)]",
                                            "    [(-1.0, 4.0, -5.0, 8.0), (-1.0, 4.0, 6.0, 8.0)]",
                                            "    [(-1.0, -3.0, 6.0, -7.0), (-1.0, 4.0, 6.0, -7.0)]",
                                            "    [(-1.0, -3.0, 6.0, 8.0), (-1.0, 4.0, 6.0, 8.0)]",
                                            "    [(2.0, -3.0, -5.0, -7.0), (2.0, 4.0, -5.0, -7.0)]",
                                            "    [(2.0, 4.0, -5.0, -7.0), (2.0, 4.0, -5.0, 8.0)]",
                                            "    [(2.0, -3.0, -5.0, 8.0), (2.0, 4.0, -5.0, 8.0)]",
                                            "    [(2.0, 4.0, -5.0, -7.0), (2.0, 4.0, 6.0, -7.0)]",
                                            "    [(2.0, 4.0, 6.0, -7.0), (2.0, 4.0, 6.0, 8.0)]",
                                            "    [(2.0, 4.0, -5.0, 8.0), (2.0, 4.0, 6.0, 8.0)]",
                                            "    [(2.0, -3.0, 6.0, -7.0), (2.0, 4.0, 6.0, -7.0)]",
                                            "    [(2.0, -3.0, 6.0, 8.0), (2.0, 4.0, 6.0, 8.0)]",
                                            "    [(-1.0, 4.0, -5.0, -7.0), (2.0, 4.0, -5.0, -7.0)]",
                                            "    [(-1.0, 4.0, -5.0, 8.0), (2.0, 4.0, -5.0, 8.0)]",
                                            "    [(-1.0, 4.0, 6.0, -7.0), (2.0, 4.0, 6.0, -7.0)]",
                                            "    [(-1.0, 4.0, 6.0, 8.0), (2.0, 4.0, 6.0, 8.0)]",
                                            "vertices:",
                                            "    -1.000, -3.000, -5.000, -7.000, ",
                                            "    2.000, -3.000, -5.000, -7.000, ",
                                            "    -1.000, -3.000, -5.000, 8.000, ",
                                            "    2.000, -3.000, -5.000, 8.000, ",
                                            "    -1.000, -3.000, 6.000, -7.000, ",
                                            "    -1.000, -3.000, 6.000, 8.000, ",
                                            "    2.000, -3.000, 6.000, -7.000, ",
                                            "    2.000, -3.000, 6.000, 8.000, ",
                                            "    -1.000, 4.000, -5.000, -7.000, ",
                                            "    -1.000, 4.000, -5.000, 8.000, ",
                                            "    -1.000, 4.000, 6.000, -7.000, ",
                                            "    -1.000, 4.000, 6.000, 8.000, ",
                                            "    2.000, 4.000, -5.000, -7.000, ",
                                            "    2.000, 4.000, -5.000, 8.000, ",
                                            "    2.000, 4.000, 6.000, -7.000, ",
                                            "    2.000, 4.000, 6.000, 8.000, ",
                                            "dimensionality: 4, embedded dimensionality: 4, measure: 3465.0",
                                            "");
        TestUtils.testMultiLineToString(expected, north);
    }
    
    /**
     * Test {@link NOrthotope#equals(Object)}
     */
    @Test
    void testEquals() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2), new NPoint(1, 3));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3), new NPoint(0, 2));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2), new NPoint(1, 1));
        final NOrthotope north = new NOrthotope(SetUtils.of(seg1, seg2, seg3, seg4));
        final NOrthotope eq = new NOrthotope(SetUtils.of(seg1, seg2, seg3, seg4));
        final NOrthotope neq = getExample();
        assertEquals(north, north);
        assertEquals(north, eq);
        assertNotEquals(north, neq);
        assertNotEquals(north, null);
        assertNotEquals(north, new Object());
    }
    
    /**
     * Test {@link NOrthotope#toString()}
     */
    @Test
    void testToString() {
        final NOrthotope north = getExample();
        
        final String expected = String.join(System.lineSeparator(),
                                            "edges:",
                                            "    [(1.0, 1.0, 0.0), (2.0, 2.0, 0.0)]",
                                            "    [(2.0, 2.0, 0.0), (1.0, 3.0, 0.0)]",
                                            "    [(1.0, 3.0, 0.0), (0.0, 2.0, 0.0)]",
                                            "    [(0.0, 2.0, 0.0), (1.0, 1.0, 0.0)]",
                                            "    [(1.0, 1.0, 2.0), (2.0, 2.0, 2.0)]",
                                            "    [(2.0, 2.0, 2.0), (1.0, 3.0, 2.0)]",
                                            "    [(1.0, 3.0, 2.0), (0.0, 2.0, 2.0)]",
                                            "    [(0.0, 2.0, 2.0), (1.0, 1.0, 2.0)]",
                                            "    [(1.0, 1.0, 0.0), (1.0, 1.0, 2.0)]",
                                            "    [(2.0, 2.0, 0.0), (2.0, 2.0, 2.0)]",
                                            "    [(1.0, 3.0, 0.0), (1.0, 3.0, 2.0)]",
                                            "    [(0.0, 2.0, 0.0), (0.0, 2.0, 2.0)]",
                                            "vertices:",
                                            "    1.000, 1.000, 0.000, ",
                                            "    2.000, 2.000, 0.000, ",
                                            "    1.000, 3.000, 0.000, ",
                                            "    0.000, 2.000, 0.000, ",
                                            "    1.000, 1.000, 2.000, ",
                                            "    2.000, 2.000, 2.000, ",
                                            "    1.000, 3.000, 2.000, ",
                                            "    0.000, 2.000, 2.000, ",
                                            "dimensionality: 3, embedded dimensionality: 3, measure: 4.000000000000001",
                                            "");
        TestUtils.testMultiLineToString(expected, north);
    }
    
    /**
     * Test {@link NOrthotope#hashCode()}
     */
    @Test
    void testHashCode() {
        assertEquals(-1915509993, getExample().hashCode());
    }
    
    /**
     * Test {@link NOrthotope#contains(NPoint)}
     */
    @Test
    void testContains() {
        final NOrthotope north = getExample();
        
        assertTrue(north.contains(new NPoint(1, 1, 0)));
        assertTrue(north.contains(new NPoint(2, 2, 0)));
        assertTrue(north.contains(new NPoint(1, 3, 0)));
        assertTrue(north.contains(new NPoint(0, 2, 0)));
        
        assertTrue(north.contains(new NPoint(1, 1, 2)));
        assertTrue(north.contains(new NPoint(2, 2, 2)));
        assertTrue(north.contains(new NPoint(1, 3, 2)));
        assertTrue(north.contains(new NPoint(0, 2, 2))); // contains all of the vertices
        
        assertFalse(north.contains(new NPoint(10, 0, 0)));
        assertFalse(north.contains(new NPoint(0, 10, 0)));
        assertFalse(north.contains(new NPoint(0, 0, 10))); // these three are obviously way out
        
        assertFalse(north.contains(new NPoint(0.9, 1, 0)));
        assertFalse(north.contains(new NPoint(2.1, 2, 0)));
        assertFalse(north.contains(new NPoint(1, 3.1, 0)));
        assertFalse(north.contains(new NPoint(-0.1, 2, 0)));
        
        assertFalse(north.contains(new NPoint(1, 0.9, 2)));
        assertFalse(north.contains(new NPoint(2.1, 2, 2)));
        assertFalse(north.contains(new NPoint(1, 3.1, 2)));
        assertFalse(north.contains(new NPoint(-0.1, 2, 2))); // these prev. 8 are all just outside
        
        assertTrue(north.contains(new NPoint(1, 1.1, 0)));
        assertTrue(north.contains(new NPoint(1.9, 2, 0)));
        assertTrue(north.contains(new NPoint(1, 2.9, 0)));
        assertTrue(north.contains(new NPoint(0.1, 2, 0)));
        
        assertTrue(north.contains(new NPoint(1, 1.1, 2)));
        assertTrue(north.contains(new NPoint(1.9, 2, 2)));
        assertTrue(north.contains(new NPoint(1, 2.9, 2)));
        assertTrue(north.contains(new NPoint(0.1, 2, 2))); // these prev. 8 are all just inside
    }
    
    /**
     * Test {@link NOrthotope#contains(NPoint)} in 4 dimensions
     */
    @Test
    void testContains2() {
        // this call produces a 4-cube embedded in a 5-space
        final NOrthotope north = cartesianProduct(getExample(), new NVector(0, 0, 0, 2), new NVector(0, 0, 0, 0, 0));
        
        assertTrue(north.contains(new NPoint(1, 1, 0, 0, 0)));
        assertTrue(north.contains(new NPoint(2, 2, 0, 0, 0)));
        assertTrue(north.contains(new NPoint(1, 3, 0, 0, 0)));
        assertTrue(north.contains(new NPoint(0, 2, 0, 0, 0)));
        
        assertTrue(north.contains(new NPoint(1, 1, 2, 0, 0)));
        assertTrue(north.contains(new NPoint(2, 2, 2, 0, 0)));
        assertTrue(north.contains(new NPoint(1, 3, 2, 0, 0)));
        assertTrue(north.contains(new NPoint(0, 2, 2, 0, 0)));
        
        assertFalse(north.contains(new NPoint(-1, -1, -1, -1, -1))); // not int he bounding box
        assertFalse(north.contains(new NPoint(3, 3, 3, 3, 0))); // just isn't contained but is in the same 4-space
        assertFalse(north.contains(new NPoint(1, 1, 0, 1, 1))); // isn't in the same 4-space
    }
    
    /**
     * Test to show that using {@link NOrthotope#createAxisAligned(double...)} is equivalent to
     * {@link NOrthotope#createAxisAligned(List)}
     */
    @Test
    void testCreateAxisAligned() {
        final List<DoubleInterval> intervals = List.of(new DoubleInterval(-1.0, 2.0),
                                                       new DoubleInterval(-3.0, 4.0),
                                                       new DoubleInterval(-5.0, 6.0),
                                                       new DoubleInterval(-7.0, 8.0));
        final NOrthotope withIntervals = NOrthotope.createAxisAligned(intervals);
        final NOrthotope withDoubles = NOrthotope.createAxisAligned(-1, 2, -3, 4, -5, 6, -7, 8);
        assertEquals(withIntervals, withDoubles);
    }
    
    /**
     * Test {@link NOrthotope#createUnitHypercube(int)}
     */
    @Test
    void testUnitHypercube() {
        final NOrthotope axisAligned = NOrthotope.createAxisAligned(0, 1, 0, 1, 0, 1, 0, 1, 0, 1);
        final NOrthotope unitHypercube5D = NOrthotope.createUnitHypercube(5);
        assertEquals(axisAligned, unitHypercube5D);
    }
    
    /**
     * Test a 3D case of {@link NOrthotope#getNMinus1FacetsAtVertex(NPoint)}
     */
    @Test
    void testGetNMinus1FacetsAtVertex3D() {
        final NOrthotope north = getExample();
        testFacets(north, north.getNMinus1FacetsAtVertex(new NPoint(1, 1, 2)), 2);
    }
    
    /**
     * Test a 4D case of {@link NOrthotope#getNMinus1FacetsAtVertex(NPoint)}. A 4D orthotope has four facet that meet at
     * each edge
     */
    @Test
    void testGetNMinus1FacetsAtVertex4D() {
        final NOrthotope north = NOrthotope.createUnitHypercube(4);
        testFacets(north, north.getNMinus1FacetsAtVertex(new NPoint(0, 0, 0, 0)), 3);
    }
    
    /**
     * Test a 4D case of {@link NOrthotope#getNMinus1FacetsAtVertex(NPoint)}
     */
    @Test
    void testGetNMinus1FacetsAtVertex4DNotAxisAligned() {
        final NOrthotope north = cartesianProduct(getExample(), new NVector(0, 0, 0, 1));
        testFacets(north, north.getNMinus1FacetsAtVertex(new NPoint(1, 1, 0, 0)), 3);
    }
    
    /**
     * Test a 5D case of {@link NOrthotope#getNMinus1FacetsAtVertex(NPoint)}
     */
    @Test
    void testGetNMinus1FacetsAtVertex5D() {
        final NOrthotope north = NOrthotope.createUnitHypercube(5);
        testFacets(north, north.getNMinus1FacetsAtVertex(new NPoint(1, 1, 1, 1, 1)), 4);
    }
    
    /**
     * Tests to show that {@link NOrthotope} facets are being created appropriately
     * 
     * @param north the {@link NOrthotope} we're testing
     * @param facetStream the facet {@link Stream} to check
     * @param facetDimensionality the dimensionality that we expect of the facets
     */
    static void testFacets(final NOrthotope north,
                           final Stream<NOrthotope> facetStream,
                           final int facetDimensionality) {
        final Collection<NOrthotope> facets = facetStream.collect(Collectors.toList());
        facets.forEach(NOrthotopeTest::validate);
        final List<NPoint> totalVertices = facets.stream()
                                                 .map(NOrthotope::getVertices)
                                                 .flatMap(Collection::stream)
                                                 .collect(Collectors.toList());
        totalVertices.forEach(v -> assertTrue(north.getVertices().contains(v),
                                              () -> String.format("%s did not contain vertex %s", north, v)));
        
        final boolean correctDimension = facets.stream()
                                               .mapToInt(NOrthotope::getDimensionality)
                                               .allMatch(i -> i == facetDimensionality);
        assertTrue(correctDimension,
                   () -> String.format("not all facets had the correct dimensionality: %d%nfacets:%n%s",
                                       facetDimensionality,
                                       facets));
        
        for (NOrthotope facet : facets) { // Verify that the center of each facet is on the boundary of this NOrthotope
            final NPoint facetCenter = facet.getCenter();
            final NVector radialVector = north.getCenter().getVectorTo(facetCenter);
            final NPoint exterior = facetCenter.plus(radialVector.getPoint());
            final NPoint boundary = north.getBoundaryPoint(north.getCenter(), exterior);
            assertTrue(facetCenter.equals(boundary, 1e-12), "Facet failed the boundary check");
         }
    }
    
    /**
     * Test a 2D case of {@link NOrthotope#cartesianProductOfVectors(NPoint, Collection)}
     */
    @Test
    void testCartesianProductOfEdges2D() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(1, 1), new NPoint(2, 2));
        final NLineSegment seg2 = new NLineSegment(new NPoint(2, 2), new NPoint(1, 3));
        final NLineSegment seg3 = new NLineSegment(new NPoint(1, 3), new NPoint(0, 2));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 2), new NPoint(1, 1));
        final NOrthotope traditional = new NOrthotope(SetUtils.of(seg1, seg2, seg3, seg4));
        final NOrthotope cartProdOfEdges = NOrthotope.cartesianProductOfVectors(new NPoint(1, 1),
                                                                                List.of(new NVector(1, 1),
                                                                                        new NVector(-1, 1)));
        
        testHaveEquivalentEdges(traditional, cartProdOfEdges);
    }
    
    /**
     * Test a 3D case of {@link NOrthotope#cartesianProductOfVectors(NPoint, Collection)}
     */
    @Test
    void testCartesianProductOfEdges3D() {
        final NLineSegment seg01 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg02 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg03 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg04 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        
        // these first four are the line segments in the XY1 plane
        final NLineSegment seg05 = new NLineSegment(new NPoint(1, 1, 2), new NPoint(2, 2, 2));
        final NLineSegment seg06 = new NLineSegment(new NPoint(2, 2, 2), new NPoint(1, 3, 2));
        final NLineSegment seg07 = new NLineSegment(new NPoint(1, 3, 2), new NPoint(0, 2, 2));
        final NLineSegment seg08 = new NLineSegment(new NPoint(0, 2, 2), new NPoint(1, 1, 2));
        
        // these first four are the line segments that connect the first surface to the second
        final NLineSegment seg09 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(1, 1, 2));
        final NLineSegment seg10 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(2, 2, 2));
        final NLineSegment seg11 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(1, 3, 2));
        final NLineSegment seg12 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(0, 2, 2));
        
        final NOrthotope traditional = new NOrthotope(SetUtils.of(seg01,
                                                                  seg02,
                                                                  seg03,
                                                                  seg04,
                                                                  seg05,
                                                                  seg06,
                                                                  seg07,
                                                                  seg08,
                                                                  seg09,
                                                                  seg10,
                                                                  seg11,
                                                                  seg12));
        final NOrthotope cartProdOfEdges = NOrthotope.cartesianProductOfVectors(new NPoint(1, 1, 0),
                                                                                List.of(new NVector(1, 1, 0),
                                                                                        new NVector(-1, 1, 0),
                                                                                        new NVector(0, 0, 2)));
        
        testHaveEquivalentEdges(traditional, cartProdOfEdges);
    }
    
    /**
     * Test {@link NOrthotope#cartesianProduct(NVector)} 2D-6D
     */
    @Test
    void testCartesianProduct1() {
        final NOrthotope north = NOrthotope.createUnitHypercube(2);
        final NOrthotope northCartProd1 = cartesianProduct(north, new NVector(0, 0, 1));
        final NOrthotope expected1 = NOrthotope.createUnitHypercube(3);
        testHaveEquivalentEdges(expected1, northCartProd1);
        
        final NOrthotope northCartProd2 = cartesianProduct(northCartProd1, new NVector(0, 0, 0, 1));
        final NOrthotope expected2 = NOrthotope.createUnitHypercube(4);
        testHaveEquivalentEdges(expected2, northCartProd2);
        
        final NOrthotope northCartProd3 = cartesianProduct(northCartProd2, new NVector(0, 0, 0, 0, 1));
        final NOrthotope expected3 = NOrthotope.createUnitHypercube(5);
        testHaveEquivalentEdges(expected3, northCartProd3);
        
        final NOrthotope northCartProd4 = cartesianProduct(northCartProd3, new NVector(0, 0, 0, 0, 0, 1));
        final NOrthotope expected4 = NOrthotope.createUnitHypercube(6);
        testHaveEquivalentEdges(expected4, northCartProd4);
    }
    
    /**
     * Test to show that two {@link NOrthotope}s have equivalent edges
     * 
     * @param expected the expected {@link NOrthotope}
     * @param actual the actual {@link NOrthotope}
     */
    private static void testHaveEquivalentEdges(final NOrthotope expected, final NOrthotope actual) {
        for (final NLineSegment edge : expected.getEdges()) {
            final NLineSegment flipped = new NLineSegment(edge.getEndPoint2(), edge.getEndPoint1());
            final boolean anyMatch = actual.getEdges().stream().anyMatch(e -> e.equals(edge) || e.equals(flipped));
            assertTrue(anyMatch,
                       () -> String.format("%s did not contain an edge matching %s of %s", actual, edge, expected));
        }
    }
    
    /**
     * Test {@link NOrthotope#cartesianProduct(NVector)} on a tilted plane in R3 crossed with a perpendicular vector
     * into R3 to make a rectangular prism.
     */
    @Test
    void testCartesianProduct2() {
        final NLineSegment seg1 = new NLineSegment(new NPoint(0, 0, 10), new NPoint(10, 0, 10));
        final NLineSegment seg2 = new NLineSegment(new NPoint(10, 0, 10), new NPoint(10, 10, 0));
        final NLineSegment seg3 = new NLineSegment(new NPoint(10, 10, 0), new NPoint(0, 10, 0));
        final NLineSegment seg4 = new NLineSegment(new NPoint(0, 10, 0), new NPoint(0, 0, 10));
        final NOrthotope north = new NOrthotope(SetUtils.of(seg1, seg2, seg3, seg4));
        final NOrthotope cartProd = north.cartesianProduct(new NVector(0, 1, 1));
        
        final String expected = String.join(System.lineSeparator(),
                                            "edges:",
                                            "    [(0.0, 0.0, 10.0), (10.0, 0.0, 10.0)]",
                                            "    [(0.0, 0.0, 10.0), (0.0, 1.0, 11.0)]",
                                            "    [(10.0, 0.0, 10.0), (10.0, 1.0, 11.0)]",
                                            "    [(0.0, 1.0, 11.0), (10.0, 1.0, 11.0)]",
                                            "    [(10.0, 0.0, 10.0), (10.0, 10.0, 0.0)]",
                                            "    [(10.0, 10.0, 0.0), (10.0, 11.0, 1.0)]",
                                            "    [(10.0, 1.0, 11.0), (10.0, 11.0, 1.0)]",
                                            "    [(10.0, 10.0, 0.0), (0.0, 10.0, 0.0)]",
                                            "    [(0.0, 10.0, 0.0), (0.0, 11.0, 1.0)]",
                                            "    [(10.0, 11.0, 1.0), (0.0, 11.0, 1.0)]",
                                            "    [(0.0, 10.0, 0.0), (0.0, 0.0, 10.0)]",
                                            "    [(0.0, 11.0, 1.0), (0.0, 1.0, 11.0)]",
                                            "vertices:",
                                            "    0.000, 0.000, 10.000, ",
                                            "    10.000, 0.000, 10.000, ",
                                            "    0.000, 1.000, 11.000, ",
                                            "    10.000, 1.000, 11.000, ",
                                            "    10.000, 10.000, 0.000, ",
                                            "    10.000, 11.000, 1.000, ",
                                            "    0.000, 10.000, 0.000, ",
                                            "    0.000, 11.000, 1.000, ",
                                            "dimensionality: 3, embedded dimensionality: 3, measure: 200.00000000000003",
                                            "");
        TestUtils.testMultiLineToString(expected, cartProd);
    }
    
    /**
     * Test {@link NOrthotope#cartesianProduct(NVector)} with a rectangular prism with height = 1, width = 1, and length
     * = 2. It is a square rotated 45 degrees with a vertex on (1, 1, 0) extended to a depth/length of 2. We cross that
     * with a vector (0, 0, 0, 1) to extend it into 4D.
     */
    @Test
    void testCartesianProduct3() {
        final NOrthotope cartProd = cartesianProduct(getExample(), new NVector(0, 0, 0, 1));
        final String expected = String.join(System.lineSeparator(),
                                            "edges:",
                                            "    [(1.0, 1.0, 0.0, 0.0), (2.0, 2.0, 0.0, 0.0)]",
                                            "    [(1.0, 1.0, 0.0, 0.0), (1.0, 1.0, 0.0, 1.0)]",
                                            "    [(2.0, 2.0, 0.0, 0.0), (2.0, 2.0, 0.0, 1.0)]",
                                            "    [(1.0, 1.0, 0.0, 1.0), (2.0, 2.0, 0.0, 1.0)]",
                                            "    [(2.0, 2.0, 0.0, 0.0), (1.0, 3.0, 0.0, 0.0)]",
                                            "    [(1.0, 3.0, 0.0, 0.0), (1.0, 3.0, 0.0, 1.0)]",
                                            "    [(2.0, 2.0, 0.0, 1.0), (1.0, 3.0, 0.0, 1.0)]",
                                            "    [(1.0, 3.0, 0.0, 0.0), (0.0, 2.0, 0.0, 0.0)]",
                                            "    [(0.0, 2.0, 0.0, 0.0), (0.0, 2.0, 0.0, 1.0)]",
                                            "    [(1.0, 3.0, 0.0, 1.0), (0.0, 2.0, 0.0, 1.0)]",
                                            "    [(0.0, 2.0, 0.0, 0.0), (1.0, 1.0, 0.0, 0.0)]",
                                            "    [(0.0, 2.0, 0.0, 1.0), (1.0, 1.0, 0.0, 1.0)]",
                                            "    [(1.0, 1.0, 2.0, 0.0), (2.0, 2.0, 2.0, 0.0)]",
                                            "    [(1.0, 1.0, 2.0, 0.0), (1.0, 1.0, 2.0, 1.0)]",
                                            "    [(2.0, 2.0, 2.0, 0.0), (2.0, 2.0, 2.0, 1.0)]",
                                            "    [(1.0, 1.0, 2.0, 1.0), (2.0, 2.0, 2.0, 1.0)]",
                                            "    [(2.0, 2.0, 2.0, 0.0), (1.0, 3.0, 2.0, 0.0)]",
                                            "    [(1.0, 3.0, 2.0, 0.0), (1.0, 3.0, 2.0, 1.0)]",
                                            "    [(2.0, 2.0, 2.0, 1.0), (1.0, 3.0, 2.0, 1.0)]",
                                            "    [(1.0, 3.0, 2.0, 0.0), (0.0, 2.0, 2.0, 0.0)]",
                                            "    [(0.0, 2.0, 2.0, 0.0), (0.0, 2.0, 2.0, 1.0)]",
                                            "    [(1.0, 3.0, 2.0, 1.0), (0.0, 2.0, 2.0, 1.0)]",
                                            "    [(0.0, 2.0, 2.0, 0.0), (1.0, 1.0, 2.0, 0.0)]",
                                            "    [(0.0, 2.0, 2.0, 1.0), (1.0, 1.0, 2.0, 1.0)]",
                                            "    [(1.0, 1.0, 0.0, 0.0), (1.0, 1.0, 2.0, 0.0)]",
                                            "    [(1.0, 1.0, 0.0, 1.0), (1.0, 1.0, 2.0, 1.0)]",
                                            "    [(2.0, 2.0, 0.0, 0.0), (2.0, 2.0, 2.0, 0.0)]",
                                            "    [(2.0, 2.0, 0.0, 1.0), (2.0, 2.0, 2.0, 1.0)]",
                                            "    [(1.0, 3.0, 0.0, 0.0), (1.0, 3.0, 2.0, 0.0)]",
                                            "    [(1.0, 3.0, 0.0, 1.0), (1.0, 3.0, 2.0, 1.0)]",
                                            "    [(0.0, 2.0, 0.0, 0.0), (0.0, 2.0, 2.0, 0.0)]",
                                            "    [(0.0, 2.0, 0.0, 1.0), (0.0, 2.0, 2.0, 1.0)]",
                                            "vertices:",
                                            "    1.000, 1.000, 0.000, 0.000, ",
                                            "    2.000, 2.000, 0.000, 0.000, ",
                                            "    1.000, 1.000, 0.000, 1.000, ",
                                            "    2.000, 2.000, 0.000, 1.000, ",
                                            "    1.000, 3.000, 0.000, 0.000, ",
                                            "    1.000, 3.000, 0.000, 1.000, ",
                                            "    0.000, 2.000, 0.000, 0.000, ",
                                            "    0.000, 2.000, 0.000, 1.000, ",
                                            "    1.000, 1.000, 2.000, 0.000, ",
                                            "    2.000, 2.000, 2.000, 0.000, ",
                                            "    1.000, 1.000, 2.000, 1.000, ",
                                            "    2.000, 2.000, 2.000, 1.000, ",
                                            "    1.000, 3.000, 2.000, 0.000, ",
                                            "    1.000, 3.000, 2.000, 1.000, ",
                                            "    0.000, 2.000, 2.000, 0.000, ",
                                            "    0.000, 2.000, 2.000, 1.000, ",
                                            "dimensionality: 4, embedded dimensionality: 4, measure: 4.000000000000001",
                                            "");
        TestUtils.testMultiLineToString(expected, cartProd);
    }
    
    /**
     * Test {@link NOrthotope#getFacets(int)}
     */
    @Test
    void testGetFacets() {
        final NOrthotope north = cartesianProduct(getExample(),
                                                  new NVector(0, 0, 0, 1),
                                                  new NVector(0, 0, 0, 0, 1),
                                                  new NVector(0, 0, 0, 0, 0, 1));
        assertSame(north, north.getFacets(6).findFirst().orElseThrow());
        testGetFacets(north, 5);
        testGetFacets(north, 4);
        testGetFacets(north, 3);
        testGetFacets(north, 2);
        testGetFacets(north, 1);
    }
    
    /**
     * Test {@link NOrthotope#boundingBoxContains(NPoint)}
     */
    @Test
    void testBoundingBoxContains() {
        // this call produces a 4-cube embedded in a 5-space
        final NOrthotope north = cartesianProduct(getExample(), new NVector(0, 0, 0, 2), new NVector(0, 0, 0, 0, 0));
        assertTrue(north.boundingBoxContains(new NPoint(1, 2, 0, 0, 0)));
        assertTrue(north.boundingBoxContains(new NPoint(0.5, 1.25, 0, 0, 0)));
        assertFalse(north.contains(new NPoint(0.5, 1.25, 0, 0, 0))); // not contained, but is in the BB
        
        assertFalse(north.boundingBoxContains(new NPoint(0, 0, 0, 0, 1)));
        assertFalse(north.boundingBoxContains(new NPoint(10, 10, 0, 0, 0)));
    }
    
    /**
     * Test {@link NOrthotope#existsInSameNSpace(NPoint)}
     */
    @Test
    void testExistsInSameNSpace() {
        final NOrthotope north = cartesianProduct(getExample(), new NVector(0, 0, 0, 2), new NVector(0, 0, 0, 0, 0));
        assertTrue(north.existsInSameNSpace(new NPoint(1, 2, 3, 0, 0)));
        assertTrue(north.existsInSameNSpace(new NPoint(1, 2, 3, 4, 0)));
        
        assertFalse(north.existsInSameNSpace(new NPoint(1, 2, 3, 0, 1)));
        assertFalse(north.existsInSameNSpace(new NPoint(1, 2, 3, 4, 1)));
    }
    
    /**
     * Test to ensure that we're properly creating {@link NOrthotope} facets of dimensionality
     * {@code facetDimensionality}. Calls {@link #testFacets(NOrthotope, Stream, int)} but also tests to show that we
     * have the correct number of total facets.
     * 
     * @param north the {@link NOrthotope} we're testing
     * @param facetDimensionality the dimensionality that we expect of the facets
     */
    private static void testGetFacets(final NOrthotope north, final int facetDimensionality) {
        final Collection<NOrthotope> facets = north.getFacets(facetDimensionality).collect(Collectors.toList());
        testFacets(north, facets.stream(), facetDimensionality);
        
        final long numCombos = IterableUtils.size(new Combinations(north.getDimensionality(), facetDimensionality));
        final long n = FastMath.round(FastMath.pow(2, north.getDimensionality() - facetDimensionality));
        final long expectedNum = n * numCombos;
        assertEquals(expectedNum, facets.size());
    }
    
    /**
     * Perform a Cartesian product that allows us to the increase dimension.
     * 
     * @param north the {@link NOrthotope}
     * @param vectors the {@link NVector}s with which we want to perform the Cartesian product
     * @return {@code north} CartProd with each of the provided {@link NVector}s
     */
    private static NOrthotope cartesianProduct(final NOrthotope north, final NVector... vectors) {
        NOrthotope currentNorth = north;
        for (final NVector vector : vectors) {
            if (currentNorth.getEmbeddedDimensionality() != vector.getEmbeddedDimensionality()) {
                final Set<NLineSegment> newEdges = currentNorth.getEdges().stream().map(edge -> {
                    final NPoint newEndPt1 = new NPoint(Arrays.copyOf(edge.getEndPoint1().getComponents(),
                                                                      vector.getEmbeddedDimensionality()));
                    final NPoint newEndPt2 = new NPoint(Arrays.copyOf(edge.getEndPoint2().getComponents(),
                                                                      vector.getEmbeddedDimensionality()));
                    return new NLineSegment(newEndPt1, newEndPt2);
                }).collect(Collectors.toCollection(SetUtils::empty));
                currentNorth = new NOrthotope(newEdges);
            }
            if (vector.magnitudeSquared() != 0.0) {
                currentNorth = currentNorth.cartesianProduct(vector);
            }
        }
        
        return currentNorth;
    }
    
    /**
     * Create and return a rectangular prism with height = 2, width = 2, and length = 2. It is a square rotated 45
     * degrees with a vertex on (1, 1, 0) extended to a depth/length of 2.
     * 
     * @return a rectangular prism {@link NOrthotope}
     */
    private static NOrthotope getExample() {
        // these first four are the line segments in the XY0 plane
        final NLineSegment seg01 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(2, 2, 0));
        final NLineSegment seg02 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(1, 3, 0));
        final NLineSegment seg03 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(0, 2, 0));
        final NLineSegment seg04 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(1, 1, 0));
        
        // these first four are the line segments in the XY1 plane
        final NLineSegment seg05 = new NLineSegment(new NPoint(1, 1, 2), new NPoint(2, 2, 2));
        final NLineSegment seg06 = new NLineSegment(new NPoint(2, 2, 2), new NPoint(1, 3, 2));
        final NLineSegment seg07 = new NLineSegment(new NPoint(1, 3, 2), new NPoint(0, 2, 2));
        final NLineSegment seg08 = new NLineSegment(new NPoint(0, 2, 2), new NPoint(1, 1, 2));
        
        // these first four are the line segments that connect the first surface to the second
        final NLineSegment seg09 = new NLineSegment(new NPoint(1, 1, 0), new NPoint(1, 1, 2));
        final NLineSegment seg10 = new NLineSegment(new NPoint(2, 2, 0), new NPoint(2, 2, 2));
        final NLineSegment seg11 = new NLineSegment(new NPoint(1, 3, 0), new NPoint(1, 3, 2));
        final NLineSegment seg12 = new NLineSegment(new NPoint(0, 2, 0), new NPoint(0, 2, 2));
        
        final NOrthotope answer = new NOrthotope(SetUtils.of(seg01,
                                                             seg02,
                                                             seg03,
                                                             seg04,
                                                             seg05,
                                                             seg06,
                                                             seg07,
                                                             seg08,
                                                             seg09,
                                                             seg10,
                                                             seg11,
                                                             seg12));
        
        validate(answer);
        return answer;
    }
    
    /**
     * Validate an {@link NOrthotope}. Also tests {@link NOrthotope#getVerticesToEdgesMap()}
     * 
     * @param north the {@link NOrthotope} to test
     */
    public static void validate(final NOrthotope north) {
        north.getVertices().forEach(north::verifyCompatibleDimensionality);
        north.getEdges().forEach(north::verifyCompatibleDimensionality);
        
        final long expNumVerts = FastMath.round(FastMath.pow(2, north.getDimensionality()));
        assertEquals(FastMath.pow(2, north.getDimensionality()),
                     north.getVertices().size(),
                     () -> String.format("the number of vertices must be 2^n (%s) where n is the dimension but it was %d",
                                         expNumVerts,
                                         north.getVertices().size()));
        
        north.getVerticesToEdgesMap().values().forEach(touchingEdges -> {
            for (final NLineSegment l1 : touchingEdges) {
                for (final NLineSegment l2 : touchingEdges) {
                    if (l1 == l2) {
                        continue;
                    }
                    
                    final double angle = l1.angleTo(l2);
                    assertEquals(FastMath.PI / 2.0, angle, 1e-6,
                                 () -> String.format("each edge of an orthotope must meet at exactly a right-angle but %s and %s met at %f degrees",
                                                     l1,
                                                     l2,
                                                     angle));
                }
            }
        });
    }
}
// CHECKSTYLE.ON: LineLength