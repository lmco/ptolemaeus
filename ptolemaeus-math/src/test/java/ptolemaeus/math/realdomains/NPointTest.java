/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.realdomains;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.linear.real.NVector;

/**
 * Tests {@link NPoint}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class NPointTest {
    
    /** Test {@link NPoint#toString()}, {@link NPoint#toString(String)}, and
     * {@link NPoint#toString(String, String, String, String)} */
    @Test
    void testToStrings() {
        final NPoint vector = new NPoint(0.754788115099576,
                                         0.695823752081075,
                                         0.18752350142414875,
                                         0.1609720885154029);
        
        final String expected1 = "(0.754788115099576, 0.695823752081075, 0.18752350142414875, 0.1609720885154029)";
        final String expected2 = "(0.755, 0.696, 0.188, 0.161)";
        final String expected3 = "[0.7548; 0.6958; 0.1875; 0.1610]";
        
        assertEquals(expected1, vector.toString());
        assertEquals(expected2, vector.toString("%.3f"));
        assertEquals(expected3, vector.toString("%.4f", "; ", "[", "]"));
    }
    
    /**
     * Test {@link NPoint#equals(NPoint, double)}
     */
    @Test
    void testEqualsWithTolerance() {
        final NPoint pt = new NPoint(1, 2, 3, 4, 5);
        final NPoint eq = new NPoint(1.1, 2.1, 3.1, 4.1, 5.1);
        final NPoint nq = new NPoint(1.2, 2.2, 3.2, 4.2, 5.2);
        
        assertTrue(pt.equals(eq, 0.224));
        assertFalse(pt.equals(nq, 0.224));
        
        assertFalse(pt.equals(eq, 0.220));
    }
    
    /**
     * Test {@link NPoint#getBoundaryPoint(NPoint, NPoint)}
     */
    @Test
    void testGetBoundaryPoint() {
        final NPoint pt = new NPoint(0.1, 0.2, 0.3, 0.4);
        assertSame(pt, pt.getBoundaryPoint(new NPoint(0.1, 0.2, 0.3, 0.4), new NPoint(1.1, 1.2, 1.3, 1.4)));
    }
    
    /**
     * Test {@link HasDimensionality#verifyCompatibleDimensionality(HasDimensionality)}
     */
    @Test
    void testVerifyCompatible() {
        final NPoint pt = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        final NPoint isComp = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        final NPoint isNotComp1 = new NPoint(new double[] { 0.1 });
        final NPoint isNotComp2 = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4, 0.1, 0.2, 0.3, 0.4 });
        final NPoint isNotComp3 = null;
        
        assertDoesNotThrow(() -> pt.verifyCompatibleDimensionality(pt));
        assertDoesNotThrow(() -> pt.verifyCompatibleDimensionality(isComp));
        assertThrows(NumericalMethodsException.class, () -> pt.verifyCompatibleDimensionality(isNotComp1));
        assertThrows(NumericalMethodsException.class, () -> pt.verifyCompatibleDimensionality(isNotComp2));
        assertThrows(NumericalMethodsException.class, () -> pt.verifyCompatibleDimensionality(isNotComp3));
    }
    
    /**
     * Test {@link NPoint#NPoint(double[])} using varargs and a double[], also test {@link NPoint#getComponent(int)},
     * {@link NPoint#getComponents()}, {@link NPoint#getEmbeddedDimensionality()}, and {@link NPoint#getMeasure()}
     */
    @Test
    void testNPointConstructor() {
        final NPoint pt = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        assertEquals(0.1, pt.getComponent(0));
        assertEquals(0.2, pt.getComponent(1));
        assertEquals(0.3, pt.getComponent(2));
        assertEquals(0.4, pt.getComponent(3));
        assertThrows(IllegalArgumentException.class, () -> pt.getComponent(4));
        
        assertArrayEquals(new double[] { 0.1, 0.2, 0.3, 0.4 }, pt.getComponents());
        
        assertEquals(4, pt.getEmbeddedDimensionality());
        assertEquals(0, pt.getMeasure());
        assertEquals(0, pt.getDimensionality());
    }
    
    /**
     * Test {@link NPoint#NPoint(List)} using a list of Doubles
     */
    @Test
    void testNPointConstructor2() {
        final NPoint pt = new NPoint(List.of(0.1, 0.2, 0.3, 0.4));
        assertEquals(0.1, pt.getComponent(0));
        assertEquals(0.2, pt.getComponent(1));
        assertEquals(0.3, pt.getComponent(2));
        assertEquals(0.4, pt.getComponent(3));
        assertThrows(IllegalArgumentException.class, () -> pt.getComponent(4));
    }
    
    /**
     * Test {@link NPoint#equals(Object)}
     */
    @Test
    void testEquals() {
        final NPoint pt = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        final NPoint eq = new NPoint(0.1, 0.2, 0.3, 0.4);
        final NPoint nq = new NPoint(new double[] { 0.0 });
        
        assertEquals(pt, pt);
        assertEquals(pt, eq);
        assertNotEquals(pt, nq);
        assertNotEquals(pt, null);
        assertNotEquals(pt, new Object());
    }
    
    /**
     * Test {@link NPoint#hashCode()}
     */
    @Test
    void testHashCode() {
        assertEquals(-1382055964, new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 }).hashCode());
    }
    
    /**
     * Test {@link NPoint#contains(NPoint)}
     */
    @Test
    void testContains() {
        final NPoint pt1 = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        final NPoint pt2 = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        final NPoint pt3 = new NPoint(new double[] { 1.1, 1.2, 1.3, 1.4 });
        
        assertTrue(pt1.contains(pt1));
        assertTrue(pt1.contains(pt2));
        assertFalse(pt1.contains(pt3));
    }
    
    /**
     * Test {@link NPoint#getZero(int)}
     */
    @Test
    void testGetOrigin() {
        assertEquals(new NPoint(), NPoint.getZero(0));
        assertEquals(new NPoint(0.0), NPoint.getZero(1));
        assertEquals(new NPoint(0.0, 0.0), NPoint.getZero(2));
        assertEquals(new NPoint(0.0, 0.0, 0.0), NPoint.getZero(3));
    }
    
    /**
     * Test {@link NPoint#plus(NPoint)}
     */
    @Test
    void testPlus() {
        final NPoint pt1 = new NPoint(1.0, 2.0, 3.0, 4.0);
        final NPoint pt2 = new NPoint(5.0, 6.0, 7.0, 8.0);
        assertEquals(new NPoint(6.0, 8.0, 10.0, 12.0), pt1.plus(pt2));
        assertThrows(NumericalMethodsException.class, () -> pt1.plus(new NPoint(0.0)));
    }
    
    /**
     * Test {@link NPoint#plus(NVector)}
     */
    @Test
    void testPlus2() {
        final NPoint pt1 = new NPoint(1.0, 2.0, 3.0, 4.0);
        final NVector vector = new NVector(5.0, 6.0, 7.0, 8.0);
        assertEquals(new NPoint(6.0, 8.0, 10.0, 12.0), pt1.plus(vector));
        assertThrows(NumericalMethodsException.class, () -> pt1.plus(new NPoint(0.0)));
    }
    
    /**
     * Test {@link NPoint#times(double)}
     */
    @Test
    void testTimes() {
        final NPoint pt = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        assertEquals(new NPoint(1.0, 2.0, 3.0, 4.0), pt.times(10));
    }
    
    /**
     * Test {@link NPoint#asVector()}
     */
    @Test
    void testAsVector() {
        final NPoint pt = new NPoint(new double[] { 0.1, 0.2, 0.3, 0.4 });
        final NVector vector = pt.asVector();
        assertEquals(new NVector(pt), vector);
    }
    
    /**
     * Test {@link NPoint#distanceTo(NPoint)}
     */
    @Test
    void testDistanceTo() {
        final NPoint pt1 = new NPoint(100.0, 100.0, 100.0, 100.0);
        final NPoint pt2 = new NPoint(200.0, 200.0, 200.0, 200.0);
        assertEquals(200.0, pt1.distanceTo(pt2));
    }
    
    /**
     * Test {@link NPoint#sqrDistanceTo(NPoint)}
     */
    @Test
    void testSqrDistanceTo() {
        final NPoint pt1 = new NPoint(100.0, 100.0, 100.0, 100.0);
        final NPoint pt2 = new NPoint(200.0, 200.0, 200.0, 200.0);
        assertEquals(40_000.0, pt1.sqrDistanceTo(pt2));
    }
    
    /**
     * Test {@link NPoint#getVectorTo(NPoint)}
     */
    @Test
    void testGetVectorTo() {
        final NPoint pt1 = new NPoint(new double[] { 1.0, 1.0, 1.0, 1.0 });
        final NPoint pt2 = new NPoint(new double[] { 2.0, 2.0, 2.0, 2.0 });
        assertEquals(new NVector(1.0, 1.0, 1.0, 1.0), pt1.getVectorTo(pt2));
        
        final NPoint pt3 = new NPoint(new double[] { 0.0, 2.0, 2.0, 2.0 });
        assertEquals(new NVector(-1.0, 1.0, 1.0, 1.0), pt1.getVectorTo(pt3));
        
        final NPoint pt4 = new NPoint(new double[] { 2.0, 0.0, 2.0, 2.0 });
        assertEquals(new NVector(1.0, -1.0, 1.0, 1.0), pt1.getVectorTo(pt4));
        
        final NPoint pt5 = new NPoint(new double[] { 2.0, 2.0, 0.0, 2.0 });
        assertEquals(new NVector(1.0, 1.0, -1.0, 1.0), pt1.getVectorTo(pt5));
        
        final NPoint pt6 = new NPoint(new double[] { 2.0, 2.0, 2.0, 0.0 });
        assertEquals(new NVector(1.0, 1.0, 1.0, -1.0), pt1.getVectorTo(pt6));
    }
}