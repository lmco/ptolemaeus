/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.ToDoubleFunction;

import org.junit.jupiter.api.Test;

import ptolemaeus.math.realdomains.RN;

/**
 * Test {@link MultivariateScalarValuedFunction}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class MultivariateScalarValuedFunctionTest {
    
    /**
     * f(x, y, z) = x*y*z
     */
    public static final ToDoubleFunction<double[]> XYZ = input -> input[0] * input[1] * input[2];
    
    /**
     * Test all of {@link MultivariateScalarValuedFunction}
     */
    @Test
    void testMost() {
        final MultivariateScalarValuedFunction f = new MultivariateScalarValuedFunction(XYZ, new RN(3));
        assertEquals(24D, f.applyAsDouble(new double[] { 2, 3, 4 }));
        assertEquals(24D, f.apply(new double[] { 2, 3, 4 }));
        
        final MultivariateScalarValuedFunction f2 = new MultivariateScalarValuedFunction(XYZ, 3);
        assertEquals(24D, f2.applyAsDouble(new double[] { 2, 3, 4 }));
        assertEquals(24D, f2.apply(new double[] { 2, 3, 4 }));
    }
}
