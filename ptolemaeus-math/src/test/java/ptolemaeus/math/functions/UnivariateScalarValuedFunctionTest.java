/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.DoubleUnaryOperator;

import org.junit.jupiter.api.Test;

import org.hipparchus.util.FastMath;

import ptolemaeus.math.realdomains.RN;

/**
 * Test {@link UnivariateScalarValuedFunction}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class UnivariateScalarValuedFunctionTest {
    
    /**
     * x^2
     */
    private static final DoubleUnaryOperator X_SQRD = x -> x * x;
    
    /**
     * sin(x)
     */
    private static final DoubleUnaryOperator SIN_X = FastMath::sin;
    
    /**
     * x ^ x  This is historically a very poorly behaved function computationally.
     */
    public static final DoubleUnaryOperator X_TO_THE_X = x -> FastMath.pow(x, x);
    
    /**
     * Test all of {@link UnivariateScalarValuedFunction}
     */
    @Test
    void testMost() {
        final UnivariateScalarValuedFunction f1 = new UnivariateScalarValuedFunction(X_SQRD, new RN(1));
        final UnivariateScalarValuedFunction f2 = new UnivariateScalarValuedFunction(SIN_X, new RN(1));
        final UnivariateScalarValuedFunction f3 = new UnivariateScalarValuedFunction(X_TO_THE_X, new RN(1));
        assertEquals(100D, f1.applyAsDouble(10));
        assertEquals(0D, f2.applyAsDouble(FastMath.PI), 1e-12);
        assertEquals(27D, f3.applyAsDouble(3));
    }
}
