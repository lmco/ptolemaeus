/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.functions;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * Test {@link VectorField}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class VectorFieldTest {
    
    /**
     * Test everything in {@link VectorField} and {@link AbstractRealFunction}
     */
    @Test
    void testAll() {
        final MultivariateVectorValuedFunction f = input -> {
            return new double[] {
                    input[0] * input[0], input[1] * input[1], input[0] * input[1] };
        };
        final VectorField mvvf = new VectorField(f, 2, 3);
        assertEquals(2, mvvf.getInputDimension());
        assertEquals(3, mvvf.getOutputDimension());
        mvvf.validateDimensions(new double[] { 4, 5 }); // doesn't throw, yay!
        
        final double[] expected = new double[] {9, 25, 15};
        assertArrayEquals(expected, mvvf.apply(new double[] { 3, 5 }));
    }
    
    /**
     * Test validate dimension failure
     */
    @Test
    void testException() {
        final MultivariateVectorValuedFunction f = input -> {
            return new double[] {
                    input[0] * input[0], input[1], input[1], input[0] * input[1] };
        };
        final VectorField mvvf = new VectorField(f, 2, 3);
        assertThrows(IllegalArgumentException.class, () -> mvvf.validateDimensions(new double[] { 4, 5, 6, 7 }));
    }
}
