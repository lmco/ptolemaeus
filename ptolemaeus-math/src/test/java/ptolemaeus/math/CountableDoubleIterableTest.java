/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.Spliterator;
import java.util.function.IntToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import org.junit.jupiter.api.Test;

import org.apache.commons.collections4.IterableUtils;

/**
 * Tests {@link CountableDoubleIterable}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class CountableDoubleIterableTest {
    
    /**
     * Test exceptional constructor cases
     */
    @Test
    void testExceptionalConstructorCases() {
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 0, true, 1, true));
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 0, true, 1, false));
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 0, false, 1, true));
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 0, false, 1, false));
        
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 1, true, 0, true));
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 1, true, 0, false));
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 1, false, 0, true));
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 1, false, 0, false));
        
        assertDoesNotThrow(() -> new CountableDoubleIterable(i -> 1.0 * i, 0, true, 0, true));
        assertThrows(IllegalArgumentException.class,
                     () -> new CountableDoubleIterable(i -> 1.0 * i, 0, true, 0, false));
        assertThrows(IllegalArgumentException.class,
                     () -> new CountableDoubleIterable(i -> 1.0 * i, 0, false, 0, true));
        assertThrows(IllegalArgumentException.class,
                     () -> new CountableDoubleIterable(i -> 1.0 * i, 0, false, 0, false));
    }
    
    /**
     * Test {@link CountableDoubleIterable#stream()} and {@link CountableDoubleIterable#spliterator()}
     */
    @Test
    void testStreamAndSpliterator() {
        final CountableDoubleIterable itrbl = CountableDoubleIterable.uniform(0D, 1_000_000D, 1_000_001);
        final DoubleStream stream = itrbl.stream().parallel();
        final List<Double> list = stream.mapToObj(Double::valueOf).collect(Collectors.toList());
        
        double prev = Double.NEGATIVE_INFINITY;
        for (final double d : list) {
            assertTrue(d > prev, "list was unordered");
            prev = d;
        }
        
        final List<Double> spltrList = new LinkedList<>();
        final Spliterator.OfDouble spltr = itrbl.spliterator();
        spltr.trySplit().tryAdvance((double d) -> spltrList.add(d));
        spltr.trySplit().tryAdvance((double d) -> spltrList.add(d)); // Spliterator can safely split
        
        assertEquals(List.of(0D, 1024D), spltrList);
    }
    
    /**
     * Test {@link CountableDoubleIterable#constant(double, int)}
     */
    @Test
    void testGetConstantRange() {
        testDiscreteDoubleRange(CountableDoubleIterable.constant(123D, 10),
                                123D,
                                123D,
                                123D,
                                123D,
                                123D,
                                123D,
                                123D,
                                123D,
                                123D,
                                123D);
        
        testDiscreteDoubleRange(CountableDoubleIterable.constant(0.0, 0)); // empty
        testDiscreteDoubleRange(CountableDoubleIterable.constant(123D, 1), 123D); // one element
    }
    
    /**
     * Test {@link CountableDoubleIterable#singleton(double)}
     */
    @Test
    void testGetSingletonRange() {
        testDiscreteDoubleRange(CountableDoubleIterable.singleton(1D), 1D);
    }
    
    /**
     * Test {@link CountableDoubleIterable#exponential(double, int, int)}
     */
    @Test
    void testGetExponentialRange() {
        testDiscreteDoubleRange(CountableDoubleIterable.exponential(2D, 0, 10),
                                1D,
                                2D,
                                4D,
                                8D,
                                16D,
                                32D,
                                64D,
                                128D,
                                256D,
                                512D,
                                1024D);
    }
    
    /**
     * Test {@link CountableDoubleIterable#exponential(double, int, int)} with a fractional base
     */
    @Test
    void testGetExponentialRangeFractionalBase() {
        testDiscreteDoubleRange(CountableDoubleIterable.exponential(0.5, 0, 10),
                                1D,
                                1D / 2D,
                                1D / 4D,
                                1D / 8D,
                                1D / 16D,
                                1D / 32D,
                                1D / 64D,
                                1D / 128D,
                                1D / 256D,
                                1D / 512D,
                                1D / 1024D);
    }
    
    /**
     * Test {@link CountableDoubleIterable#exponential(double, int, int)} with flipped indices and
     * {@link CountableDoubleIterable#toString()}
     */
    @Test
    void testGetExponentialRangeFlipped() {
        final CountableDoubleIterable cdi = CountableDoubleIterable.exponential(2D, 10, -3);
        testDiscreteDoubleRange(cdi,
                                1024D,
                                512D,
                                256D,
                                128D,
                                64D,
                                32D,
                                16D,
                                8D,
                                4D,
                                2D,
                                1D,
                                1D / 2D,
                                1D / 4D,
                                1D / 8D);
        assertEquals("[1024.0, 512.0, 256.0, 128.0, 64.0, 32.0, 16.0, 8.0, 4.0, 2.0, 1.0, 0.5, 0.25, 0.125]",
                     cdi.toString());
    }
    
    /**
     * Test {@link CountableDoubleIterable#exponential(double, int, int)} with a fractional base with flipped indices
     */
    @Test
    void testGetExponentialRangeFractionalBaseFlipeed() {
        testDiscreteDoubleRange(CountableDoubleIterable.exponential(0.5, 10, -3),
                                1D / 1024D,
                                1D / 512D,
                                1D / 256D,
                                1D / 128D,
                                1D / 64D,
                                1D / 32D,
                                1D / 16D,
                                1D / 8D,
                                1D / 4D,
                                1D / 2D,
                                1D / 1D,
                                2D,
                                4D,
                                8D);
    }
    
    /**
     * Test {@link CountableDoubleIterable#uniform(double, double, int)}
     */
    @Test
    void testGetUniformRange() {
        testDiscreteDoubleRange(CountableDoubleIterable.uniform(123D, 1123D, 11),
                                123D,
                                223D,
                                323D,
                                423D,
                                523D,
                                623D,
                                723D,
                                823D,
                                923D,
                                1023D,
                                1123D);
        
        testDiscreteDoubleRange(CountableDoubleIterable.uniform(123D, 456D, 0)); // empty
        testDiscreteDoubleRange(CountableDoubleIterable.uniform(123D, 456D, 1), 123D); // one element
        testDiscreteDoubleRange(CountableDoubleIterable.uniform(123D, 456D, 2), 123D, 456D); // two elements
    }
    
    /**
     * Test {@link CountableDoubleIterable#uniform(double, double, int)} with an interval which starts with the larger
     * numbers and moves to the smaller
     */
    @Test
    void testGetUniformRangeFlipped() {
        testDiscreteDoubleRange(CountableDoubleIterable.uniform(1123D, 123D, 11),
                                1123D,
                                1023D,
                                923D,
                                823D,
                                723D,
                                623D,
                                523D,
                                423D,
                                323D,
                                223D,
                                123D);
    }
    
    /**
     * Test {@link CountableDoubleIterable#monteCarlo(double, double, long, int)}
     */
    @Test
    void testGetMonteCarloRange() {
        testDiscreteDoubleRange(CountableDoubleIterable.monteCarlo(100D, 250D, 8675309L, 3),
                                211.29165338939475,
                                134.73302717141337,
                                138.0218070995708);
        
        testDiscreteDoubleRange(CountableDoubleIterable.monteCarlo(100D, 250D, 8675309L, 0)); // empty
        testDiscreteDoubleRange(CountableDoubleIterable.monteCarlo(100D, 250D, 8675309L, 1), 104.20715943628406); // one
    }
    
    /**
     * Test {@link CountableDoubleIterable#fromCollection(Collection)}
     */
    @Test
    void testGetCollectionRange() {
        testDiscreteDoubleRange(CountableDoubleIterable.fromCollection(List.of(123D, 456D, 789D)), 123D, 456D, 789D);
        testDiscreteDoubleRange(CountableDoubleIterable.fromCollection(List.of())); // empty
        testDiscreteDoubleRange(CountableDoubleIterable.fromCollection(List.of(123D)), 123D); // one element
    }
    
    /**
     * Test {@link CountableDoubleIterable#custom(IntToDoubleFunction, int, int)}
     */
    @Test
    void testGetCustomRange() {
        final IntToDoubleFunction f = i -> 1.0 * i * i;
        testDiscreteDoubleRange(CountableDoubleIterable.custom(f, 3, 9), 9D, 16D, 25D, 36D, 49D, 64D, 81D);
    }
    
    /**
     * Test that a {@link CountableDoubleIterable} produces the provided values on iteration
     * 
     * @param ddr the {@link CountableDoubleIterable} to test
     * @param expectedVals the expected values of that {@link CountableDoubleIterable}'s iteration
     */
    static void testDiscreteDoubleRange(final CountableDoubleIterable ddr, final double... expectedVals) {
        assertEquals(expectedVals.length, IterableUtils.size(ddr));
        
        final PrimitiveIterator.OfDouble itr = ddr.iterator();
        for (int i = 0; i < expectedVals.length; i++) {
            final int finalI = i;
            assertTrue(itr.hasNext(),
                       () -> String.format("the iterator %s did not have a value for index %d.  Array: %s",
                                           ddr,
                                           finalI,
                                           Arrays.toString(expectedVals)));
            assertEquals(expectedVals[i],
                         itr.next(),
                         1e-12,
                         () -> String.format("The iterator %s did not match the array %s",
                                             ddr,
                                             Arrays.toString(expectedVals)));
        }
        
        assertFalse(itr.hasNext(), "the iterator should not have had any remaining elements");
    }
}