/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.commons;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Quadratic.QuadraticRoots;

/** Test {@link Quadratic}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class QuadraticTest {
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with
     * {@code a == 0.0, b == 0.0, c == 0.0} and show that we throw an exception when
     * {@code a == 0.0, b == 0.0, c != 0.0} */
    @Test
    void testComputeQuadraticRootsConstant() {
        final QuadraticRoots roots = Quadratic.computeQuadraticRoots(0.0, 0.0, 0.0);
        assertEquals(Complex.ZERO, roots.smallerRoot());
        assertEquals(Complex.ZERO, roots.largerRoot());
        
        assertThrows(NumericalMethodsException.class, () -> Quadratic.computeQuadraticRoots(0.0, 0.0, 1.0));
    }
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with
     * {@code a == 0.0, b != 0.0, c == 0.0} */
    @Test
    void testComputeQuadraticRootsLinear1() {
        final QuadraticRoots roots = Quadratic.computeQuadraticRoots(0.0, 1.0, 0.0);
        assertEquals(Complex.ZERO, roots.smallerRoot());
        assertEquals(Complex.ZERO, roots.largerRoot());
        
        final QuadraticRoots roots2 = Quadratic.computeQuadraticRoots(0.0, 1000.0, 0.0);
        assertEquals(Complex.ZERO, roots2.smallerRoot());
        assertEquals(Complex.ZERO, roots2.largerRoot());
    }
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with
     * {@code a == 0.0, b != 0.0, c != 0.0} */
    @Test
    void testComputeQuadraticRootsLinear2() {
        final Iterator<Complex> answers = List.of(Complex.MINUS_ONE,
                                                  Complex.ONE,
                                                  Complex.ONE,
                                                  Complex.MINUS_ONE).iterator();
        for (final double b : new double[] { -10.0, 10.0 }) {
            for (final double c : new double[] { -10.0, 10.0 }) {
                final QuadraticRoots roots = Quadratic.computeQuadraticRoots(0.0, b, c);
                final Complex expected = answers.next();
                assertEquals(expected, roots.smallerRoot());
                assertEquals(expected, roots.largerRoot());
            }
        }
    }
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with
     * {@code b^2 - 4ac == 0.0} */
    @Test
    void testComputeQuadraticRootsZeroDiscriminant() {
        /*    b^2 - 4ac = 0
         * -> b^2       = 4ac
         * -> b         = +-2sqrt(ac) */
        final double a =  3.0;
        final double b = 18.0;
        final double c = 27.0;
        final QuadraticRoots roots1 = Quadratic.computeQuadraticRoots(a, b, c);
        assertEquals(roots1.smallerRoot(), roots1.largerRoot());
        assertEquals(new Complex(-3), roots1.smallerRoot());
        assertEquals(new Complex(-3), roots1.largerRoot());
        
        final QuadraticRoots roots2 = Quadratic.computeQuadraticRoots(a, -b, c);
        assertEquals(roots2.smallerRoot(), roots2.largerRoot());
        assertEquals(new Complex(3), roots2.smallerRoot());
        assertEquals(new Complex(3), roots2.largerRoot());
        
        assertTrue(roots1.areDegenerate());
        assertTrue(roots2.areDegenerate());
        assertTrue(roots1.areReal());
        assertTrue(roots2.areReal());
        
        assertTrue(roots1.smallerRoot().norm() <= roots1.largerRoot().norm());
    }
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with
     * {@code b^2 - 4ac > 0.0, a = 1.0} */
    @Test
    void testComputeQuadraticRootsPositiveDiscriminant1() {
        final double a =   1.0;
        final double b =  -3.0;
        final double c = -54.0;
        final QuadraticRoots roots = Quadratic.computeQuadraticRoots(a, b, c);
        assertEquals(new Complex(-6), roots.smallerRoot());
        assertEquals(new Complex(9), roots.largerRoot());
        
        assertFalse(roots.areDegenerate());
        assertTrue(roots.areReal());
        
        assertTrue(roots.smallerRoot().norm() <= roots.largerRoot().norm());
    }
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with
     * {@code b^2 - 4ac > 0.0, a != 1.0} */
    @Test
    void testComputeQuadraticRootsPositiveDiscriminant2() {
        /*   (2x - 6)(-3x + 11) = 0 -> x = 3, 11/3
         * = -6x^2 + 40x - 66 = 0 */
        final double a =  -6.0;
        final double b =  40.0;
        final double c = -66.0;
        final QuadraticRoots roots = Quadratic.computeQuadraticRoots(a, b, c);
        assertEquals(new Complex(3), roots.smallerRoot());
        assertEquals(new Complex(11.0 / 3), roots.largerRoot());
        
        assertTrue(roots.smallerRoot().norm() <= roots.largerRoot().norm());
    }
    
    /** Test {@link Quadratic#computeQuadraticRoots(double, double, double)} with {@code b^2 - 4ac < 0.0} */
    @Test
    void testComputeQuadraticRootsNegativeDiscriminant() {
        /* to get a test with "nice" values, we need the discriminant to be a negative perfect square and a, b, and c to
         * be integers (or "nice" fractions).  Here, the discriminant is 36 - 4 * 2.5 * 10 = 36 - 100 = 64.
         * So, we have x = -6 / (2 * 2.5) +- i * sqrt(64) / (2 * 2.5) 
         *               = -6/5 +- 8i/5 */
        final double a =  2.5;
        final double b =  6.0;
        final double c = 10.0;
        final QuadraticRoots roots = Quadratic.computeQuadraticRoots(a, b, c);
        assertEquals(new Complex(-6.0 / 5.0, 8.0 / 5.0), roots.smallerRoot());
        assertEquals(new Complex(-6.0 / 5.0, -8.0 / 5.0), roots.largerRoot());
        
        assertFalse(roots.areDegenerate());
        assertFalse(roots.areReal());
        
        assertTrue(roots.smallerRoot().norm() <= roots.largerRoot().norm());
    }
}
