/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.commons;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static ptolemaeus.commons.TestUtils.assertGreaterThan;
import static ptolemaeus.commons.TestUtils.assertLessThan;
import static ptolemaeus.commons.TestUtils.assertThrowsWithMessage;
import static ptolemaeus.math.commons.Numerics.MACHINE_EPSILON;
import static ptolemaeus.math.commons.Numerics.complexSqrt;
import static ptolemaeus.math.commons.Numerics.complexSquare;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.LUDecomposition;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.functions.MultivariateScalarValuedFunctionTest;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.UnivariateScalarValuedFunctionTest;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.linear.schur.MATLABSchurIO;

/** Tests for {@link Numerics}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class NumericsTest {
    
    // CHECKSTYLE.OFF: LocalFinalVariableName - it is customary to name matrices using upper-case characters
    
    /** Test {@link Numerics#requireNonNaN(RealMatrix)} and {@link Numerics#requireNonNaN(ComplexMatrix)} */
    @Test
    void testRequireNonNan() {
        final Matrix        real = MatrixTest.randomMatrix(10, 10, 1.0, 123L);
        final ComplexMatrix imag = ComplexMatrixTest.randomComplexMatrix(10, 10, 123L);
        
        assertSame(real, Numerics.requireNonNaN(real));
        assertSame(imag, Numerics.requireNonNaN(imag));
        
        real.setEntry(1, 2, Double.NaN);
        imag.setEntry(3, 4, Complex.NaN);
        
        assertThrowsWithMessage(NumericalMethodsException.class,
                                () -> Numerics.requireNonNaN(real),
                                "matrix element at (1, 2) was NaN");
        assertThrowsWithMessage(NumericalMethodsException.class,
                                () -> Numerics.requireNonNaN(imag),
                                "matrix element at (3, 4) was NaN");
    }
    
    /** Test {@link Numerics#getMatrixSmlNum(AnyMatrix)} */
    @Test
    void testGetMatrixSmlNum() {
        final Matrix A = new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 } });
        
        assertEquals(1.7536473150078527E-292, Numerics.getMatrixSmlNum(A));
    }
    
    /** Test {@link Numerics#getInftyNormMatrixEpsilon(RealMatrix)} */
    @Test
    void testGetEpsilonReal() {
        final Matrix A = new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 } });
        assertEquals(5 * Numerics.MACHINE_EPSILON, Numerics.getInftyNormMatrixEpsilon(A));
        
        final Matrix zeroInftyNorm = A.scalarMultiply(0.0);
        assertEquals(Numerics.getMatrixSmlNum(zeroInftyNorm),
                     Numerics.getInftyNormMatrixEpsilon(zeroInftyNorm));
    }
    
    /** Test {@link Numerics#getInftyNormMatrixEpsilon(ComplexMatrix)} */
    @Test
    void testGetEpsilonComplex() {
        final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 } });
        assertEquals(5 * Numerics.MACHINE_EPSILON, Numerics.getInftyNormMatrixEpsilon(A));
        
        final ComplexMatrix zeroInftyNorm = A.scalarMultiply(0.0);
        assertEquals(Numerics.getMatrixSmlNum(zeroInftyNorm),
                     Numerics.getInftyNormMatrixEpsilon(zeroInftyNorm));
    }
    
    // CHECKSTYLE.ON: LocalFinalVariableName - it is customary to name matrices using upper-case characters
    
    /** Test {@link Numerics#complex1Norm(Complex)} */
    @Test
    void testComplex1Norm() {
        final Complex z0 = Complex.ZERO;
        final Complex z1 = Complex.valueOf(-1,  0);
        final Complex z2 = Complex.valueOf(0,  -1);
        final Complex z3 = Complex.valueOf(-1, -1);
        
        assertEquals(0, Numerics.complex1Norm(z0));
        assertEquals(1, Numerics.complex1Norm(z1));
        assertEquals(1, Numerics.complex1Norm(z2));
        assertEquals(2, Numerics.complex1Norm(z3));
    }
    
    /** Simple tests to show that the non-trivial constants are computed correctly or are at least unchanging */
    @Test
    void testNonTrivialConstants() {
        final double machEps = Numerics.MACHINE_EPSILON;
        assertEquals(FastMath.nextUp(1.0) - 1.0, machEps);
        assertGreaterThan(1.0 + machEps, 1.0, "MACHINE_EPSILON");
        assertEquals(1.0, 1.0 + machEps / 2.0, "half MACHINE_EPSILON");
        assertEquals(machEps,
                     Numerics.SQUARE_ROOT_MACHINE_EPSILON
                              * Numerics.SQUARE_ROOT_MACHINE_EPSILON);
        assertEquals(machEps,
                     Numerics.CUBE_ROOT_MACHINE_EPSILON
                              * Numerics.CUBE_ROOT_MACHINE_EPSILON
                              * Numerics.CUBE_ROOT_MACHINE_EPSILON,
                     machEps); // cbrt is imperfect!
        
        assertLessThan(1.0 / Numerics.SAFE_MIN, Double.POSITIVE_INFINITY, "SAFE_MIN");
        assertEquals(1.0 / FastMath.nextDown(Numerics.SAFE_MIN), Double.POSITIVE_INFINITY);
        assertEquals(Numerics.SAFE_MIN, Double.longBitsToDouble(0x4000000000001L));
        
        assertEquals(Double.MAX_VALUE,  Numerics.HUGE);
        assertEquals(Double.MIN_NORMAL, Numerics.TINY);
        
        assertEquals(3.674495243916449E-303, Numerics.MIN_AUTOSCALING_X);
        
        assertEquals(6.283185307179586, Numerics.TWO_PI);
        assertEquals(Numerics.TAU, Numerics.TWO_PI);
    }
    
    /** Test {@link Numerics#complexNorm(double, double)} */
    @Test
    void testComplexNorm() {
        assertEquals(5.0, Numerics.complexNorm(3, 4));
        assertEquals(5.0, Numerics.complexNorm(Complex.valueOf(3, 4)));
    }
    
    /** Test {@link Numerics#complexNormSq(double, double)} */
    @Test
    void testComplexNormSq() {
        assertEquals(25.0, Numerics.complexNormSq(3, 4));
        assertEquals(25.0, Numerics.complexNormSq(Complex.valueOf(3, 4)));
    }

        /** Test {@link Numerics#complexXYRe(double, double, double, double)} and
     * {@link Numerics#complexXYIm(double, double, double, double)} */
    @Test
    void testComplexXYReIm() {
        final Complex z1 = Complex.valueOf(2, 3);
        final Complex z2 = Complex.valueOf(5, 7);
        
        final Complex z1z2 = z1.multiply(z2);
        
        final double z1z2Re = Numerics.complexXYRe(z1.getReal(), z1.getImaginary(),
                                                                z2.getReal(), z2.getImaginary());
        final double z1z2Im = Numerics.complexXYIm(z1.getReal(), z1.getImaginary(),
                                                                z2.getReal(), z2.getImaginary());
        
        assertEquals(z1z2.getReal(), z1z2Re);
        assertEquals(z1z2.getImaginary(), z1z2Im);
    }
    
    /** Test {@link Numerics#complexDivide(double, double, double, double)} */
    @Test
    void testComplexDivide() {
        // case 1: abs(c) < abs(d)
        final Complex x1 = Complex.valueOf(12, 34);
        final Complex y1 = Complex.valueOf(56, 78);
        
        final Complex expected1 = x1.divide(y1);
        
        final double[] actual1a = Numerics.complexDivide(x1.getReal(), x1.getImaginary(),
                                                                      y1.getReal(), y1.getImaginary());
        final double[] actual1b = new double[2];
        Numerics.complexDivide(x1.getReal(), x1.getImaginary(),
                                            y1.getReal(), y1.getImaginary(),
                                            actual1b);
        
        final Complex actComplex1a = Complex.valueOf(actual1a[0], actual1a[1]);
        final Complex actComplex1b = Complex.valueOf(actual1b[0], actual1b[1]);
        
        assertEquals(expected1, actComplex1a);
        assertEquals(expected1, actComplex1b);
        
        // case 2: abs(d) < abs(c)
        final Complex x2 = Complex.valueOf(12, 34);
        final Complex y2 = Complex.valueOf(78, 56);
        
        final Complex expected2 = x2.divide(y2);
        
        final double[] actual2a = Numerics.complexDivide(x2.getReal(), x2.getImaginary(),
                                                                      y2.getReal(), y2.getImaginary());
        final double[] actual2b = new double[2];
        Numerics.complexDivide(x2.getReal(), x2.getImaginary(),
                                            y2.getReal(), y2.getImaginary(),
                                            actual2b);
        
        final Complex actComplex2a = Complex.valueOf(actual2a[0], actual2a[1]);
        final Complex actComplex2b = Complex.valueOf(actual2b[0], actual2b[1]);
        
        assertEquals(expected2, actComplex2a);
        assertEquals(expected2, actComplex2b);
    }

    /** Test {@link Numerics#complexSqrt(double, double)} and 
     *       {@link Numerics#complexSqrt(double, double, double[])} */
    @Test
    void testComplexSqrt() {
        // {2 + 11i, 1 + 0i, 0 + 1i, -1 + 0i, 0 - 1i}
        final double[][] v = new double[][] { {  2,  1, 0, -1, 0 }, { 11, 0, 1, 0, -1 } };
        
        double[] actual = complexSqrt(v[0][1], v[1][1]);

        final double[][] expected = new double[][] {{ 2.56713263, 1, 0.707106781, 0,  0.707106781},
                                                    { 2.14246819, 0, 0.707106781, 1, -0.707106781}};

        for (int i = 0; i < v[0].length; i++) {
            complexSqrt(v[0][i], v[1][i], actual);
            assertEquals(expected[0][i], actual[0], 0.00001);
            assertEquals(expected[1][i], actual[1], 0.00001);
            assertEquals(Complex.valueOf(v[0][i], v[1][i]).sqrt(), Complex.valueOf(actual[0], actual[1]));
        }
    }

    /** Test {@link Numerics#complexSquare(double, double)} and
     *       {@link Numerics#complexSquare(double, double, double[])} */
    @Test
    void testComplexSquare() {
        final double[][] v = new double[][] { {  2, 1, 0, -1,  0 },
                                              { 11, 0, 1,  0, -1 } };
        final double[] actual = new double[2];
        
        complexSquare(v[0][1], v[1][1], actual);
        
        final double[][] expected = new double[][] {{ 2 * 2 - 11 * 11, 1, -1, 1, -1},
                                                    { 2 * 2 * 11,      0,  0, 0,  0}};
        
        for (int i = 0; i < v[0].length; i++) {
            complexSquare(v[0][i], v[1][i], actual);
            assertEquals(expected[0][i], actual[0], 0.00001);
            assertEquals(expected[1][i], actual[1], 0.00001);
            assertEquals(Complex.valueOf(v[0][i], v[1][i]).square(), Complex.valueOf(actual[0], actual[1]));
        }
    }
    
    /** Test {@link Numerics#toNonZeroStructureString(ComplexMatrix, double, char)}
     *  and  {@link Numerics#toNonZeroStructureString(ComplexMatrix)} */
    @Test
    void testToNonZeroStructureString() {
        final ComplexMatrix upperHess      = ComplexMatrixTest.randomUpperNHessenberg(10, 1, 1.0, 1L, false);
        final String        nonZeroStruct1 = Numerics.toNonZeroStructureString(upperHess, 0.0, '^');
        final String        expected1      = String.join(System.lineSeparator(),
                                                         " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^",
                                                         " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^",
                                                         "   ^ ^ ^ ^ ^ ^ ^ ^ ^",
                                                         "     ^ ^ ^ ^ ^ ^ ^ ^",
                                                         "       ^ ^ ^ ^ ^ ^ ^",
                                                         "         ^ ^ ^ ^ ^ ^",
                                                         "           ^ ^ ^ ^ ^",
                                                         "             ^ ^ ^ ^",
                                                         "               ^ ^ ^",
                                                         "                 ^ ^",
                                                         "");
        TestUtils.testMultiLineToString(expected1, nonZeroStruct1);
        
        assertEquals(Numerics.toNonZeroStructureString(upperHess, 0.0, '*'),
                     Numerics.toNonZeroStructureString(upperHess));
        
        final ComplexMatrix reducedUpperHess = upperHess.copy();
        reducedUpperHess.setEntry(5, 4, Complex.valueOf(1e-9));
        final String nonZeroStruct2 = Numerics.toNonZeroStructureString(reducedUpperHess, 1e-8, '^');
        final String expected2      = String.join(System.lineSeparator(),
                                                  " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^",
                                                  " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^",
                                                  "   ^ ^ ^ ^ ^ ^ ^ ^ ^",
                                                  "     ^ ^ ^ ^ ^ ^ ^ ^",
                                                  "       ^ ^ ^ ^ ^ ^ ^",
                                                  "           ^ ^ ^ ^ ^",
                                                  "           ^ ^ ^ ^ ^",
                                                  "             ^ ^ ^ ^",
                                                  "               ^ ^ ^",
                                                  "                 ^ ^",
                                                  "");
        TestUtils.testMultiLineToString(expected2, nonZeroStruct2);
    }
    
    /** Test {@link Numerics#toStructureString(FieldMatrix, Predicate, char)} */
    @Test
    void testToStructureString() {
        final Predicate<Complex> test = z -> z.getReal() > 0.1;
        
        final ComplexMatrix upperHess      = ComplexMatrixTest.randomUpperNHessenberg(10, 1, 1.0, 1L, false);
        final String        nonZeroStruct1 = Numerics.toStructureString(upperHess, test, ';');
        final String        expected1      = String.join(System.lineSeparator(),
                                                         "     ; ;   ;     ;  ",
                                                         " ;         ; ; ;   ;",
                                                         "   ;   ;   ; ;     ;",
                                                         "         ;       ;  ",
                                                         "       ; ;       ; ;",
                                                         "                 ; ;",
                                                         "           ; ; ;    ",
                                                         "             ;     ;",
                                                         "                 ;  ",
                                                         "                 ; ;", // The Matrix...
                                                         "");
        TestUtils.testMultiLineToString(expected1, nonZeroStruct1);
        
        final ComplexMatrix upperHess2 = upperHess.copy();
        upperHess2.setEntry(9, 9, Complex.ZERO);
        final String nonZeroStruct2 = Numerics.toStructureString(upperHess2, test, ';');
        final String expected2      = String.join(System.lineSeparator(),
                                                  "     ; ;   ;     ;  ",
                                                  " ;         ; ; ;   ;",
                                                  "   ;   ;   ; ;     ;",
                                                  "         ;       ;  ",
                                                  "       ; ;       ; ;",
                                                  "                 ; ;",
                                                  "           ; ; ;    ",
                                                  "             ;     ;",
                                                  "                 ;  ",
                                                  "                 ;  ",
                                                  "");
        TestUtils.testMultiLineToString(expected2, nonZeroStruct2);
    }
    
    /** Test {@link Numerics#toJavaCode(AnyMatrix)} with a {@link RealMatrix} */
    @Test
    void testToJavaReal1() {
        final double[][] recData     = new double[][] { { 1, 3 }, { 5, 7 }, { 9, 11 } };
        final Matrix     rectangular = new Matrix(recData);
        
        final String expected1 = String.join(System.lineSeparator(),
                                            "final double[][] AData = new double[][] {",
                                            "    {  1,   3 },",
                                            "    {  5,   7 },",
                                            "    {  9,  11 }",
                                            "};",
                                            "final RealMatrix A = new Matrix(AData);");
        TestUtils.testMultiLineToString(expected1, Numerics.toJavaCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                                            "final double[][] BData = new double[][] {",
                                            "    {  1,   3 },",
                                            "    {  5,   7 },",
                                            "    {  9,  11 }",
                                            "};",
                                            "final RealMatrix B = new Matrix(BData);");
        TestUtils.testMultiLineToString(expected2, Numerics.toJavaCode(rectangular, "B"));
        
        // the following (between the comments) is the actual code generated:
        
        // CHECKSTYLE.OFF: LocalFinalVariableName - customary to customize matrix variables
        // CHECKSTYLE.OFF: ParenPad - generated code
        
        final double[][] AData = new double[][] {
            {  1,   3 },
            {  5,   7 },
            {  9,  11 }
        };
        final RealMatrix A = new Matrix(AData);
        
        // CHECKSTYLE.ON: ParenPad - generated code
        // CHECKSTYLE.ON: LocalFinalVariableName
        
        assertEquals(rectangular, A);
    }
    
    /** Test {@link Numerics#toJavaCode(AnyMatrix)} with a {@link ComplexMatrix} with strictly real
     * entries */
    @Test
    void testToJavaReal2() {
        final double[][]    recData     = new double[][] { { 1, 3 }, { 5, 7 }, { 9, 11 } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        
        final String expected1 = String.join(System.lineSeparator(),
                                            "final double[][] AData = new double[][] {",
                                            "    {  1,   3 },",
                                            "    {  5,   7 },",
                                            "    {  9,  11 }",
                                            "};",
                                            "final ComplexMatrix A = new ComplexMatrix(AData);");
        TestUtils.testMultiLineToString(expected1, Numerics.toJavaCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                                            "final double[][] BData = new double[][] {",
                                            "    {  1,   3 },",
                                            "    {  5,   7 },",
                                            "    {  9,  11 }",
                                            "};",
                                            "final ComplexMatrix B = new ComplexMatrix(BData);");
        TestUtils.testMultiLineToString(expected2, Numerics.toJavaCode(rectangular, "B"));
        
        // the following (between the comments) is the actual code generated:
        
        // CHECKSTYLE.OFF: LocalFinalVariableName - customary to customize matrix variables
        // CHECKSTYLE.OFF: ParenPad - generated code
        
        final double[][] AData = new double[][] {
            {  1,   3 },
            {  5,   7 },
            {  9,  11 }
        };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        // CHECKSTYLE.ON: ParenPad - generated code
        // CHECKSTYLE.ON: LocalFinalVariableName
        
        assertEquals(rectangular, A);
    }
    
    /** Test {@link Numerics#toMathematicaCode(AnyMatrix)} with a {@link RealMatrix} */
    @Test
    void testToMathematicaReal() {
        final double[][] recData     = new double[][] { { 1, 3 }, { 5, 7 }, { 9, 11 } };
        final Matrix     rectangular = new Matrix(recData);
        
        final String expected1 = String.join(System.lineSeparator(),
                                            "A = {",
                                            "    {  1,   3 },",
                                            "    {  5,   7 },",
                                            "    {  9,  11 }",
                                            "};");
        TestUtils.testMultiLineToString(expected1, Numerics.toMathematicaCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                                            "B = {",
                                            "    {  1,   3 },",
                                            "    {  5,   7 },",
                                            "    {  9,  11 }",
                                            "};");
         TestUtils.testMultiLineToString(expected2, Numerics.toMathematicaCode(rectangular, "B"));
    }
    
    /** Test {@link Numerics#toMATLABCode(AnyMatrix)} with a {@link RealMatrix} */
    @Test
    void testToMATLABReal() {
        final double[][] recData     = new double[][] { { 1, 3 }, { 5, 7 }, { 9, 11 } };
        final Matrix     rectangular = new Matrix(recData);
        
        final String expected1 = String.join(System.lineSeparator(),
                                            "A = [",
                                            "     1,   3;",
                                            "     5,   7;",
                                            "     9,  11;",
                                            "];");
        TestUtils.testMultiLineToString(expected1, Numerics.toMATLABCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                                            "B = [",
                                            "     1,   3;",
                                            "     5,   7;",
                                            "     9,  11;",
                                            "];");
         TestUtils.testMultiLineToString(expected2, Numerics.toMATLABCode(rectangular, "B"));
    }
    
    /** Test {@link Numerics#toJavaCode(AnyMatrix)} with a {@link ComplexMatrix} */
    @Test
    void testToJavaComplex() {
        final Complex[][] recData = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                      { new Complex(5, 6), new Complex(7, 8) },
                                                      { new Complex(9, 10), new Complex(11, 12) } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        
        
        final String expected1 = String.join(System.lineSeparator(),
                "final Complex[][] AData = new Complex[][] {",
                "    { new Complex( 1,  2), new Complex(  3,  4) },",
                "    { new Complex( 5,  6), new Complex(  7,  8) },",
                "    { new Complex( 9, 10), new Complex( 11, 12) }",
                "};",
                "final ComplexMatrix A = new ComplexMatrix(AData);");
        TestUtils.testMultiLineToString(expected1, Numerics.toJavaCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                "final Complex[][] BData = new Complex[][] {",
                "    { new Complex( 1,  2), new Complex(  3,  4) },",
                "    { new Complex( 5,  6), new Complex(  7,  8) },",
                "    { new Complex( 9, 10), new Complex( 11, 12) }",
                "};",
                "final ComplexMatrix B = new ComplexMatrix(BData);");
        TestUtils.testMultiLineToString(expected2, Numerics.toJavaCode(rectangular, "B"));
        
        // the following (between the comments) is the actual code generated:
        
        // CHECKSTYLE.OFF: LocalFinalVariableName - customary to customize matrix variables
        // CHECKSTYLE.OFF: ParenPad - generated code
        
        final Complex[][] AData = new Complex[][] {
            { new Complex( 1.0,  2.0), new Complex(  3.0,  4.0) },
            { new Complex( 5.0,  6.0), new Complex(  7.0,  8.0) },
            { new Complex( 9.0, 10.0), new Complex( 11.0, 12.0) }
        };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        // CHECKSTYLE.ON: ParenPad - generated code
        // CHECKSTYLE.ON: LocalFinalVariableName
        
        assertEquals(rectangular, A);
    }
    
    /** Test {@link Numerics#toMathematicaCode(AnyMatrix)} with a {@link ComplexMatrix} */
    @Test
    void testToMathematicaComplex() {
        final Complex[][] recData = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                      { new Complex(5, 6), new Complex(7, 8) },
                                                      { new Complex(9, 10), new Complex(11, 12) } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        
        final String expected1 = String.join(System.lineSeparator(),
                "A = {",
                "    {  1 +  2 I,   3 +  4 I },",
                "    {  5 +  6 I,   7 +  8 I },",
                "    {  9 + 10 I,  11 + 12 I }",
                "};");
        TestUtils.testMultiLineToString(expected1, Numerics.toMathematicaCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                 "B = {",
                 "    {  1 +  2 I,   3 +  4 I },",
                 "    {  5 +  6 I,   7 +  8 I },",
                 "    {  9 + 10 I,  11 + 12 I }",
                 "};");
         TestUtils.testMultiLineToString(expected2, Numerics.toMathematicaCode(rectangular, "B"));
    }
    
    /** Test {@link Numerics#toMATLABCode(AnyMatrix)} with a {@link ComplexMatrix} */
    @Test
    void testToMATLABComplex() {
        final Complex[][] recData = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                      { new Complex(5, 6), new Complex(7, 8) },
                                                      { new Complex(9, 10), new Complex(11, 12) } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        
        final String expected1 = String.join(System.lineSeparator(),
                "A = [",
                "     1 +  2 * 1i,   3 +  4 * 1i;",
                "     5 +  6 * 1i,   7 +  8 * 1i;",
                "     9 + 10 * 1i,  11 + 12 * 1i;",
                "];");
        TestUtils.testMultiLineToString(expected1, Numerics.toMATLABCode(rectangular));
        
        final String expected2 = String.join(System.lineSeparator(),
                 "B = [",
                 "     1 +  2 * 1i,   3 +  4 * 1i;",
                 "     5 +  6 * 1i,   7 +  8 * 1i;",
                 "     9 + 10 * 1i,  11 + 12 * 1i;",
                 "];");
         TestUtils.testMultiLineToString(expected2, Numerics.toMATLABCode(rectangular, "B"));
    }
    
    // CHECKSTYLE.OFF: LocalFinalVariableName - it is customary to name matrices using upper-case characters
    /**
     * Test {@link Numerics#requireSquare(AnyMatrix)}
     */
    @Test
    void testCheckSquare() {
        assertDoesNotThrow(() -> Numerics.requireSquare(MatrixTest.SQR_MATRIX));
        final MathIllegalArgumentException ex = assertThrows(MathIllegalArgumentException.class,
                                                 () -> Numerics.requireSquare(MatrixTest.SKINNY_MATRIX));
        assertEquals("non square (8x5) matrix", ex.getMessage());
    }
    
    /** Test {@link Numerics#computeAInvB(RealMatrix, RealMatrix, Function)} and
     *       {@link Numerics#computeAInvB(RealMatrix, RealMatrix)} */
    @Test
    void testComputeAInvB() {
        final Matrix A = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 } });
        
        final Matrix B = new Matrix(new double[][] { { 45, 40, 40, 45, 55 },
                                                     { 40, 40, 45, 55, 45 },
                                                     { 40, 45, 55, 45, 40 },
                                                     { 45, 55, 45, 40, 40 },
                                                     { 55, 45, 40, 40, 45 } });

        final Matrix expectedX = new Matrix(new double[][] { { 5, 4, 3, 2, 1 },
                                                             { 1, 5, 4, 3, 2 },
                                                             { 2, 1, 5, 4, 3 },
                                                             { 3, 2, 1, 5, 4 },
                                                             { 4, 3, 2, 1, 5 } });
        
        final RealMatrix actualX1 = Numerics.computeAInvB(A, B); // solution to AX = B
        final double epsilon1 = 5e-14;
        MatrixTest.assertMatrixEquals(expectedX, actualX1, epsilon1);
        MatrixTest.assertMatrixEquals(B, A.multiply(actualX1), epsilon1);
        
        final Function<RealMatrix, DecompositionSolver> matrix2Decomp = M -> new LUDecomposition(M).getSolver();
        final RealMatrix actualX2 = Numerics.computeAInvB(A, B, matrix2Decomp); // solution to AX = B
        final double epsilon2 = 2e-15; // LUDecomposition does a better job with this problem
        MatrixTest.assertMatrixEquals(expectedX, actualX2, epsilon2);
        MatrixTest.assertMatrixEquals(B, A.multiply(actualX2), epsilon2);
    }
    
    /** Test {@link Numerics#computeABInv(RealMatrix, RealMatrix, Function)} and
     * {@link Numerics#computeABInv(RealMatrix, RealMatrix)}<br><br>
     * 
     * Note: the test vectors used here are set up so that we're solving the same problem as in
     * {@link #testComputeAInvB()} */
    @Test
    void testComputeABInv() {
        final Matrix B = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 } }).transpose();
        
        final Matrix A = new Matrix(new double[][] { { 45, 40, 40, 45, 55 },
                                                     { 40, 40, 45, 55, 45 },
                                                     { 40, 45, 55, 45, 40 },
                                                     { 45, 55, 45, 40, 40 },
                                                     { 55, 45, 40, 40, 45 } }).transpose();

        final Matrix expectedX = new Matrix(new double[][] { { 5, 4, 3, 2, 1 },
                                                             { 1, 5, 4, 3, 2 },
                                                             { 2, 1, 5, 4, 3 },
                                                             { 3, 2, 1, 5, 4 },
                                                             { 4, 3, 2, 1, 5 } }).transpose();
        
        final RealMatrix actualX1 = Numerics.computeABInv(A, B); // solution to XB = A
        final double epsilon1 = 5e-14;
        MatrixTest.assertMatrixEquals(expectedX, actualX1, epsilon1);
        MatrixTest.assertMatrixEquals(A, actualX1.multiply(B), epsilon1);
        
        final Function<RealMatrix, DecompositionSolver> matrix2Decomp = M -> new LUDecomposition(M).getSolver();
        final RealMatrix actualX2 = Numerics.computeABInv(A, B, matrix2Decomp); // solution to AX = B
        final double epsilon2 = 2e-15; // LUDecomposition does a better job with this problem
        MatrixTest.assertMatrixEquals(expectedX, actualX2, epsilon2);
        MatrixTest.assertMatrixEquals(A, actualX2.multiply(B), epsilon2);
    }
    
    /** Test {@link Numerics#computeAInvB(RealMatrix, RealVector, Function)} and
     *       {@link Numerics#computeAInvB(RealMatrix, RealVector)} */
    @Test
    void testComputeAInvBVector() {
        final Matrix A = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 } });
        
        final RealVector b = new NVector(new double[] { 45, 40, 40, 45, 55 });
        
        final RealVector expectedX = new NVector(new double[] { 5, 1, 2, 3, 4 });

        final RealVector actualX1 = Numerics.computeAInvB(A, b); // solution to AX = B
        
        final double epsilon1 = 3e-14;
        NumericsTest.assertEBEVectorsEqualAbsolute(expectedX, actualX1, epsilon1);
        NumericsTest.assertEBEVectorsEqualAbsolute(b, A.operate(actualX1), epsilon1);
        
        final Function<RealMatrix, DecompositionSolver> matrix2Decomp = M -> new LUDecomposition(M).getSolver();
        final RealVector actualX2 = Numerics.computeAInvB(A, b, matrix2Decomp); // solution to AX = B
        final double epsilon2 = 5e-16; // LUDecomposition does a better job with this problem
        NumericsTest.assertEBEVectorsEqualAbsolute(expectedX, actualX2, epsilon2);
        NumericsTest.assertEBEVectorsEqualAbsolute(b, A.operate(actualX2), epsilon2);
    }
    
    // CHECKSTYLE.OFF: LocalFinalVariableName
    
    /** Test {@link Numerics#doubleArrayToString(double[], int)} */
    @Test
    public void testDoubleArrayToString() {
        final String expectedString = "[        1.0000000000,         2.0000000000,         3.0000000000,         "
                                             + "4.0000000000,         5.0000000000,         6.0000000000,         "
                                             + "7.0000000000,         8.0000000000,         9.0000000000,  "
                                             + "10000000000000000000000000000.0000000000]";
        final double[] array = new double[] {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10000000000000000000000000000.00000000000000000000000000001 };
        final String actual = Numerics.doubleArrayToString(array, 10);
        assertEquals(expectedString, actual);
    }
    
    /** Test {@link Numerics#square(double)} */
    @Test
    void testSquare() {
        assertEquals(0.0,   Numerics.square(0.0));
        assertEquals(1.0,   Numerics.square(1.0));
        assertEquals(1.0,   Numerics.square(-1.0));
        assertEquals(400.0, Numerics.square(-20.0));
    }
    
    /** Test {@link Numerics#cube(double)} */
    @Test
    void testCube() {
        assertEquals(0.0,      Numerics.cube(0.0));
        assertEquals(1.0,      Numerics.cube(1.0));
        assertEquals(-1.0,     Numerics.cube(-1.0));
        assertEquals(-8_000.0, Numerics.cube(-20.0));
    }
    
    /** Test {@link Numerics#min(double, double)} */
    @Test
    void testMin2() {
        assertEquals(123.0, Numerics.min(123.0, 456.0));
        assertEquals(123.0, Numerics.min(456.0, 123.0));
    }
    
    /** Test {@link Numerics#min(double, double, double)} */
    @Test
    void testMin3() {
        assertEquals(123.0, Numerics.min(123.0, 456.0, 789.0));
        assertEquals(123.0, Numerics.min(789.0, 123.0, 456.0));
        assertEquals(123.0, Numerics.min(456.0, 789.0, 123.0));
    }
    
    /** Test {@link Numerics#min(double...)} */
    @Test
    void testMinN() {
        assertEquals(Double.POSITIVE_INFINITY, Numerics.min());
        
        assertEquals(123.0, Numerics.min(123.0));
        assertEquals(123.0, Numerics.min(123.0, 456.0));
        assertEquals(123.0, Numerics.min(123.0, 456.0, 789.0));
        assertEquals(123.0, Numerics.min(123.0, 456.0, 789.0, 999.0));
        
        assertEquals(123.0, Numerics.min(123.0, 456.0, 789.0, 999.0));
        assertEquals(123.0, Numerics.min(789.0, 123.0, 456.0, 999.0));
        assertEquals(123.0, Numerics.min(456.0, 789.0, 123.0, 999.0));
        assertEquals(123.0, Numerics.min(999.0, 456.0, 789.0, 123.0));
    }
    
    /** Test {@link Numerics#max(double, double)} */
    @Test
    void testMax2() {
        assertEquals(456.0, Numerics.max(123.0, 456.0));
        assertEquals(456.0, Numerics.max(456.0, 123.0));
    }
    
    /** Test {@link Numerics#max(double, double, double)} */
    @Test
    void testMax3() {
        assertEquals(789.0, Numerics.max(123.0, 456.0, 789.0));
        assertEquals(789.0, Numerics.max(789.0, 123.0, 456.0));
        assertEquals(789.0, Numerics.max(456.0, 789.0, 123.0));
    }
    
    /** Test {@link Numerics#max(double...)} */
    @Test
    void testMaxN() {
        assertEquals(Double.NEGATIVE_INFINITY, Numerics.max());
        
        assertEquals(789.0, Numerics.max(789.0));
        assertEquals(789.0, Numerics.max(456.0, 789.0));
        assertEquals(789.0, Numerics.max(123.0, 456.0, 789.0));
        assertEquals(789.0, Numerics.max(111.0, 123.0, 456.0, 789.0));
        
        assertEquals(789.0, Numerics.max(111.0, 123.0, 456.0, 789.0));
        assertEquals(789.0, Numerics.max(111.0, 789.0, 123.0, 456.0));
        assertEquals(789.0, Numerics.max(111.0, 456.0, 789.0, 123.0));
        assertEquals(789.0, Numerics.max(456.0, 789.0, 123.0, 111.0));
    }
    
    /** Test {@link Numerics#highPrecisionNumberFormat()} */
    @Test
    void testHighPrecisionNumberFormat() {
        final NumberFormat fmt = Numerics.highPrecisionNumberFormat();
        
        final double numToFormat1 = 0.0;
        final double numToFormat2 = 1e-20;
        final double numToFormat3 = 1e-21;
        final double numToFormat4 = 1e+20;
        final double numToFormat5 = 123456768912345676891234567689.122333444455555666666777777788888888999999999;
        // note that numToFormat5 is too large for the machine to represent all of the non-fractional digits
        
        assertEquals("0", fmt.format(numToFormat1));
        assertEquals("0.00000000000000000001", fmt.format(numToFormat2));
        assertEquals("0", fmt.format(numToFormat3));
        assertEquals("100000000000000000000", fmt.format(numToFormat4));
        assertEquals("123456768912345680000000000000", fmt.format(numToFormat5)); // loss of precision
    }
    
    /** Test {@link Numerics#computeRelativeDifference(double, double)} */
    @Test
    void testComputeRelativeDifference() {
        assertEquals(0.0, Numerics.computeRelativeDifference(1.0, 1.0));
        
        final double relDiff1 = Numerics.computeRelativeDifference(1.0, 1.1);
        final double relDiff1Flip = Numerics.computeRelativeDifference(1.1, 1.0);
        final double expectedRelDiff1 = (1.1 - 1.0) / 1.1; // = 0.090909...
        assertEquals(expectedRelDiff1, relDiff1);
        assertEquals(expectedRelDiff1, relDiff1Flip);
        
        final double relDiff2 = Numerics.computeRelativeDifference(12.0, 32.0);
        assertEquals(0.625, relDiff2); // = 20.0 / 32.0
    }
    
    /** Test {@link Numerics#requireNonNullAndDimension(RealVector, int)} */
    @Test
    void testRequireNonNullAndDimension() {
        final Exception e1 = assertThrows(ValidationException.class,
                                          () -> Numerics.requireNonNullAndDimension(new NVector(1D, 2D),
                                                                                                 -1));
        final Exception e2 = assertThrows(NullPointerException.class,
                                          () -> Numerics.requireNonNullAndDimension(null, 1));
        final Exception e3 = assertThrows(ValidationException.class,
                                        () -> Numerics.requireNonNullAndDimension(new NVector(1D, 2D), 3));
        assertDoesNotThrow(() -> Numerics.requireNonNullAndDimension(new NVector(1D, 2D, 3D), 3));
        
        assertEquals("Parameter dim with value -1 is not greater than or equal to 1", e1.getMessage());
        assertEquals("vector may not be null", e2.getMessage());
        assertEquals("Parameter v dimension with value 2 is not equal to 3", e3.getMessage());
    }
    
    /** Test {@link Numerics#signedAngleTo3D(Vector3D, Vector3D, Vector3D)} */
    @Test
    void testSignedAngleTo3D() {
        final Vector3D v = new Vector3D(1, 1, 1);
        final double ang1 = Numerics.signedAngleTo3D(v, Vector3D.PLUS_I, Vector3D.PLUS_K);
        assertEquals(-FastMath.acos(1 / FastMath.sqrt(3)), ang1, 1e-12);

        final double ang2 = Numerics.signedAngleTo3D(v, Vector3D.PLUS_I, Vector3D.MINUS_K);
        assertEquals(FastMath.acos(1 / FastMath.sqrt(3)), ang2, 1e-12);
    }
    
    /**
     * Test {@link Numerics#equalsRelMachEps(double, double)}
     */
    @Test
    void testEqualsRelMachEpsReal() {
        final double one = 1.0;
        final double onePMachEps = 1.0 + MACHINE_EPSILON;
        assertExactlyRelMachEpsApart(one, onePMachEps);
        
        final double oneHundred = 100.0;
        final double oneHundredPMachEps = 100.0 + 100.0 * MACHINE_EPSILON;
        assertExactlyRelMachEpsApart(oneHundred, oneHundredPMachEps);
        
        assertFalse(Numerics.equalsRelMachEps(oneHundred,
                                       oneHundredPMachEps + 100.0 * MACHINE_EPSILON)); // two relative machine epsilons
        
        assertTrue(Numerics.equalsRelMachEps(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY));
        assertFalse(Numerics.equalsRelMachEps(Double.POSITIVE_INFINITY, 1.0));
        assertTrue(Numerics.equalsRelMachEps(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY));
        assertFalse(Numerics.equalsRelMachEps(Double.NEGATIVE_INFINITY, 1.0));
        
        assertTrue(Numerics.equalsRelMachEps(Double.NaN, Double.NaN));
        assertFalse(Numerics.equalsRelMachEps(Double.NaN, 1.0));
        
        assertTrue(Numerics.equalsRelMachEps(Double.MAX_VALUE,
                                                          Double.MAX_VALUE - MACHINE_EPSILON * Double.MAX_VALUE));
        assertTrue(Numerics.equalsRelMachEps(-Double.MAX_VALUE,
                                                          -Double.MAX_VALUE + MACHINE_EPSILON * Double.MAX_VALUE));
        
        assertFalse(Numerics.equalsRelMachEps(Double.MAX_VALUE,
                       Double.MAX_VALUE - 2 * MACHINE_EPSILON * Double.MAX_VALUE)); // two relative machine epsilons
        assertFalse(Numerics.equalsRelMachEps(-Double.MAX_VALUE,
                       -Double.MAX_VALUE + 2 * MACHINE_EPSILON * Double.MAX_VALUE)); // two relative machine epsilons
    }
    
    /** Assert that the provided numbers are not exactly equal but are equal when using
     * {@link Numerics#equalsRelMachEps(double, double)}.
     * 
     * @param x the first value
     * @param y the second value
     * 
     * @apiNote this does not use {@link #assertExactlyRelEpsApart(double, double, double)} because I want to use
     *          {@link Numerics#equalsRelMachEps(double, double)}. */
    private static void assertExactlyRelMachEpsApart(final double x, final double y) {
        assertTrue(Numerics.equalsRelMachEps(x, y),
                   "'x' was not equal to 'y' within machine epsilon relative tolerance");
        assertNotEquals(x, y, "'x' and 'y' were exactly equal");
    }
    
    /** Test {@link Numerics#equalsRelEps(double, double, double)} using {@code epsilon == 0} */
    @Test
    void testEqualsRelEpsZeroTolerance() {
        assertTrue(Numerics.equalsRelEps(0.0, Double.MIN_VALUE, Double.MIN_VALUE));
        assertFalse(Numerics.equalsRelEps(0.0, Double.MIN_VALUE, 0.0));
    }
    
    /** Test {@link Numerics#equalsRelEps(double, double, double)} using
     * {@code epsilon == MACHINE_EPSILON * 100} */
    @Test
    void testEqualsRelEpsReal() {
        final double eps = MACHINE_EPSILON * 100;
        
        final double one = 1.0;
        final double onePMachEps = 1.0 + eps;
        assertExactlyRelEpsApart(one, onePMachEps, eps);
        
        final double oneHundred = 100.0;
        final double oneHundredPMachEps = 100.0 + 100.0 * eps;
        assertExactlyRelEpsApart(oneHundred, oneHundredPMachEps, eps);
        
        assertFalse(Numerics.equalsRelEps(oneHundred,
                                                       oneHundredPMachEps + 100.0 * eps,
                                                       eps)); // two relative machine epsilons
        
        assertTrue(Numerics.equalsRelEps(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, eps));
        assertFalse(Numerics.equalsRelEps(Double.POSITIVE_INFINITY, 1.0, eps));
        assertTrue(Numerics.equalsRelEps(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, eps));
        assertFalse(Numerics.equalsRelEps(Double.NEGATIVE_INFINITY, 1.0, eps));
        
        assertTrue(Numerics.equalsRelEps(Double.NaN, Double.NaN, eps));
        assertFalse(Numerics.equalsRelEps(Double.NaN, 1.0, eps));
        
        assertTrue(Numerics.equalsRelEps(Double.MAX_VALUE,
                                                      Double.MAX_VALUE - eps * Double.MAX_VALUE,
                                                      eps));
        assertTrue(Numerics.equalsRelEps(-Double.MAX_VALUE,
                                                      -Double.MAX_VALUE + eps * Double.MAX_VALUE,
                                                      eps));
        
        assertFalse(Numerics.equalsRelEps(Double.MAX_VALUE,
                                                       Double.MAX_VALUE - 2 * eps * Double.MAX_VALUE,
                                                       eps));
        assertFalse(Numerics.equalsRelEps(-Double.MAX_VALUE,
                                                       -Double.MAX_VALUE + 2 * eps * Double.MAX_VALUE,
                                                       eps));
    }
    
    /** Assert that the provided numbers are not exactly equal but are equal when using
     * {@link Numerics#equalsRelEps(double, double, double)} with {@code eps}.
     * 
     * @param x the first value
     * @param y the second value
     * @param eps the expected difference */
    private static void assertExactlyRelEpsApart(final double x, final double y, final double eps) {
        assertTrue(Numerics.equalsRelEps(x, y, eps),
                   "'x' was not equal to 'y' within epsilon relative tolerance");
        assertNotEquals(x, y, "'x' and 'y' were exactly equal");
    }
    
    /** Test {@link Numerics#equalsRelEps(Complex, Complex, Complex)} using
     * {@code reRelEps == MACHINE_EPSILON * 10} and {@code imRelEps == MACHINE_EPSILON * 100}<br>
     * <br>
     * 
     * This method is really just a pass-through to two calls to
     * {@link Numerics#equalsRelEps(double, double, double)}, so I'm not covering all of the special cases
     * like in the {@code double} variants of this method. */
    @Test
    void testEqualsRelEpsComplex() {
        final double reEps = MACHINE_EPSILON * 10;
        final double imEps = MACHINE_EPSILON * 100;
        final Complex eps = new Complex(reEps, imEps);
        
        final Complex z1                = new Complex(1,         1);
        final Complex z1PlusReMachEps   = new Complex(1 + reEps, 1);
        final Complex z1PlusImMachEps   = new Complex(1,         1 + imEps);
        final Complex z1PlusBothMachEps = new Complex(1 + reEps, 1 + imEps);
        
        assertExactlyRelEpsApartComplex(z1, z1PlusReMachEps,   eps);
        assertExactlyRelEpsApartComplex(z1, z1PlusImMachEps,   eps);
        assertExactlyRelEpsApartComplex(z1, z1PlusBothMachEps, eps);
        
        final Complex z2                = new Complex(100,               100);
        final Complex z2PlusReMachEps   = new Complex(100 + 100 * reEps, 100);
        final Complex z2PlusImMachEps   = new Complex(100,               100 + 100 * imEps);
        final Complex z2PlusBothMachEps = new Complex(100 + 100 * reEps, 100 + 100 * imEps);
        
        assertExactlyRelEpsApartComplex(z2, z2PlusReMachEps,   eps);
        assertExactlyRelEpsApartComplex(z2, z2PlusImMachEps,   eps);
        assertExactlyRelEpsApartComplex(z2, z2PlusBothMachEps, eps);
        
        assertFalse(Numerics.equalsRelEps(z2,
                                                       new Complex(100, 100 + 200 * imEps),
                                                       eps));
        assertFalse(Numerics.equalsRelEps(z2,
                                                       new Complex(100 + 200 * reEps, 100),
                                                       eps));
    }
    
    /** Assert that the provided numbers are not exactly equal but are equal when using
     * {@link Numerics#equalsRelEps(Complex, Complex, Complex)} with {@code eps}.
     * 
     * @param x the first value
     * @param y the second value
     * @param eps the expected difference */
    private static void assertExactlyRelEpsApartComplex(final Complex x, final Complex y, final Complex eps) {
        assertTrue(Numerics.equalsRelEps(x, y, eps),
                   "'x' was not equal to 'y' within epsilon relative tolerance");
        assertNotEquals(x, y, "'x' and 'y' were exactly equal");
    }
    
    /** Test {@link Numerics#equalsRelMachEps(Complex, Complex)}<br>
     * <br>
     * 
     * This method is really just a pass-through to two calls to
     * {@link Numerics#equalsRelMachEps(double, double)}, so I'm not covering all of the special cases like
     * in the {@code double} variants of this method. */
    @Test
    void testEqualsRelMachEpsComplex() {
        final Complex z1                = new Complex(1, 1);
        final Complex z1PlusReMachEps   = new Complex(1 + MACHINE_EPSILON, 1);
        final Complex z1PlusImMachEps   = new Complex(1, 1 + MACHINE_EPSILON);
        final Complex z1PlusBothMachEps = new Complex(1 + MACHINE_EPSILON, 1 + MACHINE_EPSILON);
        
        assertExactlyRelMachEpsApartComplex(z1, z1PlusReMachEps);
        assertExactlyRelMachEpsApartComplex(z1, z1PlusImMachEps);
        assertExactlyRelMachEpsApartComplex(z1, z1PlusBothMachEps);
        
        final Complex z2                = new Complex(100, 100);
        final Complex z2PlusReMachEps   = new Complex(100 + 100 * MACHINE_EPSILON, 100);
        final Complex z2PlusImMachEps   = new Complex(100, 100 + 100 * MACHINE_EPSILON);
        final Complex z2PlusBothMachEps = new Complex(100 + 100 * MACHINE_EPSILON, 100 + 100 * MACHINE_EPSILON);
        
        assertExactlyRelMachEpsApartComplex(z2, z2PlusReMachEps);
        assertExactlyRelMachEpsApartComplex(z2, z2PlusImMachEps);
        assertExactlyRelMachEpsApartComplex(z2, z2PlusBothMachEps);
        
        assertFalse(Numerics.equalsRelMachEps(z2, new Complex(100, 100 + 200 * MACHINE_EPSILON)));
        assertFalse(Numerics.equalsRelMachEps(z2, new Complex(100 + 200 * MACHINE_EPSILON, 100)));
    }
    
    /** Assert that the provided numbers are not exactly equal but are equal when using
     * {@link Numerics#equalsRelEps(Complex, Complex, Complex)} with {@code eps}.
     * 
     * @param x the first value
     * @param y the second value */
    private static void assertExactlyRelMachEpsApartComplex(final Complex x, final Complex y) {
        assertTrue(Numerics.equalsRelMachEps(x, y),
                   "'x' was not equal to 'y' within machine epsilon relative tolerance");
        assertNotEquals(x, y, "'x' and 'y' were exactly equal");
    }
    
    /**
     * Test {@link Numerics#getResiduals(UnaryOperator, double[], double[])}
     */
    @Test
    public void testGetResiduals() {
        MultivariateVectorValuedFunction f = (double[] input) -> {
            return new double[] {
                    input[0],
                    input[1],
                    input[2],
                    input[3],
                    input[4],
                    input[5]
            };
        };
        
        double[] xn = new double[] {
            1,
            1,
            1,
            1,
            1,
            1
        };
        
        double[] targetValues = new double[] {
            -1,
            -1,
            -1,
            -1,
            -1,
            -1
        };
        
        double[] actual = Numerics.getResiduals(f, xn, targetValues);
        double[] expected = new double[] {
            -2,
            -2,
            -2,
            -2,
            -2,
            -2
        };
        assertTrue(Arrays.equals(expected, actual));
    }

    /**
     * Test {@link Numerics#sumSq(double...)}
     */
    @Test
    public void testSumSq() {
        double actual = Numerics.sumSq(1, 2, 3, 4, 5, 6, 7, 8, 9);
        double expected = 1 + 4 + 9 + 16 + 25 + 36 + 49 + 64 + 81;
        assertEquals(expected, actual);
    }
    
    /**
     * Test {@link Numerics#addScaledArray(double[], double[], double)}
     */
    @Test
    public void testAddScaledArray() {
        double[] toAddTo = new double[] {
            1,
            2,
            3
        };
        
        double[] toScaleAndAdd = new double[] {
            1,
            1,
            1
        };
        
        double scale = 10;
        double[] actual = Numerics.addScaledArray(toAddTo, toScaleAndAdd, scale);
        double[] expected = new double[] {
          11,
          12,
          13
        };
        assertTrue(Arrays.equals(expected, actual));
    }
    
    /**
     * Test {@link Numerics#getFilledArray(int, double)}
     */
    @Test
    public void testGetFilledArray() {
        double[] expected = new double[] { 123D, 123D, 123D, 123D, 123D, 123D, 123D, 123D, 123D, 123D };
        double[] actual = Numerics.getFilledArray(10, 123D);
        Assertions.assertArrayEquals(expected, actual);
    }
    
    /** Test
     * {@link Numerics#evaluateJacobian(MultivariateVectorValuedFunction, double[], double[],
     *                                               int, int, Parallelism)}
     * with a constant function */
    @Test
    public void testgetJacobianConstantFunction() {
        final MultivariateVectorValuedFunction f = (double[] input) -> {
            return new double[] {
                1,
                1,
                1,
                1,
                1
            };
        };
        final VectorField vf = new VectorField(f, 5, 5);
        
        double[] dxs = new double[] {
            1e-6,
            1e-6,
            1e-6,
            1e-6,
            1e-6
        };
        
        for (int i = 0; i < 10; i++) {
            double[] input = new double[] {
                i,
                i,
                i,
                i,
                i
            };
            
            final Matrix jacAtI = Numerics.evaluateJacobian(vf, input, dxs, 5, 5, Parallelism.SEQUENTIAL);
            for (int j = 0; j < jacAtI.getRowDimension(); j++) {
                for (int k = 0; k < jacAtI.getColumnDimension(); k++) {
                    assertEquals(0.0, jacAtI.getEntry(j, k));
                }
            }
        }
    }
    
    /** Test
     * {@link Numerics#evaluateJacobian(MultivariateVectorValuedFunction, double[], double[],
     *                                               int, int, Parallelism)} */
    @Test
    void testEvaluateJacobian() {
        final MultivariateVectorValuedFunction f = input -> new double[] { input[0] * input[0],
                                                                           input[1] * input[1],
                                                                           input[2] * input[2] };
        final VectorField vf = new VectorField(f, 3, 3);
        
        final double[] dxs = new double[] { 1e-6, 1e-6, 1e-6 };
        final Matrix actual1 = Numerics.evaluateJacobianSequentially(vf,
                                                                                  new double[] { 3, 4, 5 },
                                                                                  dxs, 3, 3);
        final Matrix expected = new Matrix(new double[][] { { 6, 0, 0 },
                                                            { 0, 8, 0 },
                                                            { 0, 0, 10 } });
        
        MatrixTest.assertMatrixEquals(expected, actual1, 1e-8);
        
        // show that running in parallel makes no difference
        final Matrix actual2 = Numerics.evaluateJacobian(vf,
                                                                      new double[] { 3, 4, 5 },
                                                                      dxs, 3, 3,
                                                                      Parallelism.PARALLEL);
        MatrixTest.assertMatrixEquals(expected, actual2, 1e-8);
    }
    
    /** Test
     * {@link Numerics#getJacobianFunction(MultivariateVectorValuedFunction, Optional, 
     *                                                  int, int, Parallelism)}
     * with a non-null DXs array */
    @Test
    void testGetJacobianFunctionWithDXsArray() {
        final VectorField f = new VectorField(input -> new double[] { input[0] * input[0],
                                                                      input[1] * input[1],
                                                                      input[2] * input[2] },
                                              3, 3);
        
        final double[] dxs = new double[] { 1e-6, 1e-6, 1e-6 };
        final Function<double[], Matrix> jac =
                Numerics.getJacobianFunction(f, Optional.of(dxs), 3, 3, Parallelism.PARALLEL);
        
        final Matrix actual = jac.apply(new double[] { 3, 4, 5 });
        final Matrix expected = new Matrix(new double[][] { { 6, 0, 0 },
                                                            { 0, 8, 0 },
                                                            { 0, 0, 10 } });
        
        MatrixTest.assertMatrixEquals(expected, actual, 1e-8);
    }
    
    /** Test
     * {@link Numerics#getJacobianFunction(MultivariateVectorValuedFunction, Optional, 
     *                                                  int, int, Parallelism)}
     * with a null DXs array */
    @Test
    void testGetJacobianFunctionWithoutDXsArray() {
        final VectorField f = new VectorField(input -> new double[] { input[0] * input[0],
                                                                      input[1] * input[1],
                                                                      input[2] * input[2] },
                                              3, 3);
        
        final Function<double[], Matrix> jac =
                Numerics.getJacobianFunction(f, Optional.empty(), 3, 3, null); // null to run sequentially
        final Matrix actual = jac.apply(new double[] { 3, 4, 5 });
        final Matrix expected = new Matrix(new double[][] { { 6, 0, 0 },
                                                            { 0, 8, 0 },
                                                            { 0, 0, 10 } });
        
        MatrixTest.assertMatrixEquals(expected, actual, 1e-10); // better error using auto-scaling DXs
    }
    
    /** Test {@link Numerics#computeOptimalCentralDifferencesDX(double)} */
    @Test
    void testOptimalDX() {
        final double dx1 = Numerics.computeOptimalCentralDifferencesDX(10.0);
        assertEquals(10 * Numerics.CUBE_ROOT_MACHINE_EPSILON, dx1);
        
        final double dx2 = Numerics.computeOptimalCentralDifferencesDX(0.9);
        assertEquals(Numerics.CUBE_ROOT_MACHINE_EPSILON, dx2);
    }
    
    /** Test {@link Numerics#vectorElementwiseEquals(RealVector, RealVector, double)} */
    @Test
    void testVectorEquals() {
        final NVector nan1 = new NVector(Double.NaN, Double.NaN, Double.NaN);
        final NVector nan2 = new NVector(Double.NaN, Double.NaN, Double.NaN);
        
        assertTrue(Numerics.vectorElementwiseEquals(nan1, nan2, 0.0));
        
        final NVector v  = new NVector(10.0, 20.0, 30.0);
        final NVector v1 = new NVector(10.1, 20.0, 30.0);
        final NVector v2 = new NVector(10.0, 20.1, 30.0);
        final NVector v3 = new NVector(10.0, 20.0, 30.1);
        final NVector v4 = new NVector(10.1, 20.1, 30.1);
        
        final double passTol = 0.1 + 2e-15;
        final double failTol = 0.09;
        
        assertTrue(Numerics.vectorElementwiseEquals(v, v1, passTol),
                   getRealVectorAbsErrorStringSupplier(v, v1, passTol));
        assertFalse(Numerics.vectorElementwiseEquals(v, v1, failTol),
                    getRealVectorAbsErrorStringSupplier(v, v1, failTol));
        
        assertTrue(Numerics.vectorElementwiseEquals(v, v2, passTol),
                   getRealVectorAbsErrorStringSupplier(v, v2, passTol));
        assertFalse(Numerics.vectorElementwiseEquals(v, v2, failTol),
                    getRealVectorAbsErrorStringSupplier(v, v2, failTol));
        
        assertTrue(Numerics.vectorElementwiseEquals(v, v3, passTol),
                   getRealVectorAbsErrorStringSupplier(v, v3, passTol));
        assertFalse(Numerics.vectorElementwiseEquals(v, v3, failTol),
                    getRealVectorAbsErrorStringSupplier(v, v3, failTol));
        
        assertTrue(Numerics.vectorElementwiseEquals(v, v4, passTol),
                   getRealVectorAbsErrorStringSupplier(v, v4, passTol));
        assertFalse(Numerics.vectorElementwiseEquals(v, v4, failTol),
                    getRealVectorAbsErrorStringSupplier(v, v4, failTol));
    }
    
    /** Assert that the provided {@link RealVector}s are equal, element-by-element, within the required tolerance
     * 
     * @param v1 the first vector
     * @param v2 the second vector
     * @param absoluteTolerance the absolute tolerance */
    public static void assertEBEVectorsEqualAbsolute(final RealVector v1,
                                                     final RealVector v2,
                                                     final double absoluteTolerance) {
        assertTrue(Numerics.vectorElementwiseEquals(v1, v2, absoluteTolerance),
                   getRealVectorAbsErrorStringSupplier(v1, v2, absoluteTolerance));
    }
    
    /** @param v1 the first vector
     * @param v2 the second vector
     * @param tolerance the tolerance
     * @return a {@link String} {@link Supplier} that can provide an error string in the event that a vector equality
     *         comparison fails */
    private static Supplier<String> getRealVectorAbsErrorStringSupplier(final RealVector v1,
                                                                        final RealVector v2,
                                                                        final double tolerance) {
        return () -> new StringBuilder().append("Max vector entry difference was ")
                                        .append(v2.getLInfDistance(v1))
                                        .append(" which is greater than the tolerance: ")
                                        .append(tolerance)
                                        .toString();
    }
    /* TODO Ryan Moser, 2023-07-07: write a new class NumericalTestUtils (or something like it) that houses these (and
     * other) static utility methods */
    
    /** Assert that the given {@link Complex} {@link FieldVector}s are element-by-element equal, within the given
     * relative tolerance
     * 
     * @param v1 the first {@link Complex} {@link FieldVector}
     * @param v2 the second {@link Complex} {@link FieldVector}
     * @param relativeTolerance the relative tolerance */
    public static void assertEBEVectorsEqualRelative(final FieldVector<Complex> v1,
                                                     final FieldVector<Complex> v2,
                                                     final double relativeTolerance) {
        final int dim = v1.getDimension();
        assertEquals(dim, v2.getDimension(), "Vectors had difference dimension");
        
        for (int i = 0; i < dim; i++) {
            final Complex v1Entry = v1.getEntry(i);
            final Complex v2Entry = v2.getEntry(i);
            if (!Complex.equalsWithRelativeTolerance(v1Entry, v2Entry, relativeTolerance)) {
                assertEquals(ComplexVector.of(v1).toColumnString(),
                             ComplexVector.of(v2).toColumnString());
                
                // the fail call is a fail-safe, though it should be impossible
                fail("The vectors weren't equal, but the difference was too "
                        + "small to capture in the column-string representation");
            }
        }
    }
    
    /** Assert that the given {@link Complex} {@link FieldVector}s are element-by-element equal, within the given
     * absolute tolerance
     * 
     * @param v1 the first {@link Complex} {@link FieldVector}
     * @param v2 the second {@link Complex} {@link FieldVector}
     * @param absoluteTolerance the absolute tolerance */
    public static void assertEBEVectorsEqualAbsolute(final FieldVector<Complex> v1,
                                                     final FieldVector<Complex> v2,
                                                     final double absoluteTolerance) {
        final int dim = v1.getDimension();
        assertEquals(dim, v2.getDimension(), "Vectors had difference dimension");
        
        for (int i = 0; i < dim; i++) {
            final Complex v1Entry = v1.getEntry(i);
            final Complex v2Entry = v2.getEntry(i);
            if (!Complex.equals(v1Entry, v2Entry, absoluteTolerance)) {
                assertEquals(ComplexVector.of(v1).toColumnString(),
                             ComplexVector.of(v2).toColumnString());
                
                // the fail call is a fail-safe, though it should be impossible
                fail("The vectors weren't equal, but the difference was too "
                        + "small to capture in the column-string representation");
            }
        }
    }
    
    /** Test {@link Numerics#evaluateGradientSequentially(ToDoubleFunction, double[], double[])} */
    @Test
    void testEvaluateGradient() {
        final ToDoubleFunction<double[]> f = MultivariateScalarValuedFunctionTest.XYZ;
        final double[] gradient1 = Numerics.evaluateGradientSequentially(f,
                                                                         new double[] { 10, 20, 30 },
                                                                         new double[] { 1e-6, 1e-6, 1e-6 });
        final NVector expected = new NVector(600.0, 300.0, 200.0);
        final NVector actual1  = new NVector(gradient1);
        assertEBEVectorsEqualAbsolute(expected, actual1, 6.06e-7);
        
        final double[] gradient2 = Numerics.evaluateGradient(f,
                                                                         new double[] { 10, 20, 30 },
                                                                         new double[] { 1e-6, 1e-6, 1e-6 },
                                                                         Parallelism.PARALLEL);
        final NVector actual2  = new NVector(gradient2);
        assertEBEVectorsEqualAbsolute(expected, actual2, 6.06e-7);
    }
    
    /** Test {@link Numerics#evaluateDerivative(DoubleUnaryOperator, double, double)}.<br>
     * Given f(x) = x^x, f'(x) = x^x * (1 + ln(x)).
     * 
     * <pre>
     * Proof:
     * Let y = x^x
     *     -> ln(y) = xln(x)
     *     -> y'/y = x * (1/x) + 1 * ln(x) = 1 + ln(x)
     *     -> y' = y * (1 + ln(x)) = x^x * (1 + ln(x))
     * </pre>
     */
    @Test
    void testEvaluateDerivative() {
        final DoubleUnaryOperator f = UnivariateScalarValuedFunctionTest.X_TO_THE_X;
        final double derivative = Numerics.evaluateDerivative(f, 3, 1e-6);
        final double expected = f.applyAsDouble(3) * (1.0 + FastMath.log(3));
        assertEquals(expected, derivative, 1e-7);
    }
    
    /**
     * Test {@link Numerics#generateVector(Function, Iterable)}
     */
    @Test
    void testGenerateVector() {
        final double[] expected = new double[] { 0D, 0D, 0D, 0D, 0D,
                                                 1D, 1D, 1D, 1D, 1D,
                                                 2D, 2D, 2D, 2D, 2D,
                                                 3D, 3D, 3D, 3D, 3D,
                                                 4D, 4D, 4D, 4D, 4D };
        final double[] actual = Numerics.generateVector(i -> new double[] { i, i, i, i, i },
                                                                     List.of(0, 1, 2, 3, 4));
        assertArrayEquals(expected, actual);
    }
    
    /**
     * Test {@link Numerics#cartesianProduct(List)}
     */
    @Test
    void testCartesianProduct() {
        final List<List<Integer>> actual = Numerics.cartesianProduct(List.of(List.of(1, 2, 3, 4, 5),
                                                                                          List.of(6, 7, 8, 9, 0),
                                                                                          List.of(11, 12, 13, 14, 15)));
        final List<List<Integer>> expected = List.of(List.of(1, 6, 11),
                                                     List.of(1, 6, 12),
                                                     List.of(1, 6, 13),
                                                     List.of(1, 6, 14),
                                                     List.of(1, 6, 15),
                                                     List.of(1, 7, 11),
                                                     List.of(1, 7, 12),
                                                     List.of(1, 7, 13),
                                                     List.of(1, 7, 14),
                                                     List.of(1, 7, 15),
                                                     List.of(1, 8, 11),
                                                     List.of(1, 8, 12),
                                                     List.of(1, 8, 13),
                                                     List.of(1, 8, 14),
                                                     List.of(1, 8, 15),
                                                     List.of(1, 9, 11),
                                                     List.of(1, 9, 12),
                                                     List.of(1, 9, 13),
                                                     List.of(1, 9, 14),
                                                     List.of(1, 9, 15),
                                                     List.of(1, 0, 11),
                                                     List.of(1, 0, 12),
                                                     List.of(1, 0, 13),
                                                     List.of(1, 0, 14),
                                                     List.of(1, 0, 15),
                                                     List.of(2, 6, 11),
                                                     List.of(2, 6, 12),
                                                     List.of(2, 6, 13),
                                                     List.of(2, 6, 14),
                                                     List.of(2, 6, 15),
                                                     List.of(2, 7, 11),
                                                     List.of(2, 7, 12),
                                                     List.of(2, 7, 13),
                                                     List.of(2, 7, 14),
                                                     List.of(2, 7, 15),
                                                     List.of(2, 8, 11),
                                                     List.of(2, 8, 12),
                                                     List.of(2, 8, 13),
                                                     List.of(2, 8, 14),
                                                     List.of(2, 8, 15),
                                                     List.of(2, 9, 11),
                                                     List.of(2, 9, 12),
                                                     List.of(2, 9, 13),
                                                     List.of(2, 9, 14),
                                                     List.of(2, 9, 15),
                                                     List.of(2, 0, 11),
                                                     List.of(2, 0, 12),
                                                     List.of(2, 0, 13),
                                                     List.of(2, 0, 14),
                                                     List.of(2, 0, 15),
                                                     List.of(3, 6, 11),
                                                     List.of(3, 6, 12),
                                                     List.of(3, 6, 13),
                                                     List.of(3, 6, 14),
                                                     List.of(3, 6, 15),
                                                     List.of(3, 7, 11),
                                                     List.of(3, 7, 12),
                                                     List.of(3, 7, 13),
                                                     List.of(3, 7, 14),
                                                     List.of(3, 7, 15),
                                                     List.of(3, 8, 11),
                                                     List.of(3, 8, 12),
                                                     List.of(3, 8, 13),
                                                     List.of(3, 8, 14),
                                                     List.of(3, 8, 15),
                                                     List.of(3, 9, 11),
                                                     List.of(3, 9, 12),
                                                     List.of(3, 9, 13),
                                                     List.of(3, 9, 14),
                                                     List.of(3, 9, 15),
                                                     List.of(3, 0, 11),
                                                     List.of(3, 0, 12),
                                                     List.of(3, 0, 13),
                                                     List.of(3, 0, 14),
                                                     List.of(3, 0, 15),
                                                     List.of(4, 6, 11),
                                                     List.of(4, 6, 12),
                                                     List.of(4, 6, 13),
                                                     List.of(4, 6, 14),
                                                     List.of(4, 6, 15),
                                                     List.of(4, 7, 11),
                                                     List.of(4, 7, 12),
                                                     List.of(4, 7, 13),
                                                     List.of(4, 7, 14),
                                                     List.of(4, 7, 15),
                                                     List.of(4, 8, 11),
                                                     List.of(4, 8, 12),
                                                     List.of(4, 8, 13),
                                                     List.of(4, 8, 14),
                                                     List.of(4, 8, 15),
                                                     List.of(4, 9, 11),
                                                     List.of(4, 9, 12),
                                                     List.of(4, 9, 13),
                                                     List.of(4, 9, 14),
                                                     List.of(4, 9, 15),
                                                     List.of(4, 0, 11),
                                                     List.of(4, 0, 12),
                                                     List.of(4, 0, 13),
                                                     List.of(4, 0, 14),
                                                     List.of(4, 0, 15),
                                                     List.of(5, 6, 11),
                                                     List.of(5, 6, 12),
                                                     List.of(5, 6, 13),
                                                     List.of(5, 6, 14),
                                                     List.of(5, 6, 15),
                                                     List.of(5, 7, 11),
                                                     List.of(5, 7, 12),
                                                     List.of(5, 7, 13),
                                                     List.of(5, 7, 14),
                                                     List.of(5, 7, 15),
                                                     List.of(5, 8, 11),
                                                     List.of(5, 8, 12),
                                                     List.of(5, 8, 13),
                                                     List.of(5, 8, 14),
                                                     List.of(5, 8, 15),
                                                     List.of(5, 9, 11),
                                                     List.of(5, 9, 12),
                                                     List.of(5, 9, 13),
                                                     List.of(5, 9, 14),
                                                     List.of(5, 9, 15),
                                                     List.of(5, 0, 11),
                                                     List.of(5, 0, 12),
                                                     List.of(5, 0, 13),
                                                     List.of(5, 0, 14),
                                                     List.of(5, 0, 15));
        assertEquals(expected, actual);
    }
    
    /** Parse a MATLAB matrix CSV generated using {@code writematrix}
     * 
     * @param matrixFile the name of the file to read, with extension {@code .txt}, specified w.r.t.
     *        {@code src/test/resources/ptolemaeus.math.linear.schur}; e.g., {@code gallery5/A.txt}
     * @return the {@link ComplexMatrix} specified in the CSV */
    public static ComplexMatrix parseMatrixCSV(final String matrixFile) {
        final LinkedList<Complex[]> allRows = new LinkedList<>();
        
        final URL url = MATLABSchurIO.class.getResource(matrixFile);
        try (Scanner in = new Scanner(url.openStream(), StandardCharsets.ISO_8859_1)) {
            while (in.hasNextLine()) {
                final String rowString = in.nextLine();
                
                if (rowString.isEmpty()) { // empty new-line at the end of the file
                    continue;
                }
                
                final Complex[] row = parseRow(rowString);
                allRows.add(row);
            }
        }
        catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
        
        final Complex[][] data = allRows.toArray(Complex[][]::new);
        return new ComplexMatrix(data);
    }
    
    /** Parse a row of a MATLAB matrix CSV
     * 
     * @param row the {@link Complex} values in the row
     * @return the {@link Complex}{@code []} specified by the row */
    private static Complex[] parseRow(final String row) {
        final String[] rowElts = row.split(",");
        return Stream.of(rowElts).map(NumericsTest::parseEntry).toArray(Complex[]::new);
    }
    
    /** Parse a single {@link Complex} in MATLAB form {@code a+bi}, {@code a-bi}, or {@code a} where {@code a} can be
     * any real number and {@code b} is positive.<br>
     * <br>
     * 
     * The REGEX used matches one-or-more decimal values followed by either "+" or "-". We then insert ";" between the
     * two matching groups so we can split on ";"<br>
     * <br>
     * 
     * Examples for clarity:
     * <ul>
     * <li>{@code -1-3i} is first mapped to {@code -1-3}. The REGEX then matches {@code 1} followed by a {@code -} and
     * replaces those matches with {@code 1;-} so we can split on the {@code ;} so that we get two strings: {@code "-1"}
     * and {@code "-3"}</li>
     * <li>{@code -1.11111+3.33333i} is first mapped to {@code -1.11111;+3.33333}. The REGEX then matches {@code 11111}
     * followed by a {@code +} and replaces those matches with {@code 11111;+} so that we can split on the {@code ;} so
     * that we get two strings: {@code "-1.11111"} and {@code "-3.33333"}</li>
     * </ul>
     *  
     * @param entry the entry to parse
     * @return the parsed {@link Complex} */
    private static Complex parseEntry(final String entry) {
        final double a;
        final double b;
        
        if (entry.contains("i")) { /* the following regex matches one-or-more decimal values followed by either "+" or
                                    * "-". We then insert ";" between the two matching groups so we can split on ";" */
            final String   withoutI = entry.replace("i", "").replaceFirst("(\\d+)(\\+|-)", "$1;$2");
            final String[] split    = withoutI.split(";");
            
            a = Double.parseDouble(split[0]);
            b = Double.parseDouble(split[1]);
        }
        else { // real number a (b == 0) and, in this case, this is true for each entry in the matrix
            a = Double.parseDouble(entry);
            b = 0.0;
        }
        
        return Complex.valueOf(a, b);
    }
}