/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.real.Matrix;

/**
 * Test {@link WeightedLeastSquaresConstants}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class WeightedLeastSquaresConstantsTest {
    
    /**
     * Test all
     */
    @Test
    public void testAll() {
        double[] weights = new double[] {4, 5, 6};
        double epsilon = 1e-6;
        int maxIterations = 123;
        int maxUnimprovedIterations = 456;
        
        final Function<RealMatrix, DecompositionSolver> decSolverFact = RealQRDecomposition::of;
        
        WeightedLeastSquaresConstants constants = new WeightedLeastSquaresConstants(3,
                                                                                     weights,
                                                                                     epsilon,
                                                                                     maxIterations,
                                                                                     maxUnimprovedIterations,
                                                                                     decSolverFact);
        
        assertEquals(Matrix.diagonal(weights), constants.getWeights());
        assertEquals(epsilon * epsilon, constants.getEpsilonSqr());
        assertEquals(maxIterations, constants.getMaxIterations());
        assertEquals(maxUnimprovedIterations, constants.getMaxUnimprovedIters());
        assertSame(decSolverFact, constants.getDecompositionSolverFactory());
    }
}
