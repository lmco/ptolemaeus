/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import ptolemaeus.math.functions.MultivariateVectorValuedFunction;

/**
 * Test for {@link WeightedLeastSquaresSolution}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class WeightedLeastSquaresSolutionTest {
    
    /**
     * Test {@link WeightedLeastSquaresSolution}
     */
    @Test
    public void testAllSimple() {
        MultivariateVectorValuedFunction f = input -> {
            return input;
        };
        double[] point = new double[] {
                123
        };
        double[] target = new double[] {
                456
        };
        double[] expectedResid = new double[] {
                333
        };
        
        WeightedLeastSquaresSolution soln = new WeightedLeastSquaresSolution(f, point, target);
        soln.setStopType(StopType.EPSILON_CONVERGENCE);
        soln.setIterationsToStop(123456L);
        String expectedString = " Epsilon requirement met, 123456 iterations to converge, "
                + "point: [      123.0000000000], residuals: [      333.0000000000], sumSqRes: 110889.000000000000";
        
        assertTrue(Arrays.equals(point, soln.getPoint()));
        assertTrue(Arrays.equals(expectedResid, soln.getResiduals()));
        assertEquals(333 * 333, soln.getSumSquareResiduals());
        assertEquals(soln.getStopType(), StopType.EPSILON_CONVERGENCE);
        assertEquals(soln.getIterationsToStop(), 123456L);
        assertEquals(expectedString, soln.toString());
    }
    
    /**
     * Test {@link WeightedLeastSquaresSolution}
     */
    @Test
    public void testAllComplex() {
        MultivariateVectorValuedFunction f = input -> {
            return new double[] {
                input[0] * input[1],
                input[1] * input[2]
            };
        };
        double[] point = new double[] {
                10,
                20,
                30
        };
        double[] target = new double[] {
                300,
                700
        };
        double[] expectedResid = new double[] {
                100,
                100
        };
        
        WeightedLeastSquaresSolution soln = new WeightedLeastSquaresSolution(f, point, target);
        assertTrue(Arrays.equals(point, soln.getPoint()));
        assertTrue(Arrays.equals(expectedResid, soln.getResiduals()));
        assertEquals(100 * 100 + 100 * 100, soln.getSumSquareResiduals());
    }
    
    /**
     * Test {@link WeightedLeastSquaresSolution#compareTo(WeightedLeastSquaresSolution)},
     * {@link WeightedLeastSquaresSolution#hashCode()}, and {@link WeightedLeastSquaresSolution#equals(Object)}
     */
    @Test
    void testCompareToEqualsAndHash() {
        final MultivariateVectorValuedFunction f = input -> new double[] { input[0] * input[1], input[1] * input[2] };
        final double[] point1 = new double[] { 10, 20, 30 };
        final double[] point2 = new double[] { 20, 30, 40 };
        final double[] target = new double[] { 300, 700 };
        
        final WeightedLeastSquaresSolution soln = new WeightedLeastSquaresSolution(f, point1, target);
        final WeightedLeastSquaresSolution eq = new WeightedLeastSquaresSolution(f, point1, target);
        final WeightedLeastSquaresSolution neqAndWorse = new WeightedLeastSquaresSolution(f, point2, target);
        
        assertEquals(soln, eq);
        assertNotEquals(soln, neqAndWorse);
        
        assertTrue(soln.compareTo(eq) == 0);
        assertTrue(soln.compareTo(neqAndWorse) < 0);
        assertTrue(neqAndWorse.compareTo(soln) > 0);
        
        assertTrue(soln.compareTo(null) < 0);
        
        assertEquals(-1716256736, soln.hashCode());
        assertEquals(soln.hashCode(), eq.hashCode());
    }
}
