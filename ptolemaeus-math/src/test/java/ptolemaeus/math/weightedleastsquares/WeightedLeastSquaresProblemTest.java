/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.weightedleastsquares;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.linear.real.Matrix;

/**
 * Tests for {@link WeightedLeastSquaresProblem}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class WeightedLeastSquaresProblemTest {
    
    /**
     * Test all of {@link WeightedLeastSquaresProblem}
     */
    @Test
    public void testAll() {
        MultivariateVectorValuedFunction f = (double[] input) -> {
            return new double[] {
                    -1, -2, -3 };
        };
        double[] initialGuess = new double[] {1, 3, 5};
        double[] targetValues = new double[] {2, 4, 6};
        double[] dxs = new double[] {1, 2, 3};
        double[] weights = new double[] {4, 5, 6};
        double epsilon = 1e-6;
        int maxIterations = 123;
        int maxUnimprovedIterations = 456;
        
        WeightedLeastSquaresConstants constants = new WeightedLeastSquaresConstants(
                3,
                weights,
                epsilon,
                maxIterations,
                maxUnimprovedIterations,
                WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER);
        
        VectorField mvvf = new VectorField(
                f,
                initialGuess.length,
                targetValues.length);
        
        MultivariateMatrixValuedFunction jacobian = Numerics.getJacobianFunction(mvvf,
                                                                                              Optional.ofNullable(dxs),
                                                                                              initialGuess.length,
                                                                                              targetValues.length,
                                                                                              Parallelism.SEQUENTIAL);
        
        WeightedLeastSquaresProblem<WeightedLeastSquaresConstants> wlsProblem =
                new WeightedLeastSquaresProblem<>(mvvf, jacobian, initialGuess, targetValues, constants);
        
        assertSame(mvvf, wlsProblem.getF());
        assertSame(jacobian, wlsProblem.getJacobian());
        assertArrayEquals(initialGuess, wlsProblem.getInitialGuess());
        assertArrayEquals(targetValues, wlsProblem.getTargetValues());
        assertEquals(Matrix.diagonal(weights), wlsProblem.getConstants().getWeights());
        assertEquals(epsilon * epsilon, wlsProblem.getConstants().getEpsilonSqr());
        assertEquals(maxIterations, wlsProblem.getConstants().getMaxIterations());
        assertEquals(maxUnimprovedIterations, wlsProblem.getConstants().getMaxUnimprovedIters());
        
        final ValidationException ve1 =
                assertThrows(ValidationException.class,
                             () -> new WeightedLeastSquaresProblem<>(mvvf,
                                                                     jacobian,
                                                                     initialGuess,
                                                                     new double[] { 0 },
                                                                     constants));
        assertEquals("The length of the target values array (1) must be "
                         + "the same as the output dimension of the function (3)",
                     ve1.getMessage());
        
        WeightedLeastSquaresConstants badCosntants =
                new WeightedLeastSquaresConstants(1, new double[] { 0 },
                                                  epsilon, maxIterations, maxUnimprovedIterations,
                                                  WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER);
        final ValidationException     ve2          =
                assertThrows(ValidationException.class,
                             () -> new WeightedLeastSquaresProblem<>(mvvf,
                                                                     jacobian,
                                                                     initialGuess,
                                                                     targetValues,
                                                                     badCosntants));
        assertEquals("The length of the target values array (3) must be "
                         + "the same as the dimension of the weight matrix (1)",
                     ve2.getMessage());
    }
}
