/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.real;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;
import static ptolemaeus.commons.TestUtils.assertLessThanOrEqualTo;
import static ptolemaeus.commons.TestUtils.assertThrowsWithMessage;

import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.ComplexField;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.Array2DRowFieldMatrix;
import org.hipparchus.linear.Array2DRowRealMatrix;
import org.hipparchus.linear.ArrayRealVector;
import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.linear.OpenMapRealMatrix;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;

/**
 * Test {@link Matrix}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class MatrixTest {
    
    /**
     * An example square {@link Matrix}
     */
    public static final Matrix SQR_MATRIX = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                                        { 2, 3, 4, 5, 1 },
                                                                        { 3, 4, 5, 1, 2 },
                                                                        { 4, 5, 1, 2, 3 },
                                                                        { 5, 1, 2, 3, 4 } });
    
    /**
     * An example skinny {@link Matrix}
     */
    public static final Matrix SKINNY_MATRIX = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                                           { 2, 3, 4, 5, 1 },
                                                                           { 3, 4, 5, 1, 2 },
                                                                           { 4, 5, 1, 2, 3 },
                                                                           { 5, 1, 2, 3, 4 },
                                                                           { 6, 7, 8, 9, 0 },
                                                                           { 0, 6, 7, 8, 9 },
                                                                           { 9, 0, 6, 7, 8 } });
    
    /**
     * An example skinny {@link Matrix}
     */
    private static final Matrix FAT_MATRIX = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                                         { 2, 3, 4, 5, 1 },
                                                                         { 3, 4, 5, 1, 2 } });
    
    /** A 9x9 {@link Matrix} of all ones */
    private static final Matrix ALL_ONES_9X9 =
            new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
    
    /** A 3x3 {@link Matrix} of all twos */
    private static final Matrix ALL_TWOS_3X3 = new Matrix(new double[][] { { 2, 2, 2 },
                                                                           { 2, 2, 2 },
                                                                           { 2, 2, 2 } });
    
    /** A 12x9 {@link Matrix} of all ones */
    private static final Matrix ALL_ONES_12X9 =
            new Matrix(new double[][] {  { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                         { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
    
    /** A 5x2 {@link Matrix} of all twos */
    private static final Matrix ALL_TWOS_5X2 = new Matrix(
            new double[][] { { 2, 2 },
                             { 2, 2 },
                             { 2, 2 },
                             { 2, 2 },
                             { 2, 2 } });

    /** Create and return a new random {@link Matrix} in perfect upper-N-Hessenberg form (perfect meaning that the
     * values under the first N sub-diagonals are identically equal to zero)
     * 
     * @param dim both the number of rows and columns of the square matrix
     * @param n the number of non-zero sub-diagonals
     * @param maxRowHermNorm the maximum {@link NVector#getNorm()} of any of the rows
     * @param randomSeed the random seed to use
     * @return a new random {@link Matrix} in perfect upper-N-Hessenberg form */
    public static Matrix randomUpperNHessenberg(final int dim,
                                                final int n,
                                                final double maxRowHermNorm,
                                                final long randomSeed) {
        Validation.requireGreaterThan(dim, 2, "dimension");
        Validation.requireLessThan(n, dim, "n");
        
        final Matrix answer = randomMatrix(dim, dim, maxRowHermNorm, randomSeed);
        for (int i = 1; i < dim; i++) {
            for (int j = 0; j < i - n; j++) {
                answer.setEntry(i, j, 0.0);
            }
        }
        
        return answer;
    }
    
    /** Create and return a new random {@link Matrix} in perfect upper-triangular form (perfect meaning that the values
     * under the main diagonal are identically equal to zero)
     * 
     * @param dim both the number of rows and columns of the square matrix
     * @param maxRowHermNorm the maximum {@link NVector#getNorm()} of any of the rows
     * @param randomSeed the random seed to use
     * @return a new random {@link Matrix} in perfect upper-triangular form */
    public static Matrix randomUpperTriangular(final int dim, final double maxRowHermNorm, final long randomSeed) {
        return randomUpperNHessenberg(dim, 0, maxRowHermNorm, randomSeed);
    }
    
    /** Compute and return a new n-cyclic matrix.<br>
     * 
     * An n-cyclic matrix is a permutation matrix which, when pre-multiplying a {@code n x m} matrix, permutes the last
     * row of a matrix to the first, and moves everything else down one place. Post-multiplying has the analogous effect
     * on the columns of the target matrix.<br>
     * <br>
     * 
     * E.g., a 4-cyclic matrix:<br>
     * 
     * <pre>
     * 0 0 0 1
     * 1 0 0 0
     * 0 1 0 0
     * 0 0 1 0
     * </pre>
     * 
     * E.g., a row permutation:<br>
     * 
     * <pre>
     * 0 0 0 0 1           1 2 3 4 5           5 1 2 3 4
     * 1 0 0 0 0           2 3 4 5 1           1 2 3 4 5
     * 0 1 0 0 0     *     3 4 5 1 2     =     2 3 4 5 1
     * 0 0 1 0 0           4 5 1 2 3           3 4 5 1 2
     * 0 0 0 1 0           5 1 2 3 4           4 5 1 2 3
     * </pre>
     * 
     * @param n the dimension of the matrix to create
     * @return the new n-cyclic matrix */
    public static Matrix nCyclicMatrix(final int n) {
        final double[][] cyclicData = new double[n][n]; // all zeroes
        for (int rowIndex = 0; rowIndex < n; rowIndex++) {
            final int colIndex = (rowIndex - 1 + n) % n; // maps -1 to n - 1
            cyclicData[rowIndex][colIndex] = 1;
        }
        
        return new Matrix(cyclicData);
    }
    
    /** Create a matrix of the form
     * 
     * <pre>
     * 0  1 0 0
     * 1  0 &eta; 0
     * 0 -&eta; 0 1
     * 0  0 1 0
     * </pre>
     * 
     * with 2e-14 &lt;= &eta; &lt;= 1e-6 <br>
     * <br>
     * 
     * which causes the basic QR algorithm to fail to converge in a reasonable number of iterations (#iters > 10^4 for
     * EISPACK HQR, where {@code 30n = 30 * 4 = 120} is the typical upper-bound) <br>
     * <br>
     * 
     * See <a href=
     * "https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=f9e59924ffaa0a42c1a1ad9f50d4fa3be936c536"> How
     * the QR Algorithm fails to converge and how to fix it </a>, where this construction is attributed to J. W. Demmel.
     * <br>
     * 
     * @param eta the value (2e-14 &lt;= &eta; &lt;= 1e-6) to use in the internal {@code 2 x 2} sub-matrix of the
     *        problematic Demmel matrix.
     * @return the problematic Demmel matrix */
    public static Matrix problematicDemmel(final double eta) {
        Validation.requireWithinRange(eta, 2e-14, 1e-6, "eta");
        return new Matrix(new double[][] { { 0,  1,   0,   0 },
                                           { 1,  0,   eta, 0 },
                                           { 0, -eta, 0,   1 },
                                           { 0,  0,   1,   0 } });
    }
    
    /** Create a pseudo-random {@link Matrix} by stacking a series of pseudo-random {@link NVector}s creating using
     * {@link NVectorTest#randomNVector(int, double, long)}
     * 
     * @param numRows the number of rows
     * @param numCols the number of columns
     * @param maxRowNorm the maximum {@link NVector#getNorm()} of any of the rows
     * @param randomSeed the random seed to use
     * @return the new pseudo-random {@link Matrix} */
    public static Matrix randomMatrix(final int numRows,
                                      final int numCols,
                                      final double maxRowNorm,
                                      final long randomSeed) {
        final double[][] arr = new double[numRows][];
        
        for (int row = 0; row < numRows; row++) {
            final int seedMult = (1 + row) * numCols;
            arr[row] = NVectorTest.randomNVector(numCols, maxRowNorm, seedMult * randomSeed).toArray(); // clones
        }
        
        return new Matrix(arr);
    }
    
    /** This is just a convenience pass-through to {@link #assertBackwardsStableError(Matrix, Matrix, int)} where we
     * first convert the {@link NVector}s into row matrices.<br>
     * <br>
     * 
     * Assert that the the perturbation (or error) matrix {@code E = expected - actual} has small enough
     * {@link Matrix#getFrobeniusNorm()} to guarantee backward stability.
     * 
     * @param expected the expected {@link NVector}
     * @param actual the actual {@link NVector}
     * @param c a "small constant" by which we will multiply the acceptable error. Keep this as small as possible. This
     *        value is to help simulate that the error bounds are expressed in big-O notation. */
    public static void assertBackwardsStableError(final NVector expected, final NVector actual, final int c) {
        assertBackwardsStableError(new Matrix(new double[][] { expected.getDataRef() }),
                                   new Matrix(new double[][] { actual.getDataRef() }),
                                   c);
    }
    
    /** Assert that the the perturbation (or error) matrix {@code E = expected - actual} has small enough
     * {@link Matrix#getFrobeniusNorm()} to guarantee backward stability.<br>
     * <br>
     * 
     * See {@link ComplexMatrixTest#assertBackwardsStableError(ComplexMatrix, ComplexMatrix, int)} for a detailed
     * description.
     * 
     * @param expected the expected {@link Matrix}
     * @param actual the actual {@link Matrix}
     * @param c a "small constant" by which we will multiply the acceptable error. Keep this as small as possible. This
     *        value is to help simulate that the error bounds are expressed in big-O notation. */
    public static void assertBackwardsStableError(final Matrix expected, final Matrix actual, final int c) {
        MatrixUtils.checkSubtractionCompatible(expected, actual); // throws on dimension mismatch
        
        final Matrix E     = expected.minus(actual);
        final double frobE = E.getFrobeniusNorm();
        
        final int    mn                = expected.getRowDimension() * expected.getColumnDimension();
        final double frobA             = actual.getFrobeniusNorm();
        final double maxAllowableError = c * mn * Numerics.MACHINE_EPSILON * frobA;
        
        assertLessThanOrEqualTo(frobE, maxAllowableError, "the error-matrix 2-norm");
    }
    
    /** Construct a random {@link Matrix} with the desired Eigenvalues.<br>
     * <br>
     * 
     * See {@link ComplexMatrixTest#randomEigvalMatrix(ptolemaeus.math.linear.complex.ComplexVector, long)} for details
     * 
     * @param eigenvalues the desired Eigenvalues
     * @param seed the random seed for constructing the {@code S} matrix
     * @return the matrix with the desired Eigenvalues */
    public static Matrix randomEigvalMatrix(final long seed, final double... eigenvalues) {
        final int    n = eigenvalues.length;
        final Matrix L = Matrix.diagonal(eigenvalues); // L -> Lambda
        final Matrix S = randomMatrix(n, n, 1.0, seed);
        
        return LinearSolver.defaultSolver().solve(S, L.times(S));
    }
    
    /** Construct a random {@link Matrix} with the desired Eigenvalues.<br>
     * <br>
     * 
     * See {@link ComplexMatrixTest#randomEigvalMatrix(ptolemaeus.math.linear.complex.ComplexVector, long)} for details
     * 
     * @param eigenvalues the desired Eigenvalues
     * @param seed the random seed for constructing the {@code S} matrix
     * @return the matrix with the desired Eigenvalues */
    public static Matrix randomEigvalMatrix(final NVector eigenvalues, final long seed) {
        return randomEigvalMatrix(seed, eigenvalues.getDataRef());
    }
    
    /** Test that {@link Matrix#getMaxRank() max rank} is properly set */
    @Test
    void testGetMaxRank() {
        final Matrix A  = Matrix.of(ALL_ONES_12X9);
        final Matrix AT = A.transpose();
        
        assertEquals(9, A.getMaxRank());
        assertEquals(9, AT.getMaxRank());
    }
    
    /** Test {@link Matrix#constant(double, int, int)} */
    @Test
    void testConstant() {
        final Matrix A = Matrix.constant(7.0, 10, 5);
        
        assertEquals(10, A.getRowDimension());
        assertEquals(5, A.getColumnDimension());
        
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                assertEquals(7.0, A.getEntry(i, j));
            }
        }
    }
    
    /** Test {@link Matrix#ones(int, int)} */
    @Test
    void testOnes() {
        final Matrix A = Matrix.ones(10, 5);
        
        assertEquals(10, A.getRowDimension());
        assertEquals(5, A.getColumnDimension());
        
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                assertEquals(1.0, A.getEntry(i, j));
            }
        }
    }
    
    /** Test {@link Matrix#zero(int, int)} */
    @Test
    void testZero() {
        final Matrix A = Matrix.zero(10, 5);
        
        assertEquals(10, A.getRowDimension());
        assertEquals(5, A.getColumnDimension());
        
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                assertEquals(0.0, A.getEntry(i, j));
            }
        }
    }
    
    /** Test {@link Matrix#addToEntry(int, int, double)} */
    @Test
    void testAddToEntry() {
        final Matrix A = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 } });
        
        A.addToEntry(2, 2, 5);
        
        final Matrix expected = new Matrix(new double[][] { { 1, 2,  3, 4, 5 },
                                                            { 2, 3,  4, 5, 1 },
                                                            { 3, 4, 10, 1, 2 },
                                                            { 4, 5,  1, 2, 3 },
                                                            { 5, 1,  2, 3, 4 } });
        assertEquals(expected, A);
    }
    
    /** Test {@link Matrix#subtractFromEntry(int, int, double)} */
    @Test
    void testSubtractFromEntry() {
        final Matrix A = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 } });
        
        A.subtractFromEntry(2, 2, 5);
        
        final Matrix expected = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                            { 2, 3, 4, 5, 1 },
                                                            { 3, 4, 0, 1, 2 },
                                                            { 4, 5, 1, 2, 3 },
                                                            { 5, 1, 2, 3, 4 } });
        assertEquals(expected, A);
    }
    
    /** Test {@link Matrix#of(AnyMatrix)} */
    @Test
    void testOf() {
        final Matrix A = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 } });
        
        final Array2DRowRealMatrix copy1 = new Array2DRowRealMatrix(A.getDataRef());
        
        final OpenMapRealMatrix copy2 = new OpenMapRealMatrix(5, 5);
        copy2.setSubMatrix(A.getData(), 0, 0);
        
        final ComplexMatrix copy3 = ComplexMatrix.of(A);
        
        assertEquals(A, Matrix.of(A));
        assertEquals(A, Matrix.of(copy1));
        assertEquals(A, Matrix.of(copy2));
        assertEquals(A, Matrix.of(copy3));
        
        assertThrowsWithMessage(UnsupportedOperationException.class,
                                () -> Matrix.of(new Array2DRowFieldMatrix<>(ComplexField.getInstance())),
                                "Unable to create ComplexMatrix from type Array2DRowFieldMatrix");
        
        assertThrowsWithMessage(UnsupportedOperationException.class,
                                () -> Matrix.of(ComplexMatrixTest.randomComplexMatrix(1, 1, 1.0, 1L, false)),
                                "The supplied ComplexMatrix has non-zero imaginary parts");
    }
    
    /** Test {@link Matrix#isWide()}, {@link Matrix#isTall()}, and {@link Matrix#isSquare()} */
    @Test
    void testWideTallSquare() {
        final Matrix tall = ALL_ONES_12X9;
        assertTrue(tall.isTall());
        assertFalse(tall.isWide());
        assertFalse(tall.isSquare());
        
        final Matrix wide = tall.transpose();
        assertFalse(wide.isTall());
        assertTrue(wide.isWide());
        assertFalse(wide.isSquare());
        
        final Matrix square = ALL_ONES_9X9;
        assertFalse(square.isTall());
        assertFalse(square.isWide());
        assertTrue(square.isSquare());
    }
    
    /** Test {@link Matrix#Matrix(double[][], boolean)} */
    @Test
    void testNoCloneConstructor() {
        final double[][] data = SQR_MATRIX.getData();
        final Matrix A1 = new Matrix(data, false);
        assertSame(data, A1.getDataRef());
        
        final Matrix A2 = new Matrix(data, true);
        assertNotSame(data, A2.getDataRef());
        assertArrayEquality(data, A2.getDataRef());
    }
    
    /** Test {@link Matrix#times(Matrix)} */
    @Test
    void testTimes() {
        final Matrix A = ALL_ONES_12X9.copy();
        assertEquals(A.multiply(A.transpose()), A.times(A.transpose()));
    }
    
    /** Test {@link Matrix#plus(Matrix)} */
    @Test
    void testPlus() {
        final Matrix A = ALL_ONES_12X9.copy();
        assertEquals(A.scalarMultiply(2.0), A.plus(A));
    }
    
    /** Test {@link Matrix#minus(Matrix)} */
    @Test
    void testMinus() {
        final Matrix A = ALL_ONES_12X9.copy();
        assertEquals(0.0, A.minus(A).getFrobeniusNorm());
    }
    
    /** Test {@link Matrix#getSubMatrix(int, int, int, int)} */
    @Test
    void testGetSubMatrix() {
        final Matrix A = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                     { 2, 3, 4, 5, 1 },
                                                     { 3, 4, 5, 1, 2 },
                                                     { 4, 5, 1, 2, 3 },
                                                     { 5, 1, 2, 3, 4 },
                                                     { 6, 7, 8, 9, 0 },
                                                     { 0, 6, 7, 8, 9 },
                                                     { 9, 0, 6, 7, 8 } });
        
        final Matrix subA = A.getSubMatrix(2, 5, 1, 3);
        final Matrix expectedSubA = new Matrix(new double[][] { { 4, 5, 1 },
                                                                { 5, 1, 2 },
                                                                { 1, 2, 3 },
                                                                { 7, 8, 9 } });
        
        assertEquals(expectedSubA, subA);
    }

    /**
     * Test {@link Matrix#slice(int, int, int, int)} and {@link Matrix#slice(int[], int[])}
     */
    @Test
    void testSlicing() {
        MatrixSlice slice = SQR_MATRIX.slice(0, 1, 0, 1);
        assertEquals(new Matrix(new double[][]{{1, 2}, {2, 3}}), slice.copy());

        slice = SQR_MATRIX.slice(new int[]{4, 3}, new int[]{4, 3});
        assertEquals(new Matrix(new double[][]{{4, 3}, {3, 2}}), slice.copy());
    }
    
    /** Test the exceptional cases of
     * {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)} */
    @Test
    void testPostMultiplyInPlaceExceptional() {
        final Matrix A = ALL_ONES_12X9.copy();
        final Matrix B = ALL_TWOS_5X2.copy();
        
        final NumericalMethodsException nme1 = assertThrows(NumericalMethodsException.class,
                                                            () -> Matrix.postMultiplyInPlace(A, B, 0, 100));
        final NumericalMethodsException nme2 = assertThrows(NumericalMethodsException.class,
                                                            () -> Matrix.postMultiplyInPlace(A, B, 100, 0));
        
        assertEquals("Matrix A (12 x 9) cannot be post-multiplied in-place by "
                     + "smallB (5 x 2) when starting at indices (0, 100)",
                     nme1.getMessage());
        assertEquals("Matrix A (12 x 9) cannot be post-multiplied in-place by"
                     + " smallB (5 x 2) when starting at indices (100, 0)",
                     nme2.getMessage());
    }
    
    /** Test the exceptional cases of
     * {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)} */
    @Test
    void testPreMultiplyInPlaceExceptional() {
        final Matrix A = ALL_TWOS_5X2.copy();
        final Matrix B = ALL_ONES_12X9.copy();
        
        final NumericalMethodsException nme1 = assertThrows(NumericalMethodsException.class,
                                                            () -> Matrix.preMultiplyInPlace(A, B, 0, 100));
        final NumericalMethodsException nme2 = assertThrows(NumericalMethodsException.class,
                                                            () -> Matrix.preMultiplyInPlace(A, B, 100, 0));
        
        assertEquals("Matrix B (12 x 9) cannot be pre-multiplied in-place by "
                     + "smallA (5 x 2) when starting at indices (0, 100)",
                     nme1.getMessage());
        assertEquals("Matrix B (12 x 9) cannot be pre-multiplied in-place by "
                     + "smallA (5 x 2) when starting at indices (100, 0)",
                     nme2.getMessage());
    }
    
    // **************** post-multiply in-place square tests ****************
    
    /** Test a simple square case of {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)}.
     * This one sets the start indices to {@code (1, 5)}, which is above the diagonal. */
    @Test
    void testPostMultiplyInPlaceSquare1() {
        final Matrix B1 = new Matrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 2, 2, 2, 0 },
                                                      { 0, 0, 1, 0, 0, 2, 2, 2, 0 },
                                                      { 0, 0, 0, 1, 0, 2, 2, 2, 0 },
                                                      { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected1 = new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                              { 1, 1, 1, 1, 1, 7, 7, 7, 1 } });
        
        final Matrix fullMultiply = ALL_ONES_9X9.multiply(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix ABSmall1 = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_3X3, 1, 5);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple square case of {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)}.
     * This one sets the start indices to {@code (5, 1)}, which is below the diagonal. */
    @Test
    void testPostMultiplyInPlaceSquare2() {
        final Matrix B2 = new Matrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 2, 0, 1, 0, 0, 0 },
                                                      { 0, 2, 2, 2, 0, 0, 1, 0, 0 },
                                                      { 0, 2, 2, 2, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected2 = new Matrix(new double[][]  { { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                              { 1, 7, 7, 7, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = ALL_ONES_9X9.multiply(B2);
        assertEquals(expected2, fullMultiply); // sanity check
        
        final Matrix ABSmall1 = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_3X3, 5, 1);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple square case of {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)}.
     * This one sets the start indices to {@code (2, 4)}, which covers one diagonal entry. */
    @Test
    void testPostMultiplyInPlaceSquare3() {
        final Matrix B3 = new Matrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 1, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 0, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected3 = new Matrix(new double[][]  { { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                              { 1, 1, 1, 1, 6, 7, 7, 1, 1 } });
        
        final Matrix fullMultiply = ALL_ONES_9X9.multiply(B3);
        assertEquals(expected3, fullMultiply); // sanity check
        
        final Matrix ABSmall1 = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_3X3, 2, 4); // modifies the copy
        
        assertEquals(fullMultiply, ABSmall1);
    }
    
    // **************** post-multiply in-place rectangular tests ****************
    
    /** Test a simple rectangular case of
     * {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)}. This one sets the start
     * indices to {@code (4, 1)}, which is below the diagonal. {@code A} (12x9) and {@code smallB} (5x2) are both
     * "tall". */
    @Test
    void testPostMultiplyInPlaceRectangular1() {
        final Matrix B1 = new Matrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 2, 2, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected1 = new Matrix(new double[][]  { { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 11, 11, 1, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = ALL_ONES_12X9.multiply(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix ABSmall1 = ALL_ONES_12X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_5X2, 4, 1);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple rectangular case of
     * {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)}. This one sets the start
     * indices to {@code (4, 5)}, which causes {@code smallB} to cover one diagonal value. {@code A} (9x12) and
     * {@code smallB} (2x5) are both "wide". */
    @Test
    void testPostMultiplyInPlaceRectangular2() {
        final Matrix A = ALL_ONES_12X9.transpose();
        final Matrix smallB = ALL_TWOS_5X2.transpose();
        
        final Matrix B1 = new Matrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } });

        final Matrix expected1 =
                new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                             { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix ABSmall1 = A.copy(); // Important! If not copied, it's modified on the next line
        Matrix.postMultiplyInPlace(ABSmall1, smallB, 4, 5);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple rectangular case of
     * {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)}. This one sets the start
     * indices to {@code (2, 1)}, which causes {@code smallB} to cover two diagonal values. {@code A} (12x9) is "tall"
     * and {@code smallB} (2x5) is "wide". */
    @Test
    void testPostMultiplyInPlaceRectangular3() {
        final Matrix A = ALL_ONES_12X9;
        final Matrix smallB = ALL_TWOS_5X2.transpose();
        
        final Matrix B1 = new Matrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                      { 0, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected1 = new Matrix(new double[][]  { { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                              { 1, 5, 4, 4, 5, 5, 1, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix ABSmall1 = A.copy(); // Important! If not copied, it's modified on the next line
        Matrix.postMultiplyInPlace(ABSmall1, smallB, 2, 1);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    // **************** post-multiply in-place random tests ****************
    
    /** Test {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)} using
     * {@link #testPostMultiplyInPlace(Matrix, Matrix)}
     * 
     * We use a random 10x10 (square) {@link Matrix} as {@code A}, and a random 5x5 (square)
     * {@link Matrix} as {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPostMultiplyInPlaceRandomSquares() {
        final Matrix A      = randomMatrix(10, 10, 100.0, 8675309L);
        final Matrix smallB = randomMatrix(5, 5, 100.0, 7777777L);
        
        // set an arbitrary entry in both to zero to make sure we hit the short-circuit branches
        A.setEntry(2, 1, 0);
        smallB.setEntry(1, 2, 0);
        
        testPostMultiplyInPlace(A, smallB);
    }
    
    /** Test {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)} using
     * {@link #testPostMultiplyInPlace(Matrix, Matrix)}
     * 
     * We use a random 15x10 (tall) {@link Matrix} as {@code A}, and a random 5x7 (wide) {@link Matrix} as
     * {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPostMultiplyInPlaceRandomTall() {
        final Matrix A      = randomMatrix(15, 10, 100.0, 8675309L);
        final Matrix smallB = randomMatrix(5, 7, 100.0, 7777777L);
        
        testPostMultiplyInPlace(A, smallB);
    }
    
    /** Test {@link Matrix#postMultiplyInPlace(Matrix, Matrix, int, int)} by comparing results to
     * when performing the full multiplication.<br><br>
     * 
     * The {@link ComplexMatrix} version of this method is already well tested and accepted, so we test against that in 
     * addition to the normal testing.
     * 
     * @param A the matrix to be post-multiplied
     * @param smallB the smaller matrix which we treat as padded */
    private void testPostMultiplyInPlace(final Matrix A, final Matrix smallB) {
        final int maxRowStartIndex = A.getColumnDimension() - smallB.getRowDimension();
        final int maxColStartIndex = A.getColumnDimension() - smallB.getColumnDimension();
        
        final ComplexMatrix Ac      = new ComplexMatrix(A);
        final ComplexMatrix smallBc = new ComplexMatrix(smallB);
        
        for (int rowStart = 0; rowStart < maxRowStartIndex; rowStart++) { // test all allowable start indices
            for (int colStart = 0; colStart < maxColStartIndex; colStart++) {
                
                final Matrix B = Matrix.identity(A.getColumnDimension());
                B.setSubMatrix(smallB.getData(), rowStart, colStart);
                
                final Matrix expected =
                        new Matrix(new Array2DRowRealMatrix(A.getData())
                                   .multiply(new Array2DRowRealMatrix(B.getData())).getData());
                
                final Matrix ABSmall = A.copy(); // Important! If not copied, it's modified on the next line
                Matrix.postMultiplyInPlace(ABSmall, smallB, rowStart, colStart);
                
                final int fRowStart = rowStart; // finals for lambda
                final int fColStart = colStart;
                assertMatrixEquals(expected, ABSmall, 2e-12, () -> "(%d, %d)".formatted(fRowStart, fColStart));
                
                // test against the ComplexMatrix equivalent:
                final ComplexMatrix ABSmallc = Ac.copy();
                ComplexMatrix.postMultiplyInPlace(ABSmallc, smallBc, rowStart, colStart);
                final Matrix ABSmallcReal = ABSmallc.asReal(0.0).orElseThrow();
                assertEquals(ABSmallcReal, ABSmall);
            }
        }
    }
    
    // **************** pre-multiply in-place square tests ****************
    
    /** Test a simple square case of {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)}.
     * This one sets the start indices to {@code (1, 5)}, which is above the diagonal. */
    @Test
    void testPreMultiplyInPlaceSquare1() {
        final Matrix A = new Matrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 2, 2, 2, 0 },
                                                      { 0, 0, 1, 0, 0, 2, 2, 2, 0 },
                                                      { 0, 0, 0, 1, 0, 2, 2, 2, 0 },
                                                      { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });

        final Matrix expected1 = new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(ALL_ONES_9X9);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix smallAB = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.preMultiplyInPlace(ALL_TWOS_3X3, smallAB, 1, 5);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple square case of {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)}.
     * This one sets the start indices to {@code (5, 1)}, which is below the diagonal. */
    @Test
    void testPreMultiplyInPlaceSquare2() {
        final Matrix A = new Matrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 2, 0, 1, 0, 0, 0 },
                                                      { 0, 2, 2, 2, 0, 0, 1, 0, 0 },
                                                      { 0, 2, 2, 2, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected2 = new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(ALL_ONES_9X9);
        assertEquals(expected2, fullMultiply); // sanity check
        
        final Matrix smallAB = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.preMultiplyInPlace(ALL_TWOS_3X3, smallAB, 5, 1);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple square case of {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)}.
     * This one sets the start indices to {@code (2, 4)}, which covers one diagonal entry. */
    @Test
    void testPreMultiplyInPlaceSquare3() {
        final Matrix A = new Matrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 1, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 0, 2, 2, 2, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected3 = new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                              { 6, 6, 6, 6, 6, 6, 6, 6, 6 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(ALL_ONES_9X9);
        assertEquals(expected3, fullMultiply); // sanity check
        
        final Matrix smallAB = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.preMultiplyInPlace(ALL_TWOS_3X3, smallAB, 2, 4);
        assertEquals(fullMultiply, smallAB);
    }
    
    // **************** pre-multiply in-place rectangular tests ****************
    
    /** Test a simple rectangular case of
     * {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)}. This one sets the start
     * indices to {@code (4, 1)}, which is below the diagonal. {@code smallA} (5x2) and {@code B} (12x9) are both
     * "tall". */
    @Test
    void testPreMultiplyInPlaceRectangular1() {
        final Matrix A = new Matrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 2, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final Matrix expected1 = new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                              { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                              { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                              { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                              { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                              { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(ALL_ONES_12X9);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix smallAB = ALL_ONES_12X9.copy(); // Important! If not copied, it's modified on the next line
        Matrix.preMultiplyInPlace(ALL_TWOS_5X2, smallAB, 4, 1);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple rectangular case of
     * {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)}. This one sets the start
     * indices to {@code (2, 3)}, which causes {@code smallA} to cover one diagonal value. {@code smallA} (2x5) and
     * {@code B} (9x12) are both "wide". */
    @Test
    void testPreMultiplyInPlaceRectangular2() {
        final Matrix smallA = ALL_TWOS_5X2.transpose();
        final Matrix B = ALL_ONES_12X9.transpose();
        
        final Matrix A = new Matrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 2, 2, 2, 2, 2, 0 },
                                                      { 0, 0, 0, 2, 2, 2, 2, 2, 0 },
                                                      { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
    
        final Matrix expected1 =
                new Matrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                             { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                             { 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11 },
                                             { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
                                             { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                             { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                             { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                             { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                             { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final Matrix fullMultiply = A.multiply(B);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix smallAB = B.copy(); // Important! If not copied, it's modified on the next line
        Matrix.preMultiplyInPlace(smallA, smallAB, 2, 3);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple rectangular case of
     * {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)}. This one sets the start
     * indices to {@code (3, 4)}, which causes {@code smallA} to cover two diagonal values. {@code B} (12x9) is "tall"
     * and {@code smallA} (2x5) is "wide". */
    @Test
    void testPreMultiplyInPlaceRectangular3() {
        final Matrix smallA = ALL_TWOS_5X2.transpose();
        final Matrix B      = new Matrix(new double[][] { { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                          { 1, 2, 3, 4, 5, 6, 7, 8, 9 } });
        
        final Matrix A = new Matrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 1, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } });

        final Matrix expected1 =
                new Matrix(new double[][]  { { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 11, 22, 33, 44, 55, 66, 77, 88, 99 },
                                             { 10, 20, 30, 40, 50, 60, 70, 80, 90 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                             { 1, 2, 3, 4, 5, 6, 7, 8, 9 } });
        
        final Matrix fullMultiply = A.multiply(B);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final Matrix smallAB = B.copy(); // Important! If not copied, it's modified on the next line
        Matrix.preMultiplyInPlace(smallA, smallAB, 3, 4);
        assertEquals(fullMultiply, smallAB);
    }
    
    // **************** pre-multiply in-place random tests ****************
    
    /** Test {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)} using
     * {@link #testPreMultiplyInPlace(Matrix, Matrix)}
     * 
     * We use a random 10x10 (square) {@link Matrix} as {@code A}, and a random 5x5 (square)
     * {@link Matrix} as {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPreMultiplyInPlaceRandomSquares() {
        final Matrix smallA = randomMatrix(5,   5, 100.0, 7777777L);
        final Matrix B      = randomMatrix(10, 10, 100.0, 8675309L);
        
        // set an arbitrary entry in both to zero to make sure we hit the short-circuit branches
        smallA.setEntry(1, 2, 0);
        B.setEntry(2, 1, 0);
        
        testPreMultiplyInPlace(smallA, B);
    }
    
    /** Test {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)} using
     * {@link #testPreMultiplyInPlace(Matrix, Matrix)}
     * 
     * We use a random 15x10 (tall) {@link Matrix} as {@code A}, and a random 5x7 (wide)
     * {@link Matrix} as {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPreMultiplyInPlaceRandomTall() {
        final Matrix smallA = randomMatrix(5,   7, 100.0, 7777777L);
        final Matrix B      = randomMatrix(15, 10, 100.0, 8675309L);
        
        testPreMultiplyInPlace(smallA, B);
    }
    
    /** Test {@link Matrix#preMultiplyInPlace(Matrix, Matrix, int, int)} by comparing
     * results to when performing the full multiplication.<br><br>
     * 
     * The {@link ComplexMatrix} version of this method is already well tested and accepted, so we test against that in 
     * addition to the normal testing.
     * 
     * @param smallA the smaller matrix which we treat as padded
     * @param B the matrix to be pre-multiplied */
    private void testPreMultiplyInPlace(final Matrix smallA, final Matrix B) {
        final int maxRowStartIndex = B.getRowDimension() - smallA.getRowDimension();
        final int maxColStartIndex = B.getRowDimension() - smallA.getColumnDimension();
        
        final ComplexMatrix smallAc = new ComplexMatrix(smallA);
        final ComplexMatrix Bc      = new ComplexMatrix(B);
        
        for (int rowStart = 0; rowStart < maxRowStartIndex; rowStart++) { // test all allowable start indices
            for (int colStart = 0; colStart < maxColStartIndex; colStart++) {
                final Matrix A = Matrix.identity(B.getRowDimension());
                A.setSubMatrix(smallA.getData(), rowStart, colStart);
                
                final Matrix expected = A.multiply(B);
                
                final Matrix smallAB = B.copy(); // Important! If not copied, it's modified on the next line
                Matrix.preMultiplyInPlace(smallA, smallAB, rowStart, colStart);
                
                final int fRowStart = rowStart; // finals for lambda
                final int fColStart = colStart;
                assertMatrixEquals(expected, smallAB, 2e-12, () -> "(%d, %d)".formatted(fRowStart, fColStart));
                
                // test against the ComplexMatrix equivalent:
                final ComplexMatrix smallABc = Bc.copy();
                ComplexMatrix.preMultiplyInPlace(smallAc, smallABc, rowStart, colStart);
                final Matrix smallABcReal = smallABc.asReal(0.0).orElseThrow();
                assertEquals(smallABcReal, smallAB);
            }
        }
    }
    
    /** Test {@link Matrix#getRowVector(int)} */
    @Test
    void testGetRowVector() {
        final NVector row = SKINNY_MATRIX.getRowVector(2);
        assertEquals(new NVector(new double[] { 3, 4, 5, 1, 2 }), row);
    }
    
    /** Test {@link Matrix#getColumnVector(int)} */
    @Test
    void testGetColumnVector() {
        final NVector row = SKINNY_MATRIX.getColumnVector(2);
        assertEquals(new NVector(new double[] { 3, 4, 5, 1, 2, 8, 7, 6 }), row);
    }
    
    /** Test {@link Matrix#Matrix(double[][])} and {@link Matrix#Matrix(RealMatrix, boolean)} */
    @Test
    void testMatrixConstructors() {
        final Matrix m1 = new Matrix(new double[][] { { 5, 4, 3, 2, 1 },
                                                      { 1, 5, 4, 3, 2 },
                                                      { 2, 1, 5, 4, 3 },
                                                      { 3, 2, 1, 5, 4 },
                                                      { 4, 3, 2, 1, 5 } });
        final Matrix m2 = new Matrix(m1, false);
        assertEquals(m1, m2);
    }
    
    /** Test {@link Matrix#copy()} */
    @Test
    void testCopy() {
        final Matrix copy = SKINNY_MATRIX.copy();
        assertEquals(SKINNY_MATRIX, copy);
        assertNotSame(SKINNY_MATRIX.getDataRef(), copy.getDataRef());
    }
    
    /**
     * Test {@link Matrix#solve(RealMatrix)} with a square matrix
     */
    @Test
    void testSolveMatrixSquare() {
        final Matrix rhs = new Matrix(new double[][] { { 45, 40, 40, 45, 55 },
                                                       { 40, 40, 45, 55, 45 },
                                                       { 40, 45, 55, 45, 40 },
                                                       { 45, 55, 45, 40, 40 },
                                                       { 55, 45, 40, 40, 45 } });
        
        final Matrix expected = new Matrix(new double[][] { { 5, 4, 3, 2, 1 },
                                                            { 1, 5, 4, 3, 2 },
                                                            { 2, 1, 5, 4, 3 },
                                                            { 3, 2, 1, 5, 4 },
                                                            { 4, 3, 2, 1, 5 } });
        testMatrixSolve(SQR_MATRIX, rhs, expected);
    }
    
    /**
     * Test {@link Matrix#solve(RealMatrix)} with a skinny matrix
     */
    @Test
    void testSolveMatrixSkinny() {
        final Matrix rhs = new Matrix(new double[][] { { 15, 30, 45, 60, 75, 90, 105, 120 },
                                                       { 15, 30, 45, 60, 75, 90, 105, 120 },
                                                       { 15, 30, 45, 60, 75, 90, 105, 120 },
                                                       { 15, 30, 45, 60, 75, 90, 105, 120 },
                                                       { 15, 30, 45, 60, 75, 90, 105, 120 },
                                                       { 30, 60, 90, 120, 150, 180, 210, 240 },
                                                       { 30, 60, 90, 120, 150, 180, 210, 240 },
                                                       { 30, 60, 90, 120, 150, 180, 210, 240 } });
        
        final Matrix expected = new Matrix(new double[][] { { 1, 2, 3, 4, 5, 6, 7, 8 },
                                                            { 1, 2, 3, 4, 5, 6, 7, 8 },
                                                            { 1, 2, 3, 4, 5, 6, 7, 8 },
                                                            { 1, 2, 3, 4, 5, 6, 7, 8 },
                                                            { 1, 2, 3, 4, 5, 6, 7, 8 } });
        testMatrixSolve(SKINNY_MATRIX, rhs, expected);
    }
    
    /**
     * Test {@link Matrix#solve(RealMatrix)} with a fat matrix
     */
    @Test
    void testSolveMatrixFat() {
        final Matrix rhs = new Matrix(new double[][] { { 15, 30, 45 },
                                                       { 15, 30, 45 },
                                                       { 15, 30, 45 } });
        
        /* the solution is not clean like some of the others because the system is under-determined. The solution is the
         * "least norm" of the infinitely many available. */
        final Matrix expected = new Matrix(new double[][] { { 0.63829787234043, 1.27659574468085, 1.91489361702128 },
                                                            { 0.95744680851064, 1.91489361702128, 2.87234042553191 },
                                                            { 1.27659574468085, 2.5531914893617,  3.82978723404255 },
                                                            { 0.95744680851064, 1.91489361702128, 2.87234042553191 },
                                                            { 0.95744680851064, 1.91489361702128, 2.87234042553192 } });
        testMatrixSolve(FAT_MATRIX, rhs, expected);
    }
    
    /**
     * Test {@link Matrix#solve(RealMatrix)} finding {@code X} in {@code A X = B}
     * 
     * @param lhs the left-hand-side (A)
     * @param rhs the right-hand-side (B)
     * @param expected the expected solution
     */
    private static void testMatrixSolve(final Matrix lhs, final Matrix rhs, final Matrix expected) {
        final Matrix solution = lhs.solve(rhs);
        assertMatrixEquals(expected, solution, 1e-12);
        assertMatrixEquals(lhs.multiply(solution), rhs, 1e-12);
    }
    
    /**
     * Test {@link Matrix#solve(RealVector)}
     */
    @Test
    void testSolveVector() {
        final RealVector rhs = new ArrayRealVector(new double[] { 55, 45, 40, 40, 45, 80, 110, 95 });
        final RealVector soln = SKINNY_MATRIX.solve(rhs);
        final RealVector expected = new ArrayRealVector(new double[] { 1, 2, 3, 4, 5 });
        
        assertTrue(Numerics.vectorElementwiseEquals(expected, soln, 1e-12),
                   () -> String.format("expected: %s, actual: %s", expected, soln));
    }
    
    /**
     * Test {@link Matrix#getDiagonalPiece(RealMatrix)}
     */
    @Test
    public void testGetDiagonalPiece() {
        Matrix m = new Matrix(new double[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } });
        
        Matrix actual = Matrix.getDiagonalPiece(m);
        Matrix expected = new Matrix(new double[][] { { 1, 0, 0 }, { 0, 5, 0 }, { 0, 0, 9 } });
        assertEquals(expected, actual);
    }
    
    /** Test {@link Matrix#formatDoubleForColumn(double, int, int, int)} where the sum of the two parts is less than the
     * provided scientific notation threshold, including NaN and the infinities */
    @Test
    public void testFormatDoubleLessThanThreshold() {
        // single char names for shorter lines so I can keep CS enabled
        final int i = 15; // integer-part length
        final int f = 12; // fractional-part length
        final int t = 30; // threshold for scientific notation
        
        final String zeroString = Matrix.formatDoubleForColumn(0.0, i, f, t);
        assertEquals(i + f + 1 + 1, zeroString.length()); /* the string to the left of the decimal + to the
                                                                     * right + the decimal itself + an extra space
                                                                     * because 0.0 >= 0.0 */
        // notice the alignment in the expected results in these test cases:
        assertEquals("               0.0           ", zeroString);
        
        assertEquals("               1.1           ", Matrix.formatDoubleForColumn(1.1, i, f, t));
        assertEquals("               1.01          ", Matrix.formatDoubleForColumn(1.01, i, f, t));
        assertEquals("               1.001         ", Matrix.formatDoubleForColumn(1.001, i, f, t));
        assertEquals("               1.0001        ", Matrix.formatDoubleForColumn(1.0001, i, f, t));
        assertEquals("               1.00001       ", Matrix.formatDoubleForColumn(1.00001, i, f, t));
        assertEquals("               1.000001      ", Matrix.formatDoubleForColumn(1.000001, i, f, t));
        assertEquals("               1.0000001     ", Matrix.formatDoubleForColumn(1.0000001, i, f, t));
        
        assertEquals("              -1.1           ", Matrix.formatDoubleForColumn(-1.1, i, f, t));
        assertEquals("              -1.01          ", Matrix.formatDoubleForColumn(-1.01, i, f, t));
        assertEquals("              -1.001         ", Matrix.formatDoubleForColumn(-1.001, i, f, t));
        assertEquals("              -1.0001        ", Matrix.formatDoubleForColumn(-1.0001, i, f, t));
        assertEquals("              -1.00001       ", Matrix.formatDoubleForColumn(-1.00001, i, f, t));
        assertEquals("              -1.000001      ", Matrix.formatDoubleForColumn(-1.000001, i, f, t));
        assertEquals("              -1.0000001     ", Matrix.formatDoubleForColumn(-1.0000001, i, f, t));
        
        assertEquals("    -10000000000.1           ", Matrix.formatDoubleForColumn(-10000000000.1, i, f, t));
        assertEquals("    -10000000000.01          ", Matrix.formatDoubleForColumn(-10000000000.01, i, f, t));
        assertEquals("    -10000000000.001         ", Matrix.formatDoubleForColumn(-10000000000.001, i, f, t));
        assertEquals("    -10000000000.0001        ", Matrix.formatDoubleForColumn(-10000000000.0001, i, f, t));
        assertEquals("    -10000000000.00001       ", Matrix.formatDoubleForColumn(-10000000000.00001, i, f, t));
        
        // loss of precision results in these next two not matching the RHS:
        assertEquals("    -10000000000.000002      ", Matrix.formatDoubleForColumn(-10000000000.000001, i, f, t));
        assertEquals("    -10000000000.0           ", Matrix.formatDoubleForColumn(-10000000000.0000001, i, f, t));
        
        assertEquals("               0.123123123123", Matrix.formatDoubleForColumn(0.123123123123, i, f, t));
        assertEquals(" 123123123123123.0           ", Matrix.formatDoubleForColumn(123123123123123.0, i, f, t));
        
        assertEquals("                          NaN", Matrix.formatDoubleForColumn(Double.NaN, i, f, t));
        assertEquals("                     Infinity", Matrix.formatDoubleForColumn(Double.POSITIVE_INFINITY, i, f, t));
        assertEquals("                    -Infinity", Matrix.formatDoubleForColumn(Double.NEGATIVE_INFINITY, i, f, t));
    }
    
    /** Test {@link Matrix#formatDoubleForColumn(double, int, int, int)} where the sum of the two parts is greater than
     * the provided scientific notation threshold, including NaN and the infinities. In this case, it doesn't matter
     * what the individual values are, as we're delegating to formatting using the {@code e} format specifier, as in
     * {@code String.format("%.10e")} */
    @Test
    public void testFormatDoubleGreaterThanThreshold() {
        // single char names for shorter lines so I can keep CS enabled
        final int i = 35; // integer-part length
        final int f = 1;  // fractional-part length
        final int t = 30; // threshold for scientific notation
        
        final String zeroString = Matrix.formatDoubleForColumn(0.0, i, f, t);
        // two leading spaces (one because >= 0, one for spacing), then 23 characters for the number
        assertEquals(1 + 1 + 23, zeroString.length());
        
        // notice the alignment in the expected results in these test cases:
        assertEquals("  0.0000000000000000e+00 ", zeroString); /* extra space at the end because we have to consider
                                                                * exponents with at most three digits (see below) */
        assertEquals("  1.1000000000000000e+00 ", Matrix.formatDoubleForColumn(1.1, i, f, t));
        assertEquals("  1.0100000000000000e+00 ", Matrix.formatDoubleForColumn(1.01, i, f, t));
        assertEquals("  1.0010000000000000e+00 ", Matrix.formatDoubleForColumn(1.001, i, f, t));
        assertEquals("  1.0001000000000000e+00 ", Matrix.formatDoubleForColumn(1.0001, i, f, t));
        assertEquals("  1.0000100000000000e+00 ", Matrix.formatDoubleForColumn(1.00001, i, f, t));
        assertEquals("  1.0000010000000000e+00 ", Matrix.formatDoubleForColumn(1.000001, i, f, t));
        assertEquals("  1.0000001000000000e+00 ", Matrix.formatDoubleForColumn(1.0000001, i, f, t));
        
        assertEquals(" -1.1000000000000000e+00 ", Matrix.formatDoubleForColumn(-1.1, i, f, t));
        assertEquals(" -1.0100000000000000e+00 ", Matrix.formatDoubleForColumn(-1.01, i, f, t));
        assertEquals(" -1.0010000000000000e+00 ", Matrix.formatDoubleForColumn(-1.001, i, f, t));
        assertEquals(" -1.0001000000000000e+00 ", Matrix.formatDoubleForColumn(-1.0001, i, f, t));
        assertEquals(" -1.0000100000000000e+00 ", Matrix.formatDoubleForColumn(-1.00001, i, f, t));
        assertEquals(" -1.0000010000000000e+00 ", Matrix.formatDoubleForColumn(-1.000001, i, f, t));
        assertEquals(" -1.0000001000000000e+00 ", Matrix.formatDoubleForColumn(-1.0000001, i, f, t));
        
        assertEquals(" -1.0000000000100000e+10 ", Matrix.formatDoubleForColumn(-10000000000.1, i, f, t));
        assertEquals(" -1.0000000000010000e+10 ", Matrix.formatDoubleForColumn(-10000000000.01, i, f, t));
        assertEquals(" -1.0000000000001000e+10 ", Matrix.formatDoubleForColumn(-10000000000.001, i, f, t));
        assertEquals(" -1.0000000000000100e+10 ", Matrix.formatDoubleForColumn(-10000000000.0001, i, f, t));
        assertEquals(" -1.0000000000000010e+10 ", Matrix.formatDoubleForColumn(-10000000000.00001, i, f, t));
        
        // loss of precision results in these next two not matching the RHS:
        assertEquals(" -1.0000000000000002e+10 ", Matrix.formatDoubleForColumn(-10000000000.000001, i, f, t));
        assertEquals(" -1.0000000000000000e+10 ", // rounded to exactly 1e10 due to finite precision
                     Matrix.formatDoubleForColumn(-10000000000.0000001, i, f, t));
        
        assertEquals("  1.2312312312312313e-53 ",
             Matrix.formatDoubleForColumn(0.0000000000000000000000000000000000000000000000000000123123123123123123123,
                                 i, f, t));
        assertEquals("  1.2312312312312312e+72 ",
             Matrix.formatDoubleForColumn(1231231231231231231230000000000000000000000000000000000000000000000000000.0,
                                 i, f, t));
        assertEquals(" -1.2312312312312313e-53 ",
             Matrix.formatDoubleForColumn(-0.0000000000000000000000000000000000000000000000000000123123123123123123123,
                                 i, f, t));
        assertEquals(" -1.2312312312312312e+72 ",
             Matrix.formatDoubleForColumn(-1231231231231231231230000000000000000000000000000000000000000000000000000.0,
                                 i, f, t));
        
        assertEquals("  1.7976931348623157e+308", Matrix.formatDoubleForColumn(Double.MAX_VALUE, i, f, t));
        assertEquals(" -1.7976931348623157e+308", Matrix.formatDoubleForColumn(-Double.MAX_VALUE, i, f, t));
        
        assertEquals("  4.9000000000000000e-324", Matrix.formatDoubleForColumn(Double.MIN_VALUE, i, f, t));
        assertEquals(" -4.9000000000000000e-324", Matrix.formatDoubleForColumn(-Double.MIN_VALUE, i, f, t));
        
        assertEquals("  2.2250738585072014e-308", Matrix.formatDoubleForColumn(Double.MIN_NORMAL, i, f, t));
        assertEquals(" -2.2250738585072014e-308", Matrix.formatDoubleForColumn(-Double.MIN_NORMAL, i, f, t));
        
        assertEquals("                      NaN", Matrix.formatDoubleForColumn(Double.NaN, i, f, t));
        assertEquals("                 Infinity", Matrix.formatDoubleForColumn(Double.POSITIVE_INFINITY, i, f, t));
        assertEquals("                -Infinity", Matrix.formatDoubleForColumn(Double.NEGATIVE_INFINITY, i, f, t));
    }
    
    /** Test {@link Matrix#toString} */
    @Test
    public void testToString() {
        final String expected = String.join(System.lineSeparator(),
                                            " 1 2 3 4 5",
                                            " 2 3 4 5 1",
                                            " 3 4 5 1 2",
                                            " 4 5 1 2 3",
                                            " 5 1 2 3 4");
        TestUtils.testMultiLineToString(expected, SQR_MATRIX);
        
        final Matrix longFractions =
                new Matrix(new double[][] { { 1,                1.914,     1.27659574468085 },
                                            { 0.95744680851064, 2.87234,   1.91489361702128 },
                                            { 1.27659574468085, 3.8297872, 2.553            },
                                            { 0.95744680851064, 2.87,      1.91489361702128 },
                                            { 0.95744680851064, 2.1231231, 2.123123123123123123123123123123 } });
        final String expected2 = String.join(System.lineSeparator(),
                                             " 1.0              1.914     1.27659574468085 ",
                                             " 0.95744680851064 2.87234   1.91489361702128 ",
                                             " 1.27659574468085 3.8297872 2.553            ",
                                             " 0.95744680851064 2.87      1.91489361702128 ",
                                             " 0.95744680851064 2.1231231 2.123123123123123");
        TestUtils.testMultiLineToString(expected2, longFractions);
        
        final Matrix longIntsAndFracs =
            new Matrix(new double[][] { { -Long.MAX_VALUE,  1.27659574468085,  1.91489361702128 },
                                        { 0.9574468,        1.91489361702128,  Double.MIN_NORMAL },
                                        { 1.2765957446,     2.553,             1 },
                                        { 0.95744680851064, -Double.MAX_VALUE, 2.87234042553191 },
                                        { Double.MAX_VALUE, 1.91489361702128,  2.123123123123123123123123123123 } });
        final String expected3 = String.join(System.lineSeparator(),
                                         " -9.2233720368547760e+18   1.2765957446808500e+00   1.9148936170212800e+00 ",
                                         "  9.5744680000000000e-01   1.9148936170212800e+00   2.2250738585072014e-308",
                                         "  1.2765957446000000e+00   2.5530000000000000e+00   1.0000000000000000e+00 ",
                                         "  9.5744680851064000e-01  -1.7976931348623157e+308  2.8723404255319100e+00 ",
                                         "  1.7976931348623157e+308  1.9148936170212800e+00   2.1231231231231230e+00 ");
        TestUtils.testMultiLineToString(expected3, longIntsAndFracs);
        
        final Matrix aLittleBitOfEverything =
                new Matrix(new double[][] { { -1_000_000.0001,      1.00000001,              -1.00000001   },
                                            {  1_000_000.0002,    -10.0002000002,            10.0002000002 },
                                            {  1_000_000.0003,    100.0003,                     Double.NaN },
                                            { -1_000.0004,      -1000.0004,       Double.NEGATIVE_INFINITY },
                                            {  1_000.0005,      10000.0005,       Double.POSITIVE_INFINITY } });
        final String expected4 = String.join(System.lineSeparator(),
                                             " -1000000.0001     1.00000001   -1.00000001  ",
                                             "  1000000.0002   -10.0002000002 10.0002000002",
                                             "  1000000.0003   100.0003                 NaN",
                                             "    -1000.0004 -1000.0004           -Infinity",
                                             "     1000.0005 10000.0005            Infinity");
        TestUtils.testMultiLineToString(expected4, aLittleBitOfEverything);
        
        final String expected5 = String.join(System.lineSeparator(),
                                             " -1000000.0001     0.0           0.0         ",
                                             "  1000000.0002   -10.0002000002 10.0002000002",
                                             "  1000000.0003   100.0003                 NaN",
                                             "    -1000.0004 -1000.0004           -Infinity",
                                             "     1000.0005 10000.0005            Infinity");
        TestUtils.testMultiLineToString(expected5, aLittleBitOfEverything.toString(10)); // with lower bound
    }
    
    /** A simple test of {@link Matrix#doubleMatrixToStringMatrix(double[][], double)} with a lower-bound of zero
     * 
     * @see #testToString() */
    @Test
    void testDoubleMatrixToStringMatrix1() {
        final double[][] m1 = { { -1_000_000.0001,      1.00000001,              -1.00000001   },
                                {  1_000_000.0002,    -10.0002000002,            10.0002000002 },
                                {  1_000_000.0003,    100.0003,                     Double.NaN },
                                { -1_000.0004,      -1000.0004,       Double.NEGATIVE_INFINITY },
                                {  1_000.0005,      10000.0005,       Double.POSITIVE_INFINITY } };
        
        final String[][] expected1 = { { " -1000000.0001", "     1.00000001  ", " -1.00000001  ", },
                                       { "  1000000.0002", "   -10.0002000002", " 10.0002000002", },
                                       { "  1000000.0003", "   100.0003      ", "           NaN", },
                                       { "    -1000.0004", " -1000.0004      ", "     -Infinity", },
                                       { "     1000.0005", " 10000.0005      ", "      Infinity"  } };
        
        final String[][] mStr1 = Matrix.doubleMatrixToStringMatrix(m1, 0.0);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                assertEquals(expected1[i][j], mStr1[i][j]);
            }
        }
        
        // hit a few special cases:
        final double[][] m2 = { { 1000000000000000000L, 10.1 },
                                { 2,                    Double.NEGATIVE_INFINITY } };
        final String[][] expected2 = { { " 1000000000000000000", "        10.1" },
                                       { "                   2", "   -Infinity" }};
        
        final String[][] mStr2 = Matrix.doubleMatrixToStringMatrix(m2, 0.0);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                assertEquals(expected2[i][j], mStr2[i][j]);
            }
        }
    }
    
    /** The same as {@link #testDoubleMatrixToStringMatrix1()}, but with a non-zero lower-bound<br><br>
     * 
     * We use a lower bound of {@code 10}, which is obviously too large in practice, but it's fine for a test
     * 
     * @see #testToString() */
    @Test
    void testDoubleMatrixToStringMatrix2() {
        final double[][] m1 = { { -1_000_000.0001,      1.00000001,              -1.00000001   },
                                {  1_000_000.0002,    -10.0002000002,            10.0002000002 },
                                {  1_000_000.0003,    100.0003,                     Double.NaN },
                                { -1_000.0004,      -1000.0004,       Double.NEGATIVE_INFINITY },
                                {  1_000.0005,      10000.0005,       Double.POSITIVE_INFINITY } };
        
        final String[][] expected1 = { { " -1000000.0001", "     0.0         ", "  0.0         " },
                                       { "  1000000.0002", "   -10.0002000002", " 10.0002000002" },
                                       { "  1000000.0003", "   100.0003      ", "           NaN" },
                                       { "    -1000.0004", " -1000.0004      ", "     -Infinity" },
                                       { "     1000.0005", " 10000.0005      ", "      Infinity" } };
        
        final String[][] mStr1 = Matrix.doubleMatrixToStringMatrix(m1, 10.0);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                assertEquals(expected1[i][j], mStr1[i][j]);
            }
        }
        
        // hit a few special cases:
        final double[][] m2 = { { 1000000000000000000L, 10.1 },
                                { 2,                    Double.NEGATIVE_INFINITY } };
        final String[][] expected2 = { { " 1000000000000000000", "        10.1" },
                                       { "                   0", "   -Infinity" }};
        
        final String[][] mStr2 = Matrix.doubleMatrixToStringMatrix(m2, 10.0);
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                assertEquals(expected2[i][j], mStr2[i][j]);
            }
        }
    }
    
    /**
     * Test {@link Matrix#absEquivalentTo(RealMatrix, double)}
     */
    @Test
    void testAbsEquivalentTo() {
        final Matrix m1 = new Matrix(new double[][] { { 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0 },
                                                      { 0, 0, 0, 0, 0 } });
        
        final Matrix m2 = new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                      { 1, 1, 1, 1, 1 },
                                                      { 1, 1, 1, 1, 1 },
                                                      { 1, 1, 1, 1, 1 },
                                                      { 1, 1, 1, 1, 1 } });
        
        assertTrue(m1.absEquivalentTo(m1, 0.0)); // covers "same" check
        assertTrue(m1.absEquivalentTo(m2, 1e-0));
        assertFalse(m1.absEquivalentTo(m2, 1e-1));
        assertFalse(m1.absEquivalentTo(SKINNY_MATRIX, 1e-1)); // different dimensions
    }
    
    /**
     * Test {@link Matrix#diagonal(double[])}
     */
    @Test
    void testDiagonal() {
        final Matrix expected = new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                            { 0, 2, 0, 0, 0 },
                                                            { 0, 0, 3, 0, 0 },
                                                            { 0, 0, 0, 4, 0 },
                                                            { 0, 0, 0, 0, 5 } });
        final Matrix actual = Matrix.diagonal(new double[] { 1, 2, 3, 4, 5 });
        assertMatrixEquals(expected, actual, 0.0);
    }
    
    /**
     * Test the simple overrides that exist for convenience
     */
    @Test
    void testSimpleOverrides() {
        final Matrix m1 = new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                      { 0, 2, 0, 0, 0 },
                                                      { 0, 0, 3, 0, 0 },
                                                      { 0, 0, 0, 4, 0 },
                                                      { 0, 0, 0, 0, 5 } });
        final RealMatrix m2 = new Matrix(m1, false);
        
        assertEquals(m1, m1.copy());
        assertNotSame(m1, m1.copy());
        
        final Matrix mT = m1.transpose();
        final Matrix add1 = m1.add(m2);
        final Matrix add2 = m1.add(m1);
        final Matrix times1 = m1.multiply(m2);
        final Matrix times2 = m1.multiply(m1);
        final Matrix timesT1 = m1.transposeMultiply(m2);
        final Matrix timesT2 = m1.transposeMultiply(m1);
        final Matrix scalarMult = m1.scalarMultiply(2);
        
        final Matrix m1Minusm11 = m1.subtract(m1);
        final Matrix m1Minusm12 = m1.subtract((RealMatrix) m1);
        
        final Matrix m1ToThe3rd = m1.power(3);
        
        assertEquals(new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                 { 0, 2, 0, 0, 0 },
                                                 { 0, 0, 3, 0, 0 },
                                                 { 0, 0, 0, 4, 0 },
                                                 { 0, 0, 0, 0, 5 } }), mT);
        assertEquals(new Matrix(new double[][] { { 2, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 6, 0, 0 },
                                                 { 0, 0, 0, 8, 0 },
                                                 { 0, 0, 0, 0, 10 } }), add1);
        assertEquals(new Matrix(new double[][] { { 2, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 6, 0, 0 },
                                                 { 0, 0, 0, 8, 0 },
                                                 { 0, 0, 0, 0, 10 } }), add2);
        assertEquals(new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 9, 0, 0 },
                                                 { 0, 0, 0, 16, 0 },
                                                 { 0, 0, 0, 0, 25 } }), times1);
        assertEquals(new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 9, 0, 0 },
                                                 { 0, 0, 0, 16, 0 },
                                                 { 0, 0, 0, 0, 25 } }), times2);
        assertEquals(new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 9, 0, 0 },
                                                 { 0, 0, 0, 16, 0 },
                                                 { 0, 0, 0, 0, 25 } }), timesT1);
        assertEquals(new Matrix(new double[][] { { 1, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 9, 0, 0 },
                                                 { 0, 0, 0, 16, 0 },
                                                 { 0, 0, 0, 0, 25 } }), timesT2);
        assertEquals(new Matrix(new double[][] { { 2, 0, 0, 0, 0 },
                                                 { 0, 4, 0, 0, 0 },
                                                 { 0, 0, 6, 0, 0 },
                                                 { 0, 0, 0, 8, 0 },
                                                 { 0, 0, 0, 0, 10 } }), scalarMult);
        
        assertEquals(new Matrix(new double[][] { { 0, 0, 0, 0, 0 },
                                                 { 0, 0, 0, 0, 0 },
                                                 { 0, 0, 0, 0, 0 },
                                                 { 0, 0, 0, 0, 0 },
                                                 { 0, 0, 0, 0, 0 } }),
                     m1Minusm11);
        assertEquals(m1Minusm11, m1Minusm12);
        
        assertEquals(m1.transpose().multiply(m1), m1.transposeMultiply(m1));
        assertEquals(m1.transpose().multiply(m2), m1.transposeMultiply(m2));
        
        final NVector v = new NVector(1, 1, 1, 1, 1);
        assertEquals(new NVector(1, 2, 3, 4, 5), m1.operate(v));
        assertEquals(m1.transpose().operate(v), m1.operateTranspose(v));
        
        assertEquals(new Matrix(new double[][] { { 1, 0,  0,  0,   0 },
                                                 { 0, 8,  0,  0,   0 },
                                                 { 0, 0, 27,  0,   0 },
                                                 { 0, 0,  0, 64,   0 },
                                                 { 0, 0,  0,  0, 125 } }),
                     m1ToThe3rd);
        
        assertEquals(new Array2DRowRealMatrix(m1.getData()).preMultiply(v), m1.preMultiply(v));
    }
    
    /**
     * Assert that two matrices are equal within a tolerance
     * 
     * @param expected the expected {@link Matrix}
     * @param actual the actual {@link Matrix}
     * @param tolerance the entry equality tolerance
     */
    public static void assertMatrixEquals(final RealMatrix expected, final RealMatrix actual, final double tolerance) {
        assertMatrixEquals(expected, actual, tolerance, null);
    }
    
    /** Assert that two matrices are equal within a tolerance
     * 
     * @param expected the expected {@link Matrix}
     * @param actual the actual {@link Matrix}
     * @param tolerance the entry equality tolerance
     * @param addlInfoSup a {@link Supplier} of additional information for the JUnit failure-string in case of a
     *        failure */
    public static void assertMatrixEquals(final RealMatrix expected,
                                          final RealMatrix actual,
                                          final double tolerance,
                                          final Supplier<String> addlInfoSup) {
        // wrap in `RealMatrix`s in `new Matrix`s access to the equals method, and a better toString implementation
        
        final Matrix E = new Matrix(expected, false);
        final Matrix A = new Matrix(actual, false);
        
        assertTrue(E.absEquivalentTo(A, tolerance), () -> computeMatrixEqualFailedMsg(E, A, addlInfoSup));
    }
    
    /** If the assertion in {@link #assertMatrixEquals(RealMatrix, RealMatrix, double, Supplier)} fails, use this to
     * generate a message.
     * 
     * @param E the expected {@link Matrix}
     * @param A the actual {@link Matrix}
     * @param addlInfoSup a {@link Supplier} of additional information for the JUnit failure-string in case of a failure
     * @return the failure string */
    private static String computeMatrixEqualFailedMsg(final Matrix E,
                                                      final Matrix A,
                                                      final Supplier<String> addlInfoSup) {
        final Matrix error = A.subtract(E);
        
        double worstReError = 0.0;
        for (int i = 0; i < error.getRowDimension(); i++) {
            for (int j = 0; j < error.getColumnDimension(); j++) {
                
                final double ij = error.getEntry(i, j);
                worstReError = FastMath.max(worstReError, FastMath.abs(ij));
            }
        }
        
        final StringBuilder errMsg = new StringBuilder();
        
        final String addlInfo = addlInfoSup == null ? "" : addlInfoSup.get();
        if (!addlInfo.isEmpty()) {
            errMsg.append(addlInfo).append(System.lineSeparator());
        }
        
        errMsg.append("Error:")
              .append(System.lineSeparator())
              .append(error)
              .append(System.lineSeparator())
              .append("Worst error: ")
              .append(worstReError);
        return errMsg.toString();
    }
}
