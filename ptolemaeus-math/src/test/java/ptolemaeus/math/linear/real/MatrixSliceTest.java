/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.real;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.hipparchus.linear.RealMatrix;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link MatrixSlice}
 * @author Peter Davis, peter.m.davis@lmco.com
 */
class MatrixSliceTest {

    /**
     * The identity matrix of size 4x4
     */
    private final Matrix I4 = Matrix.identity(4);

    /**
     * Tests {@link MatrixSlice#getRowDimension()} and {@link MatrixSlice#getColumnDimension()}
     */
    @Test
    void testDimensions() {

        //slice which is the inner two rows/cols of I4
        MatrixSlice slice = new MatrixSlice(I4, 1, 2, 1, 2);
        assertAll(
            () -> assertEquals(2, slice.getRowDimension()),
            () -> assertEquals(2, slice.getColumnDimension())
        );

        MatrixSlice slice2 = new MatrixSlice(I4, new int[]{0, 1, 3}, new int[]{2, 3});
        assertAll(
            () -> assertEquals(3, slice2.getRowDimension()),
            () -> assertEquals(2, slice2.getColumnDimension())
        );
    }

    /**
     * Tests {@link MatrixSlice} is read-through and write-back
     */
    @Test
    void testReadThroughAndWriteBack() {
        //read-through: changes to underlying matrix change the slice
        MatrixSlice slice = new MatrixSlice(I4, 1, 2, 1, 2);
        I4.setEntry(1, 1, 42.0);
        assertEquals(42.0, slice.getEntry(0, 0));

        //write-back: changes to the slice change the underlying matrix
        slice.setEntry(1, 0, 1.0);
        assertEquals(1.0, I4.getEntry(2, 1));
    }

    /**
     * Assert all unsupported methods throw an {@link UnsupportedOperationException}:<br>
     * <ul>
     *  <li> {@link MatrixSlice#getDataRef()}</li>
     *  <li> {@link MatrixSlice#getSubMatrix(int, int, int, int)}</li>
     *  <li> {@link MatrixSlice#getSubMatrix(int[], int[])}</li>
     * </ul>
     */
    @Test
    void testUnsupported() {
        MatrixSlice slice = new MatrixSlice(I4, 1, 2, 1, 2);
        assertAll(
            () -> assertThrows(UnsupportedOperationException.class, () -> slice.getDataRef()),
            () -> assertThrows(UnsupportedOperationException.class, () -> slice.createMatrix(0, 0))
        );
    }

    /**
     * Tests {@link MatrixSlice#getSubMatrix(int, int, int, int)} and {@link MatrixSlice#getSubMatrix(int[], int[])}
     */
    @Test
    void testGetSubRange() {
        MatrixSlice slice = new MatrixSlice(I4, 0, 2, 0, 2);
        Matrix subRange = slice.getSubMatrix(0, 1, 0, 1);

        Matrix expectedSubRange = I4.getSubMatrix(0, 1, 0, 1);
        assertEquals(expectedSubRange, subRange);

        Matrix slice2 = new MatrixSlice(I4, 1, 3, 1, 3);
        RealMatrix expectedSubRange2 = I4.getSubMatrix(new int[]{2, 3}, new int[]{2, 3});
        RealMatrix subRange2 = slice2.getSubMatrix(new int[]{1, 2}, new int[]{1, 2});
        assertEquals(expectedSubRange2, subRange2);
    }

    /**
     * Tests {@link MatrixSlice#copy()}
     */
    @Test
    void testCopy() {
        I4.setEntry(1, 2, 10);
        MatrixSlice slice = new MatrixSlice(I4, 1, 2, 1, 2);
        Matrix copy = slice.copy();
        assertNotSame(slice, copy);
        assertNotSame(I4, copy);
        Matrix expected = new Matrix(new double[][]{{1.0, 10.0}, {0.0, 1.0}});
        assertEquals(expected, copy);
    }

    /**
     * Tests {@link MatrixSlice#addToEntry(int, int, double)} and {@link MatrixSlice#multiplyEntry(int, int, double)}
     */
    @Test
    void testAddAndMultiplyToEntry() {
        MatrixSlice slice = new MatrixSlice(I4, 0, 1, 0, 1);
        slice.addToEntry(0, 0, 10.0);
        assertEquals(11.0, slice.getEntry(0, 0));

        slice.multiplyEntry(0, 0, 5);
        assertEquals(55.0, slice.getEntry(0, 0));
    }

    /**
     * Tests {@link MatrixSlice#equals(Object)} and {@link MatrixSlice#hashCode()}
     */
    @Test
    void testEqualsAndHashCode() {
        MatrixSlice slice = new MatrixSlice(I4, 0, 1, 0, 1);
        assertNotNull(slice.hashCode()); // coverage call
        assertNotEquals(slice, null);
        assertNotEquals(slice, Double.valueOf(2.0));
        assertEquals(slice, slice);
        assertNotEquals(slice, new MatrixSlice(I4, 0, 2, 0, 1));
        assertNotEquals(slice, new MatrixSlice(I4, 0, 1, 0, 2));
    }
}
