/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.real;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.ArrayRealVector;
import org.hipparchus.linear.OpenMapRealVector;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.MathArrays;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.CountableDoubleIterable;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.realdomains.NPoint;

/**
 * Tests {@link NVector}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class NVectorTest {
    
    /** @param dim the dimension of the {@link NVector}
     * @param maxNorm the max {@link NVector#getNorm()} of the {@link NVector}
     * @param randomSeed a random seed
     * @return a new random {@link NVector} */
    public static NVector randomNVector(final int dim, final double maxNorm, final long randomSeed) {
        final double[] randomComponents = CountableDoubleIterable.monteCarlo(0.0, 1.0, randomSeed, dim)
                                                                 .stream()
                                                                 .toArray();
        
        return new NVector(randomComponents).normalized().times(maxNorm);
    }
    
    /** Test {@link NVector#getMagnitude()} with an {@link NVector} with a huge norm.<br>
     * <br>
     * 
     * The norm is too large to be squared which would normally result in {@code norm = sqrt(Inf) = Inf}, but we can
     * detect this and use {@link MathArrays#safeNorm(double[])} to (sometimes) prevent overflow to Infinity. */
    @Test
    void testHugeMagnitude() {
        /* HUGE is the largest finite double, so sqrt(HUGE) is the largest number whose square is finite and veryLarge
         * is too large to be squared without overflow */
        final double  veryLarge = FastMath.sqrt(Numerics.HUGE) * 1.1;
        final NVector v         = new NVector(1, 1, 1).normalized().times(veryLarge);
        
        assertEquals(Double.POSITIVE_INFINITY, v.magnitudeSquared()); // each element is squared and summed -> overflow
        assertEquals(veryLarge, v.getMagnitude());
        assertEquals(veryLarge, v.getLength());
        assertEquals(veryLarge, v.getNorm());
    }
    
    /** Test {@link NVector#getMagnitude()} with an {@link NVector} with a tiny norm.<br>
     * <br>
     * 
     * The norm is too small to be squared which would normally result in {@code norm = sqrt(0) = 0}, but we can detect
     * this and use {@link MathArrays#safeNorm(double[])} to (sometimes) prevent underflow to 0.0. */
    @Test
    void testTinyMagnitude() {
        /* verySmall is the smallest number whose square is non-zero */
        final double  verySmall = FastMath.sqrt(Double.MIN_VALUE);
        final NVector v         = new NVector(1, 1, 1).normalized().times(verySmall);
        
        assertEquals(0.0, v.magnitudeSquared()); // each element is squared and summed -> underflow
        assertEquals(verySmall, v.getMagnitude());
        assertEquals(verySmall, v.getLength());
        assertEquals(verySmall, v.getNorm());
    }
    
    /** Test {@link NVector#of(RealVector)} */
    @Test
    void testOf() {
        final NVector           v     = new NVector(1, 2, 3, 4, 5);
        final ArrayRealVector   copy1 = new ArrayRealVector(v);
        final OpenMapRealVector copy2 = new OpenMapRealVector(v);
        
        assertSame(v, NVector.of(v));
        NumericsTest.assertEBEVectorsEqualAbsolute(v, copy1, 0.0);
        NumericsTest.assertEBEVectorsEqualAbsolute(v, copy2, 0.0);
    }
    
    /** Test {@link NVector#copy()} */
    @Test
    void testCopy() {
        final NVector vect = new NVector(1, 2, 3, 4, 5);
        final NVector copy = vect.copy();
        assertEquals(vect, copy);
        assertNotSame(vect, copy);
        assertNotSame(vect.getDataRef(), copy.getDataRef());
    }
    
    /**
     * Test {@link NVector#outerProduct(RealVector)}
     */
    @Test
    void testOuterProduct() {
        final NVector v1 = new NVector(2, 3, 5, 7);
        final NVector v2 = new NVector(11, 13, 17, 19);
        
        final Matrix op = v1.outerProduct(v2);
        final String expected = String.join(System.lineSeparator(),
                                            " 22 26  34  38",
                                            " 33 39  51  57",
                                            " 55 65  85  95",
                                            " 77 91 119 133");
        TestUtils.testMultiLineToString(expected, op);
    }
    
    /** Test {@link NVector#toString()}, {@link NVector#toString(String)}, and
     * {@link NVector#toString(String, String, String, String)} */
    @Test
    void testToStrings() {
        final NVector vector = new NVector(0.754788115099576,
                                           0.695823752081075,
                                           0.18752350142414875,
                                           0.1609720885154029);
        
        final String expected1 = "<0.754788115099576, 0.695823752081075, 0.18752350142414875, 0.1609720885154029>";
        final String expected2 = "<0.755, 0.696, 0.188, 0.161>";
        final String expected3 = "[0.7548; 0.6958; 0.1875; 0.1610]";
        
        assertEquals(expected1, vector.toString());
        assertEquals(expected2, vector.toString("%.3f"));
        assertEquals(expected3, vector.toString("%.4f", "; ", "[", "]"));
        
        final String expected = String.join(System.lineSeparator(),
                                            " 0.754788115099576  ",
                                            " 0.695823752081075  ",
                                            " 0.18752350142414875",
                                            " 0.1609720885154029 ");
        TestUtils.testMultiLineToString(expected, vector.toColumnString());
    }
    
    /**
     * Test {@link NVector#getZero(int)}
     */
    @Test
    void testGetZero() {
        final NVector zero1 = NVector.getZero(1);
        final NVector zero2 = NVector.getZero(2);
        final NVector zero9 = NVector.getZero(9);
        
        assertEquals("<0.0>", zero1.toString());
        assertEquals("<0.0, 0.0>", zero2.toString());
        assertEquals("<0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0>", zero9.toString());
        
        assertNotSame(zero9, NVector.getZero(9));
    }
    
    /**
     * Test {@link NVector#plus(NVector)}
     */
    @Test
    void testPlus() {
        final NVector vector1 = new NVector(2, 3, 5, 7);
        final NVector vector2 = new NVector(11, 13, 17, 19);
        assertEquals(new NVector(13, 16, 22, 26), vector1.plus(vector2));
    }
    
    /**
     * Test {@link NVector#minus(NVector)}
     */
    @Test
    void testMinus() {
        final NVector vector1 = new NVector(2, 3, 5, 7);
        final NVector vector2 = new NVector(11, 13, 17, 19);
        assertEquals(new NVector(9, 10, 12, 12), vector2.minus(vector1));
    }
    
    /**
     * Test {@link NVector#times(double)}
     */
    @Test
    void testTimes() {
        final NVector vector = new NVector(2, 3, 5, 7);
        assertEquals(new NVector(20, 30, 50, 70), vector.times(10));
    }
    
    /** Test the {@link NVector} constructors and other basics */
    @Test
    void testBasics() {
        final NVector vector1 = new NVector(new NPoint(1, 1, 1, 1));
        assertEquals(new NPoint(1, 1, 1, 1), vector1.getPoint());
        
        final NVector vectorDifferent1 = new NVector(new ArrayRealVector(new double[] { 1, 1, 1, 1 }));
        assertEquals(new NPoint(1, 1, 1, 1), vectorDifferent1.getPoint());
        
        final NVector vectorDifferent2 = new NVector(vectorDifferent1, true);
        final NVector vectorDifferent3 = new NVector(vectorDifferent1, false);
        
        assertNotSame(vectorDifferent1.getDataRef(), vectorDifferent2.getDataRef());
        assertSame(vectorDifferent1.getDataRef(),    vectorDifferent3.getDataRef());
        
        final double[] someData = new double[] { 1, 2, 3, 4, 5 };
        final NVector vectorDifferent4 = new NVector(someData, true);
        final NVector vectorDifferent5 = new NVector(someData, false);
        
        assertNotSame(someData, vectorDifferent4.getDataRef());
        assertSame(someData,    vectorDifferent5.getDataRef());
        
        assertEquals(1, vector1.getDimensionality());
        assertEquals(4, vector1.getEmbeddedDimensionality());
        
        assertEquals(NPoint.getZero(4), vector1.getEndPoint1());
        assertEquals(new NPoint(1, 1, 1, 1), vector1.getEndPoint2());
        
        assertEquals(2081298305, vector1.hashCode());
        
        final NVector vector2 = new NVector(2, 2, 2, 2);
        assertEquals(new NPoint(2, 2, 2, 2), vector2.getPoint());
        
        assertEquals(4.0, vector2.getMagnitude());
        assertEquals(16.0, vector2.magnitudeSquared());
        
        final NVector vector3 = new NVector(10, -10, -10, 10);
        assertEquals(new NVector(1, -1, -1, 1), vector3.signum());
        
        assertEquals("<1.0, -1.0, -1.0, 1.0>", vector3.signum().toString());
        
        assertEquals(10, vector3.getComponent(0));
        assertEquals(-10, vector3.getComponent(1));
        assertEquals(-10, vector3.getComponent(2));
        assertEquals(10, vector3.getComponent(3));
        
        assertArrayEquals(new double[] { 10, -10, -10, 10 }, vector3.getComponents());
        
        assertEquals(20, vector3.getLength());
        assertEquals(vector3.getMagnitude(), vector3.getLength());
        assertEquals(400, vector3.magnitudeSquared());
        
        assertEquals(vector1, vector1);
        assertNotEquals(vector1, vector2);
        assertNotEquals(vector1, null);
        assertNotEquals(vector1, new Object());
    }
    
    /**
     * Test {@link NVector#dot(NVector)}
     */
    @Test
    void testDot() {
        final NVector vector1 = new NVector(1, 0, 0, 0);
        final NVector orthogonal = new NVector(0, 0, 0, 1);
        assertEquals(0, vector1.dot(orthogonal));
        
        final NVector vector2 = new NVector(0, 0, 0, 10);
        final NVector codirectional = new NVector(0, 0, 0, 10);
        assertEquals(100, vector2.dot(codirectional));
        
        final NVector vector3 = new NVector(1, 2, 3, 4);
        final NVector vector4 = new NVector(2, 3, 4, 5);
        assertEquals(40, vector3.dot(vector4)); // 1x2 + 2x3 + 3x4 + 4x5 = 2 + 6 + 12 + 20 = 40
    }
    
    /**
     * Test {@link NVector#normalized()}
     */
    @Test
    void testNormalized() {
        final NVector vector = new NVector(10, 10, 10, 10);
        final NVector normalized = new NVector(0.5, 0.5, 0.5, 0.5);
        assertEquals(normalized, vector.normalized());
    }
}