/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.balancing;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.eigen.EigenTest;
import ptolemaeus.math.linear.householderreductions.ComplexHessenbergReductionTest;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;
import ptolemaeus.math.linear.schur.EigenvectorFailureMode;
import ptolemaeus.math.linear.schur.MATLABSchurIO;

/** Tests for {@link MatrixBalancer}. See also {@link EigenTest#testBalancing()} where we check the error in the
 * Eigenvalues with and without balancing.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class MatrixBalancerTest {
    
    /** Show that performing balancing on a matrix that's already balanced has no effect */
    @Test
    void testAlreadyBalanced() {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(100, 100, 100.0, 8675_309L, false);
        
        final ComplexMatrix ABalanced1 = MatrixBalancer.balance(A,                 null).getBalanced();
        final ComplexMatrix ABalanced2 = MatrixBalancer.balance(A,     BalanceMode.NONE).getBalanced();
        final ComplexMatrix ABalanced3 = MatrixBalancer.balance(A, BalanceMode.ONE_NORM).getBalanced();
        final ComplexMatrix ABalanced4 = MatrixBalancer.balance(A, BalanceMode.TWO_NORM).getBalanced();
        
        assertEquals(A, ABalanced1);
        assertEquals(A, ABalanced2);
        assertEquals(A, ABalanced3);
        assertEquals(A, ABalanced4);
    }
    
    /** Demonstrate the effects of balancing an unbalanced matrix. In this case, the matrix is a random complex matrix
     * where one entry has been multiplied by {@code 1e9}. Balancing reduces the norm by a huge factor. <br>
     * <br>
     * 
     * {@link EigenTest#testBalancing()} shows that the Eigenvalues remain unchanged (i.e., show that balancing is
     * indeed a similarity transform). */
    @Test
    void testBalancing() {
        final MATLABSchurIO mlSchur = MATLABSchurIO.fromFolder("unbalanced");
        final ComplexMatrix A = mlSchur.A();
        
        final ComplexMatrix  ABalanced1 = MatrixBalancer.balance(A, BalanceMode.ONE_NORM).getBalanced();
        final ComplexMatrix  ABalanced2 = MatrixBalancer.balance(A, BalanceMode.TWO_NORM).getBalanced();
        
        final double preBalancedFrob   = A.getFrobeniusNorm();
        final double postBalancedFrob1 = ABalanced1.getFrobeniusNorm();
        final double postBalancedFrob2 = ABalanced2.getFrobeniusNorm();
        
        final double ratio1           = preBalancedFrob / postBalancedFrob1;
        final double ratio2           = preBalancedFrob / postBalancedFrob2;
        final double minExpectedRatio = 15_000.0;
        
        assertAll(() -> assertEquals(8.133707216905066E9, preBalancedFrob),
                  () -> assertEquals(534663.8539229519,   postBalancedFrob1),
                  () -> assertEquals(467626.3384704672,   postBalancedFrob2), // the 2-norm mode does a better job here
                  () -> TestUtils.assertGreaterThan(ratio1, minExpectedRatio,
                                                    () -> ("The change in the Frobenius norm (2-norm) due to "
                                                           + "balancing was not as great as expected!%n"
                                                           + "was      = %s%n"
                                                           + "expected > %s%n").formatted(ratio1, minExpectedRatio)),
                  () -> TestUtils.assertGreaterThan(ratio2, minExpectedRatio,
                                                    () -> ("The change in the Frobenius norm (2-norm) due to "
                                                           + "balancing was not as great as expected!%n"
                                                           + "was      = %s%n"
                                                           + "expected > %s%n").formatted(ratio2, minExpectedRatio)));
    }
    
    /** Demonstrate the effects of balancing an extremely unbalanced matrix. In this case, the matrix is a random
     * complex matrix where one entry has been multiplied by {@code 1e20}. Balancing reduces the norm by a huge factor.
     * <br>
     * <br>
     * 
     * In addition, show that a second pass at balancing has no effect; i.e., a balanced matrix is balanced.<br>
     * <br>
     * 
     * {@link EigenTest#testExtremeBalancing()} shows that the Eigenvalues remain unchanged when the matrix is balanced
     * (i.e., show that balancing is indeed a similarity transform). In addition, it shows that the Eigenvalues that are
     * computed without balancing are essentially meaningless due to their inaccuracy. */
    @Test
    void testExtremeBalancing() {
        final MATLABSchurIO mlSchur = MATLABSchurIO.fromFolder("extremelyunbalanced");
        final ComplexMatrix A = mlSchur.A();
        
        final ComplexMatrix ABalanced1 = MatrixBalancer.balance(A, BalanceMode.ONE_NORM).getBalanced();
        final ComplexMatrix ABalanced2 = MatrixBalancer.balance(A, BalanceMode.TWO_NORM).getBalanced();
        
        final double preBalancedFrob   = A.getFrobeniusNorm();
        final double postBalancedFrob1 = ABalanced1.getFrobeniusNorm();
        final double postBalancedFrob2 = ABalanced2.getFrobeniusNorm();
        
        assertEquals(8.133707216905043E20,  preBalancedFrob);
        assertEquals(1.4041698881843097E11, postBalancedFrob1);
        assertEquals(1.4041698881811234E11, postBalancedFrob2); // the modes do about the same (very good) job here
        
        final double ratio1           = preBalancedFrob / postBalancedFrob1;
        final double ratio2           = preBalancedFrob / postBalancedFrob2;
        final double minExpectedRatio = 5_700_000_000.0; // 5.7 billion x reduced norm!
        
        assertAll(() -> assertEquals(8.133707216905043E20,  preBalancedFrob),
                  () -> assertEquals(1.4041698881843097E11, postBalancedFrob1),
                  () -> assertEquals(1.4041698881811234E11, postBalancedFrob2), // the 2-norm mode does a better job
                  () -> TestUtils.assertGreaterThan(ratio1, minExpectedRatio,
                                                    () -> ("The change in the Frobenius norm (2-norm) due to "
                                                           + "balancing was not as great as expected!%n"
                                                           + "was      = %s%n"
                                                           + "expected > %s%n").formatted(ratio1, minExpectedRatio)),
                  () -> TestUtils.assertGreaterThan(ratio2, minExpectedRatio,
                                                    () -> ("The change in the Frobenius norm (2-norm) due to "
                                                           + "balancing was not as great as expected!%n"
                                                           + "was      = %s%n"
                                                           + "expected > %s%n").formatted(ratio2, minExpectedRatio)));
        
        final ComplexMatrix rebalanced1 = MatrixBalancer.balance(ABalanced1, BalanceMode.ONE_NORM).getBalanced();
        assertEquals(ABalanced1, rebalanced1);
        
        final ComplexMatrix rebalanced2 = MatrixBalancer.balance(ABalanced2, BalanceMode.TWO_NORM).getBalanced();
        assertEquals(ABalanced2, rebalanced2);
    }
    
    /** Balancing in the 1-norm does a better job w.r.t. standard balancing here, but that doesn't necessarily speak to
     * the <em>Eigenvalue</em> condition number.  In either case, at least we don't ruin the Hessenberg structure. */
    @Test
    void testHessenberg() {
        final ComplexMatrix A = ComplexMatrixTest.randomUpperNHessenberg(10, 1, 1_000.0, 8675_309L, false);
        
        final ComplexMatrix ABalanced1 = MatrixBalancer.balance(A, BalanceMode.ONE_NORM).getBalanced();
        final ComplexMatrix ABalanced2 = MatrixBalancer.balance(A, BalanceMode.TWO_NORM).getBalanced();
        
        final double preBalancedFrob   = A.getFrobeniusNorm();
        final double postBalancedFrob1 = ABalanced1.getFrobeniusNorm();
        final double postBalancedFrob2 = ABalanced2.getFrobeniusNorm();
        
        assertEquals(2568.216269853221, preBalancedFrob);
        assertEquals(1988.9228025761404, postBalancedFrob1);
        assertEquals(2431.2661193663666, postBalancedFrob2);
        
        ComplexHessenbergReductionTest.assertIsNHessenberg(ABalanced1, 1, 0.0, 0.0);
        ComplexHessenbergReductionTest.assertIsNHessenberg(ABalanced2, 1, 0.0, 0.0);
    }
    
    /** Test {@link MatrixBalancer#getScale()},
     *       {@link MatrixBalancer#adjustEigenvectors(ComplexMatrix)}, and
     *       {@link MatrixBalancer#adjustEigenvectors(ComplexMatrix, double[])} */
    @Test
    void testScaleAndEigenvectorAdjustments() {
        final int dim = 5;
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(dim, dim, 100.0, 8675_309L, false);
        
        A.multiplyEntry(2, 3, 1e6);
        
        final MatrixBalancer bal1 = MatrixBalancer.balance(A, BalanceMode.ONE_NORM);
        final MatrixBalancer bal2 = MatrixBalancer.balance(A, BalanceMode.TWO_NORM);
        
        assertArrayEquality(new double[] { 16.0, 16.0,  512.0, 0.5, 16.0 }, bal1.getScale(), 0.0);
        assertArrayEquality(new double[] { 32.0, 32.0, 1024.0, 1.0, 32.0 }, bal2.getScale(), 0.0);
        
        final ComplexMatrix ABalanced1 = bal1.getBalanced();
        final ComplexMatrix ABalanced2 = bal2.getBalanced();
        
        final ComplexSchurDecomposition schurA     = ComplexSchurDecomposition.of(A);
        final ComplexSchurDecomposition schurABal1 = ComplexSchurDecomposition.of(ABalanced1);
        final ComplexSchurDecomposition schurABal2 = ComplexSchurDecomposition.of(ABalanced2);
        
        // CHECKSTYLE.OFF: LineLength
        final ComplexMatrix evecA     =     schurA.computeEigenvectors(A, 5, 0, LinearSolver.defaultSolver(), EigenvectorFailureMode.THROW).orElseThrow();
        final ComplexMatrix evecABal1 = schurABal1.computeEigenvectors(ABalanced1, 5, 0, LinearSolver.defaultSolver(), EigenvectorFailureMode.THROW).orElseThrow();
        final ComplexMatrix evecABal2 = schurABal2.computeEigenvectors(ABalanced2, 5, 0, LinearSolver.defaultSolver(), EigenvectorFailureMode.THROW).orElseThrow();
        // CHECKSTYLE.ON: LineLength
        
        /* Adjust the Eigenvector matrices in-place.
         * The instance method calls the static one, but we use both here for coverage */
        bal1.adjustEigenvectors(evecABal1);
        MatrixBalancer.adjustEigenvectors(evecABal2, bal2.getScale());
        
        final Complex[] eigsUnbalanced = schurA.getEigenvaluesArr();
        final Complex[] eigsBalanced1  = schurA.getEigenvaluesArr();
        final Complex[] eigsBalanced2  = schurA.getEigenvaluesArr();
        
        for (int i = 0; i < dim; i++) {
            EigenTest.assertEigenvector(A, eigsUnbalanced[i],     evecA.getColumnVector(i), 1e-4);
            EigenTest.assertEigenvector(A, eigsBalanced1[i],  evecABal1.getColumnVector(i), 1e-6);
            EigenTest.assertEigenvector(A, eigsBalanced2[i],  evecABal2.getColumnVector(i), 1e-6);
        }
        
        // be sure that we can handle zero-columns properly
        final ComplexMatrix withZeroCol = ComplexMatrixTest.randomComplexMatrix(dim, dim, 100.0, 123L, false);
        MatrixBalancer.adjustEigenvectors(withZeroCol, new double[] { 1, 1, 1, 1, 1 });
        
        withZeroCol.setColumnVector(3, ComplexVector.zero(dim));
        
        MatrixBalancer.adjustEigenvectors(withZeroCol, new double[] { 1, 1, 1, 1, 1 });
        
        for (int i = 0; i < dim; i++) {
            if (i == 3) {
                assertEquals(0.0, withZeroCol.getColumnVector(i).hermitianNorm(), 1e-12);
            }
            else {
                assertEquals(1.0, withZeroCol.getColumnVector(i).hermitianNorm(), 1e-12);
            }
        }
    }
}
