/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.eigen;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;
import static ptolemaeus.commons.TestUtils.assertGreaterThan;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.balancing.BalanceMode;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.schur.ComplexSchurDecomposition;
import ptolemaeus.math.linear.schur.ComplexSchurDecompositionTest;
import ptolemaeus.math.linear.schur.EigenvectorFailureMode;
import ptolemaeus.math.linear.schur.MATLABSchurIO;

/** Tests for the {@link Eigen} class. These tests are fairly superficial. See {@link ComplexSchurDecompositionTest} for
 * much more comprehensive testing of the internal functionality.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class EigenTest {
    
    /** Assert that {@link ComplexVector testVector} is indeed an Eigenvector of {@link ComplexMatrix A} for the given
     * {@link Complex Eigenvalue}.
     * 
     * @param A the {@link ComplexMatrix} for which we want {@code testVector} to be an Eigenvector
     * @param eigenvalue the Eigenvalue for which we want {@code testVector} to be an Eigenvector
     * @param testVector the {@link ComplexVector} under test 
     * @param epsilon the absolute tolerance when comparing double values */
    public static void assertEigenvector(final ComplexMatrix A,
                                         final Complex eigenvalue,
                                         final ComplexVector testVector,
                                         final double epsilon) {
        final ComplexVector scaled         = testVector.times(eigenvalue);
        final ComplexVector shouldBeScaled = A.operate(testVector);
        
        NumericsTest.assertEBEVectorsEqualAbsolute(scaled, shouldBeScaled, epsilon);
        
        if (!testVector.isZero()) {
            assertEquals(1.0, testVector.hermitianNorm(), epsilon); // assert normalized
            
            final double angleRadians = shouldBeScaled.hermitianAngleTo(testVector);
            assertEquals(0, angleRadians, epsilon); // assert parallel
        }
        
        final ComplexMatrix lambdaI       = ComplexMatrix.identity(A.getColumnDimension())
                                                         .scalarMultiply(eigenvalue);
        final ComplexMatrix AMinusLambdaI = A.minus(lambdaI);
        final ComplexVector shouldBeZero  = AMinusLambdaI.operate(testVector);
        
        assertEquals(0, shouldBeZero.hermitianNorm(), epsilon); // assert solves the Eigenvalue problem: Av - lv = 0
    }
    
    /** Test {@link Eigen} with a zero-matrix */
    @Test
    void testZeroMatrix() {
        final ComplexMatrix zero    = ComplexMatrix.zero(10, 10);
        final EigenSystem   zeroSys = Eigen.eigenSystem(zero);
        
        final String expected = String.join(System.lineSeparator(),
                                            "Eigenvalues:",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            " 0",
                                            "",
                                            "Eigenvectors:",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0",
                                            " 0 0 0 0 0 0 0 0 0 0");
        TestUtils.testMultiLineToString(expected, zeroSys);
    }
    
    /** Test the impact of balancing on the accuracy of Eigenvalues.<br>
     * <br>
     * 
     * Important note: this is predicated on the Eigenvalues from MATLAB being "truth", and MATLAB's results are - like
     * here - the result of both balancing and a Schur decomposition.<br>
     * <br>
     * 
     * The matrix used here was created using the following block:
     * 
     * <pre>
     * final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(100, 100, 100.0, 8675_309L, false);
     * A.multiplyEntry(37, 73, 1e9);
     * System.out.println(Numerics.toMATLABCode(A));
     * </pre>
     */
    @Test
    public void testBalancing() { // public only so that we can point to it in other JavaDoc
        final MATLABSchurIO mlSchur = MATLABSchurIO.fromFolder("unbalanced");
        
        final ComplexMatrix A = mlSchur.A();
        
        final Complex[]     matlabEigs    = ComplexMatrix.extractDiagonal(mlSchur.eigenvalues());
        final ComplexVector matlabEigsVec = ComplexVectorTest.sortedAsVector(matlabEigs);
        
        final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        
        final Complex[]     AEigs    = Eigen.eigenvalues(A,
                                                         BalanceMode.NONE,
                                                         maxIters,
                                                         null).toArray();
        final ComplexVector AEigsVec = ComplexVectorTest.sortedAsVector(AEigs);
        
        final Complex[]     ABalancedEigs1    =
                Eigen.eigenvalues(A, BalanceMode.ONE_NORM, maxIters, null).toArray();
        final ComplexVector ABalancedEigsVec1 = ComplexVectorTest.sortedAsVector(ABalancedEigs1);
        
        final Complex[]     ABalancedEigs2    =
                Eigen.eigenvalues(A, BalanceMode.TWO_NORM, maxIters, null).toArray();
        final ComplexVector ABalancedEigsVec2 = ComplexVectorTest.sortedAsVector(ABalancedEigs2);
        
        final ComplexVector unbalErrVec = matlabEigsVec.minus(AEigsVec);
        final ComplexVector balErrVec1  = matlabEigsVec.minus(ABalancedEigsVec1);
        final ComplexVector balErrVec2  = matlabEigsVec.minus(ABalancedEigsVec2);
        
        final double unBalErr = unbalErrVec.hermitianNorm();
        final double balErr1  = balErrVec1.hermitianNorm();
        final double balErr2  = balErrVec2.hermitianNorm();
        
        // test the actual values so demonstrate that the error is actually small, and not just huge regardless
        assertAll(() -> assertEquals(6.044462153953845E-6,    unBalErr),
                  () -> assertEquals(5.688244223748336E-10,   balErr1),
                  () -> assertEquals(3.7006009979695445E-10,  balErr2), // slightly better error balancing the 2-norm
                  
                  /* The unbalanced Eigenvalues are of poor quality. The actual values of the ratios are unimportant,
                   * the point is that the difference is large. */
                  () -> assertGreaterThan(unBalErr / balErr1, 1e4,
                          "The error in the Eigenvalues computed using the balanced matrix was larger than expected"),
                  () -> assertGreaterThan(unBalErr / balErr2, 1e4,
                          "The error in the Eigenvalues computed using the balanced matrix was larger than expected"));
    }
    
    /** Test the impact of balancing on the accuracy of Eigenvalues. In this case, the Eigenvalues computed using the
     * unbalanced matrix are essentially meaningless, with the error being {@code 1.1e15} times greater than when
     * balancing.<br>
     * <br>
     * 
     * Important note: this is predicated on the Eigenvalues from MATLAB being "truth", and MATLAB's results are - like
     * here - the result of both balancing and a Schur decomposition.<br>
     * <br>
     * 
     * The matrix used here was created using the following block:
     * 
     * <pre>
     * final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(100, 100, 100.0, 8675_309L, false);
     * A.multiplyEntry(37, 73, 1e20); // this one entry being made huge destroys the balance of A
     * System.out.println(Numerics.toMATLABCode(A));
     * </pre>
     */
    @Test
    public void testExtremeBalancing() { // public only so that we can point to it in other JavaDoc
        final MATLABSchurIO mlSchur = MATLABSchurIO.fromFolder("extremelyunbalanced");
        
        final ComplexMatrix A = mlSchur.A();
        
        final Complex[]     matlabEigs    = ComplexMatrix.extractDiagonal(mlSchur.eigenvalues());
        final ComplexVector matlabEigsVec = ComplexVectorTest.sortedAsVector(matlabEigs);
        
        final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        
        final Complex[]     AEigs    = Eigen.eigenvalues(A,
                                                         BalanceMode.NONE,
                                                         maxIters,
                                                         null).toArray();
        final ComplexVector AEigsVec = ComplexVectorTest.sortedAsVector(AEigs);
        
        final Complex[]     ABalancedEigs1    =
                Eigen.eigenvalues(A, BalanceMode.ONE_NORM, maxIters, null).toArray();
        final ComplexVector ABalancedEigsVec1 = ComplexVectorTest.sortedAsVector(ABalancedEigs1);
        
        final Complex[]     ABalancedEigs2    =
                Eigen.eigenvalues(A, BalanceMode.TWO_NORM, maxIters, null).toArray();
        final ComplexVector ABalancedEigsVec2 = ComplexVectorTest.sortedAsVector(ABalancedEigs2);
        
        final ComplexVector unbalErrVec = matlabEigsVec.minus(AEigsVec);
        final ComplexVector balErrVec1  = matlabEigsVec.minus(ABalancedEigsVec1);
        final ComplexVector balErrVec2  = matlabEigsVec.minus(ABalancedEigsVec2);
        
        final double unBalErr = unbalErrVec.hermitianNorm();
        final double balErr1  = balErrVec1.hermitianNorm();
        final double balErr2  = balErrVec2.hermitianNorm();
        
        // test the actual values so demonstrate that the error is actually small, and not just huge regardless
        assertAll(() -> assertEquals(4.232975161360473E11,  unBalErr),
                  () -> assertEquals(1.5820649240941428E-4, balErr1),
                  () -> assertEquals(7.629394566545646E-5,  balErr2),  // better error balancing the 2-norm
                  
                  /* The unbalanced Eigenvalues are essentially meaningless. The actual values of the ratios are
                   * unimportant, the point is that the difference is huge. */
                  () -> assertGreaterThan(unBalErr / balErr1, 1e15,
                          "The error in the Eigenvalues computed using the balanced matrix was larger than expected"),
                  () -> assertGreaterThan(unBalErr / balErr2, 1e15,
                          "The error in the Eigenvalues computed using the balanced matrix was larger than expected"));
    }
    
    /** Test {@link Eigen#eigenSystem(RealMatrix)},
     *       {@link Eigen#eigenSystem(FieldMatrix)}, and
     *       {@link Eigen#eigenSystem(ComplexMatrix, BalanceMode, int, int, double, LinearSolver, 
     *                                Consumer, EigenvectorFailureMode)} */
    @Test
    void testEigenSystem() {
        final double[][] AData = new double[][] { { 1, 0, 0, 0 },
                                                  { 0, 2, 0, 0 },
                                                  { 0, 0, 3, 0 },
                                                  { 0, 0, 0, 4 } };
        
        final Matrix        realA             = new Matrix(AData);
        final ComplexMatrix complexA          = new ComplexMatrix(AData);
        final EigenSystem   eigs1             = Eigen.eigenSystem(realA);
        final EigenSystem   eigs2             = Eigen.eigenSystem(complexA);
        final ComplexMatrix expectedEigVecMat = new ComplexMatrix(new double[][] { { 1, 0,  0, 0 },
                                                                                   { 0, 1,  0, 0 },
                                                                                   { 0, 0,  1, 0 },
                                                                                   { 0, 0,  0, 1 } });
        
        ComplexMatrixTest.assertAbsEquals(expectedEigVecMat, eigs1.eigenvectorMatrix(), 1e-12, 0.0);
        ComplexMatrixTest.assertAbsEquals(expectedEigVecMat, eigs2.eigenvectorMatrix(), 1e-12, 0.0);
        
        final AtomicLong  counter = new AtomicLong(0L);
        final EigenSystem eigs3   = Eigen.eigenSystem(complexA,
                                                      BalanceMode.TWO_NORM,
                                                      120,
                                                      10,
                                                      0.0,
                                                      LinearSolver.defaultSolver(),
                                                      ignored -> counter.incrementAndGet(),
                                                      EigenvectorFailureMode.THROW);
        assertEquals(eigs2, eigs3);
        assertEquals(eigs1, eigs3);
        
        assertEquals(3L, counter.get());
    }
    
    /** Test {@link Eigen#eigenvalues(ComplexMatrix, BalanceMode, int, Consumer)},
     *       {@link Eigen#eigenvalues(FieldMatrix)},
     *       {@link Eigen#eigenvalues(RealMatrix)} */
    @Test
    void testEigenvalues() {
        final double[][] AData = new double[][] { { 1, 0, 0, 0 },
                                                  { 0, 2, 0, 0 },
                                                  { 0, 0, 3, 0 },
                                                  { 0, 0, 0, 4 } };

        final Matrix        realA    = new Matrix(AData);
        final ComplexMatrix complexA = new ComplexMatrix(AData);
        final Complex[] expectedEigs = new Complex[] { Complex.valueOf(1.0),
                                                       Complex.valueOf(2.0),
                                                       Complex.valueOf(4.0),
                                                       Complex.valueOf(3.0)};
        
        final Complex[] eigs1 = Eigen.eigenvalues(realA).toArray();
        assertArrayEquality(expectedEigs, eigs1);
        
        final Complex[] eigs2 = Eigen.eigenvalues(complexA).toArray();
        assertArrayEquality(eigs1, eigs2);
        
        final AtomicLong counter = new AtomicLong(0L);
        final Complex[] eigs3 = Eigen.eigenvalues(complexA,
                                                  BalanceMode.TWO_NORM,
                                                  120,
                                                  ignored -> counter.incrementAndGet()).toArray();
        assertArrayEquality(eigs1, eigs3);
        
        assertEquals(3L, counter.get());
    }
}
