/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.eigen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;

/** Test {@link RealEigenSystem}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class RealEigenSystemTest {
    
    /** fake Eigenvalues */
    private static final double[] EVALS = new double[] { 1, 2, 3, 4, 5 };
    
    /** fake Eigenvectors */
    private static final Matrix EVECS = new Matrix(new double[][] { { 1, 2, 3, 4, 5 },
                                                                    { 2, 3, 4, 5, 1 },
                                                                    { 3, 4, 5, 1, 2 },
                                                                    { 4, 5, 1, 2, 3 },
                                                                    { 5, 1, 2, 3, 4 } });
    
    /** an example {@link RealEigenSystem} */
    private static final RealEigenSystem EXAMPLE = new RealEigenSystem(EVALS, EVECS);
    
    /** Test the non-trivial validation in the constructor where the number of Eigenvalues is not equal to the number of
     * Eigenvectors */
    @Test
    void testExceptionalConstructor() {
        final String expected =
                "There must be exactly one Eigenvalue for each Eigenvector, but there were 6 and 5 respectively";
        TestUtils.assertThrowsWithMessage(ValidationException.class,
                                          () -> new RealEigenSystem(new double[] { 1, 2, 3, 4, 5, 6 },
                                                                    new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                                                { 1, 1, 1, 1, 1 },
                                                                                                { 1, 1, 1, 1, 1 },
                                                                                                { 1, 1, 1, 1, 1 },
                                                                                                { 1, 1, 1, 1, 1 } })),
                                          expected);
    }
    
    /** Test {@link RealEigenSystem#eigenvalues()} and {@link RealEigenSystem#eigenvectorMatrix()} */
    @Test
    void testAccessors() {
        assertArrayEquality(EVALS, EXAMPLE.eigenvalues(), 0.0);
        assertEquals(EVECS, EXAMPLE.eigenvectorMatrix());
    }
    
    /** Test {@link RealEigenSystem#getEigenPair(int)} */
    @Test
    void testGetEigenPair() {
        final RealEigenPair pair = EXAMPLE.getEigenPair(2);
        assertEquals(3.0, pair.eigenvalue());
        assertEquals(new NVector(3, 4, 5, 1, 2), pair.eigenvector());
    }
    
    /** Test {@link RealEigenSystem#toString()} */
    @Test
    void testToString() {
        final String expected = String.join(System.lineSeparator(),
                                            "Eigenvalues:",
                                            " 1",
                                            " 2",
                                            " 3",
                                            " 4",
                                            " 5",
                                            "",
                                            "Eigenvectors:",
                                            " 1 2 3 4 5",
                                            " 2 3 4 5 1",
                                            " 3 4 5 1 2",
                                            " 4 5 1 2 3",
                                            " 5 1 2 3 4");
        TestUtils.testMultiLineToString(expected, EXAMPLE);
    }
    
    /** Test {@link RealEigenSystem#hashCode()} */
    @Test
    void testHashCode() {
        assertEquals(-433756677, EXAMPLE.hashCode());
    }
    
    /** Test {@link RealEigenSystem#equals(Object)} */
    @Test
    void testEquals() {
        final RealEigenSystem eq = new RealEigenSystem(EVALS, EVECS);
        
        assertEquals(EXAMPLE, EXAMPLE);
        assertEquals(EXAMPLE, eq);
        
        assertFalse(EXAMPLE.equals(null));         // for coverage
        assertFalse(EXAMPLE.equals(new Object())); // for coverage
        
        final RealEigenSystem nEq1 = new RealEigenSystem(new double[] { 1, 1, 1, 1, 1 }, EVECS);
        final RealEigenSystem nEq2 = new RealEigenSystem(EVALS,
                                                         new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                                     { 1, 1, 1, 1, 1 },
                                                                                     { 1, 1, 1, 1, 1 },
                                                                                     { 1, 1, 1, 1, 1 },
                                                                                     { 1, 1, 1, 1, 1 } }));
        assertNotEquals(EXAMPLE, nEq1);
        assertNotEquals(EXAMPLE, nEq2);
    }
}




