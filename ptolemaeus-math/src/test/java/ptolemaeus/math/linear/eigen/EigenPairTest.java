/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.eigen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;

import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.NVector;

/** Test {@link EigenPair}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class EigenPairTest {
    
    /** Test the {@code record} accessors of {@link EigenPair} */
    @Test
    void testAccessors() {
        final Complex       eVal = new Complex(1.0, 1.0);
        final ComplexVector eVec = new ComplexVector(new Complex(1.0, 1.0), new Complex(1.0, 1.0));
        
        final EigenPair eigenPair1 = new EigenPair(eVal, eVec);
        assertSame(eVal, eigenPair1.eigenvalue());
        assertSame(eVec, eigenPair1.eigenvector());
    }
    
    /** Test {@link EigenPair#asRealEigenPair(double)} */
    @Test
    void testAsRealEigenPair() {
        final double        absMaxIm = 1e-12;
        final Complex       realZ    = Complex.valueOf(6, absMaxIm);
        final ComplexVector realV    = new ComplexVector(new Complex[] { Complex.valueOf(1),
                                                                         Complex.valueOf(2),
                                                                         Complex.valueOf(3),
                                                                         Complex.valueOf(4),
                                                                         Complex.valueOf(5, absMaxIm) });
        
        final double        nextUp   = FastMath.nextUp(absMaxIm);
        final Complex       complexZ = Complex.valueOf(6, nextUp);
        final ComplexVector complexV = new ComplexVector(new Complex[] { Complex.valueOf(1),
                                                                         Complex.valueOf(2),
                                                                         Complex.valueOf(3),
                                                                         Complex.valueOf(4),
                                                                         Complex.valueOf(5, nextUp) });
        
        final EigenPair ep1 = new EigenPair(realZ,    realV);
        final EigenPair ep2 = new EigenPair(complexZ, realV);
        final EigenPair ep3 = new EigenPair(realZ,    complexV);
        
        final double        expectedEval   = 6.0;
        final NVector       expectedEvec   = new NVector(1, 2, 3, 4, 5);
        final RealEigenPair expectedRealEP = new RealEigenPair(expectedEval, expectedEvec);
        
        assertTrue(ep1.asRealEigenPair(absMaxIm).isPresent());
        assertEquals(expectedRealEP, ep1.asRealEigenPair(absMaxIm).orElseThrow());
        
        assertFalse(ep2.asRealEigenPair(absMaxIm).isPresent());
        assertFalse(ep3.asRealEigenPair(absMaxIm).isPresent());
    }
}
