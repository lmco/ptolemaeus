/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.eigen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;
import static ptolemaeus.commons.TestUtils.assertThrowsWithMessage;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.Matrix;

/** Tests for {@link EigenSystem}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class EigenSystemTest {
    
    /** Test {@link EigenSystem#asRealEigenSystem(double)} */
    @Test
    void testAsRealEigenSystem() {
        final Complex       two    = Complex.ONE.multiply(2.0);
        final Complex[]     eVals  = new Complex[] { two, two, two, two, two };
        final ComplexMatrix eVecM  = ComplexMatrix.identity(5);
        final EigenSystem   eigSys1 = new EigenSystem(eVals, eVecM);
        assertTrue(eigSys1.asRealEigenSystem(0.0).isPresent());
        
        final Complex[]   eVals2  = new Complex[] { Complex.valueOf(1, 1),
                                                    two, two, two, two };
        final EigenSystem eigSys2 = new EigenSystem(eVals2, eVecM);
        
        assertFalse(eigSys2.asRealEigenSystem(0.0).isPresent());
        assertTrue(eigSys2.asRealEigenSystem(1.1).isPresent());
        
        final RealEigenSystem expected = new RealEigenSystem(new double[] { 1, 2, 2, 2, 2 },
                                                             Matrix.identity(5));
        assertEquals(expected, eigSys2.asRealEigenSystem(1.1).orElseThrow());
    }
    
    /** Test the accessors of {@link EigenSystem} */
    @Test
    void testAccessors() {
        final Complex       two    = Complex.ONE.multiply(2.0);
        final Complex[]     eVals  = new Complex[] { two, two, two, two, two };
        final ComplexMatrix eVecM  = ComplexMatrix.identity(5);
        final EigenSystem   eigSys = new EigenSystem(eVals, eVecM);
        
        assertArrayEquality(eVals, eigSys.eigenvalues());
        assertArrayEquality(eVals, eigSys.getEigenvalueVector().getData());
        assertEquals(eVecM, eigSys.eigenvectorMatrix());
        assertEquals(ComplexMatrix.diagonal(eVals), eigSys.getEigenvalueMatrix());
        
        assertThrowsWithMessage(ValidationException.class,
                      () -> new EigenSystem(new Complex[] { two, two, two, two }, eVecM),
                      "There must be exactly one Eigenvalue for each Eigenvector, but there were 4 and 5 respectively");
    }
    
    /** Test {@link EigenSystem#getEigenPair(int)} */
    @Test
    void testGetEigenPair() {
        final Complex[] eVals = new Complex[] { Complex.valueOf(1),
                                                Complex.valueOf(2),
                                                Complex.valueOf(3),
                                                Complex.valueOf(4),
                                                Complex.valueOf(5) };
        
        final ComplexMatrix eVecMatrix = ComplexMatrix.diagonal(eVals);
        
        final EigenPair ep = new EigenSystem(eVals, eVecMatrix).getEigenPair(2);
        assertEquals(Complex.valueOf(3.0), ep.eigenvalue());
        assertEquals(new ComplexVector(0, 0, 3, 0, 0), ep.eigenvector());
    }
    
    /** Test {@link EigenSystem#hashCode()} */
    @Test
    void testHashCode() {
        final Complex       two     = Complex.ONE.multiply(2.0);
        final Complex[]     eVals   = new Complex[] { two, two, two, two, two };
        final ComplexMatrix eVecM   = ComplexMatrix.identity(5);
        final EigenSystem   eigSys  = new EigenSystem(eVals, eVecM);
        final EigenSystem   eigSys2 = new EigenSystem(eVals.clone(), eVecM);
        assertEquals(eigSys, eigSys2);
        assertEquals(eigSys.hashCode(), eigSys2.hashCode());

    }
    
    /** Test {@link EigenSystem#toString()} */
    @Test
    void testToString() {
        final Complex       two    = Complex.ONE.multiply(2.0);
        final Complex[]     eVals  = new Complex[] { two, two, two, two, two };
        final ComplexMatrix eVecM  = ComplexMatrix.identity(5);
        final EigenSystem   eigSys = new EigenSystem(eVals, eVecM);
        
        final String expected = String.join(System.lineSeparator(),
                                            "Eigenvalues:",
                                            " 2",
                                            " 2",
                                            " 2",
                                            " 2",
                                            " 2",
                                            "",
                                            "Eigenvectors:",
                                            " 1 0 0 0 0",
                                            " 0 1 0 0 0",
                                            " 0 0 1 0 0",
                                            " 0 0 0 1 0",
                                            " 0 0 0 0 1");
        TestUtils.testMultiLineToString(expected, eigSys);
    }
    
    /** Test {@link EigenSystem#equals(Object)} */
    @Test
    void testEquals() {
        final Complex       two    = Complex.ONE.multiply(2.0);
        final Complex[]     eVals  = new Complex[] { two, two, two, two, two };
        final ComplexMatrix eVecM  = ComplexMatrix.identity(5);
        final EigenSystem   eigSys = new EigenSystem(eVals, eVecM);
        assertEquals(eigSys, eigSys);
        
        assertFalse(eigSys.equals(null));         // for coverage
        assertFalse(eigSys.equals(new Object())); // for coverage
        
        final EigenSystem   eigSysEq = new EigenSystem(eVals, eVecM);
        assertEquals(eigSys, eigSysEq);
        
        final Complex[] eValsNEq = new Complex[] { Complex.ONE, Complex.ONE, Complex.ONE, Complex.ONE, Complex.ONE };
        final ComplexMatrix eVecMNEq = ComplexMatrix.identity(5).scalarMultiply(2);
        
        assertNotEquals(eigSys, new EigenSystem(eValsNEq, eVecM));
        assertNotEquals(eigSys, new EigenSystem(eVals, eVecMNEq));
        assertNotEquals(eigSys, new EigenSystem(eValsNEq, eVecMNEq));
    }
}
