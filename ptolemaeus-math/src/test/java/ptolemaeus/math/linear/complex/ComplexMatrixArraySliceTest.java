/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/** Tests for {@link ComplexMatrixArraySlice}. The tests that cover most of {@link AbstractComplexMatrixSlice} are in
 * {@link ComplexMatrixIndexSliceTest}.
 * 
 * @author Peter Davis, peter.m.davis@lmco.com */
class ComplexMatrixArraySliceTest {
    
    /**
     * The identity matrix of size 4x4
     */
    private final ComplexMatrix I4 = ComplexMatrix.identity(4);

    /**
     * Tests {@link ComplexMatrixArraySlice#getRowDimension()} and {@link ComplexMatrixArraySlice#getColumnDimension()}
     */
    @Test
    void testDimensions() {
        
        ComplexMatrixArraySlice slice = new ComplexMatrixArraySlice(I4, new int[]{0, 1, 3}, new int[]{2, 3});
        assertAll(
            () -> assertEquals(3, slice.getRowDimension()),
            () -> assertEquals(2, slice.getColumnDimension())
        );
    }
    
    /** Test {@link ComplexMatrixArraySlice#getSourceRowIndex(int)} and
     *       {@link ComplexMatrixArraySlice#getSourceColIndex(int)} */
    @Test
    void testGetSourceIndices() {
        final ComplexMatrix tenten = ComplexMatrixTest.randomComplexMatrix(10, 10, 123L);
        final ComplexMatrixArraySlice slice = new ComplexMatrixArraySlice(tenten,
                                                                          new int[] { 1, 3, 5, 7, 9 },
                                                                          new int[] { 2, 4, 6, 8 });
        
        assertEquals(1, slice.getSourceRowIndex(0));
        assertEquals(3, slice.getSourceRowIndex(1));
        assertEquals(5, slice.getSourceRowIndex(2));
        assertEquals(7, slice.getSourceRowIndex(3));
        assertEquals(9, slice.getSourceRowIndex(4));
        
        assertEquals(2, slice.getSourceColIndex(0));
        assertEquals(4, slice.getSourceColIndex(1));
        assertEquals(6, slice.getSourceColIndex(2));
        assertEquals(8, slice.getSourceColIndex(3));
    }
}
