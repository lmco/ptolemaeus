/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.hipparchus.complex.Complex;
import org.junit.jupiter.api.Test;

/** Tests for {@link ComplexMatrixIndexSlice} and {@link AbstractComplexMatrixSlice}
 * 
 * @author Peter Davis, peter.m.davis@lmco.com */
class ComplexMatrixIndexSliceTest {
    
    /**
     * The identity matrix of size 4x4
     */
    private final ComplexMatrix I4 = ComplexMatrix.identity(4);

    /**
     * Tests {@link ComplexMatrixIndexSlice#getRowDimension()} and {@link ComplexMatrixIndexSlice#getColumnDimension()}
     */
    @Test
    void testDimensions() {

        //slice which is the inner two rows/cols of I4
        ComplexMatrixIndexSlice slice = new ComplexMatrixIndexSlice(I4, 1, 2, 1, 2);
        assertAll(
            () -> assertEquals(2, slice.getRowDimension()),
            () -> assertEquals(2, slice.getColumnDimension())
        );
    }

    /**
     * Tests {@link ComplexMatrixIndexSlice} is read-through and write-back
     */
    @Test
    void testReadThroughAndWriteBack() {
        //read-through: changes to underlying matrix change the slice
        AbstractComplexMatrixSlice slice = new ComplexMatrixIndexSlice(I4, 1, 2, 1, 2);
        I4.setEntry(1, 1, new Complex(42.0));
        assertEquals(new Complex(42.0), slice.getEntry(0, 0));

        //write-back: changes to the slice change the underlying matrix
        slice.setEntry(1, 0, new Complex(1.0));
        assertEquals(new Complex(1.0), I4.getEntry(2, 1));
    }

    /**
     * Assert all unsupported methods throw an {@link UnsupportedOperationException}:<br>
     * <ul>
     *  <li> {@link ComplexMatrixIndexSlice#getDataRef()}</li>
     *  <li> {@link ComplexMatrixIndexSlice#createMatrix(int, int)}</li>
     * </ul>
     */
    @Test
    void testUnsupported() {
        AbstractComplexMatrixSlice slice = new ComplexMatrixIndexSlice(I4, 1, 2, 1, 2);
        assertAll(
            () -> assertThrows(UnsupportedOperationException.class, () -> slice.getDataRef()),
            () -> assertThrows(UnsupportedOperationException.class, () -> slice.createMatrix(0, 0))
        );
    }

    /**
     * Tests {@link ComplexMatrixIndexSlice#getSubMatrix(int, int, int, int)} and 
     * {@link ComplexMatrixIndexSlice#getSubMatrix(int[], int[])}
     */
    @Test
    void testGetSubRange() {
        AbstractComplexMatrixSlice slice = new ComplexMatrixIndexSlice(I4, 0, 2, 0, 2);
        ComplexMatrix subRange = slice.getSubMatrix(0, 1, 0, 1);

        ComplexMatrix expectedSubRange = I4.getSubMatrix(0, 1, 0, 1);
        assertEquals(expectedSubRange, subRange);

        ComplexMatrix slice2 = new ComplexMatrixIndexSlice(I4, 1, 3, 1, 3);
        ComplexMatrix expectedSubRange2 = I4.getSubMatrix(new int[]{2, 3}, new int[]{2, 3});
        ComplexMatrix subRange2 = slice2.getSubMatrix(new int[]{1, 2}, new int[]{1, 2});
        assertEquals(expectedSubRange2, subRange2);
    }

    /**
     * Tests {@link ComplexMatrixIndexSlice#copy()}
     */
    @Test
    void testCopy() {
        I4.setEntry(1, 2, new Complex(10));
        AbstractComplexMatrixSlice slice = new ComplexMatrixIndexSlice(I4, 1, 2, 1, 2);
        ComplexMatrix copy = slice.copy();
        assertNotSame(slice, copy);
        assertNotSame(I4, copy);
        ComplexMatrix expected = new ComplexMatrix(new double[][]{{1.0, 10.0}, {0.0, 1.0}});
        assertEquals(expected, copy);
    }

    /**
     * Tests {@link ComplexMatrixIndexSlice#addToEntry(int, int, Complex)} and 
     * {@link ComplexMatrixIndexSlice#multiplyEntry(int, int, double)}
     */
    @Test
    void testAddAndMultiplyToEntry() {
        AbstractComplexMatrixSlice slice = new ComplexMatrixIndexSlice(I4, 0, 1, 0, 1);
        slice.addToEntry(0, 0, new Complex(10.0));
        assertEquals(new Complex(11.0), slice.getEntry(0, 0));

        slice.multiplyEntry(0, 0, 5);
        assertEquals(new Complex(55.0), slice.getEntry(0, 0));
        assertEquals(new Complex(55.0), I4.getEntry(0, 0));
    }

    /**
     * Tests {@link ComplexMatrixIndexSlice#equals(Object)} and {@link ComplexMatrixIndexSlice#hashCode()}
     */
    @Test
    void testEqualsAndHashCode() {
        AbstractComplexMatrixSlice slice = new ComplexMatrixIndexSlice(I4, 0, 1, 0, 1);
        assertNotNull(slice.hashCode()); // coverage call
        assertNotEquals(slice, null);
        assertNotEquals(slice, Double.valueOf(2.0));
        assertEquals(slice, slice);
        assertNotEquals(slice, new ComplexMatrixIndexSlice(I4, 0, 2, 0, 1));
        assertNotEquals(slice, new ComplexMatrixIndexSlice(I4, 0, 1, 0, 2));

        AbstractComplexMatrixSlice slice2 = new ComplexMatrixIndexSlice(I4, 0, 1, 0, 1);
        assertEquals(slice, slice2);

        AbstractComplexMatrixSlice slice3 = new ComplexMatrixIndexSlice(I4, 1, 2, 0, 1);
        assertNotEquals(slice, slice3);
    }
}
