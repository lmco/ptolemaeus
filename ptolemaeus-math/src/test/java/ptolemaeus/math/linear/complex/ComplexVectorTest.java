/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;

import org.hipparchus.complex.Complex;
import org.hipparchus.complex.ComplexField;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.ArrayFieldVector;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.RealVector;
import org.hipparchus.linear.SparseFieldVector;
import org.hipparchus.random.RandomDataGenerator;
import org.hipparchus.util.FastMath;
import org.junit.jupiter.api.Test;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.NVector;

/** Tests for {@link ComplexVector}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class ComplexVectorTest {
    
    /** a non-trivial {@link ComplexVector} */
    private static final ComplexVector V = new ComplexVector(new Complex(1, 12),
                                                             new Complex(2, 11),
                                                             new Complex(3, 10),
                                                             new Complex(4,  9),
                                                             new Complex(5,  8),
                                                             new Complex(6,  7));

    /** @param complexDim the desired dimension of the {@link ComplexVector}
     * @param maxHermitianNorm the {@link ComplexVector#hermitianNorm()} of the random {@link ComplexVector}
     * @param randomSeed the random seed
     * @return a new {@link RandomDataGenerator randomly} generated {@link ComplexVector} */
    public static ComplexVector randomComplexVector(final int complexDim,
                                                    final double maxHermitianNorm,
                                                    final long randomSeed) {
        final RandomDataGenerator r = new RandomDataGenerator(randomSeed);
        
        final Complex[] vectorArr = new Complex[complexDim];
        for (int i = 0; i < complexDim; i++) {
            final double a = r.nextDouble() - 0.5; // nextDouble is [0, 1], so a and b are [-0.5, 0.5]
            final double b = r.nextDouble() - 0.5; // we later normalize, so this is equivalent to [-1, 1]
            vectorArr[i] = Complex.valueOf(a, b);
        }
        
        final ComplexVector unScaled = new ComplexVector(ComplexVector.asReImArray(vectorArr));
        final ComplexVector normed   = unScaled.dividedBy(Complex.valueOf(unScaled.hermitianNorm()));
        final ComplexVector scaled   = normed.times(Complex.valueOf(maxHermitianNorm));
        
        return scaled;
    }
    
    /** Copy the given array, sort it, and return a {@link ComplexVector} with the entries sorted by their
     * {@link Complex#norm() complex norms}, giving special treatment to complex conjugates; i.e., given two complex
     * numbers {@code z1 = a1 + b1}, {@code z2 = a1 - b1 = conj(a1)}, z1 will come first.
     * 
     * @param entries the {@link ComplexVector} entries
     * @return the new {@link ComplexVector} */
    public static ComplexVector sortedAsVector(final Complex... entries) {
        final Complex[] sorted = entries.clone();
        
        final Comparator<Complex> comp =
                Comparator.comparing((Function<Complex, Double>) Numerics::complexNormSq)
                          .thenComparing(Complex::getReal)
                          .thenComparing(Complex::getImaginary);
        
        Arrays.sort(sorted, comp);
        
        return new ComplexVector(ComplexVector.asReImArray(sorted));
    }

    /** Assert that the elements of the two matrices are equivalent according to
     * {@link ComplexVector#absEquivalentTo(ComplexVector, double, double)}
     * 
     * @param v the expected {@link ComplexVector}
     * @param w the actual {@link ComplexVector}
     * @param reTol the real-number tolerance
     * @param imTol the imaginary-number tolerance */
    public static void assertAbsEquals(final ComplexVector v,
                                       final ComplexVector w,
                                       final double reTol,
                                       final double imTol) {
        assertTrue(v.absEquivalentTo(w, reTol, imTol),
                   () -> {
                       final ComplexVector error = v.minus(w);
                       
                       double worstReError = 0.0;
                       double worstImError = 0.0;
                       for (int i = 0; i < error.getLength(); i++) {                               
                               final Complex ij = error.getEntry(i);
                               worstReError = FastMath.max(worstReError, FastMath.abs(ij.getReal()));
                               worstImError = FastMath.max(worstImError, FastMath.abs(ij.getImaginary()));
                        }
                       
                       final Complex worstEntryError = Complex.valueOf(worstReError, worstImError);
                       final StringBuilder errMsg = new StringBuilder();
                       
                       errMsg.append("Error:")
                             .append(System.lineSeparator())
                             .append(error)
                             .append(System.lineSeparator())
                             .append("Worst error: ")
                             .append(worstEntryError);
                       return errMsg.toString();
                   });
    }
    
    /** Test {@link ComplexVector#sumComplex1Norm()} */
    @Test
    void testSumComplex1Norm() {
        final ComplexVector zero = ComplexVector.zero(10);
        assertEquals(0.0, zero.sumComplex1Norm());
        
        final ComplexVector v = new ComplexVector(new double[][] { {  1, -2, -3, 4, -5, 6.5 },
                                                                   { 12, 11, 10, 9, -8, 7   } });
        
        assertEquals(12 + 11 + 10 + 9 + 8 + 7 + 6.5 + 5 + 4 + 3 + 2 + 1, v.sumComplex1Norm());
    }
    
    /** Test {@link ComplexVector#of(FieldVector)} */
    @Test
    void testOf() {
        final ArrayFieldVector<Complex>  afv    = new ArrayFieldVector<>(V.getData());
        final SparseFieldVector<Complex> sparse = new SparseFieldVector<>(ComplexField.getInstance(), V.getData());
        
        final ComplexVector of1 = ComplexVector.of(V);
        final ComplexVector of2 = ComplexVector.of(afv);
        final ComplexVector of3 = ComplexVector.of(sparse);
        
        assertEquals(V, of1);
        assertEquals(V, of2);
        assertEquals(V, of3);
        
        assertSame(V.getDataRef()[0],   of1.getDataRef()[0]); 
        assertSame(V.getDataRef()[1],   of1.getDataRef()[1]); 
        assertNotSame(afv.getDataRef()[0], of2.getData()[0]); // clone ignored for this vector
        assertNotSame(afv.getDataRef()[1], of2.getData()[1]);
        
        assertNotSame(sparse.toArray(),     of3.getDataRef()); // clone ignored for this vector
        assertArrayEquals(sparse.toArray(), of3.getData());
    }
    
    /** Test {@link ComplexVector#asReal(double)} */
    @Test
    void testAsReal() {
        final double[][] reIm = new double[][] { { 1, 2, 3, 4, 5, 6 }, { 0, 0, 0, 1e-12, 0, 0 } };
        final ComplexVector cv = new ComplexVector(reIm[0], reIm[1]);
        
        final NVector expected = new NVector(reIm[0]);
        assertEquals(expected, cv.asReal(1e-12).orElseThrow());
        
        assertTrue(cv.asReal(0.0).isEmpty());
    }
    
    /** Test {@link ComplexVector#normalizedHermitian()} */
    @Test
    void testNormalizedHermitian() {
        final ComplexVector normalized = V.normalizedHermitian();
        
        assertEquals(1, normalized.hermitianNorm());     // zero tolerance
        assertEquals(0, V.hermitianAngleTo(normalized)); // zero tolerance
        
        final double        vHermNorm        = V.hermitianNorm();
        final ComplexVector scaledNormalized = normalized.mapMultiply(vHermNorm);
        for (int i = 0; i < 6; i++) {
            if (i != 5) {
                assertEquals(V.getEntry(i), scaledNormalized.getEntry(i));
            }
            else { // the only entry with a little bit of difference
                assertEquals(V.getEntry(i).getReal(),      scaledNormalized.getEntry(i).getReal());
                assertEquals(V.getEntry(i).getImaginary(), scaledNormalized.getEntry(i).getImaginary(), 1e-15);
            }
        }
    }
    
    /** Test {@link ComplexVector#asReImArray(Complex[])},
     *       {@link ComplexVector#getData()},
     *       {@link ComplexVector#ComplexVector(double[], double[])}, and
     *       {@link ComplexVector#toArray()} */
    @Test
    void testAsReImArray() {
        final double[][] expected = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                     { 12, 11, 10, 9, 8, 7 } };
        
        final double[][] actual1 = V.getDataRef();
        final double[][] actual2 = ComplexVector.asReImArray(V.getData());
        final double[][] actual3 = ComplexVector.asReImArray(V.toArray());
        assertArrayEquals(expected, V.getDataRef());
        assertArrayEquals(expected, ComplexVector.asReImArray(V.getData()));
        
        final ComplexVector fromReIm1 = new ComplexVector(actual1[0], actual1[1]);
        final ComplexVector fromReIm2 = new ComplexVector(actual2[0], actual2[1]);
        final ComplexVector fromReIm3 = new ComplexVector(actual3[0], actual3[1]);
        
        assertEquals(V, fromReIm1);
        assertEquals(V, fromReIm2);
        assertEquals(V, fromReIm3);

    }

    /** Test {@link ComplexVector#dotProduct(FieldVector)} */
    @Test
    void testDotProduct() {
        final ComplexVector cv1 = new ComplexVector(new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                                     { 12, 11, 10, 9, 8, 7 } });
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(cv1.getData());

        final ComplexVector cv2 = new ComplexVector(new double[][] { {  1,  2,  3, 4, 5, 6, 7 },
                                                                     { 12, 11, 10, 9, 8, 7, 6 } });
        final ComplexVector cv3 = new ComplexVector(cv1).mapAdd(2, 4);

        assertEquals(Complex.valueOf(-455, 448), cv2.dotProduct(cv2));
        assertEquals(cv3.dotProduct(fv1), cv3.dotProduct(cv1));

        assertThrows(MathIllegalArgumentException.class, () -> cv1.dotProduct(cv2));
    }

    /** Test {@link ComplexVector#checkVectorDimensions(FieldVector, FieldVector)},
     *       {@link ComplexVector#checkIndex(int)}, and
     *       {@link ComplexVector#ComplexVector(double[], double[])} 
     * <br>
     * 
     * Also tests to show that the dimension checks within the constructors are correct. */
    @Test
    void testDimensionChecks() {
        final double[] dRe1 = {  1,  2,  3, 4, 5, 6 };
        final double[] dIm1 = { 12, 11, 10, 9, 8, 7 };

        final double[] dRe2 = {  1,  2,  3, 4, 5, 6, 7 };
        final double[] dIm2 = { 12, 11, 10, 9, 8, 7, 6 };

        final ComplexVector cv1 = new ComplexVector(new double[][] { dRe1, dIm1 });
        final ComplexVector cv2 = cv1.copy().append(new Complex(7, 6));

        assertThrows(MathIllegalArgumentException.class, () -> cv2.subtract(cv1));
        assertThrows(MathIllegalArgumentException.class, () -> new ComplexVector(0));
        assertThrows(MathIllegalArgumentException.class, () -> new ComplexVector(dRe1, dIm2, true));
        assertThrows(MathIllegalArgumentException.class, () -> new ComplexVector(dRe2, dIm1));

    }
    
    /** Test {@link ComplexVector#toString()} and {@link ComplexVector#toString(boolean)} */
    @Test
    void testToString() {
        final ComplexVector v1 = new ComplexVector(V);
        assertEquals(" 1 + 12 i,  2 + 11 i,  3 + 10 i,  4 + 9 i,  5 + 8 i,  6 + 7 i",
                     v1.toString(false));
        assertEquals(v1.toString(false), v1.toString());
        
        final String asColumnVector = String.join(System.lineSeparator(),
                                            " 1 + 12 i",
                                            " 2 + 11 i",
                                            " 3 + 10 i",
                                            " 4 +  9 i",
                                            " 5 +  8 i",
                                            " 6 +  7 i");
        TestUtils.testMultiLineToString(asColumnVector, v1.toString(true));
        TestUtils.testMultiLineToString(asColumnVector, v1.toColumnString());
    }

    /** Test {@link ComplexVector#hashCode()} */
    @Test
    void testHashCode() {
        final ComplexVector v1 = new ComplexVector(V);

        final int hash = v1.hashCode();

        assertEquals(1872980961, hash);
        assertEquals(V.hashCode(), hash);
    }
    
    /** Test {@link ComplexVector#negate()} */
    @Test
    void testNegate() {
        final ComplexVector v = new ComplexVector(V);
        
        final ComplexVector negated         = v.negate();
        final ComplexVector expectedNegated = new ComplexVector(new double[][] { { -1,  -2,  -3,  -4, -5, -6 },
                                                                                 { -12, -11, -10, -9, -8, -7 } });
        
        assertEquals(expectedNegated, negated);
        assertEquals(1.0, v.cosHermitianAngleTo(negated));
    }
    
    /** Test {@link ComplexVector#hermitianAngleTo(ComplexVector)} and (indirectly)
     * {@link ComplexVector#cosHermitianAngleTo(ComplexVector)}<br>
     * <br>
     * 
     * See <a href=
     * "https://www.wolframalpha.com/input?i=VectorAngle%5B%5B++1+%2B+12+i%2C++2+%2B+11+i%2C++3+%2B+10+i%2C++4+%2B+9+
     * i%2C++5+%2B+8+i%2C++6+%2B+7+i+%5D%2C+%5B++12+%2B+1+i%2C++11+%2B+2+i%2C++10+%2B+3+i%2C++9+%2B+4+i%2C++8+%2B+5+
     * i%2C++7+%2B+6+i+%5D%5D">Wolfram Alpha</a> to verify results */
    @Test
    void testHermitianAngleTo() {
        // easy visualization:
        final double piOn2 = FastMath.PI / 2.0;
        assertEquals(piOn2, new ComplexVector(new double[][] {{1, 0}, {0, 0}}).hermitianAngleTo(
                            new ComplexVector(new double[][] {{0, 1}, {0, 0}})));
        
        assertEquals(piOn2, new ComplexVector(new double[][] {{0, 0}, {1, 0}}).hermitianAngleTo(
                            new ComplexVector(new double[][] {{0, 1}, {0, 0}})));
        
        assertEquals(piOn2,
                     new ComplexVector(new double[][]{{ 1, 0 }, { 0, 1 }}).hermitianAngleTo(
                     new ComplexVector(new double[][]{{ 0, 1 }, { 1, 0 }})));
        
        // check wrapped real vectors
        final NVector real1 = new NVector(1, 2, 3, 4, 5, 6);
        final NVector real2 = new NVector(12, 11, 10, 9, 8, 7);
        final double r1RoR2 = real1.angleTo(real2);
        
        final ComplexVector c1 = new ComplexVector(new Complex(1),
                                                   new Complex(2),
                                                   new Complex(3),
                                                   new Complex(4),
                                                   new Complex(5),
                                                   new Complex(6));
        final ComplexVector c2 = new ComplexVector(new Complex(12),
                                                   new Complex(11),
                                                   new Complex(10),
                                                   new Complex(9),
                                                   new Complex(8),
                                                   new Complex(7));

        final double c1Toc2 = c1.hermitianAngleTo(c2);
        assertEquals(r1RoR2, c1Toc2);
        
        // check fully complex vectors
        final ComplexVector c3 = new ComplexVector(new double[] {  1,  2,  3, 4, 5, 6 },
                                                   new double[] { 12, 11, 10, 9, 8, 7 });
        final ComplexVector c4 = new ComplexVector(new double[] { 12, 11, 10, 9, 8, 7 },  
                                                   new double[] {  1,  2,  3, 4, 5, 6 });
        
        // expected angle confirmed via WolframAlpha (see JavaDoc)
        final double c3Toc4        = c3.hermitianAngleTo(c4);
        final double expectedAngle = 0.42232034041035826;
        assertEquals(expectedAngle, c3Toc4);
        
        // check linearity
        final RandomDataGenerator rdg = new RandomDataGenerator(8675309L);
        final ComplexVector randomScaled1 = c3.dividedBy(new Complex(rdg.nextDouble(), rdg.nextDouble()));
        final ComplexVector randomScaled2 = c4.dividedBy(new Complex(rdg.nextDouble(), rdg.nextDouble()));
        final double sameAsBefore = randomScaled1.hermitianAngleTo(randomScaled2);
        assertEquals(expectedAngle, sameAsBefore, 1e-15);
        
        // exceptional cases
        assertThrows(NumericalMethodsException.class, () -> ComplexVector.zero(1).cosHermitianAngleTo(c1));
        assertThrows(NumericalMethodsException.class, () -> c1.cosHermitianAngleTo(ComplexVector.zero(1)));
    }
    
    /** Simple coverage tests for these pass-through methods:
     * 
     * <ul>
     * <li>{@link ComplexVector#plus(FieldVector)}</li>
     * <li>{@link ComplexVector#minus(FieldVector)}</li>
     * <li>{@link ComplexVector#times(Complex)}</li>
     * <li>{@link ComplexVector#dividedBy(Complex)}</li>
     * <li>{@link ComplexVector#add(FieldVector)}</li>
     * <li>{@link ComplexVector#subtract(FieldVector)}</li>
     * <li>{@link ComplexVector#outerProduct(FieldVector)}</li>
     * </ul>
     */
    @Test
    void testPassThroughs() {
        final ComplexVector v1 = new ComplexVector(V);        
        final ComplexVector v2 = new ComplexVector(new double[][] {{-1.0, -2.0, -3.0, -4.0, -5.0, -6.0},
                                                                   {   0,    0,    0,    0,    0,    0}});
        
        // plus/add
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 0 + 12 i",
                                                    " 0 + 11 i",
                                                    " 0 + 10 i",
                                                    " 0 +  9 i",
                                                    " 0 +  8 i",
                                                    " 0 +  7 i"),
                                        v1.plus(v2).toString(true));
        assertEquals(v1.plus(v2), v1.add(v2));
        assertEquals(v1.plus(v2), v1.add((FieldVector<Complex>) v2));
        
        // minus/subtract
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    "  2 + 12 i",
                                                    "  4 + 11 i",
                                                    "  6 + 10 i",
                                                    "  8 +  9 i",
                                                    " 10 +  8 i",
                                                    " 12 +  7 i"),
                                        v1.minus(v2).toString(true));
        assertEquals(v1.minus(v2), v1.subtract(v2));
        assertEquals(v1.minus(v2), v1.subtract((FieldVector<Complex>) v2));
        
        // times
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " -34 + 27 i",
                                                    " -29 + 28 i",
                                                    " -24 + 29 i",
                                                    " -19 + 30 i",
                                                    " -14 + 31 i",
                                                    "  -9 + 32 i"),
                                        v1.times(2, 3).toString(true));
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " -34 + 27 i",
                                                    " -29 + 28 i",
                                                    " -24 + 29 i",
                                                    " -19 + 30 i",
                                                    " -14 + 31 i",
                                                    "  -9 + 32 i"),
                                        v1.times(Complex.valueOf(2, 3)).toString(true));
        assertEquals(v1.times(Complex.valueOf(1, 0)), v1.times(1));
        assertEquals(v1.times(2, 0), v1.times(2));
        
        // dividedBy
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 0.1 + 1.2 i",
                                                    " 0.2 + 1.1 i",
                                                    " 0.3 + 1.0 i",
                                                    " 0.4 + 0.9 i",
                                                    " 0.5 + 0.8 i",
                                                    " 0.6 + 0.7 i"),
                                        v1.dividedBy(Complex.valueOf(10)).toString(true));
        // outerProduct        
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                    " -1 + -12 i,   -2 + -11 i,   -3 + -10 i,   -4 +  -9 i,   -5 +  -8 i,   -6 +  -7 i",
                    " -2 + -24 i,   -4 + -22 i,   -6 + -20 i,   -8 + -18 i,  -10 + -16 i,  -12 + -14 i",
                    " -3 + -36 i,   -6 + -33 i,   -9 + -30 i,  -12 + -27 i,  -15 + -24 i,  -18 + -21 i",
                    " -4 + -48 i,   -8 + -44 i,  -12 + -40 i,  -16 + -36 i,  -20 + -32 i,  -24 + -28 i",
                    " -5 + -60 i,  -10 + -55 i,  -15 + -50 i,  -20 + -45 i,  -25 + -40 i,  -30 + -35 i",
                    " -6 + -72 i,  -12 + -66 i,  -18 + -60 i,  -24 + -54 i,  -30 + -48 i,  -36 + -42 i"),
                                            v2.outerProduct(v1).toString());
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                    " -1 + -12 i,   -2 + -24 i,   -3 + -36 i,   -4 + -48 i,   -5 + -60 i,   -6 + -72 i",
                    " -2 + -11 i,   -4 + -22 i,   -6 + -33 i,   -8 + -44 i,  -10 + -55 i,  -12 + -66 i",
                    " -3 + -10 i,   -6 + -20 i,   -9 + -30 i,  -12 + -40 i,  -15 + -50 i,  -18 + -60 i",
                    " -4 +  -9 i,   -8 + -18 i,  -12 + -27 i,  -16 + -36 i,  -20 + -45 i,  -24 + -54 i",
                    " -5 +  -8 i,  -10 + -16 i,  -15 + -24 i,  -20 + -32 i,  -25 + -40 i,  -30 + -48 i",
                    " -6 +  -7 i,  -12 + -14 i,  -18 + -21 i,  -24 + -28 i,  -30 + -35 i,  -36 + -42 i"),
                                            v1.outerProduct(v2));
    }
    
    /** Test {@link ComplexVector#conjugate()} */
    @Test
    void testConjugate() {
        final ComplexVector v1 = new ComplexVector(V);
        final ComplexVector v1Dagger = v1.conjugate();
        for (int i = 0; i < 6; i++) {
            assertEquals(v1.getEntry(i).conjugate(), v1Dagger.getEntry(i));
        }
        
        final ComplexVector v2 = new ComplexVector(new NVector(2.0, 0.0, 2.0, 0.0, 2.0, 0.0));
        final ComplexVector v2Dagger = v2.conjugate();
        assertEquals(v2, v2Dagger);
    }
    
    /** Test {@link ComplexVector#isReal()} */
    @Test
    void testIsReal() {
        final ComplexVector cv1 = new ComplexVector(new double[] {1.0, 2.0, 3.0}, new double[] {0, 0, 0});
        final ComplexVector cv2 = new ComplexVector(new double[] {1.0, 2.0, 3.0}, new double[] {0, 0, 4.0});
        
        assertTrue(cv1.isReal());
        assertFalse(cv2.isReal());
    }
    
    /** Test {@link ComplexVector#isNaN()} */
    @Test
    void testIsNaNIsFinite() {
        final ComplexVector cv1 = new ComplexVector(new double[] {1.0, 2.0, 3.0}, new double[] {0, 0, 0});
        final ComplexVector cv2 = new ComplexVector(new double[] {1.0, 2.0, 3.0}, new double[] {0, 0, Double.NaN});
        final ComplexVector cv3 = new ComplexVector(new double[] {1.0, 2.0, Double.NaN}, 
                                                    new double[] {0, 0, 0});
        
        assertFalse(cv1.isNaN());
        assertTrue(cv2.isNaN());
        assertTrue(cv3.isNaN());
        
        assertTrue(cv1.isFinite());
        assertFalse(cv2.isFinite());
        assertFalse(cv3.isFinite());
    }
    
    /** Test {@link ComplexVector#isInfinite} and {@link ComplexVector#isFinite()} */
    @Test
    void testIsInfiniteIsFinite() {
        final ComplexVector cv1 = new ComplexVector(new double[] {1.0, 2.0, 3.0}, new double[] {0, 0, 0});
        final ComplexVector cv2 = new ComplexVector(new double[] {1.0, 2.0, 3.0}, 
                                                    new double[] {0, 0, Double.NEGATIVE_INFINITY});
        final ComplexVector cv3 = new ComplexVector(new double[] {1.0, 2.0, Double.NEGATIVE_INFINITY},
                                                    new double[] {0, 0, 0});
        
        assertFalse(cv1.isInfinite());
        assertTrue(cv2.isInfinite());
        assertTrue(cv3.isInfinite());
        
        assertTrue(cv1.isFinite());
        assertFalse(cv2.isFinite());
        assertFalse(cv3.isFinite());
    }
    
    /** Test {@link ComplexVector#isInfinite}, 
     *       {@link ComplexVector#isZero()}, and 
     *       {@link ComplexVector#isFinite()} */
    @Test
    void testIsZero() {
        final ComplexVector v1 = new ComplexVector(new double[] {-0.0, 0, 0}, new double[] {0, -0.0, 0});
        final ComplexVector v2 = new ComplexVector(new double[] {0, 0, 0}, new double[] {0, 0, 1});
        assertTrue(v1.isZero());
        assertFalse(v2.isZero());

        final double[][] v3Data = new double[][] {{1.0, 2.0, 3.0, 4.0}, {-0.0, 0.0, 0, 0}};
        final ComplexVector v3 = new ComplexVector(v3Data, false);

        // -0.0 == 0.0 check
        assertEquals(new ComplexVector(new double[] {1.0, 2.0, 3.0, 4.0}, new double[] {-0, -0.0, 0, 0}), 
                     v3.conjugate());
        assertEquals(new ComplexVector(new double[] {1.0, 2.0, 3.0, 4.0}, new double[] { 0,    0, 0, 0}), 
                     v3.conjugate());

        assertSame(v3Data[0], v3.getDataRef()[0]);
    }

    /** Test {@link ComplexVector#zero(int)} */
    @Test
    void testZero() {
        final ComplexVector zero = ComplexVector.zero(10);
        
        final ComplexVector v = new ComplexVector(new double[][] { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 
                                                                   { -0.0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } });
        
        assertEquals(v, zero);
        assertTrue(zero.isZero());
    }
    
    /** Test {@link ComplexVector#elementStream()} */
    @Test
    void testElementStream() {
        final Complex[] cvEntriesExpected = new Complex[] { new Complex(1.0, 12.0),
                                                            new Complex(2.0, 11.0),
                                                            new Complex(3.0, 10.0),
                                                            new Complex(4.0, 9.0),
                                                            new Complex(5.0, 8.0),
                                                            new Complex(6.0, 7.0) };
        final ComplexVector cv = new ComplexVector(ComplexVector.asReImArray(cvEntriesExpected));
        final Complex[] cvEntriesActual = cv.elementStream().toArray(Complex[]::new);
        
        assertArrayEquals(cvEntriesExpected, cvEntriesActual);
    }
    
    /** Test {@link ComplexVector#hermitianInnerProduct(FieldVector)} */
    @Test
    void testHermitianInnerProduct() {
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(new Complex[] { new Complex(4, 9), 
                                                                                new Complex(5, 8), 
                                                                                new Complex(6, 7)});
        final ComplexVector cv1 = new ComplexVector(new double[] {1, 2, 3}, 
                                                    new double[] {12, 11, 10});
        final ComplexVector cv2 = new ComplexVector(new double[] {4, 5, 6}, 
                                                    new double[] {9, 8, 7});
        
        final Complex actualInnerProduct = cv1.hermitianInnerProduct(cv2);
        final Complex expectedInnerProduct = new Complex(1.0, 12.0).multiply(new Complex(4.0, 9.0).conjugate())
                                        .add(new Complex(2.0, 11.0).multiply(new Complex(5.0, 8.0).conjugate()))
                                        .add(new Complex(3.0, 10.0).multiply(new Complex(6.0, 7.0).conjugate()));
        assertTrue(Complex.equalsWithRelativeTolerance(expectedInnerProduct, actualInnerProduct, 1e-9),
                   () -> """
                         Expected Hermitian inner-product: %s
                         Actual   Hermitian inner-product: %s
                         """.formatted(expectedInnerProduct, actualInnerProduct));
        assertEquals(actualInnerProduct, cv1.hermitianInnerProduct(fv1));
        assertThrows(MathIllegalArgumentException.class, () -> cv1.hermitianInnerProduct(cv1.append(4, 9)));
    }
    
    /** Test the constructors: {@link ComplexVector#ComplexVector(double[], double[])},     
     *                         {@link ComplexVector#ComplexVector(double[][])}, and
     *                         {@link ComplexVector#ComplexVector(double...)} */
    @Test
    void testDoubleConstructors() {
        // double[], double[]
        final ComplexVector cv1 = new ComplexVector(new double[] {1, 2, 3, 4, 5, 6},  
                                                    new double[] {12, 11, 10, 9, 8, 7});
        assertEquals(new Complex(1.0, 12.0), cv1.getEntry(0));
        assertEquals(new Complex(2.0, 11.0), cv1.getEntry(1));
        assertEquals(new Complex(3.0, 10.0), cv1.getEntry(2));
        assertEquals(new Complex(4.0,  9.0), cv1.getEntry(3));
        assertEquals(new Complex(5.0,  8.0), cv1.getEntry(4));
        assertEquals(new Complex(6.0,  7.0), cv1.getEntry(5));
        
        assertEquals(6.0, cv1.getEntryRe(5));
        assertEquals(7.0, cv1.getEntryIm(5));
        
        // double[][]
        final double[][] cv2Data = new double[][] { { 1,  2,  3, 4, 5, 6}, {12, 11, 10, 9, 8, 7} };
        final ComplexVector cv2 = new ComplexVector(cv2Data, false);
        assertEquals(cv1, cv2); 
        assertSame(cv2Data[0], cv2.getDataRef()[0]);
        assertSame(cv2Data[1], cv2.getDataRef()[1]);
        
        final ComplexVector cv3 = new ComplexVector(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        assertEquals(new Complex(1.0, 0), cv3.getEntry(0));
        assertEquals(new Complex(2.0, 0), cv3.getEntry(1));
        assertEquals(new Complex(3.0, 0), cv3.getEntry(2));
        assertEquals(new Complex(4.0, 0), cv3.getEntry(3));
        assertEquals(new Complex(5.0, 0), cv3.getEntry(4));
        assertEquals(new Complex(6.0, 0), cv3.getEntry(5));

        assertThrows(MathIllegalArgumentException.class, () -> 
                new ComplexVector(new double[] {1, 2, 3, 4, 5, 6}, new double[] {0, 0, 0, 0, 0, 0, 0}));
    }
    
    /** Test the constructors: {@link ComplexVector#ComplexVector(Complex...)},
     *                         {@link ComplexVector#ComplexVector(Complex[])} */
    @Test
    void testComplexConstructors() {
        final ComplexVector cv1 = new ComplexVector(new Complex(1.0, 12.0),
                                                    new Complex(2.0, 11.0), 
                                                    new Complex(3.0, 10.0), 
                                                    new Complex(4.0,  9.0), 
                                                    new Complex(5.0,  8.0), 
                                                    new Complex(6.0,  7.0));        
        assertEquals(new Complex(1.0, 12.0), cv1.getEntry(0));
        assertEquals(new Complex(2.0, 11.0), cv1.getEntry(1));
        assertEquals(new Complex(3.0, 10.0), cv1.getEntry(2));
        assertEquals(new Complex(4.0,  9.0), cv1.getEntry(3));
        assertEquals(new Complex(5.0,  8.0), cv1.getEntry(4));
        assertEquals(new Complex(6.0,  7.0), cv1.getEntry(5));
        
        final Complex[] cv2Data = new Complex[] { new Complex(1.0, 12.0),
                                                  new Complex(2.0, 11.0),
                                                  new Complex(3.0, 10.0),
                                                  new Complex(4.0, 9.0),
                                                  new Complex(5.0, 8.0),
                                                  new Complex(6.0, 7.0) };
        final ComplexVector cv2 = new ComplexVector(cv2Data); 
        assertEquals(cv1, cv2); 
        assertNotSame(cv2Data[0], cv2.getData()[1]);
    }

    /** Test the constructors: {@link ComplexVector#ComplexVector(ComplexVector)},
     *                         {@link ComplexVector#ComplexVector(RealVector)}, 
     *                         {@link ComplexVector#ComplexVector(FieldVector)}, and
     *                         {@link ComplexVector#ComplexVector(NVector)} */
    @Test
    void testVectorConstructors() {
        // ComplexVector constructor
        final ComplexVector cv1 = new ComplexVector(V); 
        assertNotSame(cv1.getDataRef(), V.getDataRef());
        assertEquals(new Complex(1.0, 12.0), cv1.getEntry(0));
        assertEquals(new Complex(2.0, 11.0), cv1.getEntry(1));
        assertEquals(new Complex(3.0, 10.0), cv1.getEntry(2));
        assertEquals(new Complex(4.0,  9.0), cv1.getEntry(3));
        assertEquals(new Complex(5.0,  8.0), cv1.getEntry(4));
        assertEquals(new Complex(6.0,  7.0), cv1.getEntry(5));
        
        final ComplexVector cv2 = new ComplexVector(cv1, false); 
        assertEquals(cv1, cv2);

        // RealVector constructor
        final ComplexVector cv3 = new ComplexVector(new NVector(1.0, 2.0, 3.0, 4.0, 5.0, 6.0));
        final ComplexVector cv4 = new ComplexVector(new double[] {1, 2, 3, 4, 5, 6});
        assertEquals(cv3, cv4); 
        assertEquals(new Complex(1.0, 0), cv4.getEntry(0));
        assertEquals(new Complex(2.0, 0), cv4.getEntry(1));
        assertEquals(new Complex(3.0, 0), cv4.getEntry(2));
        assertEquals(new Complex(4.0, 0), cv4.getEntry(3));
        assertEquals(new Complex(5.0, 0), cv4.getEntry(4));
        assertEquals(new Complex(6.0, 0), cv4.getEntry(5));
        
        
        final ComplexVector cv5 = new ComplexVector(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        assertEquals(cv3, cv5); 

        // FieldVector constructor
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(ComplexField.getInstance(), 6);
        final ComplexVector cv7 = new ComplexVector(fv1, true); // clone ignored for this vector
        final ComplexVector cv8 = new ComplexVector(fv1, false);

        assertEquals(cv7, cv8);
        assertNotSame(cv7.getDataRef()[0], cv8.getDataRef()[0]);
        assertNotSame(cv7.getDataRef()[1], cv8.getDataRef()[1]);
        assertNotSame(cv8.getEntry(1), fv1.getEntry(1));

        assertEquals(ComplexVector.of(fv1), cv7);
        assertEquals(Complex.valueOf(0), cv7.getEntry(0));
        assertEquals(Complex.valueOf(0), cv8.getEntry(0));

        assertThrows(MathIllegalArgumentException.class, () -> new ComplexVector(0));
    }

    /** Test cloning the constructor: {@link ComplexVector#ComplexVector(ComplexVector, boolean)} */
    @Test
    void testCloningConstructors() {
        // ComplexVectors
        final ComplexVector v1        = new ComplexVector(new double[] {1, 2, 3, 4, 5, 6}, 
                                                          new double[] {0, 0, 0, 0, 0, 0});
        final ComplexVector notCloned = new ComplexVector(v1, false); // will not clone
        final ComplexVector cloned    = new ComplexVector(v1, true);  // will clone
        
        assertSame(v1.getDataRef()[0], notCloned.getDataRef()[0]);
        assertSame(v1.getDataRef()[1], notCloned.getDataRef()[1]);
        
        assertNotSame(v1.getDataRef()[0], cloned.getDataRef()[0]);
        assertNotSame(v1.getDataRef()[1], cloned.getDataRef()[1]);

        // Non-ComplexVectors - clone ignored
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(ComplexField.getInstance(), 6);
        final ComplexVector fNotCloned = new ComplexVector(fv1, false); // will not clone
        final ComplexVector fCloned    = new ComplexVector(fv1, true);  // will not clone

        assertNotSame(fNotCloned.getDataRef()[0],   fCloned.getDataRef()[0]);
        assertNotSame(fNotCloned.getDataRef()[1],   fCloned.getDataRef()[1]);
        assertNotSame(fNotCloned.getEntry(1), fCloned.getEntry(1));

        assertNotSame(ComplexVector.of(fv1), fNotCloned);

        // RealVectors
        final RealVector r1 = new NVector(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        final ComplexVector rNotCloned = new ComplexVector(r1, false); // will not clone
        final ComplexVector rCloned    = new ComplexVector(r1, true);  // will not clone

        assertArrayEquality(rNotCloned.getDataRef()[0], r1.toArray(), 0.0);
        assertEquals(rNotCloned, rCloned);
        assertNotSame(rCloned.getDataRef()[0], rNotCloned.getDataRef()[0]);
        assertNotSame(rCloned.getDataRef()[1], rNotCloned.getDataRef()[1]);

    }

    /** Test {@link ComplexVector#norm()} and {@link ComplexVector#normSq()} */
    @Test
    void testNorm() {
        final ComplexVector cv = new ComplexVector(V);
        
        final Complex expectedNormSq =
                Complex.ZERO.add(new Complex(1.0, 12.0).multiply(new Complex(1.0, 12.0)))  // 1^2 - 12^2 + 2 * 1 * 12 i
                            .add(new Complex(2.0, 11.0).multiply(new Complex(2.0, 11.0)))  // 2^2 - 11^2 + 2 * 2 * 11 i
                            .add(new Complex(3.0, 10.0).multiply(new Complex(3.0, 10.0)))  // ...
                            .add(new Complex(4.0,  9.0).multiply(new Complex(4.0,  9.0)))  // ...
                            .add(new Complex(5.0,  8.0).multiply(new Complex(5.0,  8.0)))  // ...
                            .add(new Complex(6.0,  7.0).multiply(new Complex(6.0,  7.0))); // ...
        final double[] expectedNormSqEval = {-468.0, 364.0}; //                   = -468.0 + 364.0 i
        assertArrayEquals(expectedNormSqEval, cv.normSq());
        assertEquals(FastMath.sqrt(new Complex(cv.normSq()[0], cv.normSq()[1])),
                                   new Complex(cv.norm()[0], cv.norm()[1]));
        assertEquals(FastMath.sqrt(expectedNormSq), new Complex(cv.norm()[0], cv.norm()[1]));
    }
    
    /** Test {@link ComplexVector#hermitianNorm()} and {@link ComplexVector#hermitianNormSq()} with a
     * {@link ComplexVector} with only real components */
    @Test
    void testHermitianNormAllReal() {
        final ComplexVector cv = new ComplexVector(new double[] {2, 2, 2, 2, 2, 2},
                                                   new double[] {0, 0, 0, 0, 0, 0});
        assertEquals(cv.hermitianInnerProduct(cv).getReal(), cv.hermitianNormSq()); // alt. method
        assertEquals(FastMath.sqrt(cv.hermitianNormSq()),    cv.hermitianNorm());
        assertEquals(2.0 * FastMath.sqrt(6.0),               cv.hermitianNorm());
    }
    
    /** Test {@link ComplexVector#hermitianNorm()} and {@link ComplexVector#hermitianNormSq()} with a
     * {@link ComplexVector} with only imaginary components */
    @Test
    void testHermitianNormAllImaginary() {
        final ComplexVector cv = new ComplexVector(new double[] {0, 0, 0, 0, 0, 0}, new double[] {2, 2, 2, 2, 2, 2});
        assertEquals(cv.hermitianInnerProduct(cv).getReal(), cv.hermitianNormSq()); // alt. method
        assertEquals(FastMath.sqrt(cv.hermitianNormSq()),    cv.hermitianNorm());
        assertEquals(2.0 * FastMath.sqrt(6.0),               cv.hermitianNorm());
    }
    
    /** Test {@link ComplexVector#hermitianNorm()} and {@link ComplexVector#hermitianNormSq()} with a
     * {@link ComplexVector} with mixed real and imaginary components */
    @Test
    void testHermitianNormAllMixed() {
        final ComplexVector cv1 = new ComplexVector(new double[] {0, 2, 0, 2, 0, 2}, new double[] {2, 0, 2, 0, 2, 0});
        assertEquals(cv1.hermitianInnerProduct(cv1).getReal(), cv1.hermitianNormSq()); // alt. method
        assertEquals(FastMath.sqrt(cv1.hermitianNormSq()),     cv1.hermitianNorm());
        assertEquals(2.0 * FastMath.sqrt(6.0),                 cv1.hermitianNorm());
        
        final ComplexVector cv2 = new ComplexVector(new double[] { 1, 2, 3,  4,  5,  6 },
                                                    new double[] { 7, 8, 9, 10, 11, 12 });
        assertEquals(cv2.hermitianInnerProduct(cv2).getReal(), cv2.hermitianNormSq()); // alt. method
        assertEquals(FastMath.sqrt(cv2.hermitianNormSq()),     cv2.hermitianNorm());
        
        assertEquals(FastMath.sqrt(1 + 4 + 9 + 16 + 25 + 36 + 49 + 64 + 81 + 100 + 121 + 144), // sqrt(650.0)
                     cv2.hermitianNorm());
    }
    
    /** Test {@link ComplexVector#hermitianNorm()} and {@link ComplexVector#hermitianNormSq()} */
    @Test
    void testHermitianNorm() {
        final ComplexVector cv = new ComplexVector(V);
        
        // expectedHNormSq is the sum of the squares of the norms of the elements of the vector
        final double expectedHNormSq = (1 + 144) + (4 + 121) + (9 + 100) + (16 + 81) + (25 + 64) + (36 + 49); // 650.0
        assertEquals(FastMath.sqrt(cv.hermitianNormSq()), cv.hermitianNorm());
        assertEquals(FastMath.sqrt(expectedHNormSq),      cv.hermitianNorm());
    }

    /** Test {@link ComplexVector#getEntry(int)},
     *       {@link ComplexVector#setEntry(int, double, double)},
     *       {@link ComplexVector#setEntry(int, Complex)}, and
     *       {@link ComplexVector#unsafeSetEntry(int, double, double)} */
    @Test
    void testGetSetEntry() {
        final ComplexVector cv1 = new ComplexVector(V);
        
        assertEquals(new Complex(2, 11), cv1.getEntry(1));

        cv1.setEntry(1, 0, 24);
        assertEquals(new Complex(0, 24), cv1.getEntry(1));

        cv1.setEntry(2, new Complex(2, 2));
        assertEquals(new Complex(2, 2), cv1.getEntry(2));

        cv1.unsafeSetEntry(1, 10, 10);
        assertEquals(new Complex(10, 10), cv1.getEntry(1));

        assertThrows(MathIllegalArgumentException.class, () -> cv1.setEntry(20, 2.0, 2.0));
    }

    /** Test {@link ComplexVector#getSubVector(int, int)},
     *       {@link ComplexVector#getSubVectorDouble(int, int)},
     *       {@link ComplexVector#setSubVector(int, FieldVector)},
     *       {@link ComplexVector#setEntry(int, Complex)},
     *       {@link ComplexVector#set(double, double)}, and
     *       {@link ComplexVector#set(Complex)} */
    @Test
    void testGetSetSubVector() {
        final ComplexVector        cv1 = new ComplexVector(V);
        final ComplexVector        sv1 = new ComplexVector(new double[][] {{-3.0, -4.0, -5.0}, {-10.0, -9.0, -8.0}});
        final FieldVector<Complex> sv2 = new ArrayFieldVector<>(sv1.getData());

        // getSubVector
        assertEquals(new ComplexVector(new double[][] {{3.0, 4.0, 5.0}, {10.0, 9.0, 8.0}}),
                     cv1.getSubVector(2, 3));

        // setSubVector
        cv1.setSubVector(2, sv2);
        assertEquals(sv1, cv1.getSubVector(2, 3)); 

        // getSubVectorDouble
        assertArrayEquality(new double[][] {{-3.0, -4.0, -5.0}, {-10.0, -9.0, -8.0}}, 
                     cv1.getSubVectorDouble(2, 3));

        // set(double, double)
        cv1.set(0, 24);
        assertEquals(new ComplexVector(new double[][] {{0, 0, 0}, {24, 24, 24}}), cv1);

        // set(Complex)
        cv1.set(Complex.valueOf(1, 2));
        assertEquals(new ComplexVector(new double[][] {{1, 1, 1}, {2, 2, 2}}), cv1);

        assertThrows(MathIllegalArgumentException.class, () -> cv1.setSubVector(20, sv1));
        assertThrows(MathIllegalArgumentException.class, () -> cv1.getSubVector(9, 10));
        assertThrows(MathIllegalArgumentException.class, () -> cv1.getSubVector(-1, 10));
    }

    /** Test {@link ComplexVector#equals(Object)}, */
    @SuppressFBWarnings({ "H", "C", "EC" }) // is okay to pass in ArrayFieldVector
    @SuppressWarnings({ "unlikely-arg-type" }) // meant to fail
    @Test
    void testEquals() {
        final ComplexVector        cv1 = new ComplexVector(V);
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(cv1.getData());
        final ComplexVector        sv1 = new ComplexVector(new double[][] {{-3.0, -4.0, -5.0}, {-0.0, -0.0, 0}});
        final FieldVector<Complex> fv2 = new ArrayFieldVector<>(cv1.copy().negate());

        assertTrue(sv1.equals(sv1.conjugate()));
        assertTrue(cv1.equals(fv1));
        assertTrue(cv1.equals(cv1));
        assertTrue(cv1.equals(V));
        assertTrue(cv1.getSubVector(2, 3)
                      .equals(new ComplexVector(new double[][] {{3.0, 4.0, 5.0}, {10.0, 9.0, 8.0}})));

        assertFalse(cv1.equals(cv1.getData()));
        assertFalse(cv1.equals(fv2));
        assertFalse(cv1.equals(ComplexVector.of(fv2)));
    }

    /** Test {@link ComplexVector#addEntry(int, Complex)},
     *       {@link ComplexVector#addEntry(int, double, double)},
     *       {@link ComplexVector#add(ComplexVector)}*/
    @Test
    void testAdd() {        
        final Complex z = new Complex(1, 2);

        final ComplexVector v1     = new ComplexVector(new double[][] { { 1, 1, 1, 1, 1 },
                                                                        { 2, 2, 2, 2, 2 } });
        final ComplexVector v1Copy = new ComplexVector(v1);
        
        final double[][] expected1 = new double[][] { { 1, 2, 1, 1, 1 }, { 2, 4, 2, 2, 2 } };

        v1.addEntry(1, z.getReal(), z.getImaginary());
        v1Copy.addEntry(1, z);
        
        assertArrayEquality(expected1, v1.getDataRef());
        assertArrayEquality(expected1, v1Copy.getDataRef());

        final ComplexVector v2     = new ComplexVector(new double[][] { { -1,  1,  0, 3,  1 },
                                                                        {  2, 2.1, 2, 2, -1 } });
        final double[][] expected2 = new double[][] { { 0, 3, 1, 4, 2 }, { 4, 6.1, 4, 4, 1 } };

        assertArrayEquality(expected2, v1.add(v2).getDataRef());

    }

    /** Test {@link ComplexVector#multiplyEntry(int, double, double)}, 
     *       {@link ComplexVector#multiplyEntry(int, Complex)}, and 
     *       {@link ComplexVector#multiplyEntry(int, double)} */
    @Test
    void testMultiplyEntry() {
        final double[] doubleFactor = new double[] {-3, 2};
        final Complex complexFactor = new Complex(-3, 2);

        final ComplexVector v      = new ComplexVector(new double[][] { { 1, 1, 1, 1, 0 }, { 2, 2, 2, 2, 0 } });
        final ComplexVector vCopy  = new ComplexVector(v);
        final ComplexVector vCopy1 = new ComplexVector(v);
        final ComplexVector vCopy2 = new ComplexVector(v);

        final ComplexVector expected  = new ComplexVector(new double[][] { { 1, -7, 1, 1, 0 },
                                                                           { 2, -4, 2, 2, 0 } });
        
        final ComplexVector expected1 = new ComplexVector(new double[][] { { 1, -5, 1, 1, 0 },
                                                                           { 2,  5, 2, 2, 0 } });
        final ComplexVector expected2 = new ComplexVector(new double[][] { { 1,  3, 1, 1, 0 },
                                                                           { 2,  6, 2, 2, 0 } });
        
        v.multiplyEntry(1, doubleFactor[0], doubleFactor[1]);
        vCopy.multiplyEntry(1, complexFactor);
        vCopy1.multiplyEntry(1, 1, 3);
        vCopy2.multiplyEntry(1, 3);

        assertEquals(expected, v);
        assertEquals(expected, vCopy);
        assertEquals(expected1, vCopy1);
        assertEquals(expected2, vCopy2);

        assertThrows(MathIllegalArgumentException.class, () -> v.multiplyEntry(6, complexFactor));
    }

    /** Test {@link ComplexVector#divideEntry(int, double, double)}, 
     *       {@link ComplexVector#divideEntry(int, Complex)}, and 
     *       {@link ComplexVector#divideEntry(int, double)} */
    @Test
    void testDivideEntry() {
        final double[] doubleFactor = new double[] {-3, 2};
        final Complex complexFactor = new Complex(-3, 2);

        final ComplexVector v      = new ComplexVector(new double[][] { { 1, 1, 1, 1, 0 }, { 2, 2, 2, 2, 0 } });
        final ComplexVector vCopy  = new ComplexVector(v);
        final ComplexVector vCopy1 = new ComplexVector(v);
        final ComplexVector vCopy2 = new ComplexVector(v);

        final ComplexVector expected  = new ComplexVector(new double[][] { { 1, 0.07692307692307691, 1, 1, 0 },
                                                                           { 2, -0.6153846153846154, 2, 2, 0 } });
        
        final ComplexVector expected1 = new ComplexVector(new double[][] { { 1, 0.7000000000000001, 1, 1, 0 },
                                                                           { 2,  -0.1, 2, 2, 0 } });
        final ComplexVector expected2 = new ComplexVector(new double[][] { { 1,  0.3333333333333333, 1, 1, 0 },
                                                                           { 2,  0.6666666666666666, 2, 2, 0 } });
        
        v.divideEntry(1, doubleFactor[0], doubleFactor[1]);
        vCopy.divideEntry(1, complexFactor);
        vCopy1.divideEntry(1, 1, 3);
        vCopy2.divideEntry(1, 3);

        assertEquals(expected, v);
        assertEquals(expected, vCopy);
        assertEquals(expected1, vCopy1);
        assertEquals(expected2, vCopy2);

        assertThrows(MathIllegalArgumentException.class, () -> v.divideEntry(6, complexFactor));
        assertThrows(IllegalArgumentException.class, () -> v.divideEntry(0, 0));
        assertThrows(IllegalArgumentException.class, () -> v.divideEntry(0, 0, 0));
    }

    /** Test {@link ComplexVector#addEntry(int, Complex)} and {@link ComplexVector#addEntry(int, double, double)} */
    @Test
    void testAddEntry() {
        final Complex z = new Complex(1, 2);

        final ComplexVector v1     = new ComplexVector(new double[][] { { 1, 1, 1, 1, 1 },
                                                                        { 2, 2, 2, 2, 2 } });
        final ComplexVector v1Copy = new ComplexVector(v1);
        
        final double[][] expected1 = new double[][] { { 1, 2, 1, 1, 1 }, { 2, 4, 2, 2, 2 } };

        v1.addEntry(1, z.getReal(), z.getImaginary());
        v1Copy.addEntry(1, z);
        
        assertArrayEquality(expected1, v1.getDataRef());
        assertArrayEquality(expected1, v1Copy.getDataRef());
    }

    // CHECKSTYLE.OFF: LineLength

    /** Test {@link ComplexVector#outerProduct(FieldVector)} */
    @Test
    void testOuterProduct() {
        final ComplexVector v1 = new ComplexVector(2, 3, 5, 7);
        final ComplexVector v2 = new ComplexVector(11, 13, 17, 19);

        final ComplexMatrix op = v1.outerProduct(v2);
        final String expected = String.join(System.lineSeparator(),
                                            " 22 26  34  38",
                                            " 33 39  51  57",
                                            " 55 65  85  95",
                                            " 77 91 119 133");
        TestUtils.testMultiLineToString(expected, op);

        final ComplexVector v3 = new ComplexVector(new double[][] { {  1,  2, -0.0,  4,  5,  6 },
                                                                    { 12, 11, -0.0, -9, -8, -7 } });
        final ComplexVector v4 = new ComplexVector(new double[][] { { -34, -29, -24, -19, -14, -9 },
                                                                    {  27,  28,  29,  30,  0.01, 32 } });

        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
            " -358 + -381 i,  -365 + -320 i,  -372 + -259 i,  -379 + -198 i,  -14.12 + -167.99 i,  -393 + -76 i",
            " -365 + -320 i,  -366 + -263 i,  -367 + -206 i,  -368 + -149 i,  -28.11 + -153.98 i,  -370 + -35 i",
            "    0 +    0 i,     0 +    0 i,     0 +    0 i,     0 +    0 i,    0.0  +    0.0  i,     0 +   0 i",
            "  107 +  414 i,   136 +  373 i,   165 +  332 i,   194 +  291 i,  -55.91 +  126.04 i,   252 + 209 i",
            "   46 +  407 i,    79 +  372 i,   112 +  337 i,   145 +  302 i,  -69.92 +  112.05 i,   211 + 232 i",
            "  -15 +  400 i,    22 +  371 i,    59 +  342 i,    96 +  313 i,  -83.93 +   98.06 i,   170 + 255 i"),
                                            v3.outerProduct(v4).toString());
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
            " -358.0  + -381.0  i,  -365.0  + -320.0  i,  0 + 0 i,  107.0  + 414.0  i,   46.0  + 407.0  i,  -15.0  + 400.0  i",
            " -365.0  + -320.0  i,  -366.0  + -263.0  i,  0 + 0 i,  136.0  + 373.0  i,   79.0  + 372.0  i,   22.0  + 371.0  i",
            " -372.0  + -259.0  i,  -367.0  + -206.0  i,  0 + 0 i,  165.0  + 332.0  i,  112.0  + 337.0  i,   59.0  + 342.0  i",
            " -379.0  + -198.0  i,  -368.0  + -149.0  i,  0 + 0 i,  194.0  + 291.0  i,  145.0  + 302.0  i,   96.0  + 313.0  i",
            "  -14.12 + -167.99 i,   -28.11 + -153.98 i,  0 + 0 i,  -55.91 + 126.04 i,  -69.92 + 112.05 i,  -83.93 +  98.06 i",
            " -393.0  +  -76.0  i,  -370.0  +  -35.0  i,  0 + 0 i,  252.0  + 209.0  i,  211.0  + 232.0  i,  170.0  + 255.0  i"),
                                            v4.outerProduct(v3).toString());
    }
    // CHECKSTYLE.ON: LineLength

    /** Test {@link ComplexVector#mapMultiply(double, double)},
     *       {@link ComplexVector#mapMultiply(double)},
     *       {@link ComplexVector#mapMultiply(Complex)},
     *       {@link ComplexVector#mapMultiplyToSelf(double, double)}, and
     *       {@link ComplexVector#mapMultiplyToSelf(Complex)} */
    @Test
    void testMapMultiply() {
        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };

        final double[][] cv2Data = new double[][] { { -34, -29, -24, -19, -14, -9 },
                                                    {  27,  28,  29,  30,  31, 32 } };
        final double[][] cv3Data = new double[][] { {  2,  4,  6,  8, 10, 12 },
                                                    { 24, 22, 20, 18, 16, 14 } };
        final ComplexVector cv1 = new ComplexVector(cv1Data);
        final ComplexVector cv2 = new ComplexVector(cv2Data);

        // mapMultiply(double, double)
        assertEquals(cv2, cv1.mapMultiply(2, 3));

        // mapMultiply(double)
        assertArrayEquality(cv3Data, cv1.mapMultiply(2).getDataRef());
        assertEquals(cv1.mapMultiply(2), cv1.mapMultiply(new Complex(2, 0)));

        // mapMultiply(Complex)
        assertEquals(cv1.mapMultiply(2), cv1.mapMultiply(new Complex(2, 0)));

        // mapMultiplyToSelf(double, double)
        assertEquals(cv2, cv1.copy().mapMultiplyToSelf(2, 3));

        // mapMultiplyToSelf(Complex)
        assertEquals(cv2, cv1.copy().mapMultiplyToSelf(new Complex(2, 3)));
    }

    /** Test {@link ComplexVector#mapDivide(double, double)},
     *       {@link ComplexVector#mapDivide(double)},
     *       {@link ComplexVector#mapDivide(Complex)},
     *       {@link ComplexVector#mapDivideToSelf(double, double)}, and
     *       {@link ComplexVector#mapDivideToSelf(Complex)} */
    @Test
    void testMapDivide() {
        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };

        final double[][] cv2Data = new double[][] { { 3.25, 3.25, 3.25, 3.25, 3.25, 3.25 },
                                                    { 2.75, 2.25, 1.75, 1.25, 0.75, 0.25 } };
        final double[][] cv3Data = new double[][] { { 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 },
                                                    { 6.0, 5.5, 5.0, 4.5, 4.0, 3.5 } };

        final ComplexVector cv1 = new ComplexVector(cv1Data);
        final ComplexVector cv2 = new ComplexVector(cv2Data);
        final ComplexVector cv3 = new ComplexVector(cv3Data);

        // mapDivide(double, double)
        assertEquals(cv2, cv1.mapDivide(2, 2));

        // mapDivide(double)
        assertEquals(cv3, cv1.mapDivide(2));

        // mapDivide(Complex)
        assertEquals(cv3, cv1.mapDivide(new Complex(2, 0)));

        // mapDivideToSelf(double, double)
        assertEquals(cv2, cv1.copy().mapDivideToSelf(2, 2));

        // mapDivideToSelf(Complex)
        assertEquals(cv2, cv1.copy().mapDivideToSelf(new Complex(2, 2)));

        assertThrows(IllegalArgumentException.class, () -> cv1.mapDivide(0, 0));
    }

    /** Test {@link ComplexVector#mapAdd(double, double)},
     *       {@link ComplexVector#mapAdd(double)},
     *       {@link ComplexVector#mapAdd(Complex)},
     *       {@link ComplexVector#mapAddToSelf(double, double)}, 
     *       {@link ComplexVector#mapAddToSelf(Complex)} */
    @Test
    void testMapAdd() {
        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };

        final double[][] cv2Data = new double[][] { {  3,  4,  5,  6,  7, 8 },
                                                    { 14, 13, 12, 11, 10, 9 } };

        final double[][] cv3Data = new double[][] { {  3,  4,  5, 6, 7, 8 },
                                                    { 12, 11, 10, 9, 8, 7 } }; 

        final ComplexVector cv1 = new ComplexVector(cv1Data);
        final ComplexVector cv2 = new ComplexVector(cv2Data);
        final ComplexVector cv3 = new ComplexVector(cv3Data);

        // mapAdd(double, double)
        assertEquals(cv2, cv1.mapAdd(2, 2));
        
        // mapAdd(double)
        assertEquals(cv3, cv1.mapAdd(2));

        // mapAdd(Complex)
        assertEquals(cv2, cv1.mapAdd(new Complex(2, 2)));

        // mapAdd(double, double)
        assertEquals(cv2, cv1.copy().mapAddToSelf(2, 2));

        // mapAddToSelf(Complex)
        assertEquals(cv2, cv1.copy().mapAddToSelf(new Complex(2, 2)));
    }

    /** Test {@link ComplexVector#mapSubtract(double, double)},
     *       {@link ComplexVector#mapSubtract(double)},
     *       {@link ComplexVector#mapSubtract(Complex)},
     *       {@link ComplexVector#mapSubtractToSelf(double, double)}, and
     *       {@link ComplexVector#mapSubtractToSelf(Complex)} */
    @Test
    void testMapSubtract() {
        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };

        final double[][] cv2Data = new double[][] { { -21, -20, -19, -18, -17, -16 },
                                                    { -10, -11, -12, -13, -14, -15 } };
        final double[][] cv3Data = new double[][] { { -1,  0,  1, 2, 3, 4 },
                                                    { 12, 11, 10, 9, 8, 7 } }; 
        
        final ComplexVector cv1 = new ComplexVector(cv1Data);
        final ComplexVector cv2 = new ComplexVector(cv2Data);
        final ComplexVector cv3 = new ComplexVector(cv3Data);

        // mapSubtract(double, double)
        assertEquals(cv2, cv1.mapSubtract(22, 22));

        // mapSubtract(double)
        assertEquals(cv3, cv1.mapSubtract(2));

        // mapSubtract(Complex)
        assertEquals(cv2, cv1.mapSubtract(new Complex(22, 22)));

        // mapSubtractToSelf(double, double)
        assertEquals(cv3, cv1.mapSubtractToSelf(new Complex(2, 0)));
        assertEquals(cv1, cv2.copy().mapSubtractToSelf(-20, -22));
    }

    /** Test {@link ComplexVector#mapInv()} and {@link ComplexVector#mapInvToSelf()} */
    @Test 
    void testMapInv() {
        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };
        final double[][] invData = new double[][] { {  0.0069,  0.0160, 0.0275, 0.0412, 0.0562, 0.0706},
                                                    { -0.0828, -0.0880, -.0917, -.0928, -.0899, -.0824} };

        final ComplexVector cv1      = new ComplexVector(cv1Data);
        final ComplexVector expected = new ComplexVector(invData);

        assertAbsEquals(expected, cv1.mapInv(), 0.0001, 0.0001);
        
        cv1.mapInvToSelf();
        assertAbsEquals(expected, cv1, 0.0001, 0.0001);
    }

    /** Test {@link ComplexVector#ebeMultiply(FieldVector)} and {@link ComplexVector#ebeDivide(FieldVector)} */
    @Test 
    void testEbes() {
        final ComplexVector cv1 = new ComplexVector(new double[] {  1,  2,  3, 4, 5, 6 },
                                                    new double[] { 12, 11, 10, 9, 8, 7 });
        final ComplexVector cv2 = new ComplexVector(new double[] {  2,  1,  0, -1, -2, -3 },  
                                                    new double[] {  1,  2,  3,  4,  5,  6 });
        
        final ComplexVector expectedMult = new ComplexVector(new double[] {-10, -20, -30, -40, -50, -60 },
                                                             new double[] { 25,  15,   9,   7,   9,  15 });
        final ComplexVector expectedDiv =  new ComplexVector(new double[] { 2.80, 4.80,  3.33,  1.88,  1.03,  0.53 },
                                                             new double[] { 4.60, 1.40, -1.00, -1.47, -1.41, -1.27 });

        final ComplexVector actualMult = cv1.ebeMultiply(cv2);
        assertEquals(expectedMult, actualMult);

        final ComplexVector actualDiv = cv1.ebeDivide(cv2);
        assertAbsEquals(actualDiv, expectedDiv, 0.01, 0.01);
    }

    /** Test {@link ComplexVector#append(FieldVector)}, {@link ComplexVector#append(Complex)} */
    @Test 
    void testAppend() {
        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };
        final double[][] sv1Data = new double[][] { { 6.25, 6.5, 6.75, 7 }, 
                                                    { 6.75, 6.5, 6.25, 6 } };
        final double[][] cv2Data = new double[][] { {  1,  2,  3, 4, 5, 6, 6.01 },
                                                    { 12, 11, 10, 9, 8, 7, 6.99 } };
        final double[][] cv3Data = new double[][] { {  1,  2,  3, 4, 5, 6, 6.25, 6.5, 6.75, 7 },
                                                    { 12, 11, 10, 9, 8, 7, 6.75, 6.5, 6.25, 6 } };
        
        final ComplexVector        cv1 = new ComplexVector(cv1Data);
        final ComplexVector        sv1 = new ComplexVector(sv1Data);
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(sv1);
        final ComplexVector        cv2 = new ComplexVector(cv2Data);
        final ComplexVector        cv3 = new ComplexVector(cv3Data);

        // append(Complex)
        assertEquals(cv2, cv1.append(Complex.valueOf(6.01, 6.99)));

        // append(ComplexVector)
        assertEquals(cv3, cv1.append(fv1));
        assertEquals(cv3, cv1.append(sv1));
    }

    /** Test {@link ComplexVector#absEquivalentTo(ComplexVector, double, double)} */
    @Test
    void testAbsEquivalentTo() {
        final double passAbsEps = 0.0001;
        final double failAbsEps = 0.000001;

        final double[][] cv1Data = new double[][] { {  1,  2,  3, 4, 5, 6 },
                                                    { 12, 11, 10, 9, 8, 7 } };
        final double[][] invData = new double[][] { {  0.0069,  0.0160,  0.0275,  0.0412,  0.0562,  0.0706},
                                                    { -0.0828, -0.0880, -0.0917, -0.0928, -0.0899, -0.0824} };

        final ComplexVector cv1      = new ComplexVector(cv1Data);
        final ComplexVector expected = new ComplexVector(invData);

        assertAbsEquals(expected, cv1.mapInv(), passAbsEps, passAbsEps);

        assertTrue(expected.absEquivalentTo(expected, passAbsEps, passAbsEps));
        assertTrue(expected.absEquivalentTo(cv1.mapInv(), passAbsEps, passAbsEps));

        assertFalse(expected.absEquivalentTo(cv1.mapInv(), failAbsEps, failAbsEps));
        assertFalse(expected.absEquivalentTo(expected.append(Complex.valueOf(1)), passAbsEps, passAbsEps));
    }

    /** Test {@link ComplexVector#projection(FieldVector)} */
    @Test
    void testUnimplemented() {
        final ComplexVector        cv1 = new ComplexVector(V);
        final FieldVector<Complex> fv1 = new ArrayFieldVector<>(cv1);

        assertThrows(UnsupportedOperationException.class, () -> cv1.projection(fv1));
    }
}