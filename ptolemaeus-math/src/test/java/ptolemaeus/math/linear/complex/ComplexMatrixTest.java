/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.linear.complex;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;
import static ptolemaeus.commons.TestUtils.assertLessThanOrEqualTo;

import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.Array2DRowFieldMatrix;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldMatrixChangingVisitor;
import org.hipparchus.linear.FieldMatrixPreservingVisitor;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.SparseFieldMatrix;
import org.hipparchus.random.RandomDataGenerator;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.Validation;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.householderreductions.ComplexHessenbergReduction;
import ptolemaeus.math.linear.householderreductions.ComplexQRDecomposition;
import ptolemaeus.math.linear.householderreductions.Householder;
import ptolemaeus.math.linear.real.MatrixTest;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** Test {@link ComplexMatrix} <br>
 * <br>
 * 
 * Where possible and convenient, we use {@link Array2DRowFieldMatrix} explicitly to get expected results and compare
 * against those.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com 
 * @author Catherine Doud, Catherine.A.Doud@lmco.com*/
public class ComplexMatrixTest {
    
    /** a {@link ComplexMatrix} for use in tests testing sub-matrix functionality */
    private static final ComplexMatrix SUB_MATRIX_TESTING = new ComplexMatrix(
            new double[][] {
                            {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9 },
                            { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 },
                            { 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 },
                            { 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 },
                            { 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 },
                            { 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 },
                            { 60, 61, 62, 63, 64, 65, 66, 67, 68, 69 },
                            { 70, 71, 72, 73, 74, 75, 76, 77, 78, 79 },
                            { 80, 81, 82, 83, 84, 85, 86, 87, 88, 89 },
                            { 90, 91, 92, 93, 94, 95, 96, 97, 98, 99 }  },
            new double[][] {
                            {  -0,  -1,  -2,  -3,  -4,  -5,  -6,  -7,  -8,  -9 },
                            { -10, -11, -12, -13, -14, -15, -16, -17, -18, -19 },
                            { -20, -21, -22, -23, -24, -25, -26, -27, -28, -29 },
                            { -30, -31, -32, -33, -34, -35, -36, -37, -38, -39 },
                            { -40, -41, -42, -43, -44, -45, -46, -47, -48, -49 },
                            { -50, -51, -52, -53, -54, -55, -56, -57, -58, -59 },
                            { -60, -61, -62, -63, -64, -65, -66, -67, -68, -69 },
                            { -70, -71, -72, -73, -74, -75, -76, -77, -78, -79 },
                            { -80, -81, -82, -83, -84, -85, -86, -87, -88, -89 },
                            { -90, -91, -92, -93, -94, -95, -96, -97, -98, -99 } 
                            });
    
    // CHECKSTYLE.OFF: LineLength
    /** A {@link ComplexMatrix} equal to {@link #FROM_DOUBLE_ARRS} but created using a {@code Complex[][]} */
    private static final ComplexMatrix FROM_COMPLEX_ARR = new ComplexMatrix(new Complex[][] {
        { Complex.valueOf(1, -1), Complex.valueOf(1, -1), Complex.valueOf(1, -1), Complex.valueOf(1, -1), Complex.valueOf(1, -1) },
        { Complex.valueOf(1, -1), Complex.valueOf(2, -2), Complex.valueOf(2, -2), Complex.valueOf(2, -2), Complex.valueOf(2, -2) },
        { Complex.valueOf(0,  0), Complex.valueOf(2, -2), Complex.valueOf(3, -3), Complex.valueOf(3, -3), Complex.valueOf(3, -3) },
        { Complex.valueOf(0,  0), Complex.valueOf(0,  0), Complex.valueOf(3, -3), Complex.valueOf(4, -4), Complex.valueOf(4, -4) },
        { Complex.valueOf(0,  0), Complex.valueOf(0,  0), Complex.valueOf(0,  0), Complex.valueOf(4, -4), Complex.valueOf(5, -5) }
    });
    // CHECKSTYLE.ON: LineLength
    
    /** A {@link ComplexMatrix} equal to {@link #FROM_COMPLEX_ARR} but created using two {@code double[][]}'s */
    private static final ComplexMatrix FROM_DOUBLE_ARRS = new ComplexMatrix(
            new double[][] {
                        { 1, 1, 1, 1, 1 },
                        { 1, 2, 2, 2, 2 },
                        { 0, 2, 3, 3, 3 },
                        { 0, 0, 3, 4, 4 },
                        { 0, 0, 0, 4, 5 }
                        },
            new double[][] {
                        { -1, -1, -1, -1, -1 },
                        { -1, -2, -2, -2, -2 },
                        {  0, -2, -3, -3, -3 },
                        {  0,  0, -3, -4, -4 },
                        {  0,  0,  0, -4, -5 }
                        });
    
    /** A 9x9 {@link ComplexMatrix} of all ones */
    private static final ComplexMatrix ALL_ONES_9X9 =
            new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
    
    /** A 3x3 {@link ComplexMatrix} of all twos */
    private static final ComplexMatrix ALL_TWOS_3X3 = new ComplexMatrix(new double[][] { { 2, 2, 2 },
                                                                                         { 2, 2, 2 },
                                                                                         { 2, 2, 2 } });
    
    /** A 12x9 {@link ComplexMatrix} of all ones */
    private static final ComplexMatrix ALL_ONES_12X9 =
            new ComplexMatrix(new double[][] {  { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
    
    /** A 5x2 {@link ComplexMatrix} of all twos */
    private static final ComplexMatrix ALL_TWOS_5X2 = new ComplexMatrix(
            new double[][] { { 2, 2 },
                             { 2, 2 },
                             { 2, 2 },
                             { 2, 2 },
                             { 2, 2 } },
            new double[][] { { 0, 0 },
                             { 0, 0 },
                             { 0, 0 },
                             { 0, 0 },
                             { 0, 0 } 
                            });

    /** A 9x9 (n x m) {@link ComplexMatrix} with diagonal of real values (1:n) */
    private static final ComplexMatrix DIAG_9X9 = new ComplexMatrix(
                              new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                               { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                                               { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                                               { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                                               { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                                               { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                                               { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                                               { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                                               { 0, 0, 0, 0, 0, 0, 0, 0, 9 } });
    
    /** This is just a convenience pass-through to
     * {@link #assertBackwardsStableError(ComplexMatrix, ComplexMatrix, int)} where we first convert the
     * {@link ComplexVector}s into row matrices.<br>
     * <br>
     * 
     * Assert that the the perturbation (or error) matrix {@code E = expected - actual} has small enough
     * {@link ComplexMatrix#getFrobeniusNorm()} to guarantee backward stability.
     * 
     * @param expected the expected {@link ComplexVector}
     * @param actual the actual {@link ComplexVector}
     * @param c a "small constant" by which we will multiply the acceptable error. Keep this as small as possible. This
     *        value is to help simulate that the error bounds are expressed in big-O notation. */
    public static void assertBackwardsStableError(final ComplexVector expected,
                                                  final ComplexVector actual,
                                                  final int c) {
        assertBackwardsStableError(ComplexMatrix.fromRows(expected), ComplexMatrix.fromRows(actual), c);
    }
    
    /** Assert that the the perturbation (or error) matrix {@code E = expected - actual} has small enough
     * {@link ComplexMatrix#getFrobeniusNorm()} to guarantee backward stability.<br>
     * <br>
     * 
     * Explicitly, we assert that {@code frob(E) <= c * m * n * frob(A)}, where {@code m} is the
     * {@link ComplexMatrix#getRowDimension()}, {@code m} is he {@link ComplexMatrix#getColumnDimension()}, and
     * {@code c} is a "small constant" .<br>
     * <br>
     * 
     * A simplification of error bound is quoted by Golub and Van Loan in <em>Matrix Computations, 4th Ed.</em> as a
     * bound computed by Wilkinson in <em>Algebraic Eigenvalue Problems</em> that ensures that using {@link Householder
     * Householder reflectors} to compute a {@link ComplexHessenbergReduction Hessenberg reduction} is a backward-stable
     * process.<br>
     * <br>
     * 
     * They quote, for real, square matrices {@code frob(E) <= c * n^2 * frob(A)}.<br>
     * <br>
     * 
     * I - Ryan M. - am not able to get a copy of that book - the primary source - as I write this to confirm whether
     * this bound can be arbitrarily extended to rectangular {@link ComplexMatrix complex matrices}, but it seems
     * reasonable and is similar to - if not the same as - other error-bounds I've seen while studying the QR algorithm.
     * 
     * @param expected the expected {@link ComplexMatrix}
     * @param actual the actual {@link ComplexMatrix}
     * @param c a "small constant" by which we will multiply the acceptable error. Keep this as small as possible. This
     *        value is to help simulate that the error bounds are expressed in big-O notation. */
    public static void assertBackwardsStableError(final ComplexMatrix expected,
                                                  final ComplexMatrix actual,
                                                  final int c) {
        MatrixUtils.checkSubtractionCompatible(expected, actual); // throws on dimension mismatch
        
        final ComplexMatrix E           = expected.minus(actual);
        final double        frobE       = E.getFrobeniusNorm();
        
        final int    mn                 = expected.getRowDimension() * expected.getColumnDimension();
        final double frobA              = actual.getFrobeniusNorm();
        final double maxAllowableError  = c * mn * Numerics.MACHINE_EPSILON * frobA;
        
        assertLessThanOrEqualTo(frobE, maxAllowableError, "the error-matrix 2-norm");
    }
    
    /** Compute and return a new {@link MatrixTest#nCyclicMatrix(int) n-cyclic matrix}, wrapped in a
     * {@link ComplexMatrix}.
     * 
     * @param n the desired dimension
     * @return the new n-cyclic matrix
     * 
     * @see MatrixTest#nCyclicMatrix(int) */
    public static ComplexMatrix nCyclicMatrix(final int n) {
        return new ComplexMatrix(MatrixTest.nCyclicMatrix(n));
    }
    
    /** Create and return a new {@link MatrixTest#problematicDemmel(double) problematic Demmel matrix}, wrapped in a
     * {@link ComplexMatrix}.
     * 
     * @param eta the value (2e-14 &lt;= &eta; &lt;= 1e-6) to use in the internal {@code 2 x 2} sub-matrix of the
     *        problematic Demmel matrix.
     * @return the problematic Demmel matrix
     *        
     * @see MatrixTest#problematicDemmel(double) */
    public static ComplexMatrix problematicDemmel(final double eta) {
        return new ComplexMatrix(MatrixTest.problematicDemmel(eta));
    }
    
    /** Create and return a new random {@link ComplexMatrix} in perfect upper-N-Hessenberg form (perfect meaning that
     * the values under the first N sub-diagonals are identically equal to zero)
     * 
     * @param dim both the number of rows and columns of the square matrix
     * @param n the number of non-zero sub-diagonals
     * @param maxRowHermNorm the maximum {@link ComplexVector#hermitianNorm()} of any of the rows
     * @param randomSeed the random seed to use
     * @param allReal {@code true} to create a {@link ComplexMatrix} with only real entries (wraps the result of using
     *        {@link MatrixTest#randomMatrix(int, int, double, long)}), or {@code false} to create one with
     *        {@link Complex} entries.
     * @return a new random {@link ComplexMatrix} in perfect upper-N-Hessenberg form */
    public static ComplexMatrix randomUpperNHessenberg(final int dim,
                                                       final int n,
                                                       final double maxRowHermNorm,
                                                       final long randomSeed,
                                                       final boolean allReal) {
        Validation.requireGreaterThan(dim, 2, "dimension");
        Validation.requireLessThan(n, dim, "n");
        
        final ComplexMatrix answer = randomComplexMatrix(dim, dim, maxRowHermNorm, randomSeed, allReal);
        for (int i = 1; i < dim; i++) {
            for (int j = 0; j < i - n; j++) {
                answer.setEntry(i, j, Complex.ZERO);
            }
        }
        
        return answer;
    }
    
    /** Create and return a new random {@link ComplexMatrix} in perfect upper-triangular form (perfect meaning that the
     * values under the main diagonal are identically equal to zero)
     * 
     * @param dim both the number of rows and columns of the square matrix
     * @param maxRowHermNorm the maximum {@link ComplexVector#hermitianNorm()} of any of the rows
     * @param randomSeed the random seed to use
     * @param allReal {@code true} to create a {@link ComplexMatrix} with only real entries (wraps the result of using
     *        {@link MatrixTest#randomMatrix(int, int, double, long)}), or {@code false} to create one with
     *        {@link Complex} entries.
     * @return a new random {@link ComplexMatrix} in perfect upper-triangular form */
    public static ComplexMatrix randomUpperTriangular(final int dim,
                                                      final double maxRowHermNorm,
                                                      final long randomSeed,
                                                      final boolean allReal) {
        return randomUpperNHessenberg(dim, 0, maxRowHermNorm, randomSeed, allReal);
    }
    
    /** Create a random {@link ComplexMatrix} by stacking a series of random {@link ComplexVector}s
     * created using {@link ComplexVectorTest#randomComplexVector(int, double, long)}
     * 
     * @param numRows the number of rows
     * @param numCols the number of columns
     * @param maxRowHermNorm the maximum {@link ComplexVector#hermitianNorm()} of any of the rows
     * @param fracNonZero the <em>approximate</em> fraction of elements to set to something non-zero; e.g., if
     *        {@code fracNonZero == 0.9}, approximately (1.0 - 0.9) * 100% = 10% of the elements of the returned answer
     *        will be identically zero.
     * @param randomSeed the random seed to use
     * @param allReal {@code true} to create a {@link ComplexMatrix} with only real entries (wraps the result of using
     *        {@link MatrixTest#randomMatrix(int, int, double, long)}), or {@code false} to create one with
     *        {@link Complex} entries.
     *        
     * @return the new random {@link ComplexMatrix} */
    public static ComplexMatrix randomComplexMatrix(final int numRows,
                                                    final int numCols,
                                                    final double maxRowHermNorm,
                                                    final double fracNonZero,
                                                    final long randomSeed,
                                                    final boolean allReal) {
        Validation.requireWithinRange(fracNonZero, 0.0, 1.0, "fracNonZero");
        
        final ComplexMatrix       A   = randomComplexMatrix(numRows, numCols, maxRowHermNorm, randomSeed, allReal);
        final RandomDataGenerator rdg = new RandomDataGenerator(randomSeed);
        for (int i = 0; i < A.getColumnDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                if (rdg.nextDouble() > fracNonZero) {
                    A.setEntry(i, j, Complex.ZERO);
                }
            }
        }
        
        return A;
    }
    
    /** Create a random square {@link ComplexMatrix} by stacking a series of random {@link ComplexVector}s
     * created using {@link ComplexVectorTest#randomComplexVector(int, double, long)}. All entries will be complex, and
     * each each row will have {@link ComplexVector#hermitianNorm()}{@code  == 1.0}
     * 
     * @param dim the number of rows and columns
     * @param randomSeed the random seed to use
     * 
     * @return the new random square {@link ComplexMatrix} */
    public static ComplexMatrix randomSquare(final int dim, final long randomSeed) {
        return randomComplexMatrix(dim, dim, 1.0, randomSeed, false);
    }
    
    /** Create a random {@link ComplexMatrix#isHermitian(Complex) Hermitian} matrix
     * 
     * @param dim the number of rows and columns
     * @param randomSeed the random seed to use
     * 
     * @return the random {@link ComplexMatrix#isHermitian(Complex) Hermitian} matrix */
    public static ComplexMatrix randomHermitian(final int dim, final long randomSeed) {
        final ComplexMatrix S = randomSquare(dim, randomSeed);
        
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (i != j) {
                    final double re = (S.getEntryRe(i, j) + S.getEntryRe(j, i)) / 2.0;
                    final double im = (S.getEntryIm(i, j) + S.getEntryIm(j, i)) / 2.0;
                    
                    S.unsafeSetEntry(i, j, re,  im);
                    S.unsafeSetEntry(j, i, re, -im);
                }
            }
        }
        
        return S;
    }
    
    /** Create a random {@link ComplexMatrix} by stacking a series of random {@link ComplexVector}s
     * created using {@link ComplexVectorTest#randomComplexVector(int, double, long)}. All entries will be complex, and
     * each each row will have {@link ComplexVector#hermitianNorm()}{@code  == 1.0}
     * 
     * @param numRows the number of rows
     * @param numCols the number of columns
     * @param randomSeed the random seed to use
     * 
     * @return the new random {@link ComplexMatrix}
     * 
     * @see #randomComplexMatrix(int, int, double, long, boolean)
     * @see #randomComplexMatrix(int, int, double, double, long, boolean) */
    public static ComplexMatrix randomComplexMatrix(final int numRows, final int numCols, final long randomSeed) {
        return randomComplexMatrix(numRows, numCols, 1.0, randomSeed, false);
    }
    
    /** Create a random {@link ComplexMatrix} by stacking a series of random {@link ComplexVector}s
     * created using {@link ComplexVectorTest#randomComplexVector(int, double, long)}
     * 
     * @param numRows the number of rows
     * @param numCols the number of columns
     * @param maxRowHermNorm the maximum {@link ComplexVector#hermitianNorm()} of any of the rows
     * @param randomSeed the random seed to use
     * @param allReal {@code true} to create a {@link ComplexMatrix} with only real entries (wraps the result of using
     *        {@link MatrixTest#randomMatrix(int, int, double, long)}), or {@code false} to create one with
     *        {@link Complex} entries.
     *        
     * @return the new random {@link ComplexMatrix} */
    public static ComplexMatrix randomComplexMatrix(final int numRows,
                                                    final int numCols,
                                                    final double maxRowHermNorm,
                                                    final long randomSeed,
                                                    final boolean allReal) {
        if (allReal) {
            return new ComplexMatrix(MatrixTest.randomMatrix(numRows, numCols, maxRowHermNorm, randomSeed));
        }
        
        final Complex[][] arr = new Complex[numRows][];
        
        for (int row = 0; row < numRows; row++) {
            final int seedMult = (1 + row) * numCols;
            arr[row] = ComplexVectorTest.randomComplexVector(numCols,
                                                             maxRowHermNorm,
                                                             seedMult * randomSeed).toArray(); // clones
        }
        
        return new ComplexMatrix(arr);
    }
    
    /** Assert that the elements of the two matrices are equivalent according to
     * {@link ComplexMatrix#absEquivalentTo(ComplexMatrix, double, double)}
     * 
     * @param E the expected {@link ComplexMatrix}
     * @param A the actual {@link ComplexMatrix}
     * @param reTol the real-number tolerance
     * @param imTol the imaginary-number tolerance */
    public static void assertAbsEquals(final ComplexMatrix E,
                                       final ComplexMatrix A,
                                       final double reTol,
                                       final double imTol) {
        assertAbsEquals(E, A, reTol, imTol, () -> "");
    }
    
    /** Assert that the elements of the two matrices are equivalent according to
     * {@link ComplexMatrix#absEquivalentTo(ComplexMatrix, double, double)}
     * 
     * @param E the expected {@link ComplexMatrix}
     * @param A the actual {@link ComplexMatrix}
     * @param reTol the real-number tolerance
     * @param imTol the imaginary-number tolerance
     * @param addlInfoSup a {@link Supplier} of additional information for the JUnit failure-string in case of a
     *        failure */
    public static void assertAbsEquals(final ComplexMatrix E,
                                       final ComplexMatrix A,
                                       final double reTol,
                                       final double imTol,
                                       final Supplier<String> addlInfoSup) {
        assertTrue(E.absEquivalentTo(A, reTol, imTol),
                   () -> {
                       final ComplexMatrix error = A.minus(E);
                       
                       double worstReError = 0.0;
                       double worstImError = 0.0;
                       for (int i = 0; i < error.getRowDimension(); i++) {
                           for (int j = 0; j < error.getColumnDimension(); j++) {
                               
                               final Complex ij = error.getEntry(i, j);
                               worstReError = FastMath.max(worstReError, FastMath.abs(ij.getReal()));
                               worstImError = FastMath.max(worstImError, FastMath.abs(ij.getImaginary()));
                           }
                       }
                       
                       final Complex worstEntryError = Complex.valueOf(worstReError, worstImError);
                       final StringBuilder errMsg = new StringBuilder();
                       
                       final String addlInfo = addlInfoSup == null ? "" : addlInfoSup.get();
                       if (!addlInfo.isEmpty()) {
                           errMsg.append(addlInfo)
                                 .append(System.lineSeparator());
                       }
                       
                       errMsg.append("Error:")
                             .append(System.lineSeparator())
                             .append(error)
                             .append(System.lineSeparator())
                             .append("Worst error: ")
                             .append(worstEntryError);
                       return errMsg.toString();
                   });
    }
    
    /** Construct a random {@link ComplexMatrix} with the desired Eigenvalues.<br>
     * <br>
     * 
     * See {@link #randomEigvalMatrix(ComplexVector, long)} for details.
     * 
     * @param eigenvalues the desired Eigenvalues
     * @param seed the random seed for constructing the {@code S} matrix
     * @return the matrix with the desired Eigenvalues */
    public static ComplexMatrix randomEigvalMatrix(final long seed, final  Complex... eigenvalues) {
        return randomEigvalMatrix(new ComplexVector(eigenvalues), seed);
    }
    
    /** Construct a random {@link ComplexMatrix} with the desired Eigenvalues.<br>
     * <br>
     * 
     * If a {@link ComplexMatrix} {@code S} is invertible, then <code>A = S<sup>-1</sup>&Lambda;S</code> is similar to -
     * i.e., has the same Eigenvalues as - <code>&Lambda;</code>; thus, to create a matrix with the desired Eigenvalues,
     * we construct <code>&Lambda;</code> as a diagonal matrix with the desired Eigenvalues on the diagonal, create a
     * random matrix {@code S}, and solve for {@code A} in <code>SA = &Lambda;S</code>.<br>
     * <br>
     * 
     * Some notes/caveats:
     * <ul>
     * <li>The set of non-invertible matrices has <em>Lebesgue measure zero</em> (which approximately means that they're
     * extremely uncommon among the set of all matrices), so it's extremely unlikely that {@code S} won't be invertible,
     * but it's something to keep in mind when using this method.</li>
     * 
     * <li>This operation relies on solving a system of equations (implicitly computing the inverse), so it's more
     * stable than computing the inverse explicitly, but there is a limit to how well the desired Eigenvalues will be
     * reflected in the returned matrix.</li>
     * </ul>
     * 
     * @param eigenvalues the desired Eigenvalues
     * @param seed the random seed for constructing the {@code S} matrix
     * @return the matrix with the desired Eigenvalues */
    public static ComplexMatrix randomEigvalMatrix(final ComplexVector eigenvalues, final long seed) {
        final int           n = eigenvalues.getDimension();
        final ComplexMatrix L = ComplexMatrix.diagonal(eigenvalues); // L -> Lambda
        final ComplexMatrix S = ComplexQRDecomposition.of(randomComplexMatrix(n, n, seed)).getQ();
        
        return LinearSolver.defaultSolver().solve(S, L.times(S));
    }
    
    /** Test {@link ComplexMatrix#constant(double, double, int, int)} */
    @Test
    void testConstant() {
        final ComplexMatrix A = ComplexMatrix.constant(5.0, 7.0, 10, 5);
        
        assertEquals(10, A.getRowDimension());
        assertEquals(5, A.getColumnDimension());
        
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                assertEquals(Complex.valueOf(5.0, 7.0), A.getEntry(i, j));
            }
        }
    }
    
    /** Test {@link ComplexMatrix#ones(int, int)} */
    @Test
    void testOnes() {
        final ComplexMatrix A = ComplexMatrix.ones(10, 5);
        
        assertEquals(10, A.getRowDimension());
        assertEquals(5, A.getColumnDimension());
        
        for (int i = 0; i < A.getRowDimension(); i++) {
            for (int j = 0; j < A.getColumnDimension(); j++) {
                assertEquals(Complex.ONE, A.getEntry(i, j));
            }
        }
    }
    
    /** Test {@link ComplexMatrix#zero(int, int)} */
    @Test
    void testZero() {
        final ComplexMatrix zero1x1  = ComplexMatrix.zero(1, 1);
        final ComplexMatrix zero2x5  = ComplexMatrix.zero(2, 5);
        final ComplexMatrix zero10x5 = ComplexMatrix.zero(10, 5);
        
        for (final ComplexMatrix z : new ComplexMatrix[] { zero1x1, zero2x5, zero10x5 }) {
            assertEquals(0.0, z.getFrobeniusNorm()); // zero tolerance
        }
        
        assertEquals(1, zero1x1.getRowDimension());
        assertEquals(1, zero1x1.getColumnDimension());
        
        assertEquals(2, zero2x5.getRowDimension());
        assertEquals(5, zero2x5.getColumnDimension());
        
        assertEquals(10, zero10x5.getRowDimension());
        assertEquals(5, zero10x5.getColumnDimension());
    }
    
    /** Test {@link ComplexMatrix#isWide()}, {@link ComplexMatrix#isTall()}, and {@link ComplexMatrix#isSquare()} */
    @Test
    void testWideTallSquare() {
        final ComplexMatrix tall = ALL_ONES_12X9;
        assertTrue(tall.isTall());
        assertFalse(tall.isWide());
        assertFalse(tall.isSquare());
        
        final ComplexMatrix wide = tall.conjugateTranspose();
        assertFalse(wide.isTall());
        assertTrue(wide.isWide());
        assertFalse(wide.isSquare());
        
        final ComplexMatrix square = ALL_ONES_9X9;
        assertFalse(square.isTall());
        assertFalse(square.isWide());
        assertTrue(square.isSquare());
    }
    
    /** Test {@link ComplexMatrix#asReal(double)} */
    @Test
    void testAsReal() {
        final double[][]    data = new double[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        final ComplexMatrix A    = new ComplexMatrix(data);
        A.setEntry(1, 1, Complex.valueOf(5, 1e-12));
        
        assertTrue(A.asReal(0.9e-12).isEmpty()); // imaginary arg limit smaller than largest imaginary part
        assertTrue(A.asReal(1e-12).isPresent());
        assertArrayEquality(data, A.asReal(1e-12).get().getDataRef());
    }
    
    /** @return a {@link FieldMatrixChangingVisitor} for testing. It sets diagonal elements to zero and leaves
     *         off-diagonal elements untouched. */
    private static FieldMatrixChangingVisitor<Complex> getDiagonalZeroingVisitor() {
        return new FieldMatrixChangingVisitor<>() {
            
            @Override
            public void start(final int rows,
                              final int columns,
                              final int startRow,
                              final int endRow,
                              final int startColumn,
                              final int endColumn) {
                // do nothing
            }
            
            @Override
            public Complex visit(final int row, final int column, final Complex value) {
                if (row == column) {
                    return Complex.ZERO;
                }
                return value;
            }
            
            @Override
            public Complex end() {
                return Complex.ZERO;
            }
        };
    }
    
    /** Test {@link ComplexMatrix#walkInRowOrder(FieldMatrixChangingVisitor)},
     *       {@link ComplexMatrix#walkInColumnOrder(FieldMatrixChangingVisitor)}, and
     *       {@link ComplexMatrix#walkInOptimizedOrder(FieldMatrixChangingVisitor)} */
    @Test
    void testFullChangingWalks() {
        final ComplexMatrix M1 = FROM_DOUBLE_ARRS.copy();
        final ComplexMatrix M2 = M1.copy();
        final ComplexMatrix M3 = M1.copy();
        
        final FieldMatrixChangingVisitor<Complex> visitor = getDiagonalZeroingVisitor();
        
        M1.walkInRowOrder(visitor);
        M2.walkInColumnOrder(visitor);
        M3.walkInOptimizedOrder(visitor);
        
        final String expected = String.join(System.lineSeparator(),
                " 0 +  0 i,  1 + -1 i,  1 + -1 i,  1 + -1 i,  1 + -1 i",
                " 1 + -1 i,  0 +  0 i,  2 + -2 i,  2 + -2 i,  2 + -2 i",
                " 0 +  0 i,  2 + -2 i,  0 +  0 i,  3 + -3 i,  3 + -3 i",
                " 0 +  0 i,  0 +  0 i,  3 + -3 i,  0 +  0 i,  4 + -4 i",
                " 0 +  0 i,  0 +  0 i,  0 +  0 i,  4 + -4 i,  0 +  0 i");
        
        TestUtils.testMultiLineToString(expected, M1);
        TestUtils.testMultiLineToString(expected, M2);
        TestUtils.testMultiLineToString(expected, M3);
    }
    
    /** Test {@link ComplexMatrix#walkInRowOrder(FieldMatrixChangingVisitor, int, int, int, int)},
     *       {@link ComplexMatrix#walkInColumnOrder(FieldMatrixChangingVisitor, int, int, int, int)}, and
     *       {@link ComplexMatrix#walkInOptimizedOrder(FieldMatrixChangingVisitor, int, int, int, int)} */
    @Test
    void testPartialChangingWalks() {
        final ComplexMatrix M1 = FROM_DOUBLE_ARRS.copy();
        final ComplexMatrix M2 = M1.copy();
        final ComplexMatrix M3 = M1.copy();
        
        final FieldMatrixChangingVisitor<Complex> visitor = getDiagonalZeroingVisitor();
        
        M1.walkInRowOrder(visitor,       1, 3, 1, 3); // hit everything but the upper-left and lower-right corners
        M2.walkInColumnOrder(visitor,    1, 3, 1, 3);
        M3.walkInOptimizedOrder(visitor, 1, 3, 1, 3);
        
        final String expected = String.join(System.lineSeparator(),
                " 1 + -1 i,  1 + -1 i,  1 + -1 i,  1 + -1 i,  1 + -1 i",
                " 1 + -1 i,  0 +  0 i,  2 + -2 i,  2 + -2 i,  2 + -2 i",
                " 0 +  0 i,  2 + -2 i,  0 +  0 i,  3 + -3 i,  3 + -3 i",
                " 0 +  0 i,  0 +  0 i,  3 + -3 i,  0 +  0 i,  4 + -4 i",
                " 0 +  0 i,  0 +  0 i,  0 +  0 i,  4 + -4 i,  5 + -5 i");
        
        TestUtils.testMultiLineToString(expected, M1);
        TestUtils.testMultiLineToString(expected, M2);
        TestUtils.testMultiLineToString(expected, M3);
    }
    
    /** @return a {@link FieldMatrixPreservingVisitor} that sums the diagonal elements */
    private static FieldMatrixPreservingVisitor<Complex> getDiagonalSummingVisitor() {
        return new FieldMatrixPreservingVisitor<>() {
            
            /** a running sum of diagonal elements */
            private Complex diagonalSum = Complex.ZERO;
            
            @Override
            public void start(final int rows,
                              final int columns,
                              final int startRow,
                              final int endRow,
                              final int startColumn,
                              final int endColumn) {
                this.diagonalSum = Complex.ZERO; // reset the running sum
            }
            
            @Override
            public void visit(final int row, final int column, final Complex value) {
                if (row == column) {
                    this.diagonalSum = this.diagonalSum.add(value);
                }
            }
            
            @Override
            public Complex end() {
                return this.diagonalSum;
            }
        };
    }
    
    /** Test {@link ComplexMatrix#walkInRowOrder(FieldMatrixPreservingVisitor)},
     *       {@link ComplexMatrix#walkInColumnOrder(FieldMatrixPreservingVisitor)}, and
     *       {@link ComplexMatrix#walkInOptimizedOrder(FieldMatrixPreservingVisitor)} */
    @Test
    void testFullPreservingWalks() {
        final ComplexMatrix M1 = FROM_DOUBLE_ARRS.copy();
        final ComplexMatrix M2 = M1.copy();
        final ComplexMatrix M3 = M1.copy();
        
        final FieldMatrixPreservingVisitor<Complex> visitor = getDiagonalSummingVisitor();
        
        final Complex s1 = M1.walkInRowOrder(visitor);
        final Complex s2 = M2.walkInColumnOrder(visitor);
        final Complex s3 = M3.walkInOptimizedOrder(visitor);
        
        Complex expectedDiagSum = Complex.ZERO;
        for (int i = 0; i < 5; i++) {
            expectedDiagSum = expectedDiagSum.add(FROM_DOUBLE_ARRS.getEntry(i, i));
        }
        
        assertEquals(expectedDiagSum, s1);
        assertEquals(expectedDiagSum, s2);
        assertEquals(expectedDiagSum, s3);
    }
    
    /** Test {@link ComplexMatrix#walkInRowOrder(FieldMatrixPreservingVisitor, int, int, int, int)},
     *       {@link ComplexMatrix#walkInColumnOrder(FieldMatrixPreservingVisitor, int, int, int, int)}, and
     *       {@link ComplexMatrix#walkInOptimizedOrder(FieldMatrixPreservingVisitor, int, int, int, int)} */
    @Test
    void testPartialPreservingWalks() {
        final ComplexMatrix M1 = FROM_DOUBLE_ARRS.copy();
        final ComplexMatrix M2 = M1.copy();
        final ComplexMatrix M3 = M1.copy();
        
        final FieldMatrixPreservingVisitor<Complex> visitor = getDiagonalSummingVisitor();
        
        final Complex s1 = M1.walkInRowOrder(visitor,       1, 3, 1, 3);
        final Complex s2 = M2.walkInColumnOrder(visitor,    1, 3, 1, 3);
        final Complex s3 = M3.walkInOptimizedOrder(visitor, 1, 3, 1, 3);
        
        Complex expectedPartialDiagSum = Complex.ZERO;
        for (int i = 1; i < 4; i++) {
            expectedPartialDiagSum = expectedPartialDiagSum.add(FROM_DOUBLE_ARRS.getEntry(i, i));
        }
        
        assertEquals(expectedPartialDiagSum, s1);
        assertEquals(expectedPartialDiagSum, s2);
        assertEquals(expectedPartialDiagSum, s3);
    }
    
    /** Test {@link ComplexMatrix#getTrace()} */
    @Test
    void testTrace() {
        final Complex trace = FROM_DOUBLE_ARRS.getTrace();
        assertEquals(new Complex(15.0, -15.0), trace); /* the diagonal contain the integers from [1, 5] (negative for
                                                        * the imaginary part) and sum(i, 1, 5) == 15 */
    }
    
    /** Test {@link ComplexMatrix#timesTransposeOf(FieldMatrix)} */
    @Test
    void testTimesTransposeOf() {
        final ComplexMatrix A = FROM_DOUBLE_ARRS;
        
        final ComplexMatrix AAT1 = A.timesTransposeOf(A);
        final ComplexMatrix AAT2 = A.multiplyTransposed(A);
        assertEquals(AAT1, AAT2);
        assertEquals(A.times(FROM_DOUBLE_ARRS.transpose()), AAT1);
    }
    
    /** Test {@link ComplexMatrix#transposeMultiply(FieldMatrix)} */
    @Test
    void testTransposeMultiply() {
        final ComplexMatrix A = FROM_DOUBLE_ARRS;
        assertEquals(FROM_DOUBLE_ARRS.transpose().times(A), A.transposeMultiply(A));
    }
    
    /** Test {@link ComplexMatrix#getDoubleSubColumn(int, int, int)} */
    @Test
    void testGetDoubleSubColumn() {
        final double[][] actualSubColumn   = FROM_COMPLEX_ARR.getDoubleSubColumn(1, 2, 3);
        final double[][] expectedSubColumn = new double[][] { {  2,  3,  3 },
                                                              { -2, -3, -3 }};
        
        assertArrayEquality(expectedSubColumn, actualSubColumn);
    }
    
    /** Test {@link ComplexMatrix#getColumnVector(int)} */
    @Test
    void testGetColumnVector() {
        final ComplexMatrix M = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 }, // upper Hessenberg
                                                                   { 1, 2, 2, 2, 2 },
                                                                   { 0, 2, 3, 3, 3 },
                                                                   { 0, 0, 3, 4, 4 },
                                                                   { 0, 0, 0, 4, 5 } });
        
        assertEquals(new ComplexVector(1, 1, 0, 0, 0), M.getColumnVector(0));
        assertEquals(new ComplexVector(1, 2, 3, 3, 0), M.getColumnVector(2));
        assertEquals(new ComplexVector(1, 2, 3, 4, 5), M.getColumnVector(4));
    }
    
    /** Test {@link ComplexMatrix#getRowVector(int)} */
    @Test
    void testGetRowVector() {
        final ComplexMatrix M = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 }, // upper Hessenberg
                                                                   { 1, 2, 2, 2, 2 },
                                                                   { 0, 2, 3, 3, 3 },
                                                                   { 0, 0, 3, 4, 4 },
                                                                   { 0, 0, 0, 4, 5 } });
        
        assertEquals(new ComplexVector(1, 1, 1, 1, 1), M.getRowVector(0));
        assertEquals(new ComplexVector(0, 2, 3, 3, 3), M.getRowVector(2));
        assertEquals(new ComplexVector(0, 0, 0, 4, 5), M.getRowVector(4));
    }
    
    /** Test {@link ComplexMatrix#of(AnyMatrix)} with a {@link RealMatrix} */
    @Test
    void testOfReal() {
        final double[][] realData = new double[][] { { 1, 2 }, { 3, 4 } };
        
        final ComplexMatrix m  = new ComplexMatrix(realData);
        final ComplexMatrix m2 = ComplexMatrix.of(new ptolemaeus.math.linear.real.Matrix(realData));
        
        assertEquals(m, m2);
    }
    
    /** Test {@link ComplexMatrix#of(AnyMatrix)} with an {@link Array2DRowFieldMatrix} */
    @SuppressFBWarnings("EC")
    @Test
    void testOfComplex() {
        final Array2DRowFieldMatrix<Complex> m =
                new Array2DRowFieldMatrix<>(randomComplexMatrix(10, 10, 1.0, 1L, false).getData());
        final ComplexMatrix m2 = ComplexMatrix.of(m);
        
        assertEquals(m, m2);
    }
    
    /** Test {@link ComplexMatrix#of(AnyMatrix)} with a {@link ComplexMatrix} */
    @Test
    void testOfComplexMatrix() {
        final ComplexMatrix m  = randomComplexMatrix(10, 10, 1.0, 1L, false);
        final ComplexMatrix m2 = ComplexMatrix.of(m);
        
        assertSame(m, m2);
    }
    
    /** Test {@link ComplexMatrix#of(AnyMatrix)} with a matrix of an unsupported type */
    @Test
    void testOfExceptional() {
        class FakeMatrix implements AnyMatrix {

            @Override
            public boolean isSquare() {
                return false;
            }

            @Override
            public int getRowDimension() {
                return 0;
            }

            @Override
            public int getColumnDimension() {
                return 0;
            }
        }
        
        final UnsupportedOperationException uoe =
                assertThrows(UnsupportedOperationException.class, () -> ComplexMatrix.of(new FakeMatrix()));
        assertEquals("Unable to create ComplexMatrix from type FakeMatrix", uoe.getMessage());
    }
    
    /** Test {@link ComplexMatrix#fromColumns(ComplexVector...)} */
    @Test
    void testFromColumns() {
        final ComplexVector v1 = new ComplexVector(new Complex(1, 12),
                                                   new Complex(2, 11),
                                                   new Complex(3, 10),
                                                   new Complex(4,  9),
                                                   new Complex(5,  8),
                                                   new Complex(6,  7));
        final ComplexVector v2 = new ComplexVector(new Complex(12, 1),
                                                   new Complex(11, 2),
                                                   new Complex(10, 3),
                                                   new Complex(9,  4),
                                                   new Complex(8,  5),
                                                   new Complex(7,  6));
        
        final ComplexMatrix fromC = ComplexMatrix.fromColumns(v1, v2);
        final ComplexMatrix expectedFromC = new ComplexMatrix(new Complex[][] {
            new Complex[] { new Complex(1, 12), new Complex(12, 1) },
            new Complex[] { new Complex(2, 11), new Complex(11, 2) },
            new Complex[] { new Complex(3, 10), new Complex(10, 3) },
            new Complex[] { new Complex(4,  9), new Complex(9,  4) },
            new Complex[] { new Complex(5,  8), new Complex(8,  5) },
            new Complex[] { new Complex(6,  7), new Complex(7,  6) }
        });
        
        assertEquals(expectedFromC, fromC);
    }
    
    /** Test {@link ComplexMatrix#fromRows(ComplexVector...)} */
    @Test
    void testFromRows() {
        final ComplexVector v1 = new ComplexVector(new Complex(1, 12),
                                                   new Complex(2, 11),
                                                   new Complex(3, 10),
                                                   new Complex(4,  9),
                                                   new Complex(5,  8),
                                                   new Complex(6,  7));
        final ComplexVector v2 = new ComplexVector(new Complex(12, 1),
                                                   new Complex(11, 2),
                                                   new Complex(10, 3),
                                                   new Complex(9,  4),
                                                   new Complex(8,  5),
                                                   new Complex(7,  6));
        
        final ComplexMatrix fromR = ComplexMatrix.fromRows(v1, v2);
        final ComplexMatrix expectedFromR = new ComplexMatrix(new Complex[][] {
                new Complex[] { new Complex(1, 12),
                                new Complex(2, 11),
                                new Complex(3, 10),
                                new Complex(4, 9),
                                new Complex(5, 8),
                                new Complex(6, 7) },
                new Complex[] { new Complex(12, 1),
                                new Complex(11, 2),
                                new Complex(10, 3),
                                new Complex(9, 4),
                                new Complex(8, 5),
                                new Complex(7, 6) } });
        
        assertEquals(expectedFromR, fromR);
    }
    
    /** Test
     * {@link ComplexMatrix#copySubMatrix(int, int, int, int, Complex[][])} and
     * {@link ComplexMatrix#copySubMatrix(int, int, int, int, double[][], double[][])} */
    @Test
    void testCopySubMatrixRange() {
        final ComplexMatrix A = SUB_MATRIX_TESTING.copy();
        
        final ComplexMatrix expectedSub = new ComplexMatrix(
                new double[][] {
                    { 12, 13, 14, 15, 16 },
                    { 22, 23, 24, 25, 26 },
                    { 32, 33, 34, 35, 36 },
                    { 42, 43, 44, 45, 46 },
                    { 52, 53, 54, 55, 56 },
                },
                new double[][] {
                    { -12, -13, -14, -15, -16 },
                    { -22, -23, -24, -25, -26 },
                    { -32, -33, -34, -35, -36 },
                    { -42, -43, -44, -45, -46 },
                    { -52, -53, -54, -55, -56 }
                });
        
        final Complex[][]  copyA1 = new Complex[5][5];
        A.copySubMatrix(1, 5, 2, 6, copyA1);
        final ComplexMatrix actualCopy1 = new ComplexMatrix(copyA1);
        assertEquals(expectedSub, actualCopy1);
        
        final double[][][] copyA2 = new double[2][5][5];
        A.copySubMatrix(1, 5, 2, 6, copyA2[0], copyA2[1]);
        final ComplexMatrix actualCopy2 = new ComplexMatrix(copyA2[0], copyA2[1]);
        assertEquals(expectedSub, actualCopy2);
    }
    
    /** Test
     * {@link ComplexMatrix#copySubMatrix(int[], int[], Complex[][])}
     * {@link ComplexMatrix#copySubMatrix(int[], int[], double[][], double[][])} */
    @Test
    void testCopySubMatrixSelection() {
        final ComplexMatrix A = SUB_MATRIX_TESTING.copy();
        
        final ComplexMatrix expectedSub = new ComplexMatrix(
            new double[][] {
                { 12, 13, 16, 17 },
                { 22, 23, 26, 27 },
                { 52, 53, 56, 57 },
                { 62, 63, 66, 67 }
            },
            new double[][] {
                { -12, -13, -16, -17 },
                { -22, -23, -26, -27 },
                { -52, -53, -56, -57 },
                { -62, -63, -66, -67 }
            });
        
        final int[] rowSelection = new int[] { 1, 2, 5, 6 };
        final int[] colSelection = new int[] { 2, 3, 6, 7 };
    
        final Complex[][]  copyA1 = new Complex[4][4];
        A.copySubMatrix(rowSelection, colSelection, copyA1);
        final ComplexMatrix actualCopy1 = new ComplexMatrix(copyA1);
        assertEquals(expectedSub, actualCopy1);
        
        final double[][][] copyA2 = new double[2][4][4];
        A.copySubMatrix(rowSelection, colSelection, copyA2[0], copyA2[1]);
        final ComplexMatrix actualCopy2 = new ComplexMatrix(copyA2[0], copyA2[1]);
        assertEquals(expectedSub, actualCopy2);
    }
    
    /** Test Exceptionals for:
     *  <ul>
     *  <li> {@link ComplexMatrix#copySubMatrix(int, int, int, int, Complex[][])} </li>
     *  <li> {@link ComplexMatrix#copySubMatrix(int[], int[], double[][], double[][])} </li>
     *  <li> {@link ComplexMatrix#copySubMatrix(int[], int[], Complex[][])} </li>
     *  </ul>
     */
    @Test
    void testCopySubMatrixExceptionals() {
        final ComplexMatrix A = DIAG_9X9.copy();
        
        final Complex[][]  dest     = new Complex[][] { { Complex.ZERO } };
        final double[][][] destReIm = new double[][][] { { { 0 }, { 0 } } }; // <- freaky eyes
        
        final Class<MathIllegalArgumentException> exClass = MathIllegalArgumentException.class;
        final Class<ArrayIndexOutOfBoundsException> exClass1 = ArrayIndexOutOfBoundsException.class;
        // expect not to be able to copy a (2x2) submatrix to a (1x1) destination
        assertThrows(exClass, () -> A.copySubMatrix(0, 1, 0, 1, dest));
        assertThrows(exClass, () -> A.copySubMatrix(new int[] { 0, 1 }, new int[] { 0, 1 }, dest));
        // expect not to be able to copy a (2x2) submatrix to a (1x1) array destination
        assertThrows(exClass1, () -> A.copySubMatrix(0, 1, 0, 1, destReIm[0], destReIm[1]));
        assertThrows(exClass1, () -> A.copySubMatrix(new int[] { 0, 1 }, new int[] { 0, 1 }, destReIm[0], destReIm[1]));
    }
    
    /** Test for:
     * <ul> 
     * <li> {@link ComplexMatrix#getSubMatrixData(int, int, int, int)} </li>
     * <li> {@link ComplexMatrix#getSubMatrixData(int[], int[])} </li>
     * </ul>
     * */
    @Test
    void testGetSubMatrixData() {
        final ComplexMatrix A = DIAG_9X9.copy();
        assertArrayEquality(A.getSubMatrix(2, 5, 3, 6).getDataRef(), A.getSubMatrixData(2, 5, 3, 6));
        assertArrayEquality(A.getSubMatrix(new int[] { 1, 2 },
                                           new int[] { 1, 2 }).getDataRef(),
                            A.getSubMatrixData(new int[] { 1, 2 },
                                               new int[] { 1, 2 }));
    }
    
    /** Test for:
     * <ul>
     * <li> {@link ComplexMatrix#getSubMatrix(int, int, int, int)} </li>
     * <li> {@link ComplexMatrix#getSubMatrix(int[], int[])} </li>
     * </ul>
     * */
    @Test
    void testGetSetSubMatrix() {
        final ComplexMatrix A = SUB_MATRIX_TESTING.copy();
        
        final ComplexMatrix expectedSub1 = new ComplexMatrix(
            new double[][] {
                { 12, 13, 14, 15, 16 },
                { 22, 23, 24, 25, 26 },
                { 32, 33, 34, 35, 36 },
                { 42, 43, 44, 45, 46 },
                { 52, 53, 54, 55, 56 },
            },
            new double[][] {
                { -12, -13, -14, -15, -16 },
                { -22, -23, -24, -25, -26 },
                { -32, -33, -34, -35, -36 },
                { -42, -43, -44, -45, -46 },
                { -52, -53, -54, -55, -56 }
            });
        
        final ComplexMatrix sub1 = A.getSubMatrix(1, 5, 2, 6);
        assertEquals(expectedSub1, sub1);
        
        final ComplexMatrix expectedSub2 = new ComplexMatrix(
            new double[][] {
                { 12, 13, 16, 17 },
                { 22, 23, 26, 27 },
                { 52, 53, 56, 57 },
                { 62, 63, 66, 67 }
            },
            new double[][] {
                { -12, -13, -16, -17 },
                { -22, -23, -26, -27 },
                { -52, -53, -56, -57 },
                { -62, -63, -66, -67 }
            });
        
        final int[] rowSelection = new int[] { 1, 2, 5, 6 };
        final int[] colSelection = new int[] { 2, 3, 6, 7 };
        
        final ComplexMatrix sub2 = A.getSubMatrix(rowSelection, colSelection);
        assertEquals(expectedSub2, sub2);
    }
    
    /** Test Exceptionals for:
     * <ul>
     * <li> {@link ComplexMatrix#getSubMatrix(int, int, int, int)} </li>
     * <li> {@link ComplexMatrix#getSubMatrix(int[], int[])} </li>
     * </ul>
     */
    @Test
    void testGetSubMatrixExceptionals() {
        final ComplexMatrix A = DIAG_9X9.copy();
        
        final Class<MathIllegalArgumentException> exClass = MathIllegalArgumentException.class;

        // shouldn't be able get a sub-matrix outside of the bounds of the matrix size
        assertThrows(exClass, () -> A.getSubMatrix(123, 124, 123, 124));
        assertThrows(exClass, () -> A.getSubMatrix(new int[] { 123, 124 }, new int[] { 123, 124 }));
    }
    
    /** Test for:
     * <ul> 
     * <li> {@link ComplexMatrix#setSubMatrix(Complex[][], int, int)} </li>
     * <li> {@link ComplexMatrix#setSubMatrix(double[][], double[][], int, int)} </li>
     * </ul>
     * */
    @Test
    void testSetSubMatrix() {
        final ComplexMatrix A1 = SUB_MATRIX_TESTING.copy(); // copy is important!!
        final Complex[][] zerosComplexArray = ComplexMatrix.zero(5, 5).getData();
        A1.setSubMatrix(zerosComplexArray, 2, 3);
        
        final ComplexMatrix A2 = SUB_MATRIX_TESTING.copy(); // copy is important!!
        final double[][][] zerosDoubleArray = ComplexMatrix.zero(5, 5).getDataRef();
        A2.setSubMatrix(zerosDoubleArray[0], zerosDoubleArray[1], 2, 3);
        
        assertEquals(A1, A2);
        
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                final Complex entry = A1.getEntry(i, j);
                if ((i == 0 && j == 0) // the top-left entry is zero by design
                        || (i >= 2 && i <= 6 && j >= 3 && j <= 7)) {
                    assertTrue(entry.isZero());
                }
                else {
                    assertFalse(entry.isZero());
                }
            }
        }
    }

    /** Test for:
     * <ul>
     * <li> {@link ComplexMatrix#setRowMatrix(int, double[][], double[][])} </li>
     * <li> {@link ComplexMatrix#setRowMatrix(int, FieldMatrix)} </li>
     * </ul> */
    @Test 
    void testGetSetRowMatrix() {
        // getRowMatrix()
        final ComplexMatrix A = DIAG_9X9.copy();

        assertEquals(A.getSubMatrix(0, 8, 0, 0).transpose(), A.getRowMatrix(0));

        // setRowMatrix()
        final double[][][] AData = A.getDataRef();
        final ComplexMatrix ARowMatrixDouble = new ComplexMatrix(AData[0], AData[1]);
        final ComplexMatrix ARowMatrixComplex = new ComplexMatrix(A.getData());
        final ComplexMatrix ARowMatrixComplex2 = new ComplexMatrix(A.getData());
        
        final double[][] ADataRe = new double[][]  { { 9, 9, 9, 9, 9, 9, 9, 9, 9 } };
        final double[][] ADataIm = new double[][]  { { 0, 0, 0, 0, 0, 0, 0, 0, 0 } };
        ARowMatrixDouble.setRowMatrix(8, ADataRe, ADataIm);
        ARowMatrixComplex.setRowMatrix(8, new ComplexMatrix(ADataRe, ADataIm));
        ARowMatrixComplex2.setRowMatrix(8, new Array2DRowFieldMatrix<>(ARowMatrixComplex.getRowMatrix(8).getData()));
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                          { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                                                                          { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                                                                          { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                                                                          { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                                                                          { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                                                                          { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                                                                          { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                                                                          { 9, 9, 9, 9, 9, 9, 9, 9, 9 } });

        assertArrayEquality(expected.getDataRef(), ARowMatrixDouble.getDataRef());
        assertArrayEquality(expected.getDataRef(), ARowMatrixComplex.getDataRef());
        assertArrayEquality(expected.getDataRef(), ARowMatrixComplex2.getDataRef());
    }
    
    /** Test for:
     * <ul> 
     * <li> {@link ComplexMatrix#setColumnMatrix(int, double[][], double[][])} <\li>
     * <li> {@link ComplexMatrix#setColumnMatrix(int, FieldMatrix)} <\li>
     * </ul>
     */
    @Test
    void testGetSetColumnMatrix() {
        final ComplexMatrix A = DIAG_9X9.copy();

        // getColumnMatrix()
        final ComplexMatrix AColumn = A.getColumnMatrix(0); 

        assertArrayEquality(A.getSubMatrix(0, 8, 0, 0).getDataRef(), AColumn.getDataRef());
        
        // setColumnMatrix()
        final double[][][] AData = A.getDataRef();
        final ComplexMatrix AColumnMatrixDouble = new ComplexMatrix(AData[0], AData[1]);
        final ComplexMatrix AColumnMatrixComplex = new ComplexMatrix(A.getData());
        final ComplexMatrix AColumnMatrixComplex2 = new ComplexMatrix(A.getData());
        final double[][] ADataRe = new double[][]  { {9}, {9}, {9}, {9}, {9}, {9}, {9}, {9}, {9} };
        final double[][] ADataIm = new double[][]  { {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0} };
        
        AColumnMatrixDouble.setColumnMatrix(8, ADataRe, ADataIm);
        AColumnMatrixComplex.setColumnMatrix(8, new ComplexMatrix(ADataRe, ADataIm));
        AColumnMatrixComplex2.setColumnMatrix(8, new Array2DRowFieldMatrix<>(AColumnMatrixComplex.getColumnMatrix(8)
                                                                                                 .getData()));
        
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 9 },
                                                                          { 0, 2, 0, 0, 0, 0, 0, 0, 9 },
                                                                          { 0, 0, 3, 0, 0, 0, 0, 0, 9 },
                                                                          { 0, 0, 0, 4, 0, 0, 0, 0, 9 },
                                                                          { 0, 0, 0, 0, 5, 0, 0, 0, 9 },
                                                                          { 0, 0, 0, 0, 0, 6, 0, 0, 9 },
                                                                          { 0, 0, 0, 0, 0, 0, 7, 0, 9 },
                                                                          { 0, 0, 0, 0, 0, 0, 0, 8, 9 },
                                                                          { 0, 0, 0, 0, 0, 0, 0, 0, 9 } });

        assertArrayEquality(expected.getDataRef(), AColumnMatrixDouble.getDataRef());
        assertArrayEquality(expected.getDataRef(), AColumnMatrixComplex.getDataRef());
        assertArrayEquality(expected.getDataRef(), AColumnMatrixComplex2.getDataRef());
    }
    
    /**
     * Test {@link ComplexMatrix#slice(int, int, int, int)} and {@link ComplexMatrix#slice(int[], int[])}
     */
    @Test
    void testSlicing() {
        AbstractComplexMatrixSlice slice = SUB_MATRIX_TESTING.slice(0, 1, 0, 1);
        assertEquals(new ComplexMatrix(new double[][]{{0, 1}, {10, 11}}, new double[][]{{0, -1}, {-10, -11}}), 
                     slice.copy());

        slice = SUB_MATRIX_TESTING.slice(new int[]{4, 3}, new int[]{4, 3});
        assertEquals(new ComplexMatrix(new double[][]{{44, 43}, {34, 33}}, new double[][]{{-44, -43}, {-34, -33}}), 
                     slice.copy());
    }

    /** Test for: {@link ComplexMatrix#toReImParts()} */
    @Test
    void testToReImParts() {
        final Complex[][]   AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4) },
                                                      { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final ComplexMatrix A     = new ComplexMatrix(AData);
        
        final double[][] expectedRe = { { 1, 3 }, { 5, 7 }};
        final double[][] expectedIm = { { 2, 4 }, { 6, 8 }};
        
        final double[][][] reIm = A.toReImParts();
        
        assertArrayEquality(expectedRe, reIm[0]);
        assertArrayEquality(expectedIm, reIm[1]);
    }
    
    /** Test the exceptional cases of
     * {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} */
    @Test
    void testPostMultiplyInPlaceExceptional() {
        final ComplexMatrix A = ALL_ONES_12X9.copy();
        final ComplexMatrix B = ALL_TWOS_5X2.copy();
        
        final NumericalMethodsException nme1 = assertThrows(NumericalMethodsException.class,
                                                            () -> ComplexMatrix.postMultiplyInPlace(A, B, 0, 100));
        final NumericalMethodsException nme2 = assertThrows(NumericalMethodsException.class,
                                                            () -> ComplexMatrix.postMultiplyInPlace(A, B, 100, 0));
        
        assertEquals("Matrix A (12 x 9) cannot be post-multiplied in-place by "
                     + "smallB (5 x 2) when starting at indices (0, 100)",
                     nme1.getMessage());
        assertEquals("Matrix A (12 x 9) cannot be post-multiplied in-place by"
                     + " smallB (5 x 2) when starting at indices (100, 0)",
                     nme2.getMessage());
    }
    
    /** Test the exceptional cases of
     * {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} */
    @Test
    void testPreMultiplyInPlaceExceptional() {
        final ComplexMatrix A = ALL_TWOS_5X2.copy();
        final ComplexMatrix B = ALL_ONES_12X9.copy();
        
        final NumericalMethodsException nme1 = assertThrows(NumericalMethodsException.class,
                                                            () -> ComplexMatrix.preMultiplyInPlace(A, B, 0, 100));
        final NumericalMethodsException nme2 = assertThrows(NumericalMethodsException.class,
                                                            () -> ComplexMatrix.preMultiplyInPlace(A, B, 100, 0));
        
        assertEquals("Matrix B (12 x 9) cannot be pre-multiplied in-place by "
                     + "smallA (5 x 2) when starting at indices (0, 100)",
                     nme1.getMessage());
        assertEquals("Matrix B (12 x 9) cannot be pre-multiplied in-place by "
                     + "smallA (5 x 2) when starting at indices (100, 0)",
                     nme2.getMessage());
    }
    
    // **************** post-multiply in-place square tests ****************
    
    /** Test a simple square case of {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}.
     * This one sets the start indices to {@code (1, 5)}, which is above the diagonal. */
    @Test
    void testPostMultiplyInPlaceSquare1() {
        final ComplexMatrix B1 = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 2, 2, 2, 0 },
                                                                    { 0, 0, 1, 0, 0, 2, 2, 2, 0 },
                                                                    { 0, 0, 0, 1, 0, 2, 2, 2, 0 },
                                                                    { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected1 = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 },
                                                                            { 1, 1, 1, 1, 1, 7, 7, 7, 1 } });
        
        final ComplexMatrix fullMultiply = ALL_ONES_9X9.times(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix ABSmall1 = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_3X3, 1, 5);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple square case of {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}.
     * This one sets the start indices to {@code (5, 1)}, which is below the diagonal. */
    @Test
    void testPostMultiplyInPlaceSquare2() {
        final ComplexMatrix B2 = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 2, 0, 1, 0, 0, 0 },
                                                                    { 0, 2, 2, 2, 0, 0, 1, 0, 0 },
                                                                    { 0, 2, 2, 2, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected2 = new ComplexMatrix(new double[][]  { { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 },
                                                                            { 1, 7, 7, 7, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = ALL_ONES_9X9.times(B2);
        assertEquals(expected2, fullMultiply); // sanity check
        
        final ComplexMatrix ABSmall1 = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_3X3, 5, 1);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple square case of {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}.
     * This one sets the start indices to {@code (2, 4)}, which covers one diagonal entry. */
    @Test
    void testPostMultiplyInPlaceSquare3() {
        final ComplexMatrix B3 = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 1, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 0, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected3 = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 },
                                                                            { 1, 1, 1, 1, 6, 7, 7, 1, 1 } });
        
        final ComplexMatrix fullMultiply = ALL_ONES_9X9.times(B3);
        assertEquals(expected3, fullMultiply); // sanity check
        
        final ComplexMatrix ABSmall1 = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_3X3, 2, 4); // modifies the copy
        
        assertEquals(fullMultiply, ABSmall1);
    }
    
    // **************** post-multiply in-place rectangular tests ****************
    
    /** Test a simple rectangular case of
     * {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}. This one sets the start
     * indices to {@code (4, 1)}, which is below the diagonal. {@code A} (12x9) and {@code smallB} (5x2) are both
     * "tall". */
    @Test
    void testPostMultiplyInPlaceRectangular1() {
        final ComplexMatrix B1 = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 2, 2, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected1 = new ComplexMatrix(new double[][]  { { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 11, 11, 1, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = ALL_ONES_12X9.times(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix ABSmall1 = ALL_ONES_12X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.postMultiplyInPlace(ABSmall1, ALL_TWOS_5X2, 4, 1);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple rectangular case of
     * {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}. This one sets the start
     * indices to {@code (4, 5)}, which causes {@code smallB} to cover one diagonal value. {@code A} (9x12) and
     * {@code smallB} (2x5) are both "wide". */
    @Test
    void testPostMultiplyInPlaceRectangular2() {
        final ComplexMatrix A = ALL_ONES_12X9.transpose();
        final ComplexMatrix smallB = ALL_TWOS_5X2.transpose();
        
        final ComplexMatrix B1 = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } });

        final ComplexMatrix expected1 =
                new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 4, 5, 5, 5, 5, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix ABSmall1 = A.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.postMultiplyInPlace(ABSmall1, smallB, 4, 5);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    /** Test a simple rectangular case of
     * {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}. This one sets the start
     * indices to {@code (2, 1)}, which causes {@code smallB} to cover two diagonal values. {@code A} (12x9) is "tall"
     * and {@code smallB} (2x5) is "wide". */
    @Test
    void testPostMultiplyInPlaceRectangular3() {
        final ComplexMatrix A = ALL_ONES_12X9;
        final ComplexMatrix smallB = ALL_TWOS_5X2.transpose();
        
        final ComplexMatrix B1 = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                                    { 0, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected1 =
                                new ComplexMatrix(new double[][]  { { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 },
                                                                    { 1, 5, 4, 4, 5, 5, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(B1);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix ABSmall1 = A.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.postMultiplyInPlace(ABSmall1, smallB, 2, 1);
        assertEquals(fullMultiply, ABSmall1);
    }
    
    // **************** post-multiply in-place random tests ****************
    
    /** Test {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} using
     * {@link #testPostMultiplyInPlace(ComplexMatrix, ComplexMatrix)}
     * 
     * We use a random 10x10 (square) {@link ComplexMatrix} as {@code A}, and a random 5x5 (square)
     * {@link ComplexMatrix} as {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPostMultiplyInPlaceRandomSquares() {
        final ComplexMatrix A      = randomComplexMatrix(10, 10, 100.0, 8675309L, false);
        final ComplexMatrix smallB = randomComplexMatrix(5, 5, 100.0, 7777777L, false);
        
        // set an arbitrary entry in both to zero to make sure we hit the short-circuit branches
        A.setEntry(2, 1, Complex.ZERO);
        smallB.setEntry(1, 2, Complex.ZERO);
        
        testPostMultiplyInPlace(A, smallB);
    }
    
    /** Test {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} using
     * {@link #testPostMultiplyInPlace(ComplexMatrix, ComplexMatrix)}
     * 
     * We use a random 15x10 (tall) {@link ComplexMatrix} as {@code A}, and a random 5x7 (wide) {@link ComplexMatrix} as
     * {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPostMultiplyInPlaceRandomTall() {
        final ComplexMatrix A      = randomComplexMatrix(15, 10, 100.0, 8675309L, false);
        final ComplexMatrix smallB = randomComplexMatrix(5, 7, 100.0, 7777777L, false);
        
        testPostMultiplyInPlace(A, smallB);
    }
    
    /** Test {@link ComplexMatrix#postMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} by comparing results to
     * when performing the full multiplication.
     * 
     * @param A the matrix to be post-multiplied
     * @param smallB the smaller matrix which we treat as padded */
    private void testPostMultiplyInPlace(final ComplexMatrix A, final ComplexMatrix smallB) {
        final int maxRowStartIndex = A.getColumnDimension() - smallB.getRowDimension();
        final int maxColStartIndex = A.getColumnDimension() - smallB.getColumnDimension();
        
        for (int rowStart = 0; rowStart < maxRowStartIndex; rowStart++) { // test all allowable start indices
            for (int colStart = 0; colStart < maxColStartIndex; colStart++) {
                final ComplexMatrix B = ComplexMatrix.identity(A.getColumnDimension());
                B.setSubMatrix(smallB.getData(), rowStart, colStart);
                
                final ComplexMatrix expected =
                        new ComplexMatrix(new Array2DRowFieldMatrix<>(A.getData())
                                          .multiply(new Array2DRowFieldMatrix<>(B.getData())).getData());
                
                final ComplexMatrix ABSmall = A.copy(); // Important! If not copied, it's modified on the next line
                ComplexMatrix.postMultiplyInPlace(ABSmall, smallB, rowStart, colStart);
                
                final int fRowStart = rowStart; // finals for lambda
                final int fColStart = colStart;
                assertAbsEquals(expected, ABSmall, 2e-12, 2e-12, () -> "(%d, %d)".formatted(fRowStart, fColStart));
            }
        }
    }
    
    // **************** pre-multiply in-place square tests ****************
    
    /** Test a simple square case of {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}.
     * This one sets the start indices to {@code (1, 5)}, which is above the diagonal. */
    @Test
    void testPreMultiplyInPlaceSquare1() {
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 2, 2, 2, 0 },
                                                                    { 0, 0, 1, 0, 0, 2, 2, 2, 0 },
                                                                    { 0, 0, 0, 1, 0, 2, 2, 2, 0 },
                                                                    { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });

        final ComplexMatrix expected1 = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(ALL_ONES_9X9);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix smallAB = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.preMultiplyInPlace(ALL_TWOS_3X3, smallAB, 1, 5);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple square case of {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}.
     * This one sets the start indices to {@code (5, 1)}, which is below the diagonal. */
    @Test
    void testPreMultiplyInPlaceSquare2() {
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 2, 0, 1, 0, 0, 0 },
                                                                    { 0, 2, 2, 2, 0, 0, 1, 0, 0 },
                                                                    { 0, 2, 2, 2, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected2 = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(ALL_ONES_9X9);
        assertEquals(expected2, fullMultiply); // sanity check
        
        final ComplexMatrix smallAB = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.preMultiplyInPlace(ALL_TWOS_3X3, smallAB, 5, 1);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple square case of {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}.
     * This one sets the start indices to {@code (2, 4)}, which covers one diagonal entry. */
    @Test
    void testPreMultiplyInPlaceSquare3() {
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 1, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 0, 2, 2, 2, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected3 = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 7, 7, 7, 7, 7, 7, 7, 7, 7 },
                                                                            { 6, 6, 6, 6, 6, 6, 6, 6, 6 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(ALL_ONES_9X9);
        assertEquals(expected3, fullMultiply); // sanity check
        
        final ComplexMatrix smallAB = ALL_ONES_9X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.preMultiplyInPlace(ALL_TWOS_3X3, smallAB, 2, 4);
        assertEquals(fullMultiply, smallAB);
    }
    
    // **************** pre-multiply in-place rectangular tests ****************
    
    /** Test a simple rectangular case of
     * {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}. This one sets the start
     * indices to {@code (4, 1)}, which is below the diagonal. {@code smallA} (5x2) and {@code B} (12x9) are both
     * "tall". */
    @Test
    void testPreMultiplyInPlaceRectangular1() {
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 2, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
        
        final ComplexMatrix expected1 = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                                            { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                                            { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                                            { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                                            { 5, 5, 5, 5, 5, 5, 5, 5, 5 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                            { 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(ALL_ONES_12X9);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix smallAB = ALL_ONES_12X9.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.preMultiplyInPlace(ALL_TWOS_5X2, smallAB, 4, 1);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple rectangular case of
     * {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}. This one sets the start
     * indices to {@code (2, 3)}, which causes {@code smallA} to cover one diagonal value. {@code smallA} (2x5) and
     * {@code B} (9x12) are both "wide". */
    @Test
    void testPreMultiplyInPlaceRectangular2() {
        final ComplexMatrix smallA = ALL_TWOS_5X2.transpose();
        final ComplexMatrix B = ALL_ONES_12X9.transpose();
        
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 2, 2, 2, 2, 2, 0 },
                                                                    { 0, 0, 0, 2, 2, 2, 2, 2, 0 },
                                                                    { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1 } });
    
        final ComplexMatrix expected1 =
                new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                    { 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11 },
                                                    { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
                                                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix fullMultiply = A.times(B);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix smallAB = B.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.preMultiplyInPlace(smallA, smallAB, 2, 3);
        assertEquals(fullMultiply, smallAB);
    }
    
    /** Test a simple rectangular case of
     * {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)}. This one sets the start
     * indices to {@code (3, 4)}, which causes {@code smallA} to cover two diagonal values. {@code B} (12x9) is "tall"
     * and {@code smallA} (2x5) is "wide". */
    @Test
    void testPreMultiplyInPlaceRectangular3() {
        final ComplexMatrix smallA = ALL_TWOS_5X2.transpose();
        final ComplexMatrix B      = new ComplexMatrix(new double[][] { { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                                        { 1, 2, 3, 4, 5, 6, 7, 8, 9 } });
        
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 1, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                                                                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } });

        final ComplexMatrix expected1 =
                new ComplexMatrix(new double[][]  { { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 11, 22, 33, 44, 55, 66, 77, 88, 99 },
                                                    { 10, 20, 30, 40, 50, 60, 70, 80, 90 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                                                    { 1, 2, 3, 4, 5, 6, 7, 8, 9 } });
        
        final ComplexMatrix fullMultiply = A.times(B);
        assertEquals(expected1, fullMultiply); // sanity check
        
        final ComplexMatrix smallAB = B.copy(); // Important! If not copied, it's modified on the next line
        ComplexMatrix.preMultiplyInPlace(smallA, smallAB, 3, 4);
        assertEquals(fullMultiply, smallAB);
    }
    
    // **************** pre-multiply in-place random tests ****************
    
    /** Test {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} using
     * {@link #testPreMultiplyInPlace(ComplexMatrix, ComplexMatrix)}
     * 
     * We use a random 10x10 (square) {@link ComplexMatrix} as {@code A}, and a random 5x5 (square)
     * {@link ComplexMatrix} as {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPreMultiplyInPlaceRandomSquares() {
        final ComplexMatrix smallA = randomComplexMatrix(5,   5, 100.0, 7777777L, false);
        final ComplexMatrix B      = randomComplexMatrix(10, 10, 100.0, 8675309L, false);
        
        // set an arbitrary entry in both to zero to make sure we hit the short-circuit branches
        smallA.setEntry(1, 2, Complex.ZERO);
        B.setEntry(2, 1, Complex.ZERO);
        
        testPreMultiplyInPlace(smallA, B);
    }
    
    /** Test {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} using
     * {@link #testPreMultiplyInPlace(ComplexMatrix, ComplexMatrix)}
     * 
     * We use a random 15x10 (tall) {@link ComplexMatrix} as {@code A}, and a random 5x7 (wide)
     * {@link ComplexMatrix} as {@code smallB} and iterate over all of the allowable indices. */
    @Test
    void testPreMultiplyInPlaceRandomTall() {
        final ComplexMatrix smallA = randomComplexMatrix(5,   7, 100.0, 7777777L, false);
        final ComplexMatrix B      = randomComplexMatrix(15, 10, 100.0, 8675309L, false);
        
        testPreMultiplyInPlace(smallA, B);
    }
    
    /** Test {@link ComplexMatrix#preMultiplyInPlace(ComplexMatrix, ComplexMatrix, int, int)} by comparing
     * results to when performing the full multiplication.
     * 
     * @param smallA the smaller matrix which we treat as padded
     * @param B the matrix to be pre-multiplied */
    private void testPreMultiplyInPlace(final ComplexMatrix smallA, final ComplexMatrix B) {
        final int maxRowStartIndex = B.getRowDimension() - smallA.getRowDimension();
        final int maxColStartIndex = B.getRowDimension() - smallA.getColumnDimension();
        
        for (int rowStart = 0; rowStart < maxRowStartIndex; rowStart++) { // test all allowable start indices
            for (int colStart = 0; colStart < maxColStartIndex; colStart++) {
                final ComplexMatrix A = ComplexMatrix.identity(B.getRowDimension());
                A.setSubMatrix(smallA.getData(), rowStart, colStart);
                
                final ComplexMatrix expected = A.times(B);
                
                final ComplexMatrix smallAB = B.copy(); // Important! If not copied, it's modified on the next line
                ComplexMatrix.preMultiplyInPlace(smallA, smallAB, rowStart, colStart);
                
                final int fRowStart = rowStart; // finals for lambda
                final int fColStart = colStart;
                assertAbsEquals(expected, smallAB, 2e-12, 2e-12, () -> "(%d, %d)".formatted(fRowStart, fColStart));
            }
        }
    }

    /** Test {@link ComplexMatrix#blockDiagonalConcatenate(ComplexMatrix...)} */
    @Test
    void testBlockDiagonalConcatenate() {
        final ComplexMatrix A = new ComplexMatrix(new double[][]  { { 1, 1, 1, 1, 1 },
                                                                    { 2, 2, 2, 2, 2 },
                                                                    { 0, 3, 3, 3, 3 },
                                                                    { 0, 0, 4, 4, 4 },
                                                                    { 0, 0, 0, 5, 5 } });
        
        final ComplexMatrix B = new ComplexMatrix(new double[][]  { { 6, 6, 6, 6, 6 },
                                                                    { 7, 7, 7, 7, 7 } });
        
        final ComplexMatrix C = new ComplexMatrix(new double[][]  { {  8 },
                                                                    {  9 },
                                                                    { 10 },
                                                                    { 11 },
                                                                    { 12 } });
        
        final ComplexMatrix ABC = ComplexMatrix.blockDiagonalConcatenate(A, B, C);
        
        final String expected = String.join(System.lineSeparator(),
                                            " 1 1 1 1 1 0 0 0 0 0  0", // A, on the left
                                            " 2 2 2 2 2 0 0 0 0 0  0",
                                            " 0 3 3 3 3 0 0 0 0 0  0",
                                            " 0 0 4 4 4 0 0 0 0 0  0",
                                            " 0 0 0 5 5 0 0 0 0 0  0",
                                            
                                            " 0 0 0 0 0 6 6 6 6 6  0", // B, in the middle
                                            " 0 0 0 0 0 7 7 7 7 7  0",
                                            
                                            " 0 0 0 0 0 0 0 0 0 0  8", // C, on the right
                                            " 0 0 0 0 0 0 0 0 0 0  9",
                                            " 0 0 0 0 0 0 0 0 0 0 10",
                                            " 0 0 0 0 0 0 0 0 0 0 11",
                                            " 0 0 0 0 0 0 0 0 0 0 12");
        TestUtils.testMultiLineToString(expected, ABC);
    }
    
    /** Test {@link ComplexMatrix#extractDiagonalVector(ComplexMatrix)} and
     *       {@link ComplexMatrix#extractDiagonal(ComplexMatrix)} */
    @Test
    void testExtractDiagonal() {
        final Complex[][] AData = new Complex[][] {
            { new Complex(1, 2), new Complex(3, 4), new Complex(5, 6), new Complex(7, 8), new Complex(9, 10) },
            { new Complex(1, 2), new Complex(3, 4), new Complex(5, 6), new Complex(7, 8), new Complex(9, 10) },
            { new Complex(1, 2), new Complex(3, 4), new Complex(5, 6), new Complex(7, 8), new Complex(9, 10) }
        };
        final ComplexMatrix A = new ComplexMatrix(AData);
        final ComplexMatrix AT = A.transpose();
        
        final Complex[] expectedDiagElts = new Complex[] { AData[0][0], AData[1][1], AData[2][2] };
        assertArrayEquality(expectedDiagElts, ComplexMatrix.extractDiagonal(A));
        assertArrayEquality(expectedDiagElts, ComplexMatrix.extractDiagonal(AT));
        
        final ComplexVector expectedDiagVec = new ComplexVector(expectedDiagElts);
        assertEquals(expectedDiagVec, ComplexMatrix.extractDiagonalVector(A));
        assertEquals(expectedDiagVec, ComplexMatrix.extractDiagonalVector(AT));
    }
    
    /** Test {@link ComplexMatrix#diagonal(Complex[])} and {@link ComplexMatrix#diagonal(ComplexVector)} */
    @Test
    void testDiagonal() {
        final Complex[] diag = new Complex[] { new Complex(1, 2), new Complex(3, 4), new Complex(5, 6) };
        
        final ComplexMatrix A1 = ComplexMatrix.diagonal(diag);
        final ComplexMatrix A2 = ComplexMatrix.diagonal(new ComplexVector(diag));
        
        assertDiagonalEq(diag, A1);
        assertDiagonalEq(diag, A2);
    }
    
    /** Test {@link ComplexMatrix#getFrobeniusNorm()} and {@link ComplexMatrix#getNorm2()} */
    @Test
    void testGetFrobeniusNorm() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 0), Complex.valueOf(0, 2) },
                                                    { Complex.valueOf(0, 3), Complex.valueOf(4, 0) } };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        final double frobNorm = FastMath.sqrt(1 + 4 + 9 + 16); // sqrt(30)
        assertEquals(frobNorm, A.getFrobeniusNorm());
        assertEquals(frobNorm, A.getNorm2());
    }
    
    /** Test {@link ComplexMatrix#getNorm1()} */
    @Test
    void testGetNorm1() {
        final ComplexMatrix A =
                new ComplexMatrix(new Complex[][] { { Complex.valueOf(1, 0), Complex.valueOf(0,  2) },
                                                    { Complex.valueOf(0, 3), Complex.valueOf(4, -3) } });
        
        final double norm1 = 7; // = 2 + sqrt(4 * 4 + 3 * 3) = 2 + 5
        assertEquals(norm1, A.getNorm1());
    }
    
    /** Test {@link ComplexMatrix#getNormInfty()} */
    @Test
    void testGetNormInfty() {
        final ComplexMatrix A =
                new ComplexMatrix(new Complex[][] { { Complex.valueOf(1, 0), Complex.valueOf(0,  2) },
                                                    { Complex.valueOf(0, 3), Complex.valueOf(4, -3) } });
        
        final double normInfty = 8; // = 3 + sqrt(4 * 4 + 3 * 3) = 3 + 5
        assertEquals(normInfty, A.getNormInfty());
    }
    
    /** Test {@link ComplexMatrix#extractData(FieldMatrix)} */
    @Test
    void testExtractData() {
        final ComplexMatrix M = new ComplexMatrix(new double[][]  { { 1, 0, 0, 0, 0 },
                                                                    { 1, 2, 0, 0, 0 },
                                                                    { 0, 2, 3, 0, 0 },
                                                                    { 0, 0, 3, 4, 0 },
                                                                    { 0, 0, 0, 4, 5 } });
        
        final FieldMatrix<Complex> dense  = new ComplexMatrix(M.getData());
        final SparseFieldMatrix<Complex>     sparse = new SparseFieldMatrix<>(M);
        
        final Complex[][] denseData  = ComplexMatrix.extractData(dense);
        final Complex[][] sparseData = ComplexMatrix.extractData(sparse);
        
        assertArrayEquality(M.getData(), denseData);
        assertArrayEquality(M.getData(), sparseData);
    }
    
    /** Test {@link ComplexMatrix#checkMultiplyDimensions(AnyMatrix, boolean, AnyMatrix, boolean)} */
    @Test
    void testCheckMultiplyDimensions() {
        final ComplexMatrix rect10x20   = randomComplexMatrix(10, 20, 1.0, 1L, false);
        final ComplexMatrix rect20x10   = randomComplexMatrix(20, 10, 1.0, 1L, false);
        
        // neither transposed
        assertDoesNotThrow(() -> ComplexMatrix.checkMultiplyDimensions(rect10x20, false, rect20x10, false));
        
        // both transposed
        assertDoesNotThrow(() -> ComplexMatrix.checkMultiplyDimensions(rect10x20, true,  rect20x10, true));
        
        // in both cases, one or the other is transposed and the other is not, so the they cannot be multiplied
        assertThrows(MathIllegalArgumentException.class,
                     () -> ComplexMatrix.checkMultiplyDimensions(rect10x20, true, rect20x10, false));
        assertThrows(MathIllegalArgumentException.class,
                     () -> ComplexMatrix.checkMultiplyDimensions(rect10x20, false, rect20x10, true));
    }
    
    /** Stress test
     * {@link ComplexMatrix#multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)} with
     * square matrices */
    @Test
    void testParallelMultiplySquares() {
        final ComplexMatrix M1 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 }, // upper Hessenberg
                                                                    { 1, 2, 2, 2, 2 },
                                                                    { 0, 2, 3, 3, 3 },
                                                                    { 0, 0, 3, 4, 4 },
                                                                    { 0, 0, 0, 4, 5 } });
        
        final ComplexMatrix M2 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 }, // symmetric
                                                                    { 1, 2, 2, 2, 2 },
                                                                    { 1, 2, 3, 3, 3 },
                                                                    { 1, 2, 3, 4, 4 },
                                                                    { 1, 2, 3, 4, 5 } });
        
        final boolean[] tf = new boolean[] { true, false };
        final ComplexMatrix[] square5x5s = new ComplexMatrix[] { M1, M2,
                                                                 randomComplexMatrix(5, 5, 10, 1L, true),
                                                                 randomComplexMatrix(5, 5, 10, 2L, false),
                                                                 randomComplexMatrix(5, 5, 10000, 3L, true),
                                                                 randomComplexMatrix(5, 5, 10000, 4L, false) };
        for (final ComplexMatrix square1 : square5x5s) {
            for (final ComplexMatrix square2 : square5x5s) {
                for (final boolean transposeA : tf) {
                    for (final boolean conjugateA : tf) {
                        for (final boolean transposeB : tf) {
                            for (final boolean conjugateB : tf) {
                                testParallelMultiply(square1, transposeA, conjugateA, square2, transposeB, conjugateB);
                            }
                        }
                    }
                }
            }
        }
        
        final ComplexMatrix[] square20x20 = new ComplexMatrix[] { randomComplexMatrix(20, 20, 10, 5L, true),
                                                                  randomComplexMatrix(20, 20, 10, 6L, false),
                                                                  randomComplexMatrix(20, 20, 10000, 7L, true),
                                                                  randomComplexMatrix(20, 20, 10000, 8L, false) };
        for (final ComplexMatrix square1 : square20x20) {
            for (final ComplexMatrix square2 : square20x20) {
                for (final boolean transposeA : tf) {
                    for (final boolean conjugateA : tf) {
                        for (final boolean transposeB : tf) {
                            for (final boolean conjugateB : tf) {
                                testParallelMultiply(square1, transposeA, conjugateA, square2, transposeB, conjugateB);
                            }
                        }
                    }
                }
            }
        }
        
        testParallelMultiply(randomComplexMatrix(100, 100, 10000.0, 5L, false), false, false,
                             randomComplexMatrix(100, 100, 10000.0, 6L, false), false, false);
    }
    
    /** Stress-test
     * {@link ComplexMatrix#multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)} with
     * rectangular matrices */
    @Test
    void testParallelMultiplyRectangular() {
        final ComplexMatrix A1 = randomComplexMatrix(10, 100, 100.0, 3L, false);
        final ComplexMatrix B1 = randomComplexMatrix(100, 10, 100.0, 4L, false);
        
        final ComplexMatrix A2 = randomComplexMatrix(10, 100, 100.0, 5L, false);
        final ComplexMatrix B2 = randomComplexMatrix(100, 30, 100.0, 6L, false);
        
        final boolean[] tf = new boolean[] { true, false };
        for (final boolean conjugateA : tf) {
            for (final boolean conjugateB : tf) {
                testParallelMultiply(A1, false, conjugateA, B1, false, conjugateB); // 10x100 . 100x10 -> 10x10
                testParallelMultiply(A1, true,  conjugateA, B1, true,  conjugateB); // 100x10 . 10x100 -> 100x100
                
                testParallelMultiply(A2, false, conjugateA, B2, false, conjugateB); // 10x100 . 100x30 -> 10x30
                testParallelMultiply(B2, true,  conjugateB, A2, true,  conjugateB); // 30x100 . 100x10 -> 30x10
            }
        }
    }
    
    /** Test {@link ComplexMatrix#multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)} -
     * with both {@link Parallelism#PARALLEL} and {@link Parallelism#SEQUENTIAL} by comparing to
     * {@link FieldMatrix#multiply(FieldMatrix)}.
     * 
     * @param A the left {@link ComplexMatrix} in the multiplication
     * @param transposeA {@code true} if we want to consider {@code A}<sup>T</sup>, {@code false} to consider {@code A}
     *        as-is.
     * @param conjugateA {@code true} if we want to consider the {@link Complex#conjugate() complex conjugation} of
     *        {@code A}, {@code false} to consider {@code A} as-is.
     * @param B the right {@link ComplexMatrix} in the multiplication
     * @param transposeB {@code true} if we want to consider {@code B}<sup>T</sup>, {@code false} to consider {@code B}
     *        as-is.
     * @param conjugateB {@code true} if we want to consider the {@link Complex#conjugate() complex conjugation} of
     *        {@code B}, {@code false} to consider {@code B} as-is. */
    private static void testParallelMultiply(final ComplexMatrix A,
                                             final boolean transposeA,
                                             final boolean conjugateA,
                                             final ComplexMatrix B,
                                             final boolean transposeB,
                                             final boolean conjugateB) {
        final ComplexMatrix CopyA = A.copy();
        final ComplexMatrix CopyB = B.copy();

        final FieldMatrix<Complex> ACopy = possiblyConjugateAndOrTranspose(CopyA, transposeA, conjugateA);
        final FieldMatrix<Complex> BCopy = possiblyConjugateAndOrTranspose(CopyB, transposeB, conjugateB);
        final FieldMatrix<Complex> ABSeqFM = ACopy.multiply(BCopy);
        
        final ComplexMatrix AB = new ComplexMatrix(ABSeqFM.getData()); // need ComplexMatrix for assertAbsEquals
        final ComplexMatrix ABPar = ComplexMatrix.multiply(A, transposeA, conjugateA,
                                                           B, transposeB, conjugateB,
                                                           Parallelism.PARALLEL);
        final ComplexMatrix ABSeq = ComplexMatrix.multiply(A, transposeA, conjugateA,
                                                           B, transposeB, conjugateB,
                                                           Parallelism.SEQUENTIAL);
        assertAbsEquals(AB, ABPar, 0, 0); // zero tolerance
        assertAbsEquals(AB, ABSeq, 0, 0); // zero tolerance
    }
    
    /** Manually perform some combination of transposition and conjugation of the given matrix, and copy its data into a
     * new {@link Array2DRowFieldMatrix}. This typing is important, as we want to compare against
     * {@link ComplexMatrix#multiply(FieldMatrix, boolean, boolean, FieldMatrix, boolean, boolean, Parallelism)}.
     * 
     * @param A the original matrix
     * @param transpose whether we want to transpose the matrix
     * @param conjugate whether we want to conjugate the matrix
     * @return the new {@link FieldMatrix} */
    private static Array2DRowFieldMatrix<Complex> possiblyConjugateAndOrTranspose(final ComplexMatrix A,
                                                                 final boolean transpose,
                                                                 final boolean conjugate) {
        final ComplexMatrix copyFrom;
        final ComplexMatrix copyA = A.copy(); 
        if (transpose && conjugate) {
            copyFrom = copyA.conjugateTranspose();
        }
        else if (transpose) {
            copyFrom = copyA.transpose();
        }
        else if (conjugate) {
            copyFrom = copyA.conjugateTranspose().transpose(); // (A^H)^T == conjugate(A)
        }
        else {
            copyFrom = copyA;
        }
        
        return new Array2DRowFieldMatrix<>(copyFrom.getData()); // must use FieldMatrix, not ComplexMatrix!
    }
    
    /** Test that {@link ComplexMatrix#getMaxRank() max rank} is properly set */
    @Test
    void testGetMaxRank() {
        final ComplexMatrix A  = ComplexMatrix.of(ALL_ONES_12X9);
        final ComplexMatrix AT = A.transpose();
        
        assertEquals(9, A.getMaxRank());
        assertEquals(9, AT.getMaxRank());
    }
    
    /** Test the {@link ComplexMatrix} constructor pairs that do/don't take a {@link Parallelism} */
    @Test
    void testConstructorsWithParallelisms() {
        final Complex[][] complexData = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) } };
        
        final ComplexMatrix fromData1 = new ComplexMatrix(complexData);
        final ComplexMatrix fromData2 = new ComplexMatrix(complexData, Parallelism.PARALLEL);
        assertEquals(Parallelism.SEQUENTIAL, fromData1.getParallelism());
        assertEquals(Parallelism.PARALLEL,   fromData2.getParallelism());
        
        final ComplexMatrix fromReal1 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 } });
        final ComplexMatrix fromReal2 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 } },
                                                          Parallelism.PARALLEL);
        assertEquals(Parallelism.SEQUENTIAL, fromReal1.getParallelism());
        assertEquals(Parallelism.PARALLEL,   fromReal2.getParallelism());
        
        final ComplexMatrix fromCopy1 = new ComplexMatrix(fromData1, false);
        final ComplexMatrix fromCopy2 = new ComplexMatrix(fromData1, false, Parallelism.PARALLEL);
        assertEquals(Parallelism.SEQUENTIAL, fromCopy1.getParallelism());
        assertEquals(Parallelism.PARALLEL,   fromCopy2.getParallelism());
        
        final ComplexMatrix fromSize1 = new ComplexMatrix(1, 1);
        final ComplexMatrix fromSize2 = new ComplexMatrix(1, 1, Parallelism.PARALLEL);
        assertEquals(Parallelism.SEQUENTIAL, fromSize1.getParallelism());
        assertEquals(Parallelism.PARALLEL,   fromSize2.getParallelism());
        
        final ComplexMatrix fromArr1 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 } });
        final ComplexMatrix fromArr2 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 } }, Parallelism.PARALLEL);
        assertEquals(Parallelism.SEQUENTIAL, fromArr1.getParallelism());
        assertEquals(Parallelism.PARALLEL,   fromArr2.getParallelism());

        final ComplexMatrix empty = new ComplexMatrix(Parallelism.PARALLEL);
        assertAll(
            () -> assertEquals(Parallelism.PARALLEL, empty.getParallelism()),
            () -> assertEquals(0, empty.getRowDimension()),
            () -> assertEquals(0, empty.getColumnDimension()),
            () -> assertEquals(0, empty.getMaxRank()),
            () -> assertNull(empty.getDataRef()[0]),
            () -> assertNull(empty.getDataRef()[1])
        );
    }
    
    /** Test {@link ComplexMatrix#multiplyConjugateTransposed(FieldMatrix)} */
    @Test
    void testMultiplyConjugateTransposed() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4)},
                                                    { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        final Complex[][] BData = new Complex[][] { { new Complex(2, 2), Complex.ZERO },
                                                    { Complex.ZERO,      new Complex(3, 3) } };
        final ComplexMatrix B = new ComplexMatrix(BData);
        
        final FieldMatrix<Complex> BH = possiblyConjugateAndOrTranspose(B, true, true);
        
        assertEquals(A.multiply(BH), A.multiplyConjugateTransposed(B));
        assertEquals(A.multiply(BH), A.timesConjugateTransposeOf(B));
    }
    
    /** Test {@link ComplexMatrix#conjugateTransposeMultiply(FieldMatrix)} */
    @Test
    void testConjugateTransposeMultiply() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4)},
                                                    { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        final Complex[][] BData = new Complex[][] { { new Complex(2, 2), Complex.ZERO },
                                                    { Complex.ZERO,      new Complex(3, 3) } };
        final ComplexMatrix B = new ComplexMatrix(BData);
        
        final FieldMatrix<Complex> AH = possiblyConjugateAndOrTranspose(A, true, true);
        
        assertEquals(AH.multiply(B), A.conjugateTransposeMultiply(B));
    }
    
    /** Test {@link ComplexMatrix#preMultiply(FieldVector)} and {@link ComplexMatrix#preMultiply(Complex[])} */
    @Test
    void testPreMultiplyVector() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4)},
                                                    { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        final Complex[] vData = new Complex[] { new Complex(9, 10), new Complex(11, 12) };
        
        final FieldMatrix<Complex> AArr2D = new Array2DRowFieldMatrix<>(AData);
        final Complex[] expectedData = AArr2D.preMultiply(vData); // use Hipparchus
        
        final ComplexVector v        = new ComplexVector(vData);
        final ComplexVector expected = new ComplexVector(expectedData);
        assertArrayEquality(expectedData, A.preMultiply(vData)); // use Ptolemaeus
        assertEquals(expected, A.preMultiply(v)); // use Ptolemaeus
    }
    
    /** Test {@link ComplexMatrix#addToEntry(int, int, Complex)},
     *       {@link ComplexMatrix#addToEntry(int, int, double, double)}, and
     *       {@link ComplexMatrix#addToEntryUnsafe(int, int, double, double)} */
    @Test
    void testAddToEntry() {        
        final Complex z = new Complex(1, 2);

        final double[][] ADataRe = new double[][] { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 } };
        final double[][] ADataIm = new double[][] { { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 } };
        final ComplexMatrix A       = new ComplexMatrix(ADataRe, ADataIm);
        final ComplexMatrix CopyA1  = new ComplexMatrix(A.getData());
        final ComplexMatrix CopyA2  = new ComplexMatrix(A.getData());
        
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                          { 2, 3, 2, 2, 2 } },
                                                         new double[][] { { 0, 0, 0, 0, 0 },
                                                                          { 0, 2, 0, 0, 0 } });

        A.addToEntry(1, 1, z.getReal(), z.getImaginary());
        CopyA1.addToEntry(1, 1, z);
        CopyA2.addToEntryUnsafe(1, 1, z.getReal(), z.getImaginary());
        
        assertArrayEquality(expected.getDataRef(), A.getDataRef());
        assertArrayEquality(expected.getDataRef(), CopyA1.getDataRef());
        assertArrayEquality(expected.getDataRef(), CopyA2.getDataRef());
    }
    
    /** Test {@link ComplexMatrix#subtractFromEntry(int, int, double, double)}, and
     *       {@link ComplexMatrix#subtractFromEntryUnsafe(int, int, double, double)} */
    @Test
    void testSubtractFromEntry() {
        final Complex z = new Complex(1, 2);

        final double[][] ADataRe = new double[][] { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 } };
        final double[][] ADataIm = new double[][] { { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 } };
        final ComplexMatrix A       = new ComplexMatrix(ADataRe, ADataIm);
        final ComplexMatrix CopyA1  = new ComplexMatrix(A.getData());
        
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                          { 2, 1, 2, 2, 2 } },
                                                         new double[][] { { 0, 0, 0, 0, 0 },
                                                                          { 0, -2, 0, 0, 0 } });
        
        A.subtractFromEntry(1, 1, z.getReal(), z.getImaginary());
        CopyA1.subtractFromEntryUnsafe(1, 1, z.getReal(), z.getImaginary());
        
        assertArrayEquality(expected.getDataRef(), A.getDataRef());
        assertArrayEquality(expected.getDataRef(), CopyA1.getDataRef());
    }
    
    /** Test {@link ComplexMatrix#multiplyEntry(int, int, double, double)}, 
     *       {@link ComplexMatrix#multiplyEntry(int, int, Complex)}, and 
     *       {@link ComplexMatrix#multiplyEntry(int, int, double)} */
    @Test
    void testMultiplyEntry() {
        final double[] doubleFactor = new double[] {3, 2};
        final Complex complexFactor = new Complex(3, 2);

        final double[][] ADataRe = new double[][] { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 } };
        final double[][] ADataIm = new double[][] { { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 } };
        final ComplexMatrix A = new ComplexMatrix(ADataRe, ADataIm);
        final ComplexMatrix CopyA1 = A.copy();
        final ComplexMatrix CopyA2 = A.copy();

        final ComplexMatrix expected1 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                           { 2, 6, 2, 2, 2 } },
                                                          new double[][] { { 0, 0, 0, 0, 0 },
                                                                           { 0, 4, 0, 0, 0 } });
        
        final ComplexMatrix expected2 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                           { 2, 6, 2, 2, 2 } },
                                                          new double[][] { { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 } });
        
        A.multiplyEntry(1, 1, doubleFactor[0], doubleFactor[1]);
        CopyA1.multiplyEntry(1, 1, complexFactor);
        CopyA2.multiplyEntry(1, 1, 3);

        assertEquals(expected1, A);
        assertEquals(expected1, CopyA1);
        assertEquals(expected2, CopyA2);
    }

    /** Test {@link ComplexMatrix#preMultiply(FieldMatrix)} */
    @Test
    void testPreMultiplyMatrix() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4)},
                                                    { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        final Complex[][] BData = new Complex[][] { { new Complex(2, 2), Complex.ZERO },
                                                    { Complex.ZERO,      new Complex(3, 3) } };
        final ComplexMatrix B = new ComplexMatrix(BData);
        
        final FieldMatrix<Complex> AArr2D = new Array2DRowFieldMatrix<>(AData);
        
        assertEquals(AArr2D.preMultiply(B), A.preMultiply(B));
    }
    
    /** Test {@link ComplexMatrix#preMultiply(Complex[])} */
    @Test
    void testPreMultiplyDouble() {
        final double[][][] AData = new double[][][] {{ {1, 3},
                                                       {5, 6} },
                                                     { {2, 4},
                                                       {6, 8} }};
        final ComplexMatrix A = new ComplexMatrix(AData[0], AData[1]);

        final double[][][] BData = new double[][][] {{ {2, 0},
                                                       {0, 3} },
                                                     { {2, 0},
                                                       {0, 3} }};
        final ComplexMatrix B = new ComplexMatrix(BData[0], BData[1]);

        final FieldMatrix<Complex> AArr2D = new ComplexMatrix(AData[0], AData[1]);

        assertEquals(AArr2D.preMultiply(B), A.preMultiply(B));
    }

    /** Test {@link ComplexMatrix#operate(FieldVector)} and {@link ComplexMatrix#operate(Complex[])} */
    @Test
    void testOperate() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4)},
                                                    { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        final Complex[] vData = new Complex[] { new Complex(9, 10), new Complex(11, 12) };
        
        final FieldMatrix<Complex> AArr2D = new Array2DRowFieldMatrix<>(AData);
        final Complex[] expectedData = AArr2D.operate(vData); // use Hipparchus
        
        final ComplexVector v        = new ComplexVector(vData);
        final ComplexVector expected = new ComplexVector(expectedData);
        assertArrayEquality(expectedData, A.operate(vData)); // use Ptolemaeus
        assertEquals(expected, A.operate(v)); // use Ptolemaeus
    }
    
    /** Test {@link ComplexMatrix#conjugateTransposeOperate(ComplexVector)} */
    @Test
    void testConjugateTransposeOperate() {
        final Complex[][] AData = new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(3, 4) },
                                                    { Complex.valueOf(5, 6), Complex.valueOf(7, 8) } };
        final Complex[]   vData = new Complex[] { new Complex(9, 10), new Complex(11, 12) };
        
        final ComplexMatrix A = new ComplexMatrix(AData);
        final ComplexVector v = new ComplexVector(vData);
        
        assertEquals(A.conjugateTranspose().operate(v), A.conjugateTransposeOperate(v));
    }
    
    /** Test {@link ComplexMatrix#isReal(ComplexMatrix, double)} and {@link ComplexMatrix#isReal(double)} */
    @Test
    void testIsReal() {
        final Complex[][]   recData     = new Complex[][] { { new Complex(1, 0), new Complex(3,  0) },
                                                            { new Complex(5, 0), new Complex(7,  0) },
                                                            { new Complex(9, 0), new Complex(11, 1e-3) } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        assertFalse(rectangular.isReal(0));
        assertTrue(rectangular.isReal(1e-3));
        
        final ComplexMatrix i3 = ComplexMatrix.identity(3);
        assertTrue(ComplexMatrix.isReal(i3, 0));
        
        final Complex[][] data1 = new Complex[][] { { new Complex(1, 0), new Complex(1, 0), new Complex(1, 0) },
                                                    { new Complex(1, 0), new Complex(1, 0), new Complex(1, 0) },
                                                    { new Complex(1, 0), new Complex(1, 0), new Complex(1, 0) } };
        
        final Complex[][] data2 = new Complex[][] { { new Complex(1, 1e-3), new Complex(1, 0), new Complex(1, 0) },
                                                    { new Complex(1, 0), new Complex(1, 0), new Complex(1, 0) },
                                                    { new Complex(1, 0), new Complex(1, 0), new Complex(1, 0) } };
        
        final ComplexMatrix m1 = new ComplexMatrix(data1);
        final ComplexMatrix m2 = new ComplexMatrix(data2);
        
        assertTrue(ComplexMatrix.isReal(m1, 0));
        assertFalse(ComplexMatrix.isReal(m2, 0));
        assertTrue(ComplexMatrix.isReal(m2, 1e-3));
        
        assertTrue(m1.isReal(0));
        assertFalse(m2.isReal(0));
        assertTrue(m2.isReal(1e-3));
    }
    
    /** Test {@link ComplexMatrix#isRealSymmetric(ComplexMatrix, double)} and
     *       {@link ComplexMatrix#isRealSymmetric(double)} */
    @Test
    void testIsRealSymmetric() {
        final ComplexMatrix rectangular = new ComplexMatrix(new double[][] { { 1, 2, 3 },
                                                                             { 1, 2, 3 },
                                                                             { 1, 2, 3 },
                                                                             { 1, 2, 3 },
                                                                             { 1, 2, 3 },
                                                                             { 1, 2, 3 } });
        assertFalse(ComplexMatrix.isRealSymmetric(rectangular, Double.POSITIVE_INFINITY));
        
        final ComplexMatrix i3 = ComplexMatrix.identity(3);
        assertTrue(ComplexMatrix.isRealSymmetric(i3, 0.0));
        
        final ComplexMatrix realSymm = new ComplexMatrix(new double[][] { { 1, 2, 3, 4, 5, 6 },
                                                                          { 2, 3, 4, 5, 6, 1 },
                                                                          { 3, 4, 5, 6, 1, 2 },
                                                                          { 4, 5, 6, 1, 2, 3 },
                                                                          { 5, 6, 1, 2, 3, 4 },
                                                                          { 6, 1, 2, 3, 4, 5 } });
        assertTrue(ComplexMatrix.isRealSymmetric(realSymm, 0.0));
        
        final double sml = 1e-8;
        final double med = 2e-8;
        final double lrg = 3e-8;
        
        final ComplexMatrix notRealDiag = realSymm.copy();
        notRealDiag.setEntry(1, 1, Complex.valueOf(2.0, med));
        assertFalse(ComplexMatrix.isRealSymmetric(notRealDiag, sml));
        assertTrue(ComplexMatrix.isRealSymmetric(notRealDiag, lrg));
        
        final ComplexMatrix notSymm = realSymm.copy();
        notSymm.setEntry(0, 1, Complex.valueOf(2.0 + med, 0.0));
        assertFalse(ComplexMatrix.isRealSymmetric(notSymm, sml));
        assertTrue(ComplexMatrix.isRealSymmetric(notSymm, lrg));
        
        final ComplexMatrix notReal = realSymm.copy();
        notReal.setEntry(0, 1, Complex.valueOf(2.0, med));
        assertFalse(ComplexMatrix.isRealSymmetric(notReal, sml));
        assertTrue(ComplexMatrix.isRealSymmetric(notReal, lrg));
    }
    
    /** Test {@link ComplexMatrix#isSymmetric(ComplexMatrix, Complex)} and {@link ComplexMatrix#isSymmetric(Complex)} */
    @Test
    void testIsSymmetric() {
        final Complex[][]   recData     = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                            { new Complex(5, 6), new Complex(7, 8) },
                                                            { new Complex(9, 10), new Complex(11, 12) } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        assertFalse(ComplexMatrix.isSymmetric(rectangular, Complex.ZERO));
        
        final ComplexMatrix i3 = ComplexMatrix.identity(3);
        assertTrue(ComplexMatrix.isSymmetric(i3, Complex.ZERO));
        
        final Complex[][] data1 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, 1), new Complex(1, 1) } };
        
        final Complex[][] data2 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1.2, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, 1), new Complex(1, 1) } };
        
        final Complex[][] data3 = new Complex[][] { { new Complex(1, 1), new Complex(1.2, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, 1), new Complex(1, 1) } };
        
        final Complex[][] data4 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1.2) },
                                                    { new Complex(0, 0), new Complex(0, 1), new Complex(1, 1) } };
        
        final Complex[][] data5 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, 1.2), new Complex(1, 1) } };
        
        final ComplexMatrix m1 = new ComplexMatrix(data1);
        final ComplexMatrix m2 = new ComplexMatrix(data2);
        final ComplexMatrix m3 = new ComplexMatrix(data3);
        final ComplexMatrix m4 = new ComplexMatrix(data4);
        final ComplexMatrix m5 = new ComplexMatrix(data5);
        
        assertTrue(ComplexMatrix.isSymmetric(m1, Complex.ZERO));
        
        assertFalse(ComplexMatrix.isSymmetric(m2, Complex.ZERO));
        assertFalse(ComplexMatrix.isSymmetric(m3, Complex.ZERO));
        assertFalse(ComplexMatrix.isSymmetric(m4, Complex.ZERO));
        assertFalse(ComplexMatrix.isSymmetric(m5, Complex.ZERO));
        
        assertTrue(ComplexMatrix.isSymmetric(m2, new Complex(0.2, 0.0)));
        assertTrue(ComplexMatrix.isSymmetric(m3, new Complex(0.2, 0.0)));
        assertTrue(ComplexMatrix.isSymmetric(m4, new Complex(0.0, 0.2)));
        assertTrue(ComplexMatrix.isSymmetric(m5, new Complex(0.0, 0.2)));
        
        assertTrue(m1.isSymmetric(Complex.ZERO));
        
        assertFalse(m2.isSymmetric(Complex.ZERO));
        assertFalse(m3.isSymmetric(Complex.ZERO));
        assertFalse(m4.isSymmetric(Complex.ZERO));
        assertFalse(m5.isSymmetric(Complex.ZERO));
        
        assertTrue(m2.isSymmetric(new Complex(0.2, 0.0)));
        assertTrue(m3.isSymmetric(new Complex(0.2, 0.0)));
        assertTrue(m4.isSymmetric(new Complex(0.0, 0.2)));
        assertTrue(m5.isSymmetric(new Complex(0.0, 0.2)));
    }
    
    /** Test {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex)} and {@link ComplexMatrix#isHermitian(Complex)} */
    @Test
    void testIsHermitian() {
        final Complex[][]   recData     = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                            { new Complex(5, 6), new Complex(7, 8) },
                                                            { new Complex(9, 10), new Complex(11, 12) } };
        final ComplexMatrix rectangular = new ComplexMatrix(recData);
        assertFalse(ComplexMatrix.isHermitian(rectangular, Complex.ZERO));
        
        final ComplexMatrix i3 = ComplexMatrix.identity(3);
        assertTrue(ComplexMatrix.isHermitian(i3, Complex.ZERO));
        
        final Complex[][] data1 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, -1), new Complex(1, 1) } };
        
        final Complex[][] data2 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1.2, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, -1), new Complex(1, 1) } };
        
        final Complex[][] data3 = new Complex[][] { { new Complex(1, 1), new Complex(1.2, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, -1), new Complex(1, 1) } };
        
        final Complex[][] data4 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1.2) },
                                                    { new Complex(0, 0), new Complex(0, -1), new Complex(1, 1) } };
        
        final Complex[][] data5 = new Complex[][] { { new Complex(1, 1), new Complex(1, 0), new Complex(0, 0) },
                                                    { new Complex(1, 0), new Complex(1, 1), new Complex(0, 1) },
                                                    { new Complex(0, 0), new Complex(0, -1.2), new Complex(1, 1) } };
        
        final ComplexMatrix m1 = new ComplexMatrix(data1);
        final ComplexMatrix m2 = new ComplexMatrix(data2);
        final ComplexMatrix m3 = new ComplexMatrix(data3);
        final ComplexMatrix m4 = new ComplexMatrix(data4);
        final ComplexMatrix m5 = new ComplexMatrix(data5);
        
        assertTrue(ComplexMatrix.isHermitian(m1, Complex.ZERO));
        
        assertFalse(ComplexMatrix.isHermitian(m2, Complex.ZERO));
        assertFalse(ComplexMatrix.isHermitian(m3, Complex.ZERO));
        assertFalse(ComplexMatrix.isHermitian(m4, Complex.ZERO));
        assertFalse(ComplexMatrix.isHermitian(m5, Complex.ZERO));
        
        assertTrue(ComplexMatrix.isHermitian(m2, new Complex(0.2, 0.0)));
        assertTrue(ComplexMatrix.isHermitian(m3, new Complex(0.2, 0.0)));
        assertTrue(ComplexMatrix.isHermitian(m4, new Complex(0.0, 0.2)));
        assertTrue(ComplexMatrix.isHermitian(m5, new Complex(0.0, 0.2)));
        
        assertTrue(m1.isHermitian(Complex.ZERO));
        
        assertFalse(m2.isHermitian(Complex.ZERO));
        assertFalse(m3.isHermitian(Complex.ZERO));
        assertFalse(m4.isHermitian(Complex.ZERO));
        assertFalse(m5.isHermitian(Complex.ZERO));
        
        assertTrue(m2.isHermitian(new Complex(0.2, 0.0)));
        assertTrue(m3.isHermitian(new Complex(0.2, 0.0)));
        assertTrue(m4.isHermitian(new Complex(0.0, 0.2)));
        assertTrue(m5.isHermitian(new Complex(0.0, 0.2)));
    }
    
    /** Assert that the given matrix is diagonal and that the diagonal elements match elements of the given array
     * @param expectedDiag the expected diagonal 
     * @param actual the {@link ComplexMatrix} to check*/
    private static void assertDiagonalEq(final Complex[] expectedDiag, final FieldMatrix<Complex> actual) {
        final int dim = expectedDiag.length;
        assertEquals(dim, actual.getRowDimension());
        assertEquals(dim, actual.getColumnDimension());
        
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                final Complex ijEntry = actual.getEntry(i, j);
                if (i != j) {
                    assertEquals(Complex.ZERO, ijEntry);
                }
                else {
                    assertEquals(expectedDiag[i], ijEntry);
                }
            }
        }
    }
    
    /** Test {@link ComplexMatrix#conjugateTranspose()} with a square matrix */
    @Test
    void testConjugateTransposeSquare() {
        final Complex[][]   data = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                     { new Complex(5, 6), new Complex(7, 8) } };
        final ComplexMatrix m    = new ComplexMatrix(data);
        final ComplexMatrix mCT  = m.conjugateTranspose();
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                final Complex mIJ   = m.getEntry(i, j);
                final Complex mCTJI = mCT.getEntry(j, i);
                
                assertEquals(mIJ.getReal(), mCTJI.getReal());
                assertEquals(mIJ.getImaginary(), -mCTJI.getImaginary());
            }
        }
    }
    
    /** Test {@link ComplexMatrix#conjugateTranspose()} with a rectangular matrix */
    @Test
    void testConjugateTransposeRectangular() {
        final Complex[][]   data = new Complex[][] { { new Complex(1,  2), new Complex(3,   4) },
                                                     { new Complex(5,  6), new Complex(7,   8) },
                                                     { new Complex(9, 10), new Complex(11, 12) } };
        final ComplexMatrix m    = new ComplexMatrix(data);
        final ComplexMatrix mCT  = m.conjugateTranspose();
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                final Complex mIJ   = m.getEntry(i, j);
                final Complex mCTJI = mCT.getEntry(j, i);
                
                assertEquals(mIJ.getReal(), mCTJI.getReal());
                assertEquals(mIJ.getImaginary(), -mCTJI.getImaginary());
            }
        }
    }
    
    /** Test {@link ComplexMatrix#identity(int)} */
    @Test
    void testIdentity() {
        final ComplexMatrix i1 = ComplexMatrix.identity(1);
        final ComplexMatrix i2 = ComplexMatrix.identity(2);
        final ComplexMatrix i3 = ComplexMatrix.identity(3);
        
        TestUtils.testMultiLineToString(" 1", i1);
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 1 0",
                                                    " 0 1"),
                                        i2);
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 1 0 0",
                                                    " 0 1 0",
                                                    " 0 0 1"),
                                        i3);
    }
    
    /** Test {@link ComplexMatrix#absEquivalentTo(ComplexMatrix, double, double)} */
    @Test
    void testAbsEquivalentTo() {
        final Complex[][]   data1 = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                      { new Complex(5, 6), new Complex(7, 8) } };
        final ComplexMatrix m1    = new ComplexMatrix(data1);
        
        final Complex[][]   data2 = new Complex[][] { { new Complex(1.01, 2.001), new Complex(3.01, 4.001) },
                                                      { new Complex(5.01, 6.001), new Complex(7.01, 8.001) } };
        final ComplexMatrix m2    = new ComplexMatrix(data2);
        
        assertTrue(m1.absEquivalentTo(m1, 0, 0)); // covers the "same" check
        assertTrue(m1.absEquivalentTo(m2, 1.0001e-2, 1.0001e-3)); /* need just a bit more than the offset
                                                                   * for finite precision errors */
        
        assertFalse(m1.absEquivalentTo(m2, 1e-2,      1.0001e-3));
        assertFalse(m1.absEquivalentTo(m2, 1.0001e-2, 1.e-3));
        
        // different dimensions:
        assertFalse(m1.absEquivalentTo(ComplexMatrix.identity(1), 0, 0));
    }
    
    /** Test {@link ComplexMatrix#toString()}
     * 
     * @see MatrixTest#testFormatDoubleLessThanThreshold()
     * @see MatrixTest#testFormatDoubleGreaterThanThreshold()
     * @see MatrixTest#testToString() */
    @Test
    void testToString() {
        final Complex[][] data = new Complex[][] { { new Complex(2, 2), Complex.ZERO, Complex.INF },
                                                   { Complex.ZERO, new Complex(3, 3), Complex.NaN },
                                                   { new Complex(123123123123123.123123123123123, 123.123),
                                                     new Complex(123.123, 123123123123123.123123123123123),
                                                     new Complex(123123.123123, 456456.456456) } };
        
        final ComplexMatrix m = new ComplexMatrix(data);
        final String expected = String.join(System.lineSeparator(),
              "               2.0  +   2.0   i,    0.0   +               0.0  i,         Infinity +        Infinity i",
              "               0.0  +   0.0   i,    3.0   +               3.0  i,              NaN +             NaN i",
              " 123123123123123.12 + 123.123 i,  123.123 + 123123123123123.12 i,  123123.123123   + 456456.456456   i");
        TestUtils.testMultiLineToString(expected, m);
        
        final ComplexMatrix m2 = ComplexMatrix.identity(3); // isReal == true
        final String expected2 = String.join(System.lineSeparator(),
                                             " 1 0 0",
                                             " 0 1 0",
                                             " 0 0 1");
        TestUtils.testMultiLineToString(expected2, m2);
        
        final String expected3 = String.join(System.lineSeparator(),
              "               2.0  +   0.0   i,    0.0   +               0.0  i,         Infinity +        Infinity i",
              "               0.0  +   0.0   i,    3.0   +               3.0  i,              NaN +             NaN i",
              " 123123123123123.12 + 123.123 i,  123.123 + 123123123123123.12 i,  123123.123123   + 456456.456456   i");
        TestUtils.testMultiLineToString(expected3, m.toString(2.0, 3.0));
    }
    
    /** Test {@link ComplexMatrix#ComplexMatrix(Complex[][])} */
    @Test
    void testArrayConstructor() {
        final Complex[][] data = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                   { new Complex(5, 6), new Complex(7, 8) } };
        final ComplexMatrix m = new ComplexMatrix(data);
        
        assertEquals(data[0][0], m.getEntry(0, 0));
        assertEquals(data[0][1], m.getEntry(0, 1));
        assertEquals(data[1][0], m.getEntry(1, 0));
        assertEquals(data[1][1], m.getEntry(1, 1));
        
        assertEquals(data[0][0].getReal(), m.getEntryRe(0, 0));
        assertEquals(data[0][1].getReal(), m.getEntryRe(0, 1));
        assertEquals(data[1][0].getReal(), m.getEntryRe(1, 0));
        assertEquals(data[1][1].getReal(), m.getEntryRe(1, 1));
        
        assertEquals(data[0][0].getImaginary(), m.getEntryIm(0, 0));
        assertEquals(data[0][1].getImaginary(), m.getEntryIm(0, 1));
        assertEquals(data[1][0].getImaginary(), m.getEntryIm(1, 0));
        assertEquals(data[1][1].getImaginary(), m.getEntryIm(1, 1));
    }
    
    /** Test the {@code double[][][]} constructor {@link ComplexMatrix#ComplexMatrix(double[][], double[][])} against
     * the equivalent {@link ComplexMatrix#ComplexMatrix(Complex[][])} */
    @Test
    void test3DDoubleArrayVs2DComplexArray() {
        assertEquals(FROM_COMPLEX_ARR, FROM_DOUBLE_ARRS);
    }
    
    /** Test {@link ComplexMatrix#getDimensionString()} */
    @Test
    void testGetDimensionString() {
        final ComplexMatrix A1 = randomComplexMatrix(5, 10, 1.0, 1L, false);
        final ComplexMatrix A2 = randomComplexMatrix(10, 5, 1.0, 1L, false);
        
        assertEquals("(5 x 10)", A1.getDimensionString());
        assertEquals("(10 x 5)", A2.getDimensionString());
    }
    
    /** Test {@link ComplexMatrix#ComplexMatrix(RealMatrix)} */
    @Test
    void testFromRealConstructor() {
        final double[][] realData = new double[][] { { 1, 2 }, { 3, 4 } };
        final RealMatrix realM = new ptolemaeus.math.linear.real.Matrix(realData);
        final ComplexMatrix m = new ComplexMatrix(realM);
        
        assertEquals(new Complex(realData[0][0]), m.getEntry(0, 0));
        assertEquals(new Complex(realData[0][1]), m.getEntry(0, 1));
        assertEquals(new Complex(realData[1][0]), m.getEntry(1, 0));
        assertEquals(new Complex(realData[1][1]), m.getEntry(1, 1));
    }
    
    /** Test {@link ComplexMatrix#ComplexMatrix(double[][])} */
    @Test
    void testFromDoubleArrayConstructor() {
        final double[][] realData = new double[][] { { 1, 2 }, { 3, 4 } };
        final ComplexMatrix m = new ComplexMatrix(realData);
        
        assertEquals(new Complex(realData[0][0]), m.getEntry(0, 0));
        assertEquals(new Complex(realData[0][1]), m.getEntry(0, 1));
        assertEquals(new Complex(realData[1][0]), m.getEntry(1, 0));
        assertEquals(new Complex(realData[1][1]), m.getEntry(1, 1));
    }
    
    /** Test {@link ComplexMatrix#hashCode()} */
    @Test
    void testHashCode() {
        assertEquals(FROM_COMPLEX_ARR.hashCode(), FROM_DOUBLE_ARRS.hashCode());
        assertEquals(-1920601348, FROM_DOUBLE_ARRS.hashCode());
    }
    
    /** Test {@link ComplexMatrix#equals(Object)} */
    @Test
    void testEquals() {
        assertFalse(FROM_DOUBLE_ARRS.equals(null));
        assertFalse(FROM_DOUBLE_ARRS.equals(new Object()));
        assertNotEquals(FROM_DOUBLE_ARRS, ALL_ONES_12X9);
        
        final ComplexMatrix notEq = FROM_DOUBLE_ARRS.copy();
        notEq.setEntry(3, 3, Complex.ZERO);
        
        assertNotEquals(FROM_DOUBLE_ARRS, notEq);
        
        assertEquals(FROM_DOUBLE_ARRS, FROM_DOUBLE_ARRS);
        assertEquals(FROM_DOUBLE_ARRS, FROM_COMPLEX_ARR);
    }

    /** Test setRow()
     * <br> {@link ComplexMatrix#getRow(int)} <br>
     * <br> {@link ComplexMatrix#setRow(int, Complex[])} <br>
     */
    @Test
    void testGetSetRow() {
            final ComplexMatrix A = DIAG_9X9.copy();
            final double[][][] AData = A.getDataRef();
                                    
            final Complex[] ARow = A.getRow(8);

            final Complex nineComplex = new Complex(9, 0);
            final Complex[] expectedRow = new Complex[] { Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                          Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                          Complex.ZERO, Complex.ZERO, nineComplex  }; 
            assertArrayEquality(expectedRow, ARow);

            // setRow()
            final ComplexMatrix ARowComplex = new ComplexMatrix(AData[0], AData[1]);

            final Complex[] rowData = new Complex[]  { nineComplex, nineComplex, nineComplex, 
                                                       nineComplex, nineComplex, nineComplex,
                                                       nineComplex, nineComplex, nineComplex };
            ARowComplex.setRow(8, rowData);

            final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                                                                              { 9, 9, 9, 9, 9, 9, 9, 9, 9 } });

            assertArrayEquality(expected.getData(), ARowComplex.getData());
    }

    /** Test Exceptionals for: <br>
     * <br> {@link ComplexMatrix#setRow(int, Complex[])} <br>
     * 
    */
    @Test
    void testSetRowExceptional() {
        final ComplexMatrix A = DIAG_9X9.copy();
        final double[][][] AData = A.getDataRef();

        final Complex[] ARow = A.getRow(8);

        final Complex nineComplex = new Complex(9, 0);
        final Complex[] expectedRow = new Complex[] { Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                      Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                      Complex.ZERO, Complex.ZERO, nineComplex  }; 
        assertArrayEquality(expectedRow, ARow);

        // setRow()
        final ComplexMatrix ARowComplex = new ComplexMatrix(AData[0], AData[1]);

        final Complex[] rowData = new Complex[]  { nineComplex, nineComplex, nineComplex, 
                                                   nineComplex, nineComplex, nineComplex };
        
        // attempting to write a 1x6 row into a 1x9 row, dimensions don't match, should throw an error
        final MathIllegalArgumentException me1 = assertThrows(MathIllegalArgumentException.class,
                                                                () -> ARowComplex.setRow(8, rowData));
        
        assertEquals("got 1x6 but expected 1x9", me1.getMessage());
    }

    /** Test setColumn()
     * <br> {@link ComplexMatrix#getColumn(int)} <br>
     * <br> {@link ComplexMatrix#setColumn(int, Complex[])} <br>
     */
    @Test
    void testGetSetColumn() {
            final ComplexMatrix A = DIAG_9X9.copy();
            final double[][][] AData = A.getDataRef();
            
            // getColumn()                        
            final Complex[] AColumn = A.getColumn(8);

            final Complex nineComplex = new Complex(9, 0);
            final Complex[] expectedColumn = new Complex[] { Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                             Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                             Complex.ZERO, Complex.ZERO, nineComplex  }; 
            assertArrayEquality(expectedColumn, AColumn);

            // setColumn()
            final ComplexMatrix AColumnComplex = new ComplexMatrix(AData[0], AData[1]);

            final Complex[] columnData = new Complex[] { nineComplex, nineComplex, nineComplex, 
                                                         nineComplex, nineComplex, nineComplex,
                                                         nineComplex, nineComplex, nineComplex };
            AColumnComplex.setColumn(8, columnData);

            final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 9 },
                                                                              { 0, 2, 0, 0, 0, 0, 0, 0, 9 },
                                                                              { 0, 0, 3, 0, 0, 0, 0, 0, 9 },
                                                                              { 0, 0, 0, 4, 0, 0, 0, 0, 9 },
                                                                              { 0, 0, 0, 0, 5, 0, 0, 0, 9 },
                                                                              { 0, 0, 0, 0, 0, 6, 0, 0, 9 },
                                                                              { 0, 0, 0, 0, 0, 0, 7, 0, 9 },
                                                                              { 0, 0, 0, 0, 0, 0, 0, 8, 9 },
                                                                              { 0, 0, 0, 0, 0, 0, 0, 0, 9 } });

            assertArrayEquality(expected.getData(), AColumnComplex.getData());
    }

    /** Test exceptional cases of {@link ComplexMatrix#setColumn(int, Complex[])} */
    @Test
    void testSetColumnExceptional() {
        final ComplexMatrix A = DIAG_9X9.copy();            
        final Complex[] columnData =  new Complex[] { Complex.ZERO, Complex.ZERO, Complex.ZERO, 
                                                      Complex.ZERO, Complex.ZERO, Complex.ZERO }; 

        final MathIllegalArgumentException nme1 = assertThrows(MathIllegalArgumentException.class,
                                                               () -> A.setColumn(0, columnData));

        assertEquals("got 6x1 but expected 9x1", nme1.getMessage());
    }


    /** Test
     *  {@link ComplexMatrix#add(FieldMatrix)} and <br>
     *  {@link ComplexMatrix#subtract(FieldMatrix)}
     */
    @Test
    void testAddSubtract() {
        final ComplexMatrix A = ALL_TWOS_3X3;

        final double[][] BDataRe = new double[][] { { 1, 0, 0 },
                                                    { 0, 2, 0 },
                                                    { 0, 0, 3 } };
        final double[][] BDataIm = new double[][] { { 1, 0, 0 },
                                                    { 0, 2, 0 },
                                                    { 0, 0, 3 } };
        final ComplexMatrix B = new ComplexMatrix(BDataRe, BDataIm);
        final Array2DRowFieldMatrix<Complex> BField = new Array2DRowFieldMatrix<>(B.getData());

        final ComplexMatrix expectedAdd = new ComplexMatrix(new double[][] { { 3, 2, 2 },
                                                                             { 2, 4, 2 },
                                                                             { 2, 2, 5 } },
                                                            new double[][] { { 1, 0, 0 },
                                                                             { 0, 2, 0 },
                                                                             { 0, 0, 3 } });

        final ComplexMatrix expectedSub = new ComplexMatrix(new double[][] { { 1, 2, 2 },
                                                                             { 2, 0, 2 },
                                                                             { 2, 2, -1 } },
                                                            new double[][] { { -1, 0, 0 },
                                                                             { 0, -2, 0 },
                                                                             { 0, 0, -3 } });
        assertArrayEquality(A.add(B).getData(), expectedAdd.getData());
        assertArrayEquality(A.plus(BField).getData(), expectedAdd.getData());
        assertArrayEquality(A.subtract(B).getData(), expectedSub.getData());
        assertArrayEquality(A.minus(BField).getData(), expectedSub.getData());

    }

    /** Test {@link ComplexMatrix#scalarAdd(Complex)},
     *       {@link ComplexMatrix#scalarAdd(double, double)},
     *       {@link ComplexMatrix#scalarMultiply(Complex)}, and
     *       {@link ComplexMatrix#dividedBy(Complex)} */
    @Test
    void testScalarsComplex() {
        final double[][]    Re = { {  1,  2,  3 }, {  4,  5,  6 }, {  7,  8,  9 } };
        final double[][]    Im = { { -1, -2, -3 }, { -4, -5, -6 }, { -7, -8, -9 } };
        final ComplexMatrix A  = new ComplexMatrix(Re, Im);
        
        final Complex c = new Complex(10, 0.1);
        
        // scalarAdd(Complex)
        final ComplexMatrix expectedAddC = ComplexMatrix.of(new Array2DRowFieldMatrix<>(A.getData()).scalarAdd(c));
        assertEquals(expectedAddC, A.scalarAdd(c));
        assertEquals(expectedAddC, A.scalarAdd(c.getReal(), c.getImaginary()));
        
        // scalarMultiply(Complex)                                                      
        final ComplexMatrix expectedMultC =
                ComplexMatrix.of(new Array2DRowFieldMatrix<>(A.getData()).scalarMultiply(c));
        assertEquals(expectedMultC, A.scalarMultiply(c));
        
        // dividedBy(Complex)
        final ComplexMatrix expectedDivC =
                ComplexMatrix.of(new Array2DRowFieldMatrix<>(A.getData()).scalarMultiply(Complex.ONE.divide(c)));
        
        final double eps = 1e-16; /* need a tolerance because - due to floating point imprecision - dividing is not
                                   * necessarily absolutely equivalent to multiplying by the inverse */
        assertAbsEquals(expectedDivC, A.dividedBy(c), eps, eps);
    }
    
    /** Test {@link ComplexMatrix#scalarAdd(double, double)} with a rectangular {@link ComplexMatrix} */
    @Test
    void testScalarAddRectangle() {
        final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                   { 1, 2, 1, 1, 1 },
                                                                   { 1, 1, 3, 1, 1 },
                                                                   { 1, 1, 1, 4, 1 },
                                                                   { 1, 1, 1, 1, 5 },
                                                                   { 1, 1, 1, 4, 1 },
                                                                   { 1, 1, 3, 1, 1 },
                                                                   { 1, 2, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 7, 7, 7, 7, 7 },
                                                                          { 7, 8, 7, 7, 7 },
                                                                          { 7, 7, 9, 7, 7 },
                                                                          { 7, 7, 7, 10, 7 },
                                                                          { 7, 7, 7, 7, 11 },
                                                                          { 7, 7, 7, 10, 7 },
                                                                          { 7, 7, 9, 7, 7 },
                                                                          { 7, 8, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 } },
                                                         new double[][] { { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 } });
        
        assertEquals(expected, A.scalarAdd(6, 7));
    }
    
    /** Test {@link ComplexMatrix#scalarMultiply(double)} */
    @Test
    void testScalarMultiplyRectangular1() {
        final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                   { 1, 2, 1, 1, 1 },
                                                                   { 1, 1, 3, 1, 1 },
                                                                   { 1, 1, 1, 4, 1 },
                                                                   { 1, 1, 1, 1, 5 },
                                                                   { 1, 1, 1, 4, 1 },
                                                                   { 1, 1, 3, 1, 1 },
                                                                   { 1, 2, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 7, 7, 7, 7, 7 },
                                                                          { 7, 14, 7, 7, 7 },
                                                                          { 7, 7, 21, 7, 7 },
                                                                          { 7, 7, 7, 28, 7 },
                                                                          { 7, 7, 7, 7, 35 },
                                                                          { 7, 7, 7, 28, 7 },
                                                                          { 7, 7, 21, 7, 7 },
                                                                          { 7, 14, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 } });
        
        assertEquals(expected, A.scalarMultiply(7));
    }
    
    /** Test {@link ComplexMatrix#scalarMultiply(double, double)} */
    @Test
    void testScalarMultiplyRectangular2() {
        final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                   { 1, 2, 1, 1, 1 },
                                                                   { 1, 1, 3, 1, 1 },
                                                                   { 1, 1, 1, 4, 1 },
                                                                   { 1, 1, 1, 1, 5 },
                                                                   { 1, 1, 1, 4, 1 },
                                                                   { 1, 1, 3, 1, 1 },
                                                                   { 1, 2, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 } });
        
        final ComplexMatrix expected = new ComplexMatrix(new double[][] { { 7, 7, 7, 7, 7 },
                                                                          { 7, 14, 7, 7, 7 },
                                                                          { 7, 7, 21, 7, 7 },
                                                                          { 7, 7, 7, 28, 7 },
                                                                          { 7, 7, 7, 7, 35 },
                                                                          { 7, 7, 7, 28, 7 },
                                                                          { 7, 7, 21, 7, 7 },
                                                                          { 7, 14, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 } },
                                                         new double[][] { { 7, 7, 7, 7, 7 },
                                                                          { 7, 14, 7, 7, 7 },
                                                                          { 7, 7, 21, 7, 7 },
                                                                          { 7, 7, 7, 28, 7 },
                                                                          { 7, 7, 7, 7, 35 },
                                                                          { 7, 7, 7, 28, 7 },
                                                                          { 7, 7, 21, 7, 7 },
                                                                          { 7, 14, 7, 7, 7 },
                                                                          { 7, 7, 7, 7, 7 } });
        
        assertEquals(expected, A.scalarMultiply(7, 7));
    }
    
    /** Test {@link ComplexMatrix#scalarMultiply(double)}, and
     *       {@link ComplexMatrix#dividedBy(double)} */
    @Test
    void testScalarsReal() {
        final double[][]    Re = { {  1,  2,  3 }, {  4,  5,  6 }, {  7,  8,  9 } };
        final double[][]    Im = { { -1, -2, -3 }, { -4, -5, -6 }, { -7, -8, -9 } };
        final ComplexMatrix A  = new ComplexMatrix(Re, Im);
        
        final double d = 10;
        
        // scalarAdd(double)
        final ComplexMatrix expectedAddD = new ComplexMatrix(new double[][] { { 11, 12, 13 },
                                                                              { 14, 15, 16 },
                                                                              { 17, 18, 19 } },
                                                             new double[][] { { -1, -2, -3 },
                                                                              { -4, -5, -6 },
                                                                              { -7, -8, -9 } });
        assertArrayEquality(expectedAddD.getDataRef(), A.scalarAdd(d, 0).getDataRef());
        
        // scalarMultiply(double)
        final ComplexMatrix expectedMultD = new ComplexMatrix(new double[][] { { 10, 20, 30 },
                                                                               { 40, 50, 60 },
                                                                               { 70, 80, 90 } },
                                                              new double[][] { { -10, -20, -30 },
                                                                               { -40, -50, -60 },
                                                                               { -70, -80, -90 } });
        assertArrayEquality(expectedMultD.getDataRef(), A.scalarMultiply(d).getDataRef());
        
        // dividedBy(double)
        final ComplexMatrix expectedDivideD = new ComplexMatrix(new double[][] { { 0.1, 0.2, 0.3 },
                                                                                 { 0.4, 0.5, 0.6 },
                                                                                 { 0.7, 0.8, 0.9 } },
                                                                new double[][] { { -0.1, -0.2, -0.3 },
                                                                                 { -0.4, -0.5, -0.6 },
                                                                                 { -0.7, -0.8, -0.9 } });
        assertArrayEquality(expectedDivideD.getDataRef(), A.dividedBy(d).getDataRef());
    }
    
    /** Test
     * {@link ComplexMatrix#power(int)}
     */
    @Test
    void testPower() {
        final double[][]    Re = { {  1,  2,  3 }, {  4,  5,  6 }, {  7,  8,  9 } };
        final double[][]    Im = { { -1, -2, -3 }, { -4, -5, -6 }, { -7, -8, -9 } };
        final ComplexMatrix A  = new ComplexMatrix(Re, Im);
        
        final int p = 3;

        final ComplexMatrix expected = ComplexMatrix.of(new Array2DRowFieldMatrix<>(A.getData()).power(p));

        assertArrayEquality(A.power(p).getDataRef(), expected.getDataRef());
        
    }

    /** Test
     * {@link ComplexMatrix#checkSameDoubleDimension(double[][], double[][])}
     */
    @Test
    void testcheckSameDoubleDimension() {
        final double[][]    Re = { {  1,  2,  3 }, {  4,  5,  6 }, {  7,  8,  9 } };
        final double[][]    Im = { { -1, -2, -3 }, { -4, -5, -6 }, { -7, -8, -9 } };

        final double[][]    ReEx = { {  1,  2,  3 }, {  4,  5,  6 }, {  7,  8,  9 }, { 10, 11, 12 } };
        final double[][]    ImEx = { { -1, -2, -3, -4 }, { -5, -6, -7, -8}, { -9, -10, -11, -12} };

        assertThrows(MathIllegalArgumentException.class, () -> new ComplexMatrix(Re, ImEx));
        assertThrows(MathIllegalArgumentException.class, () -> new ComplexMatrix(ReEx, Im));
    }

    /**
     * Tests {@link ComplexMatrix#extractDoubleData(FieldMatrix)} on an {@link AbstractComplexMatrixSlice}
     */
    @Test
    void testExtractDoubleData() {
        final double[][][] slicedData = ComplexMatrix.extractDoubleData(FROM_DOUBLE_ARRS.slice(0, 1, 0, 1));
        assertArrayEquality(slicedData, new double[][][] {
                    {{1.0, 1.0}, {1.0, 2.0}},    // real
                    {{-1.0, -1.0}, {-1.0, -2.0}} // imaginary
                }
        );
    }
}