/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.concurrent.NotThreadSafe;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.WindowConstants;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Validation;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.real.Matrix;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** Program to visualize the magnitudes of a {@link ComplexMatrix} or {@link Matrix}.<br>
 * <br>
 * 
 * Finite non-zeros interpolate between colors of {@link #COLOR_MAP}, zeros show as {@link Color#BLACK}, NaNs show as
 * {@link Color#MAGENTA}, and infinite values show as {@link Color#ORANGE}.
 * 
 * @param <M> may only be one of {@link ComplexMatrix} and {@link Matrix}
 * @author Bruce Romney, bruce.romney@lmco.com */
@NotThreadSafe
public class MatrixVisualizer<M extends AnyMatrix> extends JPanel {
    
    /** The {@link MatrixVisualizer} mode determines how we show the matrices
     * 
     * @author Ryan Moser, Ryan.T.Moser@lmco.com */
    public enum MatrixVisMode {
        
        /** in {@link #DIFFERENCE} mode, when a new matrix is added, we show the difference between it and the first */
        DIFFERENCE,
        
        /** in {@link #AS_IS} mode, we show the matrices as they are */
        AS_IS;
    }
    
    /** generated */
    private static final long serialVersionUID = -3485960501384149284L;

    /**
     * Minimum number of blank pixels around the perimeter of the matrix
     */
    private static final int MARGIN = 20;
    
    /** 
     * The {@link Color}s of Class-8 Red-Green-Blue, in order.<br>
     *
     * <br>
     * See the following link for an example:
     * <a href="https://colorbrewer2.org/#type=diverging&scheme=RdYlBu&n=8">class-8 RedGreenBlue</a> */
    public static final List<Color> COLOR_MAP = List.of(
            new Color(69,  117, 180),
            new Color(116, 173, 209),
            new Color(171, 217, 233),
            new Color(224, 243, 248),
            new Color(254, 224, 144),
            new Color(253, 174,  97),
            new Color(244, 109,  67),
            new Color(215,  48,  39));
    
    /**
     * All matrices to be displayed
     */
    private List<M> allMatrices;
    
    /**
     * The slider
     */
    private JSlider slider;
    
    /**
     * The matrix currently being displayed
     */
    private M matrix;
    
    /**
     * Maximum magnitude, for color-scaling purposes
     */
    private double maxMagnitude;
    
    /**
     * Minimum magnitude, for color-scaling purposes
     */
    private double minMagnitude;
    
    /**
     * Number of matrix rows
     */
    private final int numRows;
    
    /**
     * Number of matrix columns
     */
    private final int numCols;
    
    /**
     * The {@link MatrixVisMode} to use
     */
    private final MatrixVisMode mode;
    
    /**
     * Constructor
     * @param mtx the matrix to be displayed
     */
    @SuppressFBWarnings("EI2")
    public MatrixVisualizer(final M mtx) {
        this(mtx, MatrixVisMode.AS_IS);
    }
    
    /**
     * Constructor
     * @param mtx the matrix to be displayed
     * @param mode the {@link MatrixVisMode} to use
     */
    @SuppressFBWarnings("EI2")
    public MatrixVisualizer(final M mtx, final MatrixVisMode mode) {
        checkMatrixType(mtx);
        
        this.mode = mode;
        allMatrices = new ArrayList<>();
        matrix = mtx;
        maxMagnitude = 0;
        minMagnitude = Double.POSITIVE_INFINITY;
        slider = null;
        this.add(mtx);
        numRows = matrix.getRowDimension();
        numCols = matrix.getColumnDimension();

        // Choose a default cell size in pixels, so that the display initially takes up
        // about half the screen height OR width
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int sx = (int) (screenSize.getWidth() / (2 * numCols));
        int sy = (int) (screenSize.getHeight() / (2 * numRows));
        int s = FastMath.min(sx, sy);
        setPreferredSize(new Dimension(s * numCols + 2 * MARGIN, s * numRows + 2 * MARGIN));

        // Create a new window to display 'this'
        JFrame frame = new JFrame("Matrix Visualization");
        frame.getContentPane().add(embed());
        frame.pack();
        int x = (int) (screenSize.getWidth() - frame.getWidth()) / 2;
        int y = (int) (screenSize.getHeight() - frame.getHeight()) / 2;
        frame.setLocation(x, y);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
    
    /** If {@code M} is not one of {@link ComplexMatrix} or {@link Matrix}, we throw an exception
     * 
     * @param M the matrix */
    private static void checkMatrixType(final AnyMatrix M) {
        Validation.requireTrue(M instanceof ComplexMatrix || M instanceof Matrix,
                            () -> "%s is not supported by MatrixVisualizer".formatted(M.getClass().getCanonicalName()));
    }
    
    /**
     * Add another matrix to the list of matrices to be displayed
     * @param mtx the matrix to be added
     */
    @SuppressWarnings("unchecked")
    public void add(final M mtx) {
        if ((mtx.getRowDimension() != matrix.getRowDimension()) 
                || (mtx.getColumnDimension() != matrix.getColumnDimension())) {
            throw new IllegalArgumentException("Inconsistent matrix sizes");
        }
        
        checkMatrixType(mtx);
        
        final M copy = (M) (mtx instanceof ComplexMatrix cast ? cast.copy() : ((Matrix) mtx).copy());
        allMatrices.add(copy);
        
        if (mode == MatrixVisMode.AS_IS) {
            this.updateMinMaxMagnitude(copy);
        }
        else if (mode == MatrixVisMode.DIFFERENCE) {
            final int size = allMatrices.size();
            if (size > 1) {
                final M prev = this.allMatrices.get(size - 2);
                final M curr = this.allMatrices.get(size - 1);
                final M difference;
                if (prev instanceof ComplexMatrix prevCast) {
                    final ComplexMatrix currCast = (ComplexMatrix) curr;
                    difference = (M) currCast.minus(prevCast);
                }
                else {
                    final Matrix currCast = (Matrix) curr;
                    difference = (M) currCast.subtract(((Matrix) prev));
                }
                
                this.updateMinMaxMagnitude(difference);
            }
        }
        else {
            throw new RuntimeException("Unrecognized MatrixVisMode: " + this.mode);
        }
        
        if (slider != null) {       // slider is null during initialization
            slider.setEnabled(true);
            slider.setMaximum(allMatrices.size() - 1);
            slider.setValue(allMatrices.size() - 1);
        }
    }
    
    /**
     * Update {@link #minMagnitude} and {@link #maxMagnitude} to encompass the magnitudes in 'm'
     * @param m the matrix to be considered 
     */
    private void updateMinMaxMagnitude(final M m) {
        for (int i = 0; i < m.getRowDimension(); i++) {
            for (int j = 0; j < m.getColumnDimension(); j++) {
                final double valueToPlot;
                if (m instanceof ComplexMatrix cast) {
                    valueToPlot = getValueToPlot(cast.getEntry(i, j));
                }
                else {
                    valueToPlot = getValueToPlot(Complex.valueOf(((Matrix) m).getEntry(i, j)));
                }
                
                if (Double.isFinite(valueToPlot)) {
                    maxMagnitude = FastMath.max(maxMagnitude, valueToPlot);
                    minMagnitude = FastMath.min(minMagnitude, valueToPlot);
                }
            }
        }
    }
    
    @Override
    public void paintComponent(final Graphics g) {
        // Figure out scaling
        Dimension sz = getSize();
        double xScale = (sz.width - 2.0 * MARGIN) / numCols;
        double yScale = (sz.height - 2.0 * MARGIN) / numRows;
        int pixelsPerCell = (int) FastMath.min(xScale, yScale);
        int x0 = (sz.width - numCols * pixelsPerCell) / 2;
        int y0 = (sz.height - numRows * pixelsPerCell) / 2;
        int x1 = x0 + numCols * pixelsPerCell;
        int y1 = y0 + numRows * pixelsPerCell;
        
        // Fill the panel with white
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, sz.width, sz.height);

        // Draw the colored cells
        for (int i = 0; i < numRows; ++i) {
            int y = y0 + i * pixelsPerCell;
            for (int j = 0; j < numCols; ++j) {
                int x = x0 + j * pixelsPerCell;
                final Color c;
                if (matrix instanceof ComplexMatrix cast) {
                    c = chooseColor(cast.getEntry(i, j));
                }
                else {
                    c = chooseColor(Complex.valueOf(((Matrix) matrix).getEntry(i, j)));
                }
                g.setColor(c);
                g.fillRect(x, y, pixelsPerCell, pixelsPerCell);
            }
        }
        
        // Overlay the grid
        g.setColor(Color.BLACK);
        for (int i = 0; i <= numRows; ++i) {
            g.drawLine(x0, y0 + i * pixelsPerCell, x1, y0 + i * pixelsPerCell);
        }
        for (int i = 0; i <= numCols; ++i) {
            g.drawLine(x0 + i * pixelsPerCell, y0, x0 + i * pixelsPerCell, y1);
        }
    }
    
    /** Given a {@link Complex}, determine the value to use when {@link #chooseColor(Complex) choosing a color} or
     * {@link #updateMinMaxMagnitude(AnyMatrix) updating the min and max values}
     * 
     * @param z the {@link Complex} value
     * @return the {@code double} to plot.  Always non-negative. */
    private static double getValueToPlot(final Complex z) {
        final double magn = z.norm();
        return magn <= 1.0 ? magn : 1.0 + FastMath.log(magn);
    }
    
    /**
     * Choose the color to use to represent 'value'
     * @param value the value to be displayed
     * @return the color to use
     */
    private Color chooseColor(final Complex value) {
        if (value.isZero()) {
            return Color.BLACK;
        }
        if (value.isNaN()) {
            return Color.MAGENTA;
        }
        if (value.isInfinite()) {
            return Color.ORANGE;
        }
        
        double valueToPlot = getValueToPlot(value);
        double range = maxMagnitude - minMagnitude;
        double colorMapIndex = (range == 0) 
                                     ? (COLOR_MAP.size() / 2.0)
                                     : ((COLOR_MAP.size() - 1) * (valueToPlot - minMagnitude) / range);
        
        int k = FastMath.min((int) colorMapIndex, COLOR_MAP.size() - 2);
        Color c1 = COLOR_MAP.get(k);
        Color c2 = COLOR_MAP.get(k + 1);
        double frac = colorMapIndex - k;
        int red   = (int) ((1 - frac) * c1.getRed()   + frac * c2.getRed());
        int green = (int) ((1 - frac) * c1.getGreen() + frac * c2.getGreen());
        int blue  = (int) ((1 - frac) * c1.getBlue()  + frac * c2.getBlue());
        return new Color(red, green, blue);
    }
    
    /**
     * Embed this {@link MatrixVisualizer} into a larger panel which includes a slider
     * @return the larger {@link JPanel}
     */
    @SuppressWarnings("unchecked")
    public JPanel embed() {
        JPanel p = new JPanel(new GridBagLayout());
        p.setBackground(Color.WHITE);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        p.add(this, c);
        
        ++(c.gridy);
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        slider = new JSlider(0, 1);
        slider.setEnabled(false);
        slider.setMajorTickSpacing(1);
        slider.setSnapToTicks(true);
        slider.setPaintTicks(true);
        slider.setOpaque(false);
        slider.addChangeListener(event -> {
            final M toSet;
            if (mode == MatrixVisMode.AS_IS) {
                toSet = this.allMatrices.get(slider.getValue());
            }
            else if (mode == MatrixVisMode.DIFFERENCE) {
                final int index = slider.getValue();
                if (index == 0) {
                    toSet = this.allMatrices.get(index);
                }
                else {
                    final M prev = this.allMatrices.get(index - 1);
                    final M curr = this.allMatrices.get(index);
                    if (prev instanceof ComplexMatrix prevCast) {
                        final ComplexMatrix currCast = (ComplexMatrix) curr;
                        toSet = (M) currCast.minus(prevCast);
                    }
                    else {
                        final Matrix currCast = (Matrix) curr;
                        toSet = (M) currCast.subtract(((Matrix) prev));
                    }
                }
            }
            else {
                throw new RuntimeException("Unrecognized MatrixVisMode: " + this.mode);
            }
            
            this.matrix = toSet;
            this.repaint();
        });
        p.add(slider, c);
        
        return p;
    }
    
    // -----------------------------------------------------------------------------------------------------
    // Demo program
    
    /**
     * Main program
     * @param args ignored
     */
    public static void main(final String[] args) {
        final ComplexMatrix A = new ComplexMatrix(new Matrix(new double[][] { 
            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 1, 2, 2, 2, 2, 2, 2, 2, 2 },
            { 1, 2, 3, 3, 3, 3, 3, 3, 3 },
            { 1, 2, 3, 4, 4, 4, 4, 4, 4 },
            { 1, 2, 3, 4, 5, 5, 5, 5, 5 },
            { 1, 2, 3, 4, 5, 6, 6, 6, 6 },
            { 1, 2, 3, 4, 5, 6, 7, 7, 7 },
            { 1, 2, 3, 4, 5, 6, 7, 8, 8 },
            { 1, 2, 3, 4, 5, 6, 7, 8, 9 } }));
        
        MatrixVisualizer<ComplexMatrix> viz = new MatrixVisualizer<>(A);
        for (int i = 2; i <= 10; i++) {
            FieldMatrix<Complex> F = A.scalarMultiply(Complex.valueOf(i, 2 * i));
            final ComplexMatrix M =  new ComplexMatrix(F.getData());
            viz.add(M);
        }
    }
}
