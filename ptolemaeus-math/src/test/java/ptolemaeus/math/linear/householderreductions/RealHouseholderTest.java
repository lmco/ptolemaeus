/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.MatrixUtils;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.linear.real.NVectorTest;

/** Test {@link RealHouseholder}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class RealHouseholderTest {
    
    /** a vector for testing */
    private static final double[] X_ARR = {  1,  2,  3, 4, 5, 6 };
    
    
    /** a zero vector */
    private static final double[] ZERO_ARR = { 0, 0, 0, 0, 0, 0 };
    
    /** Test {@link RealHouseholder#preMultiplySubMatrix(double[], double[][], int, int, int)} by pre-multiplying the
     * sub-matrix
     * 
     * <pre>
     * 4  0  0  0  0
     * 0  5  0  0  0
     * 0  0  6  0  0
     * 0  0  0  7  0
     * 0  0  0  0  8
     * </pre>
     * 
     * by (1, 1, 1, 1, 1) to get (4, 5, 6, 7, 8) */
    @Test
    void testPreMultiplySubMatrixSimple() {
        final double[][] A = { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                               { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                               { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                               { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                               { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                               { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                               { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                               { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                               { 0, 0, 0, 0, 0, 0, 0, 0, 9 } };
        
        final double[] w = { 1, 1, 1, 1, 1 };
        
        final double[] wA = RealHouseholder.preMultiplySubMatrix(w, A, 3, 3, 7);
        assertArrayEquality(new double[] { 4, 5, 6, 7, 8 }, wA, 0.0);
    }
    
    /** Test {@link RealHouseholder#preMultiplySubMatrix(double[], double[][], int, int, int)} with random inputs and
     * testing against the value computed by actually constructing the sub-matrix. */
    @Test
    void testPreMultiplySubMatrixRandom() {
        final Matrix  A = MatrixTest.randomMatrix(10, 10, 1.0, 123L);
        final NVector w = NVectorTest.randomNVector(5, 1.0, 456L);
        
        final double[][] AArr = A.getData();
        final double[]   wArr = w.getDataRef();
        
        final double[] expected = A.getSubMatrix(3, 3 + w.getDimension() - 1, 3, 7).preMultiply(w).toArray();
        final double[] wAArr    = RealHouseholder.preMultiplySubMatrix(wArr, AArr, 3, 3, 7);
        
        assertArrayEquality(expected, wAArr, 0.0);
    }
    
    /** Test {@link RealHouseholder#postMultiplySubMatrix(double[][], double[], int, int, int)} by post-multiplying the
     * sub-matrix
     * 
     * <pre>
     * 4  0  0  0  0
     * 0  5  0  0  0
     * 0  0  6  0  0
     * 0  0  0  7  0
     * 0  0  0  0  8
     * </pre>
     * 
     * by (1, 1, 1, 1, 1) to get (4, 5, 6, 7, 8) */
    @Test
    void testPostMultiplySubMatrixSimple() {
        final double[][] A = { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                               { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                               { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                               { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                               { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                               { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                               { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                               { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                               { 0, 0, 0, 0, 0, 0, 0, 0, 9 } };
        
        final double[] w = { 1, 1, 1, 1, 1 };
        
        final double[] Aw = RealHouseholder.postMultiplySubMatrix(A, w, 3, 7, 3);
        assertArrayEquality(new double[] { 4, 5, 6, 7, 8 }, Aw, 0.0);
    }
    
    /** Test {@link RealHouseholder#postMultiplySubMatrix(double[][], double[], int, int, int)} with random inputs and
     * testing against the value computed by actually constructing the sub-matrix. */
    @Test
    void testPostMultiplySubMatrixRandom() {
        final Matrix  A = MatrixTest.randomMatrix(10, 10, 1.0, 123L);
        final NVector w = NVectorTest.randomNVector(5, 1.0, 456L);
        
        final double[][] AArr = A.getData();
        final double[]   wArr = w.getDataRef();
        
        final double[] expected = A.getSubMatrix(3, 7, 3, 3 + w.getDimension() - 1).operate(w).toArray();
        final double[] AwArr    = RealHouseholder.postMultiplySubMatrix(AArr, wArr, 3, 7, 3);
        
        assertArrayEquality(expected, AwArr, 0.0);
    }
    
    /** Test some noop instance edge-cases */
    @Test
    void testNOOPs() {
        // one way to get the noop instance is to use the zero-vector:
        final RealHouseholder ch = RealHouseholder.of(new double[10]);
        assertEquals(Matrix.identity(10), ch.getTransform());
        
        final Matrix I = Matrix.identity(10);
        
        ch.preApplyInPlace(I, 0, 0);
        MatrixTest.assertMatrixEquals(Matrix.identity(10), I, 0); // unchanged
        
        ch.postApplyInPlace(I, 0, 0);
        MatrixTest.assertMatrixEquals(Matrix.identity(10), I, 0); // unchanged
    }
    
    /** Test {@link RealHouseholder#preApplyInPlace} by showing that it's equivalent to computing the actual
     * Householder matrix and pre-multiplying by it. We do this by just operating on the identity matrix, and the
     * correctness of the transformation matrix is shown in other tests here. */
    @Test
    void testPreApplyInPlace() {
        final NVector         x  = NVectorTest.randomNVector(10, 1.0, 123L);
        final RealHouseholder ch = RealHouseholder.of(x.getDataRef());
        
        final Matrix shouldBeTransform = Matrix.identity(10);
        ch.preApplyInPlace(shouldBeTransform, 0, 0);
        
        final Matrix transform = ch.getTransform();
        assertEquals(transform, shouldBeTransform); // zero tolerance!
    }
    
    /** Test {@link RealHouseholder#postApplyInPlace} by showing that it's equivalent to computing the actual
     * Householder matrix and post-multiplying by it. We do this by just operating on the identity matrix, and the
     * correctness of the transformation matrix is shown in other tests here. */
    @Test
    void testPostApplyInPlace() {
        final NVector         x  = NVectorTest.randomNVector(10, 1.0, 123L);
        final RealHouseholder ch = RealHouseholder.of(x.getDataRef());
        
        final Matrix shouldBeTransform = Matrix.identity(10);
        ch.postApplyInPlace(shouldBeTransform, 0, 0);
        
        final Matrix transform = ch.getTransform();
        assertEquals(transform, shouldBeTransform); // zero tolerance!
    }
    
    /** Test an edge case of {@link RealHouseholder#computeTransform(double[])} where the input vector has norm so small
     * that squaring it results in zero. */
    @Test
    void testNormSqZero() {
        final double ltSqrtSmallest = FastMath.sqrt(Double.MIN_VALUE) / 2;
        final double[] re = new double[] { ltSqrtSmallest, 0, 0, 0, 0 };
        
        assertEquals(Matrix.identity(5), RealHouseholder.computeTransform(re));
    }
    
    /** Test the real-valued case of {@link RealHouseholder#iMinusTwiceSelfOuterProduct(double[])} */
    @Test
    void testIMinusTwiceSelfOuterProductReal() {
        final NVector x        = new NVector(1, 2, 3, 4, 5, 6);
        final Matrix  xxH      = x.outerProduct(x).scalarMultiply(2.0);
        final Matrix  expected = Matrix.identity(6).minus(xxH);
        final Matrix  actual   = RealHouseholder.iMinusTwiceSelfOuterProduct(new double[] { 1, 2, 3, 4, 5, 6 });
        
        assertEquals(expected, actual);
    }
    
    /** Test {@link RealHouseholder#isZero(double[])} */
    @Test
    void testIsZeroReal() {
        assertFalse(RealHouseholder.isZero(X_ARR));
        assertTrue(RealHouseholder.isZero(ZERO_ARR));
    }
    
    /** Test {@link RealHouseholder#computeTransform(RealVector, int)} with a RealHouseholder index of zero */
    @Test
    void testRealVectorZeroIndex() {
        final NVector x = new NVector(1, 2, 3, 4, 5, 6, 7); // 7 x 1
        final Matrix  q = RealHouseholder.computeTransform(x, 0);  // 7 x 7
        
        assertDoesNotThrow(() -> MatrixUtils.checkSymmetric(q, 0.0)); // perfectly symmetric!
        
        final Matrix qSq = q.power(2);
        assertTrue(qSq.absEquivalentTo(Matrix.identity(7), 1e-15));
        
        final NVector qx = q.operate(x);
        for (int i = 0; i < 7; i++) {
            final double qxi = qx.getEntry(i);
            if (i == 0) {
                assertEquals(-x.getNorm(), qxi, 1e-14);
            }
            else {
                assertEquals(0.0, qxi, 2e-15);
            }
        }
    }
    
    /** Test {@link RealHouseholder#computeTransform(RealVector, int)} with a non-zero RealHouseholder index. Notice the
     * analogues to {@link #testRealVectorZeroIndex()}. */
    @Test
    void testRealVectorNonZeroIndex() {
        final int hessenbergOffset = 3;
        final NVector x = new NVector(1, 2, 3, 4, 5, 6, 7); // 7 x 1
        final Matrix  q = RealHouseholder.computeTransform(x, hessenbergOffset);  // 7 x 7
        
        // notice the 4x4 structure in the bottom right
        final String expected = String.join(System.lineSeparator(),
                " 1 0 0  0.0                 0.0                  0.0                  0.0                ",
                " 0 1 0  0.0                 0.0                  0.0                  0.0                ",
                " 0 0 1  0.0                 0.0                  0.0                  0.0                ",
                " 0 0 0 -0.3563483225498991 -0.4454354031873739  -0.5345224838248487  -0.6236095644623234 ",
                " 0 0 0 -0.4454354031873739  0.8537155278522498  -0.17554136657730018 -0.20479826100685022",
                " 0 0 0 -0.5345224838248487 -0.17554136657730018  0.7893503601072398  -0.24575791320822027",
                " 0 0 0 -0.6236095644623234 -0.20479826100685022 -0.24575791320822027  0.7132824345904096 ");
        TestUtils.testMultiLineToString(expected, q);
        
        assertDoesNotThrow(() -> MatrixUtils.checkSymmetric(q, 0.0)); // perfectly symmetric!
        
        final double eps = x.getNorm() * Numerics.MACHINE_EPSILON;
        
        final NVector qx = q.operate(x);
        for (int i = 0; i < 7; i++) {
            final double qxi = qx.getEntry(i);
            if (i < hessenbergOffset) {
                assertEquals(x.getEntry(i), qxi, eps);
            }
            else if (i == hessenbergOffset) {
                final double subNorm = x.getSubVector(hessenbergOffset, 4).getNorm();
                assertEquals(subNorm, -qxi, eps);
            }
            else {
                assertEquals(0.0, qxi, eps);
            }
        }
    }
    
    /** Test {@link RealHouseholder#computeTransform(RealVector, int)} with a zero-vector to hit a special-case */
    @Test
    void testAllZeroesReal() {
        final NVector v = new NVector(0.0, 0.0, 0.0, 0.0, 0.0);
        testReal(v, 0.0);
    }
    
    /** Test {@link RealHouseholder#computeTransform(RealVector, int)} some special-cases to ensure we properly handle
     * any zero vectors that pop up. */
    @Test
    void testManyZeroesReal() {
        final NVector v1 = new NVector(0.0, 0.0, 0.0, 1.0, 0.0);
        testReal(v1, 2 * Numerics.MACHINE_EPSILON);
        
        final NVector v2 = new NVector(1.0, 0.0, 0.0, 0.0, 0.0);
        testReal(v2, 0.0); // zero tolerance
    }
    
    /** Test {@link RealHouseholder#computeTransform(RealVector, int)} with a few random {@link NVector}s */
    @Test
    void testRandomRealVectors() {
        testReal(NVectorTest.randomNVector(10,   10.0, 123L), 2e-15);
        testReal(NVectorTest.randomNVector(10,  100.0, 456L), 3e-14);
        testReal(NVectorTest.randomNVector(10, 1000.0, 789L), 3e-13);
    }
    
    /** Test {@link RealHouseholder#computeTransform(RealVector, int)} by showing that the {@link Matrix} result behaves
     * as expected and has the expected properties.<br>
     * <br>
     * 
     * Notice that we can use zero tolerance when testing whether {@link MatrixUtils#isSymmetric(RealMatrix, double)},
     * regardless of input.
     * 
     * @param x the {@link NVector} to pass into {@link RealHouseholder#computeTransform(RealVector, int)}
     * @param absTol the absolute error tolerance when comparing doubles */
    private static void testReal(final NVector x, final double absTol) {
        final Matrix  q = RealHouseholder.computeTransform(x, 0); // x is n x 1 -> q is n x n
        
        assertDoesNotThrow(() -> MatrixUtils.checkSymmetric(q, 0.0)); // perfectly symmetric!
        
        final int dim = x.getDimension();
        final Matrix identity = Matrix.identity(dim);
        
        final Matrix qSq = q.power(2);
        assertTrue(qSq.absEquivalentTo(identity, absTol), () -> "Error:%n%s".formatted(qSq.subtract(identity)));
        
        final NVector qx = q.operate(x);
        for (int i = 0; i < dim; i++) {
            final double qxi = qx.getEntry(i);
            if (i == 0) {
                assertEquals(-x.getNorm(), qxi, absTol);
            }
            else {
                assertEquals(0.0, qxi, absTol);
            }
        }
    }
}
