/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.MatrixVisualizer;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;

/** Tests for {@link ComplexHessenbergReduction}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class ComplexHessenbergReductionTest {
    
    /** Test {@link ComplexHessenbergReduction#of(ComplexMatrix, int, Consumer)} with a non-null {@link Consumer}
     * monitoring evaluation */
    @Test
    void testWithEvalMonitor() {
        final ComplexMatrix A = new ComplexMatrix(new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                              { 1, 2, 2, 2, 2 },
                                                                              { 1, 2, 3, 3, 3 },
                                                                              { 1, 2, 3, 4, 4 },
                                                                              { 1, 2, 3, 4, 5 } }));
        
        final AtomicReference<ComplexMatrix> atomicM = new AtomicReference<>(null);
        final Consumer<ComplexMatrix>        monitor = atomicM::set;
        final ComplexHessenbergReduction     hess    = ComplexHessenbergReduction.of(A, 1, monitor);
        
        assertSame(hess.getR(), atomicM.get());
    }
    
    /** Test {@link ComplexHessenbergReduction} with a 10-cyclic matrix as described
     * {@link MatrixTest#nCyclicMatrix(int) here}. This hits a few special-cases in the algorithm. */
    @Test
    void testNCyclic() {
        final ComplexMatrix nCyclic10 = ComplexMatrixTest.nCyclicMatrix(10);
        testComplexMatrix(nCyclic10, 1, 0.0, 0.0); // no tolerance needed
    }
    
    /** Test creating a 1-Hessenberg {@link ComplexHessenbergReduction#of(ComplexMatrix)} with a 9x9 symmetric real
     * matrix */
    @Test
    void testRealSymmetric1Hessenberg() {
        final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                   { 1, 2, 2, 2, 2, 2, 2, 2 },
                                                                   { 1, 2, 3, 3, 3, 3, 3, 3 },
                                                                   { 1, 2, 3, 4, 4, 4, 4, 4 },
                                                                   { 1, 2, 3, 4, 5, 5, 5, 5 },
                                                                   { 1, 2, 3, 4, 5, 6, 6, 6 },
                                                                   { 1, 2, 3, 4, 5, 6, 7, 7 },
                                                                   { 1, 2, 3, 4, 5, 6, 7, 8 } });
        
        final int n = 1;
        final ComplexMatrix R = testComplexMatrix(A, n, 2e-15, 2e-15);
        
        /* performing a Hessenberg reduction on a real symmetric matrix results in a banded matrix; e.g., a 1-Hessenberg
         * reduction on a real symmetric matrix results in a symmetric tri-diagonal Matrix, as shown below */
        final String expected = String.join(System.lineSeparator(),
                                            " * *            ",
                                            " * * *          ",
                                            "   * * *        ",
                                            "     * * *      ",
                                            "       * * *    ",
                                            "         * * *  ",
                                            "           * * *",
                                            "             * *",
                                            "");
        TestUtils.testMultiLineToString(expected, Numerics.toNonZeroStructureString(R, 2e-14, '*'));
    }
    
    /** Test creating 3-Hessenberg {@link ComplexHessenbergReduction#of(ComplexMatrix)} with a 9x9 symmetric real
     * matrix */
    @Test
    void testRealSymmetric3Hessenberg() {
        final ComplexMatrix A = new ComplexMatrix(new Matrix(new double[][] { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                              { 1, 2, 2, 2, 2, 2, 2, 2, 2 },
                                                                              { 1, 2, 3, 3, 3, 3, 3, 3, 3 },
                                                                              { 1, 2, 3, 4, 4, 4, 4, 4, 4 },
                                                                              { 1, 2, 3, 4, 5, 5, 5, 5, 5 },
                                                                              { 1, 2, 3, 4, 5, 6, 6, 6, 6 },
                                                                              { 1, 2, 3, 4, 5, 6, 7, 7, 7 },
                                                                              { 1, 2, 3, 4, 5, 6, 7, 8, 8 },
                                                                              { 1, 2, 3, 4, 5, 6, 7, 8, 9 } }));
        
        final int n = 3;
        final ComplexMatrix R = testComplexMatrix(A, n, 8.00e-15, 5.23e-16);
        
        /* performing a Hessenberg reduction on a real symmetric matrix results in a banded matrix; e.g., a 1-Hessenberg
         * reduction on a real symmetric matrix results in a symmetric tri-diagonal Matrix, as shown below */
        final String expected = String.join(System.lineSeparator(),
                                            " * * * *          ",
                                            " * * * *          ",
                                            " * * * *          ",
                                            " * * * * * * *    ",
                                            "       * * * * *  ",
                                            "       * * * * *  ",
                                            "       * * * * * *",
                                            "         * * * * *",
                                            "             * * *",
                                            "");
        TestUtils.testMultiLineToString(expected, Numerics.toNonZeroStructureString(R, 2e-14, '*'));
    }
    
    /** Test {@link ComplexHessenbergReduction#of(ComplexMatrix)} with a non-symmetric real matrix */
    @Test
    void testRealNonSymmetric1Hessenberg() {
        final ComplexMatrix A = new ComplexMatrix(new Matrix(new double[][] { { 1, 20, 30, 40, 50 },
                                                                              { 1, 2, 30, 4, 50 },
                                                                              { 1, 2, 3, 40, 5 },
                                                                              { 1, 2, 3, 40, 50 },
                                                                              { 1, 20, 3, 4, 5 } }));
        
        final int n = 1;
        final ComplexMatrix R = testComplexMatrix(A, n, 2.85e-14, 2.44e-15);
        
        // CHECKSTYLE.OFF: LineLength
        final String expected = String.join(System.lineSeparator(),
                "  1.0                -70.00000000000001  -11.358152736593492 -5.7189080186366095  18.3925652774861  ",
                " -2.0000000000000004  65.75000000000003   11.90421777200664   5.422328284612274  -31.87593849929026 ",
                "  0.0                -25.752427070084103 -30.864503816793903 -5.853184067903161   23.68974935240609 ",
                "  0.0                  0.0               -30.480288016654775 11.829672082116613  -24.495744193691156",
                "  0.0                  0.0                 0.0               14.820938356055555    3.284831734677284");
        // CHECKSTYLE.ON: LineLength
        TestUtils.testMultiLineToString(expected, R.toString(1e-14, 1e-14)); // tolerances to hide nearly zero values
    }
    
    /** Test {@link ComplexHessenbergReduction#of(ComplexMatrix)} with a non-symmetric real matrix
     * 
     * It's not much (6 * machEps rather than 5 * machEps), but I still don't like it. */
    @Test
    void testRealNonSymmetric3Hessenberg() {
        final ComplexMatrix A = new ComplexMatrix(new Matrix(new double[][] { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                              { 10, 2, 2, 2, 2, 2, 2, 2, 2 },
                                                                              { 1, 20, 3, 3, 3, 3, 3, 3, 3 },
                                                                              { 10, 2, 30, 4, 4, 4, 4, 4, 4 },
                                                                              { 1, 20, 3, 40, 5, 5, 5, 5, 5 },
                                                                              { 10, 2, 30, 4, 50, 6, 6, 6, 6 },
                                                                              { 1, 20, 3, 40, 5, 60, 7, 7, 7 },
                                                                              { 10, 2, 30, 4, 50, 6, 70, 8, 8 },
                                                                              { 1, 20, 3, 40, 5, 60, 7, 80, 9 } }));

        final int n = 3;
        final ComplexHessenbergReduction hess = ComplexHessenbergReduction.of(A, n);
        
        final ComplexMatrix P = hess.getQ();
        final ComplexMatrix R = hess.getR();
        
        assertQRQHIsA(A, hess);
        ComplexQRDecompositionTest.assertUnitary(P);
        assertIsNHessenberg(R, n, 2e-14, 2e-14);
        
        /* To illustrate, here's a string representation. Note the interesting non-zero pattern here. The zero-block in
         * the upper-right is due to certain entries being symmetric. */
        final String expected = String.join(System.lineSeparator(),
                                            " * * * * *        ",
                                            " * * * * *        ",
                                            " * * * * *        ",
                                            " * * * * * * * * *",
                                            "   *   * * * * * *",
                                            "       * * * * * *",
                                            "       * * * * * *",
                                            "         * * * * *",
                                            "             * * *",
                                            "");
        TestUtils.testMultiLineToString(expected, Numerics.toNonZeroStructureString(R, 2e-14, '*'));
    }
    
    /** Test a few random 10x10 {@link ComplexMatrix complex matrices} */
    @Test
    void testRandomComplexMatrices() {
        createAndTestRandom(10, 3, 1.0,     12L);
        createAndTestRandom(10, 3, 10.0,    34L);
        createAndTestRandom(10, 3, 100.0,   56L);
        createAndTestRandom(10, 3, 1000.0,  78L);
        createAndTestRandom(10, 1, 10000.0, 90L);
    }
    
    /** Test {@link ComplexHessenbergReduction} with a random
     * {@link ComplexMatrix#isHermitian(Complex) Hermitian matrix} */
    @Test
    void testRandomHermitian() {
        final int dim = 32;
        final ComplexMatrix A = ComplexMatrixTest.randomSquare(dim, 123L);
        final ComplexMatrix H = A.timesConjugateTransposeOf(A);
        
        assertTrue(H.isHermitian(Complex.ZERO)); // sanity check
        
        final double absTol = dim * Numerics.MACHINE_EPSILON;
        testComplexMatrix(H, 1, absTol, absTol);
    }
    
    /** Create and test a random square {@link ComplexMatrix} with {@link ComplexHessenbergReduction}. We use
     * {@link ComplexMatrixTest#randomComplexMatrix(int, int, double, long, boolean)} to create the test matrices.
     * 
     * @param dim the dimension of the square matrix
     * @param n the number of sub-diagonals to preserve in the Hessenberg form
     * @param maxRowHermNorm the max {@link ComplexVector#hermitianNorm()} of any of the rows (this is just Euclidean
     *        distance if all real).
     * @param seed the random seed
     *        
     * @see #testComplexMatrix(ComplexMatrix, int, double, double) */
    void createAndTestRandom(final int dim,
                             final int n,
                             final double maxRowHermNorm,
                             final long seed) {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(dim, dim, maxRowHermNorm, seed, false);
        final double absTol = dim * maxRowHermNorm * Numerics.MACHINE_EPSILON;
        testComplexMatrix(A, n, absTol, absTol);
    }
    
    /** Test the results of applying {@link ComplexHessenbergReduction} to the given {@link ComplexMatrix}
     * 
     * @param A the test {@link ComplexMatrix}
     * @param n the number of sub-diagonals to preserve in the reduction
     * @param absReTol the absolute tolerance when comparing the real parts of complex numbers
     * @param absImTol the absolute tolerance when comparing the imaginary parts of complex numbers 
     * @return the Hessenberg matrix */
    private static ComplexMatrix testComplexMatrix(final ComplexMatrix A,
                                                   final int n,
                                                   final double absReTol,
                                                   final double absImTol) {
        /* first, test the standard use-case: internally copy A (so it's unmodified) and return the factors of the
         * Hessenberg reduction: */
        final ComplexHessenbergReduction hess = ComplexHessenbergReduction.of(A, n);
        
        final ComplexMatrix Q = hess.getQ();
        final ComplexMatrix R = hess.getR();
        
        assertQRQHIsA(A, hess);
        ComplexQRDecompositionTest.assertUnitary(Q);
        assertIsNHessenberg(R, n, absReTol, absImTol);
        
        /* Now, do it again, but use the do-in-place functionality ComplexHessenbergReduction::doInPlace that directly
         * modifies QInPlace and RInPlace and test their equality: */
        final ComplexMatrix QInPlace = ComplexMatrix.identity(A.getColumnDimension());
        final ComplexMatrix RInPlace = A.copy();
        final boolean isHermitian = A.isHermitian(Complex.ZERO);
        ComplexHessenbergReduction.doInPlace(QInPlace, RInPlace, isHermitian, n, null);
        
        /* In the following Q == QIPlace equality test, the Frobenius norm of the error matrix is < 7e-16 for in all
         * cases; i.e., negligible. This difference is due how we compute Q when using the "of" methods, and how we
         * accumulate Q in-place.  See code comments for details. */
        ComplexMatrixTest.assertBackwardsStableError(Q, QInPlace, 1);
        assertEquals(R, RInPlace); // identically equal
        
        final ComplexMatrix shouldBeA = QInPlace.times(RInPlace).timesConjugateTransposeOf(QInPlace);
        ComplexMatrixTest.assertBackwardsStableError(A, shouldBeA, 1);
        ComplexQRDecompositionTest.assertUnitary(QInPlace);
        assertIsNHessenberg(RInPlace, n, absReTol, absImTol);
        
        return R;
    }
    
    /** Assert that the {@link ComplexHessenbergReduction} was properly computed by checking whether
     * {@code QRQ}<sup>H</sup> {@code == A} as expected (within tolerance)
     * 
     * @param A the {@link ComplexMatrix} being reduced
     * @param hess the {@link ComplexHessenbergReduction} */
    private static void assertQRQHIsA(final ComplexMatrix A, final ComplexHessenbergReduction hess) {
        final ComplexMatrix Q    = hess.getQ();
        final ComplexMatrix R    = hess.getR();
        final ComplexMatrix QRQH = Q.times(R).timesConjugateTransposeOf(Q);
        
        ComplexMatrixTest.assertBackwardsStableError(A, QRQH, 1);
    }
    
    /** Assert that the given {@link ComplexMatrix} is {@code n}-Hessenberg; i.e., that is has all zeroes after
     * {@code n} sub diagonals. 0-Hessenberg is upper-triangular.
     * 
     * @param R the {@link ComplexMatrix} to test
     * @param n the number of sub-diagonals to skip
     * @param absReTol the absolute tolerance when comparing the real parts of complex numbers
     * @param absImTol the absolute tolerance when comparing the imaginary parts of complex numbers */
    public static void assertIsNHessenberg(final ComplexMatrix R,
                                           final int n,
                                           final double absReTol,
                                           final double absImTol) {
        final int numRows = R.getRowDimension();
        final int numCols = R.getColumnDimension();
        
        double worstReError = 0.0;
        double worstImError = 0.0;
        for (int j = 0; j < numCols; j++) {
            for (int i = j + n + 1; i < numRows; i++) {
                final int     finalI = i;
                final int     finalJ = j;
                final Complex ij     = R.getEntry(i, j);
                
                worstReError = FastMath.max(worstReError, FastMath.abs(ij.getReal()));
                worstImError = FastMath.max(worstImError, FastMath.abs(ij.getImaginary()));
                
                final boolean ijIsZero = Precision.equals(ij.getReal(), 0.0, absReTol)
                                         && Precision.equals(ij.getImaginary(), 0.0, absImTol);
                assertTrue(ijIsZero,
                           () -> "Element [%d, %d] -> %s != (0, 0)%nR = %n%s".formatted(finalI, finalJ, ij, R));
            }
        }
        
        // these are covered by the assertion in loop above, but I want to keep the worst case values around for testing
        final Complex worstErrorsZ = Complex.valueOf(worstReError, worstImError);
        assertTrue(worstReError <= absReTol, () -> "Errors: %s".formatted(worstErrorsZ));
        assertTrue(worstImError <= absImTol, () -> "Errors: %s".formatted(worstErrorsZ));
    }
    
    /** A mini program that uses {@link MatrixVisualizer} to display the results of a
     * {@link ComplexHessenbergReduction}.<br>
     * <br>
     * 
     * I'm using a symmetric matrix because symmetric matrices are reduced to tri-diagonal form by a Hessenberg
     * reduction, and that looks neat.
     * 
     * @param args ignored
     * @throws InterruptedException if {@link Thread#sleep(long)} is interrupted */
    public static void main(final String[] args) throws InterruptedException {
        final ComplexMatrix              A         = ComplexMatrixTest.randomComplexMatrix(20, 20, 1.0, 1L, false);
        final MatrixVisualizer<ComplexMatrix> mViz = new MatrixVisualizer<>(A);
        ComplexHessenbergReduction.of(A, 1, mViz::add);
    }
}