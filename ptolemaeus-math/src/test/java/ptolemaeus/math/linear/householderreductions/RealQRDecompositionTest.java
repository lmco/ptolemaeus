/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertLessThanOrEqualTo;

import org.junit.jupiter.api.Test;

import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.random.RandomDataGenerator;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Precision;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.LinearSolverTest;
import ptolemaeus.math.linear.MatrixVisualizer;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.linear.real.NVectorTest;

/** Tests for {@link RealQRDecomposition}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class RealQRDecompositionTest {
    
    /** a local copy of {@link Numerics#MACHINE_EPSILON} just so I don't have to keep writing it out */
    private static final double MACH_EPS = Numerics.MACHINE_EPSILON;
    
    /** When checking whether {@link RealQRDecomposition#getQ()} is unitary, we use this epsilon.<br>
     * <br>
     * 
     * Notable that this one value works - seemingly - regardless of scale (notice {@link #testLargeScale()}, which has
     * row vectors with {@link RealVector#getNorm()} up to 1e9), and {@link #testIllConditioned()} which tests
     * a terribly conditioned matrix, */
    private static final double ID_EPS = 5 * MACH_EPS;
    
    /** the column vector dimensions we want to test */
    private static final int[] DIMENSIONS = new int[] { 5, 10, 25, 50, 75 };
    
    /** Test {@link RealQRDecomposition#cleanup(Matrix)} and {@link RealQRDecomposition#adjustSingularities(Matrix)} */
    @Test
    void testCleanupAndAdjustSingularities() {
        final Matrix A = new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                     { 1, 0, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 0, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 },
                                                     { 1, 1, 1, 1, 1 } });
        
        RealQRDecomposition.cleanup(A);
        
        final Matrix expected1 = new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                             { 0, 0, 1, 1, 1 },
                                                             { 0, 0, 1, 1, 1 },
                                                             { 0, 0, 0, 0, 1 },
                                                             { 0, 0, 0, 0, 1 },
                                                             { 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0 } });
        
        assertEquals(expected1, A);
        
        final double e = Numerics.getInftyNormMatrixEpsilon(A);
        
        final Matrix expected2 = new Matrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                             { 0, e, 1, 1, 1 },
                                                             { 0, 0, 1, 1, 1 },
                                                             { 0, 0, 0, e, 1 },
                                                             { 0, 0, 0, 0, 1 },
                                                             { 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0 } });
        
        RealQRDecomposition.adjustSingularities(A);
        
        assertEquals(expected2, A);
    }
    
    /** Test a case where the input matrix is singular and {@link RealQRDecomposition#getR()} has at least one exact
     * zero on the main diagonal. */
    @Test
    void testPerfectlySingular() {
        final Matrix A = Matrix.ones(10, 10);
        
        final RealQRDecomposition QR = RealQRDecomposition.of(A, false, false); /* (false, false) so we return the QR
                                                                                 * decomposition without modification */
        for (int i = 5; i < 10; i++) {
            assertEquals(0.0, QR.getR().getEntry(i, i));
        }
        
        testMatrix(A, 3);
    }
    
    /** Test an exceptional case of {@link RealQRDecomposition#of(Matrix)}
     * where the input matrix is "wide", which is not supported */
    @Test
    void testExceptional() {
        final Matrix A = MatrixTest.randomMatrix(5, 10, 1.0, 1L);
        final ValidationException ex = assertThrows(ValidationException.class,
                                                    () -> RealQRDecomposition.of(A));
        
        assertEquals("Input matrix A is of dimension (5 x 10), but the "
                             + "QR decomposition can only be performed on square "
                             + "(m == n) and tall matrices (m > n)",
                     ex.getMessage());
    }

    /** Test a {@code 10 x 10} {@link Matrix} which has rows whose {@link RealVector#getNorm()} is at
     * most {@code 1e9}.<br><br>
     * 
     * As expected, the required epsilons are a function of the scale of the values in the matrix. */
    @Test
    void testLargeScale() {
        createAndTestRandom(10, 10, 1e9, 8675309L);
    }
    
    /** Test a few random {@code 10 x 10} {@link Matrix matrices} <br> */
    @Test
    void testAFewReal() {
        createAndTestRandom(10, 10, 1.0,     12L);
        createAndTestRandom(10, 10, 10.0,    34L);
        createAndTestRandom(10, 10, 100.0,   56L);
        createAndTestRandom(10, 10, 1000.0,  78L);
        createAndTestRandom(10, 10, 1000.0,  100L);
        createAndTestRandom(10, 10, 10000.0, 90L);
    }
    
    /** Test {@link RealQRDecomposition} using a few random rectangular matrices */
    @Test
    void testAFewComplexRectangular() {
        createAndTestRandom(5,  3, 1.0,   12L);
        createAndTestRandom(10, 5, 100.0, 34L);
        createAndTestRandom(20, 3, 100.0, 56L);
    }
    
    /** Test {@link RealQRDecomposition} with a known non-singular real matrix */
    @Test
    void testNonSingularReal3x3() {
        final double[][] data = { { 12, -51,   4 },
                                  {  6, 167, -68 },
                                  { -4,  24, -41 } };
        final Matrix A = new Matrix(data);
        testMatrix(A, 2);
    }
    
    /** Test {@link RealQRDecomposition} with a known singular real matrix */
    @Test
    void testSingularReal3x3One() {
        final double[][] data = { { 1, 4, 7, },
                                  { 2, 5, 8, },
                                  { 3, 6, 9, } };
        final Matrix A = new Matrix(data);
        testMatrix(A, 1);
    }
    
    /** Test {@link RealQRDecomposition} with another known singular real matrix */
    @Test
    void testSingularReal3x3Two() {
        double[][] data = {  {  1, 6,  4 },
                             {  2, 4, -1 },
                             { -1, 2,  5 } };
        final Matrix A = new Matrix(data);
        testMatrix(A, 2);
        
        final RealQRDecomposition QR1 = RealQRDecomposition.of(A);
        final RealQRDecomposition QR2 = RealQRDecomposition.of((RealMatrix) A);
        
        assertEquals(QR1.getQ(), QR2.getQ());
        assertEquals(QR1.getR(), QR2.getR());
    }
    
    /** Do a stress-test with a deliberately ill-conditioned matrix */
    @Test
    void testIllConditioned() {
        final double maxRowHermNorm = 1e9;
        final Matrix illCond = testIllConditioned(maxRowHermNorm);
        
        // Do a toString check just for reference.  The poor conditioning is clear.
        // CHECKSTYLE.OFF: LineLength
        final String expected = String.join(System.lineSeparator(),
                "         0.0                0.0                 475021613.3057391           174040051.62589988                 0.0                0.42996816335155996 492894225.9056642            175322744.35737762                  0.0                         0.0               ",
                "         0.0                0.0                  10742668.12947898                  0.0                        0.0                0.9250330436838938  195224080.7872981                    0.0                  91114062.52079387          367043559.1575294         ",
                "         0.0                0.0                 257152159.22098163                  0.0                454134143.72315395         0.0                         0.3418518892906355          -0.9157469933273159  361759038.9666181           259915483.56974265        ",
                " 302723801.5969978  324241497.7943367           388913052.60534             455240851.9834251           72526928.47716571 423862026.136655             85216823.34267081                   0.0                        -0.33417336653900076         0.0               ",
                "         0.0               -0.12695240988223366         0.35954932487274816         0.0                        0.0                0.0                  39446356.67545919                  -0.4868987798715789         -0.8888514378782144  521862617.9123003         ",
                "         0.0                0.5097364551327246          0.0                         0.0                        0.0                0.0                 247774294.55316016             8486462.52916672                  0.0                         0.0               ",
                "         0.0        260282858.11108798                  0.0                         0.5711001439935011 175139070.91444498         0.8170175869495133         -0.016173057855442874 142484026.66318423                  0.30619505696225424         0.6499555628132678",
                " 245809221.84413373 293437195.5245196           487325602.50589335                  0.0                        0.0         35815152.87636874                  0.0                  247873651.95925823          467381611.7044559           164738782.4355937         ",
                "         0.0                0.0                 502643276.65288144          492051116.48116195                 0.0        145863225.57465944          141381557.23134556                   0.17741107124305566         0.0                 167677338.75745174        ",
                " 429738045.975798          -0.8380234508196005          0.2784766803331289  376677628.01722115                 0.0                0.0                 124809678.54422382                   0.0                         0.0                  87434406.01665814        ");
        // CHECKSTYLE.ON: LineLength
        TestUtils.testMultiLineToString(expected, illCond);
    }
    
    /** Do an extreme stress-test with a deliberately very ill-conditioned matrix. Notice the huge tolerances. The sizes
     * make sense given the magnitude of the largest elements, but they should be driven down.<br>
     * <br>
     * 
     * Notably, the unitary check on {@code Q} works just as well as in every other test, which is great to see (i.e.,
     * we use {@link #ID_EPS} as the tolerance for both real and imaginary parts). */
    @Test
    void testExtremelyIllConditioned() {
        final double maxRowHermNorm = 1e27;
        final Matrix illCond = testIllConditioned(maxRowHermNorm);
        
        // Do a toString check just for reference.  The poor conditioning is clear.
        // CHECKSTYLE.OFF: LineLength
        final String expected = String.join(System.lineSeparator(),
                "                           0  0.0000000000000000e+00   4.7502161330573910e+26   1.7404005162589988e+26                            0  4.2996816335155996e-01   4.9289422590566426e+26   1.7532274435737762e+26   0.0000000000000000e+00   0.0000000000000000e+00 ",
                "                           0  0.0000000000000000e+00   1.0742668129478980e+25   0.0000000000000000e+00                            0  9.2503304368389380e-01   1.9522408078729810e+26   0.0000000000000000e+00   9.1114062520793870e+25   3.6704355915752940e+26 ",
                "                           0  0.0000000000000000e+00   2.5715215922098160e+26   0.0000000000000000e+00  454134143723154000000000000  0.0000000000000000e+00   3.4185188929063550e-01  -9.1574699332731590e-01   3.6175903896661810e+26   2.5991548356974266e+26 ",
                " 302723801596997800000000000  3.2424149779433670e+26   3.8891305260533995e+26   4.5524085198342506e+26   72526928477165715000000000  4.2386202613665500e+26   8.5216823342670820e+25   0.0000000000000000e+00  -3.3417336653900076e-01   0.0000000000000000e+00 ",
                "                           0 -1.2695240988223366e-01   3.5954932487274816e-01   0.0000000000000000e+00                            0  0.0000000000000000e+00   3.9446356675459190e+25  -4.8689877987157890e-01  -8.8885143787821440e-01   5.2186261791230026e+26 ",
                "                           0  5.0973645513272460e-01   0.0000000000000000e+00   0.0000000000000000e+00                            0  0.0000000000000000e+00   2.4777429455316018e+26   8.4864625291667210e+24   0.0000000000000000e+00   0.0000000000000000e+00 ",
                "                           0  2.6028285811108796e+26   0.0000000000000000e+00   5.7110014399350110e-01  175139070914445000000000000  8.1701758694951330e-01  -1.6173057855442874e-02   1.4248402666318422e+26   3.0619505696225424e-01   6.4995556281326780e-01 ",
                " 245809221844133740000000000  2.9343719552451962e+26   4.8732560250589340e+26   0.0000000000000000e+00                            0  3.5815152876368736e+25   0.0000000000000000e+00   2.4787365195925825e+26   4.6738161170445590e+26   1.6473878243559370e+26 ",
                "                           0  0.0000000000000000e+00   5.0264327665288145e+26   4.9205111648116194e+26                            0  1.4586322557465944e+26   1.4138155723134557e+26   1.7741107124305566e-01   0.0000000000000000e+00   1.6767733875745175e+26 ",
                " 429738045975798000000000000 -8.3802345081960050e-01   2.7847668033312890e-01   3.7667762801722120e+26                            0  0.0000000000000000e+00   1.2480967854422381e+26   0.0000000000000000e+00   0.0000000000000000e+00   8.7434406016658150e+25 ");
        // CHECKSTYLE.ON: LineLength
        TestUtils.testMultiLineToString(expected, illCond);
    }
    
    /** Test solving a square system; i.e., a linear system where the LHS matrix has exactly the same number of columns
     * as it does rows. The solution in this case is exact outside of numerical noise. */
    @Test
    void testSolveVectorSquareSimple() {
        final double[][] arrA = { { 1, 2, 3, 4, 5 },
                                  { 2, 3, 4, 5, 1 },
                                  { 3, 4, 5, 1, 2 },
                                  { 4, 5, 1, 2, 3 },
                                  { 5, 1, 2, 3, 4 } };
        
        final Matrix  A = new Matrix(arrA);
        final NVector b = new NVector(new double[] { 1, 2, 3, 4, 5 });
        
        final NVector x = RealQRDecomposition.of(A).solve(b);
        
        final NVector xExpected = new NVector(new double[] {1, 0, 0, 0, 0});
        
        NumericsTest.assertEBEVectorsEqualAbsolute(xExpected, x, 2 * MACH_EPS);
    }
    
    /** Test solving many random square systems with vector RHSs */
    @Test
    void testSolveVectorSquareRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix  A = MatrixTest.randomMatrix(n, n, 1.0, 8675_309L);
            final NVector b = NVectorTest.randomNVector(n, 1.0, 123L);
            
            final RealQRDecomposition QR = RealQRDecomposition.of(A);
            final NVector             x  = QR.solve(b);
            
            MatrixTest.assertBackwardsStableError(b, A.operate(x), 3);
        }
    }
    
    /** Test solving many random square systems with matrix RHSs */
    @Test
    void testSolveMatrixSquareRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix A = MatrixTest.randomMatrix(n, n, 1.0, 8675_309L);
            final Matrix B = MatrixTest.randomMatrix(n, n, 1.0, 123L);
            
            final RealQRDecomposition QR = RealQRDecomposition.of(A);
            final Matrix              X  = QR.solve(B);
            
            MatrixTest.assertBackwardsStableError(B, A.times(X), 1);
        }
    }
    
    /** Test solving a simple tall system; i.e., a linear system where the LHS matrix has fewer columns than it does
     * rows. <br>
     * <br>
     * 
     * Tall systems are over-determined, so there exists either one solution, or no solution. In the case where there
     * are zero solutions, the answer that's returned should minimize the 2-norm of the error. */
    @Test
    void testSolveVectorTallSimple() {
        final double[][] arrA = { { 1, 2, 3, 4, 5 },
                                  { 2, 3, 4, 5, 1 },
                                  { 3, 4, 5, 1, 2 },
                                  { 4, 5, 1, 2, 3 },
                                  { 5, 1, 2, 3, 4 },
                                  { 1, 5, 3, 2, 4 } };
        
        final Matrix  A = new Matrix(arrA);
        final NVector b = new NVector(new double[] { 1, 2, 3, 4, 5, 6 });
        
        final NVector x = RealQRDecomposition.of(A).solve(b);
        
        // MATLAB answer:
        final NVector xExpected = new NVector(new double[] { 4.791666666666666e-01,
                                                             4.947916666666666e-01,
                                                            -5.208333333333340e-02,
                                                            -2.083333333333330e-01,
                                                             4.166666666666665e-01 });
        
        NumericsTest.assertEBEVectorsEqualAbsolute(xExpected, x, ID_EPS);
        
        assertLSBF(A, x, b);
    }
    
    /** Test solving many random tall systems. The LHS matrices here have {@code n} rows and {@code n/2} columns. */
    @Test
    void testSolveVectorTallRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix  A = MatrixTest.randomMatrix(n, n / 2, 1.0, 8675_309L);
            final NVector b = NVectorTest.randomNVector(n, 1.0, 123L);
            
            final RealQRDecomposition QR = RealQRDecomposition.of(A);
            final NVector             x  = QR.solve(b);
            
            assertLSBF(A, x, b);
        }
    }
    
    /** Test solving many random tall systems with matrix RHSs. The LHS matrices here have {@code n} rows and
     * {@code n/2} columns. */
    @Test
    void testSolveMatrixTallRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix A = MatrixTest.randomMatrix(n, n / 2, 1.0, 8675_309L);
            final Matrix B = MatrixTest.randomMatrix(n, n, 1.0, 123L);
            
            final RealQRDecomposition QR = RealQRDecomposition.of(A);
            final Matrix              X  = QR.solve(B);
            
            assertLSBF(A, X, B);
        }
    }
    
    /** Test the {@link DecompositionSolver} implementations in {@link RealQRDecomposition}
     * 
     * <ul>
     * <li>{@link RealQRDecomposition#solve(NVector)}</li>
     * <li>{@link RealQRDecomposition#solve(Matrix)}</li>
     * <li>{@link RealQRDecomposition#getInverse()}</li>
     * <li>{@link RealQRDecomposition#isNonSingular()}</li>
     * <li>{@link RealQRDecomposition#getRowDimension()}</li>
     * <li>{@link RealQRDecomposition#getColumnDimension()}</li>
     * </ul>
     */
    @Test
    void testDecompositionSolver() {
        final int n = 10;
        
        final Matrix              sqA  = MatrixTest.randomMatrix(n, n, 1.0, 8675_309L);
        final RealQRDecomposition sqQR = RealQRDecomposition.of(sqA);
        
        assertTrue(sqQR.isNonSingular());
        
        assertEquals(10, sqQR.getRowDimension());
        assertEquals(10, sqQR.getColumnDimension());
        
        final NVector b = NVectorTest.randomNVector(n, 1.0, 123L);
        final NVector x = sqQR.solve((RealVector) b); // cast to hit the correct overrides
        
        final Matrix B = MatrixTest.randomMatrix(n, n, 1.0, 123L);
        final Matrix X = sqQR.solve((RealMatrix) B); // cast to hit the correct overrides
        
        MatrixTest.assertBackwardsStableError(b, sqA.operate(x), 3);
        MatrixTest.assertBackwardsStableError(B, sqA.times(X),   3);
        
        final Matrix inv = sqQR.getInverse();
        MatrixTest.assertBackwardsStableError(Matrix.identity(10), sqA.times(inv), 1);
        
        final Matrix              tallA  = MatrixTest.randomMatrix(n, n / 2, 1.0, 8675_309L);
        final RealQRDecomposition tallQR = RealQRDecomposition.of(tallA);
        
        assertThrows(MathIllegalArgumentException.class, () -> tallQR.getInverse()); // original matrix is non-square
        
        final NVector x2 = tallQR.solve((RealVector) b); // cast to hit the correct overrides
        final Matrix  X2 = tallQR.solve((RealMatrix) B); // cast to hit the correct overrides
        
        assertLSBF(tallA, x2, b);
        assertLSBF(tallA, X2, B);
        
        assertEquals(10, tallQR.getRowDimension());
        assertEquals(5,  tallQR.getColumnDimension());
        
        assertTrue(tallQR.isNonSingular());
        
        assertFalse(RealQRDecomposition.of(Matrix.zero(n, n)).isNonSingular());
    }
    
    /** The same as {@link #assertLSBF(Matrix, NVector, NVector)}, except where the RHS is a matrix. The test simply
     * calls {@link #assertLSBF(Matrix, NVector, NVector)} for each of the columns of {@code X}
     * 
     * @param A the LHS of the equation
     * @param X the candidate solution
     * @param B the RHS of the equation */
    static void assertLSBF(final Matrix A, final Matrix X, final Matrix B) {
        for (int j = 0; j < A.getColumnDimension(); j++) {
            assertLSBF(A, X.getColumnVector(j), B.getColumnVector(j));
        }
    }
    
    /** Assert that {@code x} is a least-squares best-fit solution to the problem {@code Ax = b}. We do this by
     * asserting that the relative residual - as described in {@link LinearSolverTest} - of the solution vector
     * {@code x} is (likely) minimal in a least-squares sense.<br>
     * <br>
     * 
     * To show that {@code x} (likely) results in the minimal error, for each cardinal dimension in the space of
     * {@code x}, we copy and perturb {@code x} in the given direction and show that the RR associated with the
     * perturbed vector is larger than the RR of the original. We use a tolerance of {@link #MACH_EPS}.
     * 
     * @param A the LHS of the equation
     * @param x the candidate solution
     * @param b the RHS of the equation */
    static void assertLSBF(final Matrix A, final NVector x, final NVector b) {
        final RandomDataGenerator rdg = new RandomDataGenerator(A.hashCode());
        
        final double yNorm       = b.getNorm();
        final double rrLeastSqrs = -MACH_EPS + b.minus(A.operate(x)).getNorm() / yNorm;
        
        for (int row = 0; row < x.getDimension(); row++) { // row -> the row of the x vector to perturb
            final NVector perturbedX = x.copy();
            
            final double entry          = perturbedX.getEntry(row);
            final double perturbation   = 1e-6 * 2 * (rdg.nextDouble() - 1.0); // 1e-6 * [-1, 1]
            final double perturbedEntry = entry * (1 + perturbation);
            
            perturbedX.setEntry(row, perturbedEntry);
            
            final double rrPerturbed = b.minus(A.operate(perturbedX)).getNorm() / yNorm;
            assertLessThanOrEqualTo(rrLeastSqrs, rrPerturbed, "rrLeastSqrs - u for column " + row);
        }
    }
    
    /** When used with a large enough {@code maxRowHermNorm} value, this test creates and tests an ill-conditioned
     * {@link Matrix}
     * 
     * @param maxRowHermNorm the max {@link RealVector#getNorm()} of any of the rows (this is just Euclidean
     *        distance if all real).
     * @return the ill conditioned matrix that was created */
    private Matrix testIllConditioned(final double maxRowHermNorm) {
        final long seed = 8675309L; // create a new 10x10 random matrix whose rows have max Hermitian norm 1e9
        final Matrix originalA = MatrixTest.randomMatrix(10, 10, maxRowHermNorm, seed);
        
        final int m = originalA.getRowDimension();
        final int n = originalA.getColumnDimension();
        
        /* Randomly change 25% of entries to zero, change an additional 25%% to numbers in [-1, 1] so that 50% total of
         * all entries are either 0 or very small relative to the original (unchanged) entries. */
        final double[][] newAData = originalA.getData(); // getData clones, so this is safe
        final RandomDataGenerator r = new RandomDataGenerator(seed);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                final double trigger = r.nextDouble();
                if (trigger < 0.25) {
                    newAData[i][j] = 0;
                }
                else if (trigger < 0.5) {
                    newAData[i][j] = 2 * (r.nextDouble() - 0.5); // a is [-1, 1];
                }
            }
        }
        
        final Matrix newA = new Matrix(newAData);
        testMatrix(newA, 2);
        return newA;
    }
    
    /** Create and test a random {@code m x n} {@link Matrix} with {@link RealQRDecomposition}. We use
     * {@link MatrixTest#randomMatrix(int, int, double, long)} to create the test matrices.
     * 
     * @param m the number of rows
     * @param n the number of columns
     * @param maxRowHermNorm the max {@link RealVector#getNorm()} of any of the rows (this is just Euclidean
     *        distance if all real).
     * @param seed the random seed
     *        
     * @see #testMatrix(Matrix, int) */
    void createAndTestRandom(final int m,
                             final int n,
                             final double maxRowHermNorm,
                             final long seed) {
        final Matrix A = MatrixTest.randomMatrix(m, n, maxRowHermNorm, seed);
        testMatrix(A, 2);
    }
    
    /** Test the results of applying {@link RealQRDecomposition} to the given {@code m x n} {@link Matrix}
     * 
     * @param A the test {@link Matrix}
     * @param toleranceScale we scale the allowed tolerance by this much. Ideally, this is {@code 1}. If not {@code 1},
     *        it must be small. */
    private static void testMatrix(final Matrix A, final int toleranceScale) {
        final RealQRDecomposition QR         = RealQRDecomposition.of(A, false, false);
        final Matrix              Q          = QR.getQ();
        final Matrix              R          = QR.getR();
        final Matrix              shouldBeA1 = Q.times(R);
        
        final double tolerance = toleranceScale * A.getFrobeniusNorm() * Numerics.MACHINE_EPSILON;
        
        assertUnitary(Q, tolerance);
        assertIsUpperTriangular(R, tolerance);
        MatrixTest.assertMatrixEquals(A, shouldBeA1, tolerance); // check that A == QR
        
        final Matrix reducedQ   = QR.getReducedQ();
        final Matrix reducedR   = QR.getReducedR();
        final Matrix shouldBeA2 = reducedQ.times(reducedR);
        MatrixTest.assertMatrixEquals(A, shouldBeA2, tolerance); // check that A == reducedQ * reducedR
        
        final RealQRDecomposition QRwCleanup = RealQRDecomposition.of(A, true, true);
        final Matrix          wCleanupQ  = QRwCleanup.getQ();
        final Matrix          wCleanupR  = QRwCleanup.getR();
        final Matrix          shouldBeA3 = wCleanupQ.times(wCleanupR);
        MatrixTest.assertMatrixEquals(A, shouldBeA3, tolerance); // check that A == wCleanupQ * wCleanupR
        
        // checking manually, it appears clean-up improves the accuracy in most cases (9 of the 15 I checked)
    }
    
    /** Assert that the given {@link Matrix} is upper-triangular
     * 
     * @param R the matrix to check
     * @param absReTol the absolute tolerance when comparing entries */
    public static void assertIsUpperTriangular(final Matrix R, final double absReTol) {
        final int numRows = R.getRowDimension();
        final int numCols = R.getColumnDimension();
        
        double worstReError = 0.0;
        for (int j = 0; j < numCols; j++) {
            for (int i = j + 1; i < numRows; i++) {
                final double ij = R.getEntry(i, j);
                
                worstReError = FastMath.max(worstReError, FastMath.abs(ij));
                
                final boolean ijIsZero = Precision.equals(ij, 0.0, absReTol);
                final int     finalI   = i;
                final int     finalJ   = j;
                assertTrue(ijIsZero,
                           () -> "Element [%d, %d] -> %s != (0, 0)%nR = %n%s".formatted(finalI, finalJ, ij, R));
            }
        }
        
        // these are covered by the assertion in loop above, but I want to keep the worst case values around for testing
        final double worstErrorsZ = worstReError;
        assertTrue(worstReError <= absReTol, () -> "Errors: %s".formatted(worstErrorsZ));
    }
    
    /** Assert that the given {@link Matrix} is unitary by checking check U.U^H == I and U^H.U == I
     * 
     * @param M the matrix to check  */
    public static void assertUnitary(final Matrix M) {
        assertUnitary(M, ID_EPS);
    }
    
    /** Assert that the given {@link Matrix} is unitary by checking check U.U^H == I and U^H.U == I
     * 
     * @param M the matrix to check 
     * @param absReTol the absolute tolerance when comparing entries */
    public static void assertUnitary(final Matrix M, final double absReTol) {
        final Matrix I   = Matrix.identity(M.getRowDimension());
        final Matrix QQH = M.times(M.transpose());
        final Matrix QHQ = M.transpose().times(M);
        MatrixTest.assertMatrixEquals(I, QQH, absReTol);
        MatrixTest.assertMatrixEquals(I, QHQ, absReTol);
    }
    
    /** A mini program that uses {@link MatrixVisualizer} to display the results of a {@link RealQRDecomposition}
     * 
     * @param args ignored
     * @throws InterruptedException if {@link Thread#sleep(long)} is interrupted */
    @SuppressWarnings("unused")
    public static void main(final String[] args) throws InterruptedException {
        final Matrix A = new Matrix(new double[][] { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                     { 1, 2, 2, 2, 2, 2, 2, 2, 2 },
                                                     { 1, 2, 3, 3, 3, 3, 3, 3, 3 },
                                                     { 1, 2, 3, 4, 4, 4, 4, 4, 4 },
                                                     { 1, 2, 3, 4, 5, 5, 5, 5, 5 },
                                                     { 1, 2, 3, 4, 5, 6, 6, 6, 6 },
                                                     { 1, 2, 3, 4, 5, 6, 7, 7, 7 },
                                                     { 1, 2, 3, 4, 5, 6, 7, 8, 8 },
                                                     { 1, 2, 3, 4, 5, 6, 7, 8, 9 } });
        
        final RealQRDecomposition QR = RealQRDecomposition.of(A);
        final Matrix              R  = QR.getR();
        final Matrix              Q  = QR.getQ();
        
        new MatrixVisualizer<>(A);
        new MatrixVisualizer<>(Q);
        new MatrixVisualizer<>(Q.times(R));
    }
}
