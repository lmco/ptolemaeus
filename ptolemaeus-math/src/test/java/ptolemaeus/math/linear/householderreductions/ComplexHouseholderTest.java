/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.SinCos;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;

/** Test {@link ComplexHouseholder}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class ComplexHouseholderTest {
    
    /** a non-trivial {@link ComplexVector} */
    private static final ComplexVector X = new ComplexVector(new Complex(1, 12),
                                                             new Complex(2, 11),
                                                             new Complex(3, 10),
                                                             new Complex(4,  9),
                                                             new Complex(5,  8),
                                                             new Complex(6,  7));
    
    /** the same thing as {@link #X}, but in the {@code double[][]} representation */
    private static final double[][] X_ARR = new double[][] {
        {  1,  2,  3, 4, 5, 6 },
        { 12, 11, 10, 9, 8, 7 }
    };
    
    /** the complex zero vector in the {@code double[][]} representation */
    private static final double[][] ZERO_ARR = new double[][] {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0 }
    };
    
    /** Test {@link ComplexHouseholder#preMultiplySubMatrixByConjugate} by pre-multiplying the sub-matrix
     * 
     * <pre>
     * 4  0  0  0  0
     * 0  5  0  0  0
     * 0  0  6  0  0
     * 0  0  0  7  0
     * 0  0  0  0  8
     * </pre>
     * 
     * by the complex conjugate of (1 + i, 1 + i, 1 + i, 1 + i, 1 + i) to get
     *                             (4 - 4i, 5 - 5i, 6 - 6i, 7 - 7i, 8 - 8i) */
    @Test
    void testPreMultiplySubMatrixSimple() {
        final double[][][] A = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                  { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                                                                  { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                                                                  { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                                                                  { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                                                                  { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                                                                  { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                                                                  { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                                                                  { 0, 0, 0, 0, 0, 0, 0, 0, 9 } }).getDataRef();
        
        final double[] wRe = { 1, 1, 1, 1, 1 };
        final double[] wIm = { 1, 1, 1, 1, 1 };
        
        final double[][] wA = ComplexHouseholder.preMultiplySubMatrixByConjugate(wRe, wIm, A[0], A[1], 3, 3, 7);
        assertArrayEquality(new double[] {  4,  5,  6,  7,  8 }, wA[0], 0.0);
        assertArrayEquality(new double[] { -4, -5, -6, -7, -8 }, wA[1], 0.0);
    }
    
    /** Test {@link ComplexHouseholder#preMultiplySubMatrixByConjugate} with random inputs and testing against the value
     * computed by actually constructing the sub-matrix. */
    @Test
    void testPreMultiplySubMatrixRandom() {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(10, 10, 123L);
        final ComplexVector w = ComplexVectorTest.randomComplexVector(5, 1.0, 456L);
        
        final double[][][] AArr = A.getDataRef();
        final double[][]   wArr = w.getCopyOfReImArrays();
        
        final double[][] expected = A.getSubMatrix(3, 3 + w.getDimension() - 1, 3, 7)
                                     .preMultiply(w.conjugate())
                                     .getCopyOfReImArrays();
        final double[][] wAArr    = ComplexHouseholder.preMultiplySubMatrixByConjugate(wArr[0], wArr[1],
                                                                                       AArr[0], AArr[1],
                                                                                       3, 3, 7);
        
        assertArrayEquality(expected[0], wAArr[0], 0.0);
        assertArrayEquality(expected[1], wAArr[1], 0.0);
    }
    
    /** Test {@link ComplexHouseholder#postMultiplySubMatrix} by post-multiplying the sub-matrix
     * 
     * <pre>
     * 4  0  0  0  0
     * 0  5  0  0  0
     * 0  0  6  0  0
     * 0  0  0  7  0
     * 0  0  0  0  8
     * </pre>
     * 
     * by (1 + i, 1 + i, 1 + i, 1 + i, 1 + i) to get (4 + 4i, 5 + 5i, 6 + 6i, 7 + 7i, 8 + 8i) */
    @Test
    void testPostMultiplySubMatrixSimple() {
        final double[][][] A = new ComplexMatrix(new double[][] { { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                  { 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                                                                  { 0, 0, 3, 0, 0, 0, 0, 0, 0 },
                                                                  { 0, 0, 0, 4, 0, 0, 0, 0, 0 },
                                                                  { 0, 0, 0, 0, 5, 0, 0, 0, 0 },
                                                                  { 0, 0, 0, 0, 0, 6, 0, 0, 0 },
                                                                  { 0, 0, 0, 0, 0, 0, 7, 0, 0 },
                                                                  { 0, 0, 0, 0, 0, 0, 0, 8, 0 },
                                                                  { 0, 0, 0, 0, 0, 0, 0, 0, 9 } }).getDataRef();
        
        final double[] wRe = { 1, 1, 1, 1, 1 };
        final double[] wIm = { 1, 1, 1, 1, 1 };
        
        final double[][] Aw = ComplexHouseholder.postMultiplySubMatrix(A[0], A[1], wRe, wIm, 3, 7, 3);
        assertArrayEquality(new double[] { 4, 5, 6, 7, 8 }, Aw[0], 0.0);
        assertArrayEquality(new double[] { 4, 5, 6, 7, 8 }, Aw[1], 0.0);
    }
    
    /** Test {@link ComplexHouseholder#postMultiplySubMatrix} with random inputs and testing against the value computed
     * by actually constructing the sub-matrix. */
    @Test
    void testPostMultiplySubMatrixRandom() {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(10, 10, 123L);
        final ComplexVector w = ComplexVectorTest.randomComplexVector(5, 1.0, 456L);
        
        final double[][][] AArr = A.getDataRef();
        final double[][]   wArr = w.getDataRef();
        
        final double[][] expected = A.getSubMatrix(3, 7, 3, 3 + w.getDimension() - 1)
                                     .operate(w)
                                     .getDataRef();
        final double[][] AwArr    = ComplexHouseholder.postMultiplySubMatrix(AArr[0], AArr[1], wArr[0], wArr[1],
                                                                            3, 7, 3);
        
        assertArrayEquality(expected[0], AwArr[0], 0.0);
        assertArrayEquality(expected[1], AwArr[1], 0.0);
    }
    
    /** Test some noop instance edge-cases */
    @Test
    void testNOOPs() {
        // one way to get the noop instance is to use the zero-vector:
        final ComplexHouseholder ch = ComplexHouseholder.of(new double[10], new double[10]);
        assertEquals(ComplexMatrix.identity(10), ch.getTransform());
        
        final ComplexMatrix I = ComplexMatrix.identity(10);
        
        ch.preApplyInPlace(I, 0, 0);
        ComplexMatrixTest.assertAbsEquals(ComplexMatrix.identity(10), I, 0, 0); // unchanged
        
        ch.postApplyInPlace(I, 0, 0);
        ComplexMatrixTest.assertAbsEquals(ComplexMatrix.identity(10), I, 0, 0); // unchanged
    }
    
    /** Test {@link ComplexHouseholder#preApplyInPlace} by showing that it's equivalent to computing the actual
     * Householder matrix and pre-multiplying by it. We do this by just operating on the identity matrix, and the
     * correctness of the transformation matrix is shown in other tests here. */
    @Test
    void testPreApplyInPlace() {
        final ComplexVector x     = ComplexVectorTest.randomComplexVector(10, 1.0, 123L);
        final double[][]    xReIm = x.getDataRef();
        
        final ComplexHouseholder ch1 = ComplexHouseholder.of(xReIm[0], xReIm[1]);
        
        final ComplexMatrix shouldBeTransform = ComplexMatrix.identity(10);
        ch1.preApplyInPlace(shouldBeTransform, 0, 0);
        
        final ComplexMatrix transform = ch1.getTransform();
        assertEquals(transform, shouldBeTransform); // zero tolerance!
        
        final ComplexHouseholder ch2 = ComplexHouseholder.of(x);
        
        assertEquals(ch1.getTransform(), ch2.getTransform());
    }
    
    /** Test {@link ComplexHouseholder#postApplyInPlace} by showing that it's equivalent to computing the actual
     * Householder matrix and post-multiplying by it. We do this by just operating on the identity matrix, and the
     * correctness of the transformation matrix is shown in other tests here. */
    @Test
    void testPostApplyInPlace() {
        final ComplexVector x     = ComplexVectorTest.randomComplexVector(10, 1.0, 123L);
        final double[][]    xReIm = x.getDataRef();
        
        final ComplexHouseholder ch = ComplexHouseholder.of(xReIm[0], xReIm[1]);
        
        final ComplexMatrix shouldBeTransform = ComplexMatrix.identity(10);
        ch.postApplyInPlace(shouldBeTransform, 0, 0);
        
        final ComplexMatrix transform = ch.getTransform();
        assertEquals(transform, shouldBeTransform); // zero tolerance!
    }
    
    /** Test an edge case of {@link ComplexHouseholder#computeTransform(double[], double[])} where the input vector has
     * norm so small that squaring it results in zero. */
    @Test
    void testNormSqZero() {
        final double ltSqrtSmallest = FastMath.sqrt(Double.MIN_VALUE) / 2;
        final double[] re = new double[] { ltSqrtSmallest, 0, 0, 0, 0 };
        final double[] im = new double[] {              0, 0, 0, 0, 0 };
        
        assertEquals(ComplexMatrix.identity(5), ComplexHouseholder.computeTransform(re, im));
    }
    
    /** Test the {@link Complex}-valued case of
     * {@link ComplexHouseholder#iMinusTwiceSelfOuterProduct(double[], double[])} */
    @Test
    void testIMinusTwiceSelfOuterProductComplex() {
        final ComplexMatrix xxH      = X.outerProduct(X.conjugate()).scalarMultiply(Complex.valueOf(2.0));
        final ComplexMatrix expected = ComplexMatrix.identity(6).minus(xxH);
        final ComplexMatrix actual   = ComplexHouseholder.iMinusTwiceSelfOuterProduct(X_ARR[0], X_ARR[1]);
        
        assertEquals(expected, actual);
    }
    
    /** Test {@link ComplexHouseholder#hermitianNorm(double[], double[])} */
    @Test
    void testHermitianNorm() {
        assertEquals(X.hermitianNorm(), ComplexHouseholder.hermitianNorm(X_ARR[0], X_ARR[1]));
    }
    
    /** Test {@link ComplexHouseholder#isZero(double[], double[])} */
    @Test
    void testIsZeroComplex() {
        assertFalse(ComplexHouseholder.isZero(X_ARR[0], X_ARR[1]));
        assertTrue(ComplexHouseholder.isZero(ZERO_ARR[0], ZERO_ARR[1]));
        
        assertFalse(ComplexHouseholder.isZero(ZERO_ARR[0], X_ARR[1]));
    }
    
    /** Test some special cases from {@link ComplexHouseholder#computeK(double[], double[])} */
    @Test
    void testEdgeCases() {
        final double[][] v1 = new ComplexVector(1, 2, 3, 4, 5).getDataRef();
        final double[][] v2 = new ComplexVector(-1, 2, 3, 4, 5).getDataRef();
        final double[][] v3 = new ComplexVector(1, 2, 3, 4, 5).times(Complex.valueOf(0, 1)).getDataRef();
        final double[][] v4 = new ComplexVector(-1, 2, 3, 4, 5).times(Complex.valueOf(0, 1)).getDataRef();
        
        final double magnitude = FastMath.sqrt(1 + 4 + 9 + 16 + 25);
        
        assertEquals(Complex.valueOf(-magnitude, 0.0), ComplexHouseholder.computeK(v1[0], v1[1]));
        assertEquals(Complex.valueOf(magnitude, 0.0),  ComplexHouseholder.computeK(v2[0], v2[1]));
        assertEquals(Complex.valueOf(0.0, -magnitude), ComplexHouseholder.computeK(v3[0], v3[1]));
        assertEquals(Complex.valueOf(0.0, magnitude),  ComplexHouseholder.computeK(v4[0], v4[1]));
    }
    
    /** Test {@link ComplexHouseholder#computeK(double[], double[])} against {@link #computeK(ComplexVector)} */
    @Test
    void testComputeKDoubleArr() {
        final Complex expected = computeK(X);
        final Complex actual = ComplexHouseholder.computeK(X_ARR[0], X_ARR[1]);
        
        assertEquals(expected, actual);
    }
    
    /** The {@link Complex k} to use in {@link ComplexHouseholder#computeTransform(double[], double[])} but computed
     * without using specialized {@code double[][]} methods.
     * 
     * @param x the vector for which we want to compute a ComplexHouseholder transform
     * @return the {@code k} value */
    static Complex computeK(final ComplexVector x) {
        final double  alpha       = x.getEntry(0).getArgument();
        final SinCos  sinCosArgX0 = FastMath.sinCos(alpha);
        final Complex eIArgX0     = Complex.valueOf(sinCosArgX0.cos(), sinCosArgX0.sin()); // e^(i arg(X0))
        final double  sigma       = x.hermitianNorm(); // sqrt(x^H * x)
        return eIArgX0.multiply(-sigma); // e^(i arg(X0)) * (-sqrt(x^H.x))
    }
    
    /** Test real and complex vectors of length two */
    @Test
    void testDim2() {
        final ComplexVector v1 = new ComplexVector(new double[] { 1, 2 });
        testComplex(v1, 1);
        
        final ComplexVector v2 = new ComplexVector(Complex.valueOf(1, 2), Complex.valueOf(3, 4));
        testComplex(v2, 1);
    }
    
    /** Test {@link ComplexHouseholder#computeTransform(ComplexVector, int)} with a zero-vector to hit a special-case */
    @Test
    void testAllZeroesComplex() {
        final ComplexVector v = new ComplexVector(new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 });
        testComplex(v, 1);
    }
    
    /** Test {@link ComplexHouseholder#computeTransform(ComplexVector, int)} some special-cases to ensure we properly
     * handle any zero vectors that pop up. */
    @Test
    void testManyZeroesComplex() {
        final ComplexVector v1 = new ComplexVector(new double[] { 0.0, 0.0, 0.0, 1.0, 0.0 });
        testComplex(v1, 2);
        
        final ComplexVector v2 = new ComplexVector(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 });
        testComplex(v2, 1);
    }
    
    /** Test {@link ComplexHouseholder#computeTransform(ComplexVector, int)} with a ComplexHouseholder index of zero.
     * This is similar to {@link #testRandomComplexVectors()}, but is more specific. */
    @Test
    void testComplexVectorZeroIndex() {
        final ComplexMatrix q = ComplexHouseholder.computeTransform(X, 0); // 6 x 6
        assertTrue(ComplexMatrix.isHermitian(q, Complex.ZERO));  // Hermitian with zero tolerance!
        
        final ComplexMatrix qSq = q.power(2);
        assertTrue(qSq.absEquivalentTo(ComplexMatrix.identity(6), 1e-15, 1e-15));
        
        final double eps = X.hermitianNorm() * Numerics.MACHINE_EPSILON;
        
        final Complex expected1stElt = computeK(X).negate();
        
        final ComplexVector qx = q.operate(X);
        for (int i = 0; i < 6; i++) {
            final int finalI = i;
            final Supplier<String> msgSupp = () -> "Failed on i == %d".formatted(finalI);
            
            final Complex qxi = qx.getEntry(i);
            if (i == 0) {
                assertEquals(X.hermitianNorm(), qxi.norm(), eps);
                assertTrue(Numerics.equalsRelEps(expected1stElt.negate(), qxi, Complex.valueOf(eps, eps)));
            }
            else {
                assertTrue(Complex.equals(Complex.ZERO, qxi, eps), msgSupp);
            }
        }
        
        testComplex(X, 1); // run the other method, too, just to be sure
    }
    
    /** Test {@link ComplexHouseholder#computeTransform(ComplexVector, int)} with a non-zero ComplexHouseholder index.
     * Notice the analogues to {@link #testComplexVectorZeroIndex()}. */
    @Test
    void testComplexVectorNonZeroIndex() {
        final int hessenbergOffset = 3;
        final ComplexMatrix q = ComplexHouseholder.computeTransform(X, hessenbergOffset); // 6 x 6
        assertTrue(ComplexMatrix.isHermitian(q, Complex.ZERO));  // Hermitian with zero tolerance!
        
        final ComplexMatrix qSq = q.power(2);
        assertTrue(qSq.absEquivalentTo(ComplexMatrix.identity(6), 1e-15, 1e-15));
        
        // notice the 3x3 structure in the bottom right
        // CHECKSTYLE.OFF: LineLength
        final String expected = String.join(System.lineSeparator(),
                " 1 + 0 i,  0 + 0 i,  0 + 0 i,   0.0                + 0.0                 i,   0.0                 +  0.0                 i,   0.0                 +  0.0                 i",
                " 0 + 0 i,  1 + 0 i,  0 + 0 i,   0.0                + 0.0                 i,   0.0                 +  0.0                 i,   0.0                 +  0.0                 i",
                " 0 + 0 i,  0 + 0 i,  1 + 0 i,   0.0                + 0.0                 i,   0.0                 +  0.0                 i,   0.0                 +  0.0                 i",
                " 0 + 0 i,  0 + 0 i,  0 + 0 i,  -0.5982755045426758 + 0.0                 i,  -0.5674365610095482  + -0.0801812531861318  i,  -0.5365976174764207  + -0.16036250637226362 i",
                " 0 + 0 i,  0 + 0 i,  0 + 0 i,  -0.5674365610095482 + 0.0801812531861318  i,   0.7945202293350468  +  0.0                 i,  -0.19855348626051653 + -0.03001389908589204 i",
                " 0 + 0 i,  0 + 0 i,  0 + 0 i,  -0.5365976174764207 + 0.16036250637226362 i,  -0.19855348626051653 +  0.03001389908589204 i,   0.803755275207629   +  0.0                 i");
        // CHECKSTYLE.ON: LineLength
        TestUtils.testMultiLineToString(expected, q);
        
        final double eps = X.hermitianNorm() * Numerics.MACHINE_EPSILON;
        final ComplexVector qx = q.operate(X);
        for (int i = 0; i < 6; i++) {
            final int finalI = i;
            final Supplier<String> msgSupp = () -> "Failed on i == %d".formatted(finalI);
            
            final Complex qxi = qx.getEntry(i);
            if (i < hessenbergOffset) {
                assertTrue(Numerics.equalsRelEps(X.getEntry(i), qxi, Complex.valueOf(eps, eps)),
                           msgSupp);
            }
            else if (i == hessenbergOffset) {
                final Complex expected1stElt = computeK(X.getSubVector(hessenbergOffset, 3));
                assertTrue(Numerics.equalsRelEps(expected1stElt, qxi, Complex.valueOf(eps, eps)),
                           msgSupp);
            }
            else {
                assertTrue(Complex.equals(Complex.ZERO, qxi, eps), msgSupp);
            }
        }
        
        testComplex(X, 1); // run the other method, too, just to be sure
    }
    
    /** Test {@link ComplexHouseholder#computeTransform(ComplexVector, int)} with a few random {@link ComplexVector}s */
    @Test
    void testRandomComplexVectors() {
        testComplex(ComplexVectorTest.randomComplexVector(10,   10.0, 123L), 2);
        testComplex(ComplexVectorTest.randomComplexVector(10,  100.0, 456L), 1);
        testComplex(ComplexVectorTest.randomComplexVector(10, 1000.0, 789L), 2);
    }
    
    /** Test {@link ComplexHouseholder#computeTransform(ComplexVector, int)} by showing that the {@link ComplexMatrix}
     * result behaves as expected and has the expected properties.<br>
     * <br>
     * 
     * Notice that we can use zero tolerance when testing whether
     * {@link ComplexMatrix#isHermitian(ComplexMatrix, Complex) q.isHermitian()}, regardless of input.
     * 
     * @param x the {@link ComplexVector} to pass into {@link ComplexHouseholder#computeTransform(ComplexVector, int)}
     * @param toleranceScale we scale the allowed tolerance by this much. Ideally, this is {@code 1}. If not {@code 1},
     *        it must be small. */
    private static void testComplex(final ComplexVector x, final int toleranceScale) {
        final ComplexMatrix q = ComplexHouseholder.computeTransform(x, 0); // x ix n x 1 -> q is n x n
        assertTrue(ComplexMatrix.isHermitian(q, Complex.ZERO));  // Hermitian with zero tolerance!
        
        final int complexDim = x.getDimension();
        
        final ComplexMatrix identity = ComplexMatrix.identity(complexDim);
        
        final double absTol = toleranceScale * x.hermitianNorm() * Numerics.MACHINE_EPSILON;
        final ComplexMatrix qSq = q.power(2);
        assertTrue(qSq.absEquivalentTo(identity, absTol, absTol),
                   () -> "Error:%n%s".formatted(qSq.minus(identity)));
        
        final ComplexVector qx = q.operate(x);
        for (int i = 0; i < complexDim; i++) {
            final int finalI = i;
            final Complex qxi = qx.getEntry(i);
            if (i == 0) {
                assertEquals(x.hermitianNorm(), qxi.norm(), absTol,
                             () -> "Error: %20e".formatted(FastMath.abs(x.hermitianNorm() - qxi.norm())));
            }
            else {
                final Supplier<String> msgSupp = () -> """
                        Failed for i = %d
                        Expected  : %s
                        Actual    : %s
                        Error norm: %20e
                        """.formatted(finalI, Complex.ZERO, qxi, qxi.norm());
                
                assertTrue(Complex.equals(Complex.ZERO, qxi, absTol), msgSupp);
            }
        }
    }
}
