/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.linear.complex.ComplexMatrix;

/** Test {@link QRDecomposition}. Testing of non-trivial {@link QRDecomposition#getReducedQ()} and
 * {@link QRDecomposition#getReducedR()} is implementation dependent, so the tests are in
 * {@link ComplexQRDecompositionTest} and {@link RealQRDecompositionTest}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see ComplexQRDecompositionTest
 * @see RealQRDecompositionTest */
public class QRDecompositionTest {
    
    /** Test the basics of {@link QRDecomposition} by going through {@link ComplexQRDecomposition} */
    @Test
    void testBasics() {
        final ComplexMatrix Q = new ComplexMatrix(new double[][] {
            { 6, 0, 0, 0, 0 },
            { 0, 6, 0, 0, 0 },
            { 0, 0, 6, 0, 0 },
            { 0, 0, 0, 6, 0 },
            { 0, 0, 0, 0, 6 }
        });
        final ComplexMatrix R = ComplexMatrix.identity(5).scalarMultiply(Complex.valueOf(5.0));
        
        final ComplexQRDecomposition QR = new ComplexQRDecomposition(Q, R);
        
        assertSame(Q, QR.getQ());
        assertSame(R, QR.getR());
        
        final String expected = String.join(System.lineSeparator(),
                                            "Q:",
                                            " 6 0 0 0 0",
                                            " 0 6 0 0 0",
                                            " 0 0 6 0 0",
                                            " 0 0 0 6 0",
                                            " 0 0 0 0 6",
                                            "",
                                            "R:",
                                            " 5 0 0 0 0",
                                            " 0 5 0 0 0",
                                            " 0 0 5 0 0",
                                            " 0 0 0 5 0",
                                            " 0 0 0 0 5");
        TestUtils.testMultiLineToString(expected, QR);
        
        // in this case, the original A would a square matrix, so the reduced Q and R are unchanged
        assertEquals(Q, QR.getReducedQ());
        assertEquals(R, QR.getReducedR());
    }
}
