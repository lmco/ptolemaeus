/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.RealVector;

import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.linear.real.NVectorTest;

/** Test {@link Householder}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * 
 * @see RealHouseholderTest
 * @see ComplexHouseholderTest */
class HouseholderTest {
    
    /** Test {@link RealHouseholder#computeTransform} using a {@link RealVector} vs
     * {@link ComplexHouseholder#computeTransform} using a {@link ComplexVector} whose imaginary parts are all zero */
    @Test
    void testRealSepcificVsZeroImagComplex() {
        final NVector       x = NVectorTest.randomNVector(10, 1.0, 123L);
        
        final Matrix h1 = Householder.of(x.getDataRef()).getTransform();
        final Matrix h2 = Householder.of(x.getDataRef(), new double[10]).getTransform().asReal(0.0).orElseThrow();
        
        assertEquals(h1, h2);
    }
}
