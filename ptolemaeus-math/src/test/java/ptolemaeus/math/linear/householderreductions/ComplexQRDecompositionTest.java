/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.householderreductions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertLessThanOrEqualTo;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.random.RandomDataGenerator;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.LinearSolverTest;
import ptolemaeus.math.linear.MatrixVisualizer;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;
import ptolemaeus.math.linear.real.Matrix;

/** Tests for {@link ComplexQRDecomposition}<br>
 * <br>
 * 
 * {@link #testIllConditioned()} suggests that we do a pretty decent job handling ill-conditioned matrices, but
 * increasing the scale disparity between entries by enough (e.g., 1e18) still produces matrices that are pretty
 * problematic (see {@link #testExtremelyIllConditioned()}). The tests work, but the required epsilons are huge when
 * checking {@code A == QR} and checking whether {@code R} is upper-triangular. Interestingly, the unitary check on
 * {@code Q} works, regardless!
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class ComplexQRDecompositionTest {
    
    /** a local copy of {@link Numerics#MACHINE_EPSILON} just so I don't have to keep writing it out */
    private static final double MACH_EPS  = Numerics.MACHINE_EPSILON;
    
    /** When checking whether {@link ComplexQRDecomposition#getQ()} is unitary, we use this epsilon.<br>
     * <br>
     * 
     * Notable that this one value works - seemingly - regardless of scale (notice {@link #testLargeScale()}, which has
     * row vectors with {@link ComplexVector#hermitianNorm()} up to 1e9), and {@link #testIllConditioned()} which tests
     * a terribly conditioned matrix, */
    public static final double ID_EPS = 5 * MACH_EPS;
    
    /** the column vector dimensions we want to test */
    private static final int[] DIMENSIONS = new int[] { 5, 10, 25, 50, 75 };
    
    /** Test {@link ComplexQRDecomposition#cleanup(ComplexMatrix)} and
     *       {@link ComplexQRDecomposition#adjustSingularities(ComplexMatrix)} */
    @Test
    void testCleanupAndAdjustSingularities() {
        final ComplexMatrix A = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                   { 1, 0, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 0, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 },
                                                                   { 1, 1, 1, 1, 1 } },
                                                  new double[][] { { 2, 2, 2, 2, 2 },
                                                                   { 2, 0, 2, 2, 2 },
                                                                   { 2, 2, 2, 2, 2 },
                                                                   { 2, 2, 2, 0, 2 },
                                                                   { 2, 2, 2, 2, 2 },
                                                                   { 2, 2, 2, 2, 2 },
                                                                   { 2, 2, 2, 2, 2 },
                                                                   { 2, 2, 2, 2, 2 },
                                                                   { 2, 2, 2, 2, 2 } });
        
        ComplexQRDecomposition.cleanup(A);
        
        final ComplexMatrix expected1 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                           { 0, 0, 1, 1, 1 },
                                                                           { 0, 0, 1, 1, 1 },
                                                                           { 0, 0, 0, 0, 1 },
                                                                           { 0, 0, 0, 0, 1 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 } },
                                                          new double[][] { { 2, 2, 2, 2, 2 },
                                                                           { 0, 0, 2, 2, 2 },
                                                                           { 0, 0, 2, 2, 2 },
                                                                           { 0, 0, 0, 0, 2 },
                                                                           { 0, 0, 0, 0, 2 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 } });
        
        assertEquals(expected1, A);
        
        final double e = Numerics.getInftyNormMatrixEpsilon(A);
        
        ComplexQRDecomposition.adjustSingularities(A);
        
        final ComplexMatrix expected2 = new ComplexMatrix(new double[][] { { 1, 1, 1, 1, 1 },
                                                                           { 0, e, 1, 1, 1 },
                                                                           { 0, 0, 1, 1, 1 },
                                                                           { 0, 0, 0, e, 1 },
                                                                           { 0, 0, 0, 0, 1 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 } },
                                                          new double[][] { { 2, 2, 2, 2, 2 },
                                                                           { 0, 0, 2, 2, 2 },
                                                                           { 0, 0, 2, 2, 2 },
                                                                           { 0, 0, 0, 0, 2 },
                                                                           { 0, 0, 0, 0, 2 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 },
                                                                           { 0, 0, 0, 0, 0 } });
        
        assertEquals(expected2, A);
    }
    
    /** Test a case where the input matrix is singular and {@link ComplexQRDecomposition#getR()} has at least one exact
     * zero on the main diagonal. */
    @Test
    void testPerfectlySingular() {
        final ComplexMatrix A = ComplexMatrix.ones(10, 10);
        
        final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A, false, false); /* (false, false) so we return the
                                                                                       * QR decomposition without
                                                                                       * modification */
        for (int i = 5; i < 10; i++) {
            assertEquals(Complex.ZERO, QR.getR().getEntry(i, i));
        }
        
        testComplexMatrix(A, 3);
    }
    
    /** Test an exceptional case of {@link ComplexQRDecomposition#of(ComplexMatrix)}
     * where the input matrix is "wide", which is not supported */
    @Test
    void testExceptional() {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(5, 10, 1.0, 1L, false);
        final ValidationException ex = assertThrows(ValidationException.class,
                                                    () -> ComplexQRDecomposition.of(A));
        
        assertEquals("Input matrix A is of dimension (5 x 10), but the "
                             + "QR decomposition can only be performed on square "
                             + "(m == n) and tall matrices (m > n)",
                     ex.getMessage());
    }

    /** Test a few random {@code 10 x 10} {@link ComplexMatrix complex matrices} */
    @Test
    void testAFewComplex() {
        createAndTestRandom(10, 10, 1.0,     12L, true);
        createAndTestRandom(10, 10, 10.0,    34L, true);
        createAndTestRandom(10, 10, 100.0,   56L, true);
        createAndTestRandom(10, 10, 1000.0,  78L, true);
        createAndTestRandom(10, 10, 10000.0, 90L, true);
    }
    
    /** Test {@link ComplexQRDecomposition} using a few random rectangular matrices */
    @Test
    void testAFewComplexRectangular() {
        createAndTestRandom(5,  3, 1.0,    12L, true);
        createAndTestRandom(10, 5, 100.0,  34L, true);
        createAndTestRandom(20, 3, 1000.0, 56L, true);
    }
    
    /** Test a {@code 10 x 10} {@link ComplexMatrix} which has rows whose {@link ComplexVector#hermitianNorm()} is at
     * most {@code 1e9}.<br><br>
     * 
     * As expected, the required epsilons are a function of the scale of the values in the matrix. */
    @Test
    void testLargeScale() {
        createAndTestRandom(10, 10, 1e9, 8675309L, true);
    }
    
    /** Test a few random {@code 10 x 10} {@link ComplexMatrix complex matrices} that only contain real values<br>
     * <br>
     * 
     * The errors in the imaginary components when we recompute A range from 1e-32 to 1e-28 */
    @Test
    void testAFewReal() {
        createAndTestRandom(10, 10, 1.0,     12L,  false);
        createAndTestRandom(10, 10, 10.0,    34L,  false);
        createAndTestRandom(10, 10, 100.0,   56L,  false);
        createAndTestRandom(10, 10, 1000.0,  78L,  false);
        createAndTestRandom(10, 10, 1000.0,  100L, false);
        createAndTestRandom(10, 10, 10000.0, 90L,  false);
    }
    
    /** Test {@link ComplexQRDecomposition} with a known non-singular real matrix */
    @Test
    void testNonSingularReal3x3() {
        final double[][] data = { { 12, -51,   4 },
                                  {  6, 167, -68 },
                                  { -4,  24, -41 } };
        final ComplexMatrix A = new ComplexMatrix(new Matrix(data));
        testComplexMatrix(A, 2);
    }
    
    /** Test {@link ComplexQRDecomposition} with a known singular real matrix */
    @Test
    void testSingularReal3x3One() {
        final double[][] data = { { 1, 4, 7, },
                                  { 2, 5, 8, },
                                  { 3, 6, 9, } };
        final ComplexMatrix A = new ComplexMatrix(new Matrix(data));
        testComplexMatrix(A, 1);
    }
    
    /** Test {@link ComplexQRDecomposition} with another known singular real matrix */
    @Test
    void testSingularReal3x3Two() {
        double[][] data = {  {  1, 6,  4 },
                             {  2, 4, -1 },
                             { -1, 2,  5 } };
        final ComplexMatrix A = new ComplexMatrix(new Matrix(data));
        testComplexMatrix(A, 2);
        
        final ComplexQRDecomposition QR1 = ComplexQRDecomposition.of(A);
        final ComplexQRDecomposition QR2 = ComplexQRDecomposition.of((FieldMatrix<Complex>) A);
        
        assertEquals(QR1.getQ(), QR2.getQ());
        assertEquals(QR1.getR(), QR2.getR());
    }
    
    /** Do a stress-test with a deliberately ill-conditioned matrix */
    @Test
    void testIllConditioned() {
        final double maxRowHermNorm = 1e9;
        final ComplexMatrix illCond = testIllConditioned(maxRowHermNorm);
        
        // Do a toString check just for reference.  The poor conditioning is clear.
        // CHECKSTYLE.OFF: LineLength
        final String expected = String.join(System.lineSeparator(),
                "         0.0         +          0.0         i,          0.0                +          0.0                 i,  -305476054.0987897           + -156530854.8197             i,   287226753.63188106         + -111987441.94320664 i,          0.0        +          0.0        i,           0.42996816335155996 +          0.0                i,  -273262119.96445245           + -277750016.1084062          i,   302080398.4957174          +  130199517.05741708          i,           0.0                 +          0.0                 i,           0.0        +          0.0                i",
                "         0.0         +          0.0         i,          0.0                +          0.0                 i,   162475215.12600842          +  315093471.3433663          i,           0.0                +          0.0        i,          0.0        +          0.0        i,           0.9250330436838938  +          0.0                i,   260203644.04636785           + -212409722.51735425         i,           0.0                +          0.0                 i,   198878705.7138953           +   54480196.35611548          i,  -240348509.14023018 + -149235594.27328092         i",
                "         0.0         +          0.0         i,          0.0                +          0.0                 i,   330242503.64884675          + -207205001.1159862          i,           0.0                +          0.0        i,  190079284.4911144  +   35549331.72388984 i,           0.0                 +          0.0                i,           0.0                  +          0.3418518892906355 i,          -0.9157469933273159 +          0.0                 i,    80878596.05928093          + -282565853.0665829           i,  -250715704.66124576 +  -77518259.16547373         i",
                "  54329416.94356301  +   84180030.05993731  i,  173896212.71226043         +  265910035.1929668           i,  -265013217.9314948           +  222379483.32497174         i,  -247409049.77874875         + -284512381.96752626 i,  315157277.63530207 + -239226741.60250735 i,    39501005.41972216          +  319894793.5217787          i,   -79588540.42151827           +    4851783.961110484        i,           0.0                +          0.0                 i,          -0.33417336653900076 +          0.0                 i,           0.0        +          0.0                i",
                "         0.0         +          0.0         i,          0.0                +         -0.12695240988223366 i,           0.35954932487274816 +          0.0                i,           0.0                +          0.0        i,          0.0        +          0.0        i,           0.0                 +          0.0                i,  -279898039.08200914           + -171533351.0142744          i,           0.0                +         -0.4868987798715789  i,          -0.8888514378782144  +          0.0                 i,  -365081711.3908507  +   53140463.07351287         i",
                "         0.0         +          0.0         i,          0.0                +          0.5097364551327246  i,           0.0                 +          0.0                i,           0.0                +          0.0        i,          0.0        +          0.0        i,           0.0                 +          0.0                i,  -113509823.68832514           +  288747664.1414564          i,  -202154441.34275335         + -215603178.5856774           i,           0.0                 +          0.0                 i,           0.0        +          0.0                i",
                "         0.0         +          0.0         i,  120720962.72310151         + -418417768.6142432           i,           0.0                 +          0.0                i,           0.5711001439935011 +          0.0        i,  161843089.45829585 + -220130802.68199128 i,           0.0                 +          0.8170175869495133 i,          -0.016173057855442874 +          0.0                i,  -249672332.36606658         +  205113194.66428596          i,           0.0                 +          0.30619505696225424 i,           0.0        +          0.6499555628132678 i",
                " -14073663.274740817 +   53424598.366254374 i,  328202830.8270247          + -310897914.15395486          i,   171167372.6411259           + -311676805.18796927         i,           0.0                +          0.0        i,          0.0        +          0.0        i,  -275177300.9277884           +  323827488.7477831          i,           0.0                  +          0.0                i,  -359355341.6287189          +  165066037.0047426           i,  -117289201.58185199          +  143193470.64603204          i,   321748937.5464708  +  -95419700.4323717          i",
                "         0.0         +          0.0         i,          0.0                +          0.0                 i,   307713273.74818456          + -154312449.91652453         i,  -160375167.65548867         +  121173844.18534105 i,          0.0        +          0.0        i,   147774831.68661562          + -279833355.29667836         i,   211737912.9133602            +   32913844.377379853        i,           0.0                +          0.17741107124305566 i,           0.0                 +          0.0                 i,  -307276193.1164113  + -314296371.7733807          i",
                " 273092438.34306824  + -254363205.0135707   i,         -0.8380234508196005 +          0.0                 i,           0.0                 +          0.2784766803331289 i,  -174353551.44391355         +  149976444.40167275 i,          0.0        +          0.0        i,           0.0                 +          0.0                i,   168610557.3267452            +   99808928.7284172          i,           0.0                +          0.0                 i,           0.0                 +          0.0                 i,  -309537443.64084345 + -284158321.56914264         i");
        // CHECKSTYLE.ON: LineLength
        TestUtils.testMultiLineToString(expected, illCond);
    }
    
    /** Do an extreme stress-test with a deliberately very ill-conditioned matrix. Notice the huge tolerances. The sizes
     * make sense given the magnitude of the largest elements, but they should be driven down.<br>
     * <br>
     * 
     * Notably, the unitary check on {@code Q} works just as well as in every other test, which is great to see (i.e.,
     * we use {@link #ID_EPS} as the tolerance for both real and imaginary parts). */
    @Test
    void testExtremelyIllConditioned() {
        final double maxRowHermNorm = 1e27;
        final ComplexMatrix illCond = testIllConditioned(maxRowHermNorm);
        
        // Do a toString check just for reference.  The poor conditioning is clear.
        // CHECKSTYLE.OFF: LineLength
        final String expected = String.join(System.lineSeparator(),
                "                           0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -3.0547605409878970e+26  + -1.5653085481970000e+26  i,   2.8722675363188106e+26  + -111987441943206630000000000 i,                            0 +                            0 i,   4.2996816335155996e-01  +  0.0000000000000000e+00  i,  -2.7326211996445244e+26  + -2.7775001610840616e+26  i,   3.0208039849571740e+26  +  1.3019951705741710e+26  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,                             0 +  0.0000000000000000e+00  i",
                "                           0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   1.6247521512600844e+26  +  3.1509347134336635e+26  i,   0.0000000000000000e+00  +                            0 i,                            0 +                            0 i,   9.2503304368389380e-01  +  0.0000000000000000e+00  i,   2.6020364404636788e+26  + -2.1240972251735424e+26  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   1.9887870571389530e+26  +  5.4480196356115490e+25  i,  -240348509140230180000000000 + -1.4923559427328093e+26  i",
                "                           0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   3.3024250364884670e+26  + -2.0720500111598620e+26  i,   0.0000000000000000e+00  +                            0 i,  190079284491114400000000000 +   35549331723889840000000000 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   0.0000000000000000e+00  +  3.4185188929063550e-01  i,  -9.1574699332731590e-01  +  0.0000000000000000e+00  i,   8.0878596059280940e+25  + -2.8256585306658293e+26  i,  -250715704661245780000000000 + -7.7518259165473730e+25  i",
                "  54329416943563000000000000 +   84180030059937300000000000 i,   1.7389621271226044e+26  +  2.6591003519296680e+26  i,  -2.6501321793149480e+26  +  2.2237948332497173e+26  i,  -2.4740904977874876e+26  + -284512381967526270000000000 i,  315157277635302100000000000 + -239226741602507340000000000 i,   3.9501005419722155e+25  +  3.1989479352177870e+26  i,  -7.9588540421518270e+25  +  4.8517839611104840e+24  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -3.3417336653900076e-01  +  0.0000000000000000e+00  i,                             0 +  0.0000000000000000e+00  i",
                "                           0 +                            0 i,   0.0000000000000000e+00  + -1.2695240988223366e-01  i,   3.5954932487274816e-01  +  0.0000000000000000e+00  i,   0.0000000000000000e+00  +                            0 i,                            0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -2.7989803908200910e+26  + -1.7153335101427440e+26  i,   0.0000000000000000e+00  + -4.8689877987157890e-01  i,  -8.8885143787821440e-01  +  0.0000000000000000e+00  i,  -365081711390850750000000000 +  5.3140463073512870e+25  i",
                "                           0 +                            0 i,   0.0000000000000000e+00  +  5.0973645513272460e-01  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   0.0000000000000000e+00  +                            0 i,                            0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -1.1350982368832514e+26  +  2.8874766414145643e+26  i,  -2.0215444134275335e+26  + -2.1560317858567737e+26  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,                             0 +  0.0000000000000000e+00  i",
                "                           0 +                            0 i,   1.2072096272310152e+26  + -4.1841776861424320e+26  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   5.7110014399350110e-01  +                            0 i,  161843089458295840000000000 + -220130802681991280000000000 i,   0.0000000000000000e+00  +  8.1701758694951330e-01  i,  -1.6173057855442874e-02  +  0.0000000000000000e+00  i,  -2.4967233236606656e+26  +  2.0511319466428597e+26  i,   0.0000000000000000e+00  +  3.0619505696225424e-01  i,                             0 +  6.4995556281326780e-01  i",
                " -14073663274740817000000000 +   53424598366254375000000000 i,   3.2820283082702470e+26  + -3.1089791415395487e+26  i,   1.7116737264112590e+26  + -3.1167680518796920e+26  i,   0.0000000000000000e+00  +                            0 i,                            0 +                            0 i,  -2.7517730092778838e+26  +  3.2382748874778310e+26  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -3.5935534162871890e+26  +  1.6506603700474260e+26  i,  -1.1728920158185200e+26  +  1.4319347064603203e+26  i,   321748937546470800000000000 + -9.5419700432371710e+25  i",
                "                           0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   3.0771327374818458e+26  + -1.5431244991652455e+26  i,  -1.6037516765548867e+26  +  121173844185341050000000000 i,                            0 +                            0 i,   1.4777483168661562e+26  + -2.7983335529667835e+26  i,   2.1173791291336023e+26  +  3.2913844377379853e+25  i,   0.0000000000000000e+00  +  1.7741107124305566e-01  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -307276193116411350000000000 + -3.1429637177338070e+26  i",
                " 273092438343068220000000000 + -254363205013570700000000000 i,  -8.3802345081960050e-01  +  0.0000000000000000e+00  i,   0.0000000000000000e+00  +  2.7847668033312890e-01  i,  -1.7435355144391356e+26  +  149976444401672740000000000 i,                            0 +                            0 i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   1.6861055732674520e+26  +  9.9808928728417220e+25  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,   0.0000000000000000e+00  +  0.0000000000000000e+00  i,  -309537443640843450000000000 + -2.8415832156914267e+26  i");
        // CHECKSTYLE.ON: LineLength
        TestUtils.testMultiLineToString(expected, illCond);
    }
    
    /** Test solving a square system; i.e., a linear system where the LHS matrix has exactly the same number of columns
     * as it does rows. The solution in this case is exact outside of numerical noise. */
    @Test
    void testSolveVectorSquareSimple() {
        final double[][] arrA = { { 1, 2, 3, 4, 5 },
                                  { 2, 3, 4, 5, 1 },
                                  { 3, 4, 5, 1, 2 },
                                  { 4, 5, 1, 2, 3 },
                                  { 5, 1, 2, 3, 4 } };
        
        final ComplexMatrix A = new ComplexMatrix(arrA);
        final ComplexVector b = new ComplexVector(new double[] { 1, 2, 3, 4, 5 });
        
        final ComplexVector x = ComplexQRDecomposition.of(A).solve(b);
        
        final ComplexVector xExpected = new ComplexVector(new double[] { 1, 0, 0, 0, 0 });
        
        NumericsTest.assertEBEVectorsEqualAbsolute(xExpected, x, 2 * MACH_EPS);
    }
    
    /** Test solving many random square systems with vector RHSs */
    @Test
    void testSolveVectorSquareRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n, 8675_309L);
            final ComplexVector b = ComplexVectorTest.randomComplexVector(n, 1.0, 123L);
            
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
            final ComplexVector          x  = QR.solve(b);
            
            ComplexMatrixTest.assertBackwardsStableError(b, A.operate(x), 4);
        }
    }
    
    /** Test solving many random square systems with matrix RHSs */
    @Test
    void testSolveMatrixSquareRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n, 8675_309L);
            final ComplexMatrix B = ComplexMatrixTest.randomComplexMatrix(n, n, 123L);
            
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
            final ComplexMatrix          X  = QR.solve(B);
            
            ComplexMatrixTest.assertBackwardsStableError(B, A.times(X), 1);
        }
    }
    
    /** Test solving a simple tall system; i.e., a linear system where the LHS matrix has fewer columns than it does
     * rows. <br>
     * <br>
     * 
     * Tall systems are over-determined, so there exists either one solution, or no solution. In the case where there
     * are zero solutions, the answer that's returned should minimize the 2-norm of the error. */
    @Test
    void testSolveVectorTallSimple() {
        final double[][] arrA = { { 1, 2, 3, 4, 5 },
                                  { 2, 3, 4, 5, 1 },
                                  { 3, 4, 5, 1, 2 },
                                  { 4, 5, 1, 2, 3 },
                                  { 5, 1, 2, 3, 4 },
                                  { 1, 5, 3, 2, 4 } };
        
        final ComplexMatrix A = new ComplexMatrix(arrA);
        final ComplexVector b = new ComplexVector(new double[] { 1, 2, 3, 4, 5, 6 });
        
        final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
        final ComplexVector          x  = QR.solve(b);
        
        // MATLAB answer:
        final ComplexVector xExpected = new ComplexVector(new double[] { 4.791666666666666e-01,
                                                                         4.947916666666666e-01,
                                                                        -5.208333333333340e-02,
                                                                        -2.083333333333330e-01,
                                                                         4.166666666666665e-01 });
        
        NumericsTest.assertEBEVectorsEqualAbsolute(xExpected, x, ID_EPS);
        
        assertLSBF(A, x, b);
    }
    
    /** Test solving many random tall systems. The LHS matrices here have {@code n} rows and {@code n/2} columns. */
    @Test
    void testSolveVectorTallRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n / 2, 8675_309L);
            final ComplexVector b = ComplexVectorTest.randomComplexVector(n, 1.0, 123L);
            
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
            final ComplexVector          x  = QR.solve(b);
            
            assertLSBF(A, x, b);
        }
    }
    
    /** Test solving many random tall systems with matrix RHSs. The LHS matrices here have {@code n} rows and
     * {@code n/2} columns. */
    @Test
    void testSolveMatrixTallRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n / 2, 8675_309L);
            final ComplexMatrix B = ComplexMatrixTest.randomComplexMatrix(n, n, 123L);
            
            final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
            final ComplexMatrix          X  = QR.solve(B);
            
            assertLSBF(A, X, B);
        }
    }
    
    /** Test the {@link DecompositionSolver} implementations in {@link ComplexQRDecomposition}
     * 
     * <ul>
     * <li>{@link ComplexQRDecomposition#solve(FieldVector)}</li>
     * <li>{@link ComplexQRDecomposition#solve(FieldMatrix)}</li>
     * <li>{@link ComplexQRDecomposition#getInverse()}</li>
     * <li>{@link ComplexQRDecomposition#isNonSingular()}</li>
     * <li>{@link ComplexQRDecomposition#getRowDimension()}</li>
     * <li>{@link ComplexQRDecomposition#getColumnDimension()}</li>
     * </ul>
     */
    @Test
    void testDecompositionSolver() {
        final int n = 10;
        
        final ComplexMatrix          sqA  = ComplexMatrixTest.randomComplexMatrix(n, n, 8675_309L);
        final ComplexQRDecomposition sqQR = ComplexQRDecomposition.of(sqA);
        
        assertTrue(sqQR.isNonSingular());
        
        assertEquals(10, sqQR.getRowDimension());
        assertEquals(10, sqQR.getColumnDimension());
        
        final ComplexVector b = ComplexVectorTest.randomComplexVector(n, 1.0, 123L);
        final ComplexVector x = sqQR.solve((FieldVector<Complex>) b); // cast to hit the correct overrides
        
        final ComplexMatrix B = ComplexMatrixTest.randomComplexMatrix(n, n, 123L);
        final ComplexMatrix X = sqQR.solve((FieldMatrix<Complex>) B); // cast to hit the correct overrides
        
        ComplexMatrixTest.assertBackwardsStableError(b, sqA.operate(x), 3);
        ComplexMatrixTest.assertBackwardsStableError(B, sqA.times(X),   3);
        
        final ComplexMatrix inv = sqQR.getInverse();
        ComplexMatrixTest.assertBackwardsStableError(ComplexMatrix.identity(10), sqA.times(inv), 1);
        
        final ComplexMatrix              tallA  = ComplexMatrixTest.randomComplexMatrix(n, n / 2, 8675_309L);
        final ComplexQRDecomposition tallQR = ComplexQRDecomposition.of(tallA);
        
        assertThrows(MathIllegalArgumentException.class, () -> tallQR.getInverse()); // original matrix is non-square
        
        final ComplexVector x2 = tallQR.solve((FieldVector<Complex>) b); // cast to hit the correct overrides
        final ComplexMatrix  X2 = tallQR.solve((FieldMatrix<Complex>) B); // cast to hit the correct overrides
        
        assertLSBF(tallA, x2, b);
        assertLSBF(tallA, X2, B);
        
        assertEquals(10, tallQR.getRowDimension());
        assertEquals(5,  tallQR.getColumnDimension());
        
        assertTrue(tallQR.isNonSingular());
        
        assertFalse(ComplexQRDecomposition.of(ComplexMatrix.zero(n, n)).isNonSingular());
    }
    
    /** The same as {@link #assertLSBF(ComplexMatrix, ComplexVector, ComplexVector)}, except where the RHS is a matrix.
     * The test simply calls {@link #assertLSBF(ComplexMatrix, ComplexVector, ComplexVector)} for each of the columns of
     * {@code X}
     * 
     * @param A the LHS of the equation
     * @param X the candidate solution
     * @param B the RHS of the equation */
    static void assertLSBF(final ComplexMatrix A, final ComplexMatrix X, final ComplexMatrix B) {
        for (int j = 0; j < A.getColumnDimension(); j++) {
            assertLSBF(A, X.getColumnVector(j), B.getColumnVector(j));
        }
    }
    
    /** Assert that {@code x} is a least-squares best-fit solution to the problem {@code Ax = b}. We do this by
     * asserting that the relative residual - as described in {@link LinearSolverTest} - of the solution vector
     * {@code x} is (likely) minimal in a least-squares sense.<br>
     * <br>
     * 
     * To show that {@code x} (likely) results in the minimal error, for each cardinal dimension in the space of
     * {@code x}, we copy and perturb {@code x} in the given direction and show that the RR associated with the
     * perturbed vector is larger than the RR of the original. We use a tolerance of {@link #MACH_EPS}.
     * 
     * @param A the LHS of the equation
     * @param x the candidate solution
     * @param b the RHS of the equation */
    static void assertLSBF(final ComplexMatrix A, final ComplexVector x, final ComplexVector b) {
        final RandomDataGenerator rdg = new RandomDataGenerator(A.hashCode());
        
        final double yNorm       = b.hermitianNorm();
        final double rrLeastSqrs = -MACH_EPS + b.minus(A.operate(x)).hermitianNorm() / yNorm;
        
        for (int row = 0; row < x.getDimension(); row++) { // row -> the row of the x vector to perturb
            final ComplexVector perturbedX = new ComplexVector(x);
            
            final Complex entry = perturbedX.getEntry(row);
            
            final double perturbRe = 1e-6 * 2 * (rdg.nextDouble() - 1.0); // 1e-6 * [-1, 1]
            final double perturbIm = 1e-6 * 2 * (rdg.nextDouble() - 1.0); // 1e-6 * [-1, 1]
            
            final Complex perturbation   = Complex.valueOf(perturbRe, perturbIm); // 1e-6 * [-1, 1] x [-1, 1]i
            final Complex perturbedEntry = entry.multiply(Complex.ONE.add(perturbation));
            
            perturbedX.setEntry(row, perturbedEntry);
            
            final double rrPerturbed = b.minus(A.operate(perturbedX)).hermitianNorm() / yNorm;
            assertLessThanOrEqualTo(rrLeastSqrs, rrPerturbed, "rrLeastSqrs - u for column " + row);
        }
    }
    
    /** When used with a large enough {@code maxRowHermNorm} value, this test creates and tests an ill-conditioned
     * {@link ComplexMatrix}
     * 
     * @param maxRowHermNorm the max {@link ComplexVector#hermitianNorm()} of any of the rows (this is just Euclidean
     *        distance if all real).
     * @return the ill conditioned matrix that was created */
    private ComplexMatrix testIllConditioned(final double maxRowHermNorm) {
        final long seed = 8675309L; // create a new 10x10 random complex matrix whose rows have max Hermitian norm 1e9
        final ComplexMatrix originalA = ComplexMatrixTest.randomComplexMatrix(10, 10, maxRowHermNorm, seed, false);
        
        final int m = originalA.getRowDimension();
        final int n = originalA.getColumnDimension();
        
        /* Randomly change 25% of entries to zero, change an additional 12.5% to purely-real numbers in [-1, 1], and
         * change an additional 12.5% to purely-imaginary numbers in [-i, i] so that 50% total of all entries are either
         * 0 or very small relative to the original (unchanged) entries. */
        final Complex[][] newAData = originalA.getData(); // getData clones, so this is safe
        final RandomDataGenerator r = new RandomDataGenerator(seed);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                final double trigger = r.nextDouble();
                if (trigger < 0.25) {
                    newAData[i][j] = Complex.ZERO;
                }
                else if (trigger < 0.375) { // 0.375 = 0.25 + 0.125
                    final double a = 2 * (r.nextDouble() - 0.5); // a is [-1, 1]
                    newAData[i][j] = Complex.valueOf(a);
                }
                else if (trigger < 0.5) { // 0.5 = 0.375 + 0.125
                    final double a = 2 * (r.nextDouble() - 0.5); // a is [-1, 1]
                    newAData[i][j] = Complex.valueOf(0, a);
                }
            }
        }
        
        final ComplexMatrix newA = new ComplexMatrix(newAData);
        testComplexMatrix(newA, 1);
        return newA;
    }
    
    /** Create and test a random {@code m x n} {@link ComplexMatrix} (whose entries are either all {@link Complex} or
     * all real (doubles) depending on {@code complex}) with {@link ComplexQRDecomposition}. We use
     * {@link ComplexMatrixTest#randomComplexMatrix(int, int, double, long, boolean)} to create the test matrices.
     * 
     * @param m the number of rows
     * @param n the number of columns
     * @param maxRowHermNorm the max {@link ComplexVector#hermitianNorm()} of any of the rows (this is just Euclidean
     *        distance if all real).
     * @param seed the random seed
     * @param complex {@code true} to create a random {@link ComplexMatrix}, {@code false} to create a random
     *        {@link Matrix} that's wrapped in a {@link ComplexMatrix}.
     *        
     * @see #testComplexMatrix(ComplexMatrix, int) */
    void createAndTestRandom(final int m,
                             final int n,
                             final double maxRowHermNorm,
                             final long seed,
                             final boolean complex) {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(m, n, maxRowHermNorm, seed, !complex);
        testComplexMatrix(A, 2);
    }
    
    /** Test the results of applying {@link ComplexQRDecomposition} to the given {@code m x n} {@link ComplexMatrix}
     * 
     * @param A the test {@link ComplexMatrix}
     * @param toleranceScale we scale the allowed tolerance by this much. Ideally, this is {@code 1}. If not {@code 1},
     *        it must be small. */
    private static void testComplexMatrix(final ComplexMatrix A, final int toleranceScale) {
        final ComplexQRDecomposition QR         = ComplexQRDecomposition.of(A, false, false);
        final ComplexMatrix          Q          = QR.getQ();
        final ComplexMatrix          R          = QR.getR();
        final ComplexMatrix          shouldBeA1 = Q.times(R);
        
        final double tolerance = toleranceScale * A.getFrobeniusNorm() * Numerics.MACHINE_EPSILON;
        
        assertUnitary(Q, tolerance, tolerance);
        assertIsUpperTriangular(R, tolerance, tolerance);
        ComplexMatrixTest.assertAbsEquals(A, shouldBeA1, tolerance, tolerance); // check that A == QR
        
        final ComplexMatrix reducedQ   = QR.getReducedQ();
        final ComplexMatrix reducedR   = QR.getReducedR();
        final ComplexMatrix shouldBeA2 = reducedQ.times(reducedR);
        ComplexMatrixTest.assertAbsEquals(A, shouldBeA2, tolerance, tolerance); // check that A == reducedQ * reducedR
        
        final ComplexQRDecomposition QRwCleanup = ComplexQRDecomposition.of(A, true, true);
        final ComplexMatrix          wCleanupQ  = QRwCleanup.getQ();
        final ComplexMatrix          wCleanupR  = QRwCleanup.getR();
        final ComplexMatrix          shouldBeA3 = wCleanupQ.times(wCleanupR);
        ComplexMatrixTest.assertAbsEquals(A, shouldBeA3, tolerance, tolerance); // check that A == wCleanupQ * wCleanupR
        
        // checking manually, it appears clean-up improves the accuracy in most cases (17 of the 20 I checked)
    }
    
    /** Assert that the given {@link ComplexMatrix} is upper-triangular. Upper-triangular is a "0-Hessenberg"
     * {@link ComplexMatrix}, so we just use
     * {@link ComplexHessenbergReductionTest#assertIsNHessenberg(ComplexMatrix, int, double, double)} with {@code 0}.
     * 
     * @param R the matrix to check
     * @param absReTol the absolute tolerance when comparing the real parts of complex numbers
     * @param absImTol the absolute tolerance when comparing the imaginary parts of complex numbers
     * 
     * @see ComplexHessenbergReductionTest#assertIsNHessenberg(ComplexMatrix, int, double, double) */
    public static void assertIsUpperTriangular(final ComplexMatrix R, final double absReTol, final double absImTol) {
        ComplexHessenbergReductionTest.assertIsNHessenberg(R, 0, absReTol, absImTol);
    }
    
    /** Assert that the given {@link ComplexMatrix} is unitary by checking check U.U^H == I and U^H.U == I
     * 
     * @param M the matrix to check  */
    public static void assertUnitary(final ComplexMatrix M) {
        assertUnitary(M, ID_EPS, ID_EPS);
    }
    
    /** Assert that the given {@link ComplexMatrix} is unitary by checking check U.U^H == I and U^H.U == I
     * 
     * @param M the matrix to check 
     * @param absReTol the absolute tolerance when comparing the real parts of complex numbers
     * @param absImTol the absolute tolerance when comparing the imaginary parts of complex numbers */
    public static void assertUnitary(final ComplexMatrix M, final double absReTol, final double absImTol) {
        final ComplexMatrix I   = ComplexMatrix.identity(M.getRowDimension());
        final ComplexMatrix QQH = M.times(M.conjugateTranspose());
        final ComplexMatrix QHQ = M.conjugateTranspose().times(M);
        ComplexMatrixTest.assertAbsEquals(I, QQH, absReTol, absImTol);
        ComplexMatrixTest.assertAbsEquals(I, QHQ, absReTol, absImTol);
    }
    
    /** A mini program that uses {@link MatrixVisualizer} to display the results of a {@link ComplexQRDecomposition}
     * 
     * @param args ignored
     * @throws InterruptedException if {@link Thread#sleep(long)} is interrupted */
    @SuppressWarnings("unused")
    public static void main(final String[] args) throws InterruptedException {
        final ComplexMatrix A = new ComplexMatrix(new Matrix(new double[][] { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                                                                              { 1, 2, 2, 2, 2, 2, 2, 2, 2 },
                                                                              { 1, 2, 3, 3, 3, 3, 3, 3, 3 },
                                                                              { 1, 2, 3, 4, 4, 4, 4, 4, 4 },
                                                                              { 1, 2, 3, 4, 5, 5, 5, 5, 5 },
                                                                              { 1, 2, 3, 4, 5, 6, 6, 6, 6 },
                                                                              { 1, 2, 3, 4, 5, 6, 7, 7, 7 },
                                                                              { 1, 2, 3, 4, 5, 6, 7, 8, 8 },
                                                                              { 1, 2, 3, 4, 5, 6, 7, 8, 9 } }));
        
        final ComplexQRDecomposition QR = ComplexQRDecomposition.of(A);
        final ComplexMatrix          R  = QR.getR();
        final ComplexMatrix          Q  = QR.getQ();
        
        new MatrixVisualizer<>(A);
        new MatrixVisualizer<>(Q);
        new MatrixVisualizer<>(Q.times(R));
    }
}
