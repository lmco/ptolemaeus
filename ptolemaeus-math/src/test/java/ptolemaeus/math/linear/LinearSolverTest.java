/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static ptolemaeus.commons.TestUtils.assertLessThan;
import static ptolemaeus.commons.TestUtils.assertThrowsWithMessage;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.FieldMatrix;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.ValidationException;
import ptolemaeus.math.CountableDoubleIterable;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;
import ptolemaeus.math.linear.householderreductions.ComplexQRDecomposition;
import ptolemaeus.math.linear.householderreductions.ComplexQRDecompositionTest;
import ptolemaeus.math.linear.householderreductions.QRDecomposition;
import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.householderreductions.RealQRDecompositionTest;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.linear.real.NVectorTest;

/** Test {@link LinearSolver}.<br>
 * <br>
 * 
 * A key measure of goodness here is the so-called "relative residual" (RR), computed as {@code norm(b - Ax) / norm(b)}
 * or {@code norm(B - AX) / norm(B)}, where we're solving for {@code x} or {@code X} in {@code Ax = b} and
 * {@code AX = B}, respectively.<br>
 * <br>
 * 
 * All RR bounds here agree with the results MATLAB produces.<br>
 * <br>
 * 
 * See this paper for details: <a href="https://arnold.hosted.uark.edu/NLA/Pages/Ax=bResidual.pdf">The residual vector
 * for Ax = b</a><br>
 * <br>
 * 
 * You'll notice that in these tests, we use the upper-triangular {@code R} matrices {@link QRDecomposition QR
 * decompositions}.<br>
 * 
 * This is done because the error explodes as the size of the random upper/lower-triangular matrix grows after
 * approximately rank 70 or so, but this is not the case when the matrix is something "nice" like {@code R} from the
 * result of a QR decomposition of a random matrix; i.e., random triangular matrices are terribly conditioned.<br>
 * <br>
 * 
 * for example, the RR is smaller than 1e-16. When looking at a random 100x100 system, the RR was about 58.0!
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class LinearSolverTest {
    
    /** the maximum allowable relative residual, as computed in the {@link #testAXeqBRelativeResidual} series of
     * methods  */
    private static final double FWD_BCK_SQR_EPSILON = 1.5e-14;
    
    /** the maximum allowable relative residual, as computed in {@link #testRealSolveWideRandom()} */
    private static final double WIDE_SOLVE_EPSILON = 2e-15;
    
    /** the column vector dimensions we want to test */
    private static final int[] DIMENSIONS = new int[] { 5, 10, 25, 50 };
    
    /** the {@link LinearSolver} to use in testing */
    private static final LinearSolver LS = new LinearSolver(25, 25);
    
    /** For coverage, test {@link LinearSolver#getRealParallelThreshold()} and
     * {@link LinearSolver#getComplexParallelThreshold()} */
    @Test
    void testGetters() {
        assertEquals(40, LinearSolver.defaultSolver().getRealParallelThreshold());
        assertEquals(20, LinearSolver.defaultSolver().getComplexParallelThreshold());
    }
    
    /** Test {@link LinearSolver#checkLinearSystemDimensions(int, int, int, int)} */
    @Test
    void testCheckLinearSystemDimensions() {
        assertDoesNotThrow(() -> LinearSolver.checkLinearSystemDimensions(10, 5, 10, 1));
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> LinearSolver.checkLinearSystemDimensions(10, 5, 1, 10),
                                "When solving for X in the equation AX = B, where A is a matrix with size "
                                + "(10 x 5), B must have the same number of rows as A, but it has size (1 x 10)");
    }
    
    // **************************************** real solves ****************************************
    
    /** A simple test of the square case of {@link LinearSolver#solve(RealMatrix, RealVector)} and
     * {@link LinearSolver#solve(RealMatrix, RealVector)}. This is the only test of this type because, when the system
     * is square or tall, we use {@link RealQRDecomposition#solve(Matrix)} and
     * {@link RealQRDecomposition#solve(NVector)}, so see {@link RealQRDecompositionTest} for thorough testing. */
    @Test
    void testRealSolveSquareTallPassThrough() {
        final double[][] arrA = { { 1, 2, 3, 4, 5 },
                                  { 2, 3, 4, 5, 1 },
                                  { 3, 4, 5, 1, 2 },
                                  { 4, 5, 1, 2, 3 },
                                  { 5, 1, 2, 3, 4 } };
        
        final Matrix  A = new Matrix(arrA);
        final Matrix  B = A;
        final NVector b = B.getRowVector(0);
        
        final Matrix  X = LS.solve(A, B);
        final NVector x = LS.solve(A, b);
        
        final Matrix  XExpected = Matrix.identity(5);
        final NVector xExpected = XExpected.getRowVector(0);
        
        MatrixTest.assertBackwardsStableError(XExpected, X, 1);
        NumericsTest.assertEBEVectorsEqualAbsolute(xExpected, x,
                                                                2 * Numerics.MACHINE_EPSILON);
    }
    
    /** Test {@link LinearSolver#solve(RealMatrix, RealVector) solving} a simple wide system; i.e., a linear system
     * where the LHS matrix has fewer rows than it does columns. <br>
     * <br>
     * 
     * "Wide" systems are under-determined, so there are either infinitely many solutions, or there is no solution.
     * Thus, the solution itself isn't of much importance outside of actually solving the problem. That said, we check
     * against an expected solution to capture any changes.<br>
     * <br>
     * 
     * Of note: {@code MATLAB R2024a} - when using {@code \}, as in {@code A \ b}, a.k.a. {@code mldivide} - finds the
     * solution to be {@code (1, 1, 1, 0, 0.8)}. This is definitely a more "clean" solution, but it's no more or less
     * correct and, in fact, the MATLAB solution has a larger 2-norm than the solution found here ({@code mldivide} does
     * not make that guarantee) and, in general, we will prefer minimizing the 2-norm of the solution in the
     * underdetermined case. */
    @Test
    void testRealSolveWideSimple() {
        final double[][] arrA = { { 1, 0, 0, 0, 0 },
                                  { 0, 2, 0, 0, 0 },
                                  { 0, 0, 3, 0, 0 },
                                  { 0, 0, 0, 4, 5 } };
        
        final Matrix  A = new Matrix(arrA);
        final NVector b = new NVector(new double[] { 1, 2, 3, 4 });
        
        final NVector x = LS.solve(A, b);
        
        /* @formatter:off
         * Regarding the fractions below, I used WolframAlpha to see if there were any nearby
         * (clean) fractions that could reasonably be an analytical answer.
         * 
         * Given
         * 
         * 16 = 4 * 4,
         * 20 = 4 * 5,
         * 41 = 4 * 5 * 2 + 1
         * 
         * this seems likely, and the sanity check below agrees
         * @formatter:on */
        final NVector expectedX = new NVector(1, 1, 1, 16.0 / 41.0, 20.0 / 41.0);
        
        // zero tolerance sanity check on the expected solution:
        NumericsTest.assertEBEVectorsEqualAbsolute(b, A.operate(expectedX), 0.0);
        
        // check the numerical result is nearly the "analytic" result from above:
        NumericsTest.assertEBEVectorsEqualAbsolute(expectedX, x, Numerics.MACHINE_EPSILON);
        
        // confirm it works, though we know it will due to being (nearly) equal to the "exact" solution:
        final double tol = 2 * Numerics.MACHINE_EPSILON;
        NumericsTest.assertEBEVectorsEqualAbsolute(b, A.operate(x), tol);
    }
    
    /** Test {@link LinearSolver#solve(RealMatrix, RealVector)} on a few random underdetermined systems. We only show
     * that the solution found is indeed a solution, as I (RM) cannot come up with a method of showing that the solution
     * has a minimal 2-norm solution that isn't circular or that relies on the pseudoinverse. */
    @Test
    void testRealSolveWideRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix  A = MatrixTest.randomMatrix(n / 2, n, 1.0, 8675_309L);
            final NVector b = NVectorTest.randomNVector(n / 2, 1.0, 123L);
            
            final NVector x = LS.solve(A, b);
            
            testAXeqBRelativeResidual(A, x, b, WIDE_SOLVE_EPSILON);
            
            final Matrix B = MatrixTest.randomMatrix(n / 2, n, 1.0, 8675_309L);
            final Matrix X = LS.solve(A, B);
            testAXeqBRelativeResidual(A, X, B, WIDE_SOLVE_EPSILON);
        }
    }
    
    /** Test using forward and back substitution to solve real-valued systems of equations with only one RHS vector */
    @Test
    void testRealVectorSquareSolve() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix A = MatrixTest.randomMatrix(n, n, 1.0, 8675_309L);
            final Matrix U = RealQRDecomposition.of(A).getR();
            final Matrix L = U.transpose();
            
            final NVector b  = NVectorTest.randomNVector(n, 1.0, 123L);
            final NVector x1 = LS.solveSquareBackSubstitution(U, b);
            final NVector x2 = LS.solveSquareForwardSubstitution(L, b);
            
            testAXeqBRelativeResidual(U, x1, b, FWD_BCK_SQR_EPSILON);
            testAXeqBRelativeResidual(L, x2, b, FWD_BCK_SQR_EPSILON);
        }
    }
    
    /** Test using forward and back substitution to solve real-valued systems of equations with matrix RHSs. The RHS
     * matrices have twice as many columns as the LHS matrices to ensure that we handle that case properly. */
    @Test
    void testRealMatrixSquareSolve() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final Matrix A = MatrixTest.randomMatrix(n, n, 1.0, 8675_309L);
            final Matrix U = RealQRDecomposition.of(A).getR();
            final Matrix L = U.transpose();
            
            final Matrix B = MatrixTest.randomMatrix(n, n * 2, 1.0, 8675_309L);
            final Matrix X1 = LS.solveSquareBackSubstitution(U, B);
            final Matrix X2 = LS.solveSquareForwardSubstitution(L, B);
            
            testAXeqBRelativeResidual(U, X1, B, FWD_BCK_SQR_EPSILON);
            testAXeqBRelativeResidual(L, X2, B, FWD_BCK_SQR_EPSILON);
        }
    }
    
    /** Assert that {@code x} solves {@code Ax = b} by ensuring that the relative residual (defined at the class-level)
     * is less than {@code maxRelRes}; i.e., assert that {@code norm(b - Ax) / norm(b) < maxRelRes}.
     * 
     * @param A the LHS {@link Matrix}
     * @param x the approximate solution {@link NVector}
     * @param b the RHS {@link NVector}
     * @param maxRelRes the allowable relative residual {@code norm(b - Ax) / norm(b)} */
    private static void testAXeqBRelativeResidual(final Matrix A,
                                                  final NVector x,
                                                  final NVector b,
                                                  final double maxRelRes) {
        testAXeqBRelativeResidual(A,
                                  new Matrix(new double[][] { x.getDataRef() }).transpose(),
                                  new Matrix(new double[][] { b.getDataRef() }).transpose(),
                                  maxRelRes);
    }
    
    /** Assert that {@code X} solves {@code AX = B} by ensuring that the relative residual (defined at the class-level)
     * is less than {@code maxRelRes}; i.e., assert that {@code norm(B - AX) / norm(X) < maxRelRes}.
     * 
     * @param A the LHS {@link Matrix}
     * @param X the approximate solution {@link Matrix}
     * @param B the RHS {@link Matrix}
     * @param maxRelRes the allowable relative residual {@code norm(B - AX) / norm(B)} */
    private static void testAXeqBRelativeResidual(final Matrix A,
                                                  final Matrix X,
                                                  final Matrix B,
                                                  final double maxRelRes) {
        // relative residual
        final double rr = B.subtract(A.multiply(X)).getFrobeniusNorm() / B.getFrobeniusNorm();
        assertLessThan(rr, maxRelRes, "relative residual");
    }
    
    // **************************************** complex solves ****************************************
    
    /** A simple test of the square case of {@link LinearSolver#solve(FieldMatrix, FieldVector)} and
     * {@link LinearSolver#solve(FieldMatrix, FieldVector)}. This is the only test of this type because, when the system
     * is square or tall, we use {@link ComplexQRDecomposition#solve(ComplexMatrix)} and
     * {@link ComplexQRDecomposition#solve(ComplexVector)}, so see {@link ComplexQRDecompositionTest} for thorough
     * testing. */
    @Test
    void testComplexSolveSquareTallPassThrough() {
        final double[][] arrA = { { 1, 2, 3, 4, 5 },
                                  { 2, 3, 4, 5, 1 },
                                  { 3, 4, 5, 1, 2 },
                                  { 4, 5, 1, 2, 3 },
                                  { 5, 1, 2, 3, 4 } };
        
        final ComplexMatrix A = new ComplexMatrix(arrA);
        final ComplexMatrix B = A;
        final ComplexVector b = B.getRowVector(0);
        
        final ComplexMatrix X = LS.solve(A, B);
        final ComplexVector x = LS.solve(A, b);
        
        final ComplexMatrix XExpected = ComplexMatrix.identity(5);
        final ComplexVector xExpected = XExpected.getRowVector(0);
        
        ComplexMatrixTest.assertBackwardsStableError(XExpected, X, 1);
        NumericsTest.assertEBEVectorsEqualAbsolute(xExpected, x,
                                                                2 * Numerics.MACHINE_EPSILON);
    }
    
    /** Test {@link LinearSolver#solve(FieldMatrix, FieldVector) solving} a simple wide system; i.e., a linear system
     * where the LHS matrix has fewer rows than it does columns.
     * 
     * @see #testRealSolveWideSimple() */
    @Test
    void testComplexSolveWideSimple() {
        final double[][] arrA = { { 1, 0, 0, 0, 0 },
                                  { 0, 2, 0, 0, 0 },
                                  { 0, 0, 3, 0, 0 },
                                  { 0, 0, 0, 4, 5 } };
        
        final ComplexMatrix A = new ComplexMatrix(arrA);
        final ComplexVector b = new ComplexVector(new double[] { 1, 2, 3, 4 });
        
        final ComplexVector x = LS.solve(A, b);
        
        /* @formatter:off
         * Regarding the fractions below, I used WolframAlpha to see if there were any nearby
         * (clean) fractions that could reasonably be an analytical answer.
         * 
         * Given
         * 
         * 16 = 4 * 4,
         * 20 = 4 * 5,
         * 41 = 4 * 5 * 2 + 1
         * 
         * this seems likely, and the sanity check below agrees
         * @formatter:on */
        final ComplexVector expectedX = new ComplexVector(1, 1, 1, 16.0 / 41.0, 20.0 / 41.0);
        
        // zero tolerance sanity check on the expected solution:
        NumericsTest.assertEBEVectorsEqualAbsolute(b, A.operate(expectedX), 0.0);
        
        // check the numerical result is nearly the "analytic" result from above:
        NumericsTest.assertEBEVectorsEqualAbsolute(expectedX, x, Numerics.MACHINE_EPSILON);
        
        // confirm it works, though we know it will due to being (nearly) equal to the "exact" solution:
        final double tol = 2 * Numerics.MACHINE_EPSILON;
        NumericsTest.assertEBEVectorsEqualAbsolute(b, A.operate(x), tol);
    }
    
    /** Test {@link LinearSolver#solve(RealMatrix, RealVector)} on a few random underdetermined systems. We only show
     * that the solution found is indeed a solution, as I (RM) cannot come up with a method of showing that the solution
     * has a minimal 2-norm solution that isn't circular or that relies on the pseudoinverse. */
    @Test
    void testComplexSolveWideRandom() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n / 2, n, 8675_309L);
            final ComplexVector b = ComplexVectorTest.randomComplexVector(n / 2, 1.0, 123L);
            final ComplexVector x = LS.solve(A, b);
            
            testAXeqBRelativeResidual(A, x, b, WIDE_SOLVE_EPSILON);
            
            final ComplexMatrix B = ComplexMatrixTest.randomComplexMatrix(n / 2, n, 8675_309L);
            final ComplexMatrix X = LS.solve(A, B);
            testAXeqBRelativeResidual(A, X, B, WIDE_SOLVE_EPSILON);
        }
    }
    
    /** Test using forward and back substitution to solve complex-valued systems of equations with only one RHS
     * vector */
    @Test
    void testComplexVectorSquareSolve() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n, 1.0, 8675_309L, false);
            final ComplexMatrix U = ComplexQRDecomposition.of(A).getR();
            final ComplexMatrix L = U.transpose();
            
            final ComplexVector b  = ComplexVectorTest.randomComplexVector(n, 1.0, 123L);
            final ComplexVector x1 = LS.solveSquareBackSubstitution(U, b);
            final ComplexVector x2 = LS.solveSquareForwardSubstitution(L, b);
            
            testAXeqBRelativeResidual(U, x1, b, FWD_BCK_SQR_EPSILON);
            testAXeqBRelativeResidual(L, x2, b, FWD_BCK_SQR_EPSILON);
        }
    }
    
    /** Test using forward and back substitution to solve real-valued systems of equations with a matrix RHSs. The RHS
     * matrices have twice as many columns as the LHS matrices to ensure that we handle that case properly. */
    @Test
    void testComplexMatrixSquareSolve() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n, 1.0, 8675_309L, false);
            final ComplexMatrix U = ComplexQRDecomposition.of(A).getR();
            final ComplexMatrix L = U.transpose();
            
            final ComplexMatrix B = ComplexMatrixTest.randomComplexMatrix(n, 2 * n, 1.0, 8675_309L, false);
            final ComplexMatrix X1 = LS.solveSquareBackSubstitution(U, B);
            final ComplexMatrix X2 = LS.solveSquareForwardSubstitution(L, B);
            
            testAXeqBRelativeResidual(U, X1, B, FWD_BCK_SQR_EPSILON);
            testAXeqBRelativeResidual(L, X2, B, FWD_BCK_SQR_EPSILON);
        }
    }
    
    /** Assert that {@code x} solves {@code Ax = b} by ensuring that the relative residual (defined at the class-level)
     * is less than {@code maxRelRes}; i.e., assert that {@code norm(b - Ax) / norm(b) < maxRelRes}.
     * 
     * @param A the LHS {@link Matrix}
     * @param x the approximate solution {@link NVector}
     * @param b the RHS {@link NVector}
     * @param maxRelRes the allowable relative residual {@code norm(b - Ax) / norm(b)} */
    private static void testAXeqBRelativeResidual(final ComplexMatrix A,
                                                  final ComplexVector x,
                                                  final ComplexVector b,
                                                  final double maxRelRes) {
        testAXeqBRelativeResidual(A, ComplexMatrix.fromColumns(x), ComplexMatrix.fromColumns(b), maxRelRes);
    }
    
    /** Assert that {@code X} solves {@code AX = B} by ensuring that the relative residual (defined at the class-level)
     * is less than {@code maxRelRes}; i.e., assert that {@code norm(B - AX) / norm(X) < maxRelRes}.
     * 
     * @param A the LHS {@link ComplexMatrix}
     * @param X the approximate solution {@link ComplexMatrix}
     * @param B the RHS {@link ComplexMatrix}
     * @param maxRelRes the allowable relative residual {@code norm(B - AX) / norm(B)} */
    private static void testAXeqBRelativeResidual(final ComplexMatrix A,
                                                  final ComplexMatrix X,
                                                  final ComplexMatrix B,
                                                  final double maxRelRes) {
        // relative residual
        final double rr = B.subtract(A.multiply(X)).getFrobeniusNorm() / B.getFrobeniusNorm();
        assertLessThan(rr, maxRelRes, "relative residual");
    }
    
    // ======================================== condition numbers ========================================
    
    /** Test {@link LinearSolver#cond(RealMatrix)} */
    @Test
    void testRealCond() {
        final Matrix realSqr = MatrixTest.randomMatrix(10, 10, 100.0, 123L);
        final Matrix realRec = MatrixTest.randomMatrix(20, 10, 100.0, 123L);
        
        assertEquals(97.16523309271895, LS.cond(realSqr));
        assertEquals(23.99492050426149, LS.cond(realRec));
        
        final Matrix lowCond = RealQRDecomposition.of(realSqr).getQ();
        assertEquals(10.0, LS.cond(lowCond));
    }
    
    /** Test {@link LinearSolver#cond(FieldMatrix)} */
    @Test
    void testComplexCond() {
        final ComplexMatrix realSqr = ComplexMatrixTest.randomComplexMatrix(10, 10, 100.0, 123L, false);
        final ComplexMatrix realRec = ComplexMatrixTest.randomComplexMatrix(20, 10, 100.0, 123L, false);
        
        assertEquals(60.83808934342632, LS.cond(realSqr));
        assertEquals(13.160739914997523, LS.cond(realRec));
        
        final ComplexMatrix lowCond = ComplexQRDecomposition.of(realSqr).getQ();
        assertEquals(10.000000000000002, LS.cond(lowCond));
    }
    
    // ======================================== real inverse ========================================
    
    /** Test {@link LinearSolver#inv(RealMatrix)} with a simple matrix */
    @Test
    void testInvSimpleReal() {
        final double[][] arrA = { { 1, 0, 0, 0 },
                                  { 0, 2, 0, 0 },
                                  { 0, 0, 3, 0 },
                                  { 0, 0, 0, 4 } };
        
        final Matrix A = new Matrix(arrA);
        final Matrix invA = LS.inv(A);
        
        final String expected = String.join(System.lineSeparator(),
                                            " 1 0.0 0.0                0.0 ",
                                            " 0 0.5 0.0                0.0 ",
                                            " 0 0.0 0.3333333333333333 0.0 ",
                                            " 0 0.0 0.0                0.25");
        TestUtils.testMultiLineToString(expected, invA);
        
        assertEquals(Matrix.identity(4), A.times(invA));
        assertEquals(Matrix.identity(4), invA.times(A));
        
        // test exception on wide or tall
        assertThrows(MathIllegalArgumentException.class,
                     () -> LS.inv(MatrixTest.randomMatrix(3, 4, 1.0, 1L)));
        assertThrows(MathIllegalArgumentException.class,
                     () -> LS.inv(MatrixTest.randomMatrix(4, 3, 1.0, 1L)));
    }
    
    /** Test {@link LinearSolver#inv(RealMatrix)} and {@link LinearSolver#pinv(RealMatrix)} with a few random square
     * matrices */
    @Test
    void testInvAndPinvRandomReals() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            final Matrix A = MatrixTest.randomMatrix(n, n, 1.0, 123L);
            testInverseReal(A);
        }
    }
    
    /** Show that inverses calculated using {@link LinearSolver#inv(RealMatrix)} are indeed inverses, within absolute
     * tolerance.  Also test {@link LinearSolver#pinv(RealMatrix)} with square matrices..
     * 
     * @param A the matrix to check */
    static void testInverseReal(final Matrix A) {
        final Matrix Ainv = LS.inv(A);
        
        final Matrix shouldBeI1 = A.times(Ainv);
        final Matrix shouldBeI2 = Ainv.times(A);
        
        final Matrix I = Matrix.identity(A.getColumnDimension());
        
        MatrixTest.assertBackwardsStableError(I, shouldBeI1, 1);
        MatrixTest.assertBackwardsStableError(I, shouldBeI2, 3);
        
        final Matrix Apinv = LS.pinv(A);
        MatrixTest.assertMatrixEquals(Ainv, Apinv, 1e-10);
    }
    
    /** Test {@link LinearSolver#pinv(RealMatrix)} with a simple tall matrix */
    @Test
    void testPinvSimpleTallReal() {
        final double[][] arrA = { { 1, 0, 0 },
                                  { 0, 2, 0 },
                                  { 0, 0, 3 },
                                  { 0, 0, 4 } };
        
        final Matrix A     = new Matrix(arrA);
        final Matrix pinvA = LS.pinv(A);
        
        // agrees with MATLAB:
        final String expected = String.join(System.lineSeparator(),
                                            " 1 0.0 0.0  0.0 ",
                                            " 0 0.5 0.0  0.0 ",
                                            " 0 0.0 0.12 0.16");
        TestUtils.testMultiLineToString(expected, pinvA);
        testPseudoinverseReal(A);
    }
    
    /** Test {@link LinearSolver#pinv(RealMatrix)} with a few random tall matrices */
    @Test
    void testPinvRandomTallReals() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            final Matrix A = MatrixTest.randomMatrix(n, n / 2, 1.0, 123L);
            testPseudoinverseReal(A);
        }
    }
    
    /** Test {@link LinearSolver#pinv(RealMatrix)} with a simple wide matrix */
    @Test
    void testPinvSimpleWideReal() {
        final double[][] arrA = { { 1, 0, 0, 0 },
                                  { 0, 2, 0, 0 },
                                  { 0, 0, 3, 4 } };
        
        final Matrix A     = new Matrix(arrA);
        final Matrix pinvA = LS.pinv(A);
        
        // agrees with MATLAB:
        final String expected = String.join(System.lineSeparator(),
                                            " 1 0.0 0.0 ",
                                            " 0 0.5 0.0 ",
                                            " 0 0.0 0.12",
                                            " 0 0.0 0.16");
        TestUtils.testMultiLineToString(expected, pinvA);
        testPseudoinverseReal(A);
    }
    
    /** Test {@link LinearSolver#pinv(RealMatrix)} with a few random wide matrices */
    @Test
    void testPinvRandomWideReal() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            final Matrix A = MatrixTest.randomMatrix(n, n / 2, 1.0, 123L);
            testPseudoinverseReal(A);
        }
    }
    
    /** Show that pseudoinverses calculated using {@link LinearSolver#pinv(RealMatrix)} satisfy the <em>Moore-Penrose
     * Conditions</em>:
     * 
     * <ol>
     * <li><code> AA<sup>+</sup>A = A </code></li>
     * <li><code> A<sup>+</sup>AA<sup>+</sup> = A<sup>+</sup> </code></li>
     * <li>AA<sup>+</sup> is Hermitian: AA<sup>+</sup> = (AA<sup>+</sup>)<sup>H</sup></li>
     * <li>A<sup>+</sup>A is Hermitian: A<sup>+</sup>A = (A<sup>+</sup>A)<sup>H</sup></li>
     * </ol>
     * 
     * Here, all matrices are real, so we only need to check that the matrices are symmetric rather than Hermitian
     * 
     * @param A the matrix to check */
    static void testPseudoinverseReal(final Matrix A) {
        final Matrix Ap = LS.pinv(A);
        
        final Matrix AAp = A.times(Ap);
        final Matrix ApA = Ap.times(A);
        
        MatrixTest.assertBackwardsStableError(AAp, AAp.transpose(), 3); // 3rd condition
        MatrixTest.assertBackwardsStableError(ApA, ApA.transpose(), 2); // 4th condition
        
        final Matrix AApA  = AAp.times(A);
        final Matrix ApAAp = ApA.times(Ap);
        
        MatrixTest.assertBackwardsStableError(A,  AApA,  1); // 1st condition
        MatrixTest.assertBackwardsStableError(Ap, ApAAp, 1); // 2nd condition
    }
    
    // ======================================== complex inverse ========================================
    
    /** Test {@link LinearSolver#inv(ComplexMatrix)} with a simple matrix */
    @Test
    void testInvSimpleComplex() {
        final double[][] arrA = { { 1, 0, 0, 0 },
                                  { 0, 2, 0, 0 },
                                  { 0, 0, 3, 0 },
                                  { 0, 0, 0, 4 } };
        
        final ComplexMatrix A = new ComplexMatrix(arrA);
        final ComplexMatrix invA = LS.inv(A);
        
        final String expected = String.join(System.lineSeparator(),
                                            " 1 0.0 0.0                0.0 ",
                                            " 0 0.5 0.0                0.0 ",
                                            " 0 0.0 0.3333333333333333 0.0 ",
                                            " 0 0.0 0.0                0.25");
        TestUtils.testMultiLineToString(expected, invA);
        
        assertEquals(ComplexMatrix.identity(4), A.times(invA));
        assertEquals(ComplexMatrix.identity(4), invA.times(A));
        
        // test exception on wide or tall
        assertThrows(MathIllegalArgumentException.class,
                     () -> LS.inv(MatrixTest.randomMatrix(3, 4, 1.0, 1L)));
        assertThrows(MathIllegalArgumentException.class,
                     () -> LS.inv(MatrixTest.randomMatrix(4, 3, 1.0, 1L)));
    }
    
    /** Test {@link LinearSolver#inv(ComplexMatrix)} with a few random matrices */
    @Test
    void testInvRandomComplex() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n, 123L);
            testInverseComplex(A);
        }
    }
    
    /** Show that inverses calculated using {@link LinearSolver#inv(ComplexMatrix)} are indeed inverses, within absolute
     * tolerance.  Also test {@link LinearSolver#pinv(ComplexMatrix)} with square matrices.
     * 
     * @param A the matrix to check */
    static void testInverseComplex(final ComplexMatrix A) {
        final ComplexMatrix Ainv = LS.inv(A);
        
        final ComplexMatrix shouldBeI1 = A.times(Ainv);
        final ComplexMatrix shouldBeI2 = Ainv.times(A);
        
        final ComplexMatrix I = ComplexMatrix.identity(A.getColumnDimension());
        
        ComplexMatrixTest.assertBackwardsStableError(I, shouldBeI1, 1);
        ComplexMatrixTest.assertBackwardsStableError(I, shouldBeI2, 1);
        
        final ComplexMatrix Apinv = LS.pinv(A);
        ComplexMatrixTest.assertAbsEquals(Ainv, Apinv, 1e-10, 1e-10);
    }
    
    /** Test {@link LinearSolver#pinv(ComplexMatrix)} with a simple tall matrix */
    @Test
    void testPinvSimpleTallComplex() {
        final double[][] arrA = { { 1, 0, 0 },
                                  { 0, 2, 0 },
                                  { 0, 0, 3 },
                                  { 0, 0, 4 } };
        
        final ComplexMatrix A     = new ComplexMatrix(arrA);
        final ComplexMatrix pinvA = LS.pinv(A);
        
        // agrees with MATLAB:
        final String expected = String.join(System.lineSeparator(),
                                            " 1 0.0 0.0  0.0 ",
                                            " 0 0.5 0.0  0.0 ",
                                            " 0 0.0 0.12 0.16");
        TestUtils.testMultiLineToString(expected, pinvA);
        testPseudoinverseComplex(A);
    }
    
    /** Test {@link LinearSolver#pinv(ComplexMatrix)} with a few random tall matrices */
    @Test
    void testPinvRandomTallComplex() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n / 2, 123L);
            testPseudoinverseComplex(A);
        }
    }
    
    /** Test {@link LinearSolver#pinv(ComplexMatrix)} with a simple wide matrix */
    @Test
    void testPinvSimpleWideComplex() {
        final double[][] arrA = { { 1, 0, 0, 0 },
                                  { 0, 2, 0, 0 },
                                  { 0, 0, 3, 4 } };
        
        final ComplexMatrix A     = new ComplexMatrix(arrA);
        final ComplexMatrix pinvA = LS.pinv(A);
        
        // agrees with MATLAB:
        final String expected = String.join(System.lineSeparator(),
                                            " 1 0.0 0.0 ",
                                            " 0 0.5 0.0 ",
                                            " 0 0.0 0.12",
                                            " 0 0.0 0.16");
        TestUtils.testMultiLineToString(expected, pinvA);
        testPseudoinverseComplex(A);
    }
    
    /** Test {@link LinearSolver#pinv(ComplexMatrix)} with a few random wide matrices */
    @Test
    void testPinvRandomWideComplex() {
        for (int i = 0; i < DIMENSIONS.length; i++) {
            final int n = DIMENSIONS[i];
            final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(n, n / 2, 123L);
            testPseudoinverseComplex(A);
        }
    }
    
    /** Show that pseudoinverses calculated using {@link LinearSolver#pinv(ComplexMatrix)} satisfy the <em>Moore-Penrose
     * Conditions</em>:
     * 
     * <ol>
     * <li><code> AA<sup>+</sup>A = A </code></li>
     * <li><code> A<sup>+</sup>AA<sup>+</sup> = A<sup>+</sup> </code></li>
     * <li>AA<sup>+</sup> is Hermitian: AA<sup>+</sup> = (AA<sup>+</sup>)<sup>H</sup></li>
     * <li>A<sup>+</sup>A is Hermitian: A<sup>+</sup>A = (A<sup>+</sup>A)<sup>H</sup></li>
     * </ol>
     * 
     * @param A the matrix to check */
    static void testPseudoinverseComplex(final ComplexMatrix A) {
        final ComplexMatrix Ap = LS.pinv(A);
        
        final ComplexMatrix AAp = A.times(Ap);
        final ComplexMatrix ApA = Ap.times(A);
        
        ComplexMatrixTest.assertBackwardsStableError(AAp, AAp.conjugateTranspose(), 3); // 3rd condition
        ComplexMatrixTest.assertBackwardsStableError(ApA, ApA.conjugateTranspose(), 2); // 4th condition
        
        final ComplexMatrix AApA  = AAp.times(A);
        final ComplexMatrix ApAAp = ApA.times(Ap);
        
        ComplexMatrixTest.assertBackwardsStableError(A,  AApA,  1); // 1st condition
        ComplexMatrixTest.assertBackwardsStableError(Ap, ApAAp, 1); // 2nd condition
    }
    
    /** Run a performance test
     * 
     * @param args ignored */
    public static void main(final String[] args) {
        final boolean runRealValued = false;
        
        // final int increment     = 5;
        // final int minMatrixDim  = 5;
        // final int maxMatrixDim  = 50;
        // final int numWarmupRuns = 1_000;
        // final int numRuns       = 2_500;
        
        final int increment     = 100;
        final int minMatrixDim  = 100;
        final int maxMatrixDim  = 1_000;
        final int numWarmupRuns = 5;
        final int numRuns       = 10;
        
        System.out.println("Warm-up");
        linearSolvePerfRun(increment, minMatrixDim, maxMatrixDim, numWarmupRuns, runRealValued);
        
        System.out.println();
        System.out.println();
        System.out.println("Test");
        linearSolvePerfRun(increment, minMatrixDim, maxMatrixDim, numRuns, runRealValued);
    }
    
    /** Test the performance of the real or complex version of {@link LinearSolver#solveSquareBackSubstitution} for a
     * variety of matrix sizes.<br>
     * <br>
     * For each matrix dimension in the specified range, we take a square submatrix of that size from the 1000x1000
     * example, and solve a random back-substitution problem using that matrix, multiple times using multiple random
     * seeds. We print the total run-time for each matrix dimension to the console.<br>
     * <br>
     * 
     * The {@code (1000 x 1000)} upper-triangular matrix here was generated using MATLAB to compute the QR decomposition
     * of a random {@code (1000 x 1000)} {@link ComplexMatrix} itself generated using {@link ComplexMatrixTest}:
     * 
     * <pre>
     * final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(1000, 1000, 1.0, 8675_309L, false);
     * System.out.println(Numerics.toMATLABCode(A));
     * </pre>
     * 
     * and then in MATLAB, past the giant matrix into a script and run this:
     * 
     * <pre>
     * [~, R] = qr(A);
     * writematrix(R);
     * </pre>
     * 
     * @param increment when iterating through matrices of various sizes, we increment the size by this much
     * @param minMatrixDim the smallest matrix dimension we'll try
     * @param maxMatrixDim the largest matrix dimension we'll try, unless {@code increment} does not evenly divide
     *        {@code maxMatrixDim - minMatrixDim}, in which case we'll stop short
     * @param numSeeds the number of random {@link NVector}s to test.
     * @param runRealValued if {@code true}, we only look at matrices with real valued entries. If {@code false}, we
     *        look at matrices with {@link Complex}-valued entries. */
    static void linearSolvePerfRun(final int increment,
                                   final int minMatrixDim,
                                   final int maxMatrixDim,
                                   final int numSeeds,
                                   final boolean runRealValued) {
        final long   seedSeed = 8675_309L;
        final long[] seeds    = CountableDoubleIterable.monteCarlo(1.0, 1_000_000.0, seedSeed, numSeeds)
                                                       .stream()
                                                       .map(scale -> scale * seedSeed)
                                                       .mapToLong(FastMath::round)
                                                       .distinct()
                                                       .toArray();
        
        final ComplexMatrix bigT = NumericsTest.parseMatrixCSV("../../linear/UpperTri1000x1000.txt");
        
        final String fmt = "%n%4d: %14d Average: %12d ns,   %.12f s";
        
        final LinearSolver defaultLS = LinearSolver.defaultSolver();
        
        if (runRealValued) {
            final Matrix projectedBigT = bigT.asReal(Double.POSITIVE_INFINITY).orElseThrow();
            for (int matrixDim = minMatrixDim; matrixDim <= maxMatrixDim; matrixDim += increment) {
                
                final Matrix T = new Matrix(projectedBigT.getSubMatrix(0, matrixDim - 1, 0, matrixDim - 1), false);
                
                /* We don't want the compiler to figure out that we're only measuring performance, so we have to make
                 * use of the return value; thus, simply sum the hashCodes */
                long sumHash  = 0L;
                long sumNanos = 0L;
                for (final long seed : seeds) {
                    final Matrix B = MatrixTest.randomMatrix(matrixDim, matrixDim, 1.0, seed * seed);
                    
                    final long   before = System.nanoTime();
                    final Matrix X      = defaultLS.solveSquareBackSubstitution(T, B);
                    final long   after  = System.nanoTime();
                    final long   nanos  = after - before;
                    
                    sumHash  += X.hashCode();
                    sumNanos += nanos;
                }
                
                System.out.format(fmt, matrixDim, sumHash, (1e-9 * sumNanos) / numSeeds);
            }
        }
        else {
            for (int matrixDim = minMatrixDim; matrixDim <= maxMatrixDim; matrixDim += increment) {
                
                final ComplexMatrix T = bigT.getSubMatrix(0, matrixDim - 1, 0, matrixDim - 1);
                
                /* We don't want the compiler to figure out that we're only measuring performance, so we have to make
                 * use of the return value; thus, simply sum the hashCodes */
                long sumHash  = 0L;
                long sumNanos = 0L;
                for (final long seed : seeds) {
                    final ComplexMatrix B =
                            ComplexMatrixTest.randomComplexMatrix(matrixDim, matrixDim, 1.0, seed * seed, false);
                    
                    final long          before = System.nanoTime();
                    final ComplexMatrix X      = defaultLS.solveSquareBackSubstitution(T, B);
                    final long          after  = System.nanoTime();
                    final long          nanos  = (after - before);
                    
                    sumHash  += X.hashCode();
                    sumNanos += nanos;
                }
                
                System.out.format(fmt, matrixDim, sumHash, sumNanos / numSeeds, (1e-9 * sumNanos) / numSeeds);
            }
        }
    }
}
