/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;
import static ptolemaeus.commons.TestUtils.assertLessThan;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.linear.AnyMatrix;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.collections.SetUtils;
import ptolemaeus.math.CountableDoubleIterable;
import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.LinearSolver;
import ptolemaeus.math.linear.MatrixVisualizer;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;
import ptolemaeus.math.linear.eigen.EigenTest;
import ptolemaeus.math.linear.householderreductions.ComplexQRDecompositionTest;
import ptolemaeus.math.linear.real.Matrix;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** Test {@link ComplexSchurDecomposition}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class ComplexSchurDecompositionTest {
    
    /** Test {@link ComplexSchurDecomposition#adjustComplexEigenvector(ComplexVector)} */
    @Test
    void testAdjustComplexEigenvector() {
        final ComplexVector v = new ComplexVector(new double[] { 1, 2,  10, 3, 4 },
                                                  new double[] { 5, 6, -11, 7, 8 }).normalizedHermitian();
        
        final ComplexVector scaled = ComplexSchurDecomposition.adjustComplexEigenvector(v);
        
        assertEquals(1.0, scaled.hermitianNorm(), Numerics.MACHINE_EPSILON); // check normalized
        assertEquals(0.0, v.hermitianAngleTo(scaled));                       // check unchanged direction
        assertTrue(scaled.getEntry(2).isReal());                             // check expected real component
    }
    
    /** Test {@link ComplexSchurDecomposition#possiblyPerturb} */
    @Test
    void testPossiblyPerturb() {
        final double  eps = 10 * Numerics.MACHINE_EPSILON;
        final Complex z1  = Complex.valueOf(5, 7);
        final Complex z2  = Complex.valueOf(5, 7 + eps);
        final Complex z3  = Complex.valueOf(5, 7 + 2 * eps);
        
        assertSame(z1, ComplexSchurDecomposition.possiblyPerturb(z1, SetUtils.of(), eps));
        
        // z2 is too close to z1, so we perturb it
        final Complex perturbed = ComplexSchurDecomposition.possiblyPerturb(z1, SetUtils.of(z2), eps);
        assertEquals(z1.add(eps), perturbed);
        
        // z3 is not close enough to be perturbed
        assertSame(z1, ComplexSchurDecomposition.possiblyPerturb(z1, SetUtils.of(z3), eps));
        
        // just to test with a set with > 1 elements
        final Complex perturbed2 = ComplexSchurDecomposition.possiblyPerturb(z1, SetUtils.of(z2, z3), eps);
        assertEquals(z1.add(eps), perturbed2);
    }
    
    /** Test {@link ComplexSchurDecomposition#constructInitGuess} */
    @Test
    void testConstructInitGuess() {
        final int    n       = 10;
        final int    attempt = 2;
        final double eps     = Numerics.MACHINE_EPSILON;
        final double rootn   = FastMath.sqrt(10);
        final double rtemp   = eps / (rootn + 1.0);
        
        final ComplexVector guess = ComplexSchurDecomposition.constructInitGuess(n, attempt, eps, rtemp, rootn);
        
        /* Note that the first and 3rd to last - i.e., the (n-2)^th - are different: the first element is always set
         * separately from the test, and then the element at the n-attempt# spot is modified. */
        final String expected = String.join(System.lineSeparator(),
                                            "  2.2204460492503130e-16 ",
                                            "  5.3346898754478756e-17 ",
                                            "  5.3346898754478756e-17 ",
                                            "  5.3346898754478756e-17 ",
                                            "  5.3346898754478756e-17 ",
                                            "  5.3346898754478756e-17 ",
                                            "  5.3346898754478756e-17 ",
                                            " -6.4881979496086150e-16 ",
                                            "  5.3346898754478756e-17 ",
                                            "  5.3346898754478756e-17 ");
        TestUtils.testMultiLineToString(expected, guess.toColumnString());
    }
    
    /** TODO Ryan Moser: 2025-01-27: FIX <br>
     * <br>
     * 
     * Test a problem case discovered while computing the shifts of a sparse {@link ComplexMatrix}. The original larger
     * matrix was generated by the following:<br>
     * <br>
     * 
     * <pre>
     * <code>
     * final int dim = 192;
     * final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(dim, dim, dim * 1_000.0, 0.002, 8675_309L, true);
     * </code>
     * </pre>
     */
    @Test
    void testProblemCase() {
        final double[][] AData = new double[][] {
            {   0.0000000000000000e+00,   5.0729501667081840e-12,   0.0000000000000000e+00,   3.6278519937256910e-27  },
            {  -7.6242359885456005e-28,   0.0000000000000000e+00,  -3.1784324609441165e-12,   0.0000000000000000e+00  },
            {   0.0000000000000000e+00,  -4.7862048044575320e-28,   0.0000000000000000e+00,   2.9392417610279276e-12  },
            {   0.0000000000000000e+00,   0.0000000000000000e+00,  -1.1255149991843387e-27,   0.0000000000000000e+00  }
        };
        
        final ComplexMatrix A = new ComplexMatrix(AData);
        
        assertThrows(NumericalMethodsException.class, () -> ComplexSchurDecomposition.of(A));
    }
    
    /** Test computing Eigenvalues and Eigenvectors for a real symmetric matrix that (originally) for which we were
     * finding complex Eigenvectors. The Eigenvectors weren't incorrect, but in the real-symmetric case we know that we
     * can find real Eigenvectors, so we should. */
    @Test
    void testProblematicSymmetric() {
        final double[][] symmData = // a problematic symmetric matrix
            { {  0.3333333333333329,     -3.3042351923367754E-17,  2.1807952269422717E-17 },
              { -3.3042351923367754E-17,  0.3333333333333333,     -2.64338815386942E-18   },
              {  2.1807952269422717E-17, -2.64338815386942E-18,    0.33333333333333337    } };
        
        final ComplexMatrix A = new ComplexMatrix(symmData);
        assertTrue(A.isHermitian(Complex.ZERO)); // sanity check
        
        testEigenvectors(A);
        
        final ComplexSchurDecomposition schur = ComplexSchurDecomposition.of(A, SchurMode.SCHUR_FORM);
        final ComplexMatrix V = schur.computeEigenvectors(A, 5, 0.0,
                                                          LinearSolver.defaultSolver(),
                                                          EigenvectorFailureMode.THROW).orElseThrow();
        
        assertTrue(V.isReal(0.0));
        
        final ComplexMatrix AV         = A.times(V);
        final ComplexMatrix shouldBeAV = V.times(ComplexMatrix.diagonal(schur.getEigenvaluesArr()));
        
        assertLessThan(AV.minus(shouldBeAV).getFrobeniusNorm(), 1e-8, "norm2(AV - VL)");
    }
    
    /** Test {@link ComplexSchurDecomposition} with a (64 x 64) {@link ComplexMatrix#isHermitian(Complex) Hermitian}
     * matrix */
    @Test
    void testHermitian64x64() {
        final ComplexMatrix A = testMATLABComparison("hermitian64x64", 5);
        assertTrue(A.isHermitian(Complex.ZERO)); // sanity check
    }
    
    /** Run the {@link ComplexSchurDecomposition} on a matrix with the structure
     * 
     * <pre>
     * 0  a  0
     * a  0  a
     * 0  a  0
     * </pre>
     * 
     * Where {@code a}s simply represent non-zero complex elements (not necessarily equal).<br>
     * <br>
     * 
     * This matrix in particular was found during the decomposition of a much larger (512x512) matrix when computing the
     * shift values for a QR step.<br>
     * <br>
     * 
     * Having all zeros on the main diagonal makes the matrix "hollow" and this checkered pattern presents a unique
     * challenge to the deflation, as the basic checks will never allow this to be deflated. */
    @Test
    void testHollowComplex() {
        
        final Complex[][] AData = new Complex[][] {
            { new Complex(0.0, 0.0),
              new Complex(0.5139811414537816, 2.138921190703253),
              new Complex(0.0, 0.0) },
            { new Complex(2.5062790040204006, -1.6196954774559735),
              new Complex(0.0, 0.0),
              new Complex(0.691779421545304, -2.1261827934027697) },
            { new Complex(0.0, 0.0),
              new Complex(-3.533919344672208, -3.626477863887958),
              new Complex(0.0, 0.0) } };
        
        final ComplexMatrix             A     = new ComplexMatrix(AData);
        final ComplexSchurDecomposition schur = ComplexSchurDecomposition.of(A);
        testSchurDecomposition(A, schur);
    }
    
    /** Run the {@link ComplexSchurDecomposition} on a matrix with the structure
     * 
     * <pre>
     * 0  a  0
     * a  0  a
     * 0  a  0
     * </pre>
     * 
     * Where {@code a}s simply represent non-zero complex elements (not necessarily equal).<br>
     * <br>
     * 
     * The matrix used here was created by simply removing the imaginary components of the matrix in
     * {@link #testHollowComplex()}.<br>
     * <br>
     * 
     * Having all zeros on the main diagonal makes the matrix "hollow" and this checkered pattern presents a unique
     * challenge to the deflation, as the basic checks will never allow this to be deflated. */
    @Test
    void testHollowReal() {
        final double[][] AData = new double[][] { { 0,                   0.5139811414537816, 0 },
                                                  { 2.5062790040204006,  0,                  0.691779421545304 },
                                                  { 0,                  -3.533919344672208,  0 } };
        
        final ComplexMatrix             A     = new ComplexMatrix(AData);
        final ComplexSchurDecomposition schur = ComplexSchurDecomposition.of(A);
        testSchurDecomposition(A, schur, 2);
    }
    
    /** Test the basics of {@link ComplexSchurDecomposition} */
    @Test
    void testBasics() {
        final ComplexMatrix Q           = ComplexMatrixTest.randomComplexMatrix(5, 5, 1.0, 1L, false);
        final ComplexMatrix U           = ComplexMatrixTest.randomComplexMatrix(5, 5, 1.0, 1L, false);
        final ComplexVector eigenvalues = new ComplexVector(new Complex[] { Complex.ZERO,
                                                          Complex.ZERO,
                                                          Complex.ZERO,
                                                          Complex.ZERO,
                                                          Complex.ZERO });
        
        final ComplexSchurDecomposition schur1 =
                new ComplexSchurDecomposition(SchurMode.BLOCK_SCHUR_FORM, Q, U, eigenvalues);
        final ComplexSchurDecomposition schur2 =
                new ComplexSchurDecomposition(SchurMode.EIGENVALUES, null, null, eigenvalues);
        
        assertEquals(Q, schur1.getQ().orElseThrow());
        assertEquals(U, schur1.getU().orElseThrow());
        assertEquals(SchurMode.BLOCK_SCHUR_FORM, schur1.getSchurMode());

        final double[][] expected = new ComplexVector(eigenvalues).getDataRef();
        final double[][] actual = schur1.getEigenvalues().getDataRef();
        assertArrayEquality(expected[0], actual[0], 0.00001);
        
        assertFalse(schur2.getQ().isPresent());
        assertFalse(schur2.getU().isPresent());
    }
    
    /** Test computing the {@link ComplexSchurDecomposition} of a zero-matrix */
    @Test
    void testZero() {
        final ComplexMatrix             zero  = ComplexMatrix.zero(10, 10);
        final ComplexSchurDecomposition schur = ComplexSchurDecomposition.of(zero);
        
        testSchurDecomposition(zero, schur);
        testEigenvectors(zero);
    }
    
    /** Test {@link ComplexSchurDecomposition#chooseMaxIters(AnyMatrix)} */
    @Test
    void testChooseMaxIters() {
        final int dim = 29;
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(dim, dim, 1.0, 1L, false);
        
        assertEquals(29 * 30, ComplexSchurDecomposition.chooseMaxIters(A));
    }
    
    /** Test {@link ComplexSchurDecomposition#compute2x2ClosedForm(ComplexMatrix, boolean, SchurMode, boolean)} */
    @Test
    void testCompute2x2ClosedFormReal1() {
        final ComplexMatrix             A2x2  = new ComplexMatrix(new double[][] { { 1, 1 }, { -2, 3 } });
        final ComplexSchurDecomposition schur =
                ComplexSchurDecomposition.compute2x2ClosedForm(A2x2, false, SchurMode.SCHUR_FORM, false);
        
        testSchurDecomposition(A2x2, schur);
    }
    
    /** Test {@link ComplexSchurDecomposition#compute2x2ClosedForm(ComplexMatrix, boolean, SchurMode, boolean)} */
    @Test
    void testCompute2x2ClosedFormReal2() {
        final ComplexMatrix             A2x2  = new ComplexMatrix(new double[][] { { 7, -2 }, { 12, -3 } });
        final ComplexSchurDecomposition schur =
                ComplexSchurDecomposition.compute2x2ClosedForm(A2x2, false, SchurMode.SCHUR_FORM, false);
        
        testSchurDecomposition(A2x2, schur);
    }
    
    /** Test {@link ComplexSchurDecomposition#compute2x2ClosedForm(ComplexMatrix, boolean, SchurMode, boolean)} */
    @Test
    void testCompute2x2ClosedFormComplex1() {
        final Complex[][]   QData = new Complex[][] { { new Complex(1, 0), new Complex(-3, 0) },
                                                      { new Complex(3, 0), new Complex(1, 0) } };
        final ComplexMatrix Q     = new ComplexMatrix(QData);
        
        final Complex[][]   UData = new Complex[][] { { new Complex(1, 2), new Complex(3, 4) },
                                                      { new Complex(0, 0), new Complex(5, 6) } };
        final ComplexMatrix U     = new ComplexMatrix(UData);
        
        final ComplexMatrix A = U.times(Q).conjugateTransposeMultiply(U);
        final ComplexSchurDecomposition schur =
                ComplexSchurDecomposition.compute2x2ClosedForm(A, false, SchurMode.SCHUR_FORM, false);
        
        testSchurDecomposition(A, schur);
    }
    
    /** Test {@link ComplexSchurDecomposition#compute2x2ClosedForm(ComplexMatrix, boolean, SchurMode, boolean)} */
    @Test
    void testCompute2x2ClosedFormComplex2() {
        final Complex[][]   AData = new Complex[][] { { new Complex(-8.3425967178491590e+01, -4.2655032797871506e+01),
                                                        new Complex(41.73187096920931, 44.12035116771716) },
                                                      { new Complex(1.8837991157521652e-16, -1.9898349311182917e-16),
                                                        new Complex(-14.435018020693034, -114.24364393495429) } };
        final ComplexMatrix A2x2 = new ComplexMatrix(AData);        
        
        final ComplexSchurDecomposition schur =
                ComplexSchurDecomposition.compute2x2ClosedForm(A2x2, false, SchurMode.SCHUR_FORM, false);
        
        testSchurDecomposition(A2x2, schur);
    }
    
    /** Test the matrix generated {@code gallery(5)} in MATLAB, which is very sensitive to perturbations. See the
     * section titled "Eigenvalues that are sensitive to round-off errors"
     * <a href="https://www.mathworks.com/help/matlab/ref/gallery.html#d126e562453"> here </a>
     * 
     * <br>
     * <br>
     * 
     * The matrix:
     * 
     * <pre>
     *    -9,    11,   -21,     63,   -252 
     *    70,   -69,   141,   -421,   1684 
     *  -575,   575, -1149,   3451, -13801 
     *  3891, -3891,  7782, -23345,  93365 
     *  1024, -1024,  2048,  -6144,  24572 
     * </pre>
     * 
     * <br>
     * <br>
     * 
     * In this test, we don't use {@link #testMATLABComparison(String, int)} because the Eigenvalues aren't equal, but
     * that's the point: the matrix that's passed in is extremely sensitive to perturbations, so small differences in
     * floating-point arithmetic result in seemingly problematically answers.
     * 
     * <br>
     * <br>
     * 
     * The actual Eigenvalues of this matrix are all zero (5 zeroes), but due to the extreme sensitivity, neither
     * {@link ComplexSchurDecomposition} not MATLAB finds them. However, they both indeed find that the average of the
     * Eigenvalues is very nearly zero, which is a good sign, and this is a typical way of studying very sensitive
     * systems. */
    @Test
    void testGallery5() {
        final MATLABSchurIO         matlabSchurOutput = MATLABSchurIO.fromFolder("gallery5");
        final ComplexMatrix             A                 = matlabSchurOutput.A().copy();
        final SchurMode                 mode              = SchurMode.SCHUR_FORM;
        final ComplexSchurDecomposition ptSchur           = ComplexSchurDecomposition.of(A, SchurMode.SCHUR_FORM);
        
        testSchurDecomposition(A, ptSchur);
        
        final ComplexVector mlEigenvalues = ComplexMatrix.extractDiagonalVector(matlabSchurOutput.eigenvalues());
        final ComplexSchurDecomposition mlSchur = new ComplexSchurDecomposition(mode,
                                                                                matlabSchurOutput.schurU(),
                                                                                matlabSchurOutput.schurT(),
                                                                                mlEigenvalues);
        
        final Complex ptAvg = Stream.of(ptSchur.getEigenvaluesArr())
                                    .reduce(Complex::add)
                                    .orElseThrow()
                                    .divide(ptSchur.getEigenvaluesArr().length);
        
        final Complex mlAvg = Stream.of(mlSchur.getEigenvaluesArr())
                                    .reduce(Complex::add)
                                    .orElseThrow()
                                    .divide(mlSchur.getEigenvaluesArr().length);
        
        assertEquals(0.0, ptAvg.getReal(),      1e-12);
        assertEquals(0.0, ptAvg.getImaginary(), 0.0);
        
        assertEquals(0.0, mlAvg.getReal(),      1e-12);
        assertEquals(0.0, mlAvg.getImaginary(), 0.0);
    }
    
    /** Test calculation of the Schur decomposition and Eigenvalues of a dense {@code 128x128} {@link ComplexMatrix
     * matrix with random complex entries}. To reduce runtime, the input matrix is already in auuper-Hessenberg form. */
    @Test
    void testDense128x128complex() {
        testMATLABComparison("dense128x128complex", 2);
    }
    
    /** Test calculation of the Schur decomposition and Eigenvalues of a {@code 128x128} {@link ComplexMatrix matrix
     * with random real entries}. To reduce runtime, the input matrix is already in auuper-Hessenberg form. */
    @Test
    void testDense128x128real() {
        testMATLABComparison("dense128x128real", 1);
    }
    
    /** Test calculation of the Schur decomposition and Eigenvalues of a {@code 10x10} {@link ComplexMatrix matrix with
     * real entries} generated (in MATLAB) using {@code gallery('frank', 10, 0)}. {@code frank} matrices have
     * ill-conditioned eigenvalues and, thus, finding the same ones as MATLAB is a good indicator of a successful
     * implementation.
     * 
     * <br>
     * <br>
     * 
     * Search for {@code frank} <a href="https://www.mathworks.com/help/matlab/ref/gallery.html#d126e562453"> here </a>
     * 
     * <br>
     * <br>
     * 
     * The backward error here is twice what MATLAB achieved, but the error in MATLAB is ~{@code 1e-13}, so the error
     * here is ~{@code 2e-13}, which is essentially the same in terms of the norm of the input matrix. */
    @Test
    void testFrank() {
        testMATLABComparison("frank", 2);
    }
    
    /** Load the MATLAB Schur decomposition that's stored in the given test folder and compare it to what we generate in
     * {@link ComplexSchurDecomposition}. The data is loaded as a {@link MATLABSchurIO} and was created and saved
     * using the script in the JavaDoc of that {@code record}.
     * 
     * 
     * @param testFolderName the name of the test folder
     * @param errorMultiplier a value by which we multiply the MATLAB backward error when comparing it to the error our
     *        implementation achieved. Should almost always be {@code 1} and, in case it's not, the absolute error must
     *        be very small. E.g., see {@link #testFrank()} 
     * @return the {@link ComplexMatrix} {@code A} for which this is a test */
    private static ComplexMatrix testMATLABComparison(final String testFolderName, final int errorMultiplier) {
        final MATLABSchurIO matlabSchurOutput = MATLABSchurIO.fromFolder(testFolderName);
        
        final ComplexMatrix             A       = matlabSchurOutput.A();
        final SchurMode                 mode    = SchurMode.SCHUR_FORM;
        final ComplexSchurDecomposition ptSchur = ComplexSchurDecomposition.of(A, SchurMode.SCHUR_FORM);
        
        // first, be sure that the Schur decomposition is at least self-consistent
        testSchurDecomposition(A, ptSchur, errorMultiplier);
        testEigenvectors(A, ptSchur);
        
        final ComplexVector mlEigenvalues = ComplexMatrix.extractDiagonalVector(matlabSchurOutput.eigenvalues());
        final ComplexSchurDecomposition mlSchur = new ComplexSchurDecomposition(mode,
                                                                                matlabSchurOutput.schurU(),
                                                                                matlabSchurOutput.schurT(),
                                                                                mlEigenvalues);
        
        // show that the algorithm here results in at most as much error as the MATLAB result
        final double ptError  = ptSchur.computeQUQH().minus(A).getFrobeniusNorm();
        final double mlError  = mlSchur.computeQUQH().minus(A).getFrobeniusNorm();
        final double maxError = mlError * errorMultiplier;
        
        assertTrue(ptError <= maxError,
                   () -> "error was larger than the scaled MATLAB error: %e vs %e".formatted(ptError, maxError));
        
        assertNotSame(ptSchur.getEigenvaluesArr(), ptSchur.getEigenvaluesArr());
        assertArrayEquality(ptSchur.getEigenvaluesArr(), ptSchur.getEigenvaluesArr());
        
        // compare the computed Eigenvalues, accounting for the fact that conjugates and screw up the sort-by-norm
        final Complex[] sortedPtEigs = ptSchur.getEigenvaluesArr().clone();
        Arrays.sort(sortedPtEigs, Comparator.comparing(Complex::norm));
        
        final Complex[] sortedMLEigs = mlSchur.getEigenvaluesArr().clone();
        Arrays.sort(sortedMLEigs, Comparator.comparing(Complex::norm));
        
        for (int i = 0; i < sortedPtEigs.length; i++) {
            final Complex ptEig = sortedPtEigs[i];
            final Complex mlEig = sortedMLEigs[i];
            
            /* the complex conjugate Eigenvalues will have norms that are nearly identically equal, so sorting the
             * Eigenvalues becomes problematic. To get around this, we simply check that the Eigenvalues are either
             * equal to each other or are conjugates of each other (within tolerance). If either is true, we succeed. */
            final int finalI = i;
            final double absEps = mlEig.norm() * 1e-8; // arbitrary small number based on values in comparison
            if (!(Complex.equals(ptEig, mlEig, absEps) || Complex.equals(ptEig, mlEig.conjugate(), absEps))) {
                assertEquals(ptEig, mlEig,
                             () -> "Eigenvalues differed at index %d%n%s%n%s".formatted(finalI, ptEig, mlEig));
            }
        }
        
        return A;
    }
    
    /** Check the self-consistency of a {@link ComplexSchurDecomposition}. Calls
     * {@link #testSchurDecomposition(ComplexMatrix, ComplexSchurDecomposition, int)} with {@code c == 1}
     * 
     * @param A the {@link ComplexMatrix} that was decomposed
     * @param schur the {@link ComplexSchurDecomposition} of {@code A} */
    static void testSchurDecomposition(final ComplexMatrix A, final ComplexSchurDecomposition schur) {
        testSchurDecomposition(A, schur, 1);
    }
    
    /** Check the self-consistency of a {@link ComplexSchurDecomposition}
     * 
     * @param A the {@link ComplexMatrix} that was decomposed
     * @param schur the {@link ComplexSchurDecomposition} of {@code A}
     * @param c a "small constant" by which we will multiply the acceptable error. Keep this as small as possible. This
     *        value is to help simulate that the error bounds are expressed in big-O notation. */
    static void testSchurDecomposition(final ComplexMatrix A, final ComplexSchurDecomposition schur, final int c) {
        final ComplexMatrix Q         = schur.getQ().orElseThrow();
        final ComplexMatrix U         = schur.getU().orElseThrow();
        final ComplexMatrix shouldBeA = schur.computeQUQH();
        
        /* divide by sqrt(2) because we're checking absolute error magnitude of complex numbers separately, and we want
         * the total error to be less than frob * machEps. It's not perfect, but it's close. */
        final double unitaryErrorBound = c * A.getFrobeniusNorm()
                                           * Numerics.MACHINE_EPSILON
                                           / FastMath.sqrt(2.0);
        ComplexQRDecompositionTest.assertUnitary(Q, unitaryErrorBound, unitaryErrorBound);
        ComplexMatrixTest.assertBackwardsStableError(A, shouldBeA, c);
        
        final ComplexMatrix UAgain = Q.conjugateTransposeMultiply(A).times(Q); // this is how U is computed internally
        assertEquals(UAgain, U);
        
        /* the following doesn't work well as a test due to how we must compute U using Q and A, and U ends up not
         * always being perfectly upper-triangular, even within small tolerance. This is not particularly concerning to
         * me, as it's standard behavior, and the real purpose of the Schur decomposition is to compute the Eigenvalues
         * of the input matrix.
         * 
         * final double upperTriErrorBound = 1.0;
         * ComplexQRDecompositionTest.assertIsUpperTriangular(U, upperTriErrorBound, upperTriErrorBound); */
    }
    
    /** Test {@link ComplexSchurDecomposition} for trivial 1x1 a 2x2 matrices
     * 
     * @see QRShiftsTest#testComputePrinciple2x2Eigen() */
    @Test
    void testClosedFormFullSolve() {
        final ComplexMatrix A1x1 = new ComplexMatrix(new Matrix(new double[][] { { 1 } }));
        final ComplexMatrix A2x2 = new ComplexMatrix(new Matrix(new double[][] { { 1, 0 }, { 0, 1 } }));
        
        final ComplexSchurDecomposition e1 = ComplexSchurDecomposition.of(A1x1, SchurMode.SCHUR_FORM);
        final ComplexSchurDecomposition e2 = ComplexSchurDecomposition.of(A2x2, SchurMode.SCHUR_FORM);
        
        assertEquals(e1.getU().orElseThrow(), ComplexMatrix.identity(1));
        assertEquals(e2.getU().orElseThrow(), ComplexMatrix.identity(2));
    }
    
    /** Test the exceptional case of {@link ComplexSchurDecomposition#computeEigenvectors} */
    @Test
    void testExceptionalEigVec() {
        final ComplexSchurDecomposition notSchurForm =
                new ComplexSchurDecomposition(SchurMode.BLOCK_SCHUR_FORM, null, null, null);
        
        assertTrue(notSchurForm.computeEigenvectors(null, 5, 0.0, LinearSolver.defaultSolver(),
                                                    EigenvectorFailureMode.AS_IS).isEmpty());
    }
    
    /** Test {@link ComplexSchurDecomposition#computeEigenvectors} using
     * {@link #testEigenvectors(ComplexMatrix)} with a few {@link ComplexMatrix} */
    @Test
    void testEigenvectorsComplex() {
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(2,   2,     100.0, 8675309L, false));
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(8,   8,    1000.0, 8675309L, false));
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(16, 16,  100000.0, 8675309L, false));
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(64, 64, 1000000.0, 8675309L, false));
    }
    
    /** Test {@link ComplexSchurDecomposition#computeEigenvectors} using
     * {@link #testEigenvectors(ComplexMatrix)} with a few real matrices wrapped in {@link ComplexMatrix complex
     * matrices} */
    @Test
    void testEigenvectorsReal() {
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(2,   2,     100.0, 8675309L, true));
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(8,   8,    1000.0, 8675309L, true));
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(16, 16,  100000.0, 8675309L, true));
        testEigenvectors(ComplexMatrixTest.randomComplexMatrix(64, 64, 1000000.0, 8675309L, true));
    }
    
    /** Test {@link ComplexSchurDecomposition#computeEigenvectors}
     * @param A the {@link ComplexMatrix} to decompose */
    static void testEigenvectors(final ComplexMatrix A) {
        final ComplexSchurDecomposition schur = ComplexSchurDecomposition.of(A, SchurMode.SCHUR_FORM);
        testEigenvectors(A, schur);
    }
    
    /** Test {@link ComplexSchurDecomposition#computeEigenvectors}
     * @param A the {@link ComplexMatrix} to decompose 
     * @param schur the {@link ComplexSchurDecomposition} to test */
    static void testEigenvectors(final ComplexMatrix A, final ComplexSchurDecomposition schur) {
        final ComplexMatrix V = schur.computeEigenvectors(A, 10, 0.0,
                                                          LinearSolver.defaultSolver(),
                                                          EigenvectorFailureMode.THROW).orElseThrow();
        
        /* epsilon is an arbitrary constant, but is actually just larger than 3 sqrt(machEps). This value actually only
         * needs to be this large for one test case, but it's still very small, so I'm not concerned. */
        final double epsilon = 5.2e-8;
        for (int i = 0; i < A.getColumnDimension(); i++) {
            final ComplexVector shouldBeEigenvector = V.getColumnVector(i);
            if (!shouldBeEigenvector.isZero()) {
                assertEquals(1.0, shouldBeEigenvector.hermitianNorm(), epsilon); // assert normalized
            }
            
            final Complex eigenvalue = schur.getEigenvaluesArr()[i];
            EigenTest.assertEigenvector(A, eigenvalue, shouldBeEigenvector, epsilon);
        }
    }
    
    /** Test
     * {@link ComplexSchurDecomposition#getWarningLogMessage(ComplexMatrix, Complex, ComplexVector, double)}
     * with a candidate solution that has a NaN entries */
    @Test
    void testGetWarningLogMessage() {
        // the actual values don't much matter here, we just want to make sure the String comes back properly formatted
        final ComplexMatrix A                = ComplexMatrix.identity(5);
        final Complex       notAnEigenvalue  = Complex.ONE;
        final ComplexVector initGuess        = ComplexVector.zero(5);
        final double        epsilon          = 1e-12;
        
        final String actual = ComplexSchurDecomposition.getWarningLogMessage(A,
                                                                             notAnEigenvalue,
                                                                             initGuess,
                                                                             epsilon);
        
        final String expected = String.join(System.lineSeparator(),
                "Failed to compute an Eigenvector. Consider submitting a bug report with",
                "",
                "Complex Schur input-matrix:",
                "final double[][] AData = new double[][] {",
                "    {  1,  0,  0,  0,  0 },",
                "    {  0,  1,  0,  0,  0 },",
                "    {  0,  0,  1,  0,  0 },",
                "    {  0,  0,  0,  1,  0 },",
                "    {  0,  0,  0,  0,  1 }",
                "};",
                "final ComplexMatrix A = new ComplexMatrix(AData);",
                "",
                "Eigenvector guess:",
                "final double[][] initialGuessVecData = new double[][] {",
                "    {  0 },",
                "    {  0 },",
                "    {  0 },",
                "    {  0 },",
                "    {  0 }",
                "};",
                "final ComplexMatrix initialGuessVec = new ComplexMatrix(initialGuessVecData);",
                "",
                "Eigenvalue: (1.0, 0.0)",
                "",
                "realSymmAbsTol: 1.000000e-12",
                "");
        
        TestUtils.testMultiLineToString(expected, actual);
    }
    
    /** Visualize the process of computing a {@link ComplexSchurDecomposition} and print the sorted Eigenvalues to
     * stdout
     * 
     * @param A the {@link ComplexMatrix} to decompose */
    static void visualize(final ComplexMatrix A) {
        final SchurMode                       mode     = SchurMode.EIGENVALUES;
        final int                             maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
        final MatrixVisualizer<ComplexMatrix> mViz     = new MatrixVisualizer<>(A);
        
        final Complex[] eigenvalues = ComplexSchurDecomposition.of(A, mode, maxIters, mViz::add).getEigenvaluesArr();
        
        System.out.println(ComplexVectorTest.sortedAsVector(eigenvalues).toColumnString());
    }
    
    /** run one of {@link #main1()} or {@link #main2()}
     * 
     * @param args ignored */
    public static void main(final String[] args) {
        // main1();
        main2();
    }
    
    /** Run {@link ComplexSchurDecomposition} with {@link MatrixVisualizer} as the evaluation monitor */
    static void main1() {
        final int    dim     = 192;
        final double rowNorm = dim * 1_000.0;
        final double density = 0.005;
        final long   seed    = 8675_309L;
        
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(dim, dim, rowNorm, density, seed, false);
        
        visualize(A);
    }
    
    /** Run a performance test of the {@link ComplexSchurDecomposition} */
    @SuppressFBWarnings({ "RV", "Dm" })
    static void main2() {
        final int    numSeeds = 1000;
        final long   seedSeed = 8675_309L;
        final long[] seeds    = CountableDoubleIterable.monteCarlo(1.0, 1_000_000.0, seedSeed, numSeeds)
                                                       .stream()
                                                       .map(scale -> scale * seedSeed)
                                                       .mapToLong(FastMath::round)
                                                       .distinct()
                                                       .toArray();
        final int     matrixDim = 64;
        final boolean onlyReal  = false;
        
        /* We don't want the compiler to figure out that sometimes we're only measuring performance, so we have to make
         * use of return values to trick the compiler. To do so, we simply compute and sum the hashCodes of the
         * Eigenvalue vectors and (optionally) the Eigenvector matrices.
         * 
         * The data must be present to compute the hashCodes, and the compiler can't know that we don't actually care
         * about the values, it only knows that we want the values printed, and this is sufficient to prevent the
         * compiler from eliding any instructions.
         * 
         * In addition, having the hashCodes handy makes it easier to track down and outliers. */
        
        long sumEigvalNanos = 0L;
        long sumEigvecNanos = 0L;
        long sumHashCodes   = 0L;
        
        for (final long seed : seeds) {
            final ComplexMatrix A = ComplexMatrixTest.randomUpperNHessenberg(matrixDim, 1, 100.0, seed, onlyReal);
            
            final SchurMode mode = SchurMode.SCHUR_FORM;
            final int maxIters = ComplexSchurDecomposition.chooseMaxIters(A);
            
            final long                      beforeEigvals = System.nanoTime();
            final ComplexSchurDecomposition schur         = ComplexSchurDecomposition.of(A, mode, maxIters, null);
            final long                      afterEigvals  = System.nanoTime();
            final long                      eigNanos      = afterEigvals - beforeEigvals;
            
            final int eigvalsHash = schur.getEigenvalues().hashCode();
            
            sumEigvalNanos += eigNanos;
            sumHashCodes   += eigvalsHash;
            
            if (mode != SchurMode.EIGENVALUES) {
                final LinearSolver solver = LinearSolver.defaultSolver();
                final EigenvectorFailureMode failMode = EigenvectorFailureMode.THROW;
                
                final long          beforeEigvecs = System.nanoTime();
                final ComplexMatrix V             = schur.computeEigenvectors(A, 5, 0.0, solver, failMode)
                                                         .orElseThrow();
                final long          afterEigvecs  = System.nanoTime();
                
                final long   eigvecNanos = afterEigvecs - beforeEigvecs;
                final long   eigvecHash  = V.hashCode();
                
                sumEigvecNanos += eigvecNanos;
                sumHashCodes   += eigvecHash;
                
                final ComplexMatrix shouldBeA = schur.computeQUQH();
                final ComplexMatrix E         = A.minus(shouldBeA);
                
                final double eFrob = E.getFrobeniusNorm();
                
                System.out.format("%12d    Eigenvalues: %.12f, Eigenvectors: %.12f, norm(A - QUQ^H): %.20f%n",
                                  eigvalsHash + eigvecHash,
                                  eigNanos    * 1e-9,
                                  eigvecNanos * 1e-9,
                                  eFrob);
            }
            else {
                System.out.format("%12d    %.12f%n", eigvalsHash, eigNanos * 1e-9);
            }
        }
        
        System.out.println();
        System.out.format("Hashsum: %d%n", sumHashCodes);
        System.out.format("Average Eigenvalue Time: %s%n", sumEigvalNanos * 1e-9 / numSeeds);
        if (sumEigvecNanos != 0.0) {
            System.out.format("Average Eigenvector Time: %s%n", sumEigvecNanos * 1e-9 / numSeeds);
        }
    }
}
