/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.MathIllegalArgumentException;

import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;
import ptolemaeus.math.linear.real.Matrix;

/** Tests for {@link QRShifts}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
class QRShiftsTest {
    
    /** Test the simple cases of {@link QRShifts#computeNumShiftsPerBulge(int)} */
    @Test
    void testComputeNumShiftsSimple() {
        final int numShifts15  = QRShifts.computeNumShiftsPerBulge(15);
        final int numShifts35  = QRShifts.computeNumShiftsPerBulge(35);
        final int numShifts63  = QRShifts.computeNumShiftsPerBulge(63);
        final int numShifts99  = QRShifts.computeNumShiftsPerBulge(99);
        final int numShifts101 = QRShifts.computeNumShiftsPerBulge(101);
        
        assertEquals(2,  numShifts15);
        assertEquals(4,  numShifts35);
        assertEquals(6,  numShifts63);
        assertEquals(8,  numShifts99);
        assertEquals(10, numShifts101);
    }
    
    /** A single test of {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)}
     * with non-random inputs */
    @Test
    void testComputeFirstColumnOfShiftPolynomialSingle() {
        final ComplexMatrix H = new ComplexMatrix(new Matrix(new double[][] {
            { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // upper Hessenberg
            { 2, 2, 2, 2, 2, 2, 2, 2, 2 },
            { 0, 3, 3, 3, 3, 3, 3, 3, 3 },
            { 0, 0, 4, 4, 4, 4, 4, 4, 4 },
            { 0, 0, 0, 5, 5, 5, 5, 5, 5 },
            { 0, 0, 0, 0, 6, 6, 6, 6, 6 },
            { 0, 0, 0, 0, 0, 7, 7, 7, 7 },
            { 0, 0, 0, 0, 0, 0, 8, 8, 8 },
            { 0, 0, 0, 0, 0, 0, 0, 9, 9 }
        }));
        
        final ComplexVector shifts1 = new ComplexVector(new Complex[] { Complex.valueOf(0.5) });
        testComputeFirstColumnOfShiftPolynomial(H, shifts1);
        
        final ComplexVector shifts2 = new ComplexVector(new Complex[] { Complex.valueOf(0.5),
                                                  Complex.valueOf(1.5),
                                                  Complex.valueOf(2.5),
                                                  Complex.valueOf(3.5),
                                                  Complex.valueOf(4.5) });
        testComputeFirstColumnOfShiftPolynomial(H, shifts2);
        
        assertThrows(MathIllegalArgumentException.class,
                     () -> testComputeFirstColumnOfShiftPolynomial(10,  10)); // too many shifts requested!
    }
    
    /** A single test of {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)} with inputs
     * extracted from the middle of a run that was giving me trouble. */
    @Test
    void testComputeFirstColumnOfShiftPolynomialStrange() {
        // CHECKSTYLE.OFF: LineLength
        final Complex[][] HData = new Complex[][] {
            { new Complex(+2.7645883914296000e+01, 0), new Complex(-7.8009085161745360e-01, 0), new Complex(+1.5145244696787940e+00, 0), new Complex(+1.8390235937218922e+00, 0), new Complex(-2.810627627838171,  0), new Complex(-1.9360482720288712, 0), new Complex(+1.9435120659682235,  0) },
            { new Complex(+1.2386626135815175e-13, 0), new Complex(+9.7292774239819600e-01, 0), new Complex(-3.7828486184628014e+00, 0), new Complex(+4.2435123340070980e-01, 0), new Complex(+0.2783375708473217, 0), new Complex(+1.788700831315558,  0), new Complex(-0.9837907635566239,  0) },
            { new Complex(-1.1756131539579216e-65, 0), new Complex(+3.6293951861884572e+00, 0), new Complex(+1.2708667624287329e+00, 0), new Complex(+5.5213693111683540e-01, 0), new Complex(-0.5187425840586091, 0), new Complex(-3.4353990314908858, 0), new Complex(-2.195745594939084,   0) },
            { new Complex(-8.9761173077593120e-69, 0), new Complex(+4.2545236365984896e-55, 0), new Complex(+5.8647182227490570e-02, 0), new Complex(-2.9139874533965866e+00, 0), new Complex(+2.529603953638256,  0), new Complex(+3.431293759522525,  0), new Complex(+0.455112297594712,   0) },
            { new Complex(-5.3437785222879386e-71, 0), new Complex(-4.3171158744465860e-57, 0), new Complex(-1.9214259262855815e-53, 0), new Complex(-2.3512315866197806e+00, 0), new Complex(-3.355479166048187,  0), new Complex(-0.9210460653114299, 0), new Complex(+0.29491770882021545, 0) },
            { new Complex(+3.6244170125479820e-72, 0), new Complex(-1.5475930629285964e-58, 0), new Complex(-4.4336950618897405e-58, 0), new Complex(-3.0814879110195774e-33, 0), new Complex(-0.2932791087103609, 0), new Complex(+2.918110931832979,  0), new Complex(-2.1999741490724127,  0) },
            { new Complex(-4.6232839370215125e-85, 0), new Complex(+3.4588616236396260e-74, 0), new Complex(-1.6994972142629770e-70, 0), new Complex(+8.3179827608201640e-49, 0), new Complex(+0.0,                0), new Complex(+0.7310789439301398, 0), new Complex(+3.3712110331860905,  0) }
        };
        final ComplexMatrix H = new ComplexMatrix(HData);
        // CHECKSTYLE.ON: LineLength
        
        final ComplexVector shifts = QRShifts.computeShifts(H, false, 2, 0);
        testComputeFirstColumnOfShiftPolynomial(H, shifts);
    }
    
    /** Test {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)} with a few
     * random {@link ComplexMatrix complex matrices} and shifts */
    @Test
    void testComputeFirstColumnOfShiftPolynomialSeveralRandom() {
        testComputeFirstColumnOfShiftPolynomial(10,  5);
        testComputeFirstColumnOfShiftPolynomial(20, 10);
        testComputeFirstColumnOfShiftPolynomial(64, 16);
    }
    
    /** Test {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)} with a
     * {@link ComplexMatrixTest#randomComplexMatrix(int, int, double, long, boolean) randoms complex matrix} and a
     * random set of shifts.
     * 
     * @param n the square dimension of the
     *        {@link ComplexMatrixTest#randomComplexMatrix(int, int, double, long, boolean) randoms complex matrix} to
     *        create
     * @param m the number of shifts */
    private static void testComputeFirstColumnOfShiftPolynomial(final int n, final int m) {
        final ComplexMatrix H = ComplexMatrixTest.randomUpperNHessenberg(n, 1, 1.0 * n, 8675309L * n * n, false);
        
        final ComplexVector fakeShifts = ComplexVectorTest.randomComplexVector(m, 1.0 * m, 8675309L * m);
        testComputeFirstColumnOfShiftPolynomial(H, fakeShifts);
        
        final ComplexVector realShifts = QRShifts.computeShifts(H, false, m, 0);
        final ComplexVector expected   = testComputeFirstColumnOfShiftPolynomial(H, realShifts);
        
        final ComplexVector actual = QRShifts.computeFirstColumnOfShiftPolynomial(H, false, m, 0);
        assertTrue((new ComplexVector(expected)).equals(actual));
    }
    
    /** Test {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)}
     * 
     * @param H the {@link ComplexMatrix}
     * @param shifts the shifts to apply
     * @return the {@link ComplexVector} result from
     *         {@link QRShifts#computeFirstColumnOfShiftPolynomial(ComplexMatrix, ComplexVector)} */
    private static ComplexVector testComputeFirstColumnOfShiftPolynomial(final ComplexMatrix H,
                                                                         final ComplexVector shifts) {
        final ComplexMatrix I         = ComplexMatrix.identity(H.getColumnDimension());
        final int           numShifts = shifts.getDimension();
        
        ComplexMatrix runningProd = I;
        for (int i = 1; i <= numShifts; i++) {
            final Complex       shift = shifts.getEntry(numShifts - i);
            final ComplexMatrix P     = H.minus(I.scalarMultiply(shift));
            runningProd = runningProd.times(P);
        }
        
        final ComplexVector vExpected = new ComplexVector(runningProd.getColumn(0)).getSubVector(0, numShifts + 1);
        final ComplexVector vActual   = new ComplexVector(QRShifts.computeFirstColumnOfShiftPolynomial(H, shifts));
        
        // the 4x machEps is mostly arbitrary, but it works out nicely
        final double absEps = vExpected.hermitianNorm() * 5 * Numerics.MACHINE_EPSILON;
        NumericsTest.assertEBEVectorsEqualAbsolute(vExpected, vActual, absEps);
        
        return vActual;
    }
    
    /** Test {@link QRShifts#computeEigenTrailingPrincipal2x2(ComplexMatrix, boolean)}. The Eigenvalues here
     * were validated by computing them using Wolfram Alpha, first. */
    @Test
    void testComputePrinciple2x2Eigen() {
        final ComplexMatrix A0 = new ComplexMatrix(new Matrix(new double[][] { { 1, 0 }, { 0, 1 } }));
        
        final ComplexMatrix A1 = new ComplexMatrix(new Matrix(new double[][] { { 1, 1 }, { 0, 0 } }));
        
        final ComplexMatrix A2 = new ComplexMatrix(new Matrix(new double[][] { { 1, 2 }, { 3, 4 } }));
        
        final ComplexMatrix A3 =
                new ComplexMatrix(new Complex[][] { { Complex.valueOf(1, 2), Complex.valueOf(2, 3) },
                                                    { Complex.valueOf(3, 4), Complex.valueOf(4, 5) } });
        
        final ComplexVector evA0 = QRShifts.computeEigenTrailingPrincipal2x2(A0, false);
        final ComplexVector evA1 = QRShifts.computeEigenTrailingPrincipal2x2(A1, false);
        final ComplexVector evA2 = QRShifts.computeEigenTrailingPrincipal2x2(A2, false);
        final ComplexVector evA3 = QRShifts.computeEigenTrailingPrincipal2x2(A3, false);
        
        assertArrayEquality(new Complex[] { Complex.valueOf(1, 0), Complex.valueOf(1, 0) }, evA0.getData());
        assertArrayEquality(new Complex[] { Complex.valueOf(1, 0), Complex.valueOf(0, 0) }, evA1.getData());
        assertArrayEquality(new Complex[] { Complex.valueOf(5.372281323269014, 0),
                                            Complex.valueOf(-0.3722813232690143, 0) },
                            evA2.getData());
        assertArrayEquality(new Complex[] { Complex.valueOf(5.356703894836645, 7.2630781473117),
                                            Complex.valueOf(-0.3567038948366452, -0.2630781473116999) },
                            evA3.getData());
        
        /* A3Big is the same as A3, but it has an extra row and column. That we get the same EVs demonstrates that we're
         * definitely only looking at the principle 2x2 in the bottom right */
        final ComplexMatrix A3Big = new ComplexMatrix(new Complex[][]
                { { Complex.valueOf(100, 100), Complex.valueOf(100, 100), Complex.valueOf(100, 100) },
                  { Complex.valueOf(100, 100), Complex.valueOf(1, 2),     Complex.valueOf(2, 3) },
                  { Complex.valueOf(100, 100), Complex.valueOf(3, 4),     Complex.valueOf(4, 5) } });
        final ComplexVector evA3Big = QRShifts.computeEigenTrailingPrincipal2x2(A3Big, false);
        assertArrayEquality(new Complex[] { Complex.valueOf(5.356703894836645, 7.2630781473117),
                                            Complex.valueOf(-0.3567038948366452, -0.2630781473116999) },
                            evA3Big.getData());
    }
    
    /** Test {@link QRShifts#computeShifts(ComplexMatrix, boolean, int, int)} */
    @Test
    void testComputeShifts() {
        // the Eigenvalues of the following matrix were checked using Wolfram Alpha
        // See testComputePrinciple2x2Eigen above
        final ComplexMatrix A3x3 = new ComplexMatrix(new Complex[][]
                { { Complex.valueOf(100, 100), Complex.valueOf(100, 100), Complex.valueOf(100, 100) },
                  { Complex.valueOf(100, 100), Complex.valueOf(1, 2),     Complex.valueOf(2, 3) },
                  { Complex.valueOf(100, 100), Complex.valueOf(0, 0),     Complex.valueOf(4, 5) } });
        
        // you always get at least two shift values, so we get two despite requesting only one
        final Complex[] shifts1 = QRShifts.computeShifts(A3x3, false, 1, 0).getData();
        assertEquals(2, shifts1.length);
        assertEquals(Complex.valueOf(4, 5), shifts1[0]);
        
        final Complex[] expectedTrailing2x2EVs = new Complex[] { Complex.valueOf(4, 5), Complex.valueOf(1, 2) };
        
        final Complex[] shifts3 = QRShifts.computeShifts(A3x3, false, 2, 0).getData();
        assertEquals(2, shifts3.length);
        assertArrayEquality(expectedTrailing2x2EVs, shifts3);
        
        final ComplexMatrix A2x2 = new ComplexMatrix(new Complex[][]
                { { Complex.valueOf(1, 2),     Complex.valueOf(2, 3) },
                  { Complex.valueOf(0, 0),     Complex.valueOf(4, 5) } });
        
        final Complex[] evA2x22 = QRShifts.computeShifts(A2x2, false, 2, 0).getData();
        
        assertArrayEquality(expectedTrailing2x2EVs, evA2x22);
    }
}
