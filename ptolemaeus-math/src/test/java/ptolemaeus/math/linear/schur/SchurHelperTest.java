/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.complex.ComplexMatrixTest;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.complex.ComplexVectorTest;
import ptolemaeus.math.linear.householderreductions.ComplexHessenbergReductionTest;
import ptolemaeus.math.linear.householderreductions.ComplexHouseholder;

/** Tests for {@link SchurHelper}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
class SchurHelperTest {
    
    /** Test {@link SchurHelper#SchurHelper(ComplexMatrix, boolean, SchurMode, int, Consumer, boolean, int)}, just to
     * make sure everything is consistent */
    @Test
    void testConstructor() {
        final Consumer<ComplexMatrix> evalMonitor = M -> { };
        final ComplexMatrix           H           = ComplexMatrixTest.randomUpperNHessenberg(20, 1, 1.0, 1L, false);
        final SchurHelper             schur       = new SchurHelper(H, false, SchurMode.SCHUR_FORM,
                                                                    123, evalMonitor, true, 8);
        
        assertFalse(schur.isHermitian);
        assertSame(SchurMode.SCHUR_FORM, schur.schurMode);
        assertEquals(123, schur.maxIters);
        assertEquals(5.010420900022436E-292, schur.smlnum);
        assertEquals(H.getColumnDimension(), schur.dim);
        
        assertEquals(ComplexMatrix.identity(schur.dim), schur.currentQ);
        assertNull(new SchurHelper(H, false, SchurMode.EIGENVALUES, // just Eigenvalues? -> no Q
                                   1, null, true, 0).currentQ);
        
        assertSame(H, schur.currentU);
        assertEquals(H, schur.currentU);
        
        assertTrue(schur.hasEvalMonitor);
        assertFalse(new SchurHelper(H, false, SchurMode.SCHUR_FORM, 1, null, true, 0).hasEvalMonitor);
        
        assertEquals(QRShifts.computeNumShiftsPerBulge(schur.dim), schur.numShiftsPerBulge);
        assertEquals(6, schur.diagBulgeDim);
        
        assertEquals(8, schur.recursiveDepth);
        
        assertFalse(new SchurHelper(H, false, // just false for testing
                                    SchurMode.SCHUR_FORM, 1, null, true, 0).isHermitian);
        assertTrue(new SchurHelper(H, true, // not actually Hermitian, just true for testing
                                   SchurMode.SCHUR_FORM, 1, null, true, 0).isHermitian);
    }
    
    /** Test {@link SchurHelper#possiblyPostMultiplyQ} */
    @Test
    void testPossiblyPostMultiplyQ() {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(20, 20, 1.0, 1L, false);
        
        // if mode == EIGENVALUES, we simply do nothing, so this is for coverage
        assertDoesNotThrow(() -> new SchurHelper(A, false, SchurMode.EIGENVALUES, 1, null, true, 0)
                                       .possiblyPostMultiplyQ(null, 0));
        
        final SchurHelper schur = new SchurHelper(A, false, SchurMode.SCHUR_FORM, 1, null, true, 0);
        
        assertEquals(schur.currentQ, ComplexMatrix.identity(schur.dim));
        
        final ComplexMatrix small = ComplexMatrixTest.randomComplexMatrix(5, 5, 1.0, 1L, false);
        
        schur.possiblyPostMultiplyQ(small, 10);
        
        final ComplexMatrix expectedQ = ComplexMatrix.identity(schur.dim);
        ComplexMatrix.postMultiplyInPlace(expectedQ, small, 10, 10);
        
        assertEquals(expectedQ, schur.currentQ);
    }
    
    /** Test {@link SchurHelper#possiblyPostApplyHouseholderToQ} */
    @Test
    void testPossiblyPostApplyHouseholderToQ() {
        final ComplexMatrix A = ComplexMatrixTest.randomComplexMatrix(20, 20, 1.0, 1L, false);
        
        // if mode == EIGENVALUES, we simply do nothing, so this is for coverage
        assertDoesNotThrow(() -> new SchurHelper(A, false, SchurMode.EIGENVALUES, 1, null, true, 0)
                                       .possiblyPostApplyHouseholderToQ(null, 0));
        
        final SchurHelper schur = new SchurHelper(A, false, SchurMode.SCHUR_FORM, 1, null, true, 0);
        
        assertEquals(schur.currentQ, ComplexMatrix.identity(schur.dim));
        
        final ComplexVector anyVector = ComplexVectorTest.randomComplexVector(5, 1.0, 123L);
        final ComplexHouseholder fakeHouseholder = ComplexHouseholder.of(anyVector);
        
        schur.possiblyPostApplyHouseholderToQ(fakeHouseholder, 10);
        
        final ComplexMatrix expectedQ = ComplexMatrix.identity(schur.dim);
        fakeHouseholder.postApplyInPlace(expectedQ, 10, 10);
        
        assertEquals(expectedQ, schur.currentQ);
    }
    
    /** Test {@link SchurHelper#doHessenbergReduction()}
     * 
     * @see ComplexHessenbergReductionTest */
    @Test
    void testDoHessenbergReduction() {
        final ComplexMatrix H     = ComplexMatrixTest.randomComplexMatrix(20, 20, 1.0, 1L, false);
        final SchurHelper   schur = new SchurHelper(H, false, SchurMode.EIGENVALUES, 1, null,
                                                    false, 0); // not manually setting the sub-Hess entries to zero
        
        schur.doHessenbergReduction();
        
        ComplexHessenbergReductionTest.assertIsNHessenberg(schur.currentU, 1, 1e-12, 1e-12);
    }
    
    /** Test {@link SchurHelper#updateUBlock(ComplexMatrix, int, int)} */
    @Test
    void testUpdateUBlock() {
        final ComplexMatrix H     = ComplexMatrixTest.randomUpperNHessenberg(20, 1, 1.0, 1L, false);
        final SchurHelper   schur = new SchurHelper(H, false, SchurMode.EIGENVALUES, 1, null, false, 0);
        
        final ComplexMatrix block = ComplexMatrixTest.randomComplexMatrix(4, 3, 1.0, 1, false);
        schur.updateUBlock(block, 10, 3); // update the block
        
        assertEquals(block, schur.currentU.getSubMatrix(10, 13, 3, 5)); // grab the block and check it
        
        // now visually:
        final String struct = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String structExpected = String.join(System.lineSeparator(),
                                            " . . . . . . . . . . . . . . . . . . . .",
                                            " . . . . . . . . . . . . . . . . . . . .",
                                            "   . . . . . . . . . . . . . . . . . . .",
                                            "     . . . . . . . . . . . . . . . . . .",
                                            "       . . . . . . . . . . . . . . . . .",
                                            "         . . . . . . . . . . . . . . . .",
                                            "           . . . . . . . . . . . . . . .",
                                            "             . . . . . . . . . . . . . .",
                                            "               . . . . . . . . . . . . .",
                                            "                 . . . . . . . . . . . .",
                                            "       . . .       . . . . . . . . . . .",
                                            "       . . .         . . . . . . . . . .",
                                            "       . . .           . . . . . . . . .",
                                            "       . . .             . . . . . . . .",
                                            "                           . . . . . . .",
                                            "                             . . . . . .",
                                            "                               . . . . .",
                                            "                                 . . . .",
                                            "                                   . . .",
                                            "                                     . .",
                                            "");
        TestUtils.testMultiLineToString(structExpected, struct);
    }
    
    /** Test {@link SchurHelper#setSubHessenbergEntriesToZero(boolean)} */
    @Test
    void testSetSubHessenbergEntriesToZero() {
        final ComplexMatrix H      = ComplexMatrixTest.randomComplexMatrix(20, 20, 1.0, 1L, false);
        final SchurHelper   schur1 = new SchurHelper(H.copy(), false, SchurMode.EIGENVALUES, 1, null, false, 0);
        
        schur1.setSubHessenbergEntriesToZero(false); // set all of them to zero
        
        for (int j = 0; j < 20; j++) {
            for (int i = j + 2; i < 20; i++) {
                assertEquals(Complex.ZERO, schur1.currentU.getEntry(i, j));
            }
        }
        
        // visually:
        final String struct         = Numerics.toNonZeroStructureString(schur1.currentU, 0.0, '.');
        final String structExpected = String.join(System.lineSeparator(),
                                                  " . . . . . . . . . . . . . . . . . . . .",
                                                  " . . . . . . . . . . . . . . . . . . . .",
                                                  "   . . . . . . . . . . . . . . . . . . .",
                                                  "     . . . . . . . . . . . . . . . . . .",
                                                  "       . . . . . . . . . . . . . . . . .",
                                                  "         . . . . . . . . . . . . . . . .",
                                                  "           . . . . . . . . . . . . . . .",
                                                  "             . . . . . . . . . . . . . .",
                                                  "               . . . . . . . . . . . . .",
                                                  "                 . . . . . . . . . . . .",
                                                  "                   . . . . . . . . . . .",
                                                  "                     . . . . . . . . . .",
                                                  "                       . . . . . . . . .",
                                                  "                         . . . . . . . .",
                                                  "                           . . . . . . .",
                                                  "                             . . . . . .",
                                                  "                               . . . . .",
                                                  "                                 . . . .",
                                                  "                                   . . .",
                                                  "                                     . .",
                                                  "");
        TestUtils.testMultiLineToString(structExpected, struct);
        
        final SchurHelper schur2 = new SchurHelper(H.copy(), false, SchurMode.EIGENVALUES, 1, null, false, 0);
        schur2.setSubHessenbergEntriesToZero(true); // only set the bulge-perturbed entries to zero
        
        for (int i = 2; i < schur2.diagBulgeDim; i++) {
            for (int j = 0; j < schur2.dim; j++) {
                final int rowIndex = i + j;
                if (rowIndex >= schur2.dim) {
                    break;
                }
                
                assertEquals(Complex.ZERO, schur2.currentU.getEntry(rowIndex, j));
            }
        }
        
        // visually:
        final String struct2         = Numerics.toNonZeroStructureString(schur2.currentU, 0.0, '.');
        final String struct2Expected = String.join(System.lineSeparator(),
                                                   " . . . . . . . . . . . . . . . . . . . .",
                                                   " . . . . . . . . . . . . . . . . . . . .",
                                                   "   . . . . . . . . . . . . . . . . . . .",
                                                   "     . . . . . . . . . . . . . . . . . .",
                                                   "       . . . . . . . . . . . . . . . . .",
                                                   "         . . . . . . . . . . . . . . . .",
                                                   " .         . . . . . . . . . . . . . . .",
                                                   " . .         . . . . . . . . . . . . . .",
                                                   " . . .         . . . . . . . . . . . . .",
                                                   " . . . .         . . . . . . . . . . . .",
                                                   " . . . . .         . . . . . . . . . . .",
                                                   " . . . . . .         . . . . . . . . . .",
                                                   " . . . . . . .         . . . . . . . . .",
                                                   " . . . . . . . .         . . . . . . . .",
                                                   " . . . . . . . . .         . . . . . . .",
                                                   " . . . . . . . . . .         . . . . . .",
                                                   " . . . . . . . . . . .         . . . . .",
                                                   " . . . . . . . . . . . .         . . . .",
                                                   " . . . . . . . . . . . . .         . . .",
                                                   " . . . . . . . . . . . . . .         . .",
                                                   "");
        // this is obviously not meant to be used this way, but it gets the point across
        TestUtils.testMultiLineToString(struct2Expected, struct2);
    }
    
    /** Test {@link SchurHelper#setSubSupHessenbergEntriesToZero(boolean)} */
    @Test
    void testSetSubSupHessenbergEntriesToZero() {
        final ComplexMatrix H      = ComplexMatrixTest.randomComplexMatrix(20, 20, 1.0, 1L, false);
        final SchurHelper   schur1 = new SchurHelper(H.copy(), true, // true for consistency, though it's unused here
                                                     SchurMode.EIGENVALUES, 1, null, false, 0);
        
        schur1.setSubSupHessenbergEntriesToZero(false); // set all of them to zero
        
        for (int j = 0; j < 20; j++) {
            for (int i = j + 2; i < 20; i++) {
                assertEquals(Complex.ZERO, schur1.currentU.getEntry(i, j));
                assertEquals(Complex.ZERO, schur1.currentU.getEntry(j, i));
            }
        }
        
        // visually:
        final String struct         = Numerics.toNonZeroStructureString(schur1.currentU, 0.0, '.');
        final String structExpected = String.join(System.lineSeparator(),
                                                  " . .                                    ",
                                                  " . . .                                  ",
                                                  "   . . .                                ",
                                                  "     . . .                              ",
                                                  "       . . .                            ",
                                                  "         . . .                          ",
                                                  "           . . .                        ",
                                                  "             . . .                      ",
                                                  "               . . .                    ",
                                                  "                 . . .                  ",
                                                  "                   . . .                ",
                                                  "                     . . .              ",
                                                  "                       . . .            ",
                                                  "                         . . .          ",
                                                  "                           . . .        ",
                                                  "                             . . .      ",
                                                  "                               . . .    ",
                                                  "                                 . . .  ",
                                                  "                                   . . .",
                                                  "                                     . .",
                                                  "");
        TestUtils.testMultiLineToString(structExpected, struct);
        
        final SchurHelper schur2 = new SchurHelper(H.copy(), true, // true for consistency, though it's unused here
                                                   SchurMode.EIGENVALUES, 1, null, false, 0);
        schur2.setSubSupHessenbergEntriesToZero(true); // only set the bulge-perturbed entries to zero
        
        for (int i = 2; i < schur2.diagBulgeDim; i++) {
            for (int j = 0; j < schur2.dim; j++) {
                final int rowIndex = i + j;
                if (rowIndex >= schur2.dim) {
                    break;
                }
                
                assertEquals(Complex.ZERO, schur2.currentU.getEntry(rowIndex, j));
                assertEquals(Complex.ZERO, schur2.currentU.getEntry(j, rowIndex));
            }
        }
        
        // visually:
        final String struct2         = Numerics.toNonZeroStructureString(schur2.currentU, 0.0, '.');
        final String struct2Expected = String.join(System.lineSeparator(),
                                                   " . .         . . . . . . . . . . . . . .",
                                                   " . . .         . . . . . . . . . . . . .",
                                                   "   . . .         . . . . . . . . . . . .",
                                                   "     . . .         . . . . . . . . . . .",
                                                   "       . . .         . . . . . . . . . .",
                                                   "         . . .         . . . . . . . . .",
                                                   " .         . . .         . . . . . . . .",
                                                   " . .         . . .         . . . . . . .",
                                                   " . . .         . . .         . . . . . .",
                                                   " . . . .         . . .         . . . . .",
                                                   " . . . . .         . . .         . . . .",
                                                   " . . . . . .         . . .         . . .",
                                                   " . . . . . . .         . . .         . .",
                                                   " . . . . . . . .         . . .         .",
                                                   " . . . . . . . . .         . . .        ",
                                                   " . . . . . . . . . .         . . .      ",
                                                   " . . . . . . . . . . .         . . .    ",
                                                   " . . . . . . . . . . . .         . . .  ",
                                                   " . . . . . . . . . . . . .         . . .",
                                                   " . . . . . . . . . . . . . .         . .",
                                                   "");
        // this is obviously not meant to be used this way, but it gets the point across
        TestUtils.testMultiLineToString(struct2Expected, struct2);
    }
    
    /** Test {@link SchurHelper} {@link SchurHelper#chaseFromColumn(int)} */
    @Test
    void testApplyShiftAndChaseFromColumn() {
        final ComplexMatrix H     = ComplexMatrixTest.randomUpperNHessenberg(20, 1, 1.0, 1L, false);
        final SchurHelper   schur = new SchurHelper(H, false, SchurMode.SCHUR_FORM, 1, null, true, 0);
        
        final String hess         = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String hessExpected =
                String.join(System.lineSeparator(),
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .",
                            "     . . . . . . . . . . . . . . . . . .",
                            "       . . . . . . . . . . . . . . . . .",
                            "         . . . . . . . . . . . . . . . .",
                            "           . . . . . . . . . . . . . . .",
                            "             . . . . . . . . . . . . . .",
                            "               . . . . . . . . . . . . .",
                            "                 . . . . . . . . . . . .",
                            "                   . . . . . . . . . . .",
                            "                     . . . . . . . . . .",
                            "                       . . . . . . . . .",
                            "                         . . . . . . . .",
                            "                           . . . . . . .",
                            "                             . . . . . .",
                            "                               . . . . .",
                            "                                 . . . .",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(hessExpected, hess);
        
        final ComplexVector anyVector = ComplexVectorTest.randomComplexVector(5, 1.0, 123L);
        final ComplexHouseholder fakeHouseholder = ComplexHouseholder.of(anyVector);
        schur.applyShiftBulge(fakeHouseholder);
        
        final String bulgeHess         = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String bulgeHessExpected =
                String.join(System.lineSeparator(),
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .", // bulge starts at at (1, 0)
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            "           . . . . . . . . . . . . . . .",
                            "             . . . . . . . . . . . . . .",
                            "               . . . . . . . . . . . . .",
                            "                 . . . . . . . . . . . .",
                            "                   . . . . . . . . . . .",
                            "                     . . . . . . . . . .",
                            "                       . . . . . . . . .",
                            "                         . . . . . . . .",
                            "                           . . . . . . .",
                            "                             . . . . . .",
                            "                               . . . . .",
                            "                                 . . . .",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(bulgeHessExpected, bulgeHess);
        
        schur.chaseFromColumn(0);
        
        /* here, we use a non-zero lower-bound because the Householder transformations can't (typically) set the entries
         * to exactly zero due to numerical imprecision. This is indeed expected behavior. */
        final double bcEps               = 1e-12;
        final String chase1 = Numerics.toNonZeroStructureString(schur.currentU, bcEps, '.');
        final String chase1Expected =
                String.join(System.lineSeparator(),
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .", // bulge moved down from the 0th column
                            "   . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .",
                            "             . . . . . . . . . . . . . .",
                            "               . . . . . . . . . . . . .",
                            "                 . . . . . . . . . . . .",
                            "                   . . . . . . . . . . .",
                            "                     . . . . . . . . . .",
                            "                       . . . . . . . . .",
                            "                         . . . . . . . .",
                            "                           . . . . . . .",
                            "                             . . . . . .",
                            "                               . . . . .",
                            "                                 . . . .",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(chase1Expected, chase1);
    }
    
    /** It's difficult to create meaningful tests of the bulge-shift + chase portion of the implicit shifted QR
     * algorithm that test the actual values of the entries of the matrices involved without testing the algorithm as a
     * whole. <br>
     * <br>
     * 
     * So, rather than that, this is a test to show graphically that the bulge-shift is applied and chased as
     * expected */
    @Test
    void testApplyAndChaseByIndexNonHermitian() {
        final ComplexMatrix H     = ComplexMatrixTest.randomUpperNHessenberg(20, 1, 1.0, 1L, false);
        final SchurHelper   schur = new SchurHelper(H, false, SchurMode.SCHUR_FORM, 1, null, true, 0);
        
        final String hess         = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String hessExpected =
                String.join(System.lineSeparator(),
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .",
                            "     . . . . . . . . . . . . . . . . . .",
                            "       . . . . . . . . . . . . . . . . .",
                            "         . . . . . . . . . . . . . . . .",
                            "           . . . . . . . . . . . . . . .",
                            "             . . . . . . . . . . . . . .",
                            "               . . . . . . . . . . . . .",
                            "                 . . . . . . . . . . . .",
                            "                   . . . . . . . . . . .",
                            "                     . . . . . . . . . .",
                            "                       . . . . . . . . .",
                            "                         . . . . . . . .",
                            "                           . . . . . . .",
                            "                             . . . . . .",
                            "                               . . . . .",
                            "                                 . . . .",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(hessExpected, hess);
        
        schur.applyShiftBulge();
        
        final String bulgeHess         = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String bulgeHessExpected =
                String.join(System.lineSeparator(),
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .", // bulge starts at at (1, 0)
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            "           . . . . . . . . . . . . . . .",
                            "             . . . . . . . . . . . . . .",
                            "               . . . . . . . . . . . . .",
                            "                 . . . . . . . . . . . .",
                            "                   . . . . . . . . . . .",
                            "                     . . . . . . . . . .",
                            "                       . . . . . . . . .",
                            "                         . . . . . . . .",
                            "                           . . . . . . .",
                            "                             . . . . . .",
                            "                               . . . . .",
                            "                                 . . . .",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(bulgeHessExpected, bulgeHess);
        
        schur.chaseShiftBulge(0, 3); // chase bulge forward three indices
        
        /* here, we use a non-zero lower-bound because the Householder transformations can't (typically) set the entries
         * to exactly zero due to numerical imprecision. This is indeed expected behavior. */
        final double bcEps               = 2e-16;
        final String partialChase1 = Numerics.toNonZeroStructureString(schur.currentU, bcEps, '.');
        final String partialChase1Expected =
                String.join(System.lineSeparator(),
                            " . . . . . . . . . . . . . . . . . . . .",
                            " . . . . . . . . . . . . . . . . . . . .",
                            "   . . . . . . . . . . . . . . . . . . .",
                            "     . . . . . . . . . . . . . . . . . .",
                            "       . . . . . . . . . . . . . . . . .", // bulge now starts at (1 + 3, 0 + 3) == (4, 3)
                            "       . . . . . . . . . . . . . . . . .",
                            "       . . . . . . . . . . . . . . . . .",
                            "       . . . . . . . . . . . . . . . . .",
                            "       . . . . . . . . . . . . . . . . .",
                            "                 . . . . . . . . . . . .",
                            "                   . . . . . . . . . . .",
                            "                     . . . . . . . . . .",
                            "                       . . . . . . . . .",
                            "                         . . . . . . . .",
                            "                           . . . . . . .",
                            "                             . . . . . .",
                            "                               . . . . .",
                            "                                 . . . .",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(partialChase1Expected, partialChase1);
        
        schur.chaseShiftBulge(3, 10);
        
        final String partialChase2 = Numerics.toNonZeroStructureString(schur.currentU, bcEps, '.');
        
        final String partialChase2Expected =
                String.join(System.lineSeparator(),
                        " . . . . . . . . . . . . . . . . . . . .",
                        " . . . . . . . . . . . . . . . . . . . .",
                        "   . . . . . . . . . . . . . . . . . . .",
                        "     . . . . . . . . . . . . . . . . . .",
                        "       . . . . . . . . . . . . . . . . .",
                        "         . . . . . . . . . . . . . . . .",
                        "           . . . . . . . . . . . . . . .",
                        "             . . . . . . . . . . . . . .",
                        "               . . . . . . . . . . . . .",
                        "                 . . . . . . . . . . . .",
                        "                   . . . . . . . . . . .",
                        "                     . . . . . . . . . .",
                        "                       . . . . . . . . .",
                        "                         . . . . . . . .",
                        "                           . . . . . . .", // bulge now starts at (4 + 10, 3 + 10) == (14, 13)
                        "                           . . . . . . .",
                        "                           . . . . . . .",
                        "                           . . . . . . .",
                        "                           . . . . . . .",
                        "                                     . .",
                        "");
        TestUtils.testMultiLineToString(partialChase2Expected, partialChase2);
    }
    
    /** The same as {@link #testApplyAndChaseByIndexNonHermitian()}, but set {@link SchurHelper#isHermitian isHermitian
     * == true} and use a {@link ComplexMatrix#isHermitian(Complex) Hermitian} matrix.<br>
     * <br>
     * 
     * Note: because the initial matrix is Hermitian, {@link SchurHelper#doHessenbergReduction()} puts us into a
     * tri-diagonal state. */
    @Test
    void testApplyAndChaseByIndexHermitian() {
        final ComplexMatrix A     = ComplexMatrixTest.randomUpperNHessenberg(20, 1, 1.0, 1L, false);
        
        final ComplexMatrix H = A.timesConjugateTransposeOf(A); // Hermitian
        final SchurHelper   schur = new SchurHelper(H, true, SchurMode.SCHUR_FORM, 1, null, true, 0);
        
        schur.doHessenbergReduction(); // Hessenberg makes Hermitian matrices tri-diagonal
        
        final String hess         = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String hessExpected =
                String.join(System.lineSeparator(),
                            " . .                                    ",
                            " . . .                                  ",
                            "   . . .                                ",
                            "     . . .                              ",
                            "       . . .                            ",
                            "         . . .                          ",
                            "           . . .                        ",
                            "             . . .                      ",
                            "               . . .                    ",
                            "                 . . .                  ",
                            "                   . . .                ",
                            "                     . . .              ",
                            "                       . . .            ",
                            "                         . . .          ",
                            "                           . . .        ",
                            "                             . . .      ",
                            "                               . . .    ",
                            "                                 . . .  ",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(hessExpected, hess);
        
        schur.applyShiftBulge();
        
        final String bulgeHess         = Numerics.toNonZeroStructureString(schur.currentU, 0.0, '.');
        final String bulgeHessExpected =
                String.join(System.lineSeparator(),
                            " . . . . . .                            ",
                            " . . . . . .                            ", // bulge starts at at (1, 0), and we 
                            " . . . . . .                            ", // can see it reflected at (1, 0),
                            " . . . . . .                            ", // unlike in the non-Hermitian case
                            " . . . . . .                            ",
                            " . . . . . . .                          ",
                            "           . . .                        ",
                            "             . . .                      ",
                            "               . . .                    ",
                            "                 . . .                  ",
                            "                   . . .                ",
                            "                     . . .              ",
                            "                       . . .            ",
                            "                         . . .          ",
                            "                           . . .        ",
                            "                             . . .      ",
                            "                               . . .    ",
                            "                                 . . .  ",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(bulgeHessExpected, bulgeHess);
        
        schur.chaseShiftBulge(0, 3); // chase bulge forward three indices
        
        /* here, we use a non-zero lower-bound because the Householder transformations can't (typically) set the entries
         * to exactly zero due to numerical imprecision. This is indeed expected behavior. */
        final double bcEps               = 1e-15;
        final String partialChase1 = Numerics.toNonZeroStructureString(schur.currentU, bcEps, '.');
        final String partialChase1Expected =
                String.join(System.lineSeparator(),
                            " . .                                    ",
                            " . . .                                  ",
                            "   . . .                                ",
                            "     . . . . . . .                      ", // bulge now starts at (1 + 3, 0 + 3) == (4, 3)
                            "       . . . . . .                      ", // and - again due to its Hermitian nature - 
                            "       . . . . . .                      ", // we see it reflected at (3, 4)
                            "       . . . . . .                      ",
                            "       . . . . . .                      ",
                            "       . . . . . . .                    ",
                            "                 . . .                  ",
                            "                   . . .                ",
                            "                     . . .              ",
                            "                       . . .            ",
                            "                         . . .          ",
                            "                           . . .        ",
                            "                             . . .      ",
                            "                               . . .    ",
                            "                                 . . .  ",
                            "                                   . . .",
                            "                                     . .",
                            "");
        TestUtils.testMultiLineToString(partialChase1Expected, partialChase1);
        
        schur.chaseShiftBulge(3, 10);
        
        final String partialChase2 = Numerics.toNonZeroStructureString(schur.currentU, bcEps, '.');
        
        final String partialChase2Expected =
                String.join(System.lineSeparator(),
                        " . .                                    ",
                        " . . .                                  ",
                        "   . . .                                ",
                        "     . . .                              ",
                        "       . . .                            ",
                        "         . . .                          ",
                        "           . . .                        ",
                        "             . . .                      ",
                        "               . . .                    ",
                        "                 . . .                  ",
                        "                   . . .                ",
                        "                     . . .              ",
                        "                       . . .            ",
                        "                         . . . . . . .  ", // bulge now starts at (4 + 10, 3 + 10) == (14, 13),
                        "                           . . . . . .  ", // and it's reflected at (13, 14)
                        "                           . . . . . .  ",
                        "                           . . . . . .  ",
                        "                           . . . . . .  ",
                        "                           . . . . . . .",
                        "                                     . .",
                        "");
        TestUtils.testMultiLineToString(partialChase2Expected, partialChase2);
    }
}
