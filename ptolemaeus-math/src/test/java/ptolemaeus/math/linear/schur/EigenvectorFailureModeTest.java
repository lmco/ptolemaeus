/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import ptolemaeus.math.NumericalMethodsException;
import ptolemaeus.math.linear.complex.ComplexVector;
import ptolemaeus.math.linear.real.NVector;

/** Test {@link EigenvectorFailureMode}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class EigenvectorFailureModeTest {
    
    /** Test all of {@link EigenvectorFailureMode} */
    @Test
    void testAll() {
        final ComplexVector v = new ComplexVector(new NVector(1, 1, 1, 1, 1));
        
        assertSame(v, EigenvectorFailureMode.AS_IS.handleFailure(v));
        assertEquals(ComplexVector.zero(5), EigenvectorFailureMode.ZERO.handleFailure(v));
        
        final NumericalMethodsException ex =
                assertThrows(NumericalMethodsException.class, () -> EigenvectorFailureMode.THROW.handleFailure(v));
        assertEquals("Failed to adequately compute Eigenvector", ex.getMessage());
    }
}
