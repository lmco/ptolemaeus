/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ptolemaeus.math.linear.schur;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.linear.complex.ComplexMatrix;

/**
 * Tests for {@link QRDeflations}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class QRDeflationsTest {
    
    /** A testing value to take the place of {@link SchurHelper#smlnum}. In practice, this will be near 1e-120, but we
     * can use anything in testing. */
    private static final double SMLNUM = 0.1;
    
    /** @return a <em>new</em> {@link ComplexMatrix} instance that's upper-Hessenberg */
    private static ComplexMatrix getTestHessenberg() {
        return new ComplexMatrix(new double[][] {
            { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 2, 2, 2, 2, 2, 2, 2, 2, 2 },
            { 0, 3, 3, 3, 3, 3, 3, 3, 3 },
            { 0, 0, 4, 4, 4, 4, 4, 4, 4 },
            { 0, 0, 0, 5, 5, 5, 5, 5, 5 },
            { 0, 0, 0, 0, 6, 6, 6, 6, 6 },
            { 0, 0, 0, 0, 0, 7, 7, 7, 7 },
            { 0, 0, 0, 0, 0, 0, 8, 8, 8 },
            { 0, 0, 0, 0, 0, 0, 0, 9, 9 },
        });
    }
    
    /** Test the simplest cases of {@link QRDeflations#canDeflate(ComplexMatrix, int, double)}; i.e., where the
     * sub-diagonal element is smaller than {@code smlnum}. */
    @Test
    void testCanDeflate1() {
        final ComplexMatrix A = getTestHessenberg();
        
        // initially, none of the sub-diag. elements are small enough for a deflation:
        IntStream.range(0, 8).forEach(i -> assertFalse(QRDeflations.canDeflate(A, i, SMLNUM)));
        // note: we can't check index 8 itself because we're checking the sub-diagonal elements
        
        // In the simplest case, if a sub-diag. element is small enough, and we can deflate at that index:
        A.unsafeSetEntry(3, 2, SMLNUM, 0);
        assertTrue(QRDeflations.canDeflate(A, 2, SMLNUM));
    }
    
    /** Test that we can handle a zero on the main diagonal */
    @Test
    void testCanDeflate2() {
        final double u = Numerics.MACHINE_EPSILON;
        
        final ComplexMatrix A = getTestHessenberg();
        
        A.unsafeSetEntry(1, 1, 0, 0);
        assertFalse(QRDeflations.canDeflate(A, 0, 0.0)); // use smlnum == 0 to bypass first check
        
        A.unsafeSetEntry(1, 0, u, 0);
        assertFalse(QRDeflations.canDeflate(A, 0, 0.0)); // still can't deflate
        
        assertTrue(QRDeflations.canDeflate(A, 0, u / 2)); // increasing smlnum a bit to return true from the lower if
    }
    
    /** Test that we can handle sequential zeros on the main diagonal */
    @Test
    void testCanDeflate3() {
        final ComplexMatrix A = getTestHessenberg();
        
        IntStream.range(0, 9).forEach(i -> A.unsafeSetEntry(i, i, 0, 0));
        
        IntStream.range(0, 8).forEach(i -> assertFalse(QRDeflations.canDeflate(A, i, 0.0)));
        // note: we can't check index 8 itself because we're checking the sub-diagonal elements
    }
    
    /** Test {@link QRDeflations#computeDeflations(ComplexMatrix, double, int, int)} */
    @Test
    void testComputeDeflations1() {
        final ComplexMatrix A = getTestHessenberg();
        
        // can't yet deflate:
        assertNull(QRDeflations.computeDeflations(A, 4, SMLNUM));
        
        A.unsafeSetEntry(5, 4, SMLNUM, 0); // now we can!
        
        // ... but we're not checking the right range
        final ComplexMatrix[] deflations1 = QRDeflations.computeDeflations(A, SMLNUM, 0, 3);
        assertNull(deflations1);
        
        // now we are!
        final ComplexMatrix[] deflations2 = QRDeflations.computeDeflations(A, SMLNUM, 4, A.getColumnDimension() - 1);
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 1 1 1 1 1",
                                                    " 2 2 2 2 2",
                                                    " 0 3 3 3 3",
                                                    " 0 0 4 4 4",
                                                    " 0 0 0 5 5"),
                                        deflations2[0]);
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 6 6 6 6",
                                                    " 7 7 7 7",
                                                    " 0 8 8 8",
                                                    " 0 0 9 9"),
                                        deflations2[1]);
    }
    
    /** Test {@link QRDeflations#computeDeflations(ComplexMatrix, int, double)} */
    @Test
    void testComputeDeflations2() {
        final ComplexMatrix A = getTestHessenberg();
        
        // can't yet deflate:
        assertNull(QRDeflations.computeDeflations(A, 4, SMLNUM));
        
        A.unsafeSetEntry(5, 4, SMLNUM, 0); // now we can!
        
        final ComplexMatrix[] deflations = QRDeflations.computeDeflations(A, 4, SMLNUM);
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 1 1 1 1 1",
                                                    " 2 2 2 2 2",
                                                    " 0 3 3 3 3",
                                                    " 0 0 4 4 4",
                                                    " 0 0 0 5 5"),
                                        deflations[0]);
        TestUtils.testMultiLineToString(String.join(System.lineSeparator(),
                                                    " 6 6 6 6",
                                                    " 7 7 7 7",
                                                    " 0 8 8 8",
                                                    " 0 0 9 9"),
                                        deflations[1]);
    }
}
