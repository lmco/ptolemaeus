/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test {@link LevenbergMarquardtParameter}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class LevenbergMarquardtParameterTest {
    
    /**
     * Test {@link LevenbergMarquardtParameter#getDefault()} and {@link LevenbergMarquardtParameter#reset()}
     */
    @Test
    void testGetDefaultAndReset() {
        final LevenbergMarquardtParameter lambda = LevenbergMarquardtParameter.getDefault();
        assertEquals(1D, lambda.get());
        
        lambda.scaleUp();
        assertEquals(2D, lambda.get());
        
        lambda.scaleDown();
        assertEquals(2D / 3D, lambda.get());
        
        lambda.reset();
        assertEquals(1D, lambda.get());
    }
    
    /**
     * Test to ensure that {@link LevenbergMarquardtParameter} properly manages &lambda;
     */
    @Test
    void testAll() {
        final LevenbergMarquardtParameter lambda = new LevenbergMarquardtParameter(65_536D, 4D, 2D);
        assertEquals(65_536D, lambda.get());
        
        lambda.scaleDown();
        lambda.scaleDown();
        lambda.scaleDown();
        lambda.scaleDown();
        lambda.scaleDown();
        lambda.scaleDown();
        
        assertEquals(1_024D, lambda.get());
        
        lambda.scaleUp();
        lambda.scaleUp();
        lambda.scaleUp();
        
        assertEquals(65_536D, lambda.get());
        
        
        final LevenbergMarquardtParameter copy = lambda.freshCopy();
        assertEquals(65_536D, copy.get());
        
        copy.scaleDown();
        copy.scaleDown();
        copy.scaleDown();
        copy.scaleDown();
        copy.scaleDown();
        copy.scaleDown();
        
        assertEquals(1_024D, copy.get());
        
        copy.scaleUp();
        copy.scaleUp();
        copy.scaleUp();
        
        assertEquals(65_536D, copy.get());
    }
}
