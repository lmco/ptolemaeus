/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresConstants;

/**
 * Test {@link LevenbergMarquardtConstants}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class LevenbergMarquardtConstantsTest {
    
    /**
     * Test all using collections for the theta and scaling ranges
     */
    @Test
    void testConstructor() {
        final Function<RealMatrix, DecompositionSolver> decSolverFact = RealQRDecomposition::of;
        LevenbergMarquardtConstants constants =
                new LevenbergMarquardtConstants(2,
                                                new double[] { 3, 4 },
                                                2.0,
                                                123,
                                                456,
                                                decSolverFact,
                                                new LevenbergMarquardtParameter(1D, 2D, 3D),
                                                LevenbergMarquardtConstants.DEFAULT_ALPHA_EASY,
                                                LevenbergMarquardtConstants.DEFAULT_H);
        
        assertEquals(2.0 * 2.0, constants.getEpsilonSqr());
        assertEquals(123, constants.getMaxIterations());
        assertEquals(456, constants.getMaxUnimprovedIters());
        assertEquals(Matrix.diagonal(new double[] { 3, 4 }), constants.getWeights());
        assertEquals(1D, constants.getLambda().get());
        assertEquals(0.75, constants.getAlpha());
        assertEquals(0.1, constants.getH());
        assertEquals(0.1 * 0.1, constants.getHSqrd());
        assertSame(decSolverFact, constants.getDecompositionSolverFactory());
    }
    
    /**
     * Test {@link LevenbergMarquardtConstants#getSimple(int)}
     */
    @Test
    void testGetSimple() {
        LevenbergMarquardtConstants constants = LevenbergMarquardtConstants.getSimple(3);
        assertEquals(1e-12, constants.getEpsilonSqr());
        assertEquals(1_000, constants.getMaxIterations());
        assertEquals(100, constants.getMaxUnimprovedIters());
        assertEquals(Matrix.diagonal(new double[] { 1D, 1D, 1D }), constants.getWeights());
        assertEquals(1D, constants.getLambda().get());
        assertEquals(0.75, constants.getAlpha());
        assertEquals(0.1, constants.getH());
        assertEquals(0.1 * 0.1, constants.getHSqrd());
        assertSame(WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER, constants.getDecompositionSolverFactory());
    }
}
