/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresConstants;

/**
 * Test {@link LevenbergMarquardtProblem}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class LevenbergMarquardtProblemTest {
    
    /** Test
     * {@link LevenbergMarquardtProblem#getSimple(MultivariateVectorValuedFunction, double[], double[], Parallelism)} */
    @Test
    void testGetSimple() {
        final MultivariateVectorValuedFunction f = (double[] input) -> {
            return new double[] { -1, -2, -3 };
        };
        final double[] initialGuess = new double[] {1, 3, 5};
        final double[] targetValues = new double[] {2, 4, 6};
        
        final LevenbergMarquardtProblem lmp = LevenbergMarquardtProblem.getSimple(f,
                                                                                  initialGuess,
                                                                                  targetValues,
                                                                                  Parallelism.SEQUENTIAL);
        assertArrayEquals(new double[] { -1, -2, -3 }, lmp.getF().apply(initialGuess));
        assertArrayEquals(initialGuess, lmp.getInitialGuess());
        assertArrayEquals(targetValues, lmp.getTargetValues());
        assertNotNull(lmp.getJacobian());
    }
    
    /**
     * Test everything
     */
    @Test
    public void testAll() {
        MultivariateVectorValuedFunction f = (double[] input) -> {
            return new double[] {
                    -1, -2, -3 };
        };
        double[] initialGuess = new double[] {1, 3, 5};
        double[] targetValues = new double[] {2, 4, 6};
        
        LevenbergMarquardtConstants constants =
                new LevenbergMarquardtConstants(3,
                                                new double[] { 3, 4, 5 },
                                                2.0,
                                                123,
                                                456,
                                                WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER,
                                                new LevenbergMarquardtParameter(1D, 2D, 3D),
                                                0.75,
                                                0.1);
        
        VectorField mvvf = new VectorField(f, initialGuess.length, targetValues.length);
        MultivariateMatrixValuedFunction jacobian =
                Numerics.getJacobianFunction(mvvf, Optional.empty(), 3, 3, Parallelism.SEQUENTIAL);
        
        LevenbergMarquardtProblem lmProblem = new LevenbergMarquardtProblem(mvvf,
                                                                            jacobian,
                                                                            initialGuess,
                                                                            targetValues,
                                                                            constants);
        
        assertSame(mvvf, lmProblem.getF());
        assertSame(jacobian, lmProblem.getJacobian());
        assertArrayEquals(initialGuess, lmProblem.getInitialGuess());
        assertSame(constants, lmProblem.getConstants());
    }
}
