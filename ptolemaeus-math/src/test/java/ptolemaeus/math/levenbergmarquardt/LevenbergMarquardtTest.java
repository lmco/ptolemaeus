/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.math.commons.Numerics.square;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;

import org.hipparchus.linear.DecompositionSolver;
import org.hipparchus.linear.RealMatrix;
import org.hipparchus.util.FastMath;
import org.hipparchus.util.Pair;
import org.hipparchus.util.Precision;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.collections.SetUtils;
import ptolemaeus.math.CountableDoubleIterable;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.linear.householderreductions.RealQRDecomposition;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.linear.real.NVectorTest;
import ptolemaeus.math.realdomains.NBall;
import ptolemaeus.math.realdomains.NPoint;
import ptolemaeus.math.realdomains.RealNDomain;
import ptolemaeus.math.weightedleastsquares.StopType;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresConstants;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresSolution;

/**
 * Test for {@link LevenbergMarquardt}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
@Tag("numerical-methods-test")
public class LevenbergMarquardtTest {
    
    /**
     * A small number for comparing doubles. This is also used as the epsilon passed into
     * {@link LevenbergMarquardtConstants} instances.
     */
    private static final double EPSILON = 1e-6;
    
    /** Test {@link LevenbergMarquardt#solve(MultivariateVectorValuedFunction, double[], double[])}<br>
     * Solve f(x, y, z) = (x, xy, xyz) = (root(3), 3, 3 * root(3)). Solution: (root(3), root(3), root(3)) */
    @Test
    void testSimpleSolve() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
            input[0],
            input[0] * input[1],
            input[0] * input[1] * input[2]
        };
        
        double sqrt3 = FastMath.sqrt(3.0);
        double[] target = new double[] {
            sqrt3,
            sqrt3 * sqrt3,
            sqrt3 * sqrt3 * sqrt3
        };
        
        double[] initGuess        = new double[] { 1, 2, 1.5 };
        double[] expectedSolution = new double[] { sqrt3, sqrt3, sqrt3 };
        
        final WeightedLeastSquaresSolution wlss = LevenbergMarquardt.solve(f, initGuess, target);
        
        testSolution(f, target, expectedSolution, false, wlss);
    }
    
    /** Test {@link LevenbergMarquardt#computeDTheta(NVector, NVector, double)} */
    @Test
    void testComputeDTheta() {
        final NVector dTheta1 = new NVector(1_000, 0, 0, 0, 0, 0);
        final NVector dTheta2 = new NVector(1, 0, 0, 0, 0, 0);
        
        final double alpha1 = 2e-3;
        final double alpha2 = 1e-4;
        
        assertEquals(dTheta1.plus(dTheta2), LevenbergMarquardt.computeDTheta(dTheta1, dTheta2, alpha1));
        assertSame(dTheta1, LevenbergMarquardt.computeDTheta(dTheta1, dTheta2, alpha2));
    }
    
    /**
     * Test {@link LevenbergMarquardt#handleFirstIterationError(boolean)}
     */
    @Test
    void testFirstItrSpecialCase1() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0]                                             + input[1],
                    input[0] * input[0]                                  + input[1] * input[1],
                    input[0] * input[0] * input[0]                       + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the [2]
        };
                                                     
        double[] target = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, };
        double[] initGuess = new double[] { 4, 4, 4 };
        
        final LevenbergMarquardtConstants lmConstants = LevenbergMarquardtConstants.getSimple(target.length);
        final VectorField                 mvvf        = new VectorField(f, initGuess.length, target.length);
        final LevenbergMarquardtProblem problem = new LevenbergMarquardtProblem(mvvf, initGuess, target, lmConstants);
        final LevenbergMarquardt lm = new LevenbergMarquardt(problem, ignrd -> { }, ignrd -> { });
        
        final Pair<WeightedLeastSquaresSolution, NVector> answer = lm.handleFirstIterationError(false);
        
        assertNotNull(answer);
    }
    
    /**
     * Test {@link LevenbergMarquardt#handleFirstIterationError(boolean)}. This is a call
     * for coverage of the while loops as we cannot deliberately ensure each {@code while} option is exhausted.
     */
    @Test
    void testFirstItrSpecialCase2() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0]                                             + input[1],
                    input[0] * input[0]                                  + input[1] * input[1],
                    input[0] * input[0] * input[0]                       + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the [2]
        };
                                                     
        double[] target = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, };
        double[] initGuess = new double[] { 4, 4, 4 };
        
        final LevenbergMarquardtConstants lmConstants = LevenbergMarquardtConstants.getSimple(target.length);
        final VectorField mvvf = new VectorField(f, initGuess.length, target.length);
        final LevenbergMarquardtProblem problem = new LevenbergMarquardtProblem(mvvf, initGuess, target, lmConstants);
        
        final LevenbergMarquardt lm = Mockito.spy(new LevenbergMarquardt(problem, ignrd -> { }, ignrd -> { }));
        Mockito.when(lm.calculateNextPoint(ArgumentMatchers.any(), ArgumentMatchers.anyBoolean())).thenReturn(null);
        
        final Pair<WeightedLeastSquaresSolution, NVector> answer = lm.handleFirstIterationError(false);
        
        assertNull(answer);
    }
    
    /**
     * Test ability to solve find at least one solution to underdetermined non-linear least-squares problems
     */
    @Test
    void testUnderdetermined1() {
        final MultivariateVectorValuedFunction f = input -> new double[] { input[0] * input[1], // x * y
                                                                  input[1] * input[2], // y * z
        }; // this is a function f : R3 -> R2, so 3 variables and 2 constraints so the system is underdetermined
        
        double[] target = new double[] { 100, 100 };
        double[] initGuess = new double[] { 1, 2, 1 };
        double[] dxs = new double[] { 1e-6, 1e-6, 1e-6 };
        double[] expectedSolution = new double[] { 7.071067817654073, 14.142135616469947, 7.0710678176540736 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Test ability to solve find at least one solution to underdetermined non-linear least-squares problems. This one
     * does not have an exact solution unlike in {@link #testUnderdetermined1()}.
     */
    // CHECKSTYLE.OFF: LineLength
    @Test
    void testUnderdetermined2() {
        final MultivariateVectorValuedFunction f = input -> new double[] { 1 + square(input[0]) + square(input[1]) + square(input[2]),
                                                                  2 + square(input[1]) + square(input[2]) + square(input[3]),
                                                                  3 + square(input[2]) + square(input[3]) + square(input[4]) };
        /**
         * this is a function f : R5 -> R3, so 5 variables and 3 constraints so the system is underdetermined. The
         * 
         * function f = (1 + x^2 + y^2 + z^2,
         *               2 + y^2 + z^2 + u^2,
         *               3 + z^2 + u^2 + v^2)
         *
         * This function can only produce vectors which have positive components and our target values are negative, so
         * there is no exact solution.
         */
        double[] target = new double[] { -1, -1, -1 };
        double[] initGuess = new double[] { 10, 11, 12, 13, 14 };
        double[] dxs = new double[] { 1e-6, 1e-6, 1e-6, 1e-6, 1e-6 };
        double[] expectedSolution = new double[] {  0.35285595245239887,
                                                    0.2161148670976809,
                                                   -0.5057306140957629,
                                                    0.16080272200868562,
                                                    0.22282010487312176 };
        
        /* This expected solution results in an answer of (1.4269764129954154, 2.3283266052197282, 3.331269768574749)
         * which is obviously worse than f(0, 0, 0, 0, 0) = (1, 2, 3), but it's decently close given that there are
         * infinitely many solutions and considering that the initial SSR is ~588,827 and this solution has an SSR of
         * ~35 */
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, false, null);
    }
    // CHECKSTYLE.ON: LineLength
    
    /**
     * Test {@link LevenbergMarquardt} with a bounded input domain
     */
    @Test
    void testBoundedInput1() {
        final MultivariateVectorValuedFunction f =
                input -> new double[] { input[0], input[1], input[2], input[3], input[4] };
        
        final double[] target = new double[] { 2.0, 0.0, 0.0, 0.0, 0.0 };
        final double[] initGuess = new double[] { -1.0, 0.0, 0.0, 0.0, 0.0 };
        final double[] expectedSolution = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 };
        final RealNDomain inputDomain = new NBall(new NPoint(0.0, 0.0, 0.0, 0.0, 0.0), 1.0);
        
        testBoundedInput(f, target, initGuess, expectedSolution, inputDomain);
    }
    
    /**
     * Test {@link LevenbergMarquardt} with a bounded input domain
     */
    @Test
    void testBoundedInput2() {
        final MultivariateVectorValuedFunction f = input -> new double[] { input[0] * input[0],
                                                                  input[1] * input[1],
                                                                  input[2] * input[2],
                                                                  input[3] * input[3],
                                                                  input[4] * input[4] };
        
        final double[] target = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
        final double[] initGuess = new double[] { 1.25, 1.25, 1.25, 1.25, 1.25 };
        final double[] expectedSolution = new double[] { 0.5, 0.5, 0.5, 0.5, 0.5 };
        final RealNDomain inputDomain = new NBall(new NPoint(1.0, 1.0, 1.0, 1.0, 1.0), FastMath.sqrt(5.0) / 2);
        
        testBoundedInput(f, target, initGuess, expectedSolution, inputDomain);
    }
    
    /**
     * Test to ensure that we can find good solutions when we bound the input with an {@link RealNDomain}
     * 
     * @param f the function to optimize
     * @param targetValues the target values
     * @param initialGuess the initial guess
     * @param expectedSolution the expected solution
     * @param inputDomain the allowable input
     */
    private static void testBoundedInput(final MultivariateVectorValuedFunction f,
                                         final double[] targetValues,
                                         final double[] initialGuess,
                                         final double[] expectedSolution,
                                         final RealNDomain inputDomain) {
        final LevenbergMarquardtConstants lmConstants =
                new LevenbergMarquardtConstants(targetValues.length,
                                                null,
                                                EPSILON,
                                                null,
                                                1,
                                                WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER,
                                                new LevenbergMarquardtParameter(1D, 2D, 3D),
                                                0.75,
                                                0.1);
        
        final VectorField mvvf = new VectorField(f,
                                                                                           initialGuess.length,
                                                                                           targetValues.length,
                                                                                           inputDomain);
        final LevenbergMarquardtProblem lmProblem = new LevenbergMarquardtProblem(mvvf,
                                                                                  initialGuess,
                                                                                  targetValues,
                                                                                  lmConstants);
        final WeightedLeastSquaresSolution solution = LevenbergMarquardt.solve(lmProblem);
        double[] solutionArray = solution.getPoint();
        
        for (int i = 0; i < expectedSolution.length; i++) {
            if (!Precision.equals(expectedSolution[i], solutionArray[i], EPSILON)) {
                assertEquals(Arrays.toString(expectedSolution), Arrays.toString(solutionArray));
            }
        }
        
        assertTrue(inputDomain.contains(new NPoint(solutionArray)),
                   () -> String.format("The input domain%n%s%ndid not include the solution point %s",
                                       inputDomain,
                                       solution));
    }
    
    // Univariate scalar valued functions
    
    /**
     * Find the solution to 3x<sup>3</sup> - 2x<sup>2</sup> - 7x + 1 = -10, starting at x = -2
     */
    @Test
    public void test1() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    (3 * input[0] * input[0] * input[0])
                  - (2 * input[0] * input[0])
                  - (7 * input[0])
                  + 1
        };
        
        double[] target = new double[] { -10.0 };
        double[] initGuess = new double[] { -2.0 };
        double[] dxs = new double[] { 1e-5 };
        double[] expectedSolution = new double[] { -1.7870904031935957 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Find the solution to 3x<sup>3</sup> - 2x<sup>2</sup> - 7x + 1 = 0, starting at x = -2.0. This finds the
     * left-most zero.
     */
    @Test
    public void test2() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    (3 * input[0] * input[0] * input[0])
                  - (2 * input[0] * input[0])
                  - (7 * input[0])
                  + 1
        };
        
        double[] target = new double[] { 0.0 };
        double[] initGuess = new double[] { -2.0 };
        double[] dxs = new double[] { 1e-5 };
        double[] expectedSolution = new double[] { -1.3095268389949772 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Find the solution to 3x<sup>3</sup> - 2x<sup>2</sup> - 7x + 1 = 0, starting at x = -0.5. This finds the middle
     * zero.
     */
    @Test
    public void test3() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    (3 * input[0] * input[0] * input[0])
                  - (2 * input[0] * input[0])
                  - (7 * input[0])
                  + 1
        };
            
        double[] target = new double[] { 0.0 };
        double[] initGuess = new double[] { -0.5 };
        double[] dxs = new double[] { 1e-5 };
        double[] expectedSolution = new double[] { 0.13851418946043306 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Find the solution to 3x<sup>3</sup> - 2x<sup>2</sup> - 7x + 1 = 0, starting at x = 2.0. This finds the
     * right-most zero.
     */
    @Test
    public void test4() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    (3 * input[0] * input[0] * input[0])
                  - (2 * input[0] * input[0])
                  - (7 * input[0])
                  + 1
        };
        
        double[] target = new double[] { 0.0 };
        double[] initGuess = new double[] { 2.0 };
        double[] dxs = new double[] { 1e-5 };
        double[] expectedSolution = new double[] { 1.8376791748561765 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    // Multivariable vector-valued functions
    
    /**
     * Solve f(x, y, z) = (x, y, z) = (root(3),root(3),root(3))
     */
    @Test
    public void test5() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0],
                    input[1],
                    input[2]
        };
        
        double sqrt3 = FastMath.sqrt(3.0);
        double[] target = new double[] { sqrt3, sqrt3, sqrt3 };
        double[] initGuess = new double[] { 0.1, 0.1, 0.1 };
        double[] dxs = new double[] { 1e-3, 1e-3, 1e-3 };
        double[] expectedSolution = new double[] { sqrt3, sqrt3, sqrt3 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Solve f(x, y, z) = (x, xy, xyz) = (root(3), 3, 3 * root(3)). Solution: (root(3), root(3), root(3))
     */
    @Test
    public void test6() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0],
                    input[0] * input[1],
                    input[0] * input[1] * input[2]
        };
        
        double sqrt3 = FastMath.sqrt(3.0);
        double[] target = new double[] {
                sqrt3,
                sqrt3 * sqrt3,
                sqrt3 * sqrt3 * sqrt3
        };
        
        double[] initGuess = new double[] { 1, 2, 1.5 };
        double[] dxs = new double[] { 1e-3, 1e-3, 1e-3 };
        double[] expectedSolution = new double[] { sqrt3, sqrt3, sqrt3 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Solve f(x, y) = (x^2 - 1, y^2 - 1) == (-1, -1); solution: (0, 0)
     */
    @Test
    public void test7() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0] * input[0] - 1,
                    input[1] * input[1] - 1
        };
            
        double[] target = new double[] { -1, -1 };
        double[] initGuess = new double[] { 1.0, 1.0 };
        double[] dxs = new double[] { 1e-1, 1e-1 };
        double[] expectedSolution = new double[] { 6.0520908574922E-4, 6.0520908574922E-4 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /** Solve f(x, y) = (sin(x), cos(y)) = (sqrt(2) / 2, sqrt(2) / 2); solution: (n * pi / 4, m * pi / 4) <br>
     * 
     * The periodic and symmetric nature of this function provide a unique challenge for the optimization: it's very
     * easy for it to get stuck in a periodic iteration, and the solutions that we find are just as likely to be small
     * as they are large.<br>
     * 
     * For instance, in this test, using an initial guess of (0, 0) results in a periodic iteration, so we use (0,
     * 1e-7). */
    @Test
    public void test8() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    FastMath.sin(input[0]),
                    FastMath.cos(input[1])
        };
        
        final double root2On2 = FastMath.sqrt(2.0) / 2.0;
        double[] target = new double[] { root2On2, root2On2 };
        double[] initGuess = new double[] { 0.0, 1e-7 };
        double[] dxs = new double[] { 1e-3, 1e-3 };
        
        final double piOn4 = FastMath.PI / 4;
        double[] expectedSolution = new double[] { piOn4, 1464441.6344983845 };
        /* This is a huge number but it does satisfy cos(y) == sqrt(2) / 2, as demonstrated in the next line. It also
         * demonstrates the danger of using a numerical optimizer with initial conditions at or near a local maximum. */
        assertEquals(root2On2, FastMath.cos(expectedSolution[1]), EPSILON);
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    // Over-determined systems
    
    /**
     * Non-linear least squares on an over-determined system:
     * f(x) = (x, x<sup>2</sup>, x<sup>3</sup>, x<sup>4</sup>, x<sup>5</sup>) = (1, 1, 1, 1, 1)
     */
    @Test
    public void test9() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0],
                    input[0] * input[0],
                    input[0] * input[0] * input[0],
                    input[0] * input[0] * input[0] * input[0],
                    input[0] * input[0] * input[0] * input[0] * input[0]
        };
        
        double[] target = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0 };
        double[] initGuess = new double[] { 2 };
        double[] dxs = new double[] { 1e-2 };
        double[] expectedSolution = new double[] { 1.0 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, true, null);
    }
    
    /**
     * Non-linear least squares on an over-determined system:
     * 
     * f(x) = (x, x<sup>2</sup>, x<sup>3</sup>, x<sup>4</sup>, x<sup>5</sup>) = (2, 2, 2, 2, 2)
     *
     * Obviously there is no exact solution, but this minimizes the square of the residuals as Least Square methods are
     * supposed to so!
     */
    @Test
    public void test10() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0],
                    input[0] * input[0],
                    input[0] * input[0] * input[0],
                    input[0] * input[0] * input[0] * input[0],
                    input[0] * input[0] * input[0] * input[0] * input[0]
        };
            
        double[] target = new double[] { 2.0, 2.0, 2.0, 2.0, 2.0, };
        double[] initGuess = new double[] { 1 };
        double[] dxs = new double[] { 1e-2 };
        double[] expectedSolution = new double[] { 1.1879136552698164 };
        
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, false,
                               WeightedLeastSquaresConstants.QR_SOLVER);
        testLevenbergMarquardt(f, target, initGuess, dxs, null, 100_000, expectedSolution, false,
                               WeightedLeastSquaresConstants.SVD_SOLVER);
    }
    
    /**
     * Non-linear least squares on an over-determined system:
     * 
     * f(x, y, z) =
     * (x + y,
     *  x<sup>2</sup> + y<sup>2</sup>,
     *  x<sup>3</sup> + y<sup>2</sup>,
     *  x<sup>4</sup> + y<sup>2</sup>,
     *  x<sup>5</sup> + y + z)
     *  = (0, 0, 0, 0, 0)
     *
     * The solution is (x, y, z) = (0, 0, 0)
     */
    @Test
    public void test11a() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0]                                             + input[1],
                    input[0] * input[0]                                  + input[1] * input[1],
                    input[0] * input[0] * input[0]                       + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the [2]
        };
        
        double[] target = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, };
        double[] initGuess = new double[] { 4, 4, 4 };
        double[] expectedSolution = new double[] {  2.79490934516929E-4,
                                                   -2.794909376747606E-4,
                                                    2.7949094706207966E-4 };
        
        testLevenbergMarquardt(f, target, initGuess, null, null, 100_000, expectedSolution, false,
                               WeightedLeastSquaresConstants.QR_SOLVER);
        testLevenbergMarquardt(f, target, initGuess, null, null, 100_000, expectedSolution, false,
                               WeightedLeastSquaresConstants.SVD_SOLVER);
    }
    
    /**
     * This is the same as {@link #test11a()}, but we use {@link LevenbergMarquardtConstants#getSimple(int)}. We
     * actually get better results.
     */
    @Test
    public void test11aSimpleConstants() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0]                                             + input[1],
                    input[0] * input[0]                                  + input[1] * input[1],
                    input[0] * input[0] * input[0]                       + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the [2]
        };
        
        double[] target = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, };
        double[] initGuess = new double[] { 4, 4, 4 };
        double[] expectedSolution = new double[] {  2.794874155807269E-4,
                                                   -2.794874187331331E-4,
                                                    2.794874281266671E-4 };
        
        final LevenbergMarquardtConstants lmConstants = LevenbergMarquardtConstants.getSimple(target.length);
        final VectorField mvvf = new VectorField(f, initGuess.length, target.length);
        final LevenbergMarquardtProblem problem = new LevenbergMarquardtProblem(mvvf, initGuess, target, lmConstants);
        final WeightedLeastSquaresSolution solution = LevenbergMarquardt.solve(problem);
        
        testSolution(mvvf, target, expectedSolution, true, solution);
    }
    
    /**
     * Non-linear least squares on an over-determined system:
     * 
     * f(x, y, z) =
     * (x + y,
     *  x<sup>2</sup> + y<sup>2</sup>,
     *  x<sup>3</sup> + y<sup>2</sup>,
     *  x<sup>4</sup> + y<sup>2</sup>,
     *  x<sup>5</sup> + y + z)
     *  = (0, 0, 0, 0, 0)
     *
     * The same as 11a, but we start with a much better guess
     */
    @Test
    public void test11b() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0]                                             + input[1],
                    input[0] * input[0]                                  + input[1] * input[1],
                    input[0] * input[0] * input[0]                       + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the `2`
        };
        
        double[] target = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, };
        double[] initGuess = new double[] { 1, 1, 1 };
        double[] expectedSolution = new double[] {  5.506537709165448E-4,
                                                   -5.506540253415648E-4,
                                                    5.506547873939853E-4 };
        
        testLevenbergMarquardt(f, target, initGuess, null, null, 100_000, expectedSolution, false, null);
    }
    
    /**
     * Non-linear least squares on an over-determined system:
     * 
     * f(x, y, z) =
     * (x + y,
     *  x<sup>2</sup> + y<sup>2</sup>,
     *  x<sup>3</sup> + y<sup>2</sup>,
     *  x<sup>4</sup> + y<sup>2</sup>,
     *  x<sup>5</sup> + y + z)
     *  = (10, 20, 30, 40, 50)
     *
     * There is no exact solution.  The algorithm minimizes the sum of the squares of the residuals.
     */
    @Test
    public void test11c() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0]                                             + input[1],
                    input[0] * input[0]                                  + input[1] * input[1],
                    input[0] * input[0] * input[0]                       + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                    input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the `2`
        };
        
        double[] target = new double[] { 10.0, 20.0, 30.0, 40.0, 50.0, };
        double[] initGuess = new double[] { 1, 1, 1 };
        double[] expectedSolution = new double[] {  2.256816550859772,
                                                   -3.915035581558753,
                                                   -4.628815950099907 };
        
        testLevenbergMarquardt(f, target, initGuess, null, null, 100_000, expectedSolution, false, null);
    }
    
    /**
     * Non-linear least squares on an over-determined system:
     */
    @Test
    public void test12() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                    input[0] + 1,
                    input[0] + 2,
                    input[0] + 3,
                    input[0] + 4,
                    input[0] + 5,
                    input[0] + 6,
                    input[0] + 7,
                    input[0] + 8,
                    input[0] + 9,
                    input[0] + 10,
                    input[0] + 11,
                    input[0] + 12,
                    input[0] + 13,
                    input[0] + 14,
                    input[0] + 15,
                    input[0] + 16,
                    input[0] + 17,
                    input[0] + 18,
                    input[0] + 19,
                    input[0] + 20,
                    input[0] + 21,
                    input[0] + 22,
                    input[0] + 23,
                    input[0] + 24,
                    input[0] + 25,
        };
        
        double[] target = new double[] {
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                30.0 // note that this is non-zero
        };
        
        double[] initGuess = new double[] {
                20
        };
        
        double[] weights1 = new double[] {
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0
        };
        
        double[] weights2 = new double[] {
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                0.000001,
                1
        };
        
        WeightedLeastSquaresSolution solution1 = getTestSolution(f, target, initGuess, null, weights1, 100_000, null);
        WeightedLeastSquaresSolution solution2 = getTestSolution(f, target, initGuess, null, weights2, 100_000, null);
        
        assertEquals(16.8, solution1.getResiduals()[24], 2e-7);
        assertEquals(4.1998992022129755E-4, solution2.getResiduals()[24], 2e-7);
        assertTrue(solution1.getSumSquareResiduals() < solution2.getSumSquareResiduals());
        /*
         * we see here that dramatically increasing the weight of an element dramatically improves the residual of 'f'
         * at that point. Manual inspection of the solutions shows that every other residual was made worse to make the
         * last one better which is exactly what we were looking for
         */
    }

    /**
     * Test the case where we run out of iterations.
     * 
     * Also test {@link LevenbergMarquardt#getTraceMessage(WeightedLeastSquaresSolution)},
     * and exception-handling by {@link LevenbergMarquardt#calculateNextPoint(WeightedLeastSquaresSolution, boolean)}.
     */
    @Test
    public void outOfIterations() {
        final MultivariateVectorValuedFunction f = input -> new double[] { 1 };
        double[] target = new double[] { -10.0 };
        double[] initGuess = new double[] { -2.0 };
        int maxIterations = 1;
        
        LevenbergMarquardtConstants lmConstants =
                new LevenbergMarquardtConstants(target.length,
                                                null,
                                                EPSILON,
                                                maxIterations,
                                                100,
                                                WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER,
                                                new LevenbergMarquardtParameter(1D, 2D, 3D),
                                                0.75,
                                                0.1);
        VectorField mvvf = new VectorField(f, 1, 1);
        LevenbergMarquardtProblem lmproblem = new LevenbergMarquardtProblem(mvvf, initGuess, target, lmConstants);
        
        LevenbergMarquardt lm = new LevenbergMarquardt(lmproblem, null, null);
        WeightedLeastSquaresSolution solution = lm.solve(false);
        
        assertEquals(StopType.MAX_ITERATIONS, solution.getStopType());
        
        // Test getTraceMessage()
        assertEquals("Best SSR: 121.00000000000000000000, current SSR: 121.00000000000000000000, " 
                + "lambda: 0.33333333333333330000, iters: 1, unimprovedIters: 1", 
                lm.getTraceMessage(solution));
        
        final Level levelWas = LogManager.getLogger(LevenbergMarquardt.class).getLevel();
        
        try {
            Configurator.setLevel(LevenbergMarquardt.class, Level.OFF);
            
            // Test exception-handling by calculateNextPoint()
            assertNull(lm.calculateNextPoint(null, true));
        }
        finally {
            Configurator.setLevel(LevenbergMarquardt.class, levelWas);
        }
    }

    
    /**
     * Test {@link LevenbergMarquardt} with the specified inputs
     * 
     * @param f the model function to produce predictions
     * @param targetValues the target values we want the output of f to match
     * @param initialGuess the initial guess for the parameters of f
     * @param dxs the dxs to use estimating values of the Jacobian of f
     * @param weights the weights {@link Matrix} to consider
     * @param maxIterations the max number of algorithm iterations
     * @param expectedSolution the expected solution
     * @param exact true if we are expecting an exact solution, false otherwise
     * @param decompFact a {@link Function} that maps {@link RealMatrix matrices} to
     *        {@link DecompositionSolver}s that we can use to solve systems of linear equations. If {@code null}, we use
     *        {@link WeightedLeastSquaresConstants#DEFAULT_DECOMP_SOLVER}.
     * @return the solution in case we want to do more with it
     */
    private static WeightedLeastSquaresSolution testLevenbergMarquardt(final MultivariateVectorValuedFunction f,
                                                           final double[] targetValues,
                                                           final double[] initialGuess,
                                                           final double[] dxs,
                                                           final double[] weights,
                                                           final Integer maxIterations,
                                                           final double[] expectedSolution,
                                                           final boolean exact,
                                                           final Function<RealMatrix, DecompositionSolver> decompFact) {
        WeightedLeastSquaresSolution solution = getTestSolution(f,
                                                                targetValues,
                                                                initialGuess,
                                                                dxs,
                                                                weights,
                                                                maxIterations,
                                                                decompFact);
        testSolution(f, targetValues, expectedSolution, exact, solution);
        return solution;
    }
    
    /**
     * Verify the correctness of a {@link WeightedLeastSquaresSolution}
     * 
     * @param f the model function to produce predictions
     * @param targetValues the target values we want the output of f to match
     * @param expectedSolution the expected solution
     * @param exact true if we are expecting an exact solution, false otherwise
     * @param solution the solution to test
     */
    private static void testSolution(final MultivariateVectorValuedFunction f,
                                     final double[] targetValues,
                                     final double[] expectedSolution,
                                     final boolean exact,
                                     final WeightedLeastSquaresSolution solution) {
        testSolution(f, targetValues, expectedSolution, exact, solution.getPoint(), solution.getStopType());
    }
    
    /**
     * Verify the correctness of a {@link WeightedLeastSquaresSolution}
     * 
     * @param f the model function to produce predictions
     * @param targetValues the target values we want the output of f to match
     * @param expectedSolution the expected solution
     * @param exact true if we are expecting an exact solution, false otherwise
     * @param solutionArray the solution point
     * @param type the {@link StopType}
     */
    static void testSolution(final MultivariateVectorValuedFunction f,
                             final double[] targetValues,
                             final double[] expectedSolution,
                             final boolean exact,
                             final double[] solutionArray,
                             final StopType type) {
        final String testName = TestUtils.determineCallingTestMethod().orElse("unable to determine test name");
        
        if (exact) {
            final Collection<Executable> assertions = new ArrayDeque<>();
            assertions.add(() -> assertEquals(StopType.EPSILON_CONVERGENCE, type));
            final double[] answer = f.apply(solutionArray);
            for (int i = 0; i < answer.length; i++) {
                if (!Precision.equals(targetValues[i], answer[i], EPSILON)) {
                    assertions.add(() -> assertEquals(Arrays.toString(targetValues),
                                                      Arrays.toString(answer),
                                                      "function applied to solution"));
                    break;
                }
            }
            
            assertAll(testName, assertions);
        }
        
        for (int i = 0; i < solutionArray.length; i++) {
            if (!Precision.equals(expectedSolution[i], solutionArray[i], 2 * EPSILON)) {
                final WeightedLeastSquaresSolution expectedWLS = new WeightedLeastSquaresSolution(f,
                                                                                                  expectedSolution,
                                                                                                  targetValues);
                final WeightedLeastSquaresSolution actualWLS = new WeightedLeastSquaresSolution(f,
                                                                                                solutionArray,
                                                                                                targetValues);
                
                final String msg = String.format("Test:%n%s%n"
                                               + "expected SSR: %.20f%n"
                                               + "  actual SSR: %.20f%n"
                                               + "If actual is less than expected, then the solution improved.",
                                               testName,
                                               expectedWLS.getSumSquareResiduals(),
                                               actualWLS.getSumSquareResiduals());
                
                assertEquals(Arrays.toString(expectedSolution), Arrays.toString(solutionArray), msg);
            }
        }
    }
    
    /**
     * Test {@link LevenbergMarquardt} with the specified inputs
     * 
     * @param f the model function to produce predictions
     * @param targetValues the target values we want the output of f to match
     * @param initialGuess the initial guess for the parameters of f
     * @param dxs the dxs to use estimating values of the Jacobian of f
     * @param weights the weights {@link Matrix} to consider
     * @param maxIterations the max number of algorithm iterations
     * @param decompFact a {@link Function} that maps {@link RealMatrix matrices} to
     *        {@link DecompositionSolver}s that we can use to solve systems of linear equations. If {@code null}, we use
     *        {@link WeightedLeastSquaresConstants#DEFAULT_DECOMP_SOLVER}.
     * @return the solution in case we want to do more with it
     */
    static WeightedLeastSquaresSolution getTestSolution(final MultivariateVectorValuedFunction f,
                                                        final double[] targetValues,
                                                        final double[] initialGuess,
                                                        final double[] dxs,
                                                        final double[] weights,
                                                        final Integer maxIterations,
                                                        final Function<RealMatrix, DecompositionSolver> decompFact) {
        
        LevenbergMarquardtConstants lmConstants =
                new LevenbergMarquardtConstants(targetValues.length,
                                                weights,
                                                EPSILON,
                                                maxIterations,
                                                100,
                                                decompFact,
                                                new LevenbergMarquardtParameter(1D, 2D, 3D),
                                                0.75,
                                                0.1);
        
        VectorField mvvf = new VectorField(f, initialGuess.length, targetValues.length);
        MultivariateMatrixValuedFunction jacobian = Numerics.getJacobianFunction(mvvf,
                                                                                 Optional.ofNullable(dxs),
                                                                                 initialGuess.length,
                                                                                 targetValues.length,
                                                                                 Parallelism.SEQUENTIAL);
        LevenbergMarquardtProblem lmproblem =
                new LevenbergMarquardtProblem(mvvf, jacobian, initialGuess, targetValues, lmConstants);
        
        
        /* The following set is used to create a Consumer that captures all guesses of the algorithm. We then do some
         * validation based on the size and composition of this set. */
        NavigableSet<WeightedLeastSquaresSolution> allSolns = SetUtils.emptyNavigable(); // modified in place!!
        
        AtomicInteger counter = new AtomicInteger(0);
        AtomicReference<WeightedLeastSquaresSolution> finalBest = new AtomicReference<>(null);
        Consumer<WeightedLeastSquaresSolution> finalBestConsumer = wlss -> {
            /* this should capture the final best solution and it counts how many times it has been called. We do this
             * so so that we can ensure that is has been called exactly once and that that call time was indeed to
             * capture the final best solution */
            counter.incrementAndGet();
            finalBest.set(wlss);
        };
        
        // with the above Consumers, we're counting the total number of calls and capturing the guesses of each call in 
        final WeightedLeastSquaresSolution wlss = LevenbergMarquardt.solve(lmproblem,
                                                                           allSolns::add,
                                                                           finalBestConsumer,
                                                                           true);
        assertAll("A Consumer check failed",
                  () -> assertFalse(allSolns.isEmpty(),    "The currentBestConsumer was never called"),
                  () -> assertSame(wlss, allSolns.first(), "The last current-best was not the same as the answer"),
                  () -> assertSame(wlss, finalBest.get(),  "The final best was not the same as the answer"),
                  () -> assertEquals(1, counter.get(),     "The finalBestConsumer was called more than once"));
        return wlss;
    }
    
    /** For the given {@link VectorField} and {@link DecompositionSolver} factory, generate random initial guesses and
     * random target values and record how long it takes to solve. In addition, sum the {@code SSR}s of the solution
     * when we don't achieve epsilon convergence.
     * 
     * 
     * @param vf the function for which we're running a {@link LevenbergMarquardt} optimization
     * @param decompSolverFactory the {@link DecompositionSolver} factory */
    private static void runPerf(final VectorField vf,
                                final Function<RealMatrix, DecompositionSolver> decompSolverFactory) {
        final double epsilon       = 1e-9;
        final int    maxIters      = 1_000;
        final int    maxUnimpIters = 100;
        
        final double norm           = 10.0;
        final int    numSeeds       = 100;
        final long   seedSeed       = 8675_309L;
        final int    numRunsPerSeed = 10;
        
        final int iDim = vf.getInputDimension();
        final int oDim = vf.getOutputDimension();
        
        System.out.format("(%dx%d) problem, num seeds: %d, num runs per seed: %d%n%n",
                          iDim, oDim, numSeeds, numRunsPerSeed);
        
        for (int i = 0; i < numRunsPerSeed; i++) {
            final int    finalI   = i;
            final long[] seeds    = CountableDoubleIterable.monteCarlo(1.0, 1_000_000.0, seedSeed, numSeeds)
                                                           .stream()
                                                           .map(scale -> (finalI + 1) * scale * seedSeed)
                                                           .mapToLong(FastMath::round)
                                                           .distinct()
                                                           .toArray();
            
            long   sumNanos     = 0L;
            double sumSumSqrRes = 0.0;
            
            for (final long seed : seeds) {
                final double[] initialGuess = NVectorTest.randomNVector(iDim, norm, seed * seed).getDataRef();
                final double[] targetValues = NVectorTest.randomNVector(oDim, norm,        seed).getDataRef();
                
                final double[] weights = LevenbergMarquardtConstants.getDoubleArrayFilled(oDim, 1.0);
                final LevenbergMarquardtConstants lmc =
                        new LevenbergMarquardtConstants(oDim,
                                                        weights,
                                                        epsilon,
                                                        maxIters,
                                                        maxUnimpIters,
                                                        decompSolverFactory,
                                                        LevenbergMarquardtParameter.getDefault(),
                                                        LevenbergMarquardtConstants.DEFAULT_ALPHA_EASY,
                                                        LevenbergMarquardtConstants.DEFAULT_H);
                
                final LevenbergMarquardtProblem problem =
                        new LevenbergMarquardtProblem(vf, initialGuess, targetValues, lmc);
                
                final long before = System.nanoTime();
                final WeightedLeastSquaresSolution soln = LevenbergMarquardt.solve(problem);
                final long after = System.nanoTime();
                
                final double dt = after - before;
                
                sumNanos += dt;
                
                if (soln.getStopType() != StopType.EPSILON_CONVERGENCE) {
                    sumSumSqrRes += soln.getSumSquareResiduals();
                }
                
                // System.out.format("Stop type: %30s, iters: %10d, SSR: %25.20f, dt: %20.20f%n",
                //                   soln.getStopType(),
                //                   soln.getIterationsToStop(),
                //                   soln.getSumSquareResiduals(),
                //                   dt * 1e-9);
            }
            
            System.out.format("SSR: %25.20f, dt: %20.20f%n", sumSumSqrRes, sumNanos * 1e-9);
        }
    }
    
    /** Run a 6x6 optimization and measure time taken
     * 
     * @param args ignored */
    public static void main(final String[] args) {
        final MultivariateVectorValuedFunction f = x -> new double[] { x[0] * x[1] * x[2],   // uvw
                                                                       x[1] * x[2] * x[3],   // vwx
                                                                       x[2] * x[3] * x[4],   // wxy
                                                                       x[3] * x[4] * x[5],   // xyz
                                                                       x[0] - x[2] - x[4],   // u - w - y
                                                                       x[1] - x[3] - x[5] }; // v - x - z
        
        final int dim = 6;
        final VectorField vf  = new VectorField(f, dim, dim);
        
        // final Function<RealMatrix, DecompositionSolver> decompSolverFactory =
        //         A -> new SingularValueDecomposition(A).getSolver();
        final Function<RealMatrix, DecompositionSolver> decompSolverFactory = RealQRDecomposition::of;
        
        runPerf(vf, decompSolverFactory);
    }
}
