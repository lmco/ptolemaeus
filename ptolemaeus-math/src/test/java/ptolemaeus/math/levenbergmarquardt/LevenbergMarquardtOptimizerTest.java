/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.levenbergmarquardt;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.Optional;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.RealMatrix;
import org.hipparchus.linear.RealVector;
import org.hipparchus.optim.ConvergenceChecker;
import org.hipparchus.optim.nonlinear.vector.leastsquares.LeastSquaresFactory;
import org.hipparchus.optim.nonlinear.vector.leastsquares.LeastSquaresOptimizer;
import org.hipparchus.optim.nonlinear.vector.leastsquares.LeastSquaresProblem;
import org.hipparchus.optim.nonlinear.vector.leastsquares.LeastSquaresProblem.Evaluation;
import org.hipparchus.optim.nonlinear.vector.leastsquares.MultivariateJacobianFunction;
import org.hipparchus.util.FastMath;

import ptolemaeus.commons.Parallelism;
import ptolemaeus.math.commons.Numerics;
import ptolemaeus.math.functions.MultivariateMatrixValuedFunction;
import ptolemaeus.math.functions.MultivariateVectorValuedFunction;
import ptolemaeus.math.functions.VectorField;
import ptolemaeus.math.linear.real.NVector;
import ptolemaeus.math.weightedleastsquares.StopType;
import ptolemaeus.math.weightedleastsquares.WeightedLeastSquaresConstants;

/** Test {@link LevenbergMarquardtOptimizer}. Note that we have few tests because the actual algorithm is thoroughly
 * tested in {@link LevenbergMarquardtTest}.
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class LevenbergMarquardtOptimizerTest {
    
    /** Test {@link LevenbergMarquardtOptimizer#LevenbergMarquardtOptimizer()} and
     * {@link LevenbergMarquardtOptimizer#LevenbergMarquardtOptimizer(double, int, int, double[], Function)} */
    @Test
    void testConstructors() {
        final LevenbergMarquardtOptimizer lmo1 = new LevenbergMarquardtOptimizer();
        assertEquals(1e-6, lmo1.getEpsilon());
        assertEquals(1_000, lmo1.getMaxNumIterations());
        assertEquals(100, lmo1.getMaxNumUnimprovedIters());
        assertNull(lmo1.getDXs());
        assertSame(WeightedLeastSquaresConstants.DEFAULT_DECOMP_SOLVER, lmo1.getDecompositionFactory());
        
        final double[] dxs = new double[] { 1.0, 2.0, 3.0 };
        final LevenbergMarquardtOptimizer lmo2 =
                new LevenbergMarquardtOptimizer(1e-8, 1_000, 10, dxs, WeightedLeastSquaresConstants.SVD_SOLVER);
        assertEquals(1e-8, lmo2.getEpsilon());
        assertEquals(1_000, lmo2.getMaxNumIterations());
        assertEquals(10, lmo2.getMaxNumUnimprovedIters());
        assertArrayEquals(dxs, lmo2.getDXs());
        assertSame(WeightedLeastSquaresConstants.SVD_SOLVER, lmo2.getDecompositionFactory());
    }
    
    /** Test {@link LevenbergMarquardtOptimizer#optimize(LeastSquaresProblem)} where there is an exact solution. This is
     * equivalent to {@link LevenbergMarquardtTest#test9()}.
     * 
     * @see LevenbergMarquardtTest#test9() */
    @Test
    void testOptimizeExact() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
            input[0],
            input[0] * input[1],
            input[0] * input[1] * input[2]
        };
        
        double sqrt3 = FastMath.sqrt(3.0);
        double[] target = new double[] {
                sqrt3,
                sqrt3 * sqrt3,
                sqrt3 * sqrt3 * sqrt3
        };
        
        double[] initGuess = new double[] { 1, 2, 1.5 };
        double[] dxs = new double[] { 1e-3, 1e-3, 1e-3 };
        double[] expectedSolution = new double[] { sqrt3, sqrt3, sqrt3 };
        
        testOptimize(f, initGuess, target, dxs, expectedSolution, true);
    }
    
    /** Test {@link LevenbergMarquardtOptimizer#optimize(LeastSquaresProblem)} where there is no exact solution. This is
     * equivalent to {@link LevenbergMarquardtTest#test11c()}.
     * 
     * @see LevenbergMarquardtTest#test11c() */
    @Test
    void testOptimizeInexact() {
        final MultivariateVectorValuedFunction f = input -> new double[] {
                input[0]                                             + input[1],
                input[0] * input[0]                                  + input[1] * input[1],
                input[0] * input[0] * input[0]                       + input[1] * input[1],
                input[0] * input[0] * input[0] * input[0]            + input[1] * input[1],
                input[0] * input[0] * input[0] * input[0] * input[0] + input[1] + input[2] // Note the `2`
        };
        
        final double[] target = new double[] { 10.0, 20.0, 30.0, 40.0, 50.0, };
        final double[] initGuess = new double[] { 1, 1, 1 };
        final double[] dxs = new double[] { 1e-2, 1e-2, 1e-2 };
        final double[] expectedSolution = new double[] { 2.256816552103954, -3.9150355760230675, -4.628816117011404 };
        /* the answer that we got here is actually worse than the original - LevenbergMarquardtTest.test11c() - but not
         * by anything terrible. It's likely due to the change in function; i.e., internally, LMOpt solves for a
         * vector-valued function minus the target values set equal to zero, or, f(x) - y = 0, rather than f(x) = y */
        
        testOptimize(f, initGuess, target, dxs, expectedSolution, false);
    }
    
    /** Test {@link LevenbergMarquardtOptimizer}
     * 
     * @param f the model function to produce predictions
     * @param initGuess the initial guess
     * @param target the target values we want the output of f to match
     * @param dxs the dxs to use when computing the Jacobian
     * @param expectedSolution the expected solution
     * @param exact true if we are expecting an exact solution, false otherwise */
    private static void testOptimize(final MultivariateVectorValuedFunction f,
                                     final double[] initGuess,
                                     final double[] target,
                                     final double[] dxs,
                                     final double[] expectedSolution,
                                     final boolean exact) {
        final VectorField mvvf = new VectorField(f, initGuess.length, target.length);
        
        final MultivariateMatrixValuedFunction jacobian =
                Numerics.getJacobianFunction(mvvf,
                                                          Optional.ofNullable(dxs),
                                                          initGuess.length,
                                                          target.length,
                                                          Parallelism.SEQUENTIAL);
        
        final MultivariateJacobianFunction mfj =
                v -> {
                    final RealVector answerV = new NVector(mvvf.apply(v.toArray()));
                    final RealMatrix answerM = jacobian.apply(v.toArray());
                    return new org.hipparchus.util.Pair<>(answerV, answerM);
                };
        
        final RealVector observed = new NVector(target);
        final RealVector start = new NVector(initGuess);
        
        final ConvergenceChecker<Evaluation> cc = (ignored1, ignored2, ignored3) -> false; /* the LevenberMarquardt impl
                                                                                            * has its own internal
                                                                                            * convergence checking */
        
        final LeastSquaresProblem lsp = LeastSquaresFactory.create(mfj, observed, start, cc, 100_000, 100_000);
        final LeastSquaresOptimizer.Optimum opt = new LevenbergMarquardtOptimizer().optimize(lsp);
        
        final double[] actualSolution = opt.getPoint().toArray();
        
        LevenbergMarquardtTest.testSolution(mvvf,
                                            target,
                                            expectedSolution,
                                            exact,
                                            actualSolution,
                                            StopType.EPSILON_CONVERGENCE); /* we just need to pass something for
                                                                            * StopType. LevenbergMarquardtOptimizer
                                                                            * doesn't capture the actual StopType. */
    }
}
