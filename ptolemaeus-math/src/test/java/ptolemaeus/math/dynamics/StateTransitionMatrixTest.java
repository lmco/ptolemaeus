/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.dynamics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import org.hipparchus.complex.Complex;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.hipparchus.linear.FieldVector;
import org.hipparchus.linear.OrderedComplexEigenDecomposition;
import org.hipparchus.linear.RealMatrix;

import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.complex.ComplexMatrix;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;

/** Test {@link StateTransitionMatrix}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
class StateTransitionMatrixTest {
    
    /** A small value for comparing doubles */
    private static final double EPSILON = 1e-9;
    
    /** Test to show that {@link StateTransitionMatrix#getEigenDecomposition()} and
     * {@link StateTransitionMatrix#getEigenPairs()} work properly and agree with the original matrix */
    @Test
    void testEigenDecomp() {
        final RealMatrix stmMatrix = new Matrix(new double[][] { { 3.0, 0.0, 0.0 },
                                                                 { 0.0, 4.0, 0.0 },
                                                                 { 0.0, 0.0, 5.0 } });
        final StateTransitionMatrix stm = new StateTransitionMatrix(stmMatrix, EigenDecompositionEpsilons.DEFAULT);
        
        final OrderedComplexEigenDecomposition decomp = stm.getEigenDecomposition();
        final List<Complex> eValues = List.of(decomp.getEigenvalues());
        
        final ComplexMatrix asComplex = new ComplexMatrix(stmMatrix);
        stm.computeEigenPairs().forEach(pair -> {
            final int index = eValues.indexOf(pair.eigenvalue());
            assertTrue(index > -1, "Eigenvalue not found");
            
            final FieldVector<Complex> eVec = decomp.getEigenvector(index);
            assertEquals(eVec, pair.eigenvector());
            
            final FieldVector<Complex> eVecOptOn = asComplex.operate(eVec);
            final FieldVector<Complex> eVecScaled = eVec.mapMultiply(pair.eigenvalue());
            NumericsTest.assertEBEVectorsEqualRelative(eVecOptOn, eVecScaled, 1e-9);
        });
    }
    
    /** Test the constructor */
    @Test
    void testConstructor() {
        final RealMatrix stmMatrix = new Matrix(new double[][] { { 3.0, 0.0, 0.0 },
                                                                 { 0.0, 4.0, 0.0 },
                                                                 { 0.0, 0.0, 5.0 } });
        final StateTransitionMatrix stm = new StateTransitionMatrix(stmMatrix, EigenDecompositionEpsilons.DEFAULT);
        assertNotNull(stm.getEigenDecomposition());
        assertNotNull(stm.getEigenDecomposition()); // for coverage
        MatrixTest.assertMatrixEquals(stmMatrix, stm.getMatrix(), EPSILON);
        
        assertThrows(MathIllegalArgumentException.class, // thrown due to a non-square matrix
                     () -> new StateTransitionMatrix(new Matrix(new double [][] { { 1.0, 1.0, 1.0 } }),
                                                     EigenDecompositionEpsilons.DEFAULT));
    }
    
    /** Test {@link StateTransitionMatrix#getEigenPairs()} */
    @Test
    void testGetEigenPairs() {
        final DynamicalSystem ds = (dt, x0) -> new double[] { dt * x0[0],
                                                              dt * x0[1],
                                                              dt * x0[2],
                                                              dt * x0[3] };
        final StateTransitionMatrixFunction stmf = new StateTransitionMatrixFunction(ds);
        
        final StateTransitionMatrix stm = stmf.apply(123.0, new double[] { 1, 2, 3, 4 });
        
        assertEquals(4, stm.getEigenPairs().size());
        assertEquals(4, stm.getEigenPairs().size()); // dup. test for coverage
    }
}
