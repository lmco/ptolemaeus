/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.math.dynamics;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import org.hipparchus.linear.RealMatrix;

import ptolemaeus.math.commons.NumericsTest;
import ptolemaeus.math.linear.real.Matrix;
import ptolemaeus.math.linear.real.MatrixTest;
import ptolemaeus.math.linear.real.NVector;

/** Test {@link StateTransitionMatrixFunction}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class StateTransitionMatrixFunctionTest {
    
    /** A small value for comparing doubles */
    private static final double EPSILON = 1e-9;
    
    /** Test the {@link StateTransitionMatrix} that we create for the given {@link DynamicalSystem}, duration, and
     * initial condition.<br>
     * <br>
     * 
     * A {@link StateTransitionMatrix} predicts how a {@link DynamicalSystem} will evolve a given initial condition for
     * some duration. Given a linear system, this prediction is exact (to within some tolerance due to finite
     * precision). Thus, we can use the actual system to get an actual evolved value and compare it to the prediction.
     * <br>
     * <br>
     * This test method should really only be used with a non-zero duration when testing linear systems, as for
     * non-linear systems this method will only work for small {@code dt}. <br>
     * <br>
     * 
     * That said, writing a test with {@code dt == 0.0} ensures that the STM predicts zero change after zero seconds of
     * evolution, regardless of linearity, so a test with {@code dt == 0} is always a good idea. <br>
     * <br>
     * 
     * @param ds the {@link DynamicalSystem}
     * @param dt the duration
     * @param ic the initial conditions
     * @param tolerance the vector element-by-element equality tolerance */
    public static void testSTM(final DynamicalSystem ds, final double dt, final double[] ic, final double tolerance) {
        final StateTransitionMatrixFunction stmf = new StateTransitionMatrixFunction(ds);
        final double[] actual = ds.evaluate(dt, ic);
        final StateTransitionMatrix stm = stmf.apply(dt, ic);
        
        testSTM(stm, ic, actual, tolerance);
    }
    
    /** Test the {@link StateTransitionMatrix} for the given initial conditions
     * 
     * @param stm the {@link StateTransitionMatrix}
     * @param ic the initial conditions
     * @param actualArr the actual value against which we will test the {@link StateTransitionMatrix}'s operation
     * @param tolerance the vector element-by-element equality tolerance
     * 
     * @see #testSTM(DynamicalSystem, double, double[], double) */
    public static void testSTM(final StateTransitionMatrix stm,
                               final double[] ic,
                               final double[] actualArr,
                               final double tolerance) {
        final NVector actual = new NVector(actualArr);
        final NVector expected = new NVector(stm.getMatrix().operate(ic));
        NumericsTest.assertEBEVectorsEqualAbsolute(actual, expected, tolerance);
    }
    
    /** Test {@link StateTransitionMatrix} with an uncoupled linear {@link DynamicalSystem} */
    @Test
    void testUncoupledLinearSystem() {
        final DynamicalSystem ds = (t, x) -> new double[] { t * x[0], 10 * t * x[1], 100 * t * x[2] };
        final StateTransitionMatrixFunction stmf = new StateTransitionMatrixFunction(ds);
        final double[] initialConditions = new double[] { 1, 2, 3 };
        
        final StateTransitionMatrix stm1 = stmf.apply(1.0, initialConditions);
        assertNotNull(stm1.getEigenDecomposition());
        
        final RealMatrix expected1 = new Matrix(new double[][] { { 1.0,  0.0,   0.0 },
                                                                 { 0.0, 10.0,   0.0 },
                                                                 { 0.0,  0.0, 100.0 } });
        final RealMatrix actual1 = stm1.getMatrix();
        MatrixTest.assertMatrixEquals(expected1, actual1, EPSILON);
        
        final StateTransitionMatrix stm2 = stmf.apply(12.0, initialConditions);
        final RealMatrix expected2 = expected1.scalarMultiply(12.0);
        final RealMatrix actual2 = stm2.getMatrix();
        MatrixTest.assertMatrixEquals(expected2, actual2, 100 * EPSILON);
        
        testSTM(ds, 0.0, initialConditions, 0.0);
        testSTM(ds, 1.0, initialConditions, 1.16e-10);
        testSTM(ds, 12.0, initialConditions, 3.8e-8);
    }
    
    /** Test {@link StateTransitionMatrix} with an uncoupled quadratic {@link DynamicalSystem} */
    @Test
    void testUncoupledQuadraticSystem() {
        final DynamicalSystem ds = (t, x) -> new double[] { t * x[0] * x[0],
                                                            t * x[1] * x[1],
                                                            t * x[2] * x[2] };
        final StateTransitionMatrixFunction stmf = new StateTransitionMatrixFunction(ds);
        final double[] initialConditions = new double[] { 1, 2, 3 };
        
        final StateTransitionMatrix stm1 = stmf.apply(1.0, initialConditions);
        assertNotNull(stm1.getEigenDecomposition());
        
        /* the values of this Matrix are the values of df_i/dx_j evaluated at the initial conditions, where f_i is the
         * i-th equation in the dynamical system, and x_j is the j-th variable.  In this case, the partials are
         * 
         * 2*t*x_1 0       0
         * 0       2*t*x_2 0 
         * 0       0       2*t*x_3 */
        final RealMatrix expected1 = new Matrix(new double[][] { { 2.0, 0.0, 0.0 },
                                                                 { 0.0, 4.0, 0.0 },
                                                                 { 0.0, 0.0, 6.0 } });
        final RealMatrix actual1 = stm1.getMatrix();
        MatrixTest.assertMatrixEquals(expected1, actual1, EPSILON);
        
        final StateTransitionMatrix stm2 = stmf.apply(12.0, initialConditions);
        final RealMatrix expected2 = expected1.scalarMultiply(12.0);
        final RealMatrix actual2 = stm2.getMatrix();
        MatrixTest.assertMatrixEquals(expected2, actual2, EPSILON);
        
        testSTM(ds, 0.0, initialConditions, 0.0);
    }
    
    /** Test {@link StateTransitionMatrix} with a coupled linear {@link DynamicalSystem} */
    @Test
    void testCoupledLinearSystem() {
        final DynamicalSystem ds = (t, x) -> new double[] { t * (x[0] + x[1]),
                                                            t * (x[1] + x[2]),
                                                            t * (x[2] + x[0]) };
        final StateTransitionMatrixFunction stmf = new StateTransitionMatrixFunction(ds);
        final double[] initialConditions = new double[] { 1, 2, 3 };
        
        final StateTransitionMatrix stm1 = stmf.apply(1.0, initialConditions);
        assertNotNull(stm1.getEigenDecomposition());
        
        final RealMatrix expected1 = new Matrix(new double[][] { { 1.0, 1.0, 0.0 },
                                                                 { 0.0, 1.0, 1.0 },
                                                                 { 1.0, 0.0, 1.0 } });
        final RealMatrix actual1 = stm1.getMatrix();
        MatrixTest.assertMatrixEquals(expected1, actual1, EPSILON);
        
        final StateTransitionMatrix stm2 = stmf.apply(12.0, initialConditions);
        final RealMatrix expected2 = expected1.scalarMultiply(12.0);
        final RealMatrix actual2 = stm2.getMatrix();
        MatrixTest.assertMatrixEquals(expected2, actual2, EPSILON);
        
        testSTM(ds, 0.0, initialConditions, 0.0);
        testSTM(ds, 1.0, initialConditions, 1e-9);
        testSTM(ds, 12.0, initialConditions, 1e-8);
    }
    
    /** Test {@link StateTransitionMatrix} with a coupled nonlinear {@link DynamicalSystem} */
    @Test
    void testCoupledQuadraticSystem() {
        final DynamicalSystem ds = (t, x) -> new double[] { t * (x[0] + x[1]) * (x[0] + x[1]),
                                                            t * (x[1] + x[2]) * (x[1] + x[2]),
                                                            t * (x[2] + x[0]) * (x[2] + x[0]) };
        final StateTransitionMatrixFunction stmf = new StateTransitionMatrixFunction(ds);
        final double[] initialConditions = new double[] { 1, 2, 3 };
        
        final StateTransitionMatrix stm1 = stmf.apply(1.0, initialConditions);
        assertNotNull(stm1.getEigenDecomposition());
        
        /* the values of this Matrix are the values of df_i/dx_j evaluated at the initial conditions, where f_i is the
         * i-th equation in the dynamical system, and x_j is the j-th variable.  In this case, the partials are
         * 
         * 2*t*(x_0 + x_1) 2*t*(x_0 + x_1) 0
         * 0               2*t*(x_2 + x_3) 2*t*(x_2 + x_3)
         * 2*t*(x_1 + x_3) 0               2*t*(x_1 + x_3) */
        final RealMatrix expected1 = new Matrix(new double[][] { { 6.0, 6.0, 0.0 },
                                                                 { 0.0, 10.0, 10.0 },
                                                                 { 8.0, 0.0, 8.0 } });
        final RealMatrix actual1 = stm1.getMatrix();
        MatrixTest.assertMatrixEquals(expected1, actual1, EPSILON);
        
        final StateTransitionMatrix stm2 = stmf.apply(12.0, initialConditions);
        final RealMatrix expected2 = expected1.scalarMultiply(12.0);
        final RealMatrix actual2 = stm2.getMatrix();
        MatrixTest.assertMatrixEquals(expected2, actual2, EPSILON * 10);
        
        testSTM(ds, 0.0, initialConditions, 0.0);
    }
}
