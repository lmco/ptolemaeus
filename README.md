# What is it?

Ptolemaeus is currently composed of two sub-projects: `math`, and `commons`:

## `ptolemaeus.math`

A rigorous and well-sourced Java mathematics library that extends the [Hipparchus mathematics library](https://www.hipparchus.org/).

Some highlights (not exhaustive!):

### Real and Complex Linear Algebra
  * [ComplexMatrix](math/src/main/java/ptolemaeus/math/linear/complex/ComplexMatrix.java): a very efficient implementation of a complex matrix, including optional parallelization with level 2 and 3 BLAS.  Backed by two `double[][]`s, all work is done in real numbers, but it is compatible with `FieldMatrix<Complex>`
  * [ComplexVector](math/src/main/java/ptolemaeus/math/linear/complex/ComplexVector.java): a vector of `Complex` entries (TODO: use two `double[]`s rather than a `Complex[]`)
  * [Eigen](math/src/main/java/ptolemaeus/math/linear/eigen/Eigen.java): for computing the Eigenvalues and Eigenvectors of complex and real matrices
  * [Householder](math/src/main/java/ptolemaeus/math/linear/householderreductions.java): complex and real Householder transformations
  * [ComplexSchurDecomposition](math/src/main/java/ptolemaeus/math/linear/schur/ComplexSchurDecomposition.java): a complex Schur decomposition using the M-shifted large-bulge QR algorithm (TODOs include adding aggressive early deflation and support for tightly-coupled small-bulge-pair shifts)
  * [ComplexQRDecomposition](math/src/main/java/ptolemaeus/math/linear/householderreductions/ComplexQRDecomposition.java): a complex QR decomposition using `Householder` transformations
  * [ComplexHessenbergDecomposition](math/src/main/java/ptolemaeus/math/linear/householderreductions/ComplexHessenbergReduction.java): a very efficient implementation of complex Hessenberg decomposition using `Householder` transformations

### Hyper-Dimensional Geometry

Frameworks for and implementations of hyper-dimensional (N-dimensional) geometry
  * [NOrthotope](math/src/main/java/ptolemaeus/math/realdomains/NOrthotope.java): arbitrarily oriented orthotope (a.k.a. k-cell, hyper-rectangle, hyper-box, box, generalized hyper-cube, etc.); i.e., a shape whose vertices are the intersection of N orthogonal lines (see `NLineSegment`)
  * [NBall](math/src/main/java/ptolemaeus/math/realdomains/NBall.java): a shape defined by points in N-dimensions which are all equidistant from a center

### Weighted Non-Linear Least Squares
  * [LevenbergMarquardt](math/src/main/java/ptolemaeus/math/levenbergmarquardt/LevenbergMarquardt.java)
    * An implementation of the Levenberg-Marquardt with geodesic acceleration
    * The search-space may be restricted arbitrarily by specifying a `RealNDomain` like the aforementioned `NOrthotope` or `NBall` 

### Non-Linear Dynamics
  * [DynamicalSystem](math/src/main/java/ptolemaeus/math/dynamics/DynamicalSystem.java)
  * [StateTransitionMatrix](math/src/main/java/ptolemaeus/math/dynamics/StateTransitionMatrix.java)

### Function Definitions
A rich framework of multi-variate vector-valued and matrix-valued function definitions. These definitions are written to cooperate with the hyper-geometric shapes (domains) defined above, as you can specify functions as only having some limited domain; i.e., functions can be defined as we often do in formal proofs: `f: U -> V`
  * [VectorField](math/src/main/java/ptolemaeus/math/functions/VectorField.java)
  * [MultivariateVectorValuedFunction](math/src/main/java/ptolemaeus/math/functions/MultivariateVectorValuedFunction.java)
  * [MultivariateMatrixValuedFunction](math/src/main/java/ptolemaeus/math/functions/MultivariateMatrixValuedFunction.java)

### On Documentation
  Whenever a reference is used in the process of development, that source is included in the JavaDoc and, if possible, a hyperlink is provided.

## `ptolemaeus.commons`
A collection of common utilities and convenience methods/classes such as:
  * [MultiIndexMap](commons/src/main/java/ptolemaeus/commons/collections/multiindexmap/MultiIndexMap.java): an efficient implementation of `Map<Iterable<K>, V>`
  * [ConcurrentMemoizer](commons/src/main/java/ptolemaeus/commons/ConcurrentMemoizer.java): thread-safe, efficient caching for expensive operations
  * [UnmodifiableLinkedList](commons/src/main/java/ptolemaeus/commons/collections/UnmodifiableLinkedList.java)
  * [StreamUtils](commons/src/main/java/ptolemaeus/commons/collections/StreamUtils.java), [MapUtils](commons/src/main/java/ptolemaeus/commons/collections/MapUtils.java), and [SetUtils](commons/src/main/java/ptolemaeus/commons/collections/SetUtils.java) which - among other common static convenience methods - use `LinkedHashMap`/`LinkedHashSet`, rather than the default `HashMap`/`HashSet` that the base Java statics in `Stream`, `Collectors`, `Map` and `Set` use
  * [Validation](commons/src/main/java/ptolemaeus/commons/Validation.java): which holds common many parameter checks

## Guidelines and Rules
  * [the Code of Conduct](CODE_OF_CONDUCT.md),
  * [the Contribution Guidelines](CONTRIBUTING.md), and
  * [the Development Guidelines](DEVELOPMENT.md)

## What's with the name?

As discussed above, this library uses many `interface`s - and a few `abstract` and concrete `class`es - from the _Hipparchus_ mathematics library, and Hipparchus - the person - was a mathematician of antiquity who made many significant contributions to mathematics - trigonometry, in particular - astronomy, and several other fields.

Given that this library "comes after" the Hipparchus library, in a way, it felt apt to choose another mathematician from (approximately) the same period, who built on the work of Hipparchus; thus, _Ptolemy_, whose Latin name - Cladius _Ptolemaeus_ - is reminiscent of the name _Hipparchus_.