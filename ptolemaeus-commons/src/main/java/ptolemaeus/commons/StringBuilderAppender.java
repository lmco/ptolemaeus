/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons;

/** A {@link StringBuilderAppender} can append its data, as a String, to a {@link StringBuilder}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public interface StringBuilderAppender {
    
    /** the default indent to use with {@link #appendToStringBuilder(StringBuilder, int)} */
    String DEFAULT_INDENT = "    ";

    /** Visit this {@link StringBuilderAppender} and append its data to the provided {@link StringBuilder}
     * 
     * @param builder the {@link StringBuilder} to which we wish to append data
     * @param indentCount the size of the indent to use for this {@link StringBuilderAppender}'s data */
    void appendToStringBuilder(StringBuilder builder, int indentCount);

    /**
     * Create an indent for use in a {@link #appendToStringBuilder(StringBuilder, int)} implementation
     * 
     * @param indentCount the the size of the indent
     * @param indent the indent to use
     * @return a String that is {@code indent} repeated {@code indentCount} many times
     */
    static String getIndent(final int indentCount, final String indent) {
        return indent.repeat(indentCount);
    }
}
