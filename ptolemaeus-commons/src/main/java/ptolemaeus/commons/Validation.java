/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons;

import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;
import java.util.function.Supplier;

import org.hipparchus.util.Precision;

/** Common number validation checks
 *
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * @author Peter Davis, peter.m.davis@lmco.com */
public final class Validation {
    
    /** a format String for the {@link #requireEqualsTo} methods */
    private static final String RE      = "Parameter %s with value %s is not equal to %s";
    /** a format String for {@link #requireEqualsTo} for doubles */
    private static final String RE_TOL  = RE + " within the abs tolerance %f";
    /** a format String for the {@link #requireGreaterThan} methods */
    private static final String RG      = "Parameter %s with value %s is not greater than %s";
    /** a format String for the {@link #requireGreaterThanEqualTo} methods */
    private static final String RGE     = "Parameter %s with value %s is not greater than or equal to %s";
    /** a format String for the {@link #requireLessThan} methods */
    private static final String RL      = "Parameter %s with value %s is not less than %s";
    /** a format String for the {@link #requireLessThanEqualTo} methods */
    private static final String RLE     = "Parameter %s with value %s is not less than or equal to %s";
    /** a format String for {@link #requireTrue} */
    private static final String RT      = "Parameter %s is false but should be true";
    /** a format String for {@link #requireFalse} */
    private static final String RF      = "Parameter %s is true but should be false";
    /** a format String for {@link #requireFinite} */
    private static final String RFINITE = "Parameter %s is %s but should be finite";
    /** a format String for {@link #requireNonNaN} */
    private static final String RNN     = "Parameter %s is NaN";
    
    /** The number of allowed floating point numbers between {@code double}s (inclusive - i.e., adjacent {@code double}s
     * are {@code 1} floating point number apart) which are considered equal in {@link #requireNearlyEqualsTo} */
    private static final int NEARLY_EQ_MAX_ULPS = 1;
    
    /** Private constructor for utility class */
    private Validation() { }
    
    /** Verify that the integer, x, is greater than or equal to the integer, y.
     * 
     * @param x the value to check
     * @param y the value which x must be greater than or equal to
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return the value */
    public static int requireGreaterThanEqualTo(final int x, final int y, final String name) {
        checkName(name);
        if (x < y) {
            final String message = RGE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        return x;
    }
    
    /** Verify that the integer, x, is greater than or equal to the integer, y.
     * 
     * @param x the value to check
     * @param y the value which x must be greater than or equal to
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return the value */
    public static long requireGreaterThanEqualTo(final long x, final long y, final String name) {
        checkName(name);
        if (x < y) {
            final String message = RGE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        return x;
    }
    
    /** verify the integer is greater than the check
     * 
     * @param x the value to check
     * @param y value which x must be greater than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if greater or equal to specified limit */
    public static double requireGreaterThanEqualTo(final double x, final double y, final String name) {
        twoDoublesMetaChecks(x, y, name);
        if (x < y) {
            final String message = RGE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify that x is greater than the check
     * 
     * @param x the value to check
     * @param y value which x must be greater than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if greater than specified limit */
    public static int requireGreaterThan(final int x, final int y, final String name) {
        checkName(name);
        if (x <= y) {
            final String message = RG.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify that x is greater than the check
     * 
     * @param x the value to check
     * @param y value which x must be greater than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if greater than specified limit */
    public static long requireGreaterThan(final long x, final long y, final String name) {
        checkName(name);
        if (x <= y) {
            final String message = RG.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify that x is greater than the check
     * 
     * @param x the value to check.  May be infinite but not {@link Double#NaN}.
     * @param y value which x must be greater than.  May be infinite but not {@link Double#NaN}.
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if greater than specified limit */
    public static double requireGreaterThan(final double x, final double y, final String name) {
        twoDoublesMetaChecks(x, y, name);
        if (x <= y) {
            final String message = RG.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify that x is less or equal to y
     * 
     * @param x value to check
     * @param y value which x must be less than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if less than or equal to specified limit */
    public static int requireLessThanEqualTo(final int x, final int y, final String name) {
        checkName(name);
        if (x > y) {
            final String message = RLE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify that x is less or equal to y
     * 
     * @param x value to check
     * @param y value which x must be less than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if less than or equal to specified limit */
    public static long requireLessThanEqualTo(final long x, final long y, final String name) {
        checkName(name);
        if (x > y) {
            final String message = RLE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify that x is less or equal to y
     * 
     * @param x value to check
     * @param y value which x must be less than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if less than or equal to specified limit */
    public static double requireLessThanEqualTo(final double x, final double y, final String name) {
        twoDoublesMetaChecks(x, y, name);
        if (x > y) {
            final String message = RLE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify the number is less or equal to y
     * 
     * @param x value to check
     * @param y value which x must be less than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if less than specified limit */
    public static int requireLessThan(final int x, final int y, final String name) {
        checkName(name);
        if (x >= y) {
            final String message = RL.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify the number is less or equal to y
     * 
     * @param x value to check
     * @param y value which x must be less than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if less than specified limit */
    public static long requireLessThan(final long x, final long y, final String name) {
        checkName(name);
        if (x >= y) {
            final String message = RL.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** verify the number is less or equal to y
     * 
     * @param x value to check
     * @param y value which x must be less than
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if less than specified limit */
    public static double requireLessThan(final double x, final double y, final String name) {
        twoDoublesMetaChecks(x, y, name);
        if (x >= y) {
            final String message = RL.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** Verify parameter equality
     * 
     * @param x parameter to check
     * @param y basis for comparison
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if equal to comparison */
    public static int requireEqualsTo(final int x, final int y, final String name) {
        checkName(name);
        if (x != y) {
            final String message = RE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** Verify parameter equality
     * 
     * @param x parameter to check
     * @param y basis for comparison
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if equal to comparison */
    public static long requireEqualsTo(final long x, final long y, final String name) {
        checkName(name);
        if (x != y) {
            final String message = RE.formatted(name, x, y);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** Verify parameter equality
     * 
     * @param x parameter to check
     * @param y basis for comparison
     * @param absoluteTolerance the absolute equality tolerance
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return x if equal to comparison */
    public static double requireEqualsTo(final double x,
                                         final double y,
                                         final double absoluteTolerance,
                                         final String name) {
        twoDoublesMetaChecks(x, y, name);
        requireNonNaN(absoluteTolerance, "absoluteTolerance");
        if (!Precision.equals(x, y, absoluteTolerance)) {
            final String message = RE_TOL.formatted(name, x, y, absoluteTolerance);
            throw new ValidationException(message);
        }
        
        return x;
    }
    
    /** Verify that a parameter is true
     * 
     * @param b the boolean parameter being checked
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return true */
    public static boolean requireTrue(final boolean b, final String name) {
        checkName(name);
        if (!b) {
            final String message = RT.formatted(name, b);
            throw new ValidationException(message);
        }
        return b;
    }
    
    /** Verify that a parameter is true
     * 
     * @param b the boolean parameter being checked
     * @param messageSupplier a message supplier.  May not be {@code null}.
     * @return true */
    public static boolean requireTrue(final boolean b, final Supplier<String> messageSupplier) {
        requireNonNull(messageSupplier, "Message supplier may not be null");
        if (!b) {
            throw new ValidationException(messageSupplier.get());
        }
        return b;
    }
    
    /** Verify that a parameter is false
     * 
     * @param b the boolean parameter being checked
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return false */
    public static boolean requireFalse(final boolean b, final String name) {
        checkName(name);
        if (b) {
            final String message = RF.formatted(name, b);
            throw new ValidationException(message);
        }
        return b;
    }
    
    /** Verify that a parameter is false
     * 
     * @param b the boolean parameter being checked
     * @param messageSupplier a message supplier.  May not be {@code null}.
     * @return false */
    public static boolean requireFalse(final boolean b, final Supplier<String> messageSupplier) {
        requireNonNull(messageSupplier, "Message supplier may not be null");
        if (b) {
            throw new ValidationException(messageSupplier.get());
        }
        return b;
    }
    
    /** Check the provided parameter for {@link Double#NaN} and {@link Double#isInfinite() infinity}. Throw a
     * {@link ValidationException} is either is encountered.
     * 
     * @param d double to check for NaN, and infinity
     * @param name the name of the parameter being checked. May not be {@code null}.
     * @return d if finite */
    public static double requireFinite(final double d, final String name) {
        checkName(name);
        if (Double.isInfinite(d)) { // check infinity and NaN separately for a better error message
            final String message = RFINITE.formatted(name, d);
            throw new ValidationException(message);
        }
        requireNonNaN(d, name);
        
        return d;
    }
    
    /** Check the provided parameter for {@link Double#NaN} and {@link Double#isInfinite() infinity}. Throw a
     * {@link ValidationException} is either is encountered.
     * 
     * @param d double to check for NaN, and infinity
     * @param messageSupplier if the given value is not {@link Double#isFinite(double) finite}, we use this
     *        {@link Supplier} to get the error message. May not be {@code null}.
     * @return d if finite */
    public static double requireFinite(final double d, final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (!Double.isFinite(d)) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return d;
    }
    
    /** Check the provided parameter for {@link Double#NaN}
     * 
     * @param d double to check for NaN
     * @param name the name of the parameter being checked.  May not be {@code null}.
     * @return d if non-{@link Double#NaN} */
    public static double requireNonNaN(final double d, final String name) {
        checkName(name);
        if (Double.isNaN(d)) {
            final String message = RNN.formatted(name);
            throw new ValidationException(message);
        }
        
        return d;
    }
    
    /** Check the provided parameter for {@link Double#NaN}
     * 
     * @param d double to check for NaN
     * @param messageSupplier if the given value is {@link Double#NaN}, we use this {@link Supplier} to get the error
     *        message. May not be {@code null}.
     * @return d if non-{@link Double#NaN} */
    public static double requireNonNaN(final double d, final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (Double.isNaN(d)) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return d;
    }
    
    /** Require that {@code p} evaluated to {@code true} for {@code i}
     * 
     * @param i the {@code int} to check
     * @param p the {@link IntPredicate}
     * @param messageSupplier a message supplier.  May not be {@code null}. 
     * @return {@code i} if it passed the check */
    public static int requireInt(final int i, final IntPredicate p, final Supplier<String> messageSupplier) {
        requireNonNull(p, "must specify an IntPredicate");
        requireNonNull(messageSupplier, "must specify a message Supplier");
        if (!p.test(i)) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return i;
    }
    
    /** Require that {@code p} evaluated to {@code true} for {@code l}
     * 
     * @param l the {@code long} to check
     * @param p the {@link LongPredicate}
     * @param messageSupplier a message supplier.  May not be {@code null}.
     * @return {@code l} if it passed the check */
    public static long requireLong(final long l, final LongPredicate p, final Supplier<String> messageSupplier) {
        requireNonNull(p, "must specify an LongPredicate");
        requireNonNull(messageSupplier, "must specify a message Supplier");
        if (!p.test(l)) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return l;
    }
    
    /** Require that {@code p} evaluated to {@code true} for {@code d}
     * 
     * @param d the {@code double} to check
     * @param p the {@link DoublePredicate}
     * @param messageSupplier a message supplier.  May not be {@code null}. 
     * @return {@code d} if it passed the check */
    public static double requireDouble(final double d,
                                       final DoublePredicate p,
                                       final Supplier<String> messageSupplier) {
        requireNonNull(p, "must specify an DoublePredicate");
        requireNonNull(messageSupplier, "must specify a message Supplier");
        if (!p.test(d)) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return d;
    }
    
    /** 
     * Requires that a double is less than or equal to a certain limit.
     * @param x the parameter
     * @param y the inclusive maximum
     * @param messageSupplier a message supplier
     * @return {@code x} if \( x \leq y\)
     * @throws ValidationException if \(x &gt; y\).
     */
    public static double requireLessThanEqualTo(final double x, final double y, 
                                                final Supplier<String> messageSupplier) {
        twoDoublesMetaChecks(x, y, messageSupplier);
        if (x > y) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return x;
    }
    
    /** 
     * Requires that a {@link Comparable} is less than or equal to another {@link Comparable} of the same type.
     * @param x the parameter
     * @param y the inclusive maximum
     * @param messageSupplier a message supplier
     * @param <T> the {@link Comparable} to compare
     * @return {@code x} if \( x \leq y\)
     * @throws ValidationException if \(x &gt; y\).
     */
    public static <T extends Comparable<T>> T requireLessThanEqualTo(final T x, final T y, 
                                                                     final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (x.compareTo(y) > 0) {
            throw new ValidationException(messageSupplier.get());
        }
        return x;
    }

    
    /** 
     * Requires that a double is greater than or equal to a certain limit
     * @param x the parameter
     * @param y the inclusive minimum
     * @param messageSupplier a message supplier
     * @return {@code x} if \(x \geq y\)
     * @throws ValidationException if \(x &lt; y\).
     */
    public static double requireGreaterThanEqualTo(final double x, final double y, 
                                                   final Supplier<String> messageSupplier) {
        twoDoublesMetaChecks(x, y, messageSupplier);
        if (x < y) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return x;
    }

    
    /** 
     * Requires that a double is greater than a certain limit
     * @param x the parameter
     * @param y the exclusive minimum
     * @param messageSupplier a message supplier
     * @return {@code x} if \(x &gt; y\)
     * @throws ValidationException if \(x \leq y\)
     */
    public static double requireGreaterThan(final double x, final double y, final Supplier<String> messageSupplier) {
        twoDoublesMetaChecks(x, y, messageSupplier);
        if (x <= y) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return x;
    }
    
    /**
     * Requires that two objects are equal.
     * @param a the first object
     * @param b the second object
     * @param message a message if the objects are not equal
     * @throws ValidationException if the objects are not equal by {@link Objects#equals}
     */
    public static void requireEqualsTo(final Object a, final Object b, final String message) {
        if (!Objects.equals(a, b)) {
            throw new ValidationException(message);
        }
    }

    /**
     * Requires that two objects are equal.
     * @param a the first object
     * @param b the second object
     * @param messageSupplier a message supplier if the objects are not equal
     * @throws ValidationException if the objects are not equal by {@link Objects#equals}
     */
    public static void requireEqualsTo(final Object a, final Object b, final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (!Objects.equals(a, b)) {
            throw new ValidationException(messageSupplier.get());
        }
    }

    /**
     * Requires that two {@code int}s are equal.
     * @param x the first int
     * @param y the second int
     * @param messageSupplier a message supplier if the ints are not equal
     * @return {@code x}
     * @throws ValidationException if the ints are not equal
     */
    public static int requireEqualsTo(final int x, final int y, final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (x != y) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return x;
    }

    /**
     * Requires that a parameter is within a specified inclusive range
     * @param x the parameter
     * @param a the lower bound
     * @param b the upper bound
     * @param name the name of the parameter
     * @return {@code x}
     * @throws ValidationException if \(x &gt; b\) or \(x &lt; b\).
     */
    public static double requireWithinRange(final double x, final double a, final double b, final String name) {
        Validation.requireGreaterThanEqualTo(x, a, name);
        return Validation.requireLessThanEqualTo(x, b, name);
    }
    
    /** Requires that two {@code double}s are identically equal, or are adjacent {@code double}s
     * 
     * @param x the first double
     * @param y the second double
     * @param name the name of the parameter being checked
     * @return {@code x} */
    public static double requireNearlyEqualsTo(final double x, final double y, final String name) {
        twoDoublesMetaChecks(x, y, name);
        if (!Precision.equals(x, y, NEARLY_EQ_MAX_ULPS)) {
            throw new ValidationException(RE.formatted(name, x, y));
        }
        
        return x;
    }
    
    /** Requires that two {@code double}s are identically equal, or are adjacent {@code double}s
     * 
     * @param x the first double
     * @param y the second double
     * @param messageSupplier a message supplier if the doubles are not nearly equal
     * @return {@code x} */
    public static double requireNearlyEqualsTo(final double x, final double y, final Supplier<String> messageSupplier) {
        twoDoublesMetaChecks(x, y, messageSupplier);
        if (!Precision.equals(x, y, NEARLY_EQ_MAX_ULPS)) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return x;
    }
    
    /** Requires that two {@code double}s are identically equal
     * 
     * @param x the first double
     * @param y the second double
     * @param name the name of the parameter being checked
     * @return {@code x}
     * @throws ValidationException if the doubles are not equal */
    public static double requireEqualsTo(final double x, final double y, final String name) {
        twoDoublesMetaChecks(x, y, name);
        if (x != y) {
            throw new ValidationException(RE.formatted(name, x, y));
        }
        
        return x;
    }
    
    /** Requires that two {@code double}s are identically equal
     * 
     * @param x the first double
     * @param y the second double
     * @param messageSupplier a message supplier if the doubles are not equal
     * @return {@code x}
     * @throws ValidationException if the doubles are not equal */
    public static double requireEqualsTo(final double x, final double y, final Supplier<String> messageSupplier) {
        twoDoublesMetaChecks(x, y, messageSupplier);
        if (x != y) {
            throw new ValidationException(messageSupplier.get());
        }
        
        return x;
    }
    
    /**
     * Requires that a {@code T[]} is non-null and non-empty
     * 
     * @param <T> the type parameter of the array
     * @param array the array to validate
     * @param message a message in case the array is null or empty
     * @return the array
     * @throws ValidationException if the array is null or empty.
     */
    public static <T> T[] requireNotNullNotEmpty(final T[] array, final String message) {
        Objects.requireNonNull(array, "array cannot be null");
        if (array.length == 0) {
            throw new ValidationException(message);
        }
        return array;
    }
    
    /**
     * Requires that a {@link Collection} is non-null and non-empty
     * @param <T> the type parameter of the collection
     * @param collection the collection to validate
     * @param message a message in case the collection is null or empty
     * @return the collection
     * @throws ValidationException if the collection is null or empty.
     */
    public static <T> Collection<T> requireNotNullNotEmpty(final Collection<T> collection, final String message) {
        Objects.requireNonNull(collection, "collection cannot be null");
        if (collection.isEmpty()) {
            throw new ValidationException(message);
        }
        return collection;
    }

    /**
     * Requires that a {@link Map} is non-null and non-empty
     * @param <K> the key type of the map
     * @param <V> the value type of the map
     * @param map the map to validate
     * @param message a message in case the map is null or empty
     * @return the map.
     * @throws ValidationException if the map is null or empty.
     */
    public static <K, V> Map<K, V> requireNotNullNotEmpty(final Map<K, V> map, final String message) {
        Objects.requireNonNull(map, "map cannot be null");
        if (map.isEmpty()) {
            throw new ValidationException(message);
        }
        return map;
    }
    
    /** Require that the given {@link Object} is {@code null}
     * 
     * @param obj the object that must be {@code null}
     * @param name the name of - or identifying information regarding - the parameter being checked.  Must not be null.
     * @throws ValidationException if the {@code obj != null} */
    public static void requireNull(final Object obj, final String name) {
        if (obj != null) {
            throw new ValidationException(name + " must be null");
        }
    }

    /** Require that the given {@link Object} is {@code null}
     * 
     * @param obj the object that must be {@code null}
     * @param messageSupplier a message supplier used to generate an error message when the object is non-{@code null}
     * @throws ValidationException if the {@code obj != null} */
    public static void requireNull(final Object obj, final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (obj != null) {
            throw new ValidationException(messageSupplier.get());
        }
    }
    
    /** Ensure that a name has been specified
     * 
     * @param name the parameter name */
    private static void checkName(final String name) {
        requireNonNull(name, "must specify a name parameter");
    }
    
    /**
     * Require that the message supplier is non-null
     * @param messageSupplier the message supplier
     */
    private static void checkMessageSupplier(final Supplier<String> messageSupplier) {
        requireNonNull(messageSupplier, "messageSupplier cannot be null");
    }
    
    /** For methods comparing {@code double}s, check to ensure that neither {@link Double#isNaN() is NaN} and that the
     * {@code name} is non-{@code null}.
     * 
     * @param x the first value
     * @param y the second value
     * @param name the name of the first parameter */
    static void twoDoublesMetaChecks(final double x, final double y, final String name) {
        checkName(name);
        if (Double.isNaN(x) || Double.isNaN(y)) {
            throw new ValidationException("Cannot compare %s with value %f to %f".formatted(name, x, y));
        }
    }
    
    /** For methods comparing {@code double}s, check to ensure that neither {@link Double#isNaN() is NaN} and that the
     * message {@link Supplier} is non-{@code null}.
     * 
     * @param x the first value
     * @param y the second value
     * @param messageSupplier the message {@link Supplier} */
    static void twoDoublesMetaChecks(final double x, final double y, final Supplier<String> messageSupplier) {
        checkMessageSupplier(messageSupplier);
        if (Double.isNaN(x) || Double.isNaN(y)) {
            throw new ValidationException("Cannot compare %f to %f.  %s".formatted(x, y, messageSupplier.get()));
        }
    }
}
