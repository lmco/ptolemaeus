/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons.collections;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import ptolemaeus.commons.Parallelism;

/**
 * Utility methods for {@link Stream}s
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public final class StreamUtils {
    
    /** do nothing */
    private StreamUtils() { }
    
    /** Return a {@link Collector} that collects to a mutable {@link ArrayDeque}
     * 
     * @param <T> the element type of the resulting {@link ArrayDeque}
     * @return a {@link Collector} that collects to a mutable {@link ArrayDeque} */
    public static <T> Collector<T, ?, ArrayDeque<T>> toArrayDeque() {
        return Collectors.toCollection(ArrayDeque::new);
    }
    
    /** Return a {@link Collector} that collects to a mutable {@link LinkedList}
     * 
     * @param <T> the element type of the resulting {@link LinkedList}
     * @return a {@link Collector} that collects to a mutable {@link LinkedList} */
    public static <T> Collector<T, ?, LinkedList<T>> toLinkedList() {
        return Collectors.toCollection(LinkedList::new);
    }
    
    /** Note: documentation copied and adapted from relevant {@link Collectors} methods. <br><br>
     * 
     * Returns a {@code Collector} that accumulates the input elements into an
     * <a href="../Map.html#unmodifiable">unmodifiable Map</a>, whose keys and values are the result of applying the
     * provided mapping functions to the input elements. <br><br>
     * 
     * The resulting {@link Map} is ordered. <br><br>
     * 
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), an
     * {@code IllegalStateException} is thrown when the collection operation is performed. If the mapped keys might have
     * duplicates, use {@link Collectors#toUnmodifiableMap(Function, Function, BinaryOperator)} to handle merging of the
     * values. <br><br>
     * 
     * The returned Collector disallows null keys and values. If either mapping function returns null,
     * {@code NullPointerException} will be thrown. <br><br>
     * 
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param keyMapper a mapping function to produce keys, must be non-null
     * @param valueMapper a mapping function to produce values, must be non-null
     * @return a {@code Collector} that accumulates the input elements into an
     *         <a href="../Map.html#unmodifiable">unmodifiable Map</a>, whose keys and values are the result of applying
     *         the provided mapping functions to the input elements
     * @throws NullPointerException if either keyMapper or valueMapper is null
     * 
     * @see #toUnmodifiableMap(Function, Function, BinaryOperator) */
    public static <T, K, U> Collector<T, ?, Map<K, U>>
                                    toUnmodifiableMap(final Function<? super T, ? extends K> keyMapper,
                                                      final Function<? super T, ? extends U> valueMapper) {
        Objects.requireNonNull(keyMapper, "keyMapper");
        Objects.requireNonNull(valueMapper, "valueMapper");
        return Collectors.collectingAndThen(toMap(keyMapper, valueMapper), Collections::unmodifiableMap);
    }
    
    /** Note: documentation copied and adapted from relevant {@link Collectors} methods. <br>
     * <br>
     * 
     * Returns a {@code Collector} that accumulates the input elements into an
     * <a href="../Map.html#unmodifiable">unmodifiable Map</a>, whose keys and values are the result of applying the
     * provided mapping functions to the input elements. <br>
     * <br>
     * 
     * The resulting {@link Map} is ordered. <br><br>
     * 
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), the value mapping function is
     * applied to each equal element, and the results are merged using the provided merging function. <br>
     * <br>
     * 
     * The returned Collector disallows null keys and values. If either mapping function returns null,
     * {@code NullPointerException} will be thrown. <br>
     * <br>
     * 
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param keyMapper a mapping function to produce keys, must be non-null
     * @param valueMapper a mapping function to produce values, must be non-null
     * @param mergeFunction a merge function, used to resolve collisions between values associated with the same key, as
     *        supplied to {@link Map#merge(Object, Object, BiFunction)}, must be non-null
     * @return a {@code Collector} that accumulates the input elements into an
     *         <a href="../Map.html#unmodifiable">unmodifiable Map</a>, whose keys and values are the result of applying
     *         the provided mapping functions to the input elements
     * @throws NullPointerException if the keyMapper, valueMapper, or mergeFunction is null
     * 
     * @see #toUnmodifiableMap(Function, Function)
     * @since 10 */
    public static <T, K, U> Collector<T, ?, Map<K, U>>
                                    toUnmodifiableMap(final Function<? super T, ? extends K> keyMapper,
                                                      final Function<? super T, ? extends U> valueMapper,
                                                      final BinaryOperator<U> mergeFunction) {
        Objects.requireNonNull(keyMapper, "keyMapper");
        Objects.requireNonNull(valueMapper, "valueMapper");
        Objects.requireNonNull(mergeFunction, "mergeFunction");
        return Collectors.collectingAndThen(toMap(keyMapper, valueMapper, mergeFunction, LinkedHashMap::new),
                                            Collections::unmodifiableMap);
    }
    
    /**
     * Note: documentation copied and adapted from relevant {@link Collectors} methods.
     * <p>
     * Returns a {@link Collector} that accumulates elements into a {@link Map} whose keys and values are the result of
     * applying the provided mapping functions to the input elements and which maintains insertion order.
     * </p>
     * <p>
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), an
     * {@link IllegalStateException} is thrown when the collection operation is performed. If the mapped keys might have
     * duplicates, use {@link #toMapOverwrite(Function, Function)} instead.
     * </p>
     * 
     * @implNote The returned {@link Collector} is not concurrent. For parallel stream pipelines, the {@code combiner}
     *           function operates by merging the keys from one map into another, which can be an expensive operation.
     *           If it is not required that results are merged into the {@link Map} in encounter order, using
     *           {@link Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)} may offer better
     *           parallel performance.
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param keyMapper a mapping function to produce keys
     * @param valueMapper a mapping function to produce values
     * @return a {@link Collector} which collects elements into a {@link Map} whose keys are the result of applying a
     *         key mapping function to the input elements, and whose values are the result of applying a value mapping
     *         function to all input elements equal to the key
     * @see StreamUtils#toMapOverwrite(Function, Function)
     * @see StreamUtils#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toConcurrentMap(Function, Function)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toUnmodifiableMap(Function, Function)
     * @see Collectors#toUnmodifiableMap(Function, Function, BinaryOperator)
     */
    public static <T, K, U> Collector<T, ?, Map<K, U>> toMap(final Function<? super T, ? extends K> keyMapper,
                                                             final Function<? super T, ? extends U> valueMapper) {
        return Collectors.toMap(keyMapper, valueMapper, StreamUtils::throwDuplicateKeyException, LinkedHashMap::new);
    }
    
    /** A pass-through to {@link #toMapOverwrite(Function, Function, Supplier)} with a {@link LinkedHashMap}
     * {@link Supplier}.<br>
     * <br>
     * See {@link #toMapOverwrite(Function, Function, Supplier)} for details and warnings.
     * 
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param keyMapper a mapping function to produce keys
     * @param valueMapper a mapping function to produce values
     * @return a {@link Collector} which collects elements into a {@link Map} whose keys are the result of applying a
     *         key mapping function to the input elements, and whose values are the result of applying a value mapping
     *         function to all input elements equal to the key and combining them using the merge function
     * 
     * @see #toMapOverwrite(Function, Function, Supplier) */
    public static <T, K, U> Collector<T, ?, Map<K, U>> toMapOverwrite(
            final Function<? super T, ? extends K> keyMapper,
            final Function<? super T, ? extends U> valueMapper) {
        return toMapOverwrite(keyMapper, valueMapper, LinkedHashMap::new);
    }
    
    /** Note: documentation copied and adapted from relevant {@link Collectors} methods. <br>
     * <br>
     * Returns a {@link Collector} that accumulates elements into a {@link Map} whose keys and values are the result of
     * applying the provided mapping functions to the input elements and which maintains insertion order. <br>
     * <br>
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), we accept the second of the
     * two values. This can be thought of as an overwrite.
     * 
     * @implNote The returned {@link Collector} is not concurrent. For parallel stream pipelines, the {@code combiner}
     *           function operates by merging the keys from one map into another, which can be an expensive operation.
     *           If it is not required that results are merged into the {@link Map} in encounter order, using
     *           {@link Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)} may offer better
     *           parallel performance.
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param <M> the {@link Map} implementation supplied by the {@code mapFactory} {@link Supplier}
     * @param keyMapper a mapping function to produce keys
     * @param valueMapper a mapping function to produce values
     * @param mapFactory mapFactory a supplier providing a new empty {@code Map} into which the results will be inserted
     * @return a {@link Collector} which collects elements into a {@link Map} whose keys are the result of applying a
     *         key mapping function to the input elements, and whose values are the result of applying a value mapping
     *         function to all input elements equal to the key and combining them using the merge function
     *         
     * @see #toMapOverwrite(Function, Function)
     * @see StreamUtils#toMap(Function, Function)
     * @see StreamUtils#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toConcurrentMap(Function, Function)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toUnmodifiableMap(Function, Function)
     * @see Collectors#toUnmodifiableMap(Function, Function, BinaryOperator) */
    public static  <T, K, U, M extends Map<K, U>> Collector<T, ?, M> toMapOverwrite(
            final Function<? super T, ? extends K> keyMapper,
            final Function<? super T, ? extends U> valueMapper,
            final Supplier<M> mapFactory) {
        return Collectors.toMap(keyMapper, valueMapper, (v1, v2) -> v2, mapFactory);
    }
    
    /**
     * Note: documentation copied and adapted from relevant {@link Collectors} methods.
     * <p>
     * Returns a {@link Collector} that accumulates elements into a {@link Map} whose keys and values are the result of
     * applying the provided mapping functions to the input elements and which maintains insertion order.
     * </p>
     * <p>
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), an
     * {@link IllegalStateException} is thrown when the collection operation is performed. If the mapped keys might have
     * duplicates, use {@link #toMapOverwrite(Function, Function)} instead.
     * </p>
     * 
     * @apiNote There are multiple ways to deal with collisions between multiple elements mapping to the same key. The
     *          other forms of {@code toMap} simply use a merge function that throws unconditionally, but you can easily
     *          write more flexible merge policies. For example, if you have a stream of {@code Person}, and you want to
     *          produce a "phone book" mapping name to address, but it is possible that two persons have the same name,
     *          you can do as follows to gracefully deal with these collisions, and produce a {@link Map} mapping names
     *          to a concatenated list of addresses:
     *          
     *          <pre>
     *          {@code
     * Map<String, String> phoneBook
     *   = people.stream().collect(
     *     toMap(Person::getName,
     *           Person::getAddress,
     *           (s, a) -> s + ", " + a));
     * }
     *          </pre>
     * 
     * @implNote The returned {@link Collector} is not concurrent. For parallel stream pipelines, the {@code combiner}
     *           function operates by merging the keys from one map into another, which can be an expensive operation.
     *           If it is not required that results are merged into the {@link Map} in encounter order, using
     *           {@link Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)} may offer better
     *           parallel performance.
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param keyMapper a mapping function to produce keys
     * @param valueMapper a mapping function to produce values
     * @param mergeFunction a merge function, used to resolve collisions between values associated with the same key
     * @return a {@link Collector} which collects elements into a {@link Map} whose keys are the result of applying a
     *         key mapping function to the input elements, and whose values are the result of applying a value mapping
     *         function to all input elements equal to the key and combining them using the merge function
     * @see StreamUtils#toMapOverwrite(Function, Function)
     * @see StreamUtils#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toConcurrentMap(Function, Function)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toUnmodifiableMap(Function, Function)
     * @see Collectors#toUnmodifiableMap(Function, Function, BinaryOperator)
     */
    public static <T, K, U> Collector<T, ?, Map<K, U>> toMap(
            final Function<? super T, ? extends K> keyMapper,
            final Function<? super T, ? extends U> valueMapper,
            final BinaryOperator<U> mergeFunction) {
        return Collectors.toMap(keyMapper, valueMapper, mergeFunction, LinkedHashMap::new);
    }
    
    /**
     * Note: documentation copied and adapted from relevant {@link Collectors} methods.
     * <p>
     * Returns a {@link Collector} that accumulates elements into a {@code Map} whose keys and values are the result of
     * applying the provided mapping functions to the input elements.
     * </p>
     * 
     * @implNote The returned {@link Collector} is not concurrent. For parallel stream pipelines, the {@code combiner}
     *           function operates by merging the keys from one map into another, which can be an expensive operation.
     *           If it is not required that results are merged into the {@link Map} in encounter order, using
     *           {@link Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)} may offer better
     *           parallel performance.
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param <M> the type of the resulting {@code Map}
     * @param keyMapper a mapping function to produce keys
     * @param valueMapper a mapping function to produce values
     * @param mapFactory a supplier providing a new empty {@code Map} into which the results will be inserted
     * @return a {@code Collector} which collects elements into a {@code Map} whose keys are the result of applying a
     *         key mapping function to the input elements, and whose values are the result of applying a value mapping
     *         function to all input elements.
     * @see StreamUtils#toMapOverwrite(Function, Function)
     * @see StreamUtils#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toConcurrentMap(Function, Function)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toUnmodifiableMap(Function, Function)
     * @see Collectors#toUnmodifiableMap(Function, Function, BinaryOperator)
     */
    public static <T, K, U, M extends Map<K, U>> Collector<T, ?, M> toMap(
            final Function<? super T, ? extends K> keyMapper,
            final Function<? super T, ? extends U> valueMapper,
            final Supplier<M> mapFactory) {
        return Collectors.toMap(keyMapper, valueMapper, StreamUtils::throwDuplicateKeyException, mapFactory);
    }
    
    /**
     * Note: documentation copied and adapted from relevant {@link Collectors} methods.
     * <p>
     * Returns a {@link Collector} that accumulates elements into a {@code Map} whose keys and values are the result of
     * applying the provided mapping functions to the input elements.
     * </p>
     * <p>
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), the value mapping function is
     * applied to each equal element, and the results are merged using the provided merging function. The {@link Map} is
     * created by a provided supplier function.
     * </p>
     * 
     * @implNote The returned {@link Collector} is not concurrent. For parallel stream pipelines, the {@code combiner}
     *           function operates by merging the keys from one map into another, which can be an expensive operation.
     *           If it is not required that results are merged into the {@link Map} in encounter order, using
     *           {@link Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)} may offer better
     *           parallel performance.
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @param <U> the output type of the value mapping function
     * @param <M> the type of the resulting {@code Map}
     * @param keyMapper a mapping function to produce keys
     * @param valueMapper a mapping function to produce values
     * @param mergeFunction a merge function, used to resolve collisions between values associated with the same key, as
     *        supplied to {@link Map#merge(Object, Object, BiFunction)}
     * @param mapFactory a supplier providing a new empty {@code Map} into which the results will be inserted
     * @return a {@code Collector} which collects elements into a {@code Map} whose keys are the result of applying a
     *         key mapping function to the input elements, and whose values are the result of applying a value mapping
     *         function to all input elements equal to the key and combining them using the merge function
     * @see StreamUtils#toMapOverwrite(Function, Function)
     * @see StreamUtils#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function)
     * @see Collectors#toMap(Function, Function, BinaryOperator)
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toConcurrentMap(Function, Function)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator)
     * @see Collectors#toConcurrentMap(Function, Function, BinaryOperator, Supplier)
     * @see Collectors#toUnmodifiableMap(Function, Function)
     * @see Collectors#toUnmodifiableMap(Function, Function, BinaryOperator)
     */
    public static <T, K, U, M extends Map<K, U>> Collector<T, ?, M> toMap(
            final Function<? super T, ? extends K> keyMapper,
            final Function<? super T, ? extends U> valueMapper,
            final BinaryOperator<U> mergeFunction,
            final Supplier<M> mapFactory) {
        return Collectors.toMap(keyMapper, valueMapper, mergeFunction, mapFactory);
    }
    
    /**
     * Construct an {@link IllegalStateException} with appropriate message for when we need to throw an exception due to
     * an unsupported Map value merging attempt.
     * 
     * @param <V> the value type
     * @param v1 the first value
     * @param v2 the second value
     * @return nothing.  This method throws an {@link IllegalStateException}
     */
    private static <V> V throwDuplicateKeyException(final V v1, final V v2) {
        throw new IllegalStateException(String.format("Duplicate key: attempted merging values %s and %s", v1, v2));
    }
    
    /**
     * Note: documentation copied and adapted from relevant {@link Collectors} methods.
     * <p>
     * Returns a {@link Collector} that accumulates the input elements into a new {@link LinkedHashSet}.
     * </p>
     * 
     * @param <T> the type of the input elements
     * @return a {@code Collector} which collects all the input elements into a {@code Set}
     * @see Collectors#toSet()
     */
    public static <T> Collector<T, ?, Set<T>> toSet() {
        return Collectors.toCollection(LinkedHashSet::new);
    }
    
    /** Note: documentation copied and adapted from relevant {@link Collectors} methods. <br><br>
     * 
     * Returns a {@code Collector} that accumulates the input elements into an
     * <a href="../Set.html#unmodifiable">unmodifiable Set</a>. The returned Collector disallows null values and will
     * throw {@code NullPointerException} if it is presented with a null value. If the input contains duplicate
     * elements, an arbitrary element of the duplicates is preserved. <br><br>
     * 
     * The resulting {@link Set} is ordered <br><br>
     * 
     * @param <T> the type of the input elements
     * @return a {@code Collector} that accumulates the input elements into an
     *         <a href="../Set.html#unmodifiable">unmodifiable Set</a> */
    public static <T> Collector<T, ?, Set<T>> toUnmodifiableSet() {
        return Collectors.collectingAndThen(toSet(), Collections::unmodifiableSet);
    }
    
    /**
     * Create a sequential {@link Stream} from an {@link Iterable}
     * 
     * @param <T> the type of the elements in the returned {@link Stream}
     * @param iterable the {@link Iterable} we want to convert to a {@link Stream}
     * @return the new sequential {@link Stream} derived from the {@link Iterable}
     */
    public static <T> Stream<T> sequential(final Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
    
    /**
     * Create a parallel {@link Stream} from an {@link Iterable}.  
     * 
     * @param <T> the type of the elements in the returned {@link Stream}
     * @param iterable the {@link Iterable} we want to convert to a {@link Stream}
     * @return the new parallel {@link Stream} derived from the {@link Iterable}
     */
    public static <T> Stream<T> parallel(final Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), true);
    }
    
    /** Create and return a {@link Stream} of the elements of the given {@link Iterable}.
     * 
     * @param <T> the type of the elements in the returned {@link Stream}
     * @param iterable the {@link Iterable} we want to convert to a {@link Stream}
     * @param parallelism the {@link Parallelism} of the {@link Stream} that we want to create
     * @return the new {@link Stream} derived from the {@link Iterable} */
    public static <T> Stream<T> stream(final Iterable<T> iterable, final Parallelism parallelism) {
        return StreamSupport.stream(iterable.spliterator(), parallelism.shouldParallelStream());
    }
    
    /**
     * Concatenate arbitrarily many {@link Stream}s into a sequential {@link Stream}
     * 
     * @param <T> the type of the elements of the {@link Stream}s
     * @param streams the {@link Stream}s to concatenate
     * @return the concatenated {@link Stream}
     */
    @SafeVarargs
    public static <T> Stream<T> concat(final Stream<? extends T>... streams) {
        return Stream.of(streams).flatMap(Function.identity());
    }
}
