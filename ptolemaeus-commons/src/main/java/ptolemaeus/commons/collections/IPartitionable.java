/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections;

import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

import ptolemaeus.commons.StringBuilderAppender;
import ptolemaeus.commons.Validation;

/**
 * <p>
 * Implementations of {@link IPartitionable} can be partitioned/broken-up into sub-elements called {@code parts}, over
 * which we can iterate.
 * </p>
 * <p>
 * A properly implemented {@link IPartitionable} will not contain nulls among its partitions.
 * </p>
 * 
 * @param <T> the element type of this {@link IPartitionable}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public interface IPartitionable<T> extends IStreamable<T>, Iterable<T>, StringBuilderAppender {
    
    /** @return the {@code parts} of this {@link IPartitionable} */
    Iterable<T> parts();
    
    @Override
    default Stream<T> stream() {
        return StreamUtils.sequential(this.parts());
    }
    
    @Override
    default Iterator<T> iterator() {
        return this.parts().iterator();
    }
    
    @Override
    default void appendToStringBuilder(final StringBuilder builder, final int indentCount) {
        Objects.requireNonNull(builder, "a builder must be specified");
        Validation.requireGreaterThanEqualTo(indentCount, 0, "indentCount");
        final String indent = StringBuilderAppender.getIndent(indentCount, StringBuilderAppender.DEFAULT_INDENT);
        this.parts().forEach(element -> builder.append(indent)
                                               .append(element.toString())
                                               .append(System.lineSeparator()));
    }
}
