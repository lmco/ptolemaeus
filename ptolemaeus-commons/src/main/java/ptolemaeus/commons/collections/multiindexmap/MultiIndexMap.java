/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections.multiindexmap;

import java.util.AbstractCollection;
import java.util.AbstractMap.SimpleEntry;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.hipparchus.util.Pair;

/**
 * <p>
 * A {@link MultiIndexMap} caches data of type {@code V} generated for {@link Iterable}s with elements of type
 * {@code K}. This allows values to be indexed by arbitrarily many keys.
 * </p>
 * 
 * @param <K> the key type
 * @param <V> the value type
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class MultiIndexMap<K, V> implements Map<Iterable<K>, V> {
    
    /**
     * A {@link Map} {@link Supplier} for this cache's nested caches and this cache itself
     */
    private final Supplier<Map<K, MultiIndexMapValue<K, V>>> mapSupplier;
    
    /**
     * The top-level cache of this {@link MultiIndexMap}
     */
    private final Map<K, MultiIndexMapValue<K, V>> cache;
    
    /**
     * Default constructor. This uses a {@link ConcurrentHashMap} as the backing map.
     */
    public MultiIndexMap() {
        this(() -> new ConcurrentHashMap<>());
    }
    
    /**
     * @param mapSupplier a {@link Supplier} of {@link Map}s. If used in a multithreaded context, it is important that
     *        all {@link Map}s generated using this {@link Supplier} are either synchronized, allow concurrent
     *        modification, or, if those things are impossible, that multithreading is disabled.
     */
    public MultiIndexMap(final Supplier<Map<K, MultiIndexMapValue<K, V>>> mapSupplier) {
        this(mapSupplier, mapSupplier.get());
    }
    
    /**
     * A private constructor to explicitly specify a {@link Map} and the {@link Supplier}
     * 
     * @param mapSupplier a {@link Supplier} of {@link Map}s. If used in a multithreaded context, it is important that
     *        all {@link Map}s generated using this {@link Supplier} are either synchronized, allow concurrent
     *        modification, or, if those things are impossible, that multithreading is disabled.
     * @param cache the {@link Map} cache for this instance
     */
    private MultiIndexMap(final Supplier<Map<K, MultiIndexMapValue<K, V>>> mapSupplier,
                          final Map<K, MultiIndexMapValue<K, V>> cache) {
        this.mapSupplier = mapSupplier;
        this.cache = cache;
    }
    
    /**
     * Retrieve the {@code V} associated with the provided {@code keys} in this {@link MultiIndexMap} or any of the
     * nested {@link MultiIndexMap}s (through {@link MultiIndexMapValue}s).
     * 
     * @param keys the {@link Iterable} {@code keys} for which we want to retrieve a {@code V}. May not be null.
     * @return the {@code V} associated with the provided {@code keys} in this {@link MultiIndexMap} or any of the
     *         nested {@link MultiIndexMap}s (through {@link MultiIndexMapValue}s). {@code null} if the associated
     *         {@code V} is null or if this is provided an "empty" {@link Iterable}
     */
    public V get(final Iterable<K> keys) {
        Objects.requireNonNull(keys, "Keys may not be null");
        final MultiIndexMapValue<K, V> value = this.getMostNestedMIMValue(keys.iterator());
        return value == null ? null : value.getData();
    }
    
    /**
     * Put an entry into this {@link MultiIndexMap} or one of the appropriate nested caches (through
     * {@link MultiIndexMapValue}s). <b>This operation will overwrite any existing value.</b>
     * 
     * @param value the {@code V} which we want to cache for the provided {@code K}s
     * @param keys the {@code K} {code varargs} keys for which we want to put a {@code V}
     * @return see {@link Map#put(Object, Object)}
     */
    @SuppressWarnings("unchecked")
    public V put(final V value, final K... keys) {
        Objects.requireNonNull(keys, "Keys may not be null");
        return this.put(List.of(keys), value);
    }
    
    /**
     * Put an entry into this {@link MultiIndexMap} or one of the appropriate nested caches (through
     * {@link MultiIndexMapValue}s). <b>This operation will overwrite any existing value.</b>
     * 
     * @param keys the {@code K} keys for which we want to put a {@code V}
     * @param value the {@code V} which we want to cache for the provided {@code K}s
     * @return see {@link Map#put(Object, Object)}
     */
    @Override
    public V put(final Iterable<K> keys, final V value) {
        Objects.requireNonNull(keys, "Keys may not be null");
        final Pair<V, V> answerPair = this.put(keys.iterator(), value, null);
        return answerPair != null ? answerPair.getFirst() : null;
    }
    
    /**
     * A private method to recursively search for where we should place the {@code V}. If we provide a non-{@code null}
     * {@link Function}, {@code f}, we will use that to compute a value for the {@code keyIterator} using the last
     * {@code K} in that {@code keyIterator}.
     * 
     * @param keyIterator the {@code K} key {@link Iterator} for which we want to put a {@code V}
     * @param value the {@code V} which we want to cache for the provided {@code K}s. May be null.
     * @param f if non-{@code null}, the {@link Function} we will use to compute a {@code V} if {@code keyIterator} is
     *        associated with only {@code null}. If null, we will store {@code null}.
     * @return returns a {@link Pair} of {@code V}s. The first is the {code V} that was replaced, if there was one, and
     *         the second is the {@code V} to which the keyIterator maps at the end of this operation.
     */
    private Pair<V, V> put(final Iterator<K> keyIterator, final V value, final Function<K, V> f) {
        if (keyIterator.hasNext()) {
            final K key = keyIterator.next();
            
            if (!keyIterator.hasNext()) {
                final MultiIndexMapValue<K, V> oldValue = this.cache.get(key);
                final V oldData = oldValue == null ? null : oldValue.getData();
                final V newData = value == null ? f != null ? f.apply(key) : null : value;
                this.cache.merge(key, new MultiIndexMapValue<>(this.mapSupplier, newData), MultiIndexMapValue::merge);
                return new Pair<>(oldData, newData);
            }
            else {
                return this.cache.computeIfAbsent(key, k -> new MultiIndexMapValue<>(this.mapSupplier, null))
                                 .getNestedCache()
                                 .put(keyIterator, value, f);
            }
        }
        
        return null;
    }
    
    /**
     * Given two {@link MultiIndexMap}s, merge them. The first {@link MultiIndexMap} is given "priority" in the sense
     * that, if it is non-{@code null}, we merge the second {@link MultiIndexMap} into it and return. If the first
     * {@link MultiIndexMap} is {@code null}, we simply return the second {@link MultiIndexMap}, which also may be
     * {@code null}. If the second is {@code null}, we return the first unchanged. If both are {@code null}, we return
     * {@code null}.
     * 
     * @param <K> the key type
     * @param <V> the value type
     * @param v1 the {@link MultiIndexMap} into which we wish to merge {@code v2}
     * @param v2 the {@link MultiIndexMap} we wish to merge into {@code v1}
     * @return if both {@link MultiIndexMap}s are non-{@code null}, we return the first {@link MultiIndexMap} with the
     *         second merged into it. If the first is {@code null}, we return the second unchanged. If the second is
     *         {@code null}, we return the first unchanged. If both are {@code null}, we return {@code null}.
     */
    static <K, V> MultiIndexMap<K, V> merge(final MultiIndexMap<K, V> v1, final MultiIndexMap<K, V> v2) {
        if (v1 == null) {
            return v2;
        }
        
        if (v2 == null) {
            return v1;
        }
        
        v2.cache.forEach((k, v) -> v1.cache.merge(k, v, MultiIndexMapValue::merge));
        
        return v1;
    }
    
    /**
     * If there is not an entry for the {@code keys}, compute one by applying {@code f} to the last key element and
     * store it. If there does exist an entry, do not overwrite it. In either case, we return the value associated with
     * {@code keys}.
     * 
     * @param keys the {@code K}s for which we want a {@code V}
     * @param f the {@link Function} we will use to compute a new {@code V} if one does not exist.
     * @return a new or previously calculated {@code V}
     */
    public V computeIfAbsent(final Function<K, V> f, final Iterable<K> keys) {
        Objects.requireNonNull(keys, "Keys may not be null");
        V answer = this.get(keys);
        if (answer == null) {
            final Pair<V, V> answerPair = this.put(keys.iterator(), null, f);
            answer = answerPair != null ? answerPair.getSecond() : null;
        }
        return answer;
    }
    
    /** A {@code varargs} convenience pass-through to {@link #computeIfAbsent(Object, Function)}
     * 
     * @param mappingFunction the mapping function to compute a value
     * @param keys key with which the specified value is to be associated
     * @return the current (existing or computed) value associated with the specified key, or null if the computed value
     *         is null
     * @see #computeIfAbsent(Object, Function) */
    @SuppressWarnings("unchecked")
    public V computeIfAbsent(final Function<? super Iterable<K>, ? extends V> mappingFunction, final K... keys) {
        return Map.super.computeIfAbsent(List.of(keys), mappingFunction);
    }
    
    @Override
    public boolean isEmpty() {
        return this.cache.isEmpty();
    }
    
    @Override
    public boolean containsKey(final Object key) {
        return this.get(key) != null;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public V get(final Object key) {
        if (key instanceof Iterable) {
            return this.get((Iterable<K>) key);
        }
        
        return null;
    }
    
    /**
     * Retrieve the {@code V} associated with the provided {@code keys} in this {@link MultiIndexMap} or any of the
     * nested {@link MultiIndexMap}s (through {@link MultiIndexMapValue}s).
     * 
     * @param keys the {@code varargs} keys of type {@code K}
     * @return the mapped value, or null if none
     * @see #get(Iterable)
     */
    @SuppressWarnings("unchecked")
    public V get(final K... keys) {
        Objects.requireNonNull(keys, "keys must be specified");
        return this.get(List.of(keys));
    }
    
    /**
     * Retrieve the {@code V} associated with the provided {@code keys} in this {@link MultiIndexMap} or any of the
     * nested {@link MultiIndexMap}s (through {@link MultiIndexMapValue}s). If no value is found, return the provided
     * {@code defaultValue}.
     * 
     * @param defaultValue the default value
     * @param keys the {@code varargs} keys of type {@code K}
     * @return the mapped value, or null if none
     * @see #getOrDefault(Object, Object)
     */
    @SuppressWarnings("unchecked")
    public V getOrDefault(final V defaultValue, final K... keys) {
        Objects.requireNonNull(keys, "keys must be specified");
        return this.getOrDefault(List.of(keys), defaultValue);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public V remove(final Object key) {
        if (key instanceof Iterable) {
            return this.put((Iterable<K>) key, null);
        }
        
        return null;
    }
    
    @Override
    public void putAll(final Map<? extends Iterable<K>, ? extends V> m) {
        m.forEach(this::put);
    }
    
    @Override
    public void clear() {
        this.cache.clear();
    }
    
    @Override
    public int size() {
        return (int) this.getValueStream().count();
    }
    
    @Override
    public boolean containsValue(final Object value) {
        return this.getValueStream().anyMatch(v -> Objects.equals(v, value));
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        this.stringBuilderVisit(sb, 0);
        return sb.toString();
    }
    
    /**
     * Visit this {@link MultiIndexMap} and append it's data to the provided {@link StringBuilder}
     * 
     * @param builder the {@link StringBuilder} to which we wish to append data
     * @param indentCount the size of the indent to use for this {@link MultiIndexMap}'s data
     */
    void stringBuilderVisit(final StringBuilder builder, final int indentCount) {
        final String indent = "    ".repeat(indentCount);
        this.cache.forEach((k, v) -> {
            if (k != null) {
                builder.append(System.lineSeparator()).append(indent).append(k);
            }
            v.stringBuilderVisit(builder, indentCount);
        });
    }
    
    @Override
    public Set<Iterable<K>> keySet() {
        return new KeySet();
    }
    
    @Override
    public Collection<V> values() {
        return new ValueCollection();
    }
    
    @Override
    public Set<Entry<Iterable<K>, V>> entrySet() {
        return new EntrySet();
    }
    
    /**
     * @return a {@link Stream} of the {@code V}s associated with keys in this {@link MultiIndexMap}
     */
    private Stream<V> getValueStream() {
        return this.cache.values()
                         .stream()
                         .flatMap(value -> Stream.concat(Stream.of(value.getData()),
                                                         value.getNestedCache().getValueStream()))
                         .filter(Objects::nonNull);
    }
    
    /** Retrieve the {@link MultiIndexMap sub-multi-index-map} associated with the provided {@code keys} in this
     * {@link MultiIndexMap} or any of the nested {@link MultiIndexMap}s (through {@link MultiIndexMapValue}s)
     * 
     * @param keys the {@link Iterable} {@code keys} for which we want to retrieve a {@code sub-map}. May not be null.
     * @return the {@link MultiIndexMap sub-multi-index-map} associated with the provided {@code keys} in this
     *         {@link MultiIndexMap} or any of the nested {@link MultiIndexMap}s (through {@link MultiIndexMapValue}s).
     *         {@code null} if the associated {@code V} is null or if this is provided an "empty" {@link Iterable} */
    public MultiIndexMap<K, V> getSubMap(final Iterable<K> keys) {
        Objects.requireNonNull(keys, "Keys may not be null");
        final MultiIndexMapValue<K, V> value = this.getMostNestedMIMValue(keys.iterator());
        return value == null ? null : value.getNestedCache();
    }
    
    /** A private method to recursively search for a {@link MultiIndexMapValue} associated with the provided
     * {@code keyIterator}
     * 
     * @param keyIterator the {@link Iterator} we will use while searching nested caches
     * @return the {@link MultiIndexMapValue} associated with the {@code K}s of {@code keyIterator} */
    private MultiIndexMapValue<K, V> getMostNestedMIMValue(final Iterator<K> keyIterator) {
        if (keyIterator.hasNext()) {
            final K key = keyIterator.next();
            final MultiIndexMapValue<K, V> value = this.cache.get(key);
            
            /** the following (nested) ternary expression is the same as
             * 
             * <pre>
             * if (value == null) {
             *     return null;
             * }
             * else if (keyIterator.hasNext()) {
             *     return value.getNestedCache().getMostNestedMIMValue(keyIterator);
             * }
             * else {
             *     return value;
             * }
             * </pre>
             * 
             * Using the ternary expression, however, improves the compilers branch prediction optimization */
            return value == null ? null
                                 : keyIterator.hasNext() ? value.getNestedCache().getMostNestedMIMValue(keyIterator)
                                                         : value;
        }
        
        return null; /* an "empty" Iterable can never map to anything using put with an "empty" Iterable, so we just
                      * return null */
    }
    
    /**
     * A {@link Set} of the keys of this {@link MultiIndexMap}
     * 
     * @author Ryan Moser, Ryan.T.Moser@lmco.com
     */
    private final class KeySet extends AbstractSet<Iterable<K>> {
        
        @Override
        public int size() {
            return MultiIndexMap.this.size();
        }
        
        @Override
        public void clear() {
            super.clear();
            MultiIndexMap.this.clear();
        }
        
        @Override
        public Iterator<Iterable<K>> iterator() {
            return new Iterator<>() {
                
                private final Iterator<Iterable<K>> backingItr = getKeyDeque(MultiIndexMap.this).iterator();
                private Iterable<K> lastReturned = null;
                
                @Override
                public boolean hasNext() {
                    return this.backingItr.hasNext();
                }
                
                @Override
                public Iterable<K> next() {
                    this.lastReturned = this.backingItr.next();
                    return this.lastReturned;
                }
                
                @Override
                public void remove() {
                    MultiIndexMap.this.remove(this.lastReturned);
                }
            };
        }
        
        /**
         * Recursively build a {@link Deque} of {@link Deque}s which represent the keys used to create this
         * {@link MultiIndexMap}. The value returned here includes any keys which were implicitly mapped to null when
         * performing put operations. <b>Note:</b> The order is not insertion order, but rather encounter order while
         * traversing the nested map structure.
         * 
         * @param map the {@link MultiIndexMap} for which we are creating a {@link KeySet}
         * @return a {@link Deque} of {@link Deque}s which represent the keys used to create this {@link MultiIndexMap}
         */
        private Set<Iterable<K>> getKeyDeque(final MultiIndexMap<K, V> map) {
            final Set<Iterable<K>> answer = new LinkedHashSet<>();
            for (final Entry<K, MultiIndexMapValue<K, V>> entry : map.cache.entrySet()) {
                final Set<Iterable<K>> partialKeySet = getKeyDeque(entry.getValue().getNestedCache());
                for (final Iterable<K> partialKey : partialKeySet) {
                    ((Deque<K>) partialKey).addFirst(entry.getKey()); // all sub-iterables are LinkedLists
                }
                
                final Deque<K> singleton = new LinkedList<>();
                singleton.add(entry.getKey());
                
                if (entry.getValue().getData() != null) {
                    answer.add(singleton);
                }
                
                answer.addAll(partialKeySet);
            }
            return answer;
        }
    }
    
    /**
     * A {@link Collection} of the {@code V}s of this {@link MultiIndexMap}
     * 
     * @author Ryan Moser, Ryan.T.Moser@lmco.com
     */
    private final class ValueCollection extends AbstractCollection<V> {
        
        @Override
        public int size() {
            return MultiIndexMap.this.size();
        }
        
        @Override
        public void clear() {
            super.clear();
            MultiIndexMap.this.clear();
        }
        
        @Override
        public Iterator<V> iterator() {
            return new Iterator<>() {
                
                private final Iterator<Entry<Iterable<K>, V>> backingItr = new EntrySet().iterator();
                
                @Override
                public boolean hasNext() {
                    return this.backingItr.hasNext();
                }
                
                @Override
                public V next() {
                    return this.backingItr.next().getValue();
                }
                
                @Override
                public void remove() {
                    this.backingItr.remove();
                }
            };
        }
    }
    
    /**
     * A {@link Set} of the entries of this {@link MultiIndexMap}
     * 
     * @author Ryan Moser, Ryan.T.Moser@lmco.com
     */
    private final class EntrySet extends AbstractSet<Entry<Iterable<K>, V>> {
        
        @Override
        public int size() {
            return MultiIndexMap.this.size();
        }
        
        @Override
        public void clear() {
            super.clear();
            MultiIndexMap.this.clear();
        }
        
        @Override
        public Iterator<Entry<Iterable<K>, V>> iterator() {
            return new Iterator<>() {
                
                private final Iterator<Iterable<K>> backingItr = new KeySet().iterator();
                
                @Override
                public boolean hasNext() {
                    return this.backingItr.hasNext();
                }
                
                @Override
                public Entry<Iterable<K>, V> next() {
                    final Iterable<K> keys = this.backingItr.next();
                    return new SimpleEntry<>(keys, MultiIndexMap.this.get(keys));
                }
                
                @Override
                public void remove() {
                    this.backingItr.remove();
                }
            };
        }
    }
}
