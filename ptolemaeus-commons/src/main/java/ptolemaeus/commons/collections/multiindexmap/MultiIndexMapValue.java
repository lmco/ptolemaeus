/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections.multiindexmap;

import java.util.Map;
import java.util.function.Supplier;

/**
 * The values of {@link MultiIndexMap}s. Instances of this class are how we nest {@link MultiIndexMap}s.
 * 
 * @param <K> the key type
 * @param <V> the value type
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class MultiIndexMapValue<K, V> {
    
    /**
     * A {@link Map} {@link Supplier} for this {@link MultiIndexMapValue}'s nested caches
     */
    private final Supplier<Map<K, MultiIndexMapValue<K, V>>> mapSupplier;
    
    /**
     * The nested {@link MultiIndexMap}
     */
    private final MultiIndexMap<K, V> nestedCache;
    
    /**
     * The {@code V} of this {@link MultiIndexMapValue}
     */
    private V data;
    
    /**
     * @param mapSupplier {@link Map} {@link Supplier} for this {@link MultiIndexMapValue}'s nested caches
     * @param data the {@code V} to associate with this {@link MultiIndexMapValue}
     */
    MultiIndexMapValue(final Supplier<Map<K, MultiIndexMapValue<K, V>>> mapSupplier, final V data) {
        this.mapSupplier = mapSupplier;
        this.nestedCache = new MultiIndexMap<>(this.mapSupplier);
        this.data = data;
    }
    
    /**
     * @return the {@link MultiIndexMap} of {@code this}. Never null.
     */
    MultiIndexMap<K, V> getNestedCache() {
        return nestedCache;
    }
    
    /**
     * @param data set the {@code V} of this
     */
    void setData(final V data) {
        this.data = data;
    }
    
    /**
     * @return the data
     */
    V getData() {
        return data;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        this.stringBuilderVisit(sb, 0);
        return sb.toString();
    }
    
    /**
     * Visit this {@link MultiIndexMapValue} and append it's data to the provided {@link StringBuilder}
     * 
     * @param builder the {@link StringBuilder} to which we wish to append data
     * @param indentCount the size of the indent to use for this {@link MultiIndexMapValue}'s data
     */
    void stringBuilderVisit(final StringBuilder builder, final int indentCount) {
        if (this.data != null) {
            final String indent = "    ".repeat(indentCount);
            builder.append(System.lineSeparator()).append(indent).append(this.data);
        }
        
        this.nestedCache.stringBuilderVisit(builder, indentCount + 1);
    }
    
    /**
     * Given two {@link MultiIndexMapValue}s, merge them. The first {@link MultiIndexMapValue} is given "priority" in
     * the sense that, if it is non-{@code null}, we merge the second {@link MultiIndexMapValue}'s cache into the
     * first's {@link MultiIndexMapValue}'s cache and set the first {@link MultiIndexMapValue}'s {@code V} to the second
     * {@link MultiIndexMapValue}'s. If the first {@link MultiIndexMapValue} is {@code null}, we simple return the
     * second {@link MultiIndexMapValue}, which also may be {@code null}. If both are {@code null}, we return
     * {@code null}.
     * 
     * @param <K> the map key type
     * @param <V> the map value type
     * @param v1 the {@link MultiIndexMapValue} into which we wish to merge {@code v2}
     * @param v2 the {@link MultiIndexMapValue} we wish to merge into {@code v1}
     * @return if both {@link MultiIndexMapValue}s are non-{@code null}, we return the first {@link MultiIndexMapValue}
     *         with the second merged into it. If the first is {@code null}, we return the second, unchanged. If both
     *         are {@code null}, we return {@code null}.
     */
    static <K, V> MultiIndexMapValue<K, V> merge(final MultiIndexMapValue<K, V> v1, final MultiIndexMapValue<K, V> v2) {
        if (v1 == null) {
            return v2;
        }
        
        if (v2 == null) {
            return v1;
        }
        
        v1.setData(v2.getData());
        MultiIndexMap.merge(v1.getNestedCache(), v2.getNestedCache());
        
        return v1;
    }
}