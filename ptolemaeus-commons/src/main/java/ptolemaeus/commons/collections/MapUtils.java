/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons.collections;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.function.Supplier;

/**
 * A collection of {@code static} utility/convenience methods for getting unmodifiable and <i>insertion order
 * preserving</i> {@link Map}s and unmodifiable {@link NavigableMap}s. This can function as a drop-in replacement for
 * the static {@link Map} methods
 * 
 * <ul>
 * <li>the {@link Map#of()} series</li>
 * <li>{@link Map#copyOf(Map)}</li>
 * </ul>
 * 
 * <p>
 * but not
 * </p>
 * 
 * <ul>
 * <li>{@link Map#entry(Object, Object)}</li>
 * <li>{@link Map#ofEntries(java.util.Map.Entry...)}</li>
 * </ul>
 * 
 * <p>
 * It also has several methods not found in that collection of methods:
 * </p>
 * 
 * <ul>
 * <li>{@link MapUtils#empty()}</li>
 * <li>{@link MapUtils#emptyNavigable()}</li>
 * <li>{@link MapUtils#emptyNavigable(Comparator)}</li>
 * <li>{@link MapUtils#copyOf(NavigableMap)}</li>
 * <li>{@link MapUtils#copyOf(Map, Supplier)}</li>
 * <li>{@link MapUtils#modifiableCopyOf(Map)}</li>
 * <li>{@link MapUtils#modifiableCopyOf(NavigableMap)}</li>
 * <li>the {@link MapUtils#navigableMapOf()} series</li>
 * <li>the {@link MapUtils#navigableMapOf(Object, Object, Object, Object, Comparator)} series</li>
 * </ul>
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public final class MapUtils {
    
    /**
     * private constructor to hide the implicit one
     */
    private MapUtils() {
        // do nothing
    }
    
    /**
     * Get an empty new and mutable {@link Map} which maintains insertion order. For an empty immutable map, use
     * {@link MapUtils#of()}
     * 
     * @param <U> key type
     * @param <V> value type
     * @return an empty {@link Map} which maintains insertion order
     */
    public static <U, V> Map<U, V> empty() {
        return new LinkedHashMap<>();
    }
    
    /**
     * Get an empty and new {@link NavigableMap} which will be sorted by the natural ordering of the key type
     * 
     * @param <U> key type
     * @param <V> value type
     * @return an empty and new {@link NavigableMap} which will be sorted by the natural ordering of the key type
     */
    public static <U, V> NavigableMap<U, V> emptyNavigable() {
        return emptyNavigable(null);
    }
    
    /**
     * Get an empty and new {@link NavigableMap} which will be sorted by the provided {@link Comparator}
     * 
     * @param <U> key type
     * @param <V> value type
     * @param comparator the {@link Comparator} which will be used to sort entries as they are added to the returned
     *        {@link NavigableMap}. May be null. If null, we use the natural ordering of the keys as the Comparator or
     *        an exception is thrown if the keys are not comparable. Use {@link #emptyNavigable()} rather than passing
     *        in null.
     * @return an empty and new {@link NavigableMap} which will be sorted by the provided {@link Comparator}
     */
    public static <U, V> NavigableMap<U, V> emptyNavigable(final Comparator<U> comparator) {
        return new TreeMap<>(comparator);
    }
    
    /**
     * Make a copy of the provided {@link Map}. The returned {@link Map} maintains insertion order of the provided
     * {@link Map} as it is copied. The returned {@link Map} is mutable, but changes to it will not be reflected in the
     * provided {@link Map} and changes to the provided {@link Map} will not be reflected in the returned {@link Map}.
     * 
     * @param <U> the key type
     * @param <V> the value type
     * @param toCopy the {@link Map} to copy
     * @return the copied {@link Map}
     */
    public static <U, V> Map<U, V> modifiableCopyOf(final Map<? extends U, ? extends V> toCopy) {
        return new LinkedHashMap<>(toCopy);
    }
    
    /**
     * Make an unmodifiable copy of the provided {@link Map}. The returned {@link Map} maintains insertion order of the
     * provided {@link Map} as it is copied. The returned {@link Map} is immutable, not just an unmodifiable view, so
     * changes in the provided {@link Map} will not be reflected in the returned {@link Map}.
     * 
     * @param <U> the key type
     * @param <V> the value type
     * @param toCopy the {@link Map} to copy
     * @return the copied {@link Map}
     */
    public static <U, V> Map<U, V> copyOf(final Map<? extends U, ? extends V> toCopy) {
        return Collections.unmodifiableMap(modifiableCopyOf(toCopy));
    }
    
    /**
     * Make a copy of the provided {@link NavigableMap}. The returned {@link NavigableMap} is mutable, but changes to it
     * will not be reflected in the provided {@link NavigableMap} and changes to the provided {@link NavigableMap} will
     * not be reflected in the returned {@link NavigableMap}. The returned {@link NavigableMap} maintains the
     * {@link Comparator} of the provided {@link NavigableMap}.
     * 
     * @param <U> the key type
     * @param <V> the value type
     * @param toCopy the {@link NavigableMap} to copy
     * @return the copied {@link NavigableMap}
     */
    public static <U, V> NavigableMap<U, V> modifiableCopyOf(final NavigableMap<U, ? extends V> toCopy) {
        return new TreeMap<>(toCopy);
    }
    
    /**
     * Make an unmodifiable copy of the provided {@link NavigableMap}. The returned {@link NavigableMap} is immutable,
     * not just an unmodifiable view, so changes in the provided {@link NavigableMap} will not be reflected in the
     * returned {@link NavigableMap}. The returned {@link NavigableMap} maintains the {@link Comparator} of the provided
     * {@link NavigableMap}.
     * 
     * @param <U> the key type
     * @param <V> the value type
     * @param toCopy the {@link NavigableMap} to copy
     * @return the copied {@link NavigableMap}
     */
    public static <U, V> NavigableMap<U, V> copyOf(final NavigableMap<U, ? extends V> toCopy) {
        return Collections.unmodifiableNavigableMap(modifiableCopyOf(toCopy));
    }
    
    /**
     * Use the provided {@link Supplier} to create a {@link Map} implementation into which we will copy the provided
     * {@link Map}
     * 
     * @param <U> the key type
     * @param <V> the value type
     * @param <M> the {@link Map} type
     * @param toCopy the {@link Map} to copy
     * @param mapSupplier a {@link Map} {@link Supplier}
     * @return the new {@link Map} which contains the entries of the provided {@link Map}
     */
    public static <U, V, M extends Map<U, V>> M copyOf(final Map<? extends U, ? extends V> toCopy,
                                                       final Supplier<? extends M> mapSupplier) {
        M map = mapSupplier.get();
        map.putAll(toCopy);
        return map;
    }
    
    // CHECKSTYLE.OFF: ParameterNumber
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with zero key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with zero key element
     *         pairs
     */
    public static <U, V> Map<U, V> of() {
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with one key element pair
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with one key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with two key element pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with two key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with three key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with three key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with four key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with four key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with five key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with five key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        linkedHashMap.put(u4, v4);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with six key element pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with six key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        linkedHashMap.put(u4, v4);
        linkedHashMap.put(u5, v5);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with seven key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with seven key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        linkedHashMap.put(u4, v4);
        linkedHashMap.put(u5, v5);
        linkedHashMap.put(u6, v6);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with eight key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with eight key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        linkedHashMap.put(u4, v4);
        linkedHashMap.put(u5, v5);
        linkedHashMap.put(u6, v6);
        linkedHashMap.put(u7, v7);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with nine key element
     * pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param u8 the ninth mapping's key
     * @param v8 the ninth mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with nine key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final U u8, final V v8) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        linkedHashMap.put(u4, v4);
        linkedHashMap.put(u5, v5);
        linkedHashMap.put(u6, v6);
        linkedHashMap.put(u7, v7);
        linkedHashMap.put(u8, v8);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with ten key element pairs
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of less-strict and order-guaranteeable (see note)
     * drop-ins for {@link Map#of()} when we need a {@link Map} of known size assuming there are no duplicate keys (see
     * note) but which also maintains insertion order.
     * </p>
     * <p>
     * Note: differences between this series of methods and the series of {@link Map#of()} methods are the allowance of
     * {@code null} keys and values and the allowance of duplicate keys. A side-effect of this is that the returned
     * {@link Map} does not have a guaranteed size; i.e. if there are duplicate keys in the key set, earlier values in
     * the value set associated with the earlier keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code Map}'s key type
     * @param <V> the {@code Map}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param u8 the ninth mapping's key
     * @param v8 the ninth mapping's value
     * @param u9 the tenth mapping's key
     * @param v9 the tenth mapping's value
     * @return an unmodifiable {@link Map} - an unmodifiable view of a {@link LinkedHashMap} - with ten key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> Map<U, V> of(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final U u8, final V v8,
            final U u9, final V v9) {
        // @formatter:on
        LinkedHashMap<U, V> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(u0, v0);
        linkedHashMap.put(u1, v1);
        linkedHashMap.put(u2, v2);
        linkedHashMap.put(u3, v3);
        linkedHashMap.put(u4, v4);
        linkedHashMap.put(u5, v5);
        linkedHashMap.put(u6, v6);
        linkedHashMap.put(u7, v7);
        linkedHashMap.put(u8, v8);
        linkedHashMap.put(u9, v9);
        return Collections.unmodifiableMap(linkedHashMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with zero key element
     * pairs.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with zero key element
     *         pairs
     */
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf() {
        TreeMap<U, V> treeMap = new TreeMap<>();
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with one key element
     * pair. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with one key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>();
        treeMap.put(u0, v0);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with two key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with two key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with three key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with three key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with four key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with four key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with five key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with five key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                u4, v4,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with six key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with six key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                u4, v4,
                u5, v5,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with seven key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with seven key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                u4, v4,
                u5, v5,
                u6, v6,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with eight key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with eight key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                u4, v4,
                u5, v5,
                u6, v6,
                u7, v7,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with nine key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param u8 the ninth mapping's key
     * @param v8 the ninth mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with nine key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final U u8, final V v8) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                u4, v4,
                u5, v5,
                u6, v6,
                u7, v7,
                u8, v8,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with ten key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param u8 the ninth mapping's key
     * @param v8 the ninth mapping's value
     * @param u9 the tenth mapping's key
     * @param v9 the tenth mapping's value
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with ten key element
     *         pairs
     */
    // @formatter:off
    public static <U extends Comparable<U>, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final U u8, final V v8,
            final U u9, final V v9) {
        return navigableMapOf(
                u0, v0,
                u1, v1,
                u2, v2,
                u3, v3,
                u4, v4,
                u5, v5,
                u6, v6,
                u7, v7,
                u8, v8,
                u9, v9,
                null);
        // @formatter:on
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with two key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with two key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with three key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with three key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with four key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with four key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with five key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with five key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        treeMap.put(u4, v4);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with six key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with six key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        treeMap.put(u4, v4);
        treeMap.put(u5, v5);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with seven key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with seven key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        treeMap.put(u4, v4);
        treeMap.put(u5, v5);
        treeMap.put(u6, v6);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with eight key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with eight key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        treeMap.put(u4, v4);
        treeMap.put(u5, v5);
        treeMap.put(u6, v6);
        treeMap.put(u7, v7);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with nine key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param u8 the ninth mapping's key
     * @param v8 the ninth mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with nine key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final U u8, final V v8,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        treeMap.put(u4, v4);
        treeMap.put(u5, v5);
        treeMap.put(u6, v6);
        treeMap.put(u7, v7);
        treeMap.put(u8, v8);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    /**
     * <p>
     * Create an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with ten key element
     * pairs. This follows the same rules as specified by {@link TreeMap} such as no {@code null} keys.
     * </p>
     * <p>
     * This series of methods is intended to serve as a collection of convenience methods for when we need a
     * {@link NavigableMap} of known size assuming there are no duplicate keys (see note).
     * </p>
     * <p>
     * Note: If there are duplicate keys in the key set, earlier values in the value set associated with the earlier
     * keys will be overwritten.
     * </p>
     * 
     * @param <U> the {@code NavigableMap}'s key type
     * @param <V> the {@code NavigableMap}'s value type
     * @param u0 the first mapping's key
     * @param v0 the first mapping's value
     * @param u1 the second mapping's key
     * @param v1 the second mapping's value
     * @param u2 the third mapping's key
     * @param v2 the third mapping's value
     * @param u3 the fourth mapping's key
     * @param v3 the fourth mapping's value
     * @param u4 the fifth mapping's key
     * @param v4 the fifth mapping's value
     * @param u5 the sixth mapping's key
     * @param v5 the sixth mapping's value
     * @param u6 the seventh mapping's key
     * @param v6 the seventh mapping's value
     * @param u7 the eighth mapping's key
     * @param v7 the eighth mapping's value
     * @param u8 the ninth mapping's key
     * @param v8 the ninth mapping's value
     * @param u9 the tenth mapping's key
     * @param v9 the tenth mapping's value
     * @param uComparator the {@link Comparator} to compare keys. May be null. If null, we use the natural ordering of
     *        the key type implying that the keys must be {@link Comparable}.
     * @return an unmodifiable {@link NavigableMap} - an unmodifiable view of a {@link TreeMap} - with ten key element
     *         pairs
     */
    // @formatter:off
    public static <U, V> NavigableMap<U, V> navigableMapOf(
            final U u0, final V v0,
            final U u1, final V v1,
            final U u2, final V v2,
            final U u3, final V v3,
            final U u4, final V v4,
            final U u5, final V v5,
            final U u6, final V v6,
            final U u7, final V v7,
            final U u8, final V v8,
            final U u9, final V v9,
            final Comparator<U> uComparator) {
        // @formatter:on
        TreeMap<U, V> treeMap = new TreeMap<>(uComparator);
        treeMap.put(u0, v0);
        treeMap.put(u1, v1);
        treeMap.put(u2, v2);
        treeMap.put(u3, v3);
        treeMap.put(u4, v4);
        treeMap.put(u5, v5);
        treeMap.put(u6, v6);
        treeMap.put(u7, v7);
        treeMap.put(u8, v8);
        treeMap.put(u9, v9);
        return Collections.unmodifiableNavigableMap(treeMap);
    }
    
    // CHECKSTYLE.ON: ParameterNumber
}
