/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * An unmodifiable {@link LinkedList}. Attempts to modify an instance will cause an
 * {@link UnsupportedOperationException} to be thrown.
 * 
 * @param <E> the type of the elements of the {@link UnmodifiableLinkedList}
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public final class UnmodifiableLinkedList<E> implements Deque<E>, List<E>, Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * The backing list of this {@link UnmodifiableLinkedList}. All valid calls will be passed through to this.
     */
    private final LinkedList<? extends E> backingList;
    
    /**
     * Hidden constructor. Use the factory methods.
     */
    private UnmodifiableLinkedList() {
        this(new LinkedList<>());
    }
    
    /**
     * Hidden constructor. Use the factory methods.
     * 
     * @param backingList the backing list for the instance being created
     */
    private UnmodifiableLinkedList(final LinkedList<? extends E> backingList) {
        this.backingList = backingList;
    }
    
    /**
     * Get a copy of the provided {@link Collection} as an {@link UnmodifiableLinkedList}
     * 
     * @param <E> the type of the elements of the {@link UnmodifiableLinkedList} to be returned
     * @param toCopy the {@link Collection} to copy
     * @return a copy of the provided {@link Collection} as an {@link UnmodifiableLinkedList}
     */
    public static <E> UnmodifiableLinkedList<E> copyOf(final Collection<? extends E> toCopy) {
        return new UnmodifiableLinkedList<>(new LinkedList<>(toCopy));
    }
    
    /**
     * Get an unmodifiable view of the provided {@link LinkedList} as an {@link UnmodifiableLinkedList}
     * 
     * @param <E> the type of the elements of the {@link UnmodifiableLinkedList} to be returned
     * @param toGetViewOf the {@link LinkedList} of which we want an unmodifiable view
     * @return an unmodifiable view of the provided {@link LinkedList} as an {@link UnmodifiableLinkedList}
     */
    public static <E> UnmodifiableLinkedList<E> viewOf(final LinkedList<? extends E> toGetViewOf) {
        return new UnmodifiableLinkedList<>(toGetViewOf);
    }
    
    /** Create and return a new {@link UnmodifiableLinkedList} containing the provided {@code Es} in encounter order
     * 
     * @param <E> the element type
     * @param es the {@code Es} that we want to include in the {@link UnmodifiableLinkedList}
     * @return a new {@link UnmodifiableLinkedList} containing the provided {@code Es} in encounter order */
    @SafeVarargs
    public static <E> UnmodifiableLinkedList<E> of(final E... es) {
        final LinkedList<E> answer = new LinkedList<>();
        for (final E e : es) {
            answer.add(e);
        }
        
        return viewOf(answer);
    }
    
    /**
     * Get a new {@link UnsupportedOperationException} with the provided message
     *
     * @return a new {@link UnsupportedOperationException} with the provided message
     */
    private static UnsupportedOperationException uoe() {
        return new UnsupportedOperationException();
    }
    
    // CHECKSTYLE.OFF: LeftCurly - Expanding these takes up so much unnecessary space
    @Override public boolean add(final E e)                                           { throw uoe(); }
    @Override public void    add(final int index, final E element)                    { throw uoe(); }
    @Override public boolean addAll(final Collection<? extends E> c)                  { throw uoe(); }
    @Override public boolean addAll(final int index, final Collection<? extends E> c) { throw uoe(); }
    @Override public void    addFirst(final E e)                                      { throw uoe(); }
    @Override public void    addLast(final E e)                                       { throw uoe(); }
    @Override public void    clear()                                                  { throw uoe(); }
    @Override public boolean offer(final E e)                                         { throw uoe(); }
    @Override public boolean offerFirst(final E e)                                    { throw uoe(); }
    @Override public boolean offerLast(final E e)                                     { throw uoe(); }
    @Override public E       poll()                                                   { throw uoe(); }
    @Override public E       pollFirst()                                              { throw uoe(); }
    @Override public E       pollLast()                                               { throw uoe(); }
    @Override public E       pop()                                                    { throw uoe(); }
    @Override public void    push(final E e)                                          { throw uoe(); }
    @Override public E       remove()                                                 { throw uoe(); }
    @Override public E       remove(final int index)                                  { throw uoe(); }
    @Override public boolean remove(final Object o)                                   { throw uoe(); }
    @Override public boolean removeAll(final Collection<?> c)                         { throw uoe(); }
    @Override public E       removeFirst()                                            { throw uoe(); }
    @Override public boolean removeFirstOccurrence(final Object o)                    { throw uoe(); }
    @Override public boolean removeIf(final Predicate<? super E> filter)              { throw uoe(); }
    @Override public E       removeLast()                                             { throw uoe(); }
    @Override public boolean removeLastOccurrence(final Object o)                     { throw uoe(); }
    @Override public void    replaceAll(final UnaryOperator<E> operator)              { throw uoe(); }
    @Override public boolean retainAll(final Collection<?> c)                         { throw uoe(); }
    @Override public E       set(final int index, final E element)                    { throw uoe(); }
    @Override public void    sort(final Comparator<? super E> c)                      { throw uoe(); }
    // CHECKSTYLE.ON: LeftCurly
    
    @Override
    public E get(final int index) {
        return this.backingList.get(index);
    }
    
    @Override
    public E getFirst() {
        return this.backingList.getFirst();
    }
    
    @Override
    public E getLast() {
        return this.backingList.getLast();
    }
    
    @Override
    public E peekFirst() {
        return this.backingList.peekFirst();
    }
    
    @Override
    public E peekLast() {
        return this.backingList.peekLast();
    }
    
    @Override
    public E element() {
        return this.backingList.element();
    }
    
    @Override
    public E peek() {
        return this.backingList.peek();
    }
    
    @Override
    public int size() {
        return this.backingList.size();
    }
    
    @Override
    public boolean isEmpty() {
        return this.backingList.isEmpty();
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.backingList.contains(o);
    }
    
    @Override
    public boolean containsAll(final Collection<?> c) {
        return this.backingList.containsAll(c);
    }
    
    @Override
    public int indexOf(final Object o) {
        return this.backingList.indexOf(o);
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        return this.backingList.lastIndexOf(o);
    }
    
    @Override
    public boolean equals(final Object obj) {
        return Objects.equals(this.backingList, obj);
    }
    
    @Override
    public String toString() {
        return this.backingList.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.backingList);
    }
    
    @Override
    public Object[] toArray() {
        return this.backingList.toArray();
    }
    
    @Override
    public <T> T[] toArray(final T[] a) {
        return this.backingList.toArray(a);
    }
    
    @Override
    public List<E> subList(final int fromIndex, final int toIndex) {
        return Collections.unmodifiableList(this.backingList.subList(fromIndex, toIndex));
    }
    
    @Override
    public Spliterator<E> spliterator() {
        return Spliterators.spliterator(this.backingList, 0); // see Collections::spliterator
    }
    
    @Override
    public Iterator<E> iterator() {
        final Iterator<? extends E> descItr = this.backingList.iterator();
        return new Iterator<>() {
            
            @Override
            public boolean hasNext() {
                return descItr.hasNext();
            }
            
            @Override
            public E next() {
                return descItr.next();
            }
        };
    }
    
    @Override
    public Iterator<E> descendingIterator() {
        final Iterator<? extends E> descItr = this.backingList.descendingIterator();
        return new Iterator<>() {
            
            @Override
            public boolean hasNext() {
                return descItr.hasNext();
            }
            
            @Override
            public E next() {
                return descItr.next();
            }
        };
    }
    
    @Override
    public ListIterator<E> listIterator() {
        return listIterator(0);
    }
    
    @Override
    public ListIterator<E> listIterator(final int index) {
        final ListIterator<? extends E> listItr = this.backingList.listIterator(index);
        return new ListIterator<>() {
            
            @Override
            public boolean hasNext() {
                return listItr.hasNext();
            }
            
            @Override
            public E next() {
                return listItr.next();
            }
            
            @Override
            public boolean hasPrevious() {
                return listItr.hasPrevious();
            }
            
            @Override
            public E previous() {
                return listItr.previous();
            }
            
            @Override
            public int nextIndex() {
                return listItr.nextIndex();
            }
            
            @Override
            public int previousIndex() {
                return listItr.previousIndex();
            }
            
            // CHECKSTYLE.OFF: LeftCurly
            @Override public void remove() { throw uoe(); }
            @Override public void set(final E e) { throw uoe(); }
            @Override public void add(final E e) { throw uoe(); }
            // CHECKSTYLE.ON: LeftCurly
        };
    }
}
