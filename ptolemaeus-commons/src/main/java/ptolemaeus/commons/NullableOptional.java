/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * A {@link NullableOptional} represents a (possibly null) value that may or may not be present. This class 
 * emulates the behavior and public signature of {@link java.util.Optional}, but allows for {@code null} values. Since 
 * the entire purpose of {@link java.util.Optional} is to cut down on null-checking in code, this class should only be 
 * used in cases where an explicit null value has merit.
 * 
 * <br>
 * 
 * This is a 
 * <a href="https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/doc-files/ValueBased.html">
 * Value-based</a>
 * class; use of identity-sensitive operations (including reference equality ({@code ==}), identity hash code, or 
 * synchronization) on instances of {@link NullableOptional} may have unpredictable results and should be avoided.
 * @param <T> the value type.
 * @author Peter Davis, peter.m.davis@lmco.com
 */
public final class NullableOptional<T> implements Supplier<T> {

    /**
     * The singleton empty {@link NullableOptional}.
     */
    private static final NullableOptional<?> EMPTY_OPTIONAL = new NullableOptional<>(false, null);

    /**
     * Whether or not the value of this {@link NullableOptional} is present
     */
    private final boolean isPresent;

    /**
     * The value of this {@link NullableOptional}
     */
    private final T value;
    
    /**
     * Private constructor 
     * @param isPresent whether or not the value of this {@link NullableOptional} is present
     * @param value The value of this {@link NullableOptional}
     */
    private NullableOptional(final boolean isPresent, final T value) {
        this.isPresent = isPresent;
        this.value = value;
    }

    /**
     * Return {@code true} if this {@link NullableOptional} is present, false otherwise.
     * @return {@code true} if this {@link NullableOptional} is present, false otherwise.
     */
    public boolean isPresent() {
        return isPresent;
    }

    /**
     * Return {@code true} if this {@link NullableOptional} is empty, false otherwise.
     * @return {@code true} if this {@link NullableOptional} is empty, false otherwise.
     */
    public boolean isEmpty() {
        return !isPresent;
    }

    /**
     * Performs the specified action if this {@link NullableOptional} is present
     * @param action the action to perform using the value
     */
    public void ifPresent(final Consumer<? super T> action) {
        Objects.requireNonNull(action, "action cannot be null");
        if (isPresent) {
            action.accept(value);
        }
    }

    /**
     * Performs the specified action if this {@link NullableOptional} is present, otherwise
     * perform the specified empty action.
     * @param action the action to perform if this {@link NullableOptional} is present
     * @param emptyAction the action to perform if this {@link NullableOptional} is empty
     */
    public void ifPresentOrElse(final Consumer<? super T> action, final Runnable emptyAction) {
        Objects.requireNonNull(action, "action cannot be null");
        Objects.requireNonNull(emptyAction, "emptyAction cannot be null");
        if (isPresent) {
            action.accept(value);
        }
        else {
            emptyAction.run();
        }
    }

    /**
     * If this {@link NullableOptional} is present and matches the predicate, returns a {@link NullableOptional} 
     * describing the value, otherwise returns an empty {@link NullableOptional}
     * @param predicate the predicate to apply to the value, if present
     * @return a {@link NullableOptional} describing the value of this {@link NullableOptional}, if present and
     * the value matches the given predicate, otherwise an empty {@link NullableOptional}
     */
    public NullableOptional<T> filter(final Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate, "predicate cannot be null");
        if (isPresent && predicate.test(value)) {
            return this;
        }
        return NullableOptional.empty();
    }

    /**
     * If a value is present, returns the result of applying the given {@link NullableOptional}-bearing mapping function
     * to the value, otherwise returns an empty {@link NullableOptional}.
     * @param <U> the type of value of the {@link NullableOptional} returned by the mapping function
     * @param mapper the mapping function to apply to a value, if present
     * @return the result of applying a {@link NullableOptional}-bearing mapping function to the value of this 
     * {@link NullableOptional}, if a value is present, otherwise an empty {@link NullableOptional}
     */
    public <U> NullableOptional<U> flatMap(final Function<? super T, NullableOptional<? extends U>> mapper) {
        Objects.requireNonNull(mapper, "mapper function cannot be null");
        if (isPresent) {
            @SuppressWarnings("unchecked")
            NullableOptional<U> r = (NullableOptional<U>) mapper.apply(value);
            Objects.requireNonNull(r, () -> "mapper function produced a null NullableOptional for value " + value);
            return r;
        }
        return NullableOptional.empty();
    }

    /**
     * If a value is present, return the value, otherwise return {@code other}
     * @param other the value to be returned if no value is present.
     * @return the value, if present, otherwise {@code other}
     */
    public T orElse(final T other) {
        return isPresent ? value : other;
    } 

    /**
     * If a value is present, returns the value, otherwise return the result produced by the supplying function
     * @param supplier the supplying function that produces a value to be returned
     * @return the value, if present, otherwise the result produced by the supplying function
     */
    public T orElseGet(final Supplier<? extends T> supplier) {
        Objects.requireNonNull(supplier, "supplier cannot be null");
        return isPresent ? value : supplier.get();
    }
    
    /**
     * If a value is present, returns the value, otherwise throws an exception produced by the exception supplying 
     * function
     * @param <X> Type of the exception being thrown
     * @param exceptionSupplier the supplying function that produces an exception to be thrown
     * @return the value, if present
     * @throws X if no value is present
     */
    public <X extends Throwable> T orElseThrow(final Supplier<? extends X> exceptionSupplier) throws X {
        Objects.requireNonNull(exceptionSupplier, "exceptionSupplier cannot be null");
        if (isPresent) {
            return value;
        }
        throw exceptionSupplier.get();
    }

    /**
     * If a value is present, return a {@link NullableOptional} describing the value, otherwise returns a 
     * {@link NullableOptional} produced by the supplying function.
     * @param supplier the supplying function that produces a {@link NullableOptional} to be returned
     * @return a {@link NullableOptional} describing the value of this {@link NullableOptional}, if a value is present, 
     * otherwise produce a {@link NullableOptional} produced by the supplying function
     */
    public NullableOptional<T> or(final Supplier<? extends NullableOptional<? extends T>> supplier) {
        Objects.requireNonNull(supplier, "supplier cannot be null");
        if (isPresent) {
            return this;
        }
        @SuppressWarnings("unchecked")
        NullableOptional<T> r = (NullableOptional<T>) supplier.get();
        Objects.requireNonNull(r, "supplier produced a null NullableOptional");
        return r;
    }

    /**
     * If a value is present, returns the value, otherwise throws {@link NoSuchElementException}
     * @return the possibly-null value described by this {@link NullableOptional}
     */
    @Override
    public T get() {
        if (!isPresent) {
            throw new NoSuchElementException("Value is not present.");
        }
        return value;
    }

    /**
     * If a value is present, returns the value, otherwise throws {@link NoSuchElementException}
     * @return the possibly-null value described by this {@link NullableOptional}
     */
    public T orElseThrow() {
        return get();
    }

    /**
     * Return the value, which will possibly be {@code null}, or just {@code null} if this {@link NullableOptional} is 
     * empty.
     * @return the possibly-null value, or {@code null} if the value is not present.
     */
    public T getOrNull() {
        return value;
    }

    /**
     * If a value is present, returns a {@link NullableOptional} describing (as if by {@link #of}) the result of 
     * applying the given mapping function to the value, otherwise an empty {@link NullableOptional}.
     * <br>
     * @param <U> the type of the value returned from the mapping function
     * @param mappingFunction the mapping function to apply to a value, if present.
     * @return a {@link NullableOptional} describing the result of applying a mapping function to the value of this 
     * {@link NullableOptional}, if a value is present, otherwise an empty {@link NullableOptional}
     */
    public <U> NullableOptional<U> map(final Function<? super T, ? extends U> mappingFunction) {
        Objects.requireNonNull(mappingFunction, "mappingFunction cannot be null");
        if (isPresent) {
            return NullableOptional.of(mappingFunction.apply(value));
        }
        return NullableOptional.empty();
    }

    /**
     * If a value is present and not null, returns a {@link NullableOptional} describing (as if by {@link #of}) the 
     * result of applying the given mapping function to the value, otherwise an empty {@link NullableOptional}.
     * <br>
     * @param <U> the type of the value returned from the mapping function
     * @param mappingFunction the mapping function to apply to a value, if present and not null
     * @return a {@link NullableOptional} describing the result of applying a mapping function to the value of this 
     * {@link NullableOptional}, if a value is present and non-null, otherwise an empty {@link NullableOptional}
     */
    public <U> NullableOptional<U> mapIfNotNull(final Function<? super T, ? extends U> mappingFunction) {
        Objects.requireNonNull(mappingFunction, "mappingFunction cannot be null");
        if (isPresent && value != null) {
            return NullableOptional.of(mappingFunction.apply(value));
        }
        return NullableOptional.empty();
    }

    /**
     * If a value is present, return a sequential {@link Stream} containing only that value, otherwise returns
     * an empty {@link Stream}
     * @return the tentative value as a {@link Stream}
     */
    public Stream<T> stream() {
        if (isPresent) {
            return Stream.of(value);
        }
        return Stream.empty();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        if (this == obj) {
            return true;
        }
        
        NullableOptional<?> other = (NullableOptional<?>) obj;

        return this.isPresent == other.isPresent
                && Objects.equals(this.value, other.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isPresent, value);
    }

    @Override
    public String toString() {
        return isPresent ? "NullableOptional.present[%s]".formatted(value) : "NullableOptional.empty";
    }

    /**
     * Returns a {@link NullableOptional} describing the given nullable value
     * @param <T> the type of the value
     * @param value the value to describe, which may be null
     * @return a {@link NullableOptional} with the value present
     */
    public static <T> NullableOptional<T> of(final T value) {
        return new NullableOptional<>(true, value);
    }

    /**
     * Returns an empty {@link NullableOptional} instance
     * @param <T> the type of the empty value
     * @return an empty {@link NullableOptional}
     */
    @SuppressWarnings("unchecked")
    public static <T> NullableOptional<T> empty() {
        return (NullableOptional<T>) EMPTY_OPTIONAL;
    }
    
}
