/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons;

import java.time.Duration;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.concurrent.ThreadSafe;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A thread-safe memoizer which stores results in a configurable {@link Map} implementation. Concurrent calls to 
 * {@link #computeIfAbsent} will avoid recomputing the function multiple times by blocking later callers until the 
 * first caller has finished.
 * 
 * @param <I> the input to the function.
 * @param <O> the output of the function.
 * @author Peter Davis, peter.m.davis@lmco.com
 */
@ThreadSafe
public class ConcurrentMemoizer<I, O> {
    
    /**
     * The computing {@link Function}
     */
    private final Function<I, O> function;

    /**
     * The {@link Map} that caches the working results.
     */
    private final Map<I, Future<O>> futureCache;

    /**
     * If null, then all exceptions during the computation of the function are re-thrown.  If non-null,
     * then the exceptions are delivered to this {@link Consumer}, and the function result is set to null.
     */
    private final Consumer<Exception> exceptionHandler;
    
    /**
     * The logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(ConcurrentMemoizer.class);

    /**
     * Construct the {@link ConcurrentMemoizer} with a {@link Map} that is supplied by {@code mapSupplier} and then
     * wrapped via {@link Collections#synchronizedMap(Map)}. 
     * @param function the computing function.
     * @param mapSupplier the {@link Supplier} for the map implementation.
     * @param anExceptionHandler If null, then all exceptions during the computation of the function are re-thrown.  
     * If non-null, then the exceptions are delivered to this {@link Consumer}, and the function result is set to null.
     */
    public ConcurrentMemoizer(final Function<I, O> function, final Supplier<Map<I, Future<O>>> mapSupplier,
            final Consumer<Exception> anExceptionHandler) {
        this.function = Objects.requireNonNull(function, "function cannot be null");
        Objects.requireNonNull(mapSupplier, "mapSupplier cannot be null");
        futureCache = Collections.synchronizedMap(mapSupplier.get());
        exceptionHandler = anExceptionHandler;
    }

    /**
     * Construct the {@link ConcurrentMemoizer} with a {@link LinkedHashMap} as the backing cache. This will behave as
     * an unbounded, thread-safe cache.
     * @param function the computing function.
     */
    public ConcurrentMemoizer(final Function<I, O> function) {
        this(function, LinkedHashMap::new, null);
    }

    /**
     * Checks the cache for the provided input, computing it if it's not present. 
     * On a cache hit, return the cached result, otherwise solve and cache the result, then return it. However, if 
     * another thread is currently computing the function for that input, this method will 
     * <strong>block</strong> until that thread finishes computing.
     * @param input the {@code I} to compute a {@code O} for.
     * @return the {@code O} corresponding to {@code input}. May be {@code null} if the computing function produced 
     * {@code null} for the given input.
     */
    @Blocking
    public O computeIfAbsent(final I input) {
        FutureTask<O> possibleFuture = new FutureTask<>(() -> function.apply(input));
        Future<O> cachedOutput = futureCache.computeIfAbsent(input, key -> possibleFuture);
        
        try {   
            O result;
            if (possibleFuture == cachedOutput) {
                // Cache miss: we added the Future, so we must compute it outside of the lock.
                possibleFuture.run();
                result = possibleFuture.get();
            }
            else {
                // Cache hit: wait on the cached result
                result = cachedOutput.get();
            }
            return result;
        }
        catch (ExecutionException ex) {
            handle(ex, "Exception occurred computing function.");
            return null;
        }
        catch (InterruptedException ex) {
            handle(ex, "Interrupted waiting for function result.");
            return null;
        }
    }
    
    /**
     * Get the memoized value corresponding to {@code input}, if it has been computed. Otherwise return an empty 
     * {@link NullableOptional}. This method will never wait on threads that are currently computing the result, and 
     * will return immediately if a thread is computing but hasn't finished.
     * @param input the {@code I} to get a {@code O} for.
     * @return a {@link NullableOptional} for the {@code O} corresponding to {@code input}, or an empty 
     * {@link NullableOptional} if the result for {@code input} is not available right now.
     */
    @NonBlocking
    public NullableOptional<O> get(final I input) {
        Future<O> cachedOutput = futureCache.get(input);

        if (cachedOutput == null || !cachedOutput.isDone()) {
            return NullableOptional.empty();
        }
        else {
            try {
                return NullableOptional.of(cachedOutput.get());
            }
            catch (ExecutionException ex) {
                handle(ex, "Exception occurred computing function.");
                return null;
            }
            catch (InterruptedException ex) {
                handle(ex, "Exception occurred retrieving a Future that is done. This shouldn't happen.");
                return null;
            }
        }
    }

    /**
     * Get the memoized value corresponding to {@code input}, if it is in the cache and has been computed. If it's not
     * in the cache at all, returns an empty {@link NullableOptional}. If it is in the cache but hasn't finished 
     * computing, this method waits up to the specified time period before throwing an exception.
     * @param input the {@code I} to get a {@code O} for.
     * @param timeout the maximum time to wait.
     * @return a {@link NullableOptional} for the {@code O} corresponding to {@code input}, or an empty 
     * {@link NullableOptional} if no thread is currently computing the result.
     * @throws TimeoutException if we time out waiting for the result to finish.
     */
    @Blocking
    public NullableOptional<O> get(final I input, final Duration timeout) throws TimeoutException {
        Future<O> cachedOutput = futureCache.get(input);

        if (cachedOutput == null) {
            return NullableOptional.empty();
        }
        else {
            try {
                return NullableOptional.of(cachedOutput.get(timeout.toMillis(), TimeUnit.MILLISECONDS));
            }
            catch (ExecutionException ex) {
                handle(ex, "Exception occurred computing function.");
                return null;
            }
            catch (InterruptedException ex) {
                handle(ex, "Exception occurred retrieving completed Future. This shouldn't happen.");
                return null;
            }
        }
    }

    /**
     * Handle an exception that has occurred
     * @param ex the exception
     * @param msg message to accompany the exception
     */
    void handle(final Exception ex, final String msg) {
        LOGGER.error(msg, ex);
        if (ex instanceof InterruptedException) {
            Thread.currentThread().interrupt();
        }
        if (exceptionHandler == null) {
            throw new RuntimeException(ex);
        }
        exceptionHandler.accept(ex);
    }

    /**
     * Clears the underlying map.
     */
    public void clear() {
        futureCache.clear();
    }

}
