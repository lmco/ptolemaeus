/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertThrowsWithMessage;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import org.hipparchus.util.FastMath;

import ptolemaeus.commons.collections.MapUtils;

/** Test {@link Validation}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 * @author Peter Davis, peter.m.davis@lmco.com */
public class ValidationTest {
    
    /** a simple message supplier for testing */
    private static final Supplier<String> MESSAGE_SUPPLIER = () -> "testSupplier";
    
    // CHECKSTYLE.OFF: LineLength
    
    /** Test {@link Validation#twoDoublesMetaChecks(double, double, String)}
     *  and  {@link Validation#twoDoublesMetaChecks(double, double, Supplier)} */
    @Test
    void testTwoDoubleChecks() {
        assertDoesNotThrow(() -> Validation.twoDoublesMetaChecks(123.0, 456.0, "aName"));
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.twoDoublesMetaChecks(Double.NaN, 456.0, "aName"),
                                "Cannot compare aName with value NaN to 456.000000");
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.twoDoublesMetaChecks(123.0, Double.NaN, "aName"),
                                "Cannot compare aName with value 123.000000 to NaN");
        
        assertDoesNotThrow(() -> Validation.twoDoublesMetaChecks(123.0, 456.0, MESSAGE_SUPPLIER));
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.twoDoublesMetaChecks(Double.NaN, 456.0, MESSAGE_SUPPLIER),
                                "Cannot compare NaN to 456.000000.  testSupplier");
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.twoDoublesMetaChecks(123.0, Double.NaN, MESSAGE_SUPPLIER),
                                "Cannot compare 123.000000 to NaN.  testSupplier");
    }
    
    /** Test {@link Validation#requireNonNaN(double, String)}
     *  and  {@link Validation#requireNonNaN(double, Supplier)} */
    @Test
    void testRequireNonNaN() {
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNonNaN(Double.NaN, "aNaN"), "Parameter aNaN is NaN");
        assertEquals(Double.NEGATIVE_INFINITY, Validation.requireNonNaN(Double.NEGATIVE_INFINITY, "NaNaN"));
        
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNonNaN(Double.NaN, MESSAGE_SUPPLIER), "testSupplier");
        assertEquals(Double.NEGATIVE_INFINITY, Validation.requireNonNaN(Double.NEGATIVE_INFINITY, MESSAGE_SUPPLIER));
    }
    
    /** Test {@link Validation#requireFinite(double, String)}
     *  and  {@link Validation#requireFinite(double, Supplier)} */
    @Test
    void testRequireFinite() {
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireFinite(Double.NaN, "aNaN"), "Parameter aNaN is NaN");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireFinite(Double.POSITIVE_INFINITY, "infty"), "Parameter infty is Infinity but should be finite");
        assertEquals(0.0, Validation.requireFinite(0.0, "NaNaN"));
        
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireFinite(Double.NaN, MESSAGE_SUPPLIER), "testSupplier");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireFinite(Double.POSITIVE_INFINITY, MESSAGE_SUPPLIER), "testSupplier");
        assertEquals(0.0, Validation.requireFinite(0.0, MESSAGE_SUPPLIER));
    }
    
    /** Test {@link Validation#requireNull(Object, String)} and {@link Validation#requireNull(Object, Supplier)} */
    @Test
    void testRequireNull() {
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNull(new Object(), "obj"), "obj must be null");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNull(new Object(), MESSAGE_SUPPLIER), MESSAGE_SUPPLIER.get());
        
        assertDoesNotThrow(() -> Validation.requireNull(null, "obj"));
        assertDoesNotThrow(() -> Validation.requireNull(null, MESSAGE_SUPPLIER));
    }
    
    /** Test {@link Validation#requireGreaterThan(int, int, String)},
     *       {@link Validation#requireGreaterThan(long, long, String)}, and
     *       {@link Validation#requireGreaterThan(double, double, String)} */
    @Test
    void testRequireGreaterThan() {
        final int lt = 123;
        final int gt = 456;
        
        final int v1    = assertDoesNotThrow(() -> Validation.requireGreaterThan(gt, lt, "int value"));
        final long v2   = assertDoesNotThrow(() -> Validation.requireGreaterThan((long) gt, (long) lt, "long value"));
        final double v3 = assertDoesNotThrow(() -> Validation.requireGreaterThan((double) gt, (double) lt, "double value"));
        
        assertEquals(gt, v1);
        assertEquals(gt, v2);
        assertEquals(gt, v3);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThan(lt, gt, "int value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThan((long) lt, (long) gt, "long value"));
        final ValidationException ve3 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThan((double) lt, (double) gt, "double value"));
        
        assertEquals("Parameter int value with value 123 is not greater than 456",        ve1.getMessage());
        assertEquals("Parameter long value with value 123 is not greater than 456",       ve2.getMessage());
        assertEquals("Parameter double value with value 123.0 is not greater than 456.0", ve3.getMessage());
        
        assertThrows(ValidationException.class, () -> Validation.requireGreaterThan(lt, lt, "int value"));
        assertThrows(ValidationException.class, () -> Validation.requireGreaterThan((long) lt, (long) lt, "long value"));
        assertThrows(ValidationException.class, () -> Validation.requireGreaterThan((double) lt, (double) lt, "double value"));
    }
    
    /** Test {@link Validation#requireGreaterThanEqualTo(int, int, String)},
     *       {@link Validation#requireGreaterThanEqualTo(long, long, String)}, and
     *       {@link Validation#requireGreaterThanEqualTo(double, double, String)} */
    @Test
    void testRequireGreaterThanEqualTo() {
        final int lt = 123;
        final int gt = 456;
        
        final int v1    = assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo(gt, lt, "int value"));
        final long v2   = assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo((long) gt, (long) lt, "long value"));
        final double v3 = assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo((double) gt, (double) lt, "double value"));
        
        assertEquals(gt, v1);
        assertEquals(gt, v2);
        assertEquals(gt, v3);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThanEqualTo(lt, gt, "int value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThanEqualTo((long) lt, (long) gt, "long value"));
        final ValidationException ve3 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThanEqualTo((double) lt, (double) gt, "double value"));
        final ValidationException ve4 = assertThrows(ValidationException.class, () -> Validation.requireGreaterThanEqualTo(lt, gt, MESSAGE_SUPPLIER));
        
        assertEquals("Parameter int value with value 123 is not greater than or equal to 456",        ve1.getMessage());
        assertEquals("Parameter long value with value 123 is not greater than or equal to 456",       ve2.getMessage());
        assertEquals("Parameter double value with value 123.0 is not greater than or equal to 456.0", ve3.getMessage());
        assertEquals("testSupplier", ve4.getMessage());
        
        assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo(lt, lt, "int value"));
        assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo((long) lt, (long) lt, "long value"));
        assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo((double) lt, (double) lt, "double value"));
        assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo(lt, lt, MESSAGE_SUPPLIER));
    }
    
    /** Test {@link Validation#requireLessThan(int, int, String)},
     *       {@link Validation#requireLessThan(long, long, String)}, and
     *       {@link Validation#requireLessThan(double, double, String)} */
    @Test
    void testRequireLessThan() {
        final int lt = 123;
        final int gt = 456;
        
        final int v1    = assertDoesNotThrow(() -> Validation.requireLessThan(lt, gt, "int value"));
        final long v2   = assertDoesNotThrow(() -> Validation.requireLessThan((long) lt, (long) gt, "long value"));
        final double v3 = assertDoesNotThrow(() -> Validation.requireLessThan((double) lt, (double) gt, "double value"));
        
        assertEquals(lt, v1);
        assertEquals(lt, v2);
        assertEquals(lt, v3);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireLessThan(gt, lt, "int value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireLessThan((long) gt, (long) lt, "long value"));
        final ValidationException ve3 = assertThrows(ValidationException.class, () -> Validation.requireLessThan((double) gt, (double) lt, "double value"));
        
        assertEquals("Parameter int value with value 456 is not less than 123",        ve1.getMessage());
        assertEquals("Parameter long value with value 456 is not less than 123",       ve2.getMessage());
        assertEquals("Parameter double value with value 456.0 is not less than 123.0", ve3.getMessage());
        
        assertThrows(ValidationException.class, () -> Validation.requireLessThan(lt, lt, "int value"));
        assertThrows(ValidationException.class, () -> Validation.requireLessThan((long) lt, (long) lt, "long value"));
        assertThrows(ValidationException.class, () -> Validation.requireLessThan((double) lt, (double) lt, "double value"));
    }
    
    /** Test {@link Validation#requireLessThanEqualTo(int, int, String)},
     *       {@link Validation#requireLessThanEqualTo(long, long, String)}, and
     *       {@link Validation#requireLessThanEqualTo(double, double, String)} */
    @Test
    void testRequireLessThanEqualTo() {
        final int lt = 123;
        final int gt = 456;
        
        final int v1    = assertDoesNotThrow(() -> Validation.requireLessThanEqualTo(lt, gt, "int value"));
        final long v2   = assertDoesNotThrow(() -> Validation.requireLessThanEqualTo((long) lt, (long) gt, "long value"));
        final double v3 = assertDoesNotThrow(() -> Validation.requireLessThanEqualTo((double) lt, (double) gt, "double value"));
        
        assertEquals(lt, v1);
        assertEquals(lt, v2);
        assertEquals(lt, v3);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireLessThanEqualTo(gt, lt, "int value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireLessThanEqualTo((long) gt, (long) lt, "long value"));
        final ValidationException ve3 = assertThrows(ValidationException.class, () -> Validation.requireLessThanEqualTo((double) gt, (double) lt, "double value"));
        
        assertEquals("Parameter int value with value 456 is not less than or equal to 123",        ve1.getMessage());
        assertEquals("Parameter long value with value 456 is not less than or equal to 123",       ve2.getMessage());
        assertEquals("Parameter double value with value 456.0 is not less than or equal to 123.0", ve3.getMessage());
        
        assertDoesNotThrow(() -> Validation.requireLessThanEqualTo(lt, lt, "int value"));
        assertDoesNotThrow(() -> Validation.requireLessThanEqualTo((long) lt, (long) lt, "long value"));
        assertDoesNotThrow(() -> Validation.requireLessThanEqualTo((double) lt, (double) lt, "double value"));
    }
    
    /** Test {@link Validation#requireEqualsTo(int, int, String)},
     *       {@link Validation#requireEqualsTo(long, long, String)}, and
     *       {@link Validation#requireEqualsTo(double, double, double, String)} */
    @Test
    void testEqualsTo() {
        final int x = 123;
        final int neq = 456;
        
        final int v1    = assertDoesNotThrow(() -> Validation.requireEqualsTo(x, x, "int value"));
        final long v2   = assertDoesNotThrow(() -> Validation.requireEqualsTo((long) x, (long) x, "long value"));
        final double v3 = assertDoesNotThrow(() -> Validation.requireEqualsTo(x, x, 0.0, "double value"));
        
        assertEquals(x, v1);
        assertEquals(x, v2);
        assertEquals(x, v3);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireEqualsTo(x, neq, "int value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireEqualsTo((long) x, (long) neq, "long value"));
        final ValidationException ve3 = assertThrows(ValidationException.class, () -> Validation.requireEqualsTo(x, neq, 10.0, "double value"));
        
        assertEquals("Parameter int value with value 123 is not equal to 456", ve1.getMessage());
        assertEquals("Parameter long value with value 123 is not equal to 456", ve2.getMessage());
        assertEquals("Parameter double value with value 123.0 is not equal to 456.0 within the abs tolerance 10.000000",
                     ve3.getMessage());
    }
    
    /** Test {@link Validation#requireTrue(boolean, String)} and {@link Validation#requireFalse(boolean, String)} */
    @Test
    void testRequireTrueFalseString() {
        final boolean v1 = assertDoesNotThrow(() -> Validation.requireTrue(true, "a true value"));
        final boolean v2 = assertDoesNotThrow(() -> Validation.requireFalse(false, "a false value"));
        assertTrue(v1);
        assertFalse(v2);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireTrue(false, "false value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireFalse(true, "true value"));
        assertEquals("Parameter false value is false but should be true", ve1.getMessage());
        assertEquals("Parameter true value is true but should be false", ve2.getMessage());
    }
    
    /** Test {@link Validation#requireTrue(boolean, Supplier)} and {@link Validation#requireFalse(boolean, Supplier)} */
    @Test
    void testRequireTrueFalseSupplier() {
        final boolean v1 = assertDoesNotThrow(() -> Validation.requireTrue(true, () -> "was false, but should have been true"));
        final boolean v2 = assertDoesNotThrow(() -> Validation.requireFalse(false, () -> "was true, but should have been false"));
        assertTrue(v1);
        assertFalse(v2);
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireTrue(false, () -> "was false, but should have been true"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireFalse(true, () -> "was true, but should have been false"));
        assertEquals("was false, but should have been true", ve1.getMessage());
        assertEquals("was true, but should have been false", ve2.getMessage());
    }
    
    /** Test {@link Validation#requireInt(int, IntPredicate, Supplier)},
     *       {@link Validation#requireLong(long, LongPredicate, Supplier)}, and
     *       {@link Validation#requireDouble(double, DoublePredicate, Supplier)} */
    @Test
    void testRequire() {
        final int x = 0;
        
        final IntPredicate    ipAlwaysTrue = a -> true;
        final LongPredicate   lpAlwaysTrue = a -> true;
        final DoublePredicate dpAlwaysTrue = a -> true;
        
        final int    v1 = assertDoesNotThrow(() -> Validation.requireInt(x,    ipAlwaysTrue, () -> "int value"));
        final long   v2 = assertDoesNotThrow(() -> Validation.requireLong(x,   lpAlwaysTrue, () -> "long value"));
        final double v3 = assertDoesNotThrow(() -> Validation.requireDouble(x, dpAlwaysTrue, () -> "double value"));
        
        assertEquals(x, v1);
        assertEquals(x, v2);
        assertEquals(x, v3);
        
        final IntPredicate    ipAlwaysFalse = a -> false;
        final LongPredicate   lpAlwaysFalse = a -> false;
        final DoublePredicate dpAlwaysFalse = a -> false;
        
        final ValidationException ve1 = assertThrows(ValidationException.class, () -> Validation.requireInt(x,    ipAlwaysFalse, () -> "int value"));
        final ValidationException ve2 = assertThrows(ValidationException.class, () -> Validation.requireLong(x,   lpAlwaysFalse, () -> "long value"));
        final ValidationException ve3 = assertThrows(ValidationException.class, () -> Validation.requireDouble(x, dpAlwaysFalse, () -> "double value"));
        
        assertEquals("int value", ve1.getMessage());
        assertEquals("long value", ve2.getMessage());
        assertEquals("double value", ve3.getMessage());
    }
    
    /**
     * Tests {@link Validation#requireEqualsTo(Object, Object, String)},
     * {@link Validation#requireEqualsTo(Object, Object, Supplier)},
     * {@link Validation#requireEqualsTo(int, int, Supplier)},
     * and {@link Validation#requireEqualsTo(double, double, Supplier)}
     */
    @Test
    void testRequireEqualsTo() {
        Object a = new Object();
        Object b = new Object();
        ValidationException e = assertThrows(ValidationException.class, 
                () -> Validation.requireEqualsTo(a, b, "test"));
        assertEquals("test", e.getMessage());

        assertDoesNotThrow(() -> Validation.requireEqualsTo(a, a, "test"));

        assertThrows(NullPointerException.class, 
                () -> Validation.requireEqualsTo(a, b, (Supplier<String>) null));
        e = assertThrows(ValidationException.class, 
                () -> Validation.requireEqualsTo(a, b, MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), e.getMessage());

        assertDoesNotThrow(() -> Validation.requireEqualsTo(a, a, MESSAGE_SUPPLIER));

        
        assertThrows(NullPointerException.class, 
                () -> Validation.requireEqualsTo(1, 1, (Supplier<String>) null));
        e = assertThrows(ValidationException.class, () -> Validation.requireEqualsTo(1, 2, MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), e.getMessage());

        assertDoesNotThrow(() -> Validation.requireEqualsTo(1, 1, MESSAGE_SUPPLIER));
    }
    
    /** Test {@link Validation#requireEqualsTo(double, double, String)}
     *  and  {@link Validation#requireEqualsTo(double, double, Supplier)} */
    @Test
    void testRequireEqualsToDouble() {
        final double x = FastMath.nextDown(1.5);
        final double y = 1.5;
        final double z = FastMath.nextUp(1.5);
        
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireEqualsTo(y, x, MESSAGE_SUPPLIER), "testSupplier");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireEqualsTo(y, z, MESSAGE_SUPPLIER), "testSupplier");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireEqualsTo(y, x, "y"), "Parameter y with value 1.5 is not equal to 1.4999999999999998");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireEqualsTo(y, z, "y"), "Parameter y with value 1.5 is not equal to 1.5000000000000002");
        
        assertEquals(y, Validation.requireEqualsTo(y, y, MESSAGE_SUPPLIER));
        assertEquals(y, Validation.requireEqualsTo(y, y, "y"));
    }
    
    /** Test {@link Validation#requireNearlyEqualsTo(double, double, String)}
     *  and  {@link Validation#requireNearlyEqualsTo(double, double, Supplier)} */
    @Test
    void testRequireNearlyEqualsTo() {
        final double xDown = FastMath.nextDown(FastMath.nextDown(1.5));
        final double x     = FastMath.nextDown(1.5);
        final double y     = 1.5;
        final double z     = FastMath.nextUp(1.5);
        final double zUp   = FastMath.nextUp(FastMath.nextUp(1.5));
        
        assertEquals(y, Validation.requireNearlyEqualsTo(y, x, MESSAGE_SUPPLIER));
        assertEquals(y, Validation.requireNearlyEqualsTo(y, z, MESSAGE_SUPPLIER));
        assertEquals(y, Validation.requireNearlyEqualsTo(y, x, "y"));
        assertEquals(y, Validation.requireNearlyEqualsTo(y, z, "y"));
        
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNearlyEqualsTo(y, xDown, MESSAGE_SUPPLIER), "testSupplier");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNearlyEqualsTo(y, zUp,   MESSAGE_SUPPLIER), "testSupplier");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNearlyEqualsTo(y, xDown, "y"), "Parameter y with value 1.5 is not equal to 1.4999999999999996");
        assertThrowsWithMessage(ValidationException.class, () -> Validation.requireNearlyEqualsTo(y, zUp,   "y"), "Parameter y with value 1.5 is not equal to 1.5000000000000004");
    }
    
    /**
     * Tests {@link Validation#requireGreaterThan(double, double, Supplier)}
     */
    @Test
    void testRequireGreaterThanSupp() {
        assertThrows(NullPointerException.class, 
                () -> Validation.requireGreaterThan(1.0, 2.0, (Supplier<String>) null));
        ValidationException ex = assertThrows(ValidationException.class, 
                () -> Validation.requireGreaterThan(1.0, 2.0, MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), ex.getMessage());
        assertDoesNotThrow(() -> Validation.requireGreaterThan(2.0, 1.5, MESSAGE_SUPPLIER));
    }

    /**
     * Tests {@link Validation#requireGreaterThanEqualTo(double, double, Supplier)}
     */
    @Test
    void testRequireGreaterThanEqualToSupp() {
        assertThrows(NullPointerException.class, 
                () -> Validation.requireGreaterThanEqualTo(1.0, 2.0, (Supplier<String>) null));
        ValidationException ex = assertThrows(ValidationException.class, 
                () -> Validation.requireGreaterThan(1.0, 2.0, MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), ex.getMessage());
        assertDoesNotThrow(() -> Validation.requireGreaterThanEqualTo(2.0, 2.0, MESSAGE_SUPPLIER));
    }

    /**
     * Tests {@link Validation#requireLessThanEqualTo(Comparable, Comparable, Supplier)} and
     * {@link Validation#requireLessThanEqualTo(double, double, Supplier)}
     */
    @Test
    void testRequireLessThanEqualToSupp() {
        assertThrows(NullPointerException.class, 
                () -> Validation.requireLessThanEqualTo(1.0, 2.0, (Supplier<String>) null));
        ValidationException ex = assertThrows(ValidationException.class, 
                () -> Validation.requireLessThanEqualTo(2.0, 1.0, MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), ex.getMessage());
        assertDoesNotThrow(() -> Validation.requireLessThanEqualTo(2.0, 2.0, MESSAGE_SUPPLIER));

        assertThrows(NullPointerException.class, 
                () -> Validation.requireLessThanEqualTo("aardvark", "anaconda", (Supplier<String>) null));
        ex = assertThrows(ValidationException.class, 
                () -> Validation.requireLessThanEqualTo("anaconda", "aardvark", MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), ex.getMessage());
        assertDoesNotThrow(() -> Validation.requireLessThanEqualTo("aardvark", "aardvark", MESSAGE_SUPPLIER));
    }

    /** Test {@link Validation#requireNotNullNotEmpty(Collection, String)} */
    @Test
    void testRequireNotNullNotEmptyCollection() {
        assertThrows(NullPointerException.class, () -> Validation.requireNotNullNotEmpty((List<String>) null, "test"));
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.requireNotNullNotEmpty(List.of(), "test"),
                                "test");
        
        final List<String> strings = List.of("aardvark");
        assertDoesNotThrow(() -> Validation.requireNotNullNotEmpty(strings, "test"));
    }
        
    /** Test {@link Validation#requireNotNullNotEmpty(Map, String)} */
    @Test
    void testRequireNotNullNotEmptyMap() {
        assertThrows(NullPointerException.class,
                     () -> Validation.requireNotNullNotEmpty((Map<String, Integer>) null, "test"));
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.requireNotNullNotEmpty(MapUtils.of(), "test"),
                                "test");
        
        final Map<String, Integer> map = MapUtils.of("aardvark", 1);
        assertDoesNotThrow(() -> Validation.requireNotNullNotEmpty(map, "test"));
    }
    
    /** Test {@link Validation#requireNotNullNotEmpty(Object[], String)} */
    @Test
    void testRequireNotNullNotEmptyObjArray() {
        assertThrows(NullPointerException.class, () -> Validation.requireNotNullNotEmpty((Object[]) null, "test"));
        
        assertThrowsWithMessage(ValidationException.class,
                                () -> Validation.requireNotNullNotEmpty(new Object[0], "test"),
                                "test");
        
        final Object[] objArr = new Object[1];
        assertDoesNotThrow(() -> Validation.requireNotNullNotEmpty(objArr, "test"));
    }
    
    /**
     * Tests {@link Validation#requireTrue(boolean, Supplier)}
     */
    @Test
    void testRequireTrueSupp() {
        assertThrows(NullPointerException.class, () -> Validation.requireTrue(false, (Supplier<String>) null));
        ValidationException e = assertThrows(ValidationException.class, 
                () -> Validation.requireTrue(false, MESSAGE_SUPPLIER));
        assertEquals(MESSAGE_SUPPLIER.get(), e.getMessage());
        assertDoesNotThrow(() -> Validation.requireTrue(true, MESSAGE_SUPPLIER));
    }

    /**
     * Tests {@link Validation#requireWithinRange(double, double, double, String)}
     */
    @Test
    void testRequireWithinRange() {
        ValidationException e = assertThrows(ValidationException.class, 
                () -> Validation.requireWithinRange(1.5, 2.0, 2.5, "test"));
        assertEquals("Parameter test with value 1.5 is not greater than or equal to 2.0", e.getMessage());
        e = assertThrows(ValidationException.class, 
                () -> Validation.requireWithinRange(3.0, 2.0, 2.5, "test"));
        assertEquals("Parameter test with value 3.0 is not less than or equal to 2.5", e.getMessage());
        assertDoesNotThrow(() -> Validation.requireWithinRange(2.25, 2.0, 2.5, "test"));
    }
    
    // CHECKSTYLE.ON: LineLength
}
