/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons;

import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.annotation.Annotation;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;
import org.opentest4j.AssertionFailedError;

import org.hipparchus.util.Precision;

import ptolemaeus.commons.collections.StreamUtils;

/** Test utilities
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com */
public class TestUtils {
    
    /** A RegEx to match standard newline characters */
    private static final String NEWLINE_REGEX = "(\r\n|\n)";
    
    /** Assert that the {@link Executable} causes the expected {@link Throwable} type to be thrown, and check that the
     * {@link Throwable} itself has the expected message.<br><br>
     * 
     * This is particularly useful when the exception message is a formatted string
     * 
     * @param expectedType the expected {@link Throwable} type
     * @param executable the {@link Executable} that we expect to throw
     * @param expectedMessage the expected {@link Throwable#getMessage()} */
    public static void assertThrowsWithMessage(final Class<? extends Throwable> expectedType,
                                               final Executable executable,
                                               final String expectedMessage) {
        final Throwable t = assertThrows(expectedType, executable);
        assertEquals(expectedMessage, t.getMessage());
    }
    
    /** assert that the provided {@link Iterable} is sorted according to the {@link Comparator#naturalOrder()} of
     * {@code E}s
     * 
     * @param <E> the element type of the {@link Iterable}
     * @param iterable the {@link Iterable} for which we want to test sorted-ness */
    public static <E extends Comparable<E>> void assertSorted(final Iterable<E> iterable) {
        assertSorted(iterable, Comparator.naturalOrder());
    }
    
    /** assert that the provided {@link Iterable} is sorted according to the provided {@link Comparator}
     * 
     * @param <E> the element type of the {@link Iterable}
     * @param iterable the {@link Iterable} for which we want to test sorted-ness
     * @param comparator the {@link Comparator} to compare {@code E}s */
    public static <E> void assertSorted(final Iterable<E> iterable, final Comparator<E> comparator) {
        E prev = null;
        for (E e : iterable) {
            if (prev != null) {
                assertTrue(comparator.compare(e, prev) > 0);
            }
            prev = e;
        }
    }
    
    /** Assert that two arrays - possibly with nested arrays - are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(Object[], Object[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final Object[] array1, final Object[] array2) {
        if (!Arrays.deepEquals(array1, array2)) {
            assertEquals(Arrays.deepToString(array1), Arrays.deepToString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(byte[], byte[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final byte[] array1, final byte[] array2) {
        if (!Arrays.equals(array1, array2)) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(short[], short[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final short[] array1, final short[] array2) {
        if (!Arrays.equals(array1, array2)) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of {@link Assertions#assertArrayEquals(int[], int[])}
     * for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final int[] array1, final int[] array2) {
        if (!Arrays.equals(array1, array2)) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(long[], long[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final long[] array1, final long[] array2) {
        if (!Arrays.equals(array1, array2)) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(float[], float[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array
     * @param epsilon the equality tolerance to apply when comparing each element of the arrays */
    public static void assertArrayEquality(final float[] array1, final float[] array2, final float epsilon) {
        boolean answer = array1.length == array2.length;
        for (int i = 0; answer && i < array1.length; i++) {
            answer &= Precision.equals(array1[i], array2[i], epsilon);
        }
        
        if (!answer) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(double[], double[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array
     * @param epsilon the equality tolerance to apply when comparing each element of the arrays */
    public static void assertArrayEquality(final double[] array1, final double[] array2, final double epsilon) {
        boolean answer = array1.length == array2.length;
        for (int i = 0; answer && i < array1.length; i++) {
            answer &= Precision.equals(array1[i], array2[i], epsilon);
        }
        
        if (!answer) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(char[], char[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final char[] array1, final char[] array2) {
        if (!Arrays.equals(array1, array2)) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that two arrays are equal. Should be used in place of
     * {@link Assertions#assertArrayEquals(boolean[], boolean[])} for a better JUnit message.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final boolean[] array1, final boolean[] array2) {
        if (!Arrays.equals(array1, array2)) {
            assertEquals(Arrays.toString(array1), Arrays.toString(array2), "the arrays were not equal");
        }
    }

    /** Assert that two 2-D {@code double[][]} arrays are equal.
     * 
     * @param array1 the first array
     * @param array2 the second array */
    public static void assertArrayEquality(final double[][] array1, final double[][] array2) {
        if (!Arrays.deepEquals(array1, array2)) {
            assertEquals(Arrays.deepToString(array1), Arrays.deepToString(array2), "the arrays were not equal");
        }
    }
    
    /** Assert that {@code x} is strictly less than {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be smaller than {@code y}
     * @param y the number that must be larger  than {@code x}
     * @param name the name of the value that we're checking */
    public static <C extends Comparable<C>> void assertLessThan(final C x, final C y, final String name) {
        assertLessThan(x, y, () -> "%s has value %s, which is >= %s".formatted(name, x, y));
    }
    
    /** Assert that {@code x} is strictly greater than {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be larger  than {@code y}
     * @param y the number that must be smaller than {@code x}
     * @param name the name of the value that we're checking */
    public static <C extends Comparable<C>> void assertGreaterThan(final C x, final C y, final String name) {
        assertGreaterThan(x, y, () -> "%s has value %s, which is <= %s".formatted(name, x, y));
    }
    
    /** Assert that {@code x} is strictly less than {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be smaller than {@code y}
     * @param y the number that must be larger  than {@code x}
     * @param msgSupp the failure message supplier */
    public static <C extends Comparable<C>> void assertLessThan(final C x, final C y,
                                                                final Supplier<String> msgSupp) {
        if (x.compareTo(y) >= 0) {
            fail(msgSupp.get());
        }
    }
    
    /** Assert that {@code x} is strictly greater than {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be larger  than {@code y}
     * @param y the number that must be smaller than {@code x}
     * @param msgSupp the failure message supplier */
    public static <C extends Comparable<C>> void assertGreaterThan(final C x, final C y,
                                                                   final Supplier<String> msgSupp) {
        if (x.compareTo(y) <= 0) {
            fail(msgSupp.get());
        }
    }
    
    /** Assert that {@code x} is less than or equal to {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be smaller than or equal to {@code y}
     * @param y the number that must be larger  than or equal to {@code x}
     * @param name the name of the value that we're checking */
    public static <C extends Comparable<C>> void assertLessThanOrEqualTo(final C x, final C y, final String name) {
        assertLessThanOrEqualTo(x, y, () -> "%s has value %s, which is > %s".formatted(name, x, y));
    }
    
    /** Assert that {@code x} is greater than or equal to {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be larger  than or equal to {@code y} 
     * @param y the number that must be smaller than or equal to {@code x} 
     * @param name the name of the value that we're checking */
    public static <C extends Comparable<C>> void assertGreaterThanOrEqualTo(final C x, final C y, final String name) {
        assertGreaterThanOrEqualTo(x, y, () -> "%s has value %s, which is < %s".formatted(name, x, y));
    }
    
    /** Assert that {@code x} is less than or equal to {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be smaller than or equal to {@code y}
     * @param y the number that must be larger  than or equal to {@code x}
     * @param msgSupp the failure message supplier */
    public static <C extends Comparable<C>> void assertLessThanOrEqualTo(final C x, final C y,
                                                                         final Supplier<String> msgSupp) {
        if (x.compareTo(y) > 0) {
            fail(msgSupp.get());
        }
    }
    
    /** Assert that {@code x} is greater than or equal to {@code y}
     * 
     * @param <C> the {@link Comparable} type we're comparing
     * @param x the number that must be larger  than or equal to {@code y}
     * @param y the number that must be smaller than or equal to {@code x}
     * @param msgSupp the failure message supplier */
    public static <C extends Comparable<C>> void assertGreaterThanOrEqualTo(final C x, final C y,
                                                                            final Supplier<String> msgSupp) {
        if (x.compareTo(y) < 0) {
            fail(msgSupp.get());
        }
    }
    
    /**
     * Test a static utility constructor is private and does not raise exceptions
     * @param clazz class type to test
     * @param <T> type of class to test
     */
    public static <T> void testConstructorIsPrivateWithoutRaisingExceptions(final Class<T> clazz) {
        try {
            Constructor<T> constructor = clazz.getDeclaredConstructor();
            assertTrue(Modifier.isPrivate(constructor.getModifiers()));
            constructor.setAccessible(true);
            constructor.newInstance();
        }
        catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException ex) {
            fail(ex);
        }
    }

    /**
     * Tests enumerations superficially to increase code coverage metrics
     * @param enumClass the class to test
     */
    public static void superficialEnumCodeCoverage(final Class<? extends Enum<?>> enumClass) {
        try {
            for (Object o : (Object[]) enumClass.getMethod("values").invoke(null)) {
                enumClass.getMethod("valueOf", String.class).invoke(null, o.toString());
            }
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    
    /** Delete all carriage returns {@code \r}
     * 
     * @param string the String from which we want to delete all carriage returns
     * @return the given String, but without carriage returns */
    public static String deleteCarriageReturns(final String string) {
        return string.replace("\r", "");
    }
    
    /** Use this to test {@code toString} methods which return Strings with more than one line. This will print out a
     * copy-paste-able code String which will reproduce the value of {@code actualObject::toString}.
     * 
     * @param expected the expected String representation of the provided {@link Object}
     * @param actualObject the actual {@link Object} whose {@code ::toString} method we are testing (may be null) */
    public static void testMultiLineToString(final String expected, final Object actualObject) {
        final String actual    = deleteCarriageReturns(Objects.toString(actualObject));
        final String expected1 = deleteCarriageReturns(expected);
        final AtomicReference<String> atomicTestName = new AtomicReference<>();
        try {
            assertEquals(expected1, actual, () -> {
                try {
                    final String testName =
                            determineCallingTestMethod().orElse("Test name not available.  How did you do that??");
                    atomicTestName.set(testName);
                }
                catch (SecurityException e) {
                    throw e;
                }
                
                return String.format("Failing Test: %s", atomicTestName.get());
            });
        }
        catch (AssertionFailedError ex) {
            final String testName   = atomicTestName.get(); // if the test failed, this will always be non-null
            final String codeString = getCodeString(actual).replace("String string =", "String expected =");
            final String testCall   = String.format("TestUtils.testMultiLineToString(expected, actual);");
            
            final String format = """
                    
                    The following output is for the failing test:
                    %s
                    %s
                    %s
                    
                    """;
            synchronized (System.out) { // prevents stdOut interrupting stdErr Strings
                System.err.format(format, testName, codeString, testCall); // System.err calls are synchronized on err
            }
            throw ex;
        }
    }
    
    /** @return the fully-qualified name of the testing method (one annotated with {@link org.junit.jupiter.api.Test})
     *         calling <em>this</em> method wrapped in an {@link Optional}. If not found, we return an empty
     *         {@link Optional} */
    public static Optional<String> determineCallingTestMethod() {
        final StackTraceElement[] st = Thread.currentThread().getStackTrace();
        for (StackTraceElement ste : st) {
            final String steClassName = ste.getClassName();
            final String steMethodName = ste.getMethodName();
            final String annotationName = org.junit.jupiter.api.Test.class.getCanonicalName();
            
            final Optional<Method> possiblyTestMethod;
            try {
                possiblyTestMethod = findMethod(steClassName, steMethodName, annotationName);
            }
            catch (ClassNotFoundException e) { // a CNFE is pretty serious, so just wrap and rethrow
                throw new RuntimeException(e);
            }
            
            if (possiblyTestMethod.isPresent()) {
                return Optional.of(String.format("%s::%s", steClassName, steMethodName));
            }
        }
        
        return Optional.empty();
    }
    
    /** Get code string that will reproduce the provided multi-line String.
     * 
     * @param original the String for which we want code
     * @return the code string */
    public static String getCodeString(final String original) {
        final boolean containsSingleQuotes = original.contains("'");
        final String[] split;
        if (containsSingleQuotes) {
            split = original.replace("\"", "\\\"").split(NEWLINE_REGEX);
        }
        else {
            split = original.replace('\"', '\'').split(NEWLINE_REGEX);
        }
        
        final boolean turnOffCheckstyle = Arrays.stream(split).map(String::length).anyMatch(length -> length > 97);
        
        final StringBuilder sb = new StringBuilder();
        if (turnOffCheckstyle) {
            sb.append("// CHECKSTYLE.OFF: LineLength");
            sb.append(System.lineSeparator());
        }
        
        sb.append("final String string = ");
        
        final String wrappedStringIndent;
        if (split.length != 1) {
            sb.append("String.join(System.lineSeparator(),").append(System.lineSeparator());
            wrappedStringIndent = "        "; // 8 spaces
        }
        else {
            wrappedStringIndent = "";
        }
        
        final boolean addEndingNewLine = original.endsWith(System.lineSeparator());
        for (int i = 0; i < split.length; i++) {
            sb.append(String.format("%s\"%s\"", wrappedStringIndent, split[i]));
            
            if (i != split.length - 1) {
                sb.append(String.format(",%n"));
            }
            else if (addEndingNewLine) {
                sb.append(String.format(",%n%s\"\"", wrappedStringIndent));
            }
        }
        
        if (split.length != 1) {
            sb.append(')');
        }
        
        if (original.contains("\"") && !containsSingleQuotes) {
            sb.append(".replace('\\'', '\\\"')");
        }
        sb.append(';');
        
        if (turnOffCheckstyle) {
            sb.append(System.lineSeparator());
            sb.append("// CHECKSTYLE.ON: LineLength");
        }
        
        return sb.toString();
    }
    
    /** Given a set of one {@link Class} name, one method {@link Method}, and any number of {@link Annotation} names
     * (which have {@link RetentionPolicy#RUNTIME the RUNTIME RetentionPolicy}), find a {@link Method} (may have any
     * accessibility) of the the given {@link Class} which has at least each of the given {@link Annotation}s. If there
     * is more than one due to overloading (or under specification of annotations), we return the first found. No
     * guarantees are made about the parameter types of the method.
     * 
     * @param className the fully-qualified {@link Class} name. May not be null.
     * @param methodName the {@link Method} name (if overloaded, we return the first that we find). May not be null.
     * @param annoClassNames the fully-qualified {@link Annotation} type names. May be empty.
     * @return if no class-method-annotation set matches the query, then we return an empty {@link Optional}, else we
     *         return the {@link Method} instance wrapped in an {@link Optional}. If there is more than match one due to
     *         overloading, we return the first found. No guarantees are made about the parameter types of the method.
     * @throws ClassNotFoundException if either the {@link Class} or the {@link Annotation} {@link Class} cannot be
     *         found
     * @implNote if not yet loaded, the {@link Class} and any {@link Annotation} {@link Class}es listed will be
     *           loaded */
    @SuppressWarnings("unchecked")
    public static Optional<Method> findMethod(final String className,
                                              final String methodName,
                                              final String... annoClassNames) throws ClassNotFoundException {
        requireNonNull(className, "className may not be null");
        requireNonNull(methodName, "methodName may not be null");
        requireNonNull(annoClassNames, "annoClassNames may not be null");
        
        final Class<?> clazz = Class.forName(className);
        Stream<Method> methodStream = StreamUtils.concat(Stream.of(clazz.getDeclaredMethods()),
                                                         Stream.of(clazz.getMethods()))
                                                 .filter(m -> m.getName().equals(methodName));
        
        for (final String annoClassName : annoClassNames) {
            requireNonNull(annoClassName, "elements of the annotation array must not be null");
            final Class<? extends Annotation> annotationClass =
                    (Class<? extends Annotation>) Class.forName(annoClassName);
            methodStream = methodStream.filter(m -> m.isAnnotationPresent(annotationClass));
        }
        
        return methodStream.findFirst();
    }
}
