/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

/**
 * Tests {@link IStreamable}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class IStreamableTest {
    
    /** a list for testing */
    private static final List<Integer> LIST = List.of(123, 234, 345, 456, 567, 678, 890);
    
    /**
     * Test all of {@link IStreamable}
     */
    @Test
    void testAll() {
        final List<Integer> recollected = getExample().stream().collect(Collectors.toList());
        assertEquals(LIST, recollected);
        assertFalse(getExample().stream().isParallel());
        assertTrue(getExample().parallelStream().isParallel());
        
        final NavigableSet<Integer> recollectedNSet = getExample().stream()
                                                                  .collect(Collectors.toCollection(TreeSet::new));
        
        assertEquals(LIST, List.copyOf(recollectedNSet));
    }
    
    /** @return an example {@link IStreamable} */
    private static IStreamable<Integer> getExample() {
        return new IStreamable<>() {
            
            @Override
            public Stream<Integer> stream() {
                return LIST.stream();
            }
        };
    }
}
