/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ptolemaeus.commons.TestUtils.testMultiLineToString;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

/**
 * Tests {@link IPartitionable}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class IPartitionableTest {
    
    /** a list for testing */
    private static final List<Integer> LIST = List.of(123, 234, 345, 456, 567, 678, 890);
    
    /**
     * Test all of {@link IPartitionable}
     */
    @Test
    void testAll() {
        final IPartitionable<Integer> partable = getExample();
        
        assertEquals(LIST, partable.parts());
        
        int index = 0;
        for (final Integer partition : partable) { // tests ::iterator
            assertEquals(LIST.get(index++), partition);
        }
        
        assertEquals(LIST, partable.stream().collect(Collectors.toList()));
        
        final StringBuilder builder = new StringBuilder();
        partable.appendToStringBuilder(builder, 4);
        final String expected = String.join(System.lineSeparator(),
                                            "                123",
                                            "                234",
                                            "                345",
                                            "                456",
                                            "                567",
                                            "                678",
                                            "                890",
                                            "");
        testMultiLineToString(expected, builder.toString());
    }
    
    /** @return an example {@link IPartitionable} */
    private static IPartitionable<Integer> getExample() {
        return new IPartitionable<>() {

            @Override
            public Iterable<Integer> parts() {
                return LIST;
            }
        };
    }
}
