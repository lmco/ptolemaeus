/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.function.IntFunction;

import org.junit.jupiter.api.Test;

/**
 * Test for {@link UnmodifiableLinkedList}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class UnmodifiableLinkedListTest {
    
    /**
     * Test to show that all methods inherited by {@link UnmodifiableLinkedList} which would modify the list throw an
     * {@link UnsupportedOperationException}
     */
    @Test
    public void testUnsupportedMethodsThrow() {
        UnmodifiableLinkedList<Object> ull = UnmodifiableLinkedList.copyOf(List.of());
        assertThrows(UnsupportedOperationException.class, () -> ull.add(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.add(0, new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.addAll(List.of()));
        assertThrows(UnsupportedOperationException.class, () -> ull.addAll(0, List.of()));
        assertThrows(UnsupportedOperationException.class, () -> ull.addFirst(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.addLast(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.clear());
        assertThrows(UnsupportedOperationException.class, () -> ull.offer(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.offerFirst(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.offerLast(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.poll());
        assertThrows(UnsupportedOperationException.class, () -> ull.pollFirst());
        assertThrows(UnsupportedOperationException.class, () -> ull.pollLast());
        assertThrows(UnsupportedOperationException.class, () -> ull.pop());
        assertThrows(UnsupportedOperationException.class, () -> ull.push(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.remove());
        assertThrows(UnsupportedOperationException.class, () -> ull.remove(0));
        assertThrows(UnsupportedOperationException.class, () -> ull.remove(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.removeAll(List.of()));
        assertThrows(UnsupportedOperationException.class, () -> ull.removeFirst());
        assertThrows(UnsupportedOperationException.class, () -> ull.removeFirstOccurrence(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.removeIf(null));
        assertThrows(UnsupportedOperationException.class, () -> ull.removeLast());
        assertThrows(UnsupportedOperationException.class, () -> ull.removeLastOccurrence(new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.replaceAll(null));
        assertThrows(UnsupportedOperationException.class, () -> ull.retainAll(List.of()));
        assertThrows(UnsupportedOperationException.class, () -> ull.set(0, new Object()));
        assertThrows(UnsupportedOperationException.class, () -> ull.sort(null));
    }
    
    /** Test {@link UnmodifiableLinkedList#of(Object...)} */
    @Test
    void testOf() {
        final UnmodifiableLinkedList<Integer> ull = UnmodifiableLinkedList.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        assertEquals(1, ull.getFirst());
        assertEquals(10, ull.getLast());
    }
    
    /**
     * <pre>
     * Tests
     * {@link UnmodifiableLinkedList#element()},
     * {@link UnmodifiableLinkedList#get(int)},
     * {@link UnmodifiableLinkedList#getFirst()},
     * {@link UnmodifiableLinkedList#getLast()},
     * {@link UnmodifiableLinkedList#peek()},
     * {@link UnmodifiableLinkedList#peekFirst()}, and
     * {@link UnmodifiableLinkedList#peekLast()}
     * </pre>
     */
    @Test
    public void testGets() {
        UnmodifiableLinkedList<Integer> ull = UnmodifiableLinkedList.copyOf(List.of(10, 11, 12, 13, 14, 15, 16, 17));
        assertEquals(10, ull.element());
        assertEquals(11, ull.get(1));
        assertEquals(10, ull.getFirst());
        assertEquals(17, ull.getLast());
        assertEquals(10, ull.peek());
        assertEquals(10, ull.peekFirst());
        assertEquals(17, ull.peekLast());
        
        for (int i = 0; i < 7; i++) {
            assertEquals(10 + i, ull.get(i));
        }
    }
    
    /**
     * <pre>
     * Tests
     * {@link UnmodifiableLinkedList#contains(Object)},
     * {@link UnmodifiableLinkedList#containsAll(Collection)},
     * {@link UnmodifiableLinkedList#indexOf(Object)},
     * {@link UnmodifiableLinkedList#isEmpty()},
     * {@link UnmodifiableLinkedList#lastIndexOf(Object)}, and
     * {@link UnmodifiableLinkedList#size()}
     * </pre>
     */
    @Test
    public void testInformationMethods() {
        UnmodifiableLinkedList<Integer> ull = UnmodifiableLinkedList.copyOf(List.of(10, 11, 12, 13, 14, 15, 16, 10));
        assertTrue(ull.contains(13));
        assertFalse(ull.contains(12345));
        assertTrue(ull.containsAll(List.of(10, 12, 13)));
        assertFalse(ull.containsAll(List.of(10, 12, 12345)));
        assertEquals(6, ull.indexOf(16));
        assertFalse(ull.isEmpty());
        assertTrue(UnmodifiableLinkedList.copyOf(List.of()).isEmpty());
        assertEquals(7, ull.lastIndexOf(10));
        assertEquals(8, ull.size());
    }
    
    /**
     * <pre>
     * Tests
     * {@link UnmodifiableLinkedList#descendingIterator()},
     * {@link UnmodifiableLinkedList#iterator()},
     * {@link UnmodifiableLinkedList#listIterator()},
     * {@link UnmodifiableLinkedList#listIterator(int)},
     * {@link UnmodifiableLinkedList#subList(int, int)},
     * {@link UnmodifiableLinkedList#toArray()},
     * {@link UnmodifiableLinkedList#toArray(IntFunction)}, and
     * {@link UnmodifiableLinkedList#toArray(Object[])}
     * </pre>
     */
    @Test
    public void testGetCollectionMethods() {
        UnmodifiableLinkedList<Integer> ull = UnmodifiableLinkedList.copyOf(List.of(10, 11, 12, 13, 14, 15, 16, 17));
        Iterator<Integer> descIter = ull.descendingIterator();
        assertEquals(17, descIter.next());
        assertEquals(16, descIter.next());
        descIter.next();
        descIter.next();
        descIter.next();
        descIter.next();
        descIter.next();
        assertEquals(10, descIter.next());
        assertFalse(descIter.hasNext());
        assertThrows(NoSuchElementException.class, () -> descIter.next());
        
        Iterator<Integer> iter = ull.iterator();
        assertEquals(10, iter.next());
        assertEquals(11, iter.next());
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        assertEquals(17, iter.next());
        assertFalse(iter.hasNext());
        assertThrows(NoSuchElementException.class, () -> iter.next());
        
        ListIterator<Integer> listITr1 = ull.listIterator();
        assertFalse(listITr1.hasPrevious());
        assertThrows(NoSuchElementException.class, () -> listITr1.previous());
        assertEquals(10, listITr1.next());
        assertEquals(11, listITr1.next());
        listITr1.next();
        listITr1.next();
        listITr1.next();
        listITr1.next();
        listITr1.next();
        assertEquals(17, listITr1.next());
        assertEquals(17, listITr1.previous());
        assertEquals(17, listITr1.next());
        assertEquals(17, listITr1.previous());
        assertEquals(16, listITr1.previous());
        listITr1.next();
        assertEquals(6, listITr1.previousIndex());
        assertEquals(7, listITr1.nextIndex());
        listITr1.next();
        assertEquals(ull.size(), listITr1.nextIndex());
        assertFalse(listITr1.hasNext());
        assertThrows(NoSuchElementException.class, () -> listITr1.next());
        assertThrows(UnsupportedOperationException.class, () -> listITr1.remove());
        assertThrows(UnsupportedOperationException.class, () -> listITr1.set(9));
        assertThrows(UnsupportedOperationException.class, () -> listITr1.add(8));
        
        ListIterator<Integer> listITr2 = ull.listIterator(2);
        assertTrue(listITr2.hasPrevious()); // Starts at index 2, but can get to 0!
        assertEquals(12, listITr2.next());
        assertEquals(13, listITr2.next());
        listITr2.next();
        listITr2.next();
        listITr2.next();
        assertEquals(17, listITr2.next());
        assertEquals(17, listITr2.previous());
        assertEquals(17, listITr2.next());
        assertEquals(17, listITr2.previous());
        assertEquals(16, listITr2.previous());
        listITr2.next();
        listITr2.next();
        assertFalse(listITr2.hasNext());
        assertThrows(NoSuchElementException.class, () -> listITr2.next());
        
        assertEquals(List.of(13, 14, 15), ull.subList(3, 6));
        
        assertArrayEquals(new Object[] {10, 11, 12, 13, 14, 15, 16, 17}, ull.toArray());
        assertArrayEquals(new Integer[] {10, 11, 12, 13, 14, 15, 16, 17}, ull.toArray(new Integer[] {}));
        
        assertEquals(16448, ull.spliterator().characteristics());
    }
    
    /**
     * <pre>
     * Tests
     * {@link UnmodifiableLinkedList#equals(Object)},
     * {@link UnmodifiableLinkedList#hashCode()}, and
     * {@link UnmodifiableLinkedList#toString()}
     * </pre>
     */
    @Test
    public void testEqualsHashToString() {
        UnmodifiableLinkedList<Integer> ull1 = UnmodifiableLinkedList.copyOf(List.of(10, 11, 12, 13, 14, 15, 16, 17));
        UnmodifiableLinkedList<Integer> ull2 = UnmodifiableLinkedList.copyOf(List.of(10, 11, 12, 13, 14, 15, 16, 17));
        UnmodifiableLinkedList<Integer> ull3 = UnmodifiableLinkedList.copyOf(List.of(10, 11, 12, 13, 14, 15, 16));
        UnmodifiableLinkedList<Integer> ull4 = UnmodifiableLinkedList.copyOf(List.of());
        UnmodifiableLinkedList<Integer> ull5 = null;
        UnmodifiableLinkedList<Integer> view = UnmodifiableLinkedList.viewOf(
                new LinkedList<>(List.of(10, 11, 12, 13, 14, 15, 16, 17)));
        assertEquals(ull1, view);
        
        assertSame(ull1, ull1);
        assertEquals(ull1, ull2);
        assertNotEquals(ull1, ull3);
        assertNotEquals(ull1, ull4);
        assertNotEquals(ull1, ull5);
        assertEquals("[10, 11, 12, 13, 14, 15, 16, 17]", ull1.toString());
        assertEquals(-30626780, ull1.hashCode());
    }
}
