/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ptolemaeus.commons.TestUtils.assertArrayEquality;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.Parallelism;

/**
 * Test {@link StreamUtils}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class StreamUtilsTest {
    
    /** Test {@link StreamUtils#toLinkedList()} */
    @Test
    void testToLinkedList() {
        final List<Integer> l = List.of(1, 2, 3, 4, 5, 6);
        
        final LinkedList<Integer> ll = l.stream().collect(StreamUtils.toLinkedList());
        assertEquals(l, ll);
    }
    
    /** Test {@link StreamUtils#toArrayDeque()} */
    @Test
    void testToArrayDeque() {
        final List<Integer> l = List.of(1, 2, 3, 4, 5, 6);
        
        final ArrayDeque<Integer> ll = l.stream().collect(StreamUtils.toArrayDeque());
        assertArrayEquality(new ArrayDeque<>(l).toArray(), ll.toArray());
    }
    
    /** Test {@link StreamUtils#toUnmodifiableMap(Function, Function)} */
    @Test
    void testToUnmodifiableMap1() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Collector<Integer, ?, Map<Double, Integer>> collector = StreamUtils.toUnmodifiableMap(i -> i / 10.0,
                                                                                                    i -> i * 10);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(0.1,  10,
                                                          0.2,  20,
                                                          0.3,  30,
                                                          0.4,  40,
                                                          0.5,  50,
                                                          0.6,  60,
                                                          0.7,  70,
                                                          0.8,  80,
                                                          0.9,  90,
                                                          1.0, 100);
        assertMapsEqualWithOrdering(expected, actual, null);
        
        assertThrows(UnsupportedOperationException.class, () -> actual.put(0.0, 1));
    }
    
    /** Test {@link StreamUtils#toUnmodifiableMap(Function, Function, BinaryOperator)} */
    @Test
    void testToUnmodifiableMap2() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Collector<Integer, ?, Map<Double, Integer>> collector = StreamUtils.toUnmodifiableMap(i -> i / 10.0,
                                                                                                    i -> i * 10,
                                                                                                   (d1, d2) -> d1 + d2);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(0.1,  10,
                                                          0.2,  20,
                                                          0.3,  30,
                                                          0.4,  40,
                                                          0.5,  50,
                                                          0.6,  60,
                                                          0.7,  70,
                                                          0.8,  80,
                                                          0.9,  90,
                                                          1.0, 100);
        assertMapsEqualWithOrdering(expected, actual, null);
        
        assertThrows(UnsupportedOperationException.class, () -> actual.put(0.0, 1));
    }
    
    /** Test {@link StreamUtils#toUnmodifiableSet()} */
    @Test
    void testToUnmodifiableSet() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        
        final Set<Integer> expected1 = SetUtils.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Set<Integer> expected2 = SetUtils.of(1, 2, 3, 4);
        
        final Set<Integer> actual1 = doesNotHaveDuplicates.stream().collect(StreamUtils.toUnmodifiableSet());
        final Set<Integer> actual2 = hasDuplicates.stream().collect(StreamUtils.toUnmodifiableSet());
        
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        
        assertThrows(UnsupportedOperationException.class, () -> actual1.add(123));
        assertThrows(UnsupportedOperationException.class, () -> actual2.add(123));
    }
    
    /**
     * Test {@link StreamUtils#toMap(Function, Function)}
     */
    @Test
    void testToMap() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Collector<Integer, ?, Map<Double, Integer>> collector = StreamUtils.toMap(i -> i / 10.0,
                                                                                        i -> i * 10);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(0.1,  10,
                                                          0.2,  20,
                                                          0.3,  30,
                                                          0.4,  40,
                                                          0.5,  50,
                                                          0.6,  60,
                                                          0.7,  70,
                                                          0.8,  80,
                                                          0.9,  90,
                                                          1.0, 100);
        assertMapsEqualWithOrdering(expected, actual, LinkedHashMap.class);
        
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        assertThrows(IllegalStateException.class, () -> hasDuplicates.stream().collect(collector));
    }
    
    /**
     * Test {@link StreamUtils#toMap(Function, Function, BinaryOperator)}
     */
    @Test
    void testToMap2() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Collector<Integer, ?, Map<Double, Integer>> collector = StreamUtils.toMap(i -> i / 10.0,
                                                                                        i -> i * 10,
                                                                                        (d1, d2) -> d1 + d2);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(0.1,  10,
                                                          0.2,  20,
                                                          0.3,  30,
                                                          0.4,  40,
                                                          0.5,  50,
                                                          0.6,  60,
                                                          0.7,  70,
                                                          0.8,  80,
                                                          0.9,  90,
                                                          1.0, 100);
        assertMapsEqualWithOrdering(expected, actual, LinkedHashMap.class);
        
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        final Map<Double, Integer> actual2 = hasDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected2 = MapUtils.of(0.1,  40,
                                                           0.2,  60,
                                                           0.3,  60,
                                                           0.4,  40);
        assertMapsEqualWithOrdering(expected2, actual2, LinkedHashMap.class);
    }
    
    /**
     * Test {@link StreamUtils#toMap(Function, Function, BinaryOperator, Supplier)}
     */
    @Test
    void testToMap3() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Supplier<Map<Double, Integer>> mapSupplier = () -> new TreeMap<>(Comparator.reverseOrder());
        final Collector<Integer, ?, Map<Double, Integer>> collector = StreamUtils.toMap(
                                                                        i -> i / 10.0,
                                                                        i -> i * 10,
                                                                        (d1, d2) -> d1 + d2,
                                                                        mapSupplier);
        
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(1.0, 100,
                                                          0.9,  90,
                                                          0.8,  80,
                                                          0.7,  70,
                                                          0.6,  60,
                                                          0.5,  50,
                                                          0.4,  40,
                                                          0.3,  30,
                                                          0.2,  20,
                                                          0.1,  10);
        assertMapsEqualWithOrdering(expected, actual, TreeMap.class);
        
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        final Map<Double, Integer> actual2 = hasDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected2 = MapUtils.of(0.4,  40,
                                                           0.3,  60,
                                                           0.2,  60,
                                                           0.1,  40);
        assertMapsEqualWithOrdering(expected2, actual2, TreeMap.class);
    }
    
    /**
     * Test {@link StreamUtils#toMap(Function, Function, Supplier)}
     */
    @Test
    void testToMap4() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Supplier<Map<Double, Integer>> mapSupplier = () -> new TreeMap<>(Comparator.reverseOrder());
        final Collector<Integer, ?, Map<Double, Integer>> collector = StreamUtils.toMap(i -> i / 10.0,
                                                                                        i -> i * 10,
                                                                                        mapSupplier);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(1.0, 100,
                                                          0.9,  90,
                                                          0.8,  80,
                                                          0.7,  70,
                                                          0.6,  60,
                                                          0.5,  50,
                                                          0.4,  40,
                                                          0.3,  30,
                                                          0.2,  20,
                                                          0.1,  10);
        assertMapsEqualWithOrdering(expected, actual, TreeMap.class);
        
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        assertThrows(IllegalStateException.class, () -> hasDuplicates.stream().collect(collector));
    }
    
    /**
     * Test {@link StreamUtils#toMapOverwrite(Function, Function)}
     */
    @Test
    void testToMapOverwriteNoSupplier() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Collector<Integer, ?, Map<Double, Integer>> collector =
                StreamUtils.toMapOverwrite(i -> i / 10.0,
                                           i -> i * 10);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(0.1,  10,
                                                          0.2,  20,
                                                          0.3,  30,
                                                          0.4,  40,
                                                          0.5,  50,
                                                          0.6,  60,
                                                          0.7,  70,
                                                          0.8,  80,
                                                          0.9,  90,
                                                          1.0, 100);
        assertMapsEqualWithOrdering(expected, actual, LinkedHashMap.class);
        
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        final Map<Double, Integer> actual2 = hasDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected2 = MapUtils.of(0.1,  10,
                                                           0.2,  20,
                                                           0.3,  30,
                                                           0.4,  40);
        assertMapsEqualWithOrdering(expected2, actual2, LinkedHashMap.class);
    }
    
    /** Test {@link StreamUtils#toMapOverwrite(Function, Function)} */
    @Test
    void testToMapOverwriteWithSupplier() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Collector<Integer, ?, Map<Double, Integer>> collector =
                StreamUtils.toMapOverwrite(i -> i / 10.0,
                                           i -> i * 10,
                                           TreeMap::new);
        final Map<Double, Integer> actual = doesNotHaveDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected = MapUtils.of(0.1,  10,
                                                          0.2,  20,
                                                          0.3,  30,
                                                          0.4,  40,
                                                          0.5,  50,
                                                          0.6,  60,
                                                          0.7,  70,
                                                          0.8,  80,
                                                          0.9,  90,
                                                          1.0, 100);
        assertMapsEqualWithOrdering(expected, actual, TreeMap.class);
        
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        final Map<Double, Integer> actual2 = hasDuplicates.stream().collect(collector);
        final Map<Double, Integer> expected2 = MapUtils.of(0.1,  10,
                                                           0.2,  20,
                                                           0.3,  30,
                                                           0.4,  40);
        assertMapsEqualWithOrdering(expected2, actual2, TreeMap.class);
    }
    
    /**
     * Test {@link StreamUtils#toSet()}
     */
    @Test
    void testToSet() {
        final List<Integer> doesNotHaveDuplicates = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final List<Integer> hasDuplicates = List.of(1, 1, 1, 1, 2, 2, 2, 3, 3, 4);
        
        final Set<Integer> expected1 = SetUtils.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Set<Integer> expected2 = SetUtils.of(1, 2, 3, 4);
        
        final Set<Integer> actual1 = doesNotHaveDuplicates.stream().collect(StreamUtils.toSet());
        final Set<Integer> actual2 = hasDuplicates.stream().collect(StreamUtils.toSet());
        
        assertEquals(LinkedHashSet.class, actual1.getClass());
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
    }
    
    /**
     * Test {@link StreamUtils#concat(Stream...)}
     */
    @Test
    void testConcat() {
        final Stream<Integer> s1 = List.of(0, 1, 2, 3, 4).stream();
        final Stream<Integer> s2 = List.of(5, 6, 7, 8, 9).stream();
        final Stream<Integer> s3 = List.of(10, 11, 12, 13, 14).stream();
        final Stream<Integer> s4 = List.of(15, 16, 17, 18, 19).stream();
        
        final Stream<Integer> s1234 = StreamUtils.concat(s1, s2, s3, s4, Stream.of(), Stream.of());
        
        assertEquals(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                             10, 11, 12, 13, 14, 15, 16, 17, 18, 19),
                     s1234.collect(Collectors.toList()));
        
        assertEquals(List.of(), StreamUtils.concat().collect(Collectors.toList()));
    }
    
    /**
     * Test {@link StreamUtils#sequential(Iterable)} with a {@link Collection}
     */
    @Test
    void testSequentialCollection() {
        final Stream<Integer> stream = StreamUtils.sequential(List.of(1, 2, 3, 4, 5, 6));
        assertFalse(stream.isParallel());
        assertEquals(List.of(1, 2, 3, 4, 5, 6), stream.collect(Collectors.toList()));
    }
    
    /**
     * Test {@link StreamUtils#parallel(Iterable)} with a {@link Collection}
     */
    @Test
    void testParallelCollection() {
        final Stream<Integer> stream = StreamUtils.parallel(List.of(1, 2, 3, 4, 5, 6));
        assertTrue(stream.isParallel());
    }
    
    /**
     * Test {@link StreamUtils#sequential(Iterable)} with a  non-{@link Collection} {@link Iterable}
     */
    @Test
    void testSequentialIterable() {
        final List<Integer> l = List.of(1, 2, 3, 4, 5, 6);
        
        final Iterable<Integer> it = new Iterable<>() {

            @Override
            public Iterator<Integer> iterator() {
                return l.iterator();
            }
        };
        
        final Stream<Integer> stream = StreamUtils.sequential(it);
        assertFalse(stream.isParallel());
        assertEquals(List.of(1, 2, 3, 4, 5, 6), stream.collect(Collectors.toList()));
    }
    
    /**
     * Test {@link StreamUtils#parallel(Iterable)} with a  non-{@link Collection} {@link Iterable}
     */
    @Test
    void testParallelIterable() {
        final List<Integer> l = List.of(1, 2, 3, 4, 5, 6);
        
        final Iterable<Integer> it = new Iterable<>() {

            @Override
            public Iterator<Integer> iterator() {
                return l.iterator();
            }
        };
        
        final Stream<Integer> stream = StreamUtils.parallel(it);
        assertTrue(stream.isParallel());
    }
    
    /** Test {@link StreamUtils#stream(Iterable, Parallelism)} with {@link Parallelism#SEQUENTIAL} */
    @Test
    void testStreamIterableSequential() {
        final List<Integer> l = List.of(1, 2, 3, 4, 5, 6);
        
        final Iterable<Integer> it = new Iterable<>() {

            @Override
            public Iterator<Integer> iterator() {
                return l.iterator();
            }
        };
        
        final Stream<Integer> stream = StreamUtils.stream(it, Parallelism.SEQUENTIAL);
        assertFalse(stream.isParallel());
        assertEquals(List.of(1, 2, 3, 4, 5, 6), stream.collect(Collectors.toList()));
    }
    
    /** Test {@link StreamUtils#stream(Iterable, Parallelism)} with {@link Parallelism#PARALLEL} */
    @Test
    void testStreamIterableParallel() {
        final List<Integer> l = List.of(1, 2, 3, 4, 5, 6);
        
        final Iterable<Integer> it = new Iterable<>() {

            @Override
            public Iterator<Integer> iterator() {
                return l.iterator();
            }
        };
        
        final Stream<Integer> stream = StreamUtils.stream(it, Parallelism.PARALLEL);
        assertTrue(stream.isParallel());
    }
    
    /**
     * Test to show that Maps are equal in the {@link Map#equals(Object)} sense, but also show that their ordering is
     * the same.
     * 
     * @param expected the expected map
     * @param actual the actual map
     * @param clazz the expected type of {@code expected}.  May be null if we don't want to check the Map typing.
     */
    @SuppressWarnings("rawtypes")
    public static void assertMapsEqualWithOrdering(final Map<Double, Integer> expected,
                                                          final Map<Double, Integer> actual,
                                                          final Class<? extends Map> clazz) {
        if (clazz != null) {
            assertEquals(clazz, actual.getClass(), "Expected Map was the wrong type");
        }
        assertEquals(expected, actual, "maps contained different mappings");
        assertEquals(List.copyOf(expected.keySet()), List.copyOf(actual.keySet()), "key ordering was different");
    }
}
