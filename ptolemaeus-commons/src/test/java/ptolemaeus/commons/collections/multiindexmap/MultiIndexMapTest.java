/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections.multiindexmap;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.TestUtils;
import ptolemaeus.commons.collections.MapUtils;
import ptolemaeus.commons.collections.SetUtils;

/**
 * Tests {@link MultiIndexMap}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class MultiIndexMapTest {
    
    /** Test {@link MultiIndexMap#getSubMap(Iterable)} */
    @Test
    void testGetSubMap() {
        final MultiIndexMap<Integer, String> mim = getExample();
        
        // in getExample, we add the following key-value pairs, those in the expected value are tagged:
        // mim.put(List.of(1), "1");
        // mim.put(List.of(1, 2), "1, 2");
        // mim.put(List.of(1, 2, 3), "1, 2, 3");
        // mim.put(List.of(1, 2, 4), "1, 2, 4");
        // mim.put(List.of(1, 2, 4, 5), "1, 2, 4, 5");
        // mim.put(List.of(1, 2, 4, 6), "1, 2, 4, 6");
        // mim.put(List.of(1, 2, 3, 5), "1, 2, 3, 5");
        // mim.put(List.of(1, 3, 4, 5), "1, 3, 4, 5");
        // mim.put(List.of(1, 3, 4, 6), "1, 3, 4, 6");
        // mim.put(List.of(1, 4, 5, 6), "1, 4, 5, 6");
        // mim.put(List.of(1, 4, 6, 7, 8), "1, 4, 6, 7, 8");
        // mim.put(List.of(1, 4, 16, 64), "1, 4, 16, 64");
        // mim.put(List.of(1, 4, 16, 64, 256), "1, 4, 16, 64, 256");
        // mim.put(List.of(2, 3, 4, 5), "2, 3, 4, 5");                          A
        // mim.put(List.of(2, 3, 5, 6), "2, 3, 5, 6");                          B
        // mim.put(List.of(2, 3, 1, 4), "2, 3, 1, 4");                          C
        // mim.put(List.of(3, 4), "3, 4");
        // mim.put(List.of(3, 5), "3, 5");
        // mim.put(List.of(3, 6), "3, 6");
        // mim.put(List.of(3, 9, 81, 729), "3, 9, 81, 729");
        // mim.put(List.of(8, 6, 7, 5, 3, 0, 9), "8, 6, 7, 5, 3, 0, 9");
        
        final MultiIndexMap<Integer, String> subMap = mim.getSubMap(List.of(2, 3));
        final String expected = String.join(System.lineSeparator(),
                                            "",
                                            "4",               // key: 2, 3, 4, no explicit values
                                            "    5",           // key: 2, 3, 4, 5
                                            "    2, 3, 4, 5",  // - value, A
                                            "5",               // key: 2, 3, 5, no explicit values
                                            "    6",           // key: 2, 3, 5, 6
                                            "    2, 3, 5, 6",  // - value, B
                                            "1",               // key: 2, 3, 1, no explicit values
                                            "    4",           // key: 2, 3, 1, 4
                                            "    2, 3, 1, 4"); // - value, C
        TestUtils.testMultiLineToString(expected, subMap);
    }
    
    /** Test {@link MultiIndexMap#put(Object, Object...)},
     *       {@link MultiIndexMap#get(Object...)},
     *       {@link MultiIndexMap#getOrDefault(Object, Object...)}, and
     *       {@link MultiIndexMap#computeIfAbsent(Function, Object...)} */
    @Test
    void testVarArgs() {
        final MultiIndexMap<Integer, String> mim = new MultiIndexMap<>(LinkedHashMap::new);
        final String first = "first";
        mim.put(first, 0, 1, 2, 3);
        mim.put("second", 0, 1, 2);
        mim.put("third", 0, 1, 2, 3, 4);
        
        assertEquals("first", mim.get(0, 1, 2, 3));
        assertEquals("second", mim.get(0, 1, 2));
        assertEquals("third", mim.get(0, 1, 2, 3, 4));
        
        assertEquals("default", mim.getOrDefault("default", 0, 1, 2, 3, 5));
        
        assertSame(first, mim.computeIfAbsent(k -> "not computed", 0, 1, 2, 3));
        assertEquals("computed", mim.computeIfAbsent(k -> "computed", 0, 1, 2, 3, 4, 5));
    }
    
    /**
     * Test {@link MultiIndexMap#MultiIndexMap(Supplier)}
     */
    @Test
    void testMultiIndexMapConstructor() {
        assertDoesNotThrow(() -> new MultiIndexMap<>());
    }
    
    /**
     * Test {@link MultiIndexMap#MultiIndexMap()}
     */
    @Test
    void testMultiIndexMapConstructor2() {
        assertDoesNotThrow(() -> new MultiIndexMap<>(LinkedHashMap::new));
    }
    
    /**
     * Test {@link MultiIndexMap#putAll(Map)}
     */
    @Test
    void testPutAll() {
        final MultiIndexMap<Integer, String> mim = new MultiIndexMap<>();
        final Map<List<Integer>, String> map = MapUtils.of(List.of(5), "1",
                                                           List.of(5, 4), "2",
                                                           List.of(5, 4, 3), "3",
                                                           List.of(5, 4, 3, 2), "4",
                                                           List.of(5, 4, 3, 2, 1), "5",
                                                           List.of(5, 4, 3, 2, 1, 0), "6");
        mim.putAll(map);
        
        map.forEach((k, v) -> assertEquals(v, mim.get(k)));
    }
    
    /**
     * Test {@link MultiIndexMap#merge(MultiIndexMap, MultiIndexMap)}
     */
    @Test
    void testMerge() {
        final MultiIndexMap<Integer, String> mim1 = getExample();
        final MultiIndexMap<Integer, String> mim2 = new MultiIndexMap<>(LinkedHashMap::new);
        mim2.put(List.of(9), "9");
        mim2.put(List.of(9, 99), "9, 99");
        mim2.put(List.of(9, 99, 999), "9, 99, 999");
        mim2.put(List.of(9, 99, 999, 9999), "9, 99, 999, 9999");
        
        final MultiIndexMap<Integer, String> merged = MultiIndexMap.merge(mim1, mim2);
        
        for (final Iterable<Integer> key : mim1.keySet()) {
            assertTrue(merged.containsKey(key), () -> String.format("merged did not contain key %s from mim1", key));
        }
        
        for (final Iterable<Integer> key : mim2.keySet()) {
            assertTrue(merged.containsKey(key), () -> String.format("merged did not contain key %s from mim2", key));
        }
    }
    
    /**
     * Test {@link MultiIndexMap#computeIfAbsent(Function, Iterable)}
     */
    @Test
    void testComputeIfAbsent() {
        final MultiIndexMap<Integer, String> mim = getExample();
        
        assertEquals(null, mim.get(List.of(10)));
        assertEquals("10", mim.computeIfAbsent(String::valueOf, List.of(10)));
        assertEquals("10", mim.get(List.of(10)));
        
        assertEquals("10", mim.computeIfAbsent(i -> "not 10", List.of(10))); // does not overwrite existing value
        assertEquals("10", mim.get(List.of(10)));
    }
    
    /**
     * Test {@link MultiIndexMap#toString()}
     */
    @Test
    void testToString() {
        final MultiIndexMap<Integer, String> mim = getExample();
        // lines with commas are values associated with the sequence of keys that map to them
        String expected = String.join(System.lineSeparator(),
                                      "",
                                      "1",
                                      "1",
                                      "    2",
                                      "    1, 2",
                                      "        3",
                                      "        1, 2, 3",
                                      "            5",
                                      "            1, 2, 3, 5",
                                      "        4",
                                      "        1, 2, 4",
                                      "            5",
                                      "            1, 2, 4, 5",
                                      "            6",
                                      "            1, 2, 4, 6",
                                      "    3",
                                      "        4",
                                      "            5",
                                      "            1, 3, 4, 5",
                                      "            6",
                                      "            1, 3, 4, 6",
                                      "    4",
                                      "        5",
                                      "            6",
                                      "            1, 4, 5, 6",
                                      "        6",
                                      "            7",
                                      "                8",
                                      "                1, 4, 6, 7, 8",
                                      "        16",
                                      "            64",
                                      "            1, 4, 16, 64",
                                      "                256",
                                      "                1, 4, 16, 64, 256",
                                      "2",
                                      "    3",
                                      "        4",
                                      "            5",
                                      "            2, 3, 4, 5",
                                      "        5",
                                      "            6",
                                      "            2, 3, 5, 6",
                                      "        1",
                                      "            4",
                                      "            2, 3, 1, 4",
                                      "3",
                                      "    4",
                                      "    3, 4",
                                      "    5",
                                      "    3, 5",
                                      "    6",
                                      "    3, 6",
                                      "    9",
                                      "        81",
                                      "            729",
                                      "            3, 9, 81, 729",
                                      "8",
                                      "    6",
                                      "        7",
                                      "            5",
                                      "                3",
                                      "                    0",
                                      "                        9",
                                      "                        8, 6, 7, 5, 3, 0, 9");
        TestUtils.testMultiLineToString(expected, mim.toString());
    }
    
    /**
     * Test {@link MultiIndexMap#remove(Object)}
     */
    @Test
    void testRemove() {
        final MultiIndexMap<Integer, String> mim = getExample();
        final String oldValue = mim.remove(List.of(1, 2, 3));
        assertEquals("1, 2, 3", oldValue);
        assertNull(mim.get(List.of(1, 2, 3)));
    }
    
    /**
     * Test {@link MultiIndexMap#values()}
     */
    @Test
    void testValues() {
        final MultiIndexMap<Integer, String> mim = getExample();
        final String actual = String.join(System.lineSeparator(), mim.values());
        String expected = String.join(System.lineSeparator(),
                                      "1",
                                      "1, 2",
                                      "1, 2, 3",
                                      "1, 2, 3, 5",
                                      "1, 2, 4",
                                      "1, 2, 4, 5",
                                      "1, 2, 4, 6",
                                      "1, 3, 4, 5",
                                      "1, 3, 4, 6",
                                      "1, 4, 5, 6",
                                      "1, 4, 6, 7, 8",
                                      "1, 4, 16, 64",
                                      "1, 4, 16, 64, 256",
                                      "2, 3, 4, 5",
                                      "2, 3, 5, 6",
                                      "2, 3, 1, 4",
                                      "3, 4",
                                      "3, 5",
                                      "3, 6",
                                      "3, 9, 81, 729",
                                      "8, 6, 7, 5, 3, 0, 9");
        TestUtils.testMultiLineToString(expected, actual);
    }
    
    /**
     * Test {@link MultiIndexMap#clear()}
     */
    @Test
    void testClear() {
        final MultiIndexMap<Integer, String> mim = getExample();
        assertEquals(21, mim.size());
        mim.clear();
        assertEquals(0, mim.size());
    }
    
    /**
     * Test {@link MultiIndexMap#isEmpty()}
     */
    @Test
    void testIsEmpty() {
        final MultiIndexMap<Integer, String> mim = getExample();
        assertFalse(mim.isEmpty());
        mim.clear();
        assertTrue(mim.isEmpty());
    }
    
    /**
     * Test {@link MultiIndexMap#size()}
     */
    @Test
    void testSize() {
        final MultiIndexMap<Integer, String> mim = getExample();
        assertEquals(21, mim.size());
    }
    
    /**
     * Test {@link MultiIndexMap#entrySet()}
     */
    @Test
    void testEntrySet() {
        final MultiIndexMap<Integer, String> mim = getExample();
        final Set<Entry<Iterable<Integer>, String>> entrySet = mim.entrySet();
        entrySet.forEach(entry -> assertEquals(entry.getValue(), mim.get(entry.getKey())));
    }
    
    /**
     * Test {@link MultiIndexMap#containsKey(Object)}
     */
    @Test
    void testContainsKey() {
        final MultiIndexMap<Integer, String> mim = getExample();
        
        assertTrue(mim.containsKey(List.of(1)));
        assertTrue(mim.containsKey(List.of(1, 2)));
        assertTrue(mim.containsKey(List.of(1, 2, 4, 6)));
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64)));
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertFalse(mim.containsKey(List.of(3, 9, 81))); // there is a path containing this key, but its value is null
        
        assertFalse(mim.containsKey(null));
        assertFalse(mim.containsKey(List.of()));
        
        assertFalse(mim.containsKey(List.of(10)));
        
        // for fun:
        mim.keySet().forEach(key -> assertTrue(mim.containsKey(key)));
    }
    
    /**
     * Test {@link MultiIndexMap#containsValue(Object)}
     */
    @Test
    void testContainsValue() {
        final MultiIndexMap<Integer, String> mim = getExample();
        assertTrue(mim.containsValue("1"));
        assertTrue(mim.containsValue("1, 2"));
        assertTrue(mim.containsValue("1, 2, 4, 6"));
        assertTrue(mim.containsValue("1, 4, 16, 64"));
        assertTrue(mim.containsValue("1, 4, 16, 64, 256"));
        assertTrue(mim.containsValue("3, 9, 81, 729"));
        
        assertFalse(mim.containsValue("not a value"));
        assertFalse(mim.containsValue(null));
        
        // for fun:
        mim.values().forEach(val -> assertTrue(mim.containsValue(val)));
    }
    
    /**
     * Test {@link MultiIndexMap#keySet()}
     */
    @Test
    void testKeySet() {
        final MultiIndexMap<Integer, String> mim = getExample();
        final Set<Iterable<Integer>> expected = SetUtils.of(List.of(1),
                                                            List.of(1, 2),
                                                            List.of(1, 2, 3),
                                                            List.of(1, 2, 4),
                                                            List.of(1, 2, 4, 5),
                                                            List.of(1, 2, 4, 6),
                                                            List.of(1, 2, 3, 5),
                                                            List.of(1, 3, 4, 5),
                                                            List.of(1, 3, 4, 6),
                                                            List.of(1, 4, 5, 6),
                                                            List.of(1, 4, 6, 7, 8),
                                                            List.of(1, 4, 16, 64),
                                                            List.of(1, 4, 16, 64, 256),
                                                            List.of(2, 3, 4, 5),
                                                            List.of(2, 3, 5, 6),
                                                            List.of(2, 3, 1, 4),
                                                            List.of(3, 4),
                                                            List.of(3, 5),
                                                            List.of(3, 6),
                                                            List.of(3, 9, 81, 729),
                                                            List.of(8, 6, 7, 5, 3, 0, 9));
        
        final Set<Iterable<Integer>> actual = mim.keySet();
        
        for (Iterable<Integer> key : expected) {
            assertTrue(actual.contains(key), () -> String.format("expected contained %s but actual did not", key));
        }
        
        for (Iterable<Integer> key : actual) {
            assertTrue(expected.contains(key), () -> String.format("actual contained %s but expected did not", key));
        }
    }
    
    /* Test view operations Iterator::remove, ::remove, ::removeAll, ::retainAll and ::clear. Note: all remove
     * operations are performed through Iterator::remove so those working are proof of Iterator::remove working. */
    
    /**
     * Test key set view operations: {@link Iterator#remove()}, {@link Set#remove(Object)},
     * {@link Set#removeAll(Collection)}, {@link Set#retainAll(Collection)}, and {@link Set#clear()}
     */
    @Test
    void testKeySetViewOps() {
        final MultiIndexMap<Integer, String> mim = getExample();
        
        assertTrue(mim.containsKey(List.of(1)));
        assertTrue(mim.containsKey(List.of(1, 2)));
        assertTrue(mim.containsKey(List.of(1, 2, 3)));
        assertTrue(mim.containsKey(List.of(1, 2, 3, 5)));
        
        final Set<Iterable<Integer>> keySet = mim.keySet();
        
        assertEquals(21, keySet.size());
        
        keySet.remove(List.of(1));
        keySet.remove(List.of(1, 2));
        keySet.remove(List.of(1, 2, 3));
        keySet.remove(List.of(1, 2, 3, 5));
        
        assertFalse(mim.containsKey(List.of(1)));
        assertFalse(mim.containsKey(List.of(1, 2)));
        assertFalse(mim.containsKey(List.of(1, 2, 3)));
        assertFalse(mim.containsKey(List.of(1, 2, 3, 5)));
        
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64)));
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        keySet.removeAll(List.of(List.of(1, 4, 16, 64, 256), List.of(1, 4, 16, 64)));
        assertFalse(mim.containsKey(List.of(1, 4, 16, 64)));
        assertFalse(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        
        assertTrue(mim.containsKey(List.of(3, 4)));
        assertTrue(mim.containsKey(List.of(3, 5)));
        assertTrue(mim.containsKey(List.of(3, 6)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertTrue(mim.containsKey(List.of(8, 6, 7, 5, 3, 0, 9)));
        
        keySet.retainAll(List.of(List.of(3, 4), List.of(3, 5), List.of(3, 6), List.of(3, 9, 81, 729)));
        assertTrue(mim.containsKey(List.of(3, 4)));
        assertTrue(mim.containsKey(List.of(3, 5)));
        assertTrue(mim.containsKey(List.of(3, 6)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertFalse(mim.containsKey(List.of(8, 6, 7, 5, 3, 0, 9))); // note that it no longer contains this key
        
        assertFalse(mim.isEmpty());
        keySet.clear();
        assertTrue(mim.isEmpty());
    }
    
    /**
     * Test value collection view operations: {@link Iterator#remove()}, {@link Collection#remove(Object)},
     * {@link Collection#removeAll(Collection)}, {@link Collection#retainAll(Collection)}, and
     * {@link Collection#clear()}
     */
    @Test
    void testValueCollectionViewOps() {
        final MultiIndexMap<Integer, String> mim = getExample();
        
        assertTrue(mim.containsKey(List.of(1)));
        assertTrue(mim.containsKey(List.of(1, 2)));
        assertTrue(mim.containsKey(List.of(1, 2, 3)));
        assertTrue(mim.containsKey(List.of(1, 2, 3, 5)));
        
        final Set<Entry<Iterable<Integer>, String>> entrySet = mim.entrySet();
        
        assertEquals(21, entrySet.size());
        
        entrySet.remove(new SimpleEntry<>(List.of(1), "1"));
        entrySet.remove(new SimpleEntry<>(List.of(1, 2), "1, 2"));
        entrySet.remove(new SimpleEntry<>(List.of(1, 2, 3), "1, 2, 3"));
        entrySet.remove(new SimpleEntry<>(List.of(1, 2, 3, 5), "1, 2, 3, 5"));
        
        assertFalse(mim.containsKey(List.of(1)));
        assertFalse(mim.containsKey(List.of(1, 2)));
        assertFalse(mim.containsKey(List.of(1, 2, 3)));
        assertFalse(mim.containsKey(List.of(1, 2, 3, 5)));
        
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64)));
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        entrySet.removeAll(List.of(new SimpleEntry<>(List.of(1, 4, 16, 64, 256), "1, 4, 16, 64, 256"),
                                   new SimpleEntry<>(List.of(1, 4, 16, 64), "1, 4, 16, 64")));
        assertFalse(mim.containsKey(List.of(1, 4, 16, 64)));
        assertFalse(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        
        assertTrue(mim.containsKey(List.of(3, 4)));
        assertTrue(mim.containsKey(List.of(3, 5)));
        assertTrue(mim.containsKey(List.of(3, 6)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertTrue(mim.containsKey(List.of(8, 6, 7, 5, 3, 0, 9)));
        
        entrySet.retainAll(List.of(new SimpleEntry<>(List.of(3, 4), "3, 4"),
                                   new SimpleEntry<>(List.of(3, 5), "3, 5"),
                                   new SimpleEntry<>(List.of(3, 6), "3, 6"),
                                   new SimpleEntry<>(List.of(3, 9, 81, 729), "3, 9, 81, 729")));
        
        assertTrue(mim.containsKey(List.of(3, 4)));
        assertTrue(mim.containsKey(List.of(3, 5)));
        assertTrue(mim.containsKey(List.of(3, 6)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertFalse(mim.containsKey(List.of(8, 6, 7, 5, 3, 0, 9))); // note that it no longer contains this key
        
        assertFalse(mim.isEmpty());
        entrySet.clear();
        assertTrue(mim.isEmpty());
    }
    
    /**
     * Test entry set view operations: {@link Iterator#remove()}, {@link Set#remove(Object)},
     * {@link Set#removeAll(Collection)}, {@link Set#retainAll(Collection)}, and {@link Set#clear()}
     */
    @Test
    void testEntrySetViewOps() {
        final MultiIndexMap<Integer, String> mim = getExample();
        
        assertTrue(mim.containsKey(List.of(1)));
        assertTrue(mim.containsKey(List.of(1, 2)));
        assertTrue(mim.containsKey(List.of(1, 2, 3)));
        assertTrue(mim.containsKey(List.of(1, 2, 3, 5)));
        
        final Collection<String> valueCollection = mim.values();
        
        assertEquals(21, valueCollection.size());
        
        valueCollection.remove("1");
        valueCollection.remove("1, 2");
        valueCollection.remove("1, 2, 3");
        valueCollection.remove("1, 2, 3, 5");
        
        assertFalse(mim.containsKey(List.of(1)));
        assertFalse(mim.containsKey(List.of(1, 2)));
        assertFalse(mim.containsKey(List.of(1, 2, 3)));
        assertFalse(mim.containsKey(List.of(1, 2, 3, 5)));
        
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64)));
        assertTrue(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        valueCollection.removeAll(List.of("1, 4, 16, 64, 256", "1, 4, 16, 64"));
        assertFalse(mim.containsKey(List.of(1, 4, 16, 64)));
        assertFalse(mim.containsKey(List.of(1, 4, 16, 64, 256)));
        
        assertTrue(mim.containsKey(List.of(3, 4)));
        assertTrue(mim.containsKey(List.of(3, 5)));
        assertTrue(mim.containsKey(List.of(3, 6)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertTrue(mim.containsKey(List.of(8, 6, 7, 5, 3, 0, 9)));
        
        valueCollection.retainAll(List.of("3, 4", "3, 5", "3, 6", "3, 9, 81, 729"));
        assertTrue(mim.containsKey(List.of(3, 4)));
        assertTrue(mim.containsKey(List.of(3, 5)));
        assertTrue(mim.containsKey(List.of(3, 6)));
        assertTrue(mim.containsKey(List.of(3, 9, 81, 729)));
        
        assertFalse(mim.containsKey(List.of(8, 6, 7, 5, 3, 0, 9))); // note that it no longer contains this key
        
        assertFalse(mim.isEmpty());
        valueCollection.clear();
        assertTrue(mim.isEmpty());
    }
    
    /**
     * @return an example {@link MultiIndexMap}
     */
    private static MultiIndexMap<Integer, String> getExample() {
        final MultiIndexMap<Integer, String> mim = new MultiIndexMap<>(LinkedHashMap::new);
        mim.put(List.of(1), "1");
        mim.put(List.of(1, 2), "1, 2");
        mim.put(List.of(1, 2, 3), "1, 2, 3");
        mim.put(List.of(1, 2, 4), "1, 2, 4");
        mim.put(List.of(1, 2, 4, 5), "1, 2, 4, 5");
        mim.put(List.of(1, 2, 4, 6), "1, 2, 4, 6");
        mim.put(List.of(1, 2, 3, 5), "1, 2, 3, 5");
        mim.put(List.of(1, 3, 4, 5), "1, 3, 4, 5");
        mim.put(List.of(1, 3, 4, 6), "1, 3, 4, 6");
        mim.put(List.of(1, 4, 5, 6), "1, 4, 5, 6");
        mim.put(List.of(1, 4, 6, 7, 8), "1, 4, 6, 7, 8");
        mim.put(List.of(1, 4, 16, 64), "1, 4, 16, 64");
        mim.put(List.of(1, 4, 16, 64, 256), "1, 4, 16, 64, 256");
        mim.put(List.of(2, 3, 4, 5), "2, 3, 4, 5");
        mim.put(List.of(2, 3, 5, 6), "2, 3, 5, 6");
        mim.put(List.of(2, 3, 1, 4), "2, 3, 1, 4");
        mim.put(List.of(3, 4), "3, 4");
        mim.put(List.of(3, 5), "3, 5");
        mim.put(List.of(3, 6), "3, 6");
        mim.put(List.of(3, 9, 81, 729), "3, 9, 81, 729");
        mim.put(List.of(8, 6, 7, 5, 3, 0, 9), "8, 6, 7, 5, 3, 0, 9");
        return mim;
    }
}