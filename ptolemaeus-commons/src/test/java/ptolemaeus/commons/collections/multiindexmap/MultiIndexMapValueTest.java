/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons.collections.multiindexmap;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.TestUtils;

/**
 * Tests {@link MultiIndexMapValue}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
class MultiIndexMapValueTest {
    
    /**
     * Test {@link MultiIndexMapValue#MultiIndexMapValue(Supplier, Object)}
     */
    @Test
    void testMultiIndexMapValueConstructor() {
        assertDoesNotThrow(() -> new MultiIndexMapValue<>(LinkedHashMap::new, ""));
    }
    
    /**
     * Test {@link MultiIndexMapValue#toString()}
     */
    @Test
    void testToString() {
        final MultiIndexMapValue<Integer, String> mimv = new MultiIndexMapValue<>(LinkedHashMap::new, "data!");
        mimv.getNestedCache().put(List.of(9), "9");
        mimv.getNestedCache().put(List.of(9, 99), "9, 99");
        mimv.getNestedCache().put(List.of(9, 99, 999), "9, 99, 999");
        mimv.getNestedCache().put(List.of(9, 99, 999, 9999), "9, 99, 999, 9999");
        String expected = String.join(System.lineSeparator(),
                                      "",
                                      "data!", // here!
                                      "    9",
                                      "    9",
                                      "        99",
                                      "        9, 99",
                                      "            999",
                                      "            9, 99, 999",
                                      "                9999",
                                      "                9, 99, 999, 9999");
        TestUtils.testMultiLineToString(expected, mimv.toString());
    }
    
    /**
     * Test {@link MultiIndexMapValue#merge(MultiIndexMapValue, MultiIndexMapValue)}
     */
    @Test
    void testMerge() {
        final MultiIndexMapValue<Integer, String> mimv1 = new MultiIndexMapValue<>(LinkedHashMap::new, "data one!");
        mimv1.getNestedCache().put(List.of(9), "9");
        mimv1.getNestedCache().put(List.of(9, 99), "9, 99");
        mimv1.getNestedCache().put(List.of(9, 99, 999), "9, 99, 999");
        mimv1.getNestedCache().put(List.of(9, 99, 999, 9999), "9, 99, 999, 9999");
        
        final MultiIndexMapValue<Integer, String> mimv2 = new MultiIndexMapValue<>(LinkedHashMap::new, "data two!");
        mimv2.getNestedCache().put(List.of(9), "1");
        mimv2.getNestedCache().put(List.of(9, 99), "1, 11");
        mimv2.getNestedCache().put(List.of(9, 99, 999), "1, 11, 111");
        mimv2.getNestedCache().put(List.of(9, 99, 999, 9999), "1, 11, 111, 1111");
        mimv2.getNestedCache().put(List.of(9, 123), "different!");
        
        final MultiIndexMapValue<Integer, String> merged = MultiIndexMapValue.merge(mimv1, mimv2);
        assertEquals("data two!", merged.getData());
        assertEquals("data two!", mimv1.getData()); // observe that mimv1 is changed
    }
    
    /**
     * Test {@link MultiIndexMapValue#setData(Object)}
     */
    @Test
    void testSetData() {
        final MultiIndexMapValue<Integer, String> mimv = getExample();
        mimv.setData("new data");
        assertEquals("new data", mimv.getData());
    }
    
    /**
     * Test {@link MultiIndexMapValue#getNestedCache()}
     */
    @Test
    void testGetNestedCache() {
        assertNotNull(new MultiIndexMapValue<>(LinkedHashMap::new, "data!").getNestedCache()); // it's never null
    }
    
    /**
     * Test {@link MultiIndexMapValue#getData()}
     */
    @Test
    void testGetData() {
        final MultiIndexMapValue<Integer, String> mimv = getExample();
        assertEquals("data!", mimv.getData());
    }
    
    /**
     * @return an example {@link MultiIndexMapValue}
     */
    private static MultiIndexMapValue<Integer, String> getExample() {
        final MultiIndexMapValue<Integer, String> answer = new MultiIndexMapValue<>(LinkedHashMap::new, "data!");
        answer.getNestedCache().put(List.of(9), "9");
        answer.getNestedCache().put(List.of(9, 99), "9, 99");
        answer.getNestedCache().put(List.of(9, 99, 999), "9, 99, 999");
        answer.getNestedCache().put(List.of(9, 99, 999, 9999), "9, 99, 999, 9999");
        return answer;
    }
}