/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import org.junit.jupiter.api.Test;

import ptolemaeus.commons.TestUtils;

/**
 * Test {@link SetUtils}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class SetUtilsTest {
    
    /**
     * Test {@link SetUtils#copyOf(Collection)}
     */
    @Test
    void testCopyOf() {
        Set<Integer> set = new LinkedHashSet<>();
        set.add(0);
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        
        assertEquals(set, SetUtils.copyOf(set));
    }
    
    /**
     * Test {@link SetUtils#copyOf(NavigableSet)}
     */
    @Test
    void testCopyOf2() {
        NavigableSet<Integer> set = new TreeSet<>();
        set.add(4);
        set.add(3);
        set.add(2);
        set.add(1);
        set.add(0);
        
        NavigableSet<Integer> copy = SetUtils.copyOf(set);
        assertEquals(set, copy);
        TestUtils.assertSorted(copy);
    }
    
    /**
     * Test {@link SetUtils#empty()}
     */
    @Test
    void testEmpty() {
        assertTrue(SetUtils.empty().isEmpty());
        assertEquals(LinkedHashSet.class, SetUtils.empty().getClass());
    }
    
    /**
     * Test {@link SetUtils#emptyNavigable()}
     */
    @Test
    void testEmptyNavigable() {
        assertTrue(SetUtils.emptyNavigable().isEmpty());
        assertTrue(NavigableSet.class.isAssignableFrom(SetUtils.emptyNavigable().getClass()));
    }
    
    /**
     * Test {@link SetUtils#of(Object[])}
     */
    @Test
    void testOf() {
        Set<Integer> set1 = new LinkedHashSet<>();
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(4);
        
        Set<Integer> set2 = SetUtils.of(0, 1, 2, 3, 4);
        assertEquals(set1, set2);
    }
    
    /**
     * Test {@link SetUtils#navigableSetOf(Comparable[])}
     */
    @Test
    void testNavigableSetOf() {
        NavigableSet<Integer> set = SetUtils.navigableSetOf(5, 4, 3, 2, 1);
        TestUtils.assertSorted(set);
    }
    
    /**
     * Test {@link SetUtils#navigableSetOf(Comparator, Object[])}
     */
    @Test
    void testNavigableSetOf2() {
        NavigableSet<Integer> set = SetUtils.navigableSetOf(Comparator.reverseOrder(), 1, 2, 3, 4, 5);
        TestUtils.assertSorted(set, Comparator.reverseOrder());
    }
    
    /**
     * Test subtract()
     */
    @Test
    public void subtract() {
        Set<Integer> original = SetUtils.of(1, 2, 3, 4, 5);
        Set<Integer> modified = SetUtils.subtract(original, 3);
        assertEquals(SetUtils.of(1, 2, 4, 5), modified);
        
        assertThrows(IllegalArgumentException.class, () -> SetUtils.subtract(original, 8));
    }
    
    /**
     * Test getIntersection()
     */
    @Test
    public void getIntersection() {
        Set<Integer> first = SetUtils.of(1, 2, 3, 4, 5);
        Set<Integer> second = SetUtils.of(4, 5, 6, 7, 8);
        assertEquals(SetUtils.of(4, 5), SetUtils.getIntersection(first, second));
        assertEquals(SetUtils.of(), SetUtils.getIntersection(first, SetUtils.of()));
        assertEquals(SetUtils.of(), SetUtils.getIntersection(SetUtils.of(), first));
    }
    
    /**
     * Test {@link SetUtils#union(Collection, Collection)}
     */
    @Test
    void testUnion() {
        final Set<Integer> first = SetUtils.of(1, 2, 3, 4, 5);
        final Set<Integer> second = SetUtils.of(4, 5, 6, 7, 8);
        assertEquals(SetUtils.of(1, 2, 3, 4, 5, 6, 7, 8), SetUtils.union(first, second));
        assertEquals(first, SetUtils.union(first, SetUtils.of()));
        assertEquals(second, SetUtils.union(SetUtils.of(), second));
    }

    /**
     * Test setDiff()
     */
    @Test
    public void setDiff() {
        Set<Integer> even = SetUtils.setDiff(
                List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
                List.of(1, 3, 5, 7, 9));
        assertEquals(Set.of(2, 4, 6, 8, 10), even);
    }
    
}
