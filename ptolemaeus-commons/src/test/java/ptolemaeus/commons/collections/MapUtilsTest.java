/**
 * MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ptolemaeus.commons.collections;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

/**
 * Tests for {@link MapUtils}
 * 
 * @author Ryan Moser, Ryan.T.Moser@lmco.com
 */
public class MapUtilsTest {
    
    /**
     * @return get a testing map with [1 - 10] mapped to [a - j]
     */
    private static Map<Integer, String> get1to10AtoJMap() {
        return MapUtils.of(1, "a",
                           2, "b",
                           3, "c",
                           4, "d",
                           5, "e",
                           6, "f",
                           7, "g",
                           8, "h",
                           9, "i",
                           10, "j");
    }
    
    /**
     * Test {@link MapUtils#modifiableCopyOf(Map)}.  Mutable and non-Navigable.
     */
    @Test
    public void testCopyOf() {
        Map<Integer, String> map = get1to10AtoJMap();
        Map<Integer, String> copy = MapUtils.modifiableCopyOf(map);
        assertEquals(new LinkedList<>(map.keySet()), new LinkedList<>(copy.keySet()));
        assertDoesNotThrow(() -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#modifiableCopyOf(NavigableMap)}.  Mutable and Navigable.
     */
    @Test
    public void testCopyOfNavigable() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                91, "a",
                82, "b",
                73, "c",
                64, "d",
                55, "e",
                46, "f",
                37, "g",
                28, "h",
                19, "i",
                10, "j",
                Comparator.reverseOrder()); // the copy maintains the same ordering
        
        NavigableMap<Integer, String> copy = MapUtils.modifiableCopyOf(map);
        assertEquals(new LinkedList<>(map.keySet()), new LinkedList<>(copy.keySet()));
        assertDoesNotThrow(() -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#modifiableCopyOf(Map)}.  Immutable and non-Navigable.
     */
    @Test
    public void testUnModCopyOf() {
        Map<Integer, String> map = get1to10AtoJMap();
        Map<Integer, String> copy = MapUtils.copyOf(map);
        assertEquals(new LinkedList<>(map.keySet()), new LinkedList<>(copy.keySet()));
        assertThrows(UnsupportedOperationException.class, () -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#modifiableCopyOf(NavigableMap)}.  Immutable and Navigable.
     */
    @Test
    public void testUnModCopyOfNavigable() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                91, "a",
                82, "b",
                73, "c",
                64, "d",
                55, "e",
                46, "f",
                37, "g",
                28, "h",
                19, "i",
                10, "j",
                Comparator.reverseOrder()); // the copy maintains the same ordering
        
        NavigableMap<Integer, String> copy = MapUtils.copyOf(map);
        assertEquals(new LinkedList<>(map.keySet()), new LinkedList<>(copy.keySet()));
        assertThrows(UnsupportedOperationException.class, () -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#copyOf(Map, Supplier)}.  Mutable and non-Navigable Supplier.
     */
    @Test
    public void testCopyOfSupplier() {
        Map<Integer, String> map = get1to10AtoJMap();
        Map<Integer, String> copy = MapUtils.copyOf(map, LinkedHashMap::new);
        assertEquals(new LinkedList<>(map.keySet()), new LinkedList<>(copy.keySet()));
        assertDoesNotThrow(() -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#copyOf(Map, Supplier)}.  Mutable and Navigable Supplier.
     */
    @Test
    public void testNavCopyOfSupplier() {
        Map<Integer, String> map = MapUtils.of(
                91, "a",
                82, "b",
                73, "c",
                64, "d",
                55, "e",
                46, "f",
                37, "g",
                28, "h",
                19, "i",
                10, "j");
        Map<Integer, String> copy = MapUtils.copyOf(map, TreeMap::new);
        List<Integer> expected = new LinkedList<>(map.keySet());
        Collections.reverse(expected); // We pass in a Supplier to create a TreeMap without a Comparator 
                                       // so it orders by the natural order of the keys (Integers)
        assertEquals(expected, new LinkedList<>(copy.keySet()));
        assertDoesNotThrow(() -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#copyOf(Map, Supplier)}.  Mutable and Navigable Supplier.
     */
    @Test
    public void testNavCopyOfSupplier2() {
        Map<Integer, String> map = MapUtils.of(
                91, "a",
                82, "b",
                73, "c",
                64, "d",
                55, "e",
                46, "f",
                37, "g",
                28, "h",
                19, "i",
                10, "j");
        Map<Integer, String> copy = MapUtils.copyOf(map, () -> new TreeMap<>(Comparator.reverseOrder()));
        // no need to reverse because we not provide a Supplier which create a reverse-order Comparator
        assertEquals(new LinkedList<>(map.keySet()), new LinkedList<>(copy.keySet()));
        assertDoesNotThrow(() -> copy.put(11, "eleven"));
    }
    
    /**
     * Test {@link MapUtils#of()} with 0 entries
     */
    @Test
    public void test0ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of();
        
        assertEquals(0, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
    }
    
    /**
     * Test {@link MapUtils#of()} with 1 entry
     */
    @Test
    public void test1ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                10, "j");
        
        assertEquals(1, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map3 = MapUtils.of(
                null, "notNull");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                null, null);
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 2 entries
     */
    @Test
    public void test2ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                9, "i",
                10, "j");
        
        assertEquals(2, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                9, "i",
                9, "replacedi");
        
        assertEquals(1, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                8, null,
                null, "notNull");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 3 entries
     */
    @Test
    public void test3ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(3, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(2, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 4 entries
     */
    @Test
    public void test4ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(4, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(3, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 5 entries
     */
    @Test
    public void test5ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(5, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(4, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                6, "f",
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                6, "f",
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 6 entries
     */
    @Test
    public void test6ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(6, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(5, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 7 entries
     */
    @Test
    public void test7ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(7, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(6, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 8 entries
     */
    @Test
    public void test8ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(8, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(7, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 9 entries
     */
    @Test
    public void test9ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(9, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(8, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#of()} with 10 entries
     */
    @Test
    public void test10ElementOfMethod() {
        Map<Integer, String> map = MapUtils.of(
                1, "a",
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                10, "j");
        
        assertEquals(10, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        Map<Integer, String> map2 = MapUtils.of(
                1, "a",
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, "h",
                9, "i",
                9, "replacedi");
        
        assertEquals(9, map2.size());
        assertEquals("replacedi", map2.get(9));
        
        Map<Integer, String> map3 = MapUtils.of(
                1, "a",
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, "notNull",
                9, "j");
        
        assertEquals("notNull", map3.get(null));
        assertNull(map3.get(8));
        
        Map<Integer, String> map4 = MapUtils.of(
                1, "a",
                2, "b",
                3, "c",
                4, "d",
                5, "e",
                6, "f",
                7, "g",
                8, null,
                null, null,
                10, "j");
        
        assertNull(map4.get(null));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 0 entries
     */
    @Test
    public void test0ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf();
        assertEquals(0, map.size());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 1 entry
     */
    @Test
    public void test1ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(1, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 2 entries
     */
    @Test
    public void test2ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(2, map.size());
        assertEquals("b", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                2, "b",
                2, "replacedb");
        
        assertEquals(1, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 3 entries
     */
    @Test
    public void test3ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(3, map.size());
        assertEquals("c", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(2, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 4 entries
     */
    @Test
    public void test4ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(4, map.size());
        assertEquals("d", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(3, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 5 entries
     */
    @Test
    public void test5ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(5, map.size());
        assertEquals("e", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(4, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 6 entries
     */
    @Test
    public void test6ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(6, map.size());
        assertEquals("f", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(5, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 7 entries
     */
    @Test
    public void test7ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(7, map.size());
        assertEquals("g", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(6, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 8 entries
     */
    @Test
    public void test8ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(8, map.size());
        assertEquals("h", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(7, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 9 entries
     */
    @Test
    public void test9ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(9, map.size());
        assertEquals("i", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(8, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 10 entries
     */
    @Test
    public void test10ElementNavigableMapOfMethod() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                10, "j",
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a");
        assertMapSorted(map, null);        
        assertEquals(10, map.size());
        assertEquals("j", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                10, "j",
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb");
        
        assertEquals(9, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 2 entries with a {@link Comparator}
     */
    @Test
    public void test2ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(2, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(1, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 3 entries with a {@link Comparator}
     */
    @Test
    public void test3ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(3, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(2, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 4 entries with a {@link Comparator}
     */
    @Test
    public void test4ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(4, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(3, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 5 entries with a {@link Comparator}
     */
    @Test
    public void test5ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(5, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(4, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 6 entries with a {@link Comparator}
     */
    @Test
    public void test6ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(6, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(5, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 7 entries with a {@link Comparator}
     */
    @Test
    public void test7ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(7, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(6, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 8 entries with a {@link Comparator}
     */
    @Test
    public void test8ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(8, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(7, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 9 entries with a {@link Comparator}
     */
    @Test
    public void test9ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(9, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(8, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * Test {@link MapUtils#navigableMapOf()} with 10 entries with a {@link Comparator}
     */
    @Test
    public void test10ElementNavigableMapOfMethodComparator() {
        NavigableMap<Integer, String> map = MapUtils.navigableMapOf(
                10, "j",
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                1, "a",
                Comparator.reverseOrder());
        assertMapSorted(map, Comparator.reverseOrder());        
        assertEquals(10, map.size());
        assertEquals("a", map.lastEntry().getValue());
        assertThrows(UnsupportedOperationException.class, () -> map.put(100, "100th"));
        
        NavigableMap<Integer, String> map2 = MapUtils.navigableMapOf(
                10, "j",
                9, "i",
                8, "h",
                7, "g",
                6, "f",
                5, "e",
                4, "d",
                3, "c",
                2, "b",
                2, "replacedb",
                Comparator.reverseOrder());
        
        assertEquals(9, map2.size());
        assertEquals("replacedb", map2.get(2));
    }
    
    /**
     * @param <U> the key type
     * @param <V> the value type
     * @param map the map for which we want to check sorting
     * @param comparator the {@link Comparable} to use to check sorting. May be null; if null, use natural ordering.
     */
    public static <U extends Comparable<U>, V> void assertMapSorted(final Map<U, V> map,
                                                                    final Comparator<U> comparator) {
        final List<U> sortedKeys = new LinkedList<>(map.keySet());
        Collections.sort(sortedKeys, comparator);
        assertEquals(sortedKeys, List.copyOf(map.keySet()));
    }
}
