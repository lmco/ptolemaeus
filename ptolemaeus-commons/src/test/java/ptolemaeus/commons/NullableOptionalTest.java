/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

/**
 * Tests for {@link NullableOptional}
 * @author Peter Davis, peter.m.davis@lmco.com
 */
class NullableOptionalTest {

    /**
     * Tests {@link NullableOptional#of(Object)},  {@link NullableOptional#empty()}, {@link NullableOptional#get()}, 
     * and {@link NullableOptional#getOrNull()}.
     */
    @Test
    void testBasics() {
        NullableOptional<Integer> empty = NullableOptional.empty();
        assertTrue(empty.isEmpty());
        assertFalse(empty.isPresent());
        NullableOptional<Integer> empty2 = NullableOptional.empty();
        assertSame(empty, empty2);
        assertThrows(NoSuchElementException.class, () -> empty.get());
        assertNull(empty.getOrNull());

        NullableOptional<Integer> present = NullableOptional.of(2);
        assertTrue(present.isPresent());
        assertFalse(present.isEmpty());
        assertEquals(2, present.get());

        NullableOptional<Integer> nullPresent = NullableOptional.of(null);
        assertTrue(nullPresent.isPresent());
        assertNull(nullPresent.get());
    }

    /**
     * Tests {@link NullableOptional#orElse(Object)}, {@link NullableOptional#orElseGet(java.util.function.Supplier)}, 
     * {@link NullableOptional#orElseThrow()}, {@link NullableOptional#orElseThrow(java.util.function.Supplier)},
     * and {@link NullableOptional#or(java.util.function.Supplier)}
     */
    @Test
    void testOrElses() {
        NullableOptional<Integer> present = NullableOptional.of(2);

        NullableOptional<Integer> empty = NullableOptional.empty();

        assertAll("orElse(T)",
            () -> assertEquals(5, empty.orElse(5)),
            () -> assertEquals(2, present.orElse(5))
        );

        assertAll("orElseGet(Supplier)",
            () -> assertThrows(NullPointerException.class, () -> present.orElseGet(null)),
            () -> assertEquals(5, empty.orElseGet(() -> 5)),
            () -> assertEquals(2, present.orElseGet(() -> 5))
        );
        
        assertAll("orElseThrow() and orElseThrow(Supplier)",
            () -> assertThrows(NullPointerException.class, () -> empty.orElseThrow(null)),
            () -> assertThrows(IllegalStateException.class, () -> empty.orElseThrow(IllegalStateException::new)),
            () -> assertDoesNotThrow(() -> present.orElseThrow(RuntimeException::new)),
            () -> assertThrows(NoSuchElementException.class, () -> empty.orElseThrow()),
            () -> assertDoesNotThrow(() -> present.orElseThrow())
        );

        assertAll("or(Supplier)",
            () -> assertThrows(NullPointerException.class, () -> empty.or(null)),
            () -> assertThrows(NullPointerException.class, () -> empty.or(() -> null)),
            () -> assertEquals(2, empty.or(() -> NullableOptional.of(2)).get()),
            () -> assertEquals(2, present.or(() -> NullableOptional.of(5)).get())
        );
    }

    /**
     * Tests {@link NullableOptional#ifPresent(java.util.function.Consumer)} and 
     * {@link NullableOptional#ifPresentOrElse(java.util.function.Consumer, Runnable)}
     */
    @Test
    void testIfPresents() {
        AtomicInteger accum = new AtomicInteger(0);
        AtomicInteger emptyAccum = new AtomicInteger(0);
        NullableOptional<Integer> empty = NullableOptional.empty();
        Consumer<? super Integer> incrementAccum = val -> accum.incrementAndGet();
        Runnable incrementEmptyAccum = () -> emptyAccum.incrementAndGet();

        empty.ifPresent(incrementAccum);
        assertEquals(0, accum.get());
        empty.ifPresentOrElse(incrementAccum, incrementEmptyAccum);
        assertAll(
            () -> assertEquals(1, emptyAccum.get()),
            () -> assertEquals(0, accum.get())
        );

        NullableOptional<Integer> present = NullableOptional.of(2);
        present.ifPresent(incrementAccum);
        assertEquals(1, accum.get());

        present.ifPresentOrElse(incrementAccum, incrementEmptyAccum);
        assertAll(
            () -> assertEquals(2, accum.get()),
            () -> assertEquals(1, emptyAccum.get()));

        assertAll("Null checks",
            () -> assertThrows(NullPointerException.class, () -> empty.ifPresent(null)),
            () -> assertThrows(NullPointerException.class, () -> empty.ifPresentOrElse(null, incrementEmptyAccum)),
            () -> assertThrows(NullPointerException.class, () -> empty.ifPresentOrElse(incrementAccum, null))
        );
    }

    /**
     * Tests {@link NullableOptional#filter(Predicate)}
     */
    @Test
    void testFilter() {
        NullableOptional<Integer> empty = NullableOptional.empty();
        NullableOptional<Integer> even = NullableOptional.of(2);
        NullableOptional<Integer> odd = NullableOptional.of(1729);
        Predicate<Integer> isEven = i -> i % 2 == 0;

        assertAll(
            () -> assertThrows(NullPointerException.class, () -> empty.filter(null)),
            () -> assertTrue(empty.filter(isEven).isEmpty()),
            () -> assertTrue(even.filter(isEven).isPresent()),
            () -> assertTrue(odd.filter(isEven).isEmpty())
        );
    }

    /**
     * Tests {@link NullableOptional#flatMap(Function)}
     */
    @Test
    void testFlatMap() {
        NullableOptional<Integer> empty = NullableOptional.empty();
        Function<? super Integer, NullableOptional<? extends Double>> halve = i -> NullableOptional.of(i / 2.0);
        assertAll(
            () -> assertThrows(NullPointerException.class, () -> empty.flatMap(null)),
            () -> assertThrows(NullPointerException.class, () -> NullableOptional.of(2).flatMap(i -> null)),
            () -> assertTrue(empty.flatMap(halve).isEmpty()),
            () -> assertEquals(1.5, NullableOptional.of(3).flatMap(halve).get())
        );
    }

    /**
     * Tests {@link NullableOptional#map(Function)} and {@link NullableOptional#mapIfNotNull(Function)}
     */
    @Test
    void testMaps() {
        NullableOptional<Integer> empty = NullableOptional.empty();
        NullableOptional<Integer> present = NullableOptional.of(2);
        NullableOptional<Integer> nullValue = NullableOptional.of(null);
        Function<? super Integer, ? extends Double> halve = i -> i / 2.0;

        assertAll("map(Function)",
            () -> assertThrows(NullPointerException.class, () -> empty.map(null)),
            () -> assertTrue(empty.map(halve).isEmpty()),
            () -> assertEquals(1.0, present.map(halve).get()),
            () -> assertThrows(NullPointerException.class, () -> nullValue.map(halve))
        );

        assertAll("mapIfNotNull",
            () -> assertThrows(NullPointerException.class, () -> empty.mapIfNotNull(null)),
            () -> assertTrue(empty.mapIfNotNull(halve).isEmpty()),
            () -> assertEquals(1.0, present.mapIfNotNull(halve).get()),
            () -> assertTrue((nullValue).mapIfNotNull(halve).isEmpty())
        );
    }

    /**
     * Tests {@link NullableOptional#stream()}
     */
    @Test
    void testStream() {
        NullableOptional<Integer> empty = NullableOptional.empty();
        NullableOptional<Integer> present = NullableOptional.of(2);

        assertAll(
            () -> assertEquals(0, empty.stream().count()),
            () -> assertEquals(2, present.stream().findFirst().get())
        );
    }

    /**
     * Tests {@link NullableOptional#equals(Object)}, {@link NullableOptional#hashCode()}, and 
     * {@link NullableOptional#toString()}.
     */
    @Test
    void testEqualsHashCodeAndToString() {
        NullableOptional<Integer> empty = NullableOptional.empty();
        NullableOptional<Integer> empty2 = NullableOptional.empty();
        assertAll("Comparing empty NullableOptionals",
            () -> assertEquals(empty, empty2),
            () -> assertEquals(empty.hashCode(), empty2.hashCode()),
            () -> assertEquals(empty.toString(), empty2.toString()),
            () -> assertEquals("NullableOptional.empty", empty.toString())
        );

        NullableOptional<Integer> present = NullableOptional.of(2);
        NullableOptional<Integer> present2 = NullableOptional.of(2);

        assertAll("Comparing present NullableOptionals",
            () -> assertEquals(present, present2),
            () -> assertEquals(present.hashCode(), present2.hashCode()),
            () -> assertEquals(present.toString(), present2.toString()),
            () -> assertEquals("NullableOptional.present[2]", present.toString()),
            () -> assertNotEquals(present, empty),
            () -> assertNotEquals(present, NullableOptional.of(3))
        );
        
        // edge cases
        assertNotEquals(present, null);
        assertNotEquals(present, new Object());
    }

}
