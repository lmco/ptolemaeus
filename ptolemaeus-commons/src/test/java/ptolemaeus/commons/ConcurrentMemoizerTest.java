/** MIT License
 *
 * Copyright (c) 2025 Lockheed Martin Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package ptolemaeus.commons;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.map.LRUMap;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link ConcurrentMemoizer}
 * @author Peter Davis, peter.m.davis@lmco.com
 */
public class ConcurrentMemoizerTest {
    
    /**
     * A private {@link ExecutorService} for concurrency tests.
     */
    private static ExecutorService executor;

    /**
     * Set up {@link #executor}
     */
    @BeforeAll
    static void setupExecutor() {
        executor = Executors.newFixedThreadPool(1);
    }

    /**
     * Shut down {@link #executor}
     */
    @AfterAll
    static void shutdownExecutor() {
        executor.shutdown();
    }

    /**
     * Test constructors and non-contesting execution paths.
     * @throws TimeoutException if a timeout occurs.
     */
    @Test
    void testBasics() throws TimeoutException {
        ConcurrentMemoizer<Integer, Object> memoizer = new ConcurrentMemoizer<>(i -> new Object());

        Object obj = memoizer.computeIfAbsent(1);
        assertSame(obj, memoizer.computeIfAbsent(1));
        assertSame(obj, memoizer.get(1).get());
        assertSame(obj, memoizer.get(1, Duration.ofMillis(100)).get());
        
        assertTrue(memoizer.get(2).isEmpty());
        assertTrue(memoizer.get(2, Duration.ofMillis(100)).isEmpty());

        memoizer.clear();
        assertNotSame(obj, memoizer.computeIfAbsent(1));

        ConcurrentMemoizer<Integer, Object> memoizerWithCustomMap = new ConcurrentMemoizer<>(i -> new Object(),
                () -> new LRUMap<>(2), null);
        obj = memoizerWithCustomMap.computeIfAbsent(1);
        memoizerWithCustomMap.computeIfAbsent(2);
        memoizerWithCustomMap.computeIfAbsent(3);
        assertNotSame(obj, memoizerWithCustomMap.computeIfAbsent(1));
    }

    /**
     * Test timeout behaviors. We force a timeout by running the compute function in another thread, but forcing it to
     * wait on a {@link CountDownLatch} , {@code blocker}, in this thread. But to make sure this thread doesn't check 
     * for the result before the computer thread has started, we have a second {@link CountDownLatch} that the compute
     * function releases before doing anything else. The interleaving looks nominally like this:
     * <ol>
     *  <li>This thread submits the compute job to the executor</li>
     *  <li>This thread immediately waits on {@code computeStarted}</li>
     *  <li>The executor eventually runs the compute thread, which releases {@code computeStarted} </li>
     *  <li>The compute thread has now started computing; the {@link Future} is in the cache and not done, so 
     * {@link ConcurrentMemoizer#get(Object)} will start returning empty </li>
     *  <li>The compute thread waits on {@code blocker}, which will release only when we're done testing. </li>
     *  <li>Meanwhile, the main thread can call the timeout {@link ConcurrentMemoizer#get(Object, Duration)} 
     *   method, and will time out, since the compute thread is blocked.</li>
     * </ol>
     * 
     * @throws InterruptedException if we get interrupted.
     */
    @Test
    void testTimeout() throws InterruptedException {
        CountDownLatch computeStarted = new CountDownLatch(1);
        CountDownLatch blocker = new CountDownLatch(1);
        ConcurrentMemoizer<Integer, Object> memoizer = new ConcurrentMemoizer<>(i -> {
            computeStarted.countDown();
            try {
                blocker.await();
            }
            catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                throw new RuntimeException();
            }
            return new Object();
        });

        Future<Object> future = null;
        try {
            future = executor.submit(() -> memoizer.computeIfAbsent(1));
            computeStarted.await();
            assertTrue(memoizer.get(1).isEmpty());
            assertThrows(TimeoutException.class, () -> memoizer.get(1, Duration.ofMillis(10)));         
        }
        finally {
            blocker.countDown();
            if (future != null) {
                future.cancel(false);
            }
        }
    }

    /**
     * Tests that exceptions in {@link ConcurrentMemoizer#computeIfAbsent(Object)} are wrapped in a
     * {@link RuntimeException} and thrown to callers.
     */
    @Test
    void testExecutionException() {
        ConcurrentMemoizer<Integer, Integer> memoizer = new ConcurrentMemoizer<>(i -> 150 / i);

        assertThrows(RuntimeException.class, () -> memoizer.computeIfAbsent(0));
        assertAll(
            () -> assertThrows(RuntimeException.class, () -> memoizer.computeIfAbsent(0)),
            () -> assertThrows(RuntimeException.class, () -> memoizer.get(0)),
            () -> assertThrows(RuntimeException.class, () -> memoizer.get(0, Duration.ofMillis(100)))
        );
    }
    
    /**
     * Test graceful exception-handling
     * @throws TimeoutException shouldn't be thrown
     */
    @Test
    public void exceptional() throws TimeoutException {
        List<Exception> exceptionList = new ArrayList<>();
        ConcurrentMemoizer<Integer, Integer> memoizer = new ConcurrentMemoizer<>(i -> 150 / i,
                LinkedHashMap::new, exceptionList::add);

        assertNull(memoizer.computeIfAbsent(0));
        assertNull(memoizer.get(0));
        assertNull(memoizer.get(0, Duration.ofSeconds(1)));
        
        assertEquals(3, exceptionList.size());
    }
    
    /**
     * Test handle() on an InterruptedException
     */
    @Test
    public void handleInterrupted() {
        ConcurrentMemoizer<Integer, Integer> memoizer = new ConcurrentMemoizer<>(i -> i);
        InterruptedException ex = new InterruptedException();
        assertThrows(RuntimeException.class, () -> memoizer.handle(ex, "My message"));
        assertTrue(Thread.interrupted());
    }
}
