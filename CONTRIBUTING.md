# Contributing

## Communication

To contribute, ask questions, or for general communication, please use our Discord channel [here](https://discord.gg/6nCxQHvpgS), or create a GitLab Issue, if you're certain that there's an issue.

## The Basics

As long as the project remains small, we'll keep things simple:

  1. You must be a member of the project to be able to contribute.
  2. Enable 2FA
  3. Clone the repository locally
  4. Create a feature-branch off of `main` with a descriptive name.  I personally like the branch names to match what I expect the MR title to be.
      * If your changes are miscellaneous changes/improvements without a particularly meaningful theme tying them together, you are encouraged to use a name that matches the following pattern: `misc_2023-10-16` where the date string is in ISO 8601 date format.
      * As suggested, this branch naming convention also applies to MRs
  5. Make and commit your changes (be sure to reference the development guide for style and testing rules, etc.).
  6. Push your branch to remote
  7. Create an MR
      * Your MR description should cover the scope and impact of your changes among the baseline and, if possible, impacts that users might expect to see; e.g. performance impacts due to a change in a numerical optimization.
      * Be sure to link any references that were used to write/can be used to verify the code in the MR, if applicable.  Links should also be included in the JavaDoc itself, but putting it in the MR description is a good documentation practice.
  8. Tag me (`@RyanMoser`) for review.  I should get an email when you post the MR, but tag me just to be sure.  If I'm not available, contact anybody marked as a maintainer or owner of the project.

## Creating GitLab Issues

Feel free to create `Issue`s as you see fit (though don't abuse the system).  `Issue`s will be used to track bugs and to propose and discuss future features/changes.

If you're writing an `Issue` for a bug, include minimal code to reproduce the problem.  For this low-level project, that should be simple.

Toy example:

```java
/** this temporary test method demonstrates a bug that [blah blah].  Issue URL: [URL to issue here] */
@Test
void testDemoLevenbergMarquardtBug () {
    final AnInputType  input  = new AnInputType(blah);
    final AnOutputType output = new Solver().solve(input);
    final double expected = 123.456;
    assertEquals(expected, output.getDoubleValue()); // this assertion fails and it shouldn't
}
```